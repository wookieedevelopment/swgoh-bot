const http = require("http");
const path = require("path");
const fs = require('fs').promises;

let indexFile;
let bgImage;
let favicon;
let gameData;

const host = 'localhost';
const port = 80;

const requestListener = function (req, res) {
    // get the file path from req.url, or '/public/index.html' if req.url is '/'
    const filePath = (req.url === '/') ? '/index.html' : req.url;
    let contents;

    // determine the contentType by the file extension
    const extname = path.extname(filePath);
    let contentType = 'text/html';
    if (extname === '.js') {
        contentType = 'text/javascript';
    } else if (extname === '.json') {
        contentType = 'application/json';
        if (filePath == "/gameData.json") contents = gameData;
    } else if (extname === '.css') {
        contentType = 'text/css';
    } else if (extname === '.jpg') {
        contentType = 'image/jpg';
        if (filePath == "/background.jpg")
        {
            contents = images[Math.floor(Math.random()*images.length)].img;
        }
    } else if (extname === '.ico')
    {
        contentType = "image/x-icon";
        contents = favicon;
    }

    // pipe the proper file to the res object
    res.writeHead(200, { 'Content-Type': contentType });

    switch (filePath)
    {
        case '/index.html': contents = indexFile; break;
    }

    res.end(contents);
};

const server = http.createServer(requestListener);

fs.readFile(__dirname + "/index.html")
    .then(contents => {
        indexFile = contents;
        server.listen(port, () => {
            console.log(`Server is running on http://${host}:${port}`);
        });
    })
    .catch(err => {
        console.error(`Could not read index.html file: ${err}`);
        process.exit(1);
    });

fs.readFile(__dirname + "/favicon.ico")
    .then(contents => {
        favicon = contents;
    });

fs.readFile(__dirname + "/../statCalcData/gameData.json")
    .then(contents => {
        gameData = contents;
    });

let imagesPath = '/img/';
let images = [];

fs.readdir(__dirname + imagesPath)
    .then(fileNames => {
            fileNames.forEach(fn => {
                fs.readFile(__dirname + imagesPath + fn).then(buf => images.push({ fileName: fn, img: buf}));
            })
        }
    );