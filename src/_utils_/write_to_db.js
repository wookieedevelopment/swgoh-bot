const database = require ("../database")

const QUERY_CATEGORIES = {
    SUMMARY: "SUMMARY",
    GENERAL: "GENERAL",
    DATACRON: "DATACRON",
    CUSTOM: "CUSTOM"
}

const RENDER_TYPES = {
    COUNT: "COUNT",
    REPORT: "REPORT"
}

const PRESET_IDS = {
    OVERVIEW: -1000,
    TW_OMICONS: -500,
    DATACRONS: -250
}

const QUERIES = [
    {
        id: PRESET_IDS.OVERVIEW,
        name: "Overview",
        view: RENDER_TYPES.REPORT,
        category: QUERY_CATEGORIES.SUMMARY
    },
    {
        id: PRESET_IDS.DATACRONS,
        name: "Datacrons",
        view: RENDER_TYPES.REPORT,
        category: QUERY_CATEGORIES.SUMMARY
    },
    {
        id: PRESET_IDS.TW_OMICONS,
        name: "TW Omicrons",
        view: RENDER_TYPES.REPORT,
        category: QUERY_CATEGORIES.SUMMARY
    },
    {
        id: 3,
        name: "CAT FAQs",
        queries: [
            {
                query: "cat(speed>1, health>1)"
            }
        ],
        view: RENDER_TYPES.REPORT,
        category: QUERY_CATEGORIES.GENERAL
    },
    {
        id: 2,
        name: "SLK Speeds",
        queries: [
            {
                query: "slk(speed>1)"
            }
        ],
        view: RENDER_TYPES.REPORT,
        category: QUERY_CATEGORIES.GENERAL
    },
    {
        id: 4,
        name: "Strong BSOJ",
        queries: [
            {
                query: "bsoj(o>=1); fennec(r>=5)"
            }
        ],
        view: RENDER_TYPES.COUNT,
        category: QUERY_CATEGORIES.GENERAL
    },
    {
        id: 1,
        name: "Trooper Speeds",
        queries: [
            {
                query: "piett(speed>1)"
            }
        ],
        view: RENDER_TYPES.REPORT,
        category: QUERY_CATEGORIES.GENERAL
    },
    {
        id: 5,
        name: "Rey Dash as LV Counter",
        queries: [  // conceptually, two queries, the first which shows how many dash omi with L9 bonus & L6 grit, second just dash bonus turn DCs
            {
                query: "rey(u=1); dash(r>=7)",
                datacronRequirements: {
                    abilities: [2140, 2130]
                }
            },
            {   
                query: "rey(u=1); dash(r>=7)",
                datacronRequirements: {
                    abilities: [2140]
                }
            }
        ],
        view: RENDER_TYPES.COUNT,
        category: QUERY_CATEGORIES.DATACRON
    },
    {
        id: 5,
        name: "Phasma as GL Counter",
        queries: [  
            {
                query: "phasma(o=1, r>=7)",
                datacronRequirements: {
                    abilities: [114]
                }
            }
        ],
        view: RENDER_TYPES.COUNT,
        category: QUERY_CATEGORIES.DATACRON
    }
]


new Promise(async (resolve, reject) =>
{
for (var q of QUERIES) {

    await database.db.any(`
    INSERT INTO tw_scout_query (query_name, view_type, category, queries)
    VALUES ($1, $2, $3, $4)`,
    [q.name, q.view, q.category, JSON.stringify(q.queries)])
}
});