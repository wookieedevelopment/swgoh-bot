var x = new Date("20120321")


var input = "teamname jedi master kenobi(g<13, z>6); ki-adi mundi (g=13, z>=1); ahsoka tano (g<3, z=1)"
var squadRx = /([\w\-()'\s]+)\s*\(([<>=\w,\s]*)\)/g
var featureRx = /\s*([\w]+)\s*(<|>|=|<=|>=)\s*([\w]+)\s*/g

input = input.trim();
var firstSpace = input.indexOf(" ");
var teamName = input.slice(0, firstSpace);
console.log(teamName);
var unitsString = input.slice(firstSpace).trim();
console.log(unitsString);

let match = squadRx.exec(unitsString);
do {
    var unitName = match[1].trim();
    var features = match[2].trim();
    let featuresMatch = featureRx.exec(features);
    
    console.log(unitName)

    do {
        var featureType = featuresMatch[1].trim();
        var comparison = featuresMatch[2].trim();
        var featureValue = featuresMatch[3].trim();
        console.log("type: " + featureType + ", comparison: " + comparison + ", value: " + featureValue);
    } while ((featuresMatch = featureRx.exec(features)) != null)

} while ((match = squadRx.exec(unitsString)) != null)
