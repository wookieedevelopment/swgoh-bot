const { SlashCommandSubcommandBuilder } = require("@discordjs/builders");
const { EmbedBuilder } = require("discord.js");
const discordUtils = require("../../utils/discord.js");

module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("help")
        .setDescription("WookieeBot Guild Management Help"),
    interact: async function(interaction)
    {
        let embed = this.getHelpEmbed();
        await interaction.editReply({ embeds: [embed] });
    },
    execute: async function(input)
    {
        let embed = this.getHelpEmbed();
        await input.channel.send({ embeds: [embed] });
    },
    getHelpEmbed: function(input) {

        let overviewCommandsField = {
            name: "Overview (All Registered Guild Members)",
            value:
`**/guild status**: show status of your guild and user registration`,
            inline: false
        };

        let statusCommandsField = {
            name: "Organization (Guild Admin Only)",
            value:
`**/guild link type:server**
**/guild link type:channel**: set all commands on the server/channel to be related to your guild. Specify alt guilds with alt-guild-reference, providing the number of the guild shown in **/user me**.  Channel registration takes precedence over server registration.
**/guild admin**: make a guild member an administrator for WookieeBot for your guild`,
            inline: false
        };

        
        let dataCommandsField = {
            name: "Data Management (Guild Admin Only)",
            value:
`**/guild refresh roster**: refresh your guild's roster. This happens automatically once per day, but you may need to do so more often.
**/guild tb upload-tbstats**, **/guild tb upload-hotutils-export**, **/guild tb list-data**, **/guild tb delete**
**/guild tb delete-upload**, **/guild tb add**: Manage TB data imports from HotUtils
**/guild cache all**: Point-in-time cache of player data for the guild. Used for TW planning/replanning.
**/guild cache one**: Cache one player in your guild (much faster)
**/guild cache list**: List guild caches`,
            inline: false
        };


        let embed = new EmbedBuilder()
            .setTitle("WookieeBot Guild Management")
            .setColor(0xD2691E)
            .addFields(overviewCommandsField, statusCommandsField, dataCommandsField);

        return embed;
    }

}
