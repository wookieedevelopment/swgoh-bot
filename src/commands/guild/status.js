const discordUtils = require("../../utils/discord.js");
const playerUtils = require("../../utils/player.js");
const database = require("../../database");
const { EmbedBuilder } = require("discord.js");
const { SlashCommandSubcommandBuilder } = require("@discordjs/builders");


module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("status")
        .setDescription("Show registration status for your guild"),
    permissionLevel: playerUtils.UserRoles.User,
    interact: async function(interaction)
    {
        let { botUser, guildId, error } = await playerUtils.authorize(interaction, this.permissionLevel)
        if (error) {
            await interaction.editReply(error);
            return;
        }

        let embed = await this.getGuildStatusResponse(guildId);
        await interaction.editReply(embed);
    },
    execute: async function(client, input, args) {
        let { botUser, guildId, error } = await playerUtils.authorize(input, this.permissionLevel)
        if (error) {
            await input.channel.send(error);
            return;
        }

        let embed = await this.getGuildStatusResponse(guildId);
        await input.channel.send(embed);
    },
    getGuildStatusResponse: async function(guildId)
    {
        const members = await database.db.any(`
        select g.guild_name, g.priority, gp.allycode, gp.player_name, ur.discord_id, ugr.guild_admin, ugr.guild_bot_admin, twx.tw_exclusion_id
        from guild_players gp
        left join guild g on gp.guild_id = g.guild_id
        left join user_registration ur on gp.allycode = ur.allycode 
        left join user_guild_role ugr on ur.discord_id = ugr.discord_id and gp.guild_id = ugr.guild_id
        left join tw_exclusion twx on (gp.allycode = twx.allycode and gp.guild_id = twx.guild_id)
        where gp.guild_id = $1
        order by ugr.guild_admin ASC, gp.player_name ASC`, [guildId]);

        if (members.length == 0) {
            return "Your guild is not registered with WookieeBot :cry:";
        }

        var guildName = members[0].guild_name;
        let subscribed = members[0].priority > 0;
        
        var embedDescription = (subscribed ? `Subscribed ${discordUtils.symbols.ok}\n\n` : `Not subscribed ${discordUtils.symbols.fail}\n\n`);

        for (var m in members)
        {
            var member = members[m];
            var allycode = member.allycode;
            var name = member.player_name;
            var discordId = discordUtils.makeIdTaggable(member.discord_id);
            
            embedDescription += name + " (" + allycode + "): " + (discordId ? discordId : ":warning: Not Registered") + (member.guild_admin ? " :crown:" : "") + "\n";
        }

        var embedToSend = new EmbedBuilder()
            .setTitle("Guild registration status for **" + guildName + "**:")
            .setDescription(embedDescription)
            .setColor(0xD2691E);

        return { embeds: [embedToSend] };
    }
    
}