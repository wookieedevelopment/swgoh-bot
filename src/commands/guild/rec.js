
const playerUtils = require("../../utils/player.js");
const { SlashCommandSubcommandGroupBuilder } = require("@discordjs/builders");
const recOmicron = require("./rec/omicron.js");
const recDatacron = require("./rec/datacron.js");
const recRemove = require("./rec/remove.js");
const recShow = require("./rec/show.js");
const recTemplate = require("./rec/template.js");

module.exports = {
    data: new SlashCommandSubcommandGroupBuilder()
        .setName("rec")
        .setDescription("Manage recommendations for your guild")
        .addSubcommand(recOmicron.data)
        .addSubcommand(recDatacron.data)
        .addSubcommand(recRemove.data)
        .addSubcommand(recShow.data)
        .addSubcommand(recTemplate.data)
        ,
    permissionLevel: playerUtils.UserRoles.User,
    interact: async function(interaction)
    {
        let subcommand = interaction.options.getSubcommand();

        switch (subcommand)
        {
            case recOmicron.data.name:
                await recOmicron.interact(interaction);
                break;
            case recDatacron.data.name:
                await recDatacron.interact(interaction);
                break;
            case recRemove.data.name:
                await recRemove.interact(interaction);
                break;
            case recShow.data.name:
                await recShow.interact(interaction);
                break;
            case recTemplate.data.name:
                await recTemplate.interact(interaction);
                break;
        }
    },
    autocomplete: async function(interaction)
    {
        let subcommand = interaction.options.getSubcommand();
  
        switch (subcommand)
        {
            case recOmicron.data.name:
                await recOmicron.autocomplete(interaction);
                break;
            case recDatacron.data.name:
                await recDatacron.autocomplete(interaction);
                break;
            case recRemove.data.name:
                await recRemove.autocomplete(interaction);
                break;
            case recTemplate.data.name:
                await recTemplate.autocomplete(interaction);
                break;
        }
    }
}