const guildUtils = require("../../../utils/guild.js");
const playerUtils = require("../../../utils/player.js");
const swapiUtils = require("../../../utils/swapi")
const database = require("../../../database");
const { logger, formatError } = require("../../../utils/log");
const { EmbedBuilder } = require("discord.js");
const { SlashCommandSubcommandBuilder } = require("@discordjs/builders");

module.exports = {
    data: new SlashCommandSubcommandBuilder()
            .setName("roster")
            .setDescription("Refresh the players in the guild."),
    permissionLevel: playerUtils.UserRoles.GuildAdmin,
    interact: async function(interaction)
    {
        let { botUser, guildId, error } = await playerUtils.authorize(interaction, this.permissionLevel)
        if (error) {
            await interaction.editReply(error);
            return;
        }

        await interaction.editReply(`Refreshing guild data for your guild.`)

    
        var comlinkGuildId;
        try {
            comlinkGuildId = await database.db.one("SELECT comlink_guild_id FROM guild WHERE guild_id = $1", [guildId], a => a.comlink_guild_id)
        } catch {
            await interaction.followUp("Your guild doesn't seem to be properly registered.  Contact the bot developer.");
            return;
        }

        var guild;

        try {
            guild = await guildUtils.getGuildDataFromAPI({ comlinkGuildId: comlinkGuildId }, { useCache: false });
        }
        catch (e)
        {
            logger.error(formatError(e));
            await interaction.followUp({ embeds: [this.createErrorEmbed("Error", e.message)] });
            return;
        }

        let members = guildUtils.getGuildAllycodesForAPIGuild(guild);
        if (!members || members.length == 0)
        {
            await interaction.followUp({ embeds: [this.createErrorEmbed("Error", "Guild seems to have no members for ally code " + allycode + ".")] });
            return;
        }

        let guildName = guildUtils.getGuildName(guild);
        await interaction.followUp(`Found guild **${guildName}** with ${members.length} members.`)
        
        try {
            await guildUtils.updateGuildDetails(guild);
            await guildUtils.refreshGuildPlayers(guild);
            await interaction.followUp(`Updated guild roster for guild **${guildName}**.  Found ${members.length} members.`)
        } catch (error) {
            let errorMsg = `Failed to populate Guild players for guild **${guildName}**.`;
            logger.error(errorMsg + "\r\n" + formatError(error))
            await interaction.followUp(errorMsg)
            return;
        }
    },
    createErrorEmbed: function(title, errorMessage) {
        errorEmbed = new EmbedBuilder().setTitle(title).setColor(0xaa0000).setDescription(errorMessage);
        return errorEmbed;
    }
}