const playerUtils = require("../../utils/player.js");
const { SlashCommandSubcommandGroupBuilder } = require("@discordjs/builders");
const criteriaSet = require("./criteria/set.js");

module.exports = {
    data: new SlashCommandSubcommandGroupBuilder()
        .setName("criteria")
        .setDescription("Manage recruiting criteria for your guild")
        .addSubcommand(criteriaSet.data)
        ,
    permissionLevel: playerUtils.UserRoles.GuildAdmin,
    interact: async function(interaction)
    {
        let subcommand = interaction.options.getSubcommand();

        switch (subcommand)
        {
            case criteriaSet.data.name:
                await criteriaSet.interact(interaction);
                break;
        }
    },
    autocomplete: async function(interaction)
    {
        let subcommand = interaction.options.getSubcommand();
  
        switch (subcommand)
        {
            // case criteriaSet.data.name:
            //     await criteriaSet.autocomplete(interaction);
            //     break;
         
            
        }
    }
}