const discordUtils = require("../../utils/discord");
const playerUtils = require("../../utils/player");
const guildUtils = require("../../utils/guild");
const database = require("../../database");
const { SlashCommandSubcommandBuilder } = require("@discordjs/builders");
const { AttachmentBuilder } = require("discord.js");
const { parseAsync } = require("json2csv");

module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("data")
        .setDescription("Download a csv of your guild's data.")
        .addIntegerOption(o =>
            o.setName("alt")
            .setDescription("Your alt account")
            .setRequired(false)
            .setAutocomplete(true)),
    permissionLevel: playerUtils.UserRoles.User,
    interact: async function(interaction) {
        let alt = interaction.options.getInteger("alt");

        let { botUser, guildId, error } = await playerUtils.authorize(interaction);

        if (error)
        {
            await interaction.editReply({ content: error });
            return;
        }

        let allycode = playerUtils.chooseBestAllycode(botUser, guildId, null, alt, true);

        if (!allycode)
        {
            await interaction.editReply({ content: "Register with `/register` or provide a valid allycode or alt." });
            return;
        }

        allycode = playerUtils.cleanAndVerifyStringAsAllyCode(allycode);
        let guild = botUser.guilds.find(g => g.guildId === guildId);
        let comlinkGuildId, guildName;

        if (!guild)
        {
            let result = await database.db.oneOrNone("SELECT * FROM guild WHERE guild_id = (SELECT guild_id FROM guild_players WHERE allycode = $1)", [allycode]);
            if (!result)
            {
                await interaction.editReply({ content: "Guild is not registered. Register with `/register`." });
                return;
            }

            guildId = result.guild_id;
            comlinkGuildId = result.comlink_guild_id;
            guildName = result.guild_name;
        }
        else
        {
            comlinkGuildId = guild.comlinkGuildId;
            guildName = guild.guildName;
        }


        let csvData = await generateCsvData(guildId);

        
        const csvParsingOptions = { defaultValue: 0 }
        let csvText = await parseAsync(csvData, csvParsingOptions);
        let buffer = Buffer.from(csvText);
        let csvAttachment = new AttachmentBuilder(buffer, { name: `guild_data_${comlinkGuildId}_${new Date().getTime()}.csv`});

        await interaction.editReply({
            content: `Guild data for **${guildName}** is in the attached CSV file.`,
            files: [csvAttachment]
        });
    },
    autocomplete: async function(interaction) {
        
		const focusedOption = interaction.options.getFocused(true);
        let choices;

        if (focusedOption.name === "alt")
        {
            choices = await playerUtils.getAltAutocompleteOptions(focusedOption.value, interaction.user.id);    
        }

        await interaction.respond(choices);
    },
}

async function generateCsvData(guildId)
{
    
    let csvData = [];

    let result = await database.db.multiResult(
        `
        SELECT 
            pds.allycode,
            ur.discord_id,
            player_name,
            gl_rey, gl_slkr, gl_jml, gl_see, gl_jmk, gl_lv, gl_jabba, gl_leia, gl_ahsoka
            capital_ships,
            timestamp
        FROM player_data_static pds
        LEFT JOIN user_registration ur ON (ur.allycode = pds.allycode)
        WHERE pds.allycode in (SELECT allycode FROM guild_players WHERE guild_id = $1);

        SELECT t_limited.*
        FROM (
            SELECT DISTINCT allycode
            FROM player_Data
            where allycode in (
                select allycode 
                from guild_players 
                where guild_id = $1
            )
        ) t_groups
        JOIN LATERAL (
            SELECT 
                allycode,
                skill_rating,
                gp_fleet,
                gp_squad,
                gear,
                relic,
                mods_six_dot,
                mods_speed10,
                mods_speed15,
                mods_speed20,
                mods_speed25,
                mod_quality,
                mod_quality2,
                mod_quality_hotbot,
                zetas,
                omicrons
            FROM player_data t_all
            WHERE t_all.allycode = t_groups.allycode
            ORDER BY t_all.allycode,t_all.timestamp desc
            limit 1
        ) t_limited ON true;
        `, [guildId]);

    for (let r of result[0].rows)
    {
        let r2 = result[1].rows.find(row => row.allycode === r.allycode);

        let glCount = 
            (r.gl_rey ? 1 : 0) + 
            (r.gl_slkr ? 1 : 0) +
            (r.gl_jml ? 1 : 0) +
            (r.gl_see ? 1 : 0) +
            (r.gl_jmk ? 1 : 0) +
            (r.gl_lv ? 1 : 0) +
            (r.gl_jabba ? 1 : 0) +
            (r.gl_leia ? 1 : 0) +
            (r.gl_ahsoka ? 1 : 0)
        ;

        let data = {
            "Name": r.player_name,
            "AllyCode": r.allycode,
            "DiscordId": r.discord_id,
            "Data Timestamp": r.timestamp,
            "Skill Rating": r2.skill_rating,
            "GP": r2.gp_fleet + r2.gp_squad,
            "GP Fleet": r2.gp_fleet,
            "GP Squad": r2.gp_squad,
            "Zetas": r2.zetas,
            "Omicrons": r2.omicrons,
            "G11": r2.gear[11],
            "G12": r2.gear[12],
            "G13": r2.gear[13],
            "R0": r2.relic[2], // relics are 2-indexed
            "R1": r2.relic[3],
            "R2": r2.relic[4],
            "R3": r2.relic[5],
            "R4": r2.relic[6],
            "R5": r2.relic[7],
            "R6": r2.relic[8],
            "R7": r2.relic[9],
            "R8": r2.relic[10],
            "R9": r2.relic[11],
            "Mod Quality (WB)": r2.mod_quality2,
            "Mod Quality (HU)": r2.mod_quality_hotbot,
            "Mod Quality (DSR)": r2.mod_quality,
            "Mods 6 dot": r2.mods_six_dot,
            "Mods 10+ Speed": r2.mods_speed10,
            "Mods 15+ Speed": r2.mods_speed15,
            "Mods 20+ Speed": r2.mods_speed20,
            "Mods 25+ Speed": r2.mods_speed25,
            "GL Count": glCount,
            "Capital Ship Count": r.capital_ships?.length ?? 0
        };

        csvData.push(data);
    }
    
    csvData.sort((a, b) => { return (a.Name.localeCompare(b.Name)); });

    return csvData;
}