const guildUtils = require("../../utils/guild.js");
const playerUtils = require("../../utils/player.js");
const swapiUtils = require("../../utils/swapi.js");
const constants = require ("../../utils/constants.js");
const database = require("../../database");
const refreshRoster = require("./refresh/refreshRoster");
const { logger, formatError } = require("../../utils/log");
const { EmbedBuilder } = require("discord.js");
const cacheUtils = require("../../utils/cache");

module.exports = {
    refreshGuildData : async function(comlinkGuildId, cacheType)
    {
        let guild = await guildUtils.getGuildDataFromAPI({ comlinkGuildId: comlinkGuildId }, { useCache: false, includeRecentGuildActivityInfo: true });

        if (!guild) {
            throw new Error("No guild data for guild ID " + comlinkGuildId);
        }

        if (guildUtils.getGuildMembersCount(guild) == 0) {
            throw new Error("Guild has no members... odd.  Guild ID: " + comlinkGuildId);
        }

        var wookieeBotGuildId = await guildUtils.getWookieeBotGuildIdForAPIGuild(guild);

        if (wookieeBotGuildId == null)
        {
            throw new Error("Guild is not actually tracked? WTF? " + comlinkGuildId);
        }

        await guildUtils.updateGuildDetails(guild);

        logger.info(`Updated guild info details for ${guildUtils.getGuildName(guild)}`);

        await guildUtils.refreshGuildPlayers(guild);

        logger.info(`Refreshed guild roster with ${guildUtils.getGuildMembersCount(guild)} members.`)

        // TEMP: Option to disable player data population for nightly refreshes
        // if (process.env.NIGHTLY_PLAYER_DATA === "true" || cacheType !== constants.CacheTypes.NIGHTLY) await this.populatePlayerData(guild, cacheType);

    },
    createErrorEmbed: function(title, errorMessage) {
        errorEmbed = new EmbedBuilder().setTitle(title).setColor(0xaa0000).setDescription(errorMessage);
        return errorEmbed;
    },
    populatePlayerData: async function(guild, cacheType) {

        if (!guild) return;

        let guildAllyCodes = guildUtils.getGuildAllycodesForAPIGuild(guild);
        let guildId = await guildUtils.getWookieeBotGuildIdForAPIGuild(guild);
        await this.insertPlayerDataForAllyCodes(guildAllyCodes, guildId, cacheType);
    },
    insertPlayerDataForAllyCodes: async function (allyCodes, guildId, cacheType)
    {
        var updateTime = new Date();

        let { players, errors } = await swapiUtils.getPlayers({ allycodes: allyCodes, useCache: true, cacheTimeHours: 1, cacheUpdatedPlayerData: false, cacheType: cacheType });

        // log errors, but don't stop
        if (errors.length > 0)
        {
            logger.error(`Failed to retrieve player data for players in guild Id (${guildId}) as part of nightly refresh.
    ${errors.join("\r\n")}`)
        }


        var playerDataInsertValues = new Array();
        var playerDataStaticInsertValues = new Array();

        for (var playerIndex = 0; playerIndex < players.length; playerIndex++)
        {
            var player = players[playerIndex];

            if (!player) continue;

            let insertData;
            try {
                insertData = this.getPlayerDataInsertValues(player, updateTime);
            } catch (err)
            {
                logger.error(`Error refreshing data for player ${swapiUtils.getPlayerAllycode(player)}\r\n` + formatError(err));
            }

            if (!insertData) continue;

            if (insertData.playerDataInsertValue) playerDataInsertValues.push(insertData.playerDataInsertValue);
            if (insertData.playerDataStaticInsertValue) playerDataStaticInsertValues.push(insertData.playerDataStaticInsertValue);
        }

        const playerDataInsert = database.pgp.helpers.insert(playerDataInsertValues, database.columnSets.playerDataCS);
        const playerDataStaticInsert = database.pgp.helpers.insert(playerDataStaticInsertValues, database.columnSets.playerDataStaticCS);

        const playerDataStaticOnConflict = ' ON CONFLICT(allycode) DO UPDATE SET ' +
            database.columnSets.playerDataStaticCS.assignColumns({from: 'EXCLUDED', skip: ['allycode']});

        // refresh static data & insert new dynamic data
        await database.db.none(playerDataStaticInsert + playerDataStaticOnConflict + "; " + playerDataInsert);
    },
    getPlayerDataInsertValues: function(player, updateTime)
    {
        let retObj = { playerDataInsertValue: null, playerDataStaticInsertValue: null, ultUnits: null };
        if (!player) return retObj;

        var poUTCOffsetMinutes = swapiUtils.getPlayerPayoutUTCOffsetMinutes(player);
        var gp = parseInt(swapiUtils.getGP(player));
        var gpSquad = parseInt(swapiUtils.getGPSquad(player));
        var gpFleet = parseInt(swapiUtils.getGPFleet(player));
        var arenaRank = swapiUtils.getSquadArenaRank(player);
        var fleetRank = swapiUtils.getFleetArenaRank(player);
        var name = swapiUtils.getPlayerName(player);
        var allyCode = swapiUtils.getPlayerAllycode(player);
        let skillRating = swapiUtils.getPlayerSkillRating(player);
        
        let portraits = swapiUtils.getPlayerUnlockedPortraits(player);
        var reekWin = portraits ? portraits.indexOf("PLAYERPORTRAIT_REEK") >= 0 : null;

        let gear = swapiUtils.getPlayerGearSummary(player);
        let relic = swapiUtils.getPlayerRelicSummary(player);

        let playerGLs = swapiUtils.getPlayerGalacticLegends(player);

        let modCounts = swapiUtils.getPlayerModSummary(player);

        let zetaCount = swapiUtils.getPlayerZetaCount(player);
        let omicronCount = swapiUtils.getPlayerOmicronCount(player);

        // same as DSR bot
        let modQuality = modCounts.speed15 / (gpSquad / 100000.0);

        // double counts +20s, triple counts +25s, and incorporates 6dot
        let modQuality2 = (modCounts.speed15 + modCounts.speed20 + modCounts.speed25 + 0.5*modCounts.sixDot) / (gpSquad / 100000.0);

        // hotbot formula ((# of 15-19 Speed * 0.8) + (# of 20-24 Speed) + (# of 25+ Speed * 1.6)) / (squadGP / 100,000)
        let modQualityHotbot = ((modCounts.speed15-modCounts.speed20) * 0.8 + (modCounts.speed20-modCounts.speed25) + modCounts.speed25*1.6) / (gpSquad / 100000.0);

        retObj.playerDataInsertValue = {
            allycode: allyCode,
            player_name: name,
            gp: gp,
            gp_fleet: gpFleet,
            gp_squad: gpSquad,
            arena_rank: arenaRank,
            fleet_rank: fleetRank,
            gl_count: playerGLs.length,
            mod_quality: modQuality,
            mod_quality2: modQuality2,
            mod_quality_hotbot: modQualityHotbot,
            mods_six_dot: modCounts.sixDot,
            mods_speed25: modCounts.speed25,
            mods_speed20: modCounts.speed20,
            mods_speed15: modCounts.speed15,
            mods_speed10: modCounts.speed10,
            gear: gear,
            relic: relic,
            skill_rating: skillRating,
            zetas: zetaCount,
            omicrons: omicronCount,
            timestamp: updateTime
        };

        retObj.playerDataStaticInsertValue = {
            allycode: allyCode,
            player_name: name,
            payout_utc_offset_minutes: poUTCOffsetMinutes,
            reek_win: reekWin,
            gl_rey: playerGLs.indexOf("GLREY") >= 0,
            gl_slkr: playerGLs.indexOf("SUPREMELEADERKYLOREN") >= 0,
            gl_jml: playerGLs.indexOf("GRANDMASTERLUKE") >= 0,
            gl_see: playerGLs.indexOf("SITHPALPATINE") >= 0,
            gl_jmk: playerGLs.indexOf("JEDIMASTERKENOBI") >= 0,
            gl_lv: playerGLs.indexOf("LORDVADER") >= 0,
            gl_jabba: playerGLs.indexOf("JABBATHEHUTT") >= 0,
            gl_leia: playerGLs.indexOf("GLLEIA") >= 0,
            gl_ahsoka: playerGLs.indexOf("GLAHSOKATANO") >= 0,
            timestamp: updateTime
        };

        return retObj;
    }
}