const playerUtils = require("../../utils/player.js");
const discordUtils = require("../../utils/discord.js");
const database = require("../../database");
const guildUtils = require("../../utils/guild.js");
const constants = require("../../utils/constants.js");
const { SlashCommandSubcommandBuilder } = require("@discordjs/builders");


module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("link")
        .setDescription("Link WookieeBot to a Discord channel or server")
        .addStringOption(o =>
            o.setName("type")
            .setDescription("Type of link.")
            .setRequired(true)
            .addChoices(
                { name: "channel", value: "channel" },
                { name: "server", value: "server" }
            ))
        .addIntegerOption(o =>
            o.setName("alt")
            .setDescription("Alt account (/user me)")
            .setRequired(false)
            .setAutocomplete(true)),
    interact: async function(interaction) {
        if (interaction.guild?.id === '537868479471026176' && !discordUtils.isBotOwner(interaction.user.id)) {  
            throw new Error("This must be done on your own server/channel.")
        }
        
        let type = interaction.options.getString("type");
        let linkType, linkId;

        if (interaction.guild && type === "server") 
        {
            // it's a direct message
            linkType = constants.DISCORD_LINK_TYPES.server;
            linkId = interaction.guild.id;
        }
        else
        {
            linkType = constants.DISCORD_LINK_TYPES.channel;
            linkId = interaction.channel.id;
        }

        let { botUser, guildId, error } = await playerUtils.authorize(interaction);

        // choose either the provided alt or the first alt that's in a guild
        let altNum = interaction.options.getInteger("alt");
        let alt = altNum ? botUser.alts.find(a => a.alt == altNum) : botUser.alts.find(a => a.guildId != null);

        let response = await this.addLink(linkId, linkType, interaction.user.id, alt, !interaction.guild);
        await interaction.editReply(response);
    },
    autocomplete: async function(interaction) {
        const focusedValue = interaction.options.getFocused();
        await interaction.respond((await playerUtils.getAltAutocompleteOptions(focusedValue, interaction.user.id)));
    },
    addLink: async function( linkId, type, discordId, alt, isDM)
    {   
        if (!alt || !alt.guildId)
        {
            return { content: "The account must be in a registered guild in order to link. Join a guild if you're not in one, or run `/register` to register your guild." };
        }

        if (!isDM)
        {
            let adminpriv = await database.db.oneOrNone("SELECT discord_id, guild_id, guild_admin, guild_bot_admin FROM user_guild_role WHERE discord_id = $1 AND guild_id = $2", [discordId, alt.guildId]);
            
            if (!adminpriv || !adminpriv.guild_admin)
            {
                return "You must be an admin for your guild to do this.";
            }
        }

        let typeString = (isDM ? "Direct Message Channel" : (type == constants.DISCORD_LINK_TYPES.server ? "Server" : "Channel"));

        try {
            let conflictClause = (isDM ? "ON CONFLICT (discord_link) DO UPDATE SET guild_id = EXCLUDED.guild_id" : "");
            await database.db.any(`INSERT INTO guild_discord_link (guild_id, discord_link, link_type) VALUES ($1, $2, $3) ${conflictClause}`, [alt.guildId, linkId, type]);
        } catch (error)
        {
            if (error.constraint == "discord_link_unique")
                return typeString + " already registered.";
            else
                return "An error occurred.  Please try again or contact the WookieeBot developer for help.";
        }

        return `${typeString} linked to \`${alt.guildName}\`.  Commands run on this ${typeString} will now refer to your guild.`;
    }
    
}