
const playerUtils = require("../../utils/player.js");
const { SlashCommandSubcommandGroupBuilder } = require("@discordjs/builders");
const guildMetaFixName = require("./meta/fixName.js");
const guildMetaResetName = require("./meta/resetName.js");
const guildMetaSetLogo = require("./meta/setLogo.js");
const guildMetaRemoveLogo = require("./meta/removeLogo.js");
const guildSetRaidEstimateModifier = require("./meta/setRaidEstimateModifier.js");
const guildSetRaidDefaultEffort = require("./meta/setRaidDefaultEffort.js");

module.exports = {
    data: new SlashCommandSubcommandGroupBuilder()
        .setName("meta")
        .setDescription("Manage metadata for your guild")
        .addSubcommand(guildMetaFixName.data)
        .addSubcommand(guildMetaSetLogo.data)
        .addSubcommand(guildMetaResetName.data)
        .addSubcommand(guildMetaRemoveLogo.data)
        // .addSubcommand(guildSetRaidEstimateModifier.data)
        .addSubcommand(guildSetRaidDefaultEffort.data)
        ,
    permissionLevel: playerUtils.UserRoles.GuildAdmin,
    interact: async function(interaction)
    {
        let subcommand = interaction.options.getSubcommand();

        switch (subcommand)
        {
            case guildMetaFixName.data.name:
                await guildMetaFixName.interact(interaction);
                break;
            case guildMetaSetLogo.data.name:
                await guildMetaSetLogo.interact(interaction);
                break;
            case guildMetaResetName.data.name:
                await guildMetaResetName.interact(interaction);
                break;
            case guildMetaRemoveLogo.data.name:
                await guildMetaRemoveLogo.interact(interaction);
                break;
            case guildSetRaidEstimateModifier.data.name:
                await guildSetRaidEstimateModifier.interact(interaction);
                break;
            case guildSetRaidDefaultEffort.data.name:
                await guildSetRaidDefaultEffort.interact(interaction);
                break;
        }
    },
    autocomplete: async function(interaction)
    {
        let subcommand = interaction.options.getSubcommand();
  
        switch (subcommand)
        {
            case guildSetRaidDefaultEffort.data.name:
                await guildSetRaidDefaultEffort.autocomplete(interaction);
                break;
        }
    }
}