
const playerUtils = require("../../utils/player.js");
const { SlashCommandSubcommandGroupBuilder } = require("@discordjs/builders");
const readyJourney = require("./ready/journey.js");
const readyGL = require("./ready/gl.js");

module.exports = {
    data: new SlashCommandSubcommandGroupBuilder()
        .setName("ready")
        .setDescription("Check readiness for your guild.")
        .addSubcommand(readyGL.data)
        .addSubcommand(readyJourney.data)
        ,
    permissionLevel: playerUtils.UserRoles.User,
    interact: async function(interaction)
    {
        let subcommand = interaction.options.getSubcommand();

        switch (subcommand)
        {
            case readyGL.data.name:
                await readyGL.interact(interaction);
                break;
            case readyJourney.data.name:
                await readyJourney.interact(interaction);
                break;
        }
    },
    autocomplete: async function(interaction)
    {
        let subcommand = interaction.options.getSubcommand();
  
        switch (subcommand)
        {
            case readyGL.data.name:
                await readyGL.autocomplete(interaction);
                break;
            case readyJourney.data.name:
                await readyJourney.autocomplete(interaction);
                break;
        }
    }
}