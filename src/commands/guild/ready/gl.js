const playerUtils = require("../../../utils/player");
const readyUtils = require("../../../utils/ready");
const queueUtils = require ("../../../utils/queue");
const discordUtils = require ("../../../utils/discord");
const database = require ("../../../database");
const { SlashCommandSubcommandBuilder } = require('@discordjs/builders');


const CHECK_CHOICES = [
    { name: readyUtils.GL_CHECK_FUNCTION_MAPPING.ALL.desc, value: readyUtils.GL_CHECK_FUNCTION_MAPPING.ALL.value },
    { name: readyUtils.GL_CHECK_FUNCTION_MAPPING.AHSOKA.desc, value: readyUtils.GL_CHECK_FUNCTION_MAPPING.AHSOKA.value },
    { name: readyUtils.GL_CHECK_FUNCTION_MAPPING.LEIA.desc, value: readyUtils.GL_CHECK_FUNCTION_MAPPING.LEIA.value },
    { name: readyUtils.GL_CHECK_FUNCTION_MAPPING.JTH.desc, value: readyUtils.GL_CHECK_FUNCTION_MAPPING.JTH.value },
    { name: readyUtils.GL_CHECK_FUNCTION_MAPPING.JMK.desc, value: readyUtils.GL_CHECK_FUNCTION_MAPPING.JMK.value },
    { name: readyUtils.GL_CHECK_FUNCTION_MAPPING.JML.desc, value: readyUtils.GL_CHECK_FUNCTION_MAPPING.JML.value },
    { name: readyUtils.GL_CHECK_FUNCTION_MAPPING.LV.desc, value: readyUtils.GL_CHECK_FUNCTION_MAPPING.LV.value },
    { name: readyUtils.GL_CHECK_FUNCTION_MAPPING.Rey.desc, value: readyUtils.GL_CHECK_FUNCTION_MAPPING.Rey.value },
    { name: readyUtils.GL_CHECK_FUNCTION_MAPPING.SEE.desc, value: readyUtils.GL_CHECK_FUNCTION_MAPPING.SEE.value },
    { name: readyUtils.GL_CHECK_FUNCTION_MAPPING.SLK.desc, value: readyUtils.GL_CHECK_FUNCTION_MAPPING.SLK.value },
];

module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("gl")
        .setDescription("Check readiness for galactic legends")
        .addStringOption(o => 
            o.setName("name")
            .setDescription("The GL to check")
            .setRequired(true)
            .addChoices(...CHECK_CHOICES))
        .addStringOption(o => 
            o.setName("allycode")
            .setDescription("An allycode in a guild to check (e.g., to check another guild)")
            .setRequired(false)
            )
        .addIntegerOption(o =>
            o.setName("alt")
            .setDescription("Alt account (/user me)")
            .setRequired(false)
            .setAutocomplete(true)),
    interact: async function(interaction)
    {
        let missingPermissions = discordUtils.getMissingBotPermissions(interaction, interaction.channel);
        if (missingPermissions)
        {
            await interaction.editReply(discordUtils.convertMissingBotPermissionsArrayToResponse(missingPermissions));
            return;
        }
        
        let name = interaction.options.getString("name");
        let allycode = interaction.options.getString("allycode");
        let altNum = interaction.options.getInteger("alt");
        
        if (!allycode)
        {
            let { botUser, guildId, error } = await playerUtils.authorize(interaction);
            if (error) {
                await interaction.editReply({ embeds: [discordUtils.createErrorEmbed("Error", error)] });
                return;
            }

            let alt = await playerUtils.chooseBestAlt(botUser, guildId, altNum, true);

            if (!alt)
            {
                let errorEmbed = discordUtils.createErrorEmbed("Not in Guild", `Account is not in a guild. Choose a different alt, specify an ally code, join a guild, or register.`);
                await interaction.editReply({ embeds: [errorEmbed] });
                return;
            }

            allycode = alt.allycode;
        }
        
        allycode = playerUtils.cleanAndVerifyStringAsAllyCode(allycode);
        
        let qsm = await queueUtils.getQueueSizeMessage(queueUtils.QUEUES.READY, interaction.client.shard);
        const editMessage = await interaction.editReply(`Processing GL ready check. ${qsm}`);

        await queueUtils.queueReadyJob(interaction.client.shard,
            { 
                type: "GL",
                name: name, 
                allycode: allycode, 
                messageId: editMessage.id, 
                channelId: editMessage.channelId ?? editMessage.channel.id,
                userId: interaction.user.id
            }
        );
    },
    autocomplete: async function(interaction) {
        const focusedValue = interaction.options.getFocused();
        await interaction.respond((await playerUtils.getAltAutocompleteOptions(focusedValue, interaction.user.id)));
    }
}