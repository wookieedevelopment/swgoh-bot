const playerUtils = require("../../../utils/player");
const readyUtils = require("../../../utils/ready");
const queueUtils = require ("../../../utils/queue");
const discordUtils = require ("../../../utils/discord");
const database = require ("../../../database");
const { SlashCommandSubcommandBuilder } = require('@discordjs/builders');

const CHECK_CHOICES = [
    { name: readyUtils.JOURNEY_CHECK_FUNCTION_MAPPING.CONQUEST.desc, value: readyUtils.JOURNEY_CHECK_FUNCTION_MAPPING.CONQUEST.value },
    { name: readyUtils.JOURNEY_CHECK_FUNCTION_MAPPING.BAYLANSKOLL.desc, value: readyUtils.JOURNEY_CHECK_FUNCTION_MAPPING.BAYLANSKOLL.value },
    { name: readyUtils.JOURNEY_CHECK_FUNCTION_MAPPING.JJB.desc, value: readyUtils.JOURNEY_CHECK_FUNCTION_MAPPING.JJB.value },
    { name: readyUtils.JOURNEY_CHECK_FUNCTION_MAPPING.EPICBK.desc, value: readyUtils.JOURNEY_CHECK_FUNCTION_MAPPING.EPICBK.value },
    { name: readyUtils.JOURNEY_CHECK_FUNCTION_MAPPING.LEV.desc, value: readyUtils.JOURNEY_CHECK_FUNCTION_MAPPING.LEV.value },
    { name: readyUtils.JOURNEY_CHECK_FUNCTION_MAPPING.JKCAL.desc, value: readyUtils.JOURNEY_CHECK_FUNCTION_MAPPING.JKCAL.value },
    { name: readyUtils.JOURNEY_CHECK_FUNCTION_MAPPING.APHRA.desc, value: readyUtils.JOURNEY_CHECK_FUNCTION_MAPPING.APHRA.value },
    { name: readyUtils.JOURNEY_CHECK_FUNCTION_MAPPING.PROF.desc, value: readyUtils.JOURNEY_CHECK_FUNCTION_MAPPING.PROF.value },
    { name: readyUtils.JOURNEY_CHECK_FUNCTION_MAPPING.EXEC.desc, value: readyUtils.JOURNEY_CHECK_FUNCTION_MAPPING.EXEC.value },
    { name: readyUtils.JOURNEY_CHECK_FUNCTION_MAPPING.GI.desc, value: readyUtils.JOURNEY_CHECK_FUNCTION_MAPPING.GI.value },
    { name: readyUtils.JOURNEY_CHECK_FUNCTION_MAPPING.BAM.desc, value: readyUtils.JOURNEY_CHECK_FUNCTION_MAPPING.BAM.value },
    { name: readyUtils.JOURNEY_CHECK_FUNCTION_MAPPING.STARKILLER.desc, value: readyUtils.JOURNEY_CHECK_FUNCTION_MAPPING.STARKILLER.value },
    { name: readyUtils.JOURNEY_CHECK_FUNCTION_MAPPING.JKL.desc, value: readyUtils.JOURNEY_CHECK_FUNCTION_MAPPING.JKL.value },
    { name: readyUtils.JOURNEY_CHECK_FUNCTION_MAPPING.METACAPITALSHIPS.desc, value: readyUtils.JOURNEY_CHECK_FUNCTION_MAPPING.METACAPITALSHIPS.value },
    { name: readyUtils.JOURNEY_CHECK_FUNCTION_MAPPING.ALLCAPITALSHIPS.desc, value: readyUtils.JOURNEY_CHECK_FUNCTION_MAPPING.ALLCAPITALSHIPS.value }
];

module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("journey")
        .setDescription("Check readiness for journeys")
        .addStringOption(o => 
            o.setName("name")
            .setDescription("The journey to check")
            .setRequired(true)
            .addChoices(...CHECK_CHOICES))
        .addStringOption(o => 
            o.setName("allycode")
            .setDescription("An allycode in a guild to check (e.g., to check another guild)")
            .setRequired(false)
            )
        .addIntegerOption(o =>
            o.setName("alt")
            .setDescription("Alt account (/user me)")
            .setRequired(false)
            .setAutocomplete(true)),
    interact: async function(interaction)
    {
        let missingPermissions = discordUtils.getMissingBotPermissions(interaction, interaction.channel);
        if (missingPermissions)
        {
            await interaction.editReply(discordUtils.convertMissingBotPermissionsArrayToResponse(missingPermissions));
            return;
        }
        
        let name = interaction.options.getString("name");
        let allycode = interaction.options.getString("allycode");
        let altNum = interaction.options.getInteger("alt");
        
        if (!allycode)
        {
            let { botUser, guildId, error } = await playerUtils.authorize(interaction);
            if (error) {
                await interaction.editReply({ embeds: [discordUtils.createErrorEmbed("Error", error)] });
                return;
            }
            
            let alt = await playerUtils.chooseBestAlt(botUser, guildId, altNum, true);

            if (!alt)
            {
                let errorEmbed = discordUtils.createErrorEmbed("Not in Guild", `Account is not in a guild. Choose a different alt, specify an ally code, join a guild, or register.`);
                await interaction.editReply({ embeds: [errorEmbed] });
                return;
            }

            allycode = alt.allycode;
        }
        
        let qsm = await queueUtils.getQueueSizeMessage(queueUtils.QUEUES.READY, interaction.client.shard);
        const editMessage = await interaction.editReply(`Processing journey ready check. ${qsm}`);

        await queueUtils.queueReadyJob(interaction.client.shard, 
            {
                type: "JOURNEY",
                name: name, 
                allycode: allycode, 
                messageId: editMessage.id, 
                channelId: editMessage.channelId ?? editMessage.channel.id,
                userId: interaction.user.id 
            });
    },
    autocomplete: async function(interaction) {
        const focusedValue = interaction.options.getFocused();
        await interaction.respond((await playerUtils.getAltAutocompleteOptions(focusedValue, interaction.user.id)));
    }
}