const discordUtils = require("../../utils/discord");
const playerUtils = require("../../utils/player");
const guildUtils = require("../../utils/guild");
const database = require("../../database");
const { SlashCommandSubcommandBuilder } = require("@discordjs/builders");

module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("admin")
        .setDescription("Toggle guild admin rights for a guild member")
        .addUserOption(o => 
            o.setName("user")
            .setDescription("The user for whom to toggle admin status.")
            .setRequired(true)),
    name: "guild.admin",
    description: "Toggle guild admin rights for a guild member",
    permissionLevel: playerUtils.UserRoles.GuildAdmin,
    interact: async function(interaction) {
        let user = interaction.options.getUser("user");
        let message = await this.toggleAdmin(interaction, user.id, interaction.user.id);
        await interaction.editReply(message);
    },
    execute: async function(client, input, args) {
        
        if (!args)
        {
            await input.reply("Need to provide a discord tag.");
            return;
        }

        var userIdToMakeAdmin = discordUtils.cleanDiscordId(args[0].toLowerCase().trim());
        
        let message = await this.toggleAdmin(input, userIdToMakeAdmin, input.author.id)

        await input.reply(message);
    },
    toggleAdmin: async function(inputOrInteraction, userIdToMakeAdmin, commandSenderId)
    {
        let { botUser, guildId, error } = await playerUtils.authorize(inputOrInteraction, this.permissionLevel)
        if (error) {
            return error;
        }

        if (userIdToMakeAdmin == commandSenderId)
        {
            return "Don't touch yourself like that.";
        }

        var r = await database.db.oneOrNone("UPDATE user_guild_role SET guild_admin = NOT guild_admin WHERE discord_id = $1 AND guild_id = $2 RETURNING guild_admin", [userIdToMakeAdmin, guildId], r => r ? r.guild_admin : null);

        var message;

        if (r == null) message = "'" + userIdToMakeAdmin + "' is not a valid discord ID for a member of your guild."
        else if (r) message = "User "+ discordUtils.makeIdTaggable(userIdToMakeAdmin) + " is now an admin for your guild.";
        else message = "User "+ discordUtils.makeIdTaggable(userIdToMakeAdmin) + " is no longer an admin for your guild.";
        
        return message;
    }
}