const database = require("../../../database");

module.exports = {
    getGuildCriteria: getGuildCriteria,
    setGuildCriteria: setGuildCriteria
}

async function setGuildCriteria(comlinkGuildId, 
  {
    minGP,
    minModQuality,
    minGLCount,
    maxMissedConquestUnits,
    minTWOmiCount,
    minDC9Count,
    revaUnlocked,
    watUnlocked,
    revaReady,
    watReady,
    zeffoReady,
    mandaloreReady
  }
)
{
  const criteria = [];
  const values = [];

  if (minGP != null)
  {
    criteria.push("gp_min");
    values.push(minGP);
  }

  if (minModQuality != null)
  {
    criteria.push("modq_min");
    values.push(minModQuality);
  }

  if (minGLCount != null)
  {
    criteria.push("gl_count");
    values.push(minGLCount);
  }

  if (maxMissedConquestUnits != null)
  {
    criteria.push("missed_conquest_units_count");
    values.push(maxMissedConquestUnits);
  }

  if (minTWOmiCount != null)
  {
    criteria.push("tw_omi_count");
    values.push(minTWOmiCount);
  }

  if (minDC9Count != null)
  {
    criteria.push("dc9_count");
    values.push(minDC9Count);
  }

  if (revaUnlocked != null)
  {
    criteria.push("has_reva");
    values.push(revaUnlocked);
  }

  if (watUnlocked != null)
  {
    criteria.push("has_wat");
    values.push(watUnlocked);
  }

  if (revaReady != null)
  {
    criteria.push("reva_ready");
    values.push(revaReady);
  }

  if (watReady != null)
  {
    criteria.push("wat_ready");
    values.push(watReady);
  }

  if (mandaloreReady != null)
  {
    criteria.push("mandalore_ready");
    values.push(mandaloreReady);
  }

  if (zeffoReady != null)
  {
    criteria.push("zeffo_ready");
    values.push(zeffoReady);
  }

  if (criteria.length == 0) return;

  let updateExcludedText = criteria.map(c => `${c} = EXCLUDED.${c}`).join(",");

  criteria.push("comlink_guild_id");
  values.push(comlinkGuildId);

  let queryValuesPlaceholders = [];
  for (let i = 1; i <= values.length; i++) queryValuesPlaceholders.push(`$${i}`);
  let queryValuesPlaceholderText = queryValuesPlaceholders.join(",");

  await database.db.none(
    `INSERT INTO guild_criteria (${criteria.join(",")})
    VALUES (${queryValuesPlaceholderText})
    ON CONFLICT (comlink_guild_id) DO UPDATE
    SET ${updateExcludedText}`,
    values
  );
}

async function getGuildCriteria(comlinkGuildId)
{
    let criteria = await database.db.oneOrNone("SELECT * FROM guild_criteria WHERE comlink_guild_id = $1", [comlinkGuildId]);

    if (!criteria)
    {
      await database.db.none(`
        INSERT INTO guild_criteria (
          comlink_guild_id, 
          gp_min, 
          modq_min, 
          gl_count, 
          missed_conquest_units_count, 
          has_reva, 
          has_wat, 
          wat_ready, 
          reva_ready, 
          zeffo_ready, 
          mandalore_ready, 
          tw_omi_count, 
          dc9_count)
        VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13)
      `, [
          comlinkGuildId, 
          0, 0, 0, 0, 
          true, true, true, true, true, true,
          0, 0]);
      criteria = await database.db.oneOrNone("SELECT * FROM guild_criteria WHERE comlink_guild_id = $1", [comlinkGuildId]);
    }

    return criteria;
}
