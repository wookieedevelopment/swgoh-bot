const playerUtils = require("../../../utils/player");
const database = require("../../../database");
const criteriaCommon = require("./common");
const { SlashCommandSubcommandBuilder } = require("@discordjs/builders");


const criteriaDisplayNames = {
    gp_min: "Minimum GP",
    modq_min: "Minimum Mod Quality",
    gl_count: "Minimum GL Count",
    missed_conquest_units_count: "Minimum allowed missed conquest units",
    req_conquest_count: "Require Conquest Count",
    has_reva: "Include REVA unlock",
    has_wat: "Include WAT unlock",
    reva_ready: "Include REVA ready",
    wat_ready: "Include WAT ready",
    zeffo_ready: "Include Zeffo ready",
    mando_ready: "Include Mando ready",
    tw_omi_count: "Minimum # TW omis",
    dc9_count: "Minimum # LVL 9 DCs"
};



module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("set")
        .setDescription("Set your guild's recruit criteria")
        .addIntegerOption(o => 
            o.setName("min-gp")
            .setDescription("Minimum GP")
            .setMinValue(0)
            .setRequired(false)
        )
        .addNumberOption(o => 
            o.setName("min-mod-quality")
            .setDescription("Minimum Mod Quality (WookieeBot Style)")
            .setMinValue(0)
            .setRequired(false)
        )
        .addIntegerOption(o => 
            o.setName("min-gl-count")
            .setDescription("Minimum Galactic Legends Count")
            .setMinValue(0)
            .setMaxValue(100)
            .setRequired(false)
        )
        .addIntegerOption(o => 
            o.setName("max-missed-conquest-units")
            .setDescription("Maximum Missed Conquest Units Count")
            .setMinValue(0)
            .setMaxValue(100)
            .setRequired(false)
        )
        .addIntegerOption(o => 
            o.setName("min-tw-omi-count")
            .setDescription("Minimum TW Omicrons Count")
            .setMinValue(0)
            .setMaxValue(500)
            .setRequired(false)
        )
        .addIntegerOption(o => 
            o.setName("min-dc9-count")
            .setDescription("Minimum L9 Datacrons Count")
            .setMinValue(0)
            .setMaxValue(500)
            .setRequired(false)
        )
        .addBooleanOption(o => 
            o.setName("reva-unlocked")
            .setDescription("Reva Unlocked")
            .setRequired(false)
        )
        .addBooleanOption(o => 
            o.setName("wat-unlocked")
            .setDescription("Wat Unlocked")
            .setRequired(false)
        )
        .addBooleanOption(o => 
            o.setName("wat-ready")
            .setDescription("Ready for Wat Mission")
            .setRequired(false)
        )
        .addBooleanOption(o => 
            o.setName("reva-ready")
            .setDescription("Ready for Reva Mission")
            .setRequired(false)
        )
        .addBooleanOption(o => 
            o.setName("zeffo-ready")
            .setDescription("Ready for Zeffo Unlock")
            .setRequired(false)
        )
        .addBooleanOption(o => 
            o.setName("mandalore-ready")
            .setDescription("Ready for Mandalore Unlock")
            .setRequired(false)
        ),
    permissionLevel: playerUtils.UserRoles.GuildAdmin,
    interact: async function(interaction) {
        let { botUser, guildId, error } = await playerUtils.authorize(interaction, this.permissionLevel);
        if (error) {
            await interaction.editReply(error);
            return;
        }

        const guild = botUser.guilds.find(g => g.guildId === guildId);
        const comlinkGuildId = guild.comlinkGuildId;

        const minGP = interaction.options.getInteger("min-gp");
        const minModQuality = interaction.options.getNumber("min-mod-quality");
        const minGLCount = interaction.options.getInteger("min-gl-count");
        const maxMissedConquestUnits = interaction.options.getInteger("max-missed-conquest-units");
        const minTWOmiCount = interaction.options.getInteger("min-tw-omi-count");
        const minDC9Count = interaction.options.getInteger("min-dc9-count");
        const revaUnlocked = interaction.options.getBoolean("reva-unlocked");
        const watUnlocked = interaction.options.getBoolean("wat-unlocked");
        const revaReady = interaction.options.getBoolean("reva-ready");
        const watReady = interaction.options.getBoolean("wat-ready");
        const zeffoReady = interaction.options.getBoolean("zeffo-ready");
        const mandaloreReady = interaction.options.getBoolean("mandalore-ready");

        await criteriaCommon.setGuildCriteria(comlinkGuildId,
            { 
                minGP: minGP,
                mandaloreReady: mandaloreReady,
                maxMissedConquestUnits: maxMissedConquestUnits,
                minDC9Count: minDC9Count,
                minGLCount: minGLCount,
                minModQuality: minModQuality,
                minTWOmiCount: minTWOmiCount,
                revaReady: revaReady,
                revaUnlocked: revaUnlocked,
                watReady: watReady,
                watUnlocked: watUnlocked,
                zeffoReady: zeffoReady
            }
        );

        const outputTexts = [];

        if (minGP != null) outputTexts.push(` - Minimum GP set to ${minGP.toLocaleString('en')}`);
        if (minModQuality != null) outputTexts.push(` - Minimum Mod Quality (WookieeBot Style) set to ${minModQuality.toLocaleString('en')}`);
        if (minGLCount != null) outputTexts.push(` - Minimum Galactic Legends Count set to ${minGLCount.toLocaleString('en')}`);
        if (maxMissedConquestUnits != null) outputTexts.push(` - Maximum Missed Conquest Units set to ${maxMissedConquestUnits.toLocaleString('en')}`);
        if (minTWOmiCount != null) outputTexts.push(` - Minimum TW Omicron Count set to ${minTWOmiCount.toLocaleString('en')}`);
        if (minDC9Count != null) outputTexts.push(` - Minimum Level 9 Datacrons Count set to ${minDC9Count.toLocaleString('en')}`);
        if (watUnlocked != null) outputTexts.push(` - Wat Unlocked set to ${watUnlocked}`);
        if (revaUnlocked != null) outputTexts.push(` - Reva Unlocked set to ${revaUnlocked}`);
        if (watReady != null) outputTexts.push(` - Wat Readiness Requiredset to ${watReady}`);
        if (revaReady != null) outputTexts.push(` - Reva Readiness Requiredset to ${revaReady}`);
        if (zeffoReady != null) outputTexts.push(` - Zeffo Readiness Required set to ${zeffoReady}`);
        if (mandaloreReady != null) outputTexts.push(` - Mandalore Readiness Required set to ${mandaloreReady}`);

        let responseText = outputTexts.join("\n");

        await interaction.editReply(`Guild Criteria Updated:\n${responseText}\n`);
    },
};
