const discordUtils = require("../../utils/discord");
const playerUtils = require("../../utils/player");
const database = require("../../database");
const guildUtils = require("../../utils/guild")
const got = require('got');
const parse = require("csv-parse/lib/sync");
const registerBulkUsers = require("./users/registerBulkUsers")
const registerOneUser = require("./users/registerOneUser")
const { SlashCommandSubcommandGroupBuilder } = require("@discordjs/builders");

module.exports = {
    data: new SlashCommandSubcommandGroupBuilder()
        .setName("users")
        .setDescription("Register users to the guild.")
        .addSubcommand(registerBulkUsers.data)
        .addSubcommand(registerOneUser.data),
    permissionLevel: playerUtils.UserRoles.GuildAdmin,
    interact: async function(interaction)
    {  
        let subcommand = interaction.options.getSubcommand();

        switch (subcommand)
        {
            case registerBulkUsers.data.name:
                await registerBulkUsers.interact(interaction);
                break;
            case registerOneUser.data.name:
                await registerOneUser.interact(interaction);
                break;                                    
        }
    }
}