
const playerUtils = require("../../utils/player.js");
const cacheList = require("./cache/cacheList")
const { SlashCommandSubcommandGroupBuilder } = require("@discordjs/builders");
const cacheOne = require("./cache/cacheOne.js");
const cacheAll = require("./cache/cacheAll")

module.exports = {
    data: new SlashCommandSubcommandGroupBuilder()
        .setName("cache")
        .setDescription("Manage cached data for your guild.")
        .addSubcommand(cacheList.data)
        .addSubcommand(cacheOne.data)
        .addSubcommand(cacheAll.data)
        ,
    types: {
        all: 1,
        rostersOnly: 2,
        ultsOnly: 3,
        one: 4
    },
    permissionLevel: playerUtils.UserRoles.GuildAdmin,
    interact: async function(interaction)
    {
        let subcommand = interaction.options.getSubcommand();

        switch (subcommand)
        {
            case cacheList.data.name:
                await cacheList.interact(interaction);
                break;
            case cacheOne.data.name:
                await cacheOne.interact(interaction);
                break;
            case cacheAll.data.name:
                await cacheAll.interact(interaction);
                break;
        }
    },
    execute: async function(client, input, type, args) {
        await input.channel.send("Please use the slash command (**/guild cache**) instead.");
        return;
    }
}