const playerUtils = require("../../utils/player.js");
const discordUtils = require("../../utils/discord.js");
const database = require("../../database");
const guildUtils = require("../../utils/guild.js");
const { SlashCommandSubcommandBuilder } = require("@discordjs/builders");
const constants = require("../../utils/constants.js");
const { EmbedBuilder } = require("discord.js");

module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("show-links")
        .setDescription("Show WookieeBot links to Discord")
        .addIntegerOption(o =>
            o.setName("alt")
            .setDescription("Alt account (/user me)")
            .setRequired(false)
            .setAutocomplete(true)),
    interact: async function(interaction) {
        
        let { botUser, guildId, error } = await playerUtils.authorize(interaction);
        let alt = interaction.options.getInteger("alt");

        const allycode = playerUtils.chooseBestAllycode(botUser, guildId, null, alt);

        const links = await getLinks(allycode);

        const response = generateResponse(allycode, links);

        await interaction.editReply(response);
    },
    autocomplete: async function(interaction) {
        const focusedValue = interaction.options.getFocused();
        await interaction.respond((await playerUtils.getAltAutocompleteOptions(focusedValue, interaction.user.id)));
    },
}


async function getLinks(allycode)
{
    const links = await database.db.any(`
        SELECT guild_name,discord_link,link_type
        FROM guild_players
        LEFT JOIN guild ON (guild.guild_id = guild_players.guild_id)
        LEFT JOIN guild_discord_link ON (guild_discord_link.guild_id = guild_players.guild_id)
        WHERE allycode = $1`, [allycode]);

    return links;
}

function generateResponse(allycode, links)
{
    let response = { content: null };
    if (!links || links.length === 0)
    {
        response.content = "WookieeBot is not linked to Discord for your guild.";
        return response;
    }

    if (links[0].guild_name == null)
    {
        response.content = `WookieeBot does not have you (Ally Code ${discordUtils.addDashesToAllycode(allycode)}) as a member of a guild.\n\nIf you are not registered, run \`/register allycode:${allycode}\` to register.\n\nIf you are registered and a member of a guild, ask your guild's WookieeBot administrator to run \`/guild refresh roster\`.`;
        return response;
    }

    let channelLinks = links.filter(l => l.link_type == constants.DISCORD_LINK_TYPES.channel);
    let serverLinks = links.filter(l => l.link_type == constants.DISCORD_LINK_TYPES.server);
    let guildName = links[0].guild_name;

    let channelLinksText, serverLinksText;

    if (serverLinks.length === 0)
    {
        serverLinksText = "No server links."
    }
    else
    {
        serverLinksText = `Server Links:\n` + serverLinks.map(l => { 
            return `- ${discordUtils.makeChannelTaggable(l.discord_link)}`;
        }).join("\n");
    }

    if (channelLinks.length === 0)
    {
        channelLinksText = "No channel links."
    }
    else
    {
        channelLinksText = `Channel Links:\n` + channelLinks.map(l => { 
            return `- ${discordUtils.makeChannelTaggable(l.discord_link)}`;
        }).join("\n");
    }

    response.content = 
`**Discord Links: ${guildName}**

${serverLinksText}

${channelLinksText}
`;

    return response;
}