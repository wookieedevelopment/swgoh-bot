
const { SlashCommandSubcommandBuilder } = require("@discordjs/builders");
const { EmbedBuilder, ActionRowBuilder, StringSelectMenuBuilder, ButtonBuilder, ButtonStyle } = require("discord.js");
const comlink = require("../../utils/comlink.js");
const guildUtils = require("../../utils/guild.js");
const swapiUtils = require("../../utils/swapi.js");
const discordUtils = require("../../utils/discord.js");
const cheatingGuild = require("../cheating/guild.js");
const scoutGuild = require("../scout/scoutGuild.js");
const database = require("../../database.js");

const MAX_GUILDS_TO_FIND = 25;
module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("search")
        .setDescription("Search for guilds by name.")
        .addStringOption(o =>
            o.setName("text")
            .setDescription("Text to search for in guild names")
            .setRequired(false)
            .setMaxLength(30)
            .setMinLength(1))
        .addIntegerOption(o =>
            o.setName("allycode")
            .setDescription("Ally Code in the guild")
            .setRequired(false)
            .setMinValue(100000000)
            .setMaxValue(999999999)),
    interact: async function(interaction)
    {
        let searchString = interaction.options.getString("text") ?? null;
        let allycode = interaction.options.getInteger("allycode") ?? null;

        if (!searchString && !allycode)
        {
            await interaction.editReply("You must provide either search text or an Ally Code.");
            return;
        }

        let response;

        if (searchString)
        {
            let guilds = await comlink.searchGuilds(searchString, 0, MAX_GUILDS_TO_FIND);

            let embed = new EmbedBuilder()
                .setTitle(`Guilds matching "${searchString}"`)
                .setColor(0x555500);
            
            if (!guilds || guilds.guild.length === 0)
            {
                embed.setDescription("No guilds found.")
                    .setColor(0x550000);
                await interaction.followUp({ embeds: [embed] });
                return;
            }

            guilds.guild.sort((a, b) => {
                if (a.memberCount != b.memberCount) return b.memberCount - a.memberCount;
                return parseInt(b.guildGalacticPower) - parseInt(a.guildGalacticPower);
            });
            
            let matchingGuilds = guilds.guild.map(g => { return {
                label: g.name,
                value: g.id,
                description: `${g.memberCount} member${g.memberCount === 1 ? "" : "s"}. GP: ${parseInt(g.guildGalacticPower).toLocaleString("en-US")}.`
            }});

            if (matchingGuilds.length === MAX_GUILDS_TO_FIND)
                embed.setDescription(`Found at least ${MAX_GUILDS_TO_FIND} guilds. Displaying only the first ${MAX_GUILDS_TO_FIND}. To see others, please provide a more precise name.`);
            else
                embed.setDescription(`Found ${matchingGuilds.length} guilds matching the search text.`);
            
                
            let componentsRow = new ActionRowBuilder()
            .addComponents(
                new StringSelectMenuBuilder()
                    .setCustomId(`guild:search`)
                    .setPlaceholder("Choose a guild for details")
                    .addOptions(matchingGuilds)
            );

            response = {
                embeds: [embed],
                components: [componentsRow]
            }
        } 
        else
        {
            let player = await comlink.getPlayer(allycode.toString());
            if (!player) {
                await interaction.editReply("No player was found with that Ally Code.");
                return;
            }
            response = await getGuildOverviewResponse(swapiUtils.getPlayerGuildId(player));
        }

        await interaction.editReply(response);
    },
    onSelect: async function(interaction)
    {
        //let originalMessage = interaction.message.content;
        await interaction.deferReply();

        let guildId = interaction.values[0];

        let response = await getGuildOverviewResponse(guildId);

        await interaction.followUp(response);
    },
    handleButton: async function(interaction)
    {
        if (interaction.customId.startsWith("guild:search:cheat:"))
        {        
            await interaction.deferReply();
            await handleCheatButton(interaction);
        } 
        else if (interaction.customId.startsWith("guild:search:mem:"))
        {
            await interaction.deferUpdate();
            await handleShowMembersButton(interaction);
        }
        else if (interaction.customId.startsWith("guild:search:view:"))
        {
            await interaction.deferUpdate();
            await handleShowOverviewButton(interaction);
        }
        else if (interaction.customId.startsWith("guild:search:scout:"))
        {
            await interaction.deferReply();
            await handleScoutButton(interaction);
        }
    }
}

async function handleShowOverviewButton(interaction)
{
    let regex = /guild:search:view:(.+)/;
    let matches = regex.exec(interaction.customId);
    if (!matches || matches.length != 2) {
        // something went wrong
        logger.error("guild:search:view:handleButton interaction.customId doesn't match regex. customId is: " + interaction.customId);
        await interaction.followUp({ content: "Something went wrong, unfortunately. Please contact the developer." });
        return;
    }
    let guildId = matches[1];

    let response = await getGuildOverviewResponse(guildId);

    await interaction.editReply(response);
}

async function getGuildOverviewResponse(guildId)
{
    let guild = await guildUtils.getGuildDataFromAPI({ comlinkGuildId: guildId }, { useCache: true, cacheTimeHours: 4, includeRecentGuildActivityInfo: true });

    let twResults;
    if (guild.recentTerritoryWarResult && guild.recentTerritoryWarResult.length > 0)
    {
        guild.recentTerritoryWarResult.sort((a, b) => (parseInt(b.endTimeSeconds) - parseInt(a.endTimeSeconds)));

        let having = guild.recentTerritoryWarResult.map(r => `sum(case when score = ${r.opponentScore} and opponent_score = ${r.score} and ROUND(end_time_seconds/1000) = ROUND(${r.endTimeSeconds}/1000) then 1 else 0 end) > 0`).join(" or ")
        let warMatches = await database.db.any(
            `select war_result_id,guild_name,end_time_seconds
            from war_result
            group by war_result_id
            having ${having};
            `);
        
        twResults = `\`\`\`diff
${guild.recentTerritoryWarResult?.map(r => 
    {
        let opName = warMatches.find(m => Math.floor(m.end_time_seconds/1000) === Math.floor(r.endTimeSeconds/1000))?.guild_name ?? "Unknown";
        let opScore = parseInt(r.opponentScore);
        let score = parseInt(r.score);
        let warDate = new Date(parseInt(r.endTimeSeconds * 1000));
        return `${score > opScore ? "+ W" : "- L"} vs. ${opName}\n${score > opScore ? "+ " : "- "}${warDate.toLocaleDateString("en")} | ${Math.round(r.power / 1000000)}M | ${score} - ${opScore}`;

//        return `${score > opScore ? "+W" : "-L"} | ${discordUtils.fit(warDate.toLocaleDateString("en"), 10)} | ${Math.round(r.power / 1000000)}M | ${discordUtils.fit("Unknown", 14)} | ${score}-${opScore}`
    }).join("\n\n")}
\`\`\``
    } else {
        twResults = "N/A";
    }
    
    let topGACPlayers = [...new Set(guildUtils.getGuildRoster(guild).filter(r => swapiUtils.getPlayerSkillRating(r) != null))].sort((a, b) => (swapiUtils.getPlayerSkillRating(b) ?? 0) - (swapiUtils.getPlayerSkillRating(a) ?? 0)).slice(0, 10);
    let averageSR = Math.round(guildUtils.getGuildRoster(guild).reduce((p, c, i) => p+(swapiUtils.getPlayerSkillRating(c) ?? 0), 0) / guildUtils.getGuildMembersCount(guild));

    let topGACPlayersText;

    if (topGACPlayers.length > 0)
    {
        topGACPlayersText = topGACPlayers.map((p) => `${discordUtils.fit(swapiUtils.getPlayerName(p), 21)}${discordUtils.LTR} | ${swapiUtils.getPlayerAllycode(p)} | ${swapiUtils.getPlayerSkillRating(p) ?? "N/A"}`).join("\n")

        topGACPlayersText = 
`\`\`\`
Top GAC Player        | Ally Code | Rating 
----------------------|-----------|--------
${topGACPlayersText}
\`\`\``
    } else {
        topGACPlayersText = "N/A";
    }

    let leadership = guildUtils.getGuildRoster(guild).filter(r => swapiUtils.getPlayerGuildMemberLevel(r) > 2);
    
    leadership.sort((a, b) => 
    {
        return swapiUtils.getPlayerGuildMemberLevel(b) - swapiUtils.getPlayerGuildMemberLevel(a) ||
        swapiUtils.getPlayerName(a).localeCompare(swapiUtils.getPlayerName(b));
    });

    let leadershipText = 
`\`\`\`
Name               | Ally Code | Role 
-------------------|-----------|------
${leadership.map(l => {
    
    let role;
    
    switch (swapiUtils.getPlayerGuildMemberLevel(l))
    {
        case 4: role = "Lead"; break;
        case 3: role = "Off."; break;
        default: role = "Mem."; break;
    }
    
    return `${discordUtils.fit(swapiUtils.getPlayerName(l), 18)}${discordUtils.LTR} | ${swapiUtils.getPlayerAllycode(l)} | ${role}`;
}).join("\n")}
\`\`\``

    let averageGP = Math.round(parseInt(guild.profile.guildGalacticPower) / guildUtils.getGuildMembersCount(guild));

    let lastRaid, lastRaidScore, lastRaidCampaignNodeId;
    if (guild.recentRaidResult && guild.recentRaidResult.length > 0)
    {
        guild.recentRaidResult.sort((a, b) => { 
            let endTimeA = parseInt(a.endTime);
            let endTimeB = parseInt(b.endTime);
            return endTimeB - endTimeA;
        });

        lastRaid = guild.recentRaidResult[0];
        lastRaidScore = parseInt(lastRaid.guildRewardScore);
        lastRaidCampaignNodeId = lastRaid.identifier.campaignNodeId.toUpperCase();
    }

    let swgohggUrl = `https://swgoh.gg/g/${guildId}`;    
    let embed = new EmbedBuilder()
        .setTitle(`Guild Info - ${guildUtils.getGuildName(guild)}`)
        .setDescription(
`**Name:** ${guildUtils.getGuildName(guild)}
**Description:** ${guildUtils.getGuildDescription(guild)}
**Members:** ${guildUtils.getGuildMembersCount(guild)}
**Total GP:** ${parseInt(guild.profile.guildGalacticPower).toLocaleString("en")}
**Average GP:** ${averageGP.toLocaleString("en")}
**Daily Reset:** ${new Date(parseInt(guild.nextChallengesRefresh)*1000).toLocaleTimeString("en", { timeZone: "UTC" } )} UTC
**Average Skill Rating:** ${averageSR}

**Leadership:** ${leadershipText}
**Recent TW Results:** ${twResults}
**Last Raid:** ${(!lastRaid) ? "N/A" : (`\`\`\`js\n${guildUtils.RAID_NAME_MAPPING[lastRaidCampaignNodeId]} [${lastRaidScore.toLocaleString("en")}]\n\`\`\``)}
**Top GAC Players:** ${topGACPlayersText}

${swgohggUrl}
Note: If the above link does not work, the guild has not been synced on swgoh.gg.
`
        );

    let buttonRow1 = new ActionRowBuilder()
        .addComponents(
            new ButtonBuilder()
                .setCustomId(guildUtils.generateGuildMembersSortCustomId(`guild:search:mem`, guildId, guildUtils.GUILD_MEMBERS_SORT_OPTIONS.Rank, false))
                .setLabel("Show All Members")
                .setStyle(ButtonStyle.Primary),
            new ButtonBuilder()
                .setCustomId(`guild:search:scout:${guildId}`)
                .setLabel("TW Scout")
                .setStyle(ButtonStyle.Success),
            new ButtonBuilder()
                .setCustomId(`guild:search:cheat:${guildId}`)
                .setLabel("Run Cheat Check")
                .setStyle(ButtonStyle.Danger)
        );

    let response = { embeds: [embed], components: [buttonRow1] };

    return response;
}

async function handleCheatButton(interaction)
{
    let regex = /guild:search:cheat:(.+)/;
    let matches = regex.exec(interaction.customId);
    if (!matches || matches.length != 2) {
        // something went wrong
        logger.error("guild:search:cheat:handleButton interaction.customId doesn't match regex. customId is: " + interaction.customId);
        await interaction.editReply({ content: "Something went wrong, unfortunately. Please contact the developer." });
        return;
    }

    let guildId = matches[1];
    let guild = await guildUtils.getGuildDataFromAPI({ comlinkGuildId: guildId }, { useCache: true, cacheTimeHours: 4, includeRecentGuildActivityInfo: true });

    let statusInteraction = await interaction.followUp(`Checking ${guildUtils.getGuildRoster(guild).length} players in guild **${guildUtils.getGuildName(guild)}**. Please wait...`);

    let response = await cheatingGuild.checkGuild(
        guild,
        async (statusMessage) => { await statusInteraction.edit(statusMessage); });

    await statusInteraction.edit(response);
}

async function handleScoutButton(interaction)
{
    let regex = /guild:search:scout:(.+)/;
    let matches = regex.exec(interaction.customId);
    if (!matches || matches.length != 2) {
        // something went wrong
        logger.error("guild:search:scout:handleButton interaction.customId doesn't match regex. customId is: " + interaction.customId);
        await interaction.editReply({ content: "Something went wrong, unfortunately. Please contact the developer." });
        return;
    }

    let guildId = matches[1];

    let guild = await guildUtils.getGuildDataFromAPI({ comlinkGuildId: guildId }, { useCache: true, cacheTimeHours: 4, includeRecentGuildActivityInfo: true });

    let statusInteraction = await interaction.followUp(`Scouting **${guildUtils.getGuildName(guild)}**. Please wait...`);

    let response = await scoutGuild.scout(swapiUtils.getPlayerAllycode(guildUtils.getGuildRoster(guild)[0]), 
        async (statusMessage) => { await statusInteraction.edit(statusMessage); })


    await statusInteraction.edit(response);
}

async function handleShowMembersButton(interaction)
{
    let { prefix, guildId, by, asc } = guildUtils.parseGuildMembersSortCustomId(interaction.customId);

    let response = await guildUtils.createGuildMembersResponse(guildId, prefix, by, asc);

    let overviewRow = new ActionRowBuilder()
        .addComponents(
            new ButtonBuilder()
                .setLabel("Overview")
                .setStyle(ButtonStyle.Secondary)
                .setCustomId(`guild:search:view:${guildId}`)
        );

    response.components.push(overviewRow);
        
    await interaction.editReply(response);

}