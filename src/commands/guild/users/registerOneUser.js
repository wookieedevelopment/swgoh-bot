const discordUtils = require("../../../utils/discord");
const playerUtils = require("../../../utils/player");
const database = require("../../../database");
const { SlashCommandSubcommandBuilder } = require("@discordjs/builders");

module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("register")
        .setDescription("Register a discord user to an ally code in your guild.")
        .addIntegerOption(o => 
            o.setName("allycode")
            .setDescription("The ally code of your guild member, including alts.")
            .setRequired(true))
        .addUserOption(o =>
            o.setName("user")
            .setDescription("The user to register to the ally code.")
            .setRequired(true)),
    permissionLevel: playerUtils.UserRoles.GuildAdmin,
    interact: async function(interaction)
    {
        let allycode = interaction.options.getInteger("allycode");

        try { allycode = playerUtils.cleanAndVerifyStringAsAllyCode(allycode.toString()); }
        catch (err) { await interaction.editReply({ embeds: [discordUtils.createErrorEmbed("Rwww. ('No.').", err.message)] }); return; }

        let user = interaction.options.getUser("user");
        let { botUser, guildId, error } = await playerUtils.authorize(interaction, this.permissionLevel)
        if (error) {
            await interaction.editReply(error);
            return;
        }

        let response = await this.doRegistration(user.id, allycode, guildId);
        await interaction.editReply(response);
    },
    doRegistration: async function(userIdToRegister, allycode, guildId)
    {
        // get guild id for the allycode to register
        var guildPlayerInfo = await database.db.oneOrNone("SELECT guild_id FROM guild_players WHERE allycode = $1 AND guild_id=$2", [allycode, guildId]);

        // player not part of guild
        if (!guildPlayerInfo)
        {
            return "Ally code \`" + allycode + "\` is not confirmed as part of your guild. Please double check the ally code. If it is correct, either your guild is not registered (contact the bot owner) or run \`/guild refresh roster\` and try again.";
        }

        if (await this.registerUserAndSetDefaultRolesForGuild(userIdToRegister, allycode, guildPlayerInfo.guild_id))
            return "Success! Ally code \`" + allycode + "\` has been registered to " + discordUtils.makeIdTaggable(userIdToRegister) + " and assigned to your guild.";
        else
            return `\`${allycode}\` was already registered to ${discordUtils.makeIdTaggable(userIdToRegister)}`;
        
    },
    registerUserAndSetDefaultRolesForGuild: async function(userId, allycode, guildId)
    {
        const existingUser = await playerUtils.getUser(userId);

        var addRole = false, addReg = false;

        if (!existingUser) {
            // user has never been registered before. assign them to the guild and allycode
            addRole = true;
            addReg = true;
        } 
        else 
        {
            if (!existingUser.guilds.find(g => g.guildId == guildId)) {
                // user wasn't assigned as part of the guild, so give them the default roles
                addRole = true;
            }  

            if (!existingUser.allycodes.find(a => a == allycode))
            {
                // user exists, but they do not have that ally code associated
                addReg = true;
            }
        }

        if (addRole) {
            try {
                await database.db.none("INSERT INTO user_guild_role VALUES ($1, $2, $3, $4)", [userId, guildId, false, false]);
            } catch { addRole = false; }
        }

        if (addReg)
        {
            let newAltNumber = 0;
            let altData = await database.db.oneOrNone("SELECT MAX(alt) FROM user_registration WHERE discord_id = $1", [userId]);
            if (altData != null) newAltNumber = altData.max + 1;

            // only one discord user per ally code
            await database.db.none("INSERT INTO user_registration (allycode, discord_id, alt) VALUES ($1, $2, $3) ON CONFLICT(allycode) DO UPDATE SET discord_id = $2, alt = $3", [allycode, userId, newAltNumber]);
        }

        if (addRole || addReg) return true;
        return false;
    }
}