const playerUtils = require("../../../utils/player");
const database = require("../../../database");
const got = require('got');
const parse = require("csv-parse/lib/sync");
const discordUtils = require("../../../utils/discord");
const { SlashCommandSubcommandBuilder, EmbedBuilder } = require("@discordjs/builders");
const { AttachmentBuilder  } = require('discord.js');

function isValidAllyCode(allycode) {
    // Implement your validation logic here
    if (!/^[1-9]{9}$/.test(allycode)) {
        return false;
    }
    // Ensure the Discord ID is exactly 18 or 19 characters
    if (allycode.length === 9 ) {
        return true;
    }
    return false;
}

function isValidDiscordId(discordId) {
    if (!/^\d{17,19}$/.test(discordId)) {
        return false;
    }
    // If length is 18 or 19 and ends with 000 flag as invalid
    if (discordId.endsWith("000")) {
        return false;
    }

    return true;
}

const userRegistrationCS = new database.pgp.helpers.ColumnSet(
    ['allycode', 
    'discord_id',
    'alt'
    ], 
    {table: 'user_registration'});

module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("register-bulk")
        .setDescription("Register users in bulk via .csv spreadsheet")
        .addAttachmentOption(o =>
            o.setName("file")
            .setDescription("CSV with two columns: AllyCode and DiscordId")
            .setRequired(true)),
    permissionLevel: playerUtils.UserRoles.GuildAdmin,
    interact: async function(interaction)
    {
        let { botUser, guildId, error } = await playerUtils.authorize(interaction, this.permissionLevel)
        if (error) {
            await interaction.editReply(error);
            return;
        }
        let file = interaction.options.getAttachment("file");
        
        let response = await this.registerUsers(file, guildId, botUser, interaction.user.id);
        await interaction.followUp(response);
    },
    registerUsers: async function(attachment, guildId, botUser, callerDiscordId) {
        if (guildId == null) return { content: "Run this from a channel which is linked to your guild. Link with `/guild link`" };
        if (!attachment)   
        {
            return { content: "You must attach a CSV that has two columns: AllyCode, DiscordId. DiscordId can be found by enabling Developer Mode in Discord, right clicking on a user, and clicking Copy ID. You can build this yourself or download from the HotUtils website. Go to Guild->Overview, click Download Data, and then Download Guild Data.\n\nPlease try again." };
        }
        
        if (attachment.size >= 1024*10) // 10KB limit, maybe?
        {
            return { content: "This file seems too big to be the guild registration data. You must attach a CSV with two columns: AllyCode, DiscordId. DiscordId can be found by enabling Developer Mode in Discord, right clicking on a user, and clicking Copy ID. You can build this yourself or download from the HotUtils website. Go to Guild->Overview, click Download Data, and then Download Guild Data.\n\nPlease try again." };
        }
        
        let ssUrl = attachment.url;
        let buffer = await got(ssUrl, { timeout: 50000 });

        // Determine the delimiter used in the CSV
        const delimiterOptions = [',', ';', '\t', '|', ' | ', ' , ', ' ; ', ' '];

        const fileContent = buffer.body.toString(); // Convert the buffer to a string
                
        const records = parse(fileContent, { columns: true, delimiter: delimiterOptions });

        if (records.length === 0)
        {
            return { content: "No registration information is in file you uploaded." };
        }

        if (((records[0]["﻿AllyCode"] === null) && (records[0]["AllyCode"] === null)) // the weird symbol is in case HU screws up their encoding again
             || records[0]["DiscordId"] === null)
        {
            return { content: `A required column header is missing from the CSV. Ensure the first row is: "AllyCode,DiscordId".` };
        }        
                
        // only register players that are in the guild of the user and aren't already registered to prevent hacking and shit
        let guildPlayers = await database.db.any(
            `SELECT allycode,discord_id,member_type
            FROM guild_players 
            LEFT JOIN user_registration USING (allycode)
            WHERE guild_id = $1`, [guildId]);

        let files = [];
        let invalidEntries = [];
        let skippedEntries = [];
        let userRegistrationInsertValues = new Array();
        let userGuildRoleInsertValues = new Array();

        for (let r of records)
        {
            let allycode = r["﻿AllyCode"] ?? r["AllyCode"];
            let discordId = r["DiscordId"];
            
            let validatedDiscordId = isValidDiscordId(discordId);
            let validatedAllyCode = isValidAllyCode(allycode);

            let invalidReason = "";

            if (!validatedDiscordId)
            {
                invalidReason += "Invalid DiscordId format: "
                if (discordId?.endsWith("000"))
                {
                    invalidReason += `Check that it does not end with 000.  `
                } else {
                    invalidReason += `Check that it is 17-19 digits.  ` 
                }
            }

            if (!validatedAllyCode)
            {
                invalidReason += "Invalid AllyCode."
            }

            if (botUser.allycodes.indexOf(allycode) >= 0)
            {
                if (discordId != callerDiscordId)
                {
                    invalidReason += "You cannot change your own registration this way."
                }
                else
                {
                    skippedEntries.push( { allycode, discordId });
                    continue;
                }
            }

            if (userRegistrationInsertValues.find(v => v.allycode === allycode))
            {
                invalidReason += "Ally Code is in the file more than once.";
            }

            if (invalidReason.length > 0)
            {
                invalidEntries.push( { allycode, discordId, invalidReason });
                continue;
            }
        
            // at this point, allycode and discordId are valid

            let player = guildPlayers.find((p) => p.allycode == allycode);
            if (!player) 
            {
                invalidReason += "Ally Code is not known to WookieeBot to be in your guild. Run `/guild refresh roster` and/or verify Ally Code."
                invalidEntries.push( { allycode, discordId, invalidReason });
                continue;
            }

            // don't overwrite registration for members that already have valid registration
            // guilds will need to do this manually
            if (player.discord_id && player.discord_id != discordId && isValidDiscordId(player.discord_id))
            {
                invalidReason += `Already registered to ${discordUtils.makeIdTaggable(player.discord_id)}. The player can use \`/register\` or you can use \`/guild users register\`.`;
                invalidEntries.push( { allycode, discordId, invalidReason });
            }
            else
            {
                // new alt number should be the highest alt # in the database plus the count of registrations 
                // in the same dataset to the same player
                let newAltNumber = userRegistrationInsertValues.filter(v => v.discord_id === discordId).length; // starts at 0
                let maxPriorAlt = await database.db.result("SELECT MAX(alt) FROM user_registration WHERE discord_id = $1", [discordId], r => r.max);
                if (maxPriorAlt != null) newAltNumber += (maxPriorAlt + 1);

                // player hasn't been registered, so add them to the list to be registered
                userRegistrationInsertValues.push({
                    allycode: allycode,
                    discord_id: discordId,
                    alt: newAltNumber
                });
            }

            // every player needs their roles validated in case a player's registration was corrupted
            let isOfficerOrLeader = player.member_type === "LEADER" || player.member_type === "OFFICER";
            userGuildRoleInsertValues.push({
                discord_id: discordId,  
                guild_id: guildId,
                guild_admin: isOfficerOrLeader,
                guild_bot_admin: isOfficerOrLeader
            });
        };

        if (userRegistrationInsertValues.length == 0 && userGuildRoleInsertValues.length == 0 && invalidEntries.length == 0)
        {
            return { content: `All users in the attached file that are known by WookieeBot to be in your guild have been registered. If your guild roster recently changed, try running \`/guild refresh roster\` first.` };
        }

        let response = "";
        if (userRegistrationInsertValues.length >= 1) {
            const userRegistrationInsert = database.pgp.helpers.insert(userRegistrationInsertValues, userRegistrationCS);
            await database.db.none(userRegistrationInsert + " ON CONFLICT (allycode) DO UPDATE SET discord_id = EXCLUDED.discord_id, alt = EXCLUDED.alt");

            response += `Registered Players: ${userRegistrationInsertValues.length}\n`;
        }

        if (userGuildRoleInsertValues.length >= 1)
        {
            const userGuildRoleInsert = database.pgp.helpers.insert(userGuildRoleInsertValues, database.columnSets.userGuildRoleCS);
            await database.db.none(userGuildRoleInsert + " ON CONFLICT DO NOTHING");
            response += `Validated Roles: ${userGuildRoleInsertValues.length}\n`;
        }
        
        if (skippedEntries.length > 0)
        {
            response += `Skipped: ${skippedEntries.length}. Any skipped entries are registered to you.\n`
        }

        response += `Invalid Entries: ${invalidEntries.length}\n\n`;
        if (invalidEntries.length >= 5)
        {
            // many errors
            let invalidCsv = "AllyCode,DiscordId,Reason\n"; // CSV header
            invalidCsv += invalidEntries.map(entry => `${entry.allycode},${entry.discordId},"${entry.invalidReason}"`).join("\n");
            const attachment = new AttachmentBuilder(Buffer.from(invalidCsv), { name: "invalid_entries.csv" });

            response += 'Review/edit the attached file and then attempt \`/guild users register-bulk\` with the corrected file.';
            
            files.push(attachment)
        } 
        else if (invalidEntries.length > 0)
        {
            response += `Please run \`/guild users register\` to manually register each player:\n`;
            response += invalidEntries.map(entry => `- AllyCode: ${entry.allycode}, DiscordId: ${entry.discordId}\n> Reason: ${entry.invalidReason}`).join("\n");
        }

        let embed = new EmbedBuilder()
            .setTitle("WookieeBot Bulk Registration Results")
            .setColor((invalidEntries.length > 0) ? 0x550000 : 0x005500)
            .setDescription(response);

        return { embeds: [embed], files: files };
    }
}