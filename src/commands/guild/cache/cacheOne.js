const playerUtils = require("../../../utils/player.js");
const { SlashCommandSubcommandBuilder } = require("@discordjs/builders");
const swapiUtils = require("../../../utils/swapi");
const { EmbedBuilder } = require("discord.js");
const constants = require("../../../utils/constants")

module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("one")
        .setDescription("Cache game data for one player.")
        .addIntegerOption(o => 
            o.setName("allycode")
            .setDescription("Ally code for the player to cache.")
            .setRequired(true)),
    permissionLevel: playerUtils.UserRoles.GuildAdmin,
    interact: async function(interaction)
    {
        let allycode = playerUtils.cleanAndVerifyStringAsAllyCode(interaction.options.getInteger("allycode").toString());
        let guildAllycodes = [allycode];
        
        let { botUser, guildId, error } = await playerUtils.authorize(interaction, playerUtils.UserRoles.GuildAdmin)
        if (error) {
            await interaction.editReply(error);
            return;
        }

        let { players, errors } = await swapiUtils.getPlayers({ allycodes: [allycode], useCache: false, cacheType: constants.CacheTypes.MANUAL });

        let embedToSend = new EmbedBuilder().setTitle("Guild Cache Results");
        if (errors.length > 0)
        {
            embedToSend.setDescription(`Failed to cache data for ${allycode}: ${errors[0].message}`);
            embedToSend.setColor(0xaa0000);
        }
        else
        {
            embedToSend.setDescription(`Cached data for ${allycode}.`)
            embedToSend.setColor(0x00aa00);
        }
        await interaction.followUp({ embeds: [embedToSend]});
    }
}