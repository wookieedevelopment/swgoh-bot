
const playerUtils = require("../../../utils/player.js");
const database = require("../../../database");
const { EmbedBuilder } = require("discord.js");
const { SlashCommandSubcommandBuilder } = require("@discordjs/builders");

module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("list")
        .setDescription("List caches for the guild."),
    permissionLevel: playerUtils.UserRoles.GuildAdmin,
    interact: async function(interaction)
    {
        let { botUser, guildId, error } = await playerUtils.authorize(interaction, playerUtils.UserRoles.GuildAdmin)
        if (error) {
            await interaction.editReply(error);
            return;
        }

        let response = await this.getCacheListResponse(guildId);
        await interaction.editReply(response);
    },
    getCacheListResponse: async function(guildId)
    {
        let caches = await database.db.any(`
        SELECT * 
        FROM (
            SELECT
                ROW_NUMBER() OVER (PARTITION BY type ORDER BY timestamp desc) AS r,
                player_cache.timestamp, type
            FROM player_cache
            WHERE guild_id = $1) x
        WHERE
            x.r <= 1;`, [guildId])

        let output = 
            "```" +   
            " No. |   Type  |       Time\r\n" + 
            "-----|---------|------------------------\r\n";
            
        output += caches.map((c, ix) => ` ${(ix + 1).toString().padEnd(3)} | ${c.type.padEnd(7)} | ${c.timestamp.toLocaleString("en-us", {
                    year: 'numeric',
                    month: 'short',
                    day: 'numeric',
                    hour: 'numeric',
                    minute: 'numeric',
                    timeZone: 'utc'
                })}`).join("\r\n")

        output += "```";
        let embed = new EmbedBuilder().setTitle("Guild Caches").setDescription(output).setFooter({ text: "All times in UTC" })
        return { embeds: [embed] };
    }
}