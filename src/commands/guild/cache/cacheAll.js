const playerUtils = require("../../../utils/player.js");
const { SlashCommandSubcommandBuilder } = require("@discordjs/builders");
const guildUtils = require("../../../utils/guild")
const swapiUtils = require("../../../utils/swapi");
const queueUtils = require("../../../utils/queue.js");
const constants = require("../../../utils/constants")
const { EmbedBuilder } = require("discord.js");
const { CLIENT } = require("../../../utils/discordClient");

const QUEUE_FUNCTION_KEY = "guild:cache:all";

module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("all")
        .setDescription("Refreshes guild roster and caches all players in your guild."),
    permissionLevel: playerUtils.UserRoles.GuildAdmin,
    interact: async function(interaction)
    {
        let { botUser, guildId, error } = await playerUtils.authorize(interaction, playerUtils.UserRoles.GuildAdmin)
        if (error) {
            await interaction.editReply(error);
            return;
        }

        let qsm = await queueUtils.getQueueSizeMessage(queueUtils.QUEUES.OTHER, interaction.client.shard);
        const editMessage = await interaction.editReply(`Queued **/guild cache all** request. ${qsm}`);
        
        await queueUtils.queueOtherJob(interaction.client.shard, 
        {
            functionKey: QUEUE_FUNCTION_KEY,
            data: { 
                guildId: guildId
            },
            channelId: editMessage.channelId ?? editMessage.channel.id,
            messageId: editMessage.id,
            userId: interaction.user.id,
        },
        {
            expireInHours: 4
        });
    }
}

async function generateCacheAllResponse(jobData)
{
    const channel = CLIENT.channels.cache.get(jobData.channelId);

    const editMessage = (await channel.messages.fetch({ message: jobData.messageId }));
    await editMessage.edit({ content: "Checking guild roster..." });

    let guild = await guildUtils.getGuildDataFromAPI({ wookieeBotGuildId: jobData.data.guildId }, { useCache: false, includeRecentGuildActivityInfo: true });

    await editMessage.edit({ content: "Found " + guildUtils.getGuildRoster(guild).length + " players. Caching player data..." });

    let guildAllycodes = guildUtils.getGuildAllycodesForAPIGuild(guild);

    let { players, errors} = await swapiUtils.getPlayers({
        allycodes: guildAllycodes, 
        useCache: false, 
        cacheType: constants.CacheTypes.MANUAL,
        updateStatusCallback: async (allycodes, players, errors) => {
            await editMessage.edit({ content: `Cached ${players.length}/${allycodes.length} players for **${guildUtils.getGuildName(guild)}**. Errors: ${errors.length}` }); 
        }}
    )
    
    let failureString = "";
    for (let error of errors)
    {
        failureString += error.message + "\r\n";
        guildAllycodes = guildAllycodes.filter(a => a != error.allycode)
    }

    var embedToSend = new EmbedBuilder().setTitle("Guild Cache Results");
    let succssfullyCachedPlayerCount = (guildUtils.getGuildRoster(guild).length - errors.length);
    var messageToSend = `Cached ${succssfullyCachedPlayerCount} player${succssfullyCachedPlayerCount == 1 ? "" : "s"}.`;
    if (errors.length > 0)
    {
        if (errors.length == 1) messageToSend += "\r\n\r\nThere was 1 failure\r\n" + failureString;
        else messageToSend += "\r\n\r\nThere were " + (failuresCount) + " failures:\r\n" + failureString;
        embedToSend.setColor(0xaa0000);
    } 
    else
    {
        embedToSend.setColor(0x00aa00);
    }

    embedToSend.setDescription(messageToSend.substring(0, 4000));

    const response = { embeds: [embedToSend] }; 
    return response;
}

queueUtils.addOtherFunction(QUEUE_FUNCTION_KEY,  generateCacheAllResponse);