const discordUtils = require("../../../utils/discord");
const playerUtils = require("../../../utils/player");
const guildUtils = require("../../../utils/guild");
const database = require("../../../database");
const { SlashCommandSubcommandBuilder } = require("@discordjs/builders");

module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("fix-name")
        .setDescription("Fix name display caused by CG localization issues")
        .addStringOption(o => 
            o.setName("name")
            .setDescription("Proper guild name.")
            .setRequired(true)),
    permissionLevel: playerUtils.UserRoles.GuildAdmin,
    interact: async function(interaction) {
        let name = interaction.options.getString("name");
        let response = await this.setFixedName(interaction, name);
        await interaction.editReply(response);
    },
    setFixedName: async function(interaction, name)
    {
        let { botUser, guildId, error } = await playerUtils.authorize(interaction, this.permissionLevel)
        if (error) {
            return { content: error };
        }

        await database.db.any("UPDATE guild SET fixed_name = $1 WHERE guild_id = $2", [name, guildId]);

        return { content: `Guild name updated to **${name}**`};
    }
}