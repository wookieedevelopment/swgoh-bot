const discordUtils = require("../../../utils/discord");
const playerUtils = require("../../../utils/player");
const guildUtils = require("../../../utils/guild");
const database = require("../../../database");
const { SlashCommandSubcommandBuilder } = require("@discordjs/builders");

module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("reset-name")
        .setDescription("Reset name to original"),
    permissionLevel: playerUtils.UserRoles.GuildAdmin,
    interact: async function(interaction) {
        let response = await this.resetName(interaction);
        await interaction.editReply(response);
    },
    resetName: async function(interaction)
    {
        let { botUser, guildId, error } = await playerUtils.authorize(interaction, this.permissionLevel)
        if (error) {
            return { content: error };
        }

        let res = await database.db.result("UPDATE guild SET fixed_name = NULL WHERE guild_id = $1 RETURNING guild_name", [guildId]);

        return { content: `Guild name reset to **${res.rows[0].guild_name}**.` };
    }
}