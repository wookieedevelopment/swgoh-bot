const playerUtils = require("../../../utils/player");
const database = require("../../../database");
const recommendationsUtils = require("../../../utils/recommendations");
const { SlashCommandSubcommandBuilder } = require("@discordjs/builders");

module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("default-raid-effort")
        .setDescription("Set default raid effort for /raid commands for your guild.")
        .addIntegerOption(o =>
            o.setName("effort")
            .setDescription("Effort Level")
            .addChoices(
                { name: recommendationsUtils.EFFORT.LOW.name, value: recommendationsUtils.EFFORT.LOW.value },
                { name: recommendationsUtils.EFFORT.MEDIUM.name, value: recommendationsUtils.EFFORT.MEDIUM.value },
                { name: recommendationsUtils.EFFORT.HIGH.name, value: recommendationsUtils.EFFORT.HIGH.value }
            )
            .setRequired(true))
        .addIntegerOption(o =>
            o.setName("alt")
            .setDescription("Alt account (/user me)")
            .setRequired(false)
            .setAutocomplete(true)),
    permissionLevel: playerUtils.UserRoles.GuildAdmin,
    interact: async function(interaction) {
        let effort = interaction.options.getInteger("effort");
        let response = await this.setDefaultEffort(interaction, effort);
        await interaction.editReply(response);
    },
    setDefaultEffort: async function(interaction, effort)
    {
        let { botUser, guildId, error } = await playerUtils.authorize(interaction, this.permissionLevel)
        if (error) {
            return { content: error };
        }

        await database.db.any("UPDATE guild SET raid_effort_default = $1 WHERE guild_id = $2", [effort, guildId]);

        return { content: `Default Raid Effort set to **${recommendationsUtils.mapValueToEffort(effort).name}**.`};
    },
    autocomplete: async function(interaction) {
        const focusedValue = interaction.options.getFocused();
        await interaction.respond((await playerUtils.getAltAutocompleteOptions(focusedValue, interaction.user.id)));
    }
}