const playerUtils = require("../../../utils/player");
const database = require("../../../database");
const got = require("got");
const { SlashCommandSubcommandBuilder } = require("@discordjs/builders");
const { AttachmentBuilder } = require("discord.js");

module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("set-logo")
        .setDescription("Upload a logo (.png, .jpg, etc.) for the guild")
        .addAttachmentOption(o => 
            o.setName("logo")
            .setDescription("Guild logo")
            .setRequired(true)),
    permissionLevel: playerUtils.UserRoles.GuildAdmin,
    interact: async function(interaction) {
        let logo = interaction.options.getAttachment("logo");
        let response = await this.setLogo(interaction, logo);
        await interaction.editReply(response);
    },
    setLogo: async function(interaction, logo)
    {
        let { botUser, guildId, error } = await playerUtils.authorize(interaction, this.permissionLevel)
        if (error) {
            return { content: error };
        }

        if (!logo.contentType.startsWith("image")) {
            return { content: "That is not an image."};
        }

        if (logo.size > 262144)
        {
            return { content: "Please keep the logo under 256 KB. Thank you!" }
        }

        let logoUrl = logo.url;
        let buffer = await got(logoUrl).buffer();

        await database.db.any("UPDATE guild SET logo = $1 WHERE guild_id = $2", [buffer, guildId]);

        const file = new AttachmentBuilder().setFile(buffer);
        return { content: `Guild logo updated. New logo shown below.`, files: [file]};
    }
}