const discordUtils = require("../../../utils/discord");
const playerUtils = require("../../../utils/player");
const guildUtils = require("../../../utils/guild");
const database = require("../../../database");
const { SlashCommandSubcommandBuilder } = require("@discordjs/builders");

module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("remove-logo")
        .setDescription("Remove guild logo"),
    permissionLevel: playerUtils.UserRoles.GuildAdmin,
    interact: async function(interaction) {
        let response = await this.removeLogo(interaction);
        await interaction.editReply(response);
    },
    removeLogo: async function(interaction)
    {
        let { botUser, guildId, error } = await playerUtils.authorize(interaction, this.permissionLevel)
        if (error) {
            return { content: error };
        }

        let res = await database.db.result("UPDATE guild SET logo = NULL WHERE guild_id = $1 RETURNING guild_name", [guildId]);

        return { content: `Removed logo for **${res[0].guild_name}**.` };
    }
}