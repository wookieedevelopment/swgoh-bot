const discordUtils = require("../../../utils/discord");
const playerUtils = require("../../../utils/player");
const guildUtils = require("../../../utils/guild");
const database = require("../../../database");
const { SlashCommandSubcommandBuilder } = require("@discordjs/builders");

module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("raid-estimate-factor")
        .setDescription("Adjustment to raid estimates to set as targets for your guild.")
        .addNumberOption(o => 
            o.setName("factor")
            .setDescription("Multiply WookieeBot estimates by this much as guild targets.")
            .setMinValue(0.01)
            .setMaxValue(3)
            .setRequired(true)),
    permissionLevel: playerUtils.UserRoles.GuildAdmin,
    interact: async function(interaction) {
        let factor = interaction.options.getNumber("factor");
        let response = await this.setFactor(interaction, factor);
        await interaction.editReply(response);
    },
    setFactor: async function(interaction, factor)
    {
        let { botUser, guildId, error } = await playerUtils.authorize(interaction, this.permissionLevel)
        if (error) {
            return { content: error };
        }

        await database.db.any("UPDATE guild SET raid_estimate_modifier = $1 WHERE guild_id = $2", [factor, guildId]);

        return { content: `Guild Raid Estimate Modifier updated to **${factor}**`};
    }
}