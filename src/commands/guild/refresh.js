const playerUtils = require("../../utils/player");
const refreshRoster = require("./refresh/refreshRoster")
const { SlashCommandSubcommandGroupBuilder } = require("@discordjs/builders");

module.exports = {
    data: new SlashCommandSubcommandGroupBuilder()
        .setName("refresh")
        .setDescription("Force refresh data for the guild.")
        .addSubcommand(refreshRoster.data)
        ,
    permissionLevel: playerUtils.UserRoles.GuildAdmin,
    interact: async function(interaction)
    {  
        let subcommand = interaction.options.getSubcommand();

        switch (subcommand)
        {
            case refreshRoster.data.name:
                await refreshRoster.interact(interaction);
                break;    
        }
    }
}