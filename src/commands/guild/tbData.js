const discordUtils = require("../../utils/discord.js");
const playerUtils = require("../../utils/player.js");
const database = require("../../database");
const got = require('got');
const parse = require("csv-parse/lib/sync");
const { EmbedBuilder } = require("discord.js");
const https = require('https');

const tbTypes = {
    dsgtb: { key: "DSGTB", maxWaves: 75, maxPhases: 4 },
    lsgtb: { key: "LSGTB", maxWaves: 74, maxPhases: 4 },
    dshtb: { key: "DSHTB", maxWaves: 76, maxPhases: 6 },
    lshtb: { key: "LSHTB", maxWaves: 72, maxPhases: 6 }
}

const tbCombatData = require("../../data/tb_data.json");
const { logger } = require("../../utils/log.js");
const { SlashCommandSubcommandBuilder, SlashCommandSubcommandGroupBuilder } = require("@discordjs/builders");

const tbDataCS = new database.pgp.helpers.ColumnSet(
    ['allycode', 
    'tb_type', 
    'date', 
    'combat_waves',
    'guild_id',
    'phase_data_json'
    ], 
    {table: 'player_tb_data'});

const dataTypes = {
    waves: 1
}

const TB_TYPE_CHOICES = [
    { name: "LS Hoth TB", value: "lshtb" },
    { name: "LS Geo TB", value: "lsgtb" },
    { name: "DS Hoth TB", value: "dshtb" },
    { name: "DS Geo TB", value: "dsgtb" }
];

module.exports = {
    data: new SlashCommandSubcommandGroupBuilder()
        .setName("tb")
        .setDescription("Manage TB Data for reporting.")
        .addSubcommand(s =>
            s.setName("upload-tbstats")
            .setDescription("Upload tbstats data from HotBot (from the '/tb stats' command).")
            .addAttachmentOption(o =>
                o.setName("file")
                .setDescription("The tbstats data file from HotBot (with the '/tb stats' command).")
                .setRequired(true)))
        .addSubcommand(s =>
            s.setName("upload-hotutils-export")
            .setDescription("Upload TB data exported from HotUtils website.")
            .addStringOption(o =>
                o.setName("tb-type")
                .setDescription("Type of TB")
                .setRequired(true)
                .addChoices(...TB_TYPE_CHOICES))
            .addStringOption(o => 
                o.setName("date")
                .setDescription("The date from which to delete data. Format: yyyymmdd (e.g., 20210730)")
                .setRequired(true))
            .addAttachmentOption(o =>
                o.setName("file")
                .setDescription("The TB Data file exported from HotUtils website.")
                .setRequired(true)))
        .addSubcommand(s => 
            s.setName("list-data")
            .setDescription("List TB Data that you've uploaded."))
        .addSubcommand(s => 
            s.setName("add")
            .setDescription("Add a data point manually (not via spreadsheet upload).")
            .addIntegerOption(o =>
                o.setName("allycode")
                .setDescription("Ally code for the player for whom to add a data point.")
                .setRequired(true))
            .addStringOption(o =>
                o.setName("tb-type")
                .setDescription("Type of TB")
                .setRequired(true)
                .addChoices(...TB_TYPE_CHOICES))
            .addStringOption(o => 
                o.setName("date")
                .setDescription("The date from which to delete data. Format: yyyymmdd (e.g., 20210730)")
                .setRequired(true))
            .addIntegerOption(o => 
                o.setName("waves")
                .setDescription("Number of waves for the player.")
                .setRequired(true))
            )
        .addSubcommand(s => 
            s.setName("delete-upload")
            .setDescription("Delete an uploaded TB data set (via upload-data).")
            .addStringOption(o =>
                o.setName("tb-type")
                .setDescription("Type of TB")
                .setRequired(true)
                .addChoices(...TB_TYPE_CHOICES))
            .addStringOption(o => 
                o.setName("date")
                .setDescription("The date from which to delete data. Format: yyyymmdd (e.g., 20210730)")
                .setRequired(true)))
        .addSubcommand(s => 
            s.setName("delete")
            .setDescription("Delete a data point that was added manually (via add).")
            .addIntegerOption(o =>
                o.setName("allycode")
                .setDescription("Ally code for the player for whom to delete a data point.")
                .setRequired(true))
            .addStringOption(o =>
                o.setName("tb-type")
                .setDescription("Type of TB")
                .setRequired(true)
                .addChoices(...TB_TYPE_CHOICES))
            .addStringOption(o => 
                o.setName("date")
                .setDescription("The date from which to delete data. Format: yyyymmdd (e.g., 20210730)")
                .setRequired(true))),
    name: "guild.uploadtbdata",
    description: "Upload TB Data from HotBot for tracking",
    permissionLevel: playerUtils.UserRoles.GuildAdmin,
    interact: async function(interaction)
    {
        let subcommand = interaction.options.getSubcommand();

        switch (subcommand)
        {
            case "upload-hotutils-export":
                await this.uploadHotutilsExportInteract(interaction);
                break;

            case "upload-tbstats":
                await this.uploadTbStatsInteract(interaction);
                break;
            
            case "list-data":
                await this.listTBDataInteract(interaction);
                break;
        
            case "delete-upload":
                await this.deleteUploadedDataInteract(interaction);
                break;
    
            case "add":
                await this.addManualTbDataInteract(interaction);
                break;

            case "delete":
                await this.deleteManualTbDataInteract(interaction);
                break;
                                    
        }
    },
    uploadHotutilsExportInteract: async function(interaction)
    {
        
        let tbTypeString = interaction.options.getString("tb-type");
        let dateString = interaction.options.getString("date");
        let file = interaction.options.getAttachment("file");

        let parseResponse = this.parseTBTypeAndDate(tbTypeString, dateString);

        if (parseResponse.error) { await interaction.editReply(parseResponse.error); return; }

        let { botUser, guildId, error } = await playerUtils.authorize(interaction, this.permissionLevel)
        if (error) {
            await interaction.editReply(error);
            return;
        }
        if (guildId == null) {
            await interaction.editReply("You don't have a guild registered.");
            return;
        }
        
        let response = await this.upload(file, guildId, parseResponse.tbType, parseResponse.date);
        await interaction.followUp(response);

//         const filter = (response) => { return response.author.id == interaction.user.id; }
//         await interaction.editReply(
// `Please upload to this channel the file that is generated from the HotUtils website by exporting TB data from a specific phase. `
//             , { fetchReply: true})
//             .then(async () => {
//                 await interaction.channel.awaitMessages({ 
//                     filter, max: 1
//                 })
//                 .then(async (collected) => {
//                     let attachment = collected.first().attachments.first();
//                     let response = await this.upload(attachment, guildId, parseResponse.tbType, parseResponse.date);
//                     await interaction.followUp(response);
//                 })
//             });
        
    },
    uploadTbStatsInteract: async function(interaction)
    {
        let { botUser, guildId, error } = await playerUtils.authorize(interaction, this.permissionLevel)
        if (error) {
            await interaction.editReply(error);
            return;
        }
        if (guildId == null) {
            await interaction.editReply("You don't have a guild registered.");
            return;
        }

        let file = interaction.options.getAttachment("file");
        
        let response = await this.upload(file, guildId, null, null);
        await interaction.followUp(response);
        
        // const filter = (response) => { return response.author.id == interaction.user.id; }
//         await interaction.editReply(
// `Please upload to this channel the file that is generated by running HotBot's $tbstats command. This can be $tbstats (for the current TB) or $tbstats all (for historic data). `
//             , { fetchReply: true})
//             .then(async () => {
//                 await interaction.channel.awaitMessages({ 
//                     filter, max: 1
//                 })
//                 .then(async (collected) => {
//                     let attachment = collected.first().attachments.first();
//                     let response = await this.upload(attachment, guildId, null, null);
//                     await interaction.followUp(response);
//                 })
//             });
    },
    uploadDataExecute: async function(input, args)
    {
        let { botUser, guildId, error } = await playerUtils.authorize(input, this.permissionLevel);
        if (error) { await input.reply(error); return; }
        if (guildId == null) {
            await input.reply("You don't have a guild registered.");
            return;
        }
        
        let parseResponse = this.parseTBTypeAndDateFromArgs(args) || {};
        if (parseResponse.error)
        {
            await input.reply(parseResponse.error);
            return;
        }

        var attachment = input.attachments.first();

        let response = await this.upload(attachment, guildId, parseResponse.tbType, parseResponse.date);
        await input.reply(response);
    },
    getAttachmentBuffer: async function(attachment)
    {
        if (!attachment)   
        {
            throw new Error("You must either upload a TB data export from HotUtils by running the hotbot command **tbstats all** or an export of a specific phase from the HotUtils website.");
        }

        if (attachment.size >= 1024*1024*8) // 8MB limit, maybe?
        {
            throw new Error("This file is too big.  Please limit to less than 8MB.  If you have a lot of TB data, you may need to split the file into more than one.  Just upload each separately.");
        }

        var ssUrl = attachment.url;
        var buffer = await got(ssUrl);
        return buffer;
    },
    upload: async function(attachment, guildId, tbType, date) {
        
        let buffer;
        try 
        {
            buffer = await this.getAttachmentBuffer(attachment);
        } 
        catch (error)
        {
            return error.message;
        }

        if (buffer.body.startsWith("Name"))
        {
            if (tbType == null || date == null) 
            {
                return "You have to specify a tb type and a date (yyyymmdd) to upload the export from HotUtils website. For example:\r\n" + 
                       "guild.uploadtbdata lsgtb 20210730";
            }

            // this is an export from hotbot website
            return await this.loadTbDataFromHotUtilsWebsite(tbType, date, buffer, guildId);
        }
        else 
        {
            return await this.loadTbDataFromBot(buffer, guildId);
        }
    },
    loadTbDataFromHotUtilsWebsite: async function(tbType, dataDate, buffer, guildId)
    {
        const records = parse(buffer.body, { columns: true });

        let tbDataInsertValues = records.map((r) =>
        {
            let allycode = r["Ally Code"];

            let phaseData = {
                combatWaves: [], totalScore: [], deployedPower: [], combatAttempts: []
            }
            
            let phase = 0;
            while (r["Mission Waves Phase " + (phase+1)] != null)
            {
                let cd = tbCombatData[tbType.key][phase];
                phaseData.combatWaves[phase] = [];
                for (let combatNum = 0; combatNum < cd.length; combatNum++)
                {
                    let key = `P${phase+1} ${cd[combatNum].type.replace(/^\w/, c => c.toUpperCase())} ${cd[combatNum].scores.slice(1).join("_")} [${cd[combatNum].num}]`
                    let val = parseInt(r[key]);
                    phaseData.combatWaves[phase][combatNum] = val;
                }
                phaseData.totalScore[phase] = parseInt(r[`Territory Points Phase ${phase + 1}`]);
                phaseData.deployedPower[phase] = 0; // TODO: calculate by multiplying the combat scores by points and then subtracting from total score
                phaseData.combatAttempts[phase] = parseInt(r[`Mission Attempts Phase ${phase + 1}`]);

                phase++;
            }

            return {
                allycode: allycode,
                tb_type: tbType.key,
                date: dataDate,
                combat_waves: parseInt(r["Mission Waves Total"]),
                guild_id: guildId,
                phase_data_json: phaseData
            }
        });

        var recsDeleted = 0;

        try {
            recsDeleted = await database.db.result("DELETE FROM player_tb_data WHERE guild_id = $1 AND manual_entry = FALSE AND date = $2", 
            [
                guildId, 
                dataDate
            ], r => r.rowCount)
        } catch (error)
        {
            x = 5;
        }
        const tbDataInsert = database.pgp.helpers.insert(tbDataInsertValues, tbDataCS);
        await database.db.none(tbDataInsert);

        return `TB data uploaded for 1 Territory Battle.  Replaced ${recsDeleted} existing records.`;

    },
    loadTbDataFromBot: async function(buffer, guildId)
    {

        var bufferString = buffer.rawBody.toString("utf-8");
        const records = parse(bufferString, { columns: true });

        var uniqueTBDates = new Array();
        records.filter(r => {
            let type = this.hotbotCodeToTbType(r.Instance);
            if (!type) return `${r.Instance} is not yet a supported TB type.`;
            return r.MapStatId == `strike_attempt_round_${type.maxPhases}`
        }).forEach(r => {
            var dateMillis = parseInt(r.CurrentRoundEndTime);
            if (uniqueTBDates.indexOf(dateMillis) == -1) uniqueTBDates.push(dateMillis);
        });

        // this will exist if the data was created after phase 4 complete
        var totalCombatWavesRecords = records.filter(r => r.MapStatId == "strike_encounter" && uniqueTBDates.indexOf(parseInt(r.CurrentRoundEndTime)) >= 0);
        if (totalCombatWavesRecords.length == 0)
        {
            return "There doesn't seem to be any combat summary data (strike_encounter) in your spreadsheet.  Please double check.";
        }
        

        try {
            playerUtils.cleanAndVerifyStringAsAllyCode(totalCombatWavesRecords[0].AllyCode); // throws exception if it's not an ally code

            let tbType = this.hotbotCodeToTbType(totalCombatWavesRecords[0].Instance);
            if (tbType == null)
                throw new Error("There is something wrong with the Instance codes in the file. Please verify you have not altered the file.");

            if (totalCombatWavesRecords[0].Score %1 !== 0 
                || totalCombatWavesRecords[0].Score < 0 
                || totalCombatWavesRecords[0].Score > tbType.maxWaves)
                throw new Error("There seems to be something wrong with the combat mission waves.  These do not appear to be valid numbers.");
        } 
        catch (error)
        {
            return { embeds: [discordUtils.createErrorEmbed("Rwww. ('No.').", error.message)] };
        }

        // scoreRoundN - (points for combat and fleet) - power_roundN = 0
        let additionalData = new Array();
        let scoresData = records.filter(r => r.MapStatId.startsWith("score_round_"));
        for (let sIx = 0; sIx < scoresData.length; sIx++)
        {
            let r = scoresData[sIx];

            let roundNum = r.MapStatId[r.MapStatId.length - 1];
            let type = this.hotbotCodeToTbType(r.Instance);
            if (type == null) continue;
            let pointsData = tbCombatData[type.key][roundNum - 1];
            let totalScore = parseInt(r.Score);
            //logger.info(`(${sIx}) Looking at round ${roundNum} record for ${r.AllyCode}. Total score ${totalScore}.`)

            let result = {
                allycode: r.AllyCode,
                dateMillis: parseInt(r['﻿CurrentRoundEndTime']),
                tbType: type,
                round: roundNum,
                power: 0,
                combatAttempts: 0,
                totalScore: totalScore,
                combatWaves: new Array(pointsData.length).fill(0)
            };

            additionalData.push(result);

            if (!tbCombatData[type.key]) continue;

            const relevantRecords = records.filter(rec => rec.AllyCode == r.AllyCode && rec.CurrentRoundEndTime == r.CurrentRoundEndTime && rec.Instance == r.Instance)
            
            // find the matching power for the round
            const powerRecord = relevantRecords.find(power => (power.MapStatId == ("power_round_" + roundNum)));
            // find matching attempts for the round
            const attemptsRecord = relevantRecords.find(attempts => (attempts.MapStatId == ("strike_attempt_round_" + roundNum)));
            

            if (!powerRecord)
            {
                // user scored no points
                continue;
            }
            
            result.power = parseInt(powerRecord.Score);

            if (!attemptsRecord)
            {
                // user had points but didn't do any combat
                continue;
            }

            result.combatAttempts = parseInt(attemptsRecord.Score);

            // now we need to go through and find what combination gets us to zero points
            let combatScore = totalScore-result.power;

            // Pre-calculate divisors
            let divisors = [];
            for (let i = pointsData.length - 1; i >= 0; i--) {
                divisors[i] = divisors[i + 1] ? divisors[i + 1] * pointsData[i + 1].scores.length : 1;
            }

            let numPermutations = pointsData.reduce((a, b) => a * b.scores.length, 1)
            
            //let numPermutations = divisors.reduce((a, b) => a + b, 0)
            for (let p = 0; p < numPermutations; p++)
            {

                let { score, waves } = this.getPermutation(p, pointsData, divisors);
                if (score == combatScore)
                {
                    result.combatWaves = waves;
                    break;
                }
            }
        }


            
        var tbDataInsertValues = new Array();

        totalCombatWavesRecords.forEach(record => {
            var dateMillis = parseInt(record.CurrentRoundEndTime);
            let date = new Date(dateMillis);

            let tbType = this.hotbotCodeToTbType(record.Instance);

            let filteredAdditionalData = additionalData.filter(d => d.dateMillis == dateMillis && d.tbType == tbType && d.allycode == record.AllyCode);

            let phaseData = {
                combatWaves: [], totalScore: [], deployedPower: [], combatAttempts: []
            }
            for (let fad = 0; fad < filteredAdditionalData.length; fad++)
            {
                let round = filteredAdditionalData[fad].round - 1;
                phaseData.combatWaves[round] = filteredAdditionalData[fad].combatWaves;
                phaseData.totalScore[round] = filteredAdditionalData[fad].totalScore;
                phaseData.deployedPower[round] = filteredAdditionalData[fad].power;
                phaseData.combatAttempts[round] = filteredAdditionalData[fad].combatAttempts;
            }

            tbDataInsertValues.push(
                {
                    allycode: record.AllyCode,
                    tb_type: tbType.key,
                    date: date,
                    combat_waves: record.Score,
                    guild_id: guildId,
                    phase_data_json: phaseData
                }
            )
        })

        uniqueTBDates = uniqueTBDates.map(d => new Date(d));
        var recsDeleted = 0;

        try {
            recsDeleted = await database.db.result("DELETE FROM player_tb_data WHERE guild_id = $1 AND manual_entry = FALSE AND date IN ($2:raw)", 
            [
                guildId, 
                uniqueTBDates.map(a => `'${a.getFullYear()}-${a.getMonth()+1}-${a.getDate()}'::date`).join(", ")
            ], r => r.rowCount)
        } catch (error)
        {
            x = 5;
        }
        const tbDataInsert = database.pgp.helpers.insert(tbDataInsertValues, tbDataCS);
        await database.db.none(tbDataInsert);

        return `TB data uploaded for ${uniqueTBDates.length} Territory Battles.  Replaced ${recsDeleted} existing records.`;
    },
    getPermutation: function(n, pointsData, divisors) {
        var score = 0, curArray;
        let waves = [];

        for (var i = 0; i < pointsData.length; i++) {
            curArray = pointsData[i].scores;
            score += curArray[Math.floor(n / divisors[i]) % curArray.length];
            waves[i] = Math.floor(n / divisors[i]) % curArray.length;
        }

        return { score: score, waves: waves };
    },
    hotbotCodeToTbType: function(code)
    {
        switch (code)
        {
            case "t01D": return tbTypes.lshtb; break;
            case "t02D": return tbTypes.dshtb; break;
            case "t03D": return tbTypes.dsgtb; break;
            case "t04D": return tbTypes.lsgtb; break;
            default:
                return null;
        }
    },
    addManualTbDataInteract: async function(interaction)
    {

        let allycode = interaction.options.getInteger("allycode").toString();
        let tbTypeString = interaction.options.getString("tb-type");
        let dateString = interaction.options.getInteger("date").toString();
        let waves = interaction.options.getInteger("waves");

        let { tbType, date, error } = this.parseTBTypeAndDate(tbTypeString, dateString);

        if (!error) response = await this.addManualTbData(interaction, allycode, tbType, date, waves);
        else response = error;

        await interaction.editReply(response);
    },
    addManualTbDataExecute: async function(input, args)
    {

        // allycode tb_type yyyymmdd waves=67
        let spl = this.splitArgs(args);
        if (!spl || spl.length < 4)
        {
            await input.channel.send("You must specify an allycode, a tb type, the date, and the data to set. For example:\r\n" +
                "guild.addmanualtbdata 123456789 LSGTB 20210730 waves=62");
            return;
        }
        let { tbType, date, error } = this.parseTBTypeAndDate(spl[1], spl[2]) || {};
        if (error) { await input.channel.send(error); return; }
        if (tbType == null || date == null) return;

        const regex = /\s*(\w+)=(\d+)\s*/g
        
        var data = spl.slice(3).join(" ").trim();
        let dataMatch = regex.exec(data);
        let waves;
        
        if (dataMatch != null)
        {
            let responseString = "";
            do {
                var type = dataMatch[1];
                var value = parseInt(dataMatch[2]);

                if (dataTypes[type] == dataTypes.waves)
                {
                    waves = value;
                }
                else
                {
                    responseString += ":warning: Data type not found: " + type + "\r\n"
                }

            } while ((dataMatch = regex.exec(data)) != null)

            if (responseString != "")
            {
                await input.channel.send(responseString);
            }
        } else {
            await input.channel.send("Invalid data format.  Try **waves=62**, for example.")
            return;
        }

        let response = await this.addManualTbData(input, spl[0], tbType, date, waves);
        await input.reply(response);
    },
    addManualTbData: async function(inputOrInteraction, allycode, tbType, date, waves)
    {
        // guild admin only
        let { botUser, guildId, error } = await playerUtils.authorize(inputOrInteraction, this.permissionLevel);
        if (error) { return error; }
        if (guildId == null) return "You don't have a guild registered.";

        try { allycode = playerUtils.cleanAndVerifyStringAsAllyCode(allycode); }
        catch (err) { return { embeds: [discordUtils.createErrorEmbed("Rwww. ('No.').", err.message)] }; }

        let player = await database.db.oneOrNone("SELECT player_name FROM guild_players WHERE guild_id = $1 AND allycode = $2",
            [guildId, allycode]);
        if (!player)
        {
            return `Ally code ${allycode} does not appear to be part of your guild. If they just joined, try again tomorrow.`;
        }

        let warning = "";
        if (waves > tbType.maxWaves)
        {
            warning = `:warning: Max wave count for ${tbType.key} is ${tbType.maxWaves}.  You specified ${waves}.\r\n`
        }

        await database.db.any(
            `INSERT INTO player_tb_data (allycode, tb_type, date, combat_waves, guild_id, manual_entry) 
            VALUES ($1, $2, $3, $4, $5, $6)
            ON CONFLICT (allycode, tb_type, date, guild_id, manual_entry)
            DO UPDATE SET combat_waves = $4`,
            [allycode, tbType.key, date, waves, guildId, true]);

        return warning + `:white_check_mark: Set ${tbType.key} combat waves for ${allycode} on ${date.getFullYear()}-${date.getMonth()+1}-${date.getDate()} to ${waves}.\r\n`
    },
    deleteManualTbDataInteract: async function(interaction)
    {
        let allycode = interaction.options.getInteger("allycode").toString();
        let tbTypeString = interaction.options.getString("tb-type");
        let dateString = interaction.options.getInteger("date").toString();

        let { tbType, date, error } = this.parseTBTypeAndDate(tbTypeString, dateString);

        if (!error) response = await this.deleteManualTbData(interaction, allycode, tbType, date);
        else response = error;

        await interaction.editReply(response);
    },
    deleteManualTbDataExecute: async function(input, args)
    {
        // allycode tb_type yyyymmdd        
        let spl = this.splitArgs(args);
        if (!spl || spl.length < 3)
        {
            await input.channel.send("You must specify an allycode, a tb type, and the date. For example:\r\n" +
                "guild.deletemanualtbdata 123456789 LSGTB 20210730");
            return;
        }
        let { tbType, date, error } = this.parseTBTypeAndDate(spl[1], spl[2]) || {};
        if (error) { await input.channel.send(error); return; }
        if (tbType == null || date == null) return;


        let response = await this.deleteManualTbData(input, spl[0], tbType, date);
        await input.reply(response);
    },
    deleteManualTbData: async function(inputOrInteraction, allycode, tbType, date)
    {
        let { botUser, guildId, error } = await playerUtils.authorize(inputOrInteraction, this.permissionLevel);
        if (error) { return error; }
        if (guildId == null) return "You don't have a guild registered.";

        try { allycode = playerUtils.cleanAndVerifyStringAsAllyCode(allycode); }
        catch (err) { return { embeds: [discordUtils.createErrorEmbed("Rwww. ('No.').", err.message)] }; }

        let player = await database.db.oneOrNone("SELECT player_name FROM guild_players WHERE guild_id = $1 AND allycode = $2",
            [guildId, allycode]);
        if (!player)
        {
            return `Ally code ${allycode} does not appear to be part of your guild. If they just joined, try again tomorrow.`;
        }

        var deleted = await database.db.result("DELETE FROM player_tb_data WHERE manual_entry=TRUE AND guild_id = $1 AND allycode = $2 AND tb_type = $3 AND date = $4",
            [guildId, allycode, tbType.key, date], r => r.rowCount)

        if (deleted)
        {
            return `Deleted manual data for ${allycode} for the ${tbType.key} on ${date.getFullYear()}-${date.getMonth()+1}-${date.getDate()}.`;
        }
        else
        {
            return `No data found for ${allycode} for the ${tbType.key} on ${date.getFullYear()}-${date.getMonth()+1}-${date.getDate()}.`;
        }
    },
    deleteUploadedDataInteract: async function(interaction)
    {
        let tbTypeString = interaction.options.getString("tb-type");
        let dateString = interaction.options.getInteger("date").toString();

        let { tbType, date, error } = this.parseTBTypeAndDate(tbTypeString, dateString);

        if (!error) response = await this.deleteUploadedData(interaction, tbType, date);
        else response = error;

        await interaction.editReply(response);
    },
    deleteUploadedDataExecute: async function(input, args)
    {
        let { tbType, date, error } = this.parseTBTypeAndDateFromArgs(args) || {};
        if (tbType == null || date == null)
        {
            await input.reply("You have to specify a tb type and a date (yyyymmdd) that the data was recorded. For example:\r\n" + 
            "guild.deletetbdata dsgtb 20210730");
            return;
        }
        if (error)
        {
            await input.reply(error);
            return;
        }

        let response = await this.deleteUploadedData(input, tbType, date);
        await input.reply(response);
    },
    deleteUploadedData: async function(inputOrInteraction, tbType, date)
    {

        let { botUser, guildId, error } = await playerUtils.authorize(inputOrInteraction, this.permissionLevel);
        if (error) { return error; }
        if (guildId == null) return "You don't have a guild registered.";

        await database.db.none("DELETE FROM player_tb_data WHERE guild_id = $1 AND tb_type = $2 AND date = $3 AND manual_entry = FALSE",
            [guildId, tbType.key, date])

        return `All ${tbType.key} for your guild from ${date.toDateString()} has been deleted.`;
    },
    listTBDataInteract: async function(interaction)
    {
        let response = await this.listTBData(interaction);
        await interaction.editReply(response);
    },
    listTBDataExecute: async function(input)
    {
        let response = await this.listTBData(input);
        await input.reply(response);
    },
    listTBData: async function(inputOrInteraction)
    {
        let { botUser, guildId, error } = await playerUtils.authorize(inputOrInteraction, this.permissionLevel);
        if (error) { return error; }
        if (guildId == null) return "You don't have a guild registered.";

        const uploads = await database.db.any(
            `SELECT DISTINCT tb_type, date, count(combat_waves) count
            FROM player_tb_data
            WHERE guild_id = $1
            GROUP BY tb_type, date
            ORDER BY date ASC`, [guildId]);

        var embedToSend = new EmbedBuilder().setTitle("TB Data Uploads").setColor(0x338811);
        var desc = "";
        for (var uploadIx = 0; uploadIx < uploads.length; uploadIx++)
        {
            let upload = uploads[uploadIx];
            desc += `${uploadIx+1}) ${upload.tb_type}, ${upload.date.getFullYear()}${(upload.date.getMonth()+1).toString().padStart(2, '0')}${upload.date.getDate().toString().padStart(2, '0')}: ${upload.count} ${upload.count == 1 ? "entry" : "entries"}\r\n`
        }
        embedToSend.setDescription(desc);
        return { embeds: [embedToSend] };
    },
    splitArgs: function(args)
    {
        if (!args || args.length == 0)
        {
            return null;
        }

        return args[0].split(" ");

    },
    parseTBTypeAndDateFromArgs: function(args)
    {
        let spl = this.splitArgs(args);

        if (spl == null || spl.length < 2)
        {
            return null;
        }

        var typeString = spl[0].trim();
        var dateString = spl[1].trim();

        let { tbType, date, error } = this.parseTBTypeAndDate(typeString, dateString);

        return { tbType, date, error };
    },
    parseTBTypeAndDate: function(typeString, dateString)
    {

        var error = null;
        var tbType = tbTypes[typeString];
        if (tbType == null)
        {
            error = `Invalid TB Type "${typeString}".  Options are: ${Object.keys(tbTypes).join(", ")}`;
        }
        
        var dateRegex = /^(20[1-2][0-9])([0-1][0-9])([0-3][0-9])$/
        var dateMatch = dateRegex.exec(dateString);
        if (dateMatch == null)
        {
            error = `Invalid date provided "${dateString}".  Format should be yyyymmdd (e.g., 20210730).`;
        }
        else {
            var date = new Date(dateMatch[1], parseInt(dateMatch[2])-1, dateMatch[3]);
        }

        return {tbType, date, error};
    }
    
}