
const playerUtils = require("../../../utils/player");
const discordUtils = require("../../../utils/discord");
const database = require("../../../database");
const recCommon = require("./common");
const { SlashCommandSubcommandBuilder } = require("@discordjs/builders");
const { AttachmentBuilder, ActionRowBuilder, ButtonBuilder, ButtonStyle, ComponentType } = require("discord.js");
const { logger, formatError } = require("../../../utils/log");

const YES_BUTTON_NAME = "guild:template:load:yes"
const NO_BUTTON_NAME = "guild:template:load:no"

module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("template")
        .setDescription("Leverage pre-canned recommendation templates")
        .addStringOption(o => 
            o.setName("type")
            .addChoices(
                { name: recCommon.REC_TYPES.OMICRON.displayName, value: recCommon.REC_TYPES.OMICRON.abbreviation }
            )
            .setRequired(true)
            .setDescription("Recommendation type"))
        .addStringOption(o => 
            o.setName("name")
            .setRequired(true)
            .setAutocomplete(true)
            .setDescription("Template name"))
        .addBooleanOption(o => 
            o.setName("load")
            .setRequired(false)
            .setDescription("Replace your current recommendations with the template")),
    permissionLevel: playerUtils.UserRoles.GuildAdmin,
    interact: async function(interaction)
    {
        let { botUser, guildId, error } = await playerUtils.authorize(interaction, this.permissionLevel)
        if (error) {
            await interaction.editReply(error);
            return;
        }

        let type = interaction.options.getString("type");
        let name = interaction.options.getString("name");
        let load = interaction.options.getBoolean("load");
        
        if (!load) {
            let response = await this.show(name, type);
            await interaction.editReply(response);
        }
        else 
        {
            const filter = async i => {
                await i.deferUpdate();
                return i.user.id === interaction.user.id;
            };

            
            let result = await database.db.one(`
            SELECT template_display_name
            FROM guild_recommendation_template
            WHERE type = $1 AND template_name = $2
            LIMIT 1`,
            [type, name]);
            
            let templateDisplayName = result.template_display_name;
    
            // ask about replacing existing plan
            let message = await interaction.editReply(
                { 
                    content: `Are you sure you want to overwrite your guild's **${recCommon.REC_TYPES_ABBREVIATION_MAP[type].displayName.toLowerCase()}** recommendations with **${templateDisplayName}**? This cannot be undone.`,
                    components: [
                        new ActionRowBuilder().addComponents(
                            new ButtonBuilder()
                                .setCustomId(YES_BUTTON_NAME)
                                .setLabel("Yes")
                                .setStyle(ButtonStyle.Success),
                            new ButtonBuilder()
                                .setCustomId(NO_BUTTON_NAME)
                                .setLabel("No")
                                .setStyle(ButtonStyle.Danger)
                        )
                    ]
                });
    
            message.awaitMessageComponent({ filter, componentType: ComponentType.Button, time: 300000 })
                .then(async i => {
                    if (i.customId == NO_BUTTON_NAME)
                        await interaction.editReply({ content: "Load cancelled.", components: [] });
                    else
                    {
                        await interaction.editReply({ content: "Processing...", components: [] });
                        let response = await this.load(guildId, name, type);
                        await interaction.editReply(response);
                    }
                })
                .catch(err => logger.error(formatError(err)));
        }
    },
    load: async function(guildId, name, type)
    {
        await database.db.none(`
        DELETE FROM guild_recommendation WHERE TYPE = $1 AND guild_id = $2;
        INSERT INTO guild_recommendation (guild_id, type, tier, value, description, unit_def_id, essential)
        SELECT $2, type, tier, value, description, unit_def_id, essential FROM guild_recommendation_template WHERE template_name = $3`,
        [type, guildId, name]);

        let response = { content: `Replaced existing guild **${recCommon.REC_TYPES_ABBREVIATION_MAP[type].displayName.toLowerCase()}** recommendations. New recommendations shown below.` };

        let recsData = await recCommon.loadRecommendationsForGuild(guildId, type);
        const buffer = await recCommon.drawRecommendationsToBuffer(recsData, type);
        
        let fileName = "guild-recs-" + Date.now() + ".png"
        const file = new AttachmentBuilder(buffer, { name: fileName });
        response.files = [file];

        return response;
    },
    show: async function(templateName, type)
    {
        let recsData = await recCommon.loadRecommendationsForTemplate(templateName);
        const buffer = await recCommon.drawRecommendationsToBuffer(recsData, type);
        
        let fileName = "template-recs-" + Date.now() + ".png"
        const file = new AttachmentBuilder(buffer, { name: fileName });
        return { files: [file] };
    },
    autocomplete: async function(interaction)
    {
        const type = interaction.options.getString("type");
        const focusedValue = interaction.options.getFocused();

        let matches = await database.db.any(`
        SELECT DISTINCT template_name,template_display_name
        FROM guild_recommendation_template
        WHERE type = $1 AND template_display_name ILIKE $2
        LIMIT 25`,
        [type, `%${focusedValue}%`]);

		await interaction.respond(
			matches
                .map(r => (
                    { 
                        name: discordUtils.fitForAutocompleteChoice(r.template_display_name), 
                        value: r.template_name
                    }))
		);
    }
}