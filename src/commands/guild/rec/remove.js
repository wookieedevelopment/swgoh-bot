
const playerUtils = require("../../../utils/player");
const database = require("../../../database");
const recCommon = require("./common");
const discordUtils = require("../../../utils/discord.js");
const { SlashCommandSubcommandBuilder } = require("@discordjs/builders");

module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("rem")
        .setDescription("Remove a guild recommendation.")
        .addStringOption(o => 
            o.setName("type")
            .addChoices(...recCommon.REC_TYPE_CHOICES)
            .setRequired(true)
            .setDescription("Recommendation type"))
        .addStringOption(o => 
            o.setName("rec")
            .setRequired(true)
            .setAutocomplete(true)
            .setDescription("The recommendation to remove.")),
    permissionLevel: playerUtils.UserRoles.GuildAdmin,
    interact: async function(interaction)
    {
        let { botUser, guildId, error } = await playerUtils.authorize(interaction, this.permissionLevel)
        if (error) {
            await interaction.editReply(error);
            return;
        }

        let rec = interaction.options.getString("rec");
        
        let response = await this.remove(guildId, rec);
        await interaction.editReply(response);
    },
    remove: async function(guildId, recommendationId)
    {
        let recommendationRemoved = await database.db.result("DELETE FROM guild_recommendation WHERE guild_id = $1 AND guild_recommendation_id = $2", 
            [guildId, recommendationId], r => r.rowCount);

        if (recommendationRemoved === 1) {
            return { content: "Removed recommendation." };
        }

        return { content: "No such recommendation for your guild." };
    },
    autocomplete: async function(interaction)
    {
        let { botUser, guildId, error } = await playerUtils.authorize(interaction, this.permissionLevel);     
        if (error)
        {
            await interaction.respond([{ name: "You do not have permission to do this.", value: "wwwwwwww" }]);
            return;
        }

        const type = interaction.options.getString("type");
        const focusedValue = interaction.options.getFocused().toLowerCase();

        let matches = await database.db.any(`
        SELECT guild_recommendation_id,description
        FROM guild_recommendation
        WHERE guild_id = $1 AND type = $2 AND description ILIKE $3
        ORDER BY guild_recommendation_id DESC
        LIMIT 25`,
        [guildId, type, `%${focusedValue}%`]);

		await interaction.respond(
			matches
                .map(r => (
                    { 
                        name: discordUtils.fitForAutocompleteChoice(r.description), 
                        value: r.guild_recommendation_id.toString()
                    }))
		);
    }
}
