const database = require("../../../database");
const swapiUtils = require("../../../utils/swapi");
const unitsUtils = require("../../../utils/units");
const { createCanvas, Image, loadImage } = require("canvas");
const discordUtils = require ("../../../utils/discord");
const datacronUtils = require ("../../../utils/datacron");
const drawingUtils = require("../../../utils/drawing");
const constants = require("../../../utils/constants");
const { drawText } = require("canvas-txt");

const REC_TYPES = {
    OMICRON: { abbreviation: "O", displayName: "Omicron" },
    DATACRON: { abbreviation: "D", displayName: "Datacron" }
}

const REC_TYPES_ABBREVIATION_MAP = {
    "O": REC_TYPES.OMICRON,
    "D": REC_TYPES.DATACRON
}

const REC_TYPE_CHOICES = [
    { name: REC_TYPES.OMICRON.displayName, value: REC_TYPES.OMICRON.abbreviation },
    { name: REC_TYPES.DATACRON.displayName, value: REC_TYPES.DATACRON.abbreviation }
];

const DATACRON_TIERS_ENUM = {
    CRITICAL: { value: 0, name: "Critical", desc: "Must Have" },
    IMPORTANT: { value: 1, name: "Important", desc: "Get If You Can" },
    SITUATIONAL: { value: 2, name: "Situational", desc: "May or May Not Be Used" }
};
const DATACRON_TIERS = [DATACRON_TIERS_ENUM.CRITICAL.name, DATACRON_TIERS_ENUM.IMPORTANT.name, DATACRON_TIERS_ENUM.SITUATIONAL.name];
const DATACRON_VALUE_MAP = {
    0: DATACRON_TIERS_ENUM.CRITICAL,
    1: DATACRON_TIERS_ENUM.IMPORTANT,
    2: DATACRON_TIERS_ENUM.SITUATIONAL,
}

const TIERS = ["S", "A", "B", "C", "D", "F"];
const ABILITY_TYPES = {
    1: { short: "B", long: "Basic" },       // character
    2: { short: "S", long: "Special" },     // character
    3: { short: "L", long: "Lead" },        // character
    4: { short: "U", long: "Unique" },      // character
    5: { short: "C", long: "Ship Char" },   // ship ablity that maps to character
    6: { short: "Ult", long: "Ultimate" }   // character
}

const DATACRON_CANVAS_DATA = {
    sectionHeaderHeight: 40,
    abilityRowHeight: 30,
    recRowHeight: 126, // abilityRowHeight*4 + borderWidth*3
    borderWidth: 2,
    checkboxColumnWidth: 30,
    checkboxWidthHeight: 30,
    notepadWidth: 18,
    notepadHeight: 22,
    abilityIconWidthHeight: 18,
    abilityIconColumnWidth: 22, 
    quantityColumnWidth: 60,
    abilityWidth: 1000,
    headerHeight: 50,
    recRowPaddingLeft: 20,
    recHeaderPaddingLeft: 10,
    paddingBetweenTiers: 10,
    width: 1110, // recRowPaddingLeft + checkboxColumnWidth + abilityWidth + quantityColumnWidth
}

const OMICRON_CANVAS_DATA = {
    firstRowHeight: 100,
    firstColWidth: 100,
    recColWidth: 490,
    recColPadding: 20,
    unitPortraitHeight: 80,
    unitPortraitWidth: 80,
    abilityIconHeight: 15,
    abilityIconWidth: 15,
    minTierHeight: 100,
    portraitRowSpacerHeight: 10,
    portraitPadding: 8,
    maxPortraitsPerRow: 5,
    modeImageHeight: 70,
    tierImageHeight: 100,
    cellBorderWidth: 5,
    abilityIconWidth: 30,
    abilityIconHeight: 30,
    footerHeight: 100,
    maxGuildLogoHeight: 90,
    maxGuildLogoWidth: 200,
    essentialIconWidth: 22
}

const FOOTER_CANVAS_DATA = {
    footerHeight: 100,
    maxGuildLogoHeight: 90,
    maxGuildLogoWidth: 200,
    padding: 20,
}

const TIER_DISPLAY = {
    "S": { color: "#5B9BD5" },
    "A": { color: "#F4DB90" },
    "B": { color: "#E7E6E6" },
    "C": { color: "#ED7D31" },
    "D": { color: "#111111" },
    "F": { color: "#FFC000" },
}

const OMICRON_DISPLAY_CATEGORIES = {
    RAID_CONQUEST_CHALLENGES: { id: "RAID_CONQUEST_CHALLENGES", displayName: "Raid, Conquest, GC", headerImg: `./src/img/omi-mode-raid-conquest.png`, fillStyle: "rgba(46,210,210,.05)" },
    TB: { id: "TB", displayName: "Territory Battles", headerImg: `./src/img/omi-mode-tb.png`, fillStyle: "rgba(112,173,71,.05)" },
    TW: { id: "TW", displayName: "Territory Wars", headerImg: `./src/img/omi-mode-tw.png`, fillStyle: "rgba(112,48,160,.05)" },
    GAC: { id: "GAC", displayName: "Grand Arena", headerImg: `./src/img/omi-mode-gac.png`, fillStyle: "rgba(255,0,0,.05)" },
};

const OMICRON_MODES_DISPLAY_ORDER = [OMICRON_DISPLAY_CATEGORIES.GAC, OMICRON_DISPLAY_CATEGORIES.TW, OMICRON_DISPLAY_CATEGORIES.TB, OMICRON_DISPLAY_CATEGORIES.RAID_CONQUEST_CHALLENGES];
const OMICRON_MODES_CHOICES = [
    { name: OMICRON_DISPLAY_CATEGORIES.TW.displayName, value: OMICRON_DISPLAY_CATEGORIES.TW.id },
    { name: OMICRON_DISPLAY_CATEGORIES.GAC.displayName, value: OMICRON_DISPLAY_CATEGORIES.GAC.id },
    { name: OMICRON_DISPLAY_CATEGORIES.TB.displayName, value: OMICRON_DISPLAY_CATEGORIES.TB.id },
    { name: OMICRON_DISPLAY_CATEGORIES.RAID_CONQUEST_CHALLENGES.displayName, value: OMICRON_DISPLAY_CATEGORIES.RAID_CONQUEST_CHALLENGES.id }
];

// omicron_mode
// 7 = TB
// 4 = raid
// 8 = TW
// 9 = GAC
// 11 = Conquest
// 14 = GAC 5v5
// 15 = GAC 5v5
const OMICRON_MODES = {
    4: { category: OMICRON_DISPLAY_CATEGORIES.RAID_CONQUEST_CHALLENGES },
    7: { category: OMICRON_DISPLAY_CATEGORIES.TB },
    8: { category: OMICRON_DISPLAY_CATEGORIES.TW },
    9: { category: OMICRON_DISPLAY_CATEGORIES.GAC },
    11: { category: OMICRON_DISPLAY_CATEGORIES.RAID_CONQUEST_CHALLENGES },
    12: { category: OMICRON_DISPLAY_CATEGORIES.RAID_CONQUEST_CHALLENGES },
    14: { category: OMICRON_DISPLAY_CATEGORIES.GAC },
    15: { category: OMICRON_DISPLAY_CATEGORIES.GAC },
}

module.exports = {
    REC_TYPES: REC_TYPES,
    REC_TYPES_ABBREVIATION_MAP: REC_TYPES_ABBREVIATION_MAP,
    TIERS: TIERS,
    DATACRON_TIERS: DATACRON_TIERS,
    DATACRON_TIERS_ENUM: DATACRON_TIERS_ENUM,
    DATACRON_VALUE_MAP: DATACRON_VALUE_MAP,
    ABILITY_TYPES: ABILITY_TYPES,
    OMICRON_MODES: OMICRON_MODES,
    OMICRON_MODES_CHOICES: OMICRON_MODES_CHOICES,
    OMICRON_DISPLAY_CATEGORIES: OMICRON_DISPLAY_CATEGORIES,
    OMICRON_MODES_DISPLAY_ORDER: OMICRON_MODES_DISPLAY_ORDER,
    REC_TYPE_CHOICES: REC_TYPE_CHOICES,
    TIER_DISPLAY: TIER_DISPLAY,
    loadRecommendationsForGuild: loadRecommendationsForGuild,
    loadRecommendationsForTemplate: loadRecommendationsForTemplate,
    drawRecommendationsToBuffer: drawRecommendationsToBuffer
}

async function drawRecommendationsToBuffer(recsData, type, playerData)
{
    if (type === REC_TYPES.DATACRON.abbreviation) return await drawDatacronRecommendationsToBuffers(recsData, playerData);
    else if (type === REC_TYPES.OMICRON.abbreviation) return await drawOmicronRecommendationsToBuffer(recsData);
}

async function drawDatacronRecommendationsToBuffers(recsData, playerData)
{
    let buffers = [];
    for (let r of recsData.recs.sets)
    {
        let obj = {
            recs: r,
            metadata: recsData.metadata
        }
        const buff = await drawDatacronRecommendationsToBuffer(obj, playerData);
        buffers.push(buff);
    }

    return buffers;
}

async function drawDatacronRecommendationsToBuffer(recsData, playerData)
{
    let recs = recsData.recs;

    let height = DATACRON_CANVAS_DATA.headerHeight + (playerData ? 0 : FOOTER_CANVAS_DATA.footerHeight);
    
    for (let i = 0; i < DATACRON_TIERS.length; i++)
    {
        if (recs[i].length > 0) height += DATACRON_CANVAS_DATA.sectionHeaderHeight + DATACRON_CANVAS_DATA.recRowHeight*recs[i].length + DATACRON_CANVAS_DATA.borderWidth*2*(2 + recs[i].length - 1) + DATACRON_CANVAS_DATA.paddingBetweenTiers;
    }

    let width = DATACRON_CANVAS_DATA.width;
    
    const canvas = createCanvas(width, height);
    const context = canvas.getContext('2d');
    let bgPattern = context.createPattern(drawingUtils.getImage(drawingUtils.IMAGES.BG_REC), 'repeat');
    context.fillStyle = bgPattern;
    context.fillRect(0, 0, width, height);
    
    context.fillStyle = "#00111150";
    context.fillRect(0, 0, context.canvas.width, DATACRON_CANVAS_DATA.headerHeight);


    context.fillStyle = "#fff";
    drawText(context, 
        playerData ? `Datacron Checklist for ${swapiUtils.getPlayerName(playerData)}${discordUtils.LTR} (${swapiUtils.getPlayerAllycode(playerData)})` : "Guild Datacron Checklist",
        {
            x: 0,
            y: 0,
            width: width,
            height: DATACRON_CANVAS_DATA.headerHeight,
            align: "center",
            fontSize: 20,
            font: `"Noto Sans","Noto Sans SC","Noto Sans Armenian","Noto Sans Thai","Noto Sans Arabic","Noto Sans Korean"`,
            justify: false,
            vAlign: "middle"
        }
        );
        
    let curPosY = DATACRON_CANVAS_DATA.headerHeight;    

    for (let v = 0; v < DATACRON_TIERS.length; v++)
    {
        if (recs[v].length === 0) continue;
        let tierEnum = DATACRON_VALUE_MAP[v];
        let tierName = `${tierEnum.name} (${tierEnum.desc})`;
        curPosY = await drawDatacronTierRecommendationsToBuffer(context, playerData, tierName, recs[v], curPosY);
        curPosY += DATACRON_CANVAS_DATA.paddingBetweenTiers;
    }
    
    if (!playerData) await addFooterToRecommendationsImage(context, recsData.metadata);

    const buffer = canvas.toBuffer('image/png');

    let allSets = await datacronUtils.getDatacronSetList();

    return {
        buffer: buffer,
        set: recsData.recs.set,
        color: allSets.find(s => s.set_id === recsData.recs.set).color_code
    };
}

async function drawDatacronTierRecommendationsToBuffer(context, playerData, tierName, recsToDraw, startingPosY)
{
    const datacronData = await datacronUtils.getActiveWookieeBotDatacronData();
    let sets = await datacronUtils.getDatacronSetList();
    let curPosY = startingPosY;

    drawingUtils.drawText(
        context, 
        tierName,
        DATACRON_CANVAS_DATA.recHeaderPaddingLeft, 
        curPosY + DATACRON_CANVAS_DATA.sectionHeaderHeight/2);

    curPosY += DATACRON_CANVAS_DATA.sectionHeaderHeight;
    
    for (let r = 0; r < recsToDraw.length; r++)
    {
        let rec = recsToDraw[r];

        context.fillStyle = "#fff";
        context.fillRect(DATACRON_CANVAS_DATA.recHeaderPaddingLeft, curPosY, context.canvas.width, DATACRON_CANVAS_DATA.borderWidth*2);
        curPosY += DATACRON_CANVAS_DATA.borderWidth*2;

        let checkboxToDraw, quantityText;
        if (!playerData)
        {
            checkboxToDraw = drawingUtils.getImage(drawingUtils.IMAGES.CHECKBOX_EMPTY);
            quantityText = "x" + rec.quantity;
        }
        else
        {
            let matchingDcs = swapiUtils.getPlayerDatacronWithAllWookieeBotDatacronIds(playerData, rec.affixes);
            checkboxToDraw = (matchingDcs.length >= rec.quantity) ? drawingUtils.getImage(drawingUtils.IMAGES.CHECKBOX_CHECKED) : drawingUtils.getImage(drawingUtils.IMAGES.CHECKBOX_EMPTY);
            quantityText = `${matchingDcs.length}/${rec.quantity}`;
        }
        
        // draw checkbox
        context.drawImage(checkboxToDraw,
            DATACRON_CANVAS_DATA.recRowPaddingLeft + (DATACRON_CANVAS_DATA.checkboxColumnWidth - DATACRON_CANVAS_DATA.checkboxWidthHeight)/2, 
            curPosY + DATACRON_CANVAS_DATA.recRowHeight/2 - DATACRON_CANVAS_DATA.checkboxWidthHeight/2, 
            DATACRON_CANVAS_DATA.checkboxWidthHeight, 
            DATACRON_CANVAS_DATA.checkboxWidthHeight);

        // draw quantity
        drawingUtils.drawText(
            context,
            quantityText,
            DATACRON_CANVAS_DATA.recRowPaddingLeft + DATACRON_CANVAS_DATA.checkboxColumnWidth + DATACRON_CANVAS_DATA.quantityColumnWidth/2,
            curPosY + DATACRON_CANVAS_DATA.recRowHeight/2,
            { fontSize: "15pt", align: "center", baseline: "middle" }
        );

        // if there are abilities
        if (rec.affixes.find(af => af != constants.NO_DATACRON_SPECIFIED)) await drawAbilityRows(context, rec, datacronData, sets, curPosY);
        else drawStats(context, rec, datacronData, curPosY);
        
        curPosY += DATACRON_CANVAS_DATA.recRowHeight;
    }

    return curPosY;
}

function drawStats(context, rec, datacronData, curPosY)
{

}

async function drawAbilityRows(context, rec, datacronData, sets, curPosY)
{
    // draw row background based on datacron color, if there's an affix specified
    
    let matchingDc = datacronData.find(dc => rec.affixes.find(af => af === dc.datacron_id));
    if (matchingDc)
    {
        let setData = sets.find(s => s.set_id === matchingDc.set_id);
        if (!setData) return;
        context.fillStyle = (setData.color_code ?? "#aaaaaa") + "30";
        context.fillRect(DATACRON_CANVAS_DATA.recRowPaddingLeft + DATACRON_CANVAS_DATA.checkboxColumnWidth + DATACRON_CANVAS_DATA.quantityColumnWidth, curPosY, context.canvas.width, DATACRON_CANVAS_DATA.recRowHeight);
    }

    context.fillStyle = "#ffff";
    // draw separators for the ability rows
    context.fillRect(
        DATACRON_CANVAS_DATA.recRowPaddingLeft + DATACRON_CANVAS_DATA.checkboxColumnWidth + DATACRON_CANVAS_DATA.quantityColumnWidth,
        curPosY + DATACRON_CANVAS_DATA.recRowHeight/4,
        context.canvas.width,
        DATACRON_CANVAS_DATA.borderWidth
    );
    context.fillRect(
        DATACRON_CANVAS_DATA.recRowPaddingLeft + DATACRON_CANVAS_DATA.checkboxColumnWidth + DATACRON_CANVAS_DATA.quantityColumnWidth,
        curPosY + DATACRON_CANVAS_DATA.recRowHeight*2/4,
        context.canvas.width,
        DATACRON_CANVAS_DATA.borderWidth
    );
    context.fillRect(
        DATACRON_CANVAS_DATA.recRowPaddingLeft + DATACRON_CANVAS_DATA.checkboxColumnWidth + DATACRON_CANVAS_DATA.quantityColumnWidth,
        curPosY + DATACRON_CANVAS_DATA.recRowHeight*3/4,
        context.canvas.width,
        DATACRON_CANVAS_DATA.borderWidth
    );

    // draw the ability row text
    let xPos = DATACRON_CANVAS_DATA.recRowPaddingLeft + DATACRON_CANVAS_DATA.checkboxColumnWidth + DATACRON_CANVAS_DATA.quantityColumnWidth + 4;
    for (let af = 0; af < 3; af++)
    {
        let desc = `L${(af+1)*3}: `;
        if (rec.affixes[af] == constants.NO_DATACRON_SPECIFIED)
        {
            desc += "Any"
        }
        else 
        {
            let matchingDc = datacronData.find(dc => dc.datacron_id === rec.affixes[af]);
            
            if (matchingDc.character)
            {
                let portrait = await unitsUtils.getPortraitForUnitByDefId(matchingDc.character);
                    
                let img = portrait ?? './src/img/no-portrait.png';

                const unitIconImg = await loadImage(img);
                context.drawImage(unitIconImg, 
                    xPos - 2, 
                    curPosY + (DATACRON_CANVAS_DATA.recRowHeight/8)*(af*2 + 1) - DATACRON_CANVAS_DATA.abilityIconWidthHeight/2,
                    DATACRON_CANVAS_DATA.abilityIconWidthHeight, DATACRON_CANVAS_DATA.abilityIconWidthHeight);
            }
            desc += matchingDc.short_description;
        }

        drawingUtils.drawText(
            context,
            desc,
            xPos + DATACRON_CANVAS_DATA.abilityIconColumnWidth,
            curPosY + (DATACRON_CANVAS_DATA.recRowHeight/8)*(af*2 + 1),
            { fontSize: "10pt", align: "left", baseline: "middle", maxWidth: context.canvas.width - xPos - 4 - DATACRON_CANVAS_DATA.abilityIconColumnWidth }
        );
    }

    // draw notepad
    context.drawImage(drawingUtils.getImage(drawingUtils.IMAGES.NOTEPAD),
        xPos-2,
        curPosY + (DATACRON_CANVAS_DATA.recRowHeight/8)*7 - Math.floor(DATACRON_CANVAS_DATA.notepadHeight/2),
        DATACRON_CANVAS_DATA.notepadWidth, 
        DATACRON_CANVAS_DATA.notepadHeight);

    // draw the note
    drawingUtils.drawText(
        context,
        (rec.note?.length > 0 ? rec.note : "No note."),
        xPos + DATACRON_CANVAS_DATA.abilityIconColumnWidth,
        curPosY + (DATACRON_CANVAS_DATA.recRowHeight/8)*7,
        { fontSize: "10pt", align: "left", baseline: "middle", maxWidth: context.canvas.width - xPos - 4 - DATACRON_CANVAS_DATA.abilityIconColumnWidth }
    )
}

async function drawOmicronRecommendationsToBuffer(recsData)
{
    let recsByMode = recsData.recs;
    let modesToInclude = Object.keys(recsByMode);
    let modes = OMICRON_MODES_DISPLAY_ORDER.filter(d => modesToInclude.find(m => m === d.id));

    let height = OMICRON_CANVAS_DATA.firstRowHeight + OMICRON_CANVAS_DATA.footerHeight;
    let maxRecsByTier = new Array(TIERS.length).fill(0);
    let heightByTier = new Array(TIERS.length).fill(0);

    for (let m of modes)
    {
        for (let t = 0; t < TIERS.length; t++)
        {
            maxRecsByTier[t] = Math.max(maxRecsByTier[t], recsByMode[m.id][t].length);
        }
    }

    for (let t = 0; t < TIERS.length; t++)
    {
        let tierHeight = Math.max(
            OMICRON_CANVAS_DATA.minTierHeight, 
            OMICRON_CANVAS_DATA.recColPadding*2 + Math.ceil(maxRecsByTier[t]/OMICRON_CANVAS_DATA.maxPortraitsPerRow) * (OMICRON_CANVAS_DATA.unitPortraitHeight + OMICRON_CANVAS_DATA.portraitRowSpacerHeight) - OMICRON_CANVAS_DATA.portraitRowSpacerHeight
        );
        height += tierHeight;
        heightByTier[t] = tierHeight;
    }

    const width = OMICRON_CANVAS_DATA.firstColWidth + OMICRON_CANVAS_DATA.recColWidth * modes.length;
    const canvas = createCanvas(width, height);
    const context = canvas.getContext('2d');

    context.drawImage(drawingUtils.getImage(drawingUtils.IMAGES.BG_REC), 0, 0)
    
    let essentialImg;
    if (recsByMode.essentials.length > 0) essentialImg = await loadImage('./src/img/essential.png');

    let curPosY;
    let curPosX = OMICRON_CANVAS_DATA.firstColWidth;
    for (let mode of modes)
    {
        context.lineWidth = OMICRON_CANVAS_DATA.cellBorderWidth;
        context.fillStyle = mode.fillStyle;
        context.fillRect(curPosX, OMICRON_CANVAS_DATA.firstRowHeight, OMICRON_CANVAS_DATA.recColWidth, height - OMICRON_CANVAS_DATA.firstRowHeight - OMICRON_CANVAS_DATA.footerHeight);

        context.lineWidth = OMICRON_CANVAS_DATA.cellBorderWidth;
        context.strokeStyle = "white";
        context.strokeRect(curPosX, 0, OMICRON_CANVAS_DATA.recColWidth, height - OMICRON_CANVAS_DATA.footerHeight);

        const mode_img = new Image();
        mode_img.onload = () => context.drawImage(mode_img, 
            curPosX + (OMICRON_CANVAS_DATA.recColWidth / 2) - (((OMICRON_CANVAS_DATA.modeImageHeight / mode_img.height)*mode_img.width)/2),
            20,
            (OMICRON_CANVAS_DATA.modeImageHeight / mode_img.height)*mode_img.width, 
            OMICRON_CANVAS_DATA.modeImageHeight);
        mode_img.onerror = err => { throw err }
        mode_img.src = mode.headerImg;

        let curPosY = OMICRON_CANVAS_DATA.firstRowHeight;
        for (let t = 0; t < TIERS.length; t++)
        {
            let recsForTier = recsByMode[mode.id][t];
            let x = OMICRON_CANVAS_DATA.recColPadding + OMICRON_CANVAS_DATA.cellBorderWidth; 
            let y = OMICRON_CANVAS_DATA.recColPadding;

            for (let r = 0; r < recsForTier.length; r++)
            {
                let portrait = await unitsUtils.getPortraitForUnitByDefId(recsForTier[r].character_base_id);
                
                let img = portrait ?? './src/img/no-portrait.png';
                context.fillStyle = TIER_DISPLAY[TIERS[t]].color;
                context.fillRect(curPosX + x, curPosY + y, OMICRON_CANVAS_DATA.unitPortraitWidth+2, OMICRON_CANVAS_DATA.unitPortraitHeight+2);

                const unitIconImg = await loadImage(img);
                context.drawImage(unitIconImg, 
                    curPosX + x + 1, 
                    curPosY + y + 1, 
                    OMICRON_CANVAS_DATA.unitPortraitWidth, OMICRON_CANVAS_DATA.unitPortraitHeight);

                let abilityIcon = await unitsUtils.getAbilityIconForUnitByBaseId(recsForTier[r].base_id);
                img = abilityIcon ?? './src/img/no-portrait.png';
                const abilityIconImg = await loadImage(img);
                context.drawImage(abilityIconImg, 
                    curPosX + x + 1 + OMICRON_CANVAS_DATA.unitPortraitWidth - OMICRON_CANVAS_DATA.abilityIconWidth, 
                    curPosY + y + 1 + OMICRON_CANVAS_DATA.unitPortraitHeight - OMICRON_CANVAS_DATA.abilityIconHeight, 
                    OMICRON_CANVAS_DATA.abilityIconWidth, OMICRON_CANVAS_DATA.abilityIconHeight);

                if (recsByMode.essentials.find(e => e === recsForTier[r]))
                {
                    context.drawImage(essentialImg,
                        curPosX + x + OMICRON_CANVAS_DATA.unitPortraitWidth - OMICRON_CANVAS_DATA.essentialIconWidth - 1,
                        curPosY + y + 1,
                        OMICRON_CANVAS_DATA.essentialIconWidth,
                        OMICRON_CANVAS_DATA.essentialIconWidth / essentialImg.width * essentialImg.height
                    );
                }

                if (r % OMICRON_CANVAS_DATA.maxPortraitsPerRow === 4)
                { 
                    x = OMICRON_CANVAS_DATA.recColPadding + OMICRON_CANVAS_DATA.cellBorderWidth; 
                    y += OMICRON_CANVAS_DATA.portraitRowSpacerHeight + OMICRON_CANVAS_DATA.unitPortraitHeight;
                } 
                else 
                {
                    x += OMICRON_CANVAS_DATA.unitPortraitWidth + OMICRON_CANVAS_DATA.portraitPadding;
                }
            }

            curPosY += heightByTier[t];
        }

        curPosX += OMICRON_CANVAS_DATA.recColWidth;
    }

    curPosY = OMICRON_CANVAS_DATA.firstRowHeight;
    for (let t = 0; t < TIERS.length; t++)
    {
        context.lineWidth = OMICRON_CANVAS_DATA.cellBorderWidth;
        context.strokeStyle = "white";
        context.strokeRect(0, curPosY, width, heightByTier[t]);

        const tier_img = new Image();
        tier_img.onload = () => context.drawImage(tier_img, 
            (OMICRON_CANVAS_DATA.firstColWidth / 2) - (((OMICRON_CANVAS_DATA.tierImageHeight / tier_img.height)*tier_img.width)/2), 
            curPosY + (heightByTier[t] / 2) - (OMICRON_CANVAS_DATA.tierImageHeight / 2), 
            (OMICRON_CANVAS_DATA.tierImageHeight / tier_img.height)*tier_img.width, 
            OMICRON_CANVAS_DATA.tierImageHeight);
        tier_img.onerror = err => { throw err }
        tier_img.src = `./src/img/tier-${t}.png`;

        curPosY += heightByTier[t];
    }

    await addFooterToRecommendationsImage(context, recsData.metadata);

    const buffer = canvas.toBuffer('image/png');

    return buffer;
}

async function addFooterToRecommendationsImage(context, metadata)
{
    
    let name = metadata.name;
    let logo = metadata.logo;
    let nowString = new Date().toLocaleDateString();
    let guildLeaderName = metadata.leaderName;
    let guildLeaderAllycode = metadata.leaderAllycode;

    context.font = `20pt "Noto Sans","Noto Sans SC","Noto Sans Armenian","Noto Sans Thai","Noto Sans Arabic","Noto Sans Korean"`
    context.textAlign = "left"
    context.textBaseline = "middle";
    context.fillStyle = "#fff"
    
    
    if (guildLeaderName)
    {
        context.fillText(name, FOOTER_CANVAS_DATA.padding, context.canvas.height - FOOTER_CANVAS_DATA.footerHeight * 3 / 4);
        let footerSubtext = `Leader: ${guildLeaderName} (${discordUtils.addDashesToAllycode(guildLeaderAllycode)})`
        context.font = `15pt "Noto Sans"`;
        context.fillText(footerSubtext, FOOTER_CANVAS_DATA.padding, context.canvas.height - 42);
        footerSubtext = `Generated by WookieeBot on ${nowString}`
        context.font = `15pt "Noto Sans"`;
        context.fillText(footerSubtext, FOOTER_CANVAS_DATA.padding, context.canvas.height - 15);
    } 
    else
    {
        context.fillText(name, FOOTER_CANVAS_DATA.padding, context.canvas.height - FOOTER_CANVAS_DATA.footerHeight / 2);
    }
    
    if (logo)
    {
        const logoImg = await loadImage(logo);

        let logoRenderHeight = FOOTER_CANVAS_DATA.maxGuildLogoHeight;
        let logoRenderWidth = logoRenderHeight / logoImg.height * logoImg.width;
        if (logoRenderWidth > FOOTER_CANVAS_DATA.maxGuildLogoWidth)
        {
            logoRenderHeight = FOOTER_CANVAS_DATA.maxGuildLogoWidth / logoRenderWidth * logoRenderHeight;
            logoRenderWidth = FOOTER_CANVAS_DATA.maxGuildLogoWidth;
        }

        context.drawImage(logoImg, 
            context.canvas.width - FOOTER_CANVAS_DATA.padding - logoRenderWidth, 
            context.canvas.height - FOOTER_CANVAS_DATA.footerHeight + (FOOTER_CANVAS_DATA.footerHeight - logoRenderHeight)/2, 
            logoRenderWidth, logoRenderHeight);
    }
}

async function mapDCReqs(recsData)
{
    if (recsData === null || recsData.length === 0) return null;
    let recs = {
        sets: [],
        all: []
    };

    recs[DATACRON_TIERS_ENUM.CRITICAL.value] = new Array();
    recs[DATACRON_TIERS_ENUM.IMPORTANT.value] = new Array();
    recs[DATACRON_TIERS_ENUM.SITUATIONAL.value] = new Array();

    const datacronData = await datacronUtils.getActiveWookieeBotDatacronData();
    
    for (let r of recsData)
    {
        let valueObj = JSON.parse(r.value);

        let match = null;
        for (let af of valueObj.affixes)
        {
            if (af === constants.NO_DATACRON_SPECIFIED) continue;
            match = datacronData.find(dc => dc.datacron_id === af);
            if (!match) break;
        }
        
        if (!match) continue; // datacron has expired
        
        let recObj = {
            description: r.description,
            affixes: valueObj.affixes,
            stats: valueObj.stats,
            quantity: r.data.quantity,
            set: match.set_id,
            sortString: r.description,
            note: r.data.note
        };

        switch (r.tier)
        {
            case DATACRON_TIERS_ENUM.CRITICAL.value: 
                recObj.tier = DATACRON_TIERS_ENUM.CRITICAL; 
                break;
            case DATACRON_TIERS_ENUM.IMPORTANT.value: 
                recObj.tier = DATACRON_TIERS_ENUM.IMPORTANT; 
                break;
            case DATACRON_TIERS_ENUM.SITUATIONAL.value: 
                recObj.tier = DATACRON_TIERS_ENUM.SITUATIONAL; 
                break;
        }

        recs[r.tier].push(recObj);
        recs.all.push(recObj);

        let recSet = recs.sets.find(s => s.set === recObj.set);
        if (!recSet)
        {
            recSet = {
                set: recObj.set,
                all: []
            };
            
            recSet[DATACRON_TIERS_ENUM.CRITICAL.value] = new Array();
            recSet[DATACRON_TIERS_ENUM.IMPORTANT.value] = new Array();
            recSet[DATACRON_TIERS_ENUM.SITUATIONAL.value] = new Array();

            recs.sets.push(recSet);
        }
        
        recSet[r.tier].push(recObj);
        recSet.all.push(recObj);
    }

    recs[DATACRON_TIERS_ENUM.CRITICAL.value].sort(DC_SORT);
    recs[DATACRON_TIERS_ENUM.IMPORTANT.value].sort(DC_SORT);
    recs[DATACRON_TIERS_ENUM.SITUATIONAL.value].sort(DC_SORT);
    recs.all.sort(DC_SORT);

    recs.sets.sort(DC_SORT);
    
    for (let s = 0; s < recs.sets.length; s++)
    {
        recs.sets[s][DATACRON_TIERS_ENUM.CRITICAL.value].sort(DC_SORT);
        recs.sets[s][DATACRON_TIERS_ENUM.IMPORTANT.value].sort(DC_SORT);
        recs.sets[s][DATACRON_TIERS_ENUM.SITUATIONAL.value].sort(DC_SORT);
        recs.sets[s].all.sort(DC_SORT)
    }

    return recs;
}

const DC_SORT = (a, b) => (b.set - a.set) || a.sortString.localeCompare(b.sortString);

async function mapOmicronRecsByMode(recsData)
{
    if (recsData === null || recsData.length === 0) return null;
    let omicronAbilitiesList = await swapiUtils.getOmicronAbilitiesList();
    let recsByMode = { essentials: new Array(), all: new Array() };

    for (let r of recsData)
    {
        let omi = omicronAbilitiesList.find(o => o.base_id === r.value);
        let mode = OMICRON_MODES[omi.omicron_mode].category.id;
        if (!recsByMode[mode])
        {
            recsByMode[mode] = new Array(TIERS.length);
            for (let i = 0; i < TIERS.length; i++) recsByMode[mode][i] = new Array(); 
        }

        recsByMode[mode][r.tier].push(omi);
        recsByMode.all.push(omi);
        if (r.essential) recsByMode.essentials.push(omi);
    }

    return recsByMode;
}

function createRecommendationsObject(recs, name, logo, leaderName, leaderAllycode)
{
    return {
        metadata: {
            name: name,
            logo: logo,
            leaderName: leaderName,
            leaderAllycode: leaderAllycode
        },
        recs: recs
    };
}

async function loadRecommendationsForTemplate(templateName)
{
    let recData = await database.db.any(`
        SELECT template_display_name,tier,value,unit_def_id,essential
        FROM guild_recommendation_template
        WHERE template_name = $1
        ORDER BY tier ASC,essential DESC`, [templateName]);

    let recsByMode = await mapOmicronRecsByMode(recData);
    
    return createRecommendationsObject(
        recsByMode,
        recData[0].template_display_name,
        null,
        null,
        null
    );
}

async function loadRecommendationsForGuild(guildId, type)
{
    let guildAndRecsData = await database.db.multiResult(`
        SELECT player_name,allycode,guild.guild_name,fixed_name,logo 
        FROM guild 
        JOIN guild_players USING (guild_id)
        WHERE guild.guild_id = $1 AND member_type = 'LEADER'; 
        SELECT tier,value,unit_def_id,description,essential,data
        FROM guild_recommendation 
        WHERE guild_id = $1 AND type = $2
        ORDER BY tier ASC,essential DESC`, [guildId, type]);

    let recs;
    if (type === REC_TYPES.OMICRON.abbreviation) recs = await mapOmicronRecsByMode(guildAndRecsData[1]);
    else if (type === REC_TYPES.DATACRON.abbreviation) recs = await mapDCReqs(guildAndRecsData[1]);

    return createRecommendationsObject(
        recs,
        guildAndRecsData[0].rows[0].fixed_name ?? guildAndRecsData[0].rows[0].guild_name,
        guildAndRecsData[0].rows[0].logo,
        guildAndRecsData[0].rows[0].player_name,
        guildAndRecsData[0].rows[0].allycode
    );
}