const recCommon = require ("./common");
const swapiUtils = require("../../../utils/swapi");
const discordUtils = require("../../../utils/discord.js");
const playerUtils = require("../../../utils/player");
const datacronUtils = require("../../../utils/datacron");
const constants = require("../../../utils/constants");
const database = require("../../../database");
const { SlashCommandSubcommandBuilder } = require("@discordjs/builders");

module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("dc")
        .setDescription("Set a guild datacron recomemndation.")
        .addIntegerOption(o => 
            o.setName("set")
            .setRequired(true)
            .setAutocomplete(true)
            .setDescription("Set of the datacron"))
        .addStringOption(o => 
            o.setName("tier")
            .setRequired(true)
            .setDescription("Recommendation tier")
            .addChoices(
                { name: "Critical (Most Important)", value: recCommon.DATACRON_TIERS_ENUM.CRITICAL.name },
                { name: "Important", value: recCommon.DATACRON_TIERS_ENUM.IMPORTANT.name },
                { name: "Situational (Least Important)", value: recCommon.DATACRON_TIERS_ENUM.SITUATIONAL.name }
            ))
        .addIntegerOption(o =>
            o.setName("a3")
            .setDescription("Datacron Level 3 ability to match.")
            .setAutocomplete(true)
            .setRequired(false))
        .addIntegerOption(o =>
            o.setName("a6")
            .setDescription("Datacron Level 6 ability to match.")
            .setAutocomplete(true)
            .setRequired(false))
        .addIntegerOption(o =>
            o.setName("a9")
            .setDescription("Datacron Level 9 ability to match.")
            .setAutocomplete(true)
            .setRequired(false))
        // .addStringOption(o =>
        //     o.setName("stats")
        //     .setDescription("Stats to match")
        //     .setRequired(false))
        .addIntegerOption(o =>
            o.setName("quantity")
            .setDescription("Count of this datacron to collect (default: 1")
            .setMinValue(1)
            .setMaxValue(50)
            .setRequired(false))
        .addStringOption(o => 
            o.setName("note")
            .setDescription("Additional associated text (max length 250)")
            .setRequired(false)
            .setMaxLength(250)),
    permissionLevel: playerUtils.UserRoles.GuildAdmin,
    interact: async function(interaction)
    {
        let { botUser, guildId, error } = await playerUtils.authorize(interaction, this.permissionLevel)
        if (error) {
            await interaction.editReply(error);
            return;
        }

        let set = interaction.options.getInteger("set");
        let tier = interaction.options.getString("tier");
        let affix3 = interaction.options.getInteger("a3") ?? constants.NO_DATACRON_SPECIFIED;
        let affix6 = interaction.options.getInteger("a6") ?? constants.NO_DATACRON_SPECIFIED;
        let affix9 = interaction.options.getInteger("a9") ?? constants.NO_DATACRON_SPECIFIED;
        let note = interaction.options.getString("note");
        let stats = null; //interaction.options.getString("stats");
        let quantity = interaction.options.getInteger("quantity");
        
        let affixes = [];
        if (affix3) affixes.push(affix3);
        if (affix6) affixes.push(affix6);
        if (affix9) affixes.push(affix9);

        let parsedStats = parseStats(stats);

        let response = await this.setDatacronRecommandation(guildId, tier, set, affixes, parsedStats, quantity, note);
        await interaction.editReply(response);
    },
    autocomplete: async function(interaction)
    {
		const focusedOption = interaction.options.getFocused(true);
        
        let choices;

        if (focusedOption.name === "set")
        {
            // set
            let activeDatacronSetList = await datacronUtils.getDatacronSetList(true);
            choices = activeDatacronSetList.map(s => { return { name: `${s.set_id} (${s.color_text})`, value: s.set_id }; });
        }
        else if (focusedOption.name.startsWith("a"))
        {
            let set = interaction.options.getInteger("set");
            let level = parseInt(focusedOption.name.charAt(1));
            // affix ability
            let allDatacronAbilities = await datacronUtils.getDatacronAbilities(set ? [set] : null, null, true);
            let allDatacronAbilitiesAtLevel = await datacronUtils.getDatacronAbilities(set ? [set] : null, level, true);
            let filteredDatacronAbilities = allDatacronAbilitiesAtLevel.filter(a => a.short_description.toLowerCase().indexOf(focusedOption.value.toLowerCase()) != -1).slice(0, 25);

            let a3 = interaction.options.getInteger("a3");
            let a6 = interaction.options.getInteger("a6");
            let a9 = interaction.options.getInteger("a9");
            if (focusedOption.name === "a3" && (a6 ?? a9))
            {
                let a69Alignment;
                if (a6)
                {
                    a69Alignment = allDatacronAbilities.find(dca => dca.datacron_id === a6).alignment;
                }
                else
                {
                    // seems that CG isn't tying the alignment of level 9 to the level 3, just to level 6
                    // uncomment the code below if CG changes this.
                    a69Alignment = "alignment_any";
                }

                // restrict alignment to the level 9 alignment
                // if ((a69Alignment == "alignment_any" || a69Alignment == null) && a9)
                // {
                //     a69Alignment = allDatacronAbilities.find(dca => dca.datacron_id === a9).alignment;
                // }

                // check if a6 or a9 have a value, limit a3 to valid options
                filteredDatacronAbilities = filteredDatacronAbilities.filter(a => (a.alignment == a69Alignment || a.alignment == "alignment_any" || a69Alignment == "alignment_any"));
            }
            else if (focusedOption.name === "a6")
            {
                // check if a3 or a9 have a value, limit a6 to valid options
                if (a9) 
                {
                    let a9Match = allDatacronAbilities.find(dca => dca.datacron_id === a9);
                    filteredDatacronAbilities = filteredDatacronAbilities.filter(a => (a.alignment == a9Match?.alignment || a.alignment == "alignment_any" || a9Match?.alignment == "alignment_any") && a.faction == a9Match?.faction);
                }
                else if (a3)
                {
                    let a3Match = allDatacronAbilities.find(dca => dca.datacron_id === a3);
                    filteredDatacronAbilities = filteredDatacronAbilities.filter(a => (a.alignment == a3Match?.alignment || a.alignment == "alignment_any" || a3Match?.alignment == "alignment_any"));
                }
            }
            else if (focusedOption.name === "a9")
            {
                // check if a3 or a6 have a value, limit a9 to valid options
                if (a6)
                {
                    let a6Match = allDatacronAbilities.find(dca => dca.datacron_id === a6);
                    filteredDatacronAbilities = filteredDatacronAbilities.filter(a => (a.alignment == a6Match?.alignment || a.alignment == "alignment_any" || a6Match?.alignment == "alignment_any") && a.faction == a6Match?.faction);
                }
                else if (a3)
                {
                    let a3Match = allDatacronAbilities.find(dca => dca.datacron_id === a3);
                    filteredDatacronAbilities = filteredDatacronAbilities.filter(a => (a.alignment == a3Match?.alignment || a.alignment == "alignment_any" || a3Match?.alignment == "alignment_any"));
                }
            }
            choices = filteredDatacronAbilities.map(a => { return { name: discordUtils.fitForAutocompleteChoice(a.short_description), value: a.datacron_id }});
        }

        await interaction.respond(choices);
    },
    setDatacronRecommandation: async function(guildId, tier, set, affixes, parsedStats, quantity, note)
    {
        if (!affixes.find(a => a != constants.NO_DATACRON_SPECIFIED))
        {
            return { content: "Must specify at least one ability (a3, a6, or a9) or stat (not supported yet)." }
        }

        let tierValue = recCommon.DATACRON_TIERS.findIndex(t => t === tier);
        if (tierValue === -1) return { content: "Invalid tier. Please choose from the list." };

        let activeDatacronSetList = await datacronUtils.getDatacronSetList(true);
        if (!activeDatacronSetList.find(s => s.set_id === set)) return { content: "Invalid set. Please choose one from the list."};

        let datacronAbilities = await datacronUtils.getDatacronAbilities([set], null, true);
        let matchingAbilities = datacronAbilities.filter(dca => affixes.find(a => a === dca.datacron_id));
        let abilityTexts = matchingAbilities.map(a => a.short_description);

        let parsedStatsTexts = parsedStats.map(s => `${s.name} >= ${s.minValue}`);

        let dcData = {
            affixes: affixes,
            stats: parsedStats
        };

        let data = {
            quantity: quantity ?? 1,
            note: note ?? ""
        };

        let desc = data.quantity + "x " + matchingAbilities.map(a => a.mini_description).join(" & ") + (parsedStatsTexts.length === 0 ? "" : " || " + parsedStatsTexts.join(", "));

        let existing = await database.db.oneOrNone("SELECT * FROM guild_recommendation WHERE Guild_id = $1 AND type = $2 AND value = $3",
            [guildId, recCommon.REC_TYPES.DATACRON.abbreviation, JSON.stringify(dcData)]);

        if (existing)
        {
            if (quantity == null) data.quantity = existing.data?.quantity;
            if (note == null) data.note = existing.data?.note;
        }

        await database.db.any(
            `INSERT INTO guild_recommendation (guild_id, type, value, tier, description, data)
            VALUES ($1, $2, $3, $4, $5, $6)
            ON CONFLICT (guild_id, type, value) DO UPDATE SET tier = $4, description = $5, data = $6`,
            [guildId, recCommon.REC_TYPES.DATACRON.abbreviation, JSON.stringify(dcData), tierValue, desc, data]
        );

        return { content: 
`Set the following datacron to **${tier}**:
Set: ${set}
Abilities:
- ${abilityTexts.length > 0 ? abilityTexts.join("\n- ") : "Any"}
Stats:
- ${parsedStatsTexts.length > 0 ? parsedStatsTexts.join("\n- "): "Any" }
Quantity: ${data.quantity}

Note: ${data.note}`
};
    }
}

function parseStats(statsString)
{
    let statsArray = [];
    return statsArray;
}