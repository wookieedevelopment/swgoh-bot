
const playerUtils = require("../../../utils/player");
const recCommon = require("./common");
const { SlashCommandSubcommandBuilder } = require("@discordjs/builders");
const { AttachmentBuilder, EmbedBuilder } = require("discord.js");


module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("show")
        .setDescription("Show guild recommendation infographic")
        .addStringOption(o => 
            o.setName("type")
            .addChoices(...recCommon.REC_TYPE_CHOICES)
            .setRequired(true)
            .setDescription("Recommendation type")),
    permissionLevel: playerUtils.UserRoles.User,
    interact: async function(interaction)
    {
        let { botUser, guildId, error } = await playerUtils.authorize(interaction, this.permissionLevel)
        if (error) {
            await interaction.editReply(error);
            return;
        }

        let type = interaction.options.getString("type");
        let response = await this.show(guildId, type);
        await interaction.editReply(response);
    },
    show: async function(guildId, type)
    {
        let recsData = await recCommon.loadRecommendationsForGuild(guildId, type);
        if (recsData.recs === null || recsData.recs.all.length === 0)
        {
            return { content: `You have no ${recCommon.REC_TYPES_ABBREVIATION_MAP[type].displayName.toLowerCase()} recommendations for your guild. Add some with \`/guild rec dc\`.` }; 
        }

        const buffer = await recCommon.drawRecommendationsToBuffer(recsData, type);
        
        if (buffer instanceof Array)
        {
            let files = [], embeds = [];
            let date = Date.now();
            for (let b = 0; b < buffer.length; b++)
            {
                let fileName = `guild-recs-${date}-${buffer[b].set}.png`;
                files.push(new AttachmentBuilder(buffer[b].buffer, { name: fileName }));   
         
                let embed = new EmbedBuilder()
                        .setColor(buffer[b].color ?? "Grey")
                        .setTitle(`Set ${buffer[b].set}`)
                        .setImage(`attachment://${fileName}`);
                embeds.push(embed);
            }

            return { embeds: embeds, files: files };   
        }
        else
        {
            let fileName = "guild-recs-" + Date.now() + ".png"
            const file = new AttachmentBuilder(buffer, { name: fileName });
            return { files: [file] };  
        }
    }
}