const recCommon = require ("./common");
const swapiUtils = require("../../../utils/swapi");
const discordUtils = require("../../../utils/discord.js");
const playerUtils = require("../../../utils/player");
const database = require("../../../database");
const { SlashCommandSubcommandBuilder } = require("@discordjs/builders");

module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("omi")
        .setDescription("Set a guild omicron recommendation.")
        .addStringOption(o => 
            o.setName("name")
            .setRequired(true)
            .setAutocomplete(true)
            .setDescription("The omicron to recommend"))
        .addStringOption(o => 
            o.setName("tier")
            .setRequired(true)
            .setDescription("Recommendation tier (S > A > B > C > D > F)")
            .addChoices(
                { name: "S (Best)", value: "S" },
                { name: "A", value: "A" },
                { name: "B", value: "B" },
                { name: "C", value: "C" },
                { name: "D", value: "D" },
                { name: "F (Trash)", value: "F" }
            ))
        .addBooleanOption(o => 
            o.setName("essential")
            .setRequired(false)
            .setDescription("Whether the omicron is a must-have for this tier")),
    permissionLevel: playerUtils.UserRoles.GuildAdmin,
    interact: async function(interaction)
    {
        let { botUser, guildId, error } = await playerUtils.authorize(interaction, this.permissionLevel)
        if (error) {
            await interaction.editReply(error);
            return;
        }

        let omi = interaction.options.getString("name");
        let tier = interaction.options.getString("tier");
        let essential = interaction.options.getBoolean("essential") ?? false;
        
        let response = await this.setOmiRecommandation(guildId, omi, tier, essential);
        await interaction.editReply(response);
    },
    autocomplete: async function(interaction)
    {
        let { botUser, guildId, error } = await playerUtils.authorize(interaction, this.permissionLevel);     
        if (error)
        {
            await interaction.respond([{ name: "You do not have permission to do this.", value: "wwwwwwww" }]);
            return;
        }

        const focusedValue = interaction.options.getFocused().toLowerCase();

        let omicronsList = await swapiUtils.getOmicronAbilitiesList();
        let matchingOmis = omicronsList.filter(a => a.unitName.toLowerCase().indexOf(focusedValue) >= 0 || a.name.toLowerCase().indexOf(focusedValue) >= 0 );

        matchingOmis.sort((a, b) => (a.unitName.localeCompare(b.unitName)) || (a.name.localeCompare(b.name)) );

		await interaction.respond(
			matchingOmis
                .slice(0, 25)
                .map(choice => (
                    { 
                        name: discordUtils.fitForAutocompleteChoice(
                            `${choice.unitName} (${recCommon.ABILITY_TYPES[choice.type].long}): ${choice.name}`
                        ), 
                        value: choice.base_id 
                    }))
		);
    },
    setOmiRecommandation: async function(guildId, omi, tier, essential)
    {
        let tierValue = recCommon.TIERS.findIndex(t => t === tier);
        if (tierValue === -1) return { content: "Invalid tier. Please choose from the list." };

        let omicronsList = await swapiUtils.getOmicronAbilitiesList();
        let omiMatch = omicronsList.find(o => o.base_id === omi);
        if (!omiMatch) return { content: "Invalid omicron. Please choose one from the list."};

        let omiSpot = recCommon.ABILITY_TYPES[omiMatch.type].long;
        let desc = `${omiMatch.unitName} (${omiSpot}): ${omiMatch.name}`;

        await database.db.any(
            `INSERT INTO guild_recommendation (guild_id, type, value, unit_def_id, tier, description, essential)
            VALUES ($1, $2, $3, $4, $5, $6, $7)
            ON CONFLICT (guild_id, type, value) DO UPDATE SET tier = $5, description = $6, essential = $7`,
            [guildId, recCommon.REC_TYPES.OMICRON.abbreviation, omi, omiMatch.character_base_id, tierValue, desc, essential]
        );

        return { content: `**${omiMatch.unitName} (${omiSpot}): ${omiMatch.name}** set to **${tier}**-tier, ${essential ? "essential" : "non-essential" }.`};
    }
}
