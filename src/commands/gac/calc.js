
const { SlashCommandSubcommandBuilder } = require("@discordjs/builders");
const { Collection, ActionRowBuilder, StringSelectMenuBuilder } = require("discord.js");
const database = require("../../database")
const playerUtils = require('../../utils/player')
const constants = require('../../utils/constants')
const swapiUtils = require("../../utils/swapi")
const fs = require('fs');
const riskCalc = require ("./calcs/risk")
const discordUtils = require ('../../utils/discord');

var CALCULATIONS = new Collection();

const calcFiles = fs.readdirSync(`${__dirname}/calcs`).filter(file => file.endsWith('.js'));

for (const file of calcFiles) {
	const calc = require(`${__dirname}/calcs/${file}`);
    CALCULATIONS.set(calc.data.name, calc);
}

CALCULATIONS.sort((a, b) => a.data.label.localeCompare(b.data.label))

module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("calc")
        .setDescription("GAC Calculations")
        .addIntegerOption(o =>
            o.setName("opponent")
            .setDescription("Opponent's ally code.")
            .setRequired(true))
        .addIntegerOption(o =>
            o.setName("me")
            .setDescription("Your ally code, if your guild is not registered.")
            .setRequired(false))
        .addBooleanOption(o =>
            o.setName("force-refresh")
            .setDescription("Refresh player data. Slows results. Automatically happens if data is >1 day old.")
            .setRequired(false))
        .addIntegerOption(o =>
            o.setName("alt")
            .setDescription("Alt account (/user me)")
            .setRequired(false)
            .setAutocomplete(true)),
    interact : async function(interaction) {
        let meAllyCode = interaction.options.getInteger("me")?.toString();
        let opAllyCode = interaction.options.getInteger("opponent")?.toString();
        let forceRefresh = interaction.options.getBoolean("force-refresh");
        let alt = interaction.options.getInteger("alt");

        let { botUser, guildId, error } = await playerUtils.authorize(interaction);
        
        if (meAllyCode) meAllyCode = playerUtils.cleanAndVerifyStringAsAllyCode(meAllyCode);
        meAllyCode = playerUtils.chooseBestAllycode(botUser, guildId, meAllyCode, alt);

        if (meAllyCode == null)
        {
            await interaction.editReply("Register with `/register` or provide an allycode using the `me` option.");
            return;
        }

        opAllyCode = playerUtils.cleanAndVerifyStringAsAllyCode(opAllyCode);
        
        let { playerData, dataTimestamps } = await this.fetchPlayers(meAllyCode, opAllyCode, forceRefresh);
        
        embeds = [ riskCalc.doCalc(playerData[0], playerData[1]) ];

        await interaction.editReply(
            { 
                content: this.createMessageContent(playerData, dataTimestamps), 
                embeds: embeds, 
                components: [this.createSelectActionRowBuilder(meAllyCode, opAllyCode)] 
            });
    },
    autocomplete: async function(interaction) {
        const focusedValue = interaction.options.getFocused();
        await interaction.respond((await playerUtils.getAltAutocompleteOptions(focusedValue, interaction.user.id)));
    },
    createMessageContent: function(playerData, dataTimestamps)
    {
        return `Choose a calculation to view from the drop down below.

Using data from ${dataTimestamps[0].toUTCString()} for ${swapiUtils.getPlayerName(playerData[0])}${discordUtils.LTR} (${swapiUtils.getPlayerAllycode(playerData[0])}).
Using data from ${dataTimestamps[1].toUTCString()} for ${swapiUtils.getPlayerName(playerData[1])}${discordUtils.LTR} (${swapiUtils.getPlayerAllycode(playerData[1])})`;
    },
    createSelectActionRowBuilder: function(meAllyCode, opAllyCode)
    {
        const row = new ActionRowBuilder()
        .addComponents(
            new StringSelectMenuBuilder()
                .setCustomId(`gac-calc:${meAllyCode},${opAllyCode}`)
                .setPlaceholder("Choose a calculation:")
                .addOptions(
                    CALCULATIONS.map((v, k) => { return {label: v.data.label, value: k } })
                )
        )

        return row;
    },
    onSelect: async function(interaction)
    {
        //let originalMessage = interaction.message.content;
        // await interaction.deferUpdate();

        await interaction.update({ content: "Running calculation, please wait...", components: [], embeds: [] });
        let regex = /gac-calc:(\d{9}),(\d{9})/
        let matches = regex.exec(interaction.customId)

        if (!matches || matches.length < 3)
        {
            await interaction.editReply({ content: "An error occurred, please try again." });
            return;
        }
        let meAllyCode = matches[1];
        let opAllyCode = matches[2];
        let type = interaction.values[0];

        let { playerData, dataTimestamps } = await this.fetchPlayers(meAllyCode, opAllyCode, false);
        
        let calcToRun = CALCULATIONS.get(type); 
        let embeds;

        // some calcs require async, others not
        if (calcToRun.doCalc.constructor && calcToRun.doCalc.constructor.name == 'AsyncFunction')
        {
            embeds = [await calcToRun.doCalc(playerData[0], playerData[1])];
        }
        else
        {
            embeds = [calcToRun.doCalc(playerData[0], playerData[1])];
        }

        await interaction.editReply(
            { 
                content: this.createMessageContent(playerData, dataTimestamps),
                embeds: embeds, 
                components: [this.createSelectActionRowBuilder(meAllyCode, opAllyCode)] 
            });
    },
    fetchPlayers: async function(meAllyCode, opAllyCode, forceRefresh)
    {

        let playerData = new Array(2);
        let dataTimestamps = new Array(2);

        if (!forceRefresh)
        {
            let existingData = await database.db.multiResult(
                `SELECT data_text, player_cache.timestamp
                FROM player_cache 
                LEFT JOIN cache_data USING (cache_data_id)
                WHERE allycode = $1 AND type = $3 AND player_cache.timestamp > (now() - INTERVAL '1 DAY')
                ORDER BY player_cache.timestamp DESC
                LIMIT 1;
                SELECT data_text, player_cache.timestamp
                FROM player_cache 
                LEFT JOIN cache_data USING (cache_data_id)
                WHERE allycode = $2 AND type = $3 AND player_cache.timestamp > (now() - INTERVAL '1 DAY')
                ORDER BY player_cache.timestamp DESC
                LIMIT 1;`,
                [meAllyCode, opAllyCode, constants.CacheTypes.QUERY]
            );
            
            if (existingData[0].rows.length > 0)
            {
                playerData[0] = JSON.parse(existingData[0].rows[0].data_text);
                dataTimestamps[0] = existingData[0].rows[0].timestamp;
            } 

            if (existingData[1].rows.length > 0)
            {
                playerData[1] = JSON.parse(existingData[1].rows[0].data_text);
                dataTimestamps[1] = existingData[1].rows[0].timestamp;
            } 
        } 
        
        let now = new Date();
        if (playerData[0] == null)
        {
            playerData[0] = await swapiUtils.getPlayer(meAllyCode);
            dataTimestamps[0] = now;
        }

        if (playerData[1] == null)
        {
            playerData[1] = await swapiUtils.getPlayer(opAllyCode);
            dataTimestamps[1] = now;
        }

        return { playerData, dataTimestamps };
    }
}