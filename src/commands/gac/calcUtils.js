const metadataCache = require("../../utils/metadataCache")
const fs = require("fs");
const swapi = require("../../utils/swapi");

const RESULT_COLORS =
{
  GOOD: 0x336633,
  BAD: 0x661122,
  NEUTRAL: 0xaa8800
}

const RISK_FACTORS =
{
  metaFleets: "meta_fleets",
  zetaCount: "zeta_count",
  sixDotMods: "six_dot_mods",
  speedMods: "speed_mods",
  glCount: "gl_count",
  gacOmicrons: "gac_omicrons",
  relics: "relics",
  gear: "gear",
  datacrons: "datacrons"
}

const GAC_TYPES = 
{
  GAC_3v3: 1,
  GAC_5v5: 2,
  GAC_ANY: 3
}

cacheGacRiskData();
fs.watchFile("./src/data/gac_risk_data.json",
  (curr, prev) => { cacheGacRiskData(); });

module.exports = {
  RESULT_COLORS: RESULT_COLORS,
  GAC_TYPES: GAC_TYPES,
  calculateRisk: function (meData, opponentData) {
    let riskResult = {
      score: 50,
      factors: "Risk factors:\n"
    };

    let gacRiskData = metadataCache.get(metadataCache.CACHE_KEYS.gacRiskData);

    addMetaFleetRisk(riskResult, gacRiskData, meData, opponentData);
    addZetaRisk(riskResult, gacRiskData, meData, opponentData);
    addGearRisk(riskResult, gacRiskData, meData, opponentData);
    addSixDotModsRisk(riskResult, gacRiskData, meData, opponentData);
    addSpeedModsRisk(riskResult, gacRiskData, meData, opponentData);
    addGLRisk(riskResult, gacRiskData, meData, opponentData);
    addOmicronRisk(riskResult, gacRiskData, meData, opponentData);
    addRelicsRisk(riskResult, gacRiskData, meData, opponentData);
    addDatacronRisk(riskResult, gacRiskData, meData, opponentData);

    riskResult.score = Math.round(riskResult.score);
    if (riskResult.score > 100) riskResult.score = 100;
    else if (riskResult.score < 0) riskResult.score = 0;

    return riskResult;
  }
}

function cacheGacRiskData() {
  fs.readFile("./src/data/gac_risk_data.json", { encoding: "utf-8" }, (err, data) => {
    let riskData = JSON.parse(data);

    metadataCache.set(metadataCache.CACHE_KEYS.gacRiskData, riskData, 1000 * 60 * 60 * 24 * 30);
  });
}

function scaleScoreByRatio(ratio, threshold, maxScoreEffect) {
  if (threshold == 0) return 0;

  let ratioToScaleOn = 1 - ((ratio > 1) ? 1.0 / ratio : ratio);

  let effect = maxScoreEffect;
  if (ratioToScaleOn < threshold)
    effect = Math.min(maxScoreEffect, Math.round(ratioToScaleOn * maxScoreEffect / threshold));

  if (ratio > 1) effect *= -1;
  return effect;
}

function calcRatio(myCount, oppCount, ratioWhenOppCountIsZero) {
  ratio = ratioWhenOppCountIsZero; // default if opponent has none and player has
  if (oppCount > 0)
    ratio = myCount / oppCount;
  else if (myCount == 0) // account for when both players have 0
    ratio = 1;

  return ratio;
}

function addMetaFleetRisk(riskResult, gacRiskData, meData, opponentData) {
  var meMFCount = swapi.getPlayerRoster(meData).filter(u => gacRiskData.metaFleets.indexOf(swapi.getUnitDefId(u)) >= 0).length;
  var opMFCount = swapi.getPlayerRoster(opponentData).filter(u => gacRiskData.metaFleets.indexOf(swapi.getUnitDefId(u)) >= 0).length;

  let mfRatio = calcRatio(meMFCount, opMFCount, 10000);

  let effect = scaleScoreByRatio(mfRatio, gacRiskData.riskFactors[RISK_FACTORS.metaFleets].threshold, gacRiskData.riskFactors[RISK_FACTORS.metaFleets].effect);

  riskResult.score += effect;

  if (mfRatio < 0.5) {
    riskResult.factors += `Meta Fleets: Opponent has significantly more (risk ${effect}).\n`;
  }
  else if (mfRatio < 1) {
    riskResult.factors += `Meta Fleets: Opponent has more (risk ${effect}).\n`;
  }
  else if (mfRatio > 1.5) {
    riskResult.factors += `Meta Fleets: Opponent has significantly fewer (risk ${effect}).\n`;
  }
  else if (mfRatio > 1) {
    riskResult.factors += `Meta Fleets: Opponent has fewer (risk ${effect}).\n`;
  }
  else {
    riskResult.factors += `Meta Fleets: Equal (risk ${effect})\n`;
  }
}

function addOmicronRisk(riskResult, gacRiskData, meData, opponentData) {
  var meOmiCount = swapi.getPlayerGacOmicronCount(meData);
  var opOmiCount = swapi.getPlayerGacOmicronCount(opponentData);

  let omiRatio = calcRatio(meOmiCount, opOmiCount, meOmiCount + 1);

  let effect = scaleScoreByRatio(omiRatio, gacRiskData.riskFactors[RISK_FACTORS.gacOmicrons].threshold, gacRiskData.riskFactors[RISK_FACTORS.gacOmicrons].effect);
  riskResult.score += effect;

  if (omiRatio < 0.5) {
    riskResult.factors += `GAC Omicrons: Opponent has significantly more (risk ${effect}).\n`;
  }
  else if (omiRatio < 1) {
    riskResult.factors += `GAC Omicrons: Opponent has more (risk ${effect}).\n`;
  }
  else if (omiRatio > 1.5) {
    riskResult.factors += `GAC Omicrons: Opponent has significantly fewer (risk ${effect}).\n`;
  }
  else if (omiRatio > 1) {
    riskResult.factors += `GAC Omicrons: Opponent has fewer (risk ${effect}).\n`;
  }
  else {
    riskResult.factors += `GAC Omicrons: Equal (risk ${effect})\n`;
  }
}

// Risk based on zeta count
function addZetaRisk(riskResult, gacRiskData, meData, opponentData) {
  let meZetaCount = swapi.getPlayerZetaCount(meData);
  let oppZetaCount = swapi.getPlayerZetaCount(opponentData);
  let zetaRatio = calcRatio(meZetaCount, oppZetaCount, meZetaCount + 1);

  var zetaDiff = oppZetaCount - meZetaCount;

  let effect = scaleScoreByRatio(zetaRatio, gacRiskData.riskFactors[RISK_FACTORS.zetaCount].threshold, gacRiskData.riskFactors[RISK_FACTORS.zetaCount].effect);
  riskResult.score += effect;

  if (zetaRatio < 0.6) {
    riskResult.factors += `Zetas: Opponent has ridiculously more (${zetaDiff}, risk ${effect}).\n`;
  }
  else if (zetaRatio < 0.8) {
    riskResult.factors += `Zetas: Opponent has many more (${zetaDiff}, risk ${effect}).\n`;
  }
  else if (zetaRatio < 0.9) {
    riskResult.factors += `Zetas: Opponent has notably more (${zetaDiff}, risk ${effect}).\n`;
  }
  else if (zetaRatio > 1.4) {
    riskResult.factors += `Zetas: Opponent has ridiculously fewer  (${zetaDiff}, risk ${effect}).\n`;
  }
  else if (zetaRatio > 1.2) {
    riskResult.factors += `Zetas: Opponent has many fewer  (${zetaDiff}, risk ${effect}).\n`;
  }
  else if (zetaRatio > 1.1) {
    riskResult.factors += `Zetas: Opponent has notably fewer (${zetaDiff}, risk ${effect}).\n`;
  }
  else {
    riskResult.factors += `Zetas: Comparable (${zetaDiff}, risk ${effect})\n`;
  }
}

// Risk based on 6 dot mods count
function addSixDotModsRisk(riskResult, gacRiskData, meData, opponentData) {
  let meSixDotMods = swapi.getPlayerModSummary(meData).sixDot;
  let oppSixDotMods = swapi.getPlayerModSummary(opponentData).sixDot;
  let sixDotModsRatio = calcRatio(meSixDotMods, oppSixDotMods, meSixDotMods + 1);
  var sixDotModsDiff = oppSixDotMods - meSixDotMods;

  let effect = scaleScoreByRatio(sixDotModsRatio, gacRiskData.riskFactors[RISK_FACTORS.sixDotMods].threshold, gacRiskData.riskFactors[RISK_FACTORS.sixDotMods].effect);
  riskResult.score += effect;

  if (sixDotModsRatio < 0.6) {
    riskResult.factors += `6-Dot Mods: Opponent has ridiculously more (${sixDotModsDiff}, risk ${effect}).\n`;
  }
  else if (sixDotModsRatio < 0.8) {
    riskResult.factors += `6-Dot Mods: Opponent has many more (${sixDotModsDiff}, risk ${effect}).\n`;
  }
  else if (sixDotModsRatio < 0.9) {
    riskResult.factors += `6-Dot Mods: Opponent has notably more (${sixDotModsDiff}, risk ${effect}).\n`;
  }
  else if (sixDotModsRatio > 1.4) {
    riskResult.factors += `6-Dot Mods: Opponent has ridiculously fewer (${sixDotModsDiff}, risk ${effect}).\n`;
  }
  else if (sixDotModsRatio > 1.2) {
    riskResult.factors += `6-Dot Mods: Opponent has many fewer (${sixDotModsDiff}, risk ${effect}).\n`;
  }
  else if (sixDotModsRatio > 1.1) {
    riskResult.factors += `6-Dot Mods: Opponent has notably fewer (${sixDotModsDiff}, risk ${effect}).\n`;
  }
  else {
    riskResult.factors += `6-Dot Mods: Comparable (${sixDotModsDiff}, risk ${effect})\n`;
  }
}

// Risk based on speed mods approximation
function addSpeedModsRisk(riskResult, gacRiskData, meData, opponentData) {
  let meMods = swapi.getPlayerModSummary(meData);
  let oppMods = swapi.getPlayerModSummary(opponentData);

  var meSpeed25Mods = meMods.speed25;
  var meSpeed20Mods = meMods.speed20 - meSpeed25Mods;
  var meSpeed15Mods = meMods.speed15 - meSpeed20Mods - meSpeed25Mods;
  var meSpeed10Mods = meMods.speed10 - meSpeed15Mods - meSpeed20Mods - meSpeed25Mods;

  var meSpeedModsEstimate = meSpeed25Mods * 25 + meSpeed20Mods * 20 + meSpeed15Mods * 15 + meSpeed10Mods * 10;

  var opSpeed25Mods = oppMods.speed25;
  var opSpeed20Mods = oppMods.speed20 - opSpeed25Mods;
  var opSpeed15Mods = oppMods.speed15 - opSpeed20Mods - opSpeed25Mods;
  var opSpeed10Mods = oppMods.speed10 - opSpeed15Mods - opSpeed20Mods - opSpeed25Mods;

  var opSpeedModsEstimate = opSpeed25Mods * 25 + opSpeed20Mods * 20 + opSpeed15Mods * 15 + opSpeed10Mods * 10;

  let speedModsRatio = calcRatio(meSpeedModsEstimate, opSpeedModsEstimate, meSpeedModsEstimate);

  let effect = scaleScoreByRatio(speedModsRatio, gacRiskData.riskFactors[RISK_FACTORS.speedMods].threshold, gacRiskData.riskFactors[RISK_FACTORS.speedMods].effect);
  riskResult.score += effect;

  if (speedModsRatio < 0.65) {
    riskResult.factors += `Speeds (Mods): Opponent has ridiculously higher (risk ${effect}).\n`;
  }
  else if (speedModsRatio < 0.8) {
    riskResult.factors += `Speeds (Mods): Opponent has significantly higher (risk ${effect}).\n`;
  }
  else if (speedModsRatio < 0.9) {
    riskResult.factors += `Speeds (Mods): Opponent has notably higher (risk ${effect}).\n`;
  }
  else if (speedModsRatio > 1.35) {
    riskResult.factors += `Speeds (Mods): Opponent has ridiculously lower (risk ${effect}).\n`;
  }
  else if (speedModsRatio > 1.2) {
    riskResult.factors += `Speeds (Mods): Opponent has significantly lower (risk ${effect}).\n`;
  }
  else if (speedModsRatio > 1.1) {
    riskResult.factors += `Speeds (Mods): Opponent has notably lower (risk ${effect}).\n`;
  }
  else {
    riskResult.factors += `Speeds (Mods): Comparable (risk ${effect})\n`;
  }
}

// Risk based on GL counts
function addGLRisk(riskResult, gacRiskData, meData, opponentData) {
  var meGLCount = swapi.getPlayerGalacticLegends(meData).length;
  var opGLCount = swapi.getPlayerGalacticLegends(opponentData).length;
  let glRatio = calcRatio(meGLCount, opGLCount, meGLCount + 1);

  let effect = scaleScoreByRatio(glRatio, gacRiskData.riskFactors[RISK_FACTORS.glCount].threshold, gacRiskData.riskFactors[RISK_FACTORS.glCount].effect);
  riskResult.score += effect;

  if (glRatio < 0.3) {
    riskResult.factors += `GLs: Opponent has ridiculously more (risk ${effect}).\n`;
  }
  else if (glRatio < 0.5) {
    riskResult.factors += `GLs: Opponent has significantly more (risk ${effect}).\n`;
  }
  else if (glRatio < 1) {
    riskResult.factors += `GLs: Opponent has more (risk ${effect}).\n`;
  }
  else if (glRatio > 1.7) {
    riskResult.factors += `GLs: Opponent has ridiculously fewer (risk ${effect}).\n`;
  }
  else if (glRatio > 1.5) {
    riskResult.factors += `GLs: Opponent has significantly fewer (risk ${effect}).\n`;
  }
  else if (glRatio > 1) {
    riskResult.factors += `GLs: Opponent has fewer (risk ${effect}).\n`;
  }
  else {
    riskResult.factors += `GLs: Equal\n`;
  }
}

function addRelicsRisk(riskResult, gacRiskData, meData, opponentData) {
  var opRelicsSumProd = 0;
  var meRelicsSumProd = 0;

  let meRelics = swapi.getPlayerRelicSummary(meData);
  let oppRelics = swapi.getPlayerRelicSummary(opponentData);

  for (var tier = 3; tier < oppRelics.length; tier++) {
    opRelicsSumProd += oppRelics[tier] * (tier - 2);

    meRelicsSumProd += meRelics[tier] * (tier - 2);
  }

  var relicsRatio = calcRatio(meRelicsSumProd, opRelicsSumProd, meRelicsSumProd);
  var relicsDiff = opRelicsSumProd - meRelicsSumProd;

  let effect = scaleScoreByRatio(relicsRatio, gacRiskData.riskFactors[RISK_FACTORS.relics].threshold, gacRiskData.riskFactors[RISK_FACTORS.relics].effect);
  riskResult.score += effect;

  if (relicsRatio < 0.65) {
    riskResult.factors += `Relics: Opponent has ridiculously higher relic values (${relicsDiff}, risk ${effect}).\n`;
  }
  else if (relicsRatio < 0.8) {
    riskResult.factors += `Relics: Opponent has much higher relic values (${relicsDiff}, risk ${effect}).\n`;
  }
  else if (relicsRatio < 0.9) {
    riskResult.factors += `Relics: Opponent has notably higher relic values (${relicsDiff}, risk ${effect}).\n`;
  }
  else if (relicsRatio > 1.35) {
    riskResult.factors += `Relics: Opponent has ridiculously lower relic values (${relicsDiff}, risk ${effect}).\n`;
  }
  else if (relicsRatio > 1.2) {
    riskResult.factors += `Relics: Opponent has much lower relic values (${relicsDiff}, risk ${effect}).\n`;
  }
  else if (relicsRatio > 1.1) {
    riskResult.factors += `Relics: Opponent has notably lower relic values (${relicsDiff}, risk ${effect}).\n`;
  }
  else {
    riskResult.factors += `Relics: Comparable (${relicsDiff}, risk ${effect})\n`;
  }
}

function addGearRisk(riskResult, gacRiskData, meData, opponentData) {

  let meGear = swapi.getPlayerGearSummary(meData);
  let oppGear = swapi.getPlayerGearSummary(opponentData);

  let meGearRating = meGear[13] * 20 + meGear[12] * 10 + meGear[11] * 5;
  let oppGearRating = oppGear[13] * 20 + oppGear[12] * 10 + oppGear[11] * 5;

  let gearRatio = calcRatio(meGearRating, oppGearRating, meGearRating);

  let effect = scaleScoreByRatio(gearRatio, gacRiskData.riskFactors[RISK_FACTORS.gear].threshold, gacRiskData.riskFactors[RISK_FACTORS.gear].effect);
  riskResult.score += effect;

  if (gearRatio < 0.65) {
    riskResult.factors += `Gear: Opponent has ridiculously better gear (risk ${effect}).\n`;
  }
  else if (gearRatio < 0.8) {
    riskResult.factors += `Gear: Opponent has much better gear (risk ${effect}).\n`;
  }
  else if (gearRatio < 0.9) {
    riskResult.factors += `Gear: Opponent has notably better gear (risk ${effect}).\n`;
  }
  else if (gearRatio > 1.35) {
    riskResult.factors += `Gear: Opponent has ridiculously worse gear (risk ${effect}).\n`;
  }
  else if (gearRatio > 1.2) {
    riskResult.factors += `Gear: Opponent has much worse gear (risk ${effect}).\n`;
  }
  else if (gearRatio > 1.1) {
    riskResult.factors += `Gear: Opponent has notably worse gear (risk ${effect}).\n`;
  }
  else {
    riskResult.factors += `Gear: Comparable (risk ${effect})\n`;
  }
}

const dc9Scale = 20, dc6Scale = 10, dc3Scale = 5;
// Risk based on speed mods approximation
function addDatacronRisk(riskResult, gacRiskData, meData, opponentData) {
  let meDCs = swapi.getPlayerDatacrons(meData);
  let oppDCs = swapi.getPlayerDatacrons(opponentData);

  let meLevel9DCsCount = meDCs.filter(dc => dc.affix.length == 9).length;
  let meLevel6DCsCount = meDCs.filter(dc => dc.affix.length >= 6 && dc.affix.length < 9).length;
  let meLevel3DCsCount = meDCs.filter(dc => dc.affix.length >= 3 && dc.affix.length < 6).length;

  let oppLevel9DCsCount = oppDCs.filter(dc => dc.affix.length == 9).length;
  let oppLevel6DCsCount = oppDCs.filter(dc => dc.affix.length >= 6 && dc.affix.length < 9).length;
  let oppLevel3DCsCount = oppDCs.filter(dc => dc.affix.length >= 3 && dc.affix.length < 6).length;

  let meDCValueEstimate = meLevel9DCsCount * dc9Scale + meLevel6DCsCount * dc6Scale + meLevel3DCsCount * dc3Scale;
  let oppDCValueEstimate = oppLevel9DCsCount * dc9Scale + oppLevel6DCsCount * dc6Scale + oppLevel3DCsCount * dc3Scale;

  let dcRatio = calcRatio(meDCValueEstimate, oppDCValueEstimate, meDCValueEstimate);

  let effect = scaleScoreByRatio(dcRatio, gacRiskData.riskFactors[RISK_FACTORS.datacrons].threshold, gacRiskData.riskFactors[RISK_FACTORS.datacrons].effect);
  riskResult.score += effect;

  if (dcRatio < 0.65) {
    riskResult.factors += `Datacrons: Opponent has ridiculously better (risk ${effect}).\n`;
  }
  else if (dcRatio < 0.8) {
    riskResult.factors += `Datacrons: Opponent has significantly better (risk ${effect}).\n`;
  }
  else if (dcRatio < 0.9) {
    riskResult.factors += `Datacrons: Opponent has notably better (risk ${effect}).\n`;
  }
  else if (dcRatio > 1.35) {
    riskResult.factors += `Datacrons: Opponent has ridiculously worse (risk ${effect}).\n`;
  }
  else if (dcRatio > 1.2) {
    riskResult.factors += `Datacrons: Opponent has significantly worse (risk ${effect}).\n`;
  }
  else if (dcRatio > 1.1) {
    riskResult.factors += `Datacrons: Opponent has notably worse (risk ${effect}).\n`;
  }
  else {
    riskResult.factors += `Datacrons: Comparable (risk ${effect})\n`;
  }
}