const { EmbedBuilder } = require("discord.js");
const playerUtils = require("../../../utils/player");
const swapi = require("../../../utils/swapi");
const calcUtils = require ("../calcUtils")

module.exports = {
    data: 
    { 
        name: "shaak-it-vs-lv",
        label: "Can Shaak with IT take out Lord Vader?"
    },
    doCalc: function(me, opp) {
        let embed = new EmbedBuilder()
            .setTitle("Can Shaak be used with IT to take out Lord Vader?");

        let opMessageResponse = this.getMessage(swapi.getPlayerRoster(opp));
        let meMessageResponse = this.getMessage(swapi.getPlayerRoster(me));

        embed.addFields(
            {name: `${swapi.getPlayerName(me)}`, value: meMessageResponse.message},
            {name: `${swapi.getPlayerName(opp)}`, value: opMessageResponse.message}
        )

        embed.setColor(opMessageResponse.color);

        return embed;

    },
    getMessage: function(roster)
    {
        const SHAAK_DEF_ID = "SHAAKTI";
        const PIETT_DEF_ID = "ADMIRALPIETT";
      
        let shaak = roster.find(unit => swapi.getUnitDefId(unit) == SHAAK_DEF_ID);
        let piett = roster.find(unit => swapi.getUnitDefId(unit) == PIETT_DEF_ID);
      
        let shaakSpeed = swapi.getUnitStat(shaak, swapi.STATS.SPEED);
        let piettSpeed = swapi.getUnitStat(piett, swapi.STATS.SPEED);
      
        let fastEnough = shaakSpeed >= piettSpeed + 21;
        let message = 
`${fastEnough ? "Shaak+Piett **can** counter LV." : "Shaak is too slow to counter LV."}

Shaak: ${shaakSpeed}
Piett: ${piettSpeed}`
      
        let color = calcUtils.RESULT_COLORS.GOOD;
        if (fastEnough) color = calcUtils.RESULT_COLORS.BAD;

        return { message, color };
    }
}