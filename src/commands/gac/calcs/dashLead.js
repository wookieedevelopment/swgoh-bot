const { EmbedBuilder } = require("discord.js");
const playerUtils = require("../../../utils/player");
const swapi = require("../../../utils/swapi");
const calcUtils = require ("../calcUtils")

module.exports = {
    data: 
    { 
        name: "dash",
        label: "Dash Speed with Dash Lead"
    },
    doCalc: function(me, opp) {
        let embed = new EmbedBuilder()
            .setTitle("Dash Speed with Dash Lead");

        let meMessage = this.getDashSpeedMessage(swapi.getPlayerRoster(me));
        let opMessage = this.getDashSpeedMessage(swapi.getPlayerRoster(opp));

        embed.addFields(
            {name: `${swapi.getPlayerName(me)}`, value: meMessage},
            {name: `${swapi.getPlayerName(opp)}`, value: opMessage}
        )

        embed.setFooter({ text: "Formula: Dash Speed + 20" });

        embed.setColor(calcUtils.RESULT_COLORS.NEUTRAL);

        return embed;

    },
    getDashSpeedMessage: function(roster)
    {
        let dash = roster.find( unit => swapi.getUnitDefId(unit) == "DASHRENDAR" );
        
        if (!dash) return "Dash not activated.";
       
        let speed = swapi.getUnitStat(dash, swapi.STATS.SPEED);
        let displayText = `Dash Speed: ${speed+20}`
        return displayText;
    }
}