const { EmbedBuilder } = require("discord.js");
const playerUtils = require("../../../utils/player");
const swapi = require("../../../utils/swapi");
const calcUtils = require ("../calcUtils")

module.exports = {
    data: 
    { 
        name: "kyle-katarn",
        label: "Kyle Katarn Speed with Rebel Fighters"
    },
    doCalc: function(me, opp) {
        let embed = new EmbedBuilder()
            .setTitle("Kyle Katarn with Rebel Fighters");

        let meMessage = this.getKKSpeedMessage(swapi.getPlayerRoster(me));
        let opMessage = this.getKKSpeedMessage(swapi.getPlayerRoster(opp));

        embed.addFields(
            {name: `${swapi.getPlayerName(me)}`, value: meMessage},
            {name: `${swapi.getPlayerName(opp)}`, value: opMessage}
        )

        embed.setFooter({ text: "Formula: Kyle Katan Speed / 0.7" });

        embed.setColor(calcUtils.RESULT_COLORS.NEUTRAL);

        return embed;

    },
    getKKSpeedMessage: function(roster)
    {
        let kyleKatarn = roster.find( unit => swapi.getUnitDefId(unit) == "KYLEKATARN" );
        
        if (!kyleKatarn) return "Kyle Katarn not activated.";
        
        let speed = swapi.getUnitStat(kyleKatarn, swapi.STATS.SPEED); 
        let displayText = `Kyle Katarn Turn 1 Effective Speed: ${+(Math.round((speed / 0.7) + "e+2")  + "e-2")}`;
        return displayText;
    }
}