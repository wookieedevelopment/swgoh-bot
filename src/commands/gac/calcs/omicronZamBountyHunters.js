const { EmbedBuilder } = require("discord.js");
const playerUtils = require("../../../utils/player");
const swapi = require("../../../utils/swapi");
const calcUtils = require ("../calcUtils")

module.exports = {
    data: 
    { 
        name: "zam" ,
        label: "BH Speeds with Omicron Zam"
    },
    doCalc: async function(me, opp) {
        let embed = new EmbedBuilder()
            .setTitle("BH Speeds with Omicron Zam");

        let meMessage = await this.getBHSpeedMessage(swapi.getPlayerRoster(me));
        let opMessage = await this.getBHSpeedMessage(swapi.getPlayerRoster(opp));

        embed.addFields(
            {name: `${swapi.getPlayerName(me)}`, value: meMessage},
            {name: `${swapi.getPlayerName(opp)}`, value: opMessage}
        )

        embed.setFooter({ text: "Speed boost: ⌊⌊zam*1.2⌋/5⌋" });

        embed.setColor(calcUtils.RESULT_COLORS.NEUTRAL);

        return embed;

    },
    getBHSpeedMessage: async function(roster)
    {
        const ZAM_DEF_ID = "ZAMWESELL";
        const BOSSK_DEF_ID = "BOSSK";
        const GREEF_DEF_ID = "GREEFKARGA";
        const ZAM_OMI_SKILL_ID = "uniqueskill_ZAMWESELL01";
        let zam = roster.find(unit => swapi.getUnitDefId(unit) == ZAM_DEF_ID);

        let zamOmiAbilities = await swapi.getUnitOmicronAbilities(zam);
        let hasOmi = zamOmiAbilities?.find(a => a.base_id == ZAM_OMI_SKILL_ID);
        
        // if they don't have the omi, then exit
        if (!hasOmi) {
            return "No Omicron on Zam.";
        }

        let bossk = roster.find(unit => swapi.getUnitDefId(unit) == BOSSK_DEF_ID);
        let greef = roster.find(unit => swapi.getUnitDefId(unit) == GREEF_DEF_ID);

        let zamSpeed = swapi.getUnitStat(zam, swapi.STATS.SPEED);
        let bosskSpeed = swapi.getUnitStat(bossk, swapi.STATS.SPEED);
        let greefSpeed = swapi.getUnitStat(greef, swapi.STATS.SPEED);

        let zamNewSpeed = Math.floor(zamSpeed * 1.2);
        let speedBoost = Math.floor(zamNewSpeed / 5);

        let displayText = `Zam Speed: ${zamSpeed} => ${zamNewSpeed}
        Bossk Speed: ${bosskSpeed} => ${bosskSpeed + speedBoost}
        Greef Speed: ${greefSpeed} => ${greefSpeed + speedBoost}`;

        return displayText;
    }
}