const { EmbedBuilder } = require("discord.js");
const swapi = require("../../../utils/swapi");
const calcUtils = require ("../calcUtils")
const Gradient = require("javascript-color-gradient");
const riskGradient = new Gradient().setColorGradient("#008800", "#880000").setMidpoint(20);



module.exports = {
    data: 
    { 
        name: "risk",
        label: ">> Overall Risk Comparison <<"
    },
    doCalc: function(me, opp) {
        let embed = new EmbedBuilder()
            .setTitle("Risk Comparison");
        let riskResult = calcUtils.calculateRisk(me, opp);

        embed.setDescription(
`Risk: ${riskResult.score}

${riskResult.factors}`)

        embed.setColor(riskGradient.getColor(Math.max(1, Math.floor(riskResult.score / 5.0))));

        return embed;

    },
    getDashSpeedMessage: function(roster)
    {
        let dash = roster.find( unit => swapi.getUnitDefId(unit) == "DASHRENDAR" );
        
        if (!dash) return "Dash not activated.";
       
        let speed = swapi.getUnitStat(dash, swapi.STATS.SPEED);
        let displayText = `Dash Speed: ${speed+20}`
        return displayText;
    }
}