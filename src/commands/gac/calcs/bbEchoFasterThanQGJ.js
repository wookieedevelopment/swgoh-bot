const { EmbedBuilder } = require("discord.js");
const playerUtils = require("../../../utils/player");
const swapi = require("../../../utils/swapi");
const discordUtils = require ("../../../utils/discord");
const calcUtils = require ("../calcUtils")

module.exports = {
    data: 
    { 
        name: "echo-qgj",
        label: "Will BB Echo go before enemy QGJ team?"
    },
    doCalc: function(me, opp) {
        let embed = new EmbedBuilder()
            .setTitle("Will BB Echo go before enemy QGJ team?");

        let meMessageResponse = this.getMessage(swapi.getPlayerRoster(me), swapi.getPlayerRoster(opp));
        let opMessageResponse = this.getMessage(swapi.getPlayerRoster(opp), swapi.getPlayerRoster(me));

        embed.addFields(
            {name: `${swapi.getPlayerName(me)}${discordUtils.LTR} attacking ${swapi.getPlayerName(opp)}${discordUtils.LTR}`, value: meMessageResponse.message},
            {name: `${swapi.getPlayerName(opp)}${discordUtils.LTR} attacking ${swapi.getPlayerName(me)}${discordUtils.LTR}`, value: opMessageResponse.message}
        );

        embed.setColor(meMessageResponse.color);

        return embed;

    },
    getMessage: function(meRoster, opRoster)
    {
        const ECHO_DEF_ID = "BADBATCHECHO";
        const KAM_DEF_ID = "KIADIMUNDI";
        const JKA_DEF_ID = "ANAKINKNIGHT";
        const QGJ_DEF_ID = "QUIGONJINN";

        let message, color;
        let echo = meRoster.find(unit => swapi.getUnitDefId(unit) == ECHO_DEF_ID);
        
        if (!echo)
        {
            message = "Bad Batch Echo not activated. This will not work.";
            color = calcUtils.RESULT_COLORS.BAD;
            return { message, color };
        }
        
        let echoSpeed = swapi.getUnitStat(echo, swapi.STATS.SPEED);

        let jka = opRoster.find(unit => swapi.getUnitDefId(unit) == JKA_DEF_ID);

        if (!jka)
        {
            message = "Opponent does not have JKA.";
            color = calcUtils.RESULT_COLORS.GOOD;
            return { message, color };
        }

        let qgj = opRoster.find(unit => swapi.getUnitDefId(unit) == QGJ_DEF_ID);
        let qgjOmis = swapi.getUnitOmicronAbilities(qgj);

        if (!qgjOmis || qgjOmis.length === 0)
        {
            message = "Opponent does not have QGJ lead omicron.";
            color = calcUtils.RESULT_COLORS.GOOD;
            return { message, color };
        }

        let qgjSpeed = swapi.getUnitStat(qgj, swapi.STATS.SPEED);
        let fasterThanQgj = (echoSpeed > qgjSpeed + 60);

        let kam = opRoster.find(unit => swapi.getUnitDefId(unit) == KAM_DEF_ID);      

        let fasterThanKam = null, kamSpeed = 0;
        if (kam)
        {
            kamSpeed = swapi.getUnitStat(kam, swapi.STATS.SPEED);
            fasterThanKam = (echoSpeed > kamSpeed + 80);
        }

        let jkaSpeed = swapi.getUnitStat(jka, swapi.STATS.SPEED);
        let fasterThanJka = (echoSpeed > jkaSpeed + 60);

        let notHaveString = "";
        let checksString = "";
        let speedsSummaryString = "";

        if (!kam)
        {
            notHaveString = "Opponent does not have KAM.";
        }

        if (fasterThanQgj) checksString += `${discordUtils.symbols.pass} Echo will go before QGJ.\n`
        else checksString += `${discordUtils.symbols.fail} Echo will go after QGJ.\n`;

        if (fasterThanJka) checksString += `${discordUtils.symbols.pass} Echo will go before JKA.\n`
        else checksString += `${discordUtils.symbols.fail} Echo will go after JKA.\n`;

        if (fasterThanKam) checksString += `${discordUtils.symbols.pass} Echo will go before KAM.\n`
        else if (fasterThanKam === false) checksString += `${discordUtils.symbols.fail} Echo will go after KAM.\n`;

        speedsSummaryString = 
`Echo: ${echoSpeed}
QGJ: ${qgjSpeed}
JKA: ${jkaSpeed}
${kam ? `KAM: ${kamSpeed}` : ""}`;

        message = 
`${notHaveString}
${checksString}
${speedsSummaryString}`;

        if (fasterThanJka && fasterThanKam && fasterThanQgj) color = calcUtils.RESULT_COLORS.GOOD;
        else if (fasterThanJka) color = calcUtils.RESULT_COLORS.NEUTRAL;
        else color = calcUtils.RESULT_COLORS.BAD;

        return { message, color };
    }
}