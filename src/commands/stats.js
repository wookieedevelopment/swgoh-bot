const discordUtils = require ("../utils/discord")
const { SlashCommandBuilder } = require('@discordjs/builders');
const { ActionRowBuilder, ButtonBuilder, EmbedBuilder, ButtonStyle, Embed } = require('discord.js');
const database = require("../database");
const queueUtils = require("../utils/queue");

module.exports = {
    data: new SlashCommandBuilder()
        .setName("stats")
        .setDescription("See WookieeBot stats"),
    interact: async function(interaction)
    {
        let statsData = await database.db.multiResult(`
            SELECT COUNT(*) FROM guild_players;
            SELECT COUNT(*) FROM guild;
            SELECT COUNT(*) FROM tb_combat_team;
            SELECT COUNT(*) FROM raid_team;
            SELECT timestamp FROM player_data ORDER BY timestamp ASC LIMIT 1;
            SELECT COUNT(*) FROM cheating_queries;
            SELECT COUNT(*) FROM user_registration;
        `);

        let oldestDataDays = Math.ceil(Math.abs(new Date() - statsData[4].rows[0].timestamp) / (3600_000*24));
        let oldestDataYears = Math.floor(oldestDataDays / 365);
        let remainderDays = oldestDataDays % 365;
        
        let queueSizes = await queueUtils.getQueueSizes();
        let queueNames = Object.keys(queueSizes);
        let queuesString = "> " + queueNames.map(qn => `${qn}: ${queueSizes[qn]}`).join("\n> ");

        let totalServers = (await interaction.client.shard.fetchClientValues('guilds.cache.size')).reduce((acc, guildCount) => acc + guildCount, 0);
        let embed = new EmbedBuilder()
            .setTitle("WookieeBot Stats")
            .setColor("#995533")
            .setDescription(
`Discord Servers: ${totalServers.toLocaleString('en')}
Guilds Tracked: ${parseInt(statsData[1].rows[0].count).toLocaleString('en')}
Players Tracked: ${parseInt(statsData[0].rows[0].count).toLocaleString('en')}
Registered Players: ${parseInt(statsData[6].rows[0].count).toLocaleString('en')}
Earliest Player Data Age: ${oldestDataYears}y ${remainderDays}d
Cheat checks run: ${parseInt(statsData[5].rows[0].count).toLocaleString('en')}
TB Teams Known: ${parseInt(statsData[2].rows[0].count).toLocaleString('en')}
Raid Teams Known: ${parseInt(statsData[3].rows[0].count).toLocaleString('en')}

Queue Sizes:
${queuesString}`
            )

        await interaction.editReply({ embeds: [embed] });
    }
}