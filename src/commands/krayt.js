const kraytReady = require("./krayt/ready");
const kraytMax = require("./krayt/max");
const kraytGuild = require("./krayt/guild");
const { SlashCommandBuilder } = require('@discordjs/builders');

module.exports = {
    data: new SlashCommandBuilder()
        .setName("krayt")
        .setDescription("Krayt Dragon Raid Tools")
        .addSubcommand(kraytReady.data)
        .addSubcommand(kraytMax.data)
        .addSubcommand(kraytGuild.data),
    interact: async function(interaction)
    {
        let subcommand = interaction.options.getSubcommand();
        switch (subcommand)
        {
            case kraytReady.data.name:
                await kraytReady.interact(interaction);
                break;
            case kraytMax.data.name:
                await kraytMax.interact(interaction);
                break;
            case kraytGuild.data.name:
                await kraytGuild.interact(interaction);
                break;
        }
    },
    handleButton: async function(interaction)
    {  
        if (interaction.customId.startsWith("kr:g:"))
            await kraytGuild.handleButton(interaction);
    },
    onSelect: async function(interaction)
    {
        if (interaction.customId.startsWith("kr:g:"))
            await kraytGuild.onSelect(interaction);
    }
}