const opsCommon = require ("../common");
const loadOps = require("./loadOps");

module.exports = {
    toggleRareOnly: async function(interaction, comlinkGuildId, phase)
    {
        let plan = await opsCommon.getGuildTbPlanData(comlinkGuildId);
        let phaseData = plan[phase];
        let newRareOnly = phaseData[0].rareOnly ? false : true;
        for (let planetDataIx = 0; planetDataIx < phaseData.length; planetDataIx++)
        {
            phaseData[planetDataIx].rareOnly = newRareOnly;
        }

        await opsCommon.saveGuildTbPlanData(comlinkGuildId, { plan: plan });

        let response = await loadOps.load(interaction, opsCommon.OPERATION_VIEWS.PLANET, comlinkGuildId, phase);

        return response;
    }
}