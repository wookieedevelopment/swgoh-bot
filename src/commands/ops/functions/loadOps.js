
const { ActionRowBuilder, ButtonBuilder, StringSelectMenuBuilder, ButtonStyle, EmbedBuilder, Embed } = require("discord.js");
const opsCommon = require ("../common");
const tbCommon = require ("../../tb/common");
const guildUtils = require("../../../utils/guild");
const discordUtils = require("../../../utils/discord");


module.exports = {
    load: async function(interaction, view, comlinkGuildId, phase)
    {
        if (interaction.editReply)
        {
            await interaction.editReply({ content: "Loading assignments, please wait...", files: [], embeds: [], components: [] });
        }
        
        let opsConfig = await opsCommon.loadGuildTbConfig(comlinkGuildId);

        let opsData = await opsCommon.getOpsData(comlinkGuildId);
        if (phase == null) phase = parseInt(interaction.values[0]);
        
        let response;

        if (view === opsCommon.OPERATION_VIEWS.PLANET)
            response = await opsCommon.getShowAssignmentsByPlanetResponse(opsData, phase, { rareThreshold: opsConfig.rare_threshold, rareByDefault: opsConfig.rare_by_default });
        else
            response = await opsCommon.getShowAssignmentsByPlayerResponse(opsData, phase, { rareThreshold: opsConfig.rare_threshold, rareByDefault: opsConfig.rare_by_default, playersShownPerImage: opsConfig.players_per_image });

        let playersExcludedFromPhase = opsConfig.players?.filter(p => p.phase == phase && !p.available);

        let excludedEmbed = await getPlayersExcludedFromPhaseEmbed(comlinkGuildId, playersExcludedFromPhase);
        if (excludedEmbed) response.embeds.push(excludedEmbed);

        let warningsEmbed = generateShowAssignmentsWarningsEmbed(opsData, phase, playersExcludedFromPhase);
        if (warningsEmbed) response.embeds.push(warningsEmbed);
        
        response.components = generateComponents(comlinkGuildId, phase, opsData, view);

        return response;
    }
}

function generateShowAssignmentsWarningsEmbed(opsData, phaseToShow, playersExcludedFromPhase)
{
    let warningsList = new Array();
    let phaseAssignmentsData = opsData.phases[phaseToShow-1];

    let excludedWithAssignments = phaseAssignmentsData.playerAssignments
        .filter(pa => pa.assignedReqs.length > 0 && playersExcludedFromPhase.find(x => x.allycode === pa.allycode));

    if (excludedWithAssignments.length > 0)
    {
        warningsList.push("Excluded players have assignments. Regenerate or reassign.");
    }

    if (warningsList.length > 0)
    {
        let warningsEmbed = new EmbedBuilder()
            .setTitle(discordUtils.symbols.warning + " Warnings")
            .setDescription("- " + warningsList.join("\n- "))

        return warningsEmbed;
    }

    return null;
}

async function getPlayersExcludedFromPhaseEmbed(comlinkGuildId, playersExcludedFromPhase)
{
    if (playersExcludedFromPhase?.length > 0)
    {

        let guildData = await guildUtils.getGuildDataFromAPI({ comlinkGuildId: comlinkGuildId }, { useCache: true });
        let matchingPlayerText = guildUtils.
                                    getGuildRoster(guildData).
                                    filter(m => playersExcludedFromPhase.find(p => p.allycode === m.allyCode)).
                                    map(m => `${m.name} (${m.allyCode})`);

        matchingPlayerText.sort();
        let value = "- " + matchingPlayerText.join("\n- "); 

        let excludedPlayersEmbed = new EmbedBuilder()
            .setTitle("Players NOT Available")
            .setDescription(value);
            
        return excludedPlayersEmbed;
    }
}

function generateComponents(comlinkGuildId, phase, opsData, curView)
{
    let phaseSelectRow = new ActionRowBuilder()
        .addComponents(
            new StringSelectMenuBuilder()
                .setCustomId(`ops:m:load:${comlinkGuildId}`)
                .setPlaceholder("Load assignments by Phase")
                .addOptions(tbCommon.TB_PHASES.map(p => { return { label: p.label, value: p.n.toString() }; }))
        );

    let adjustPhase = new ActionRowBuilder()
            .addComponents(
                new StringSelectMenuBuilder()
                    .setCustomId(`ops:m:adjust:${phase}${comlinkGuildId}`)
                    .setPlaceholder("Adjust Ops for Planet...")
                    .addOptions(opsData.phases[phase-1].planets.map(p => { return { label: tbCommon.getLabelForPlanet(p.planet), value: p.planet.name }}))
            );

        let buttonRow = new ActionRowBuilder()
            .addComponents(
                new ButtonBuilder()
                    .setCustomId(`ops:m:rare:${phase}${comlinkGuildId}`)
                    .setLabel("Toggle Rare-Only for Phase")
                    .setStyle(ButtonStyle.Success),
                new ButtonBuilder()
                    .setCustomId(`ops:m:genphase:${phase}${comlinkGuildId}`)
                    .setLabel("Regenerate Assignments")
                    .setStyle(ButtonStyle.Success)
            );

    let buttonRow2 = new ActionRowBuilder();

    if (curView === opsCommon.OPERATION_VIEWS.PLANET)
    {
        buttonRow2.addComponents(
           new ButtonBuilder()
            .setCustomId(`ops:m:load-player:${phase}${comlinkGuildId}`)
            .setLabel("Player View")
            .setStyle(ButtonStyle.Primary));
    } else {
        buttonRow2.addComponents(
           new ButtonBuilder()
            .setCustomId(`ops:m:load-planet:${phase}${comlinkGuildId}`)
            .setLabel("Planet View")
            .setStyle(ButtonStyle.Primary));
    }

    buttonRow2.addComponents(
        new ButtonBuilder()
            .setCustomId(`ops:m:export:${phase}${comlinkGuildId}`)
            .setLabel("Export Assignments")
            .setStyle(ButtonStyle.Primary),
        new ButtonBuilder()
            .setCustomId(`ops:m:send:p${phase}${comlinkGuildId}`)
            .setLabel("Send Assignments to Guild")
            .setStyle(ButtonStyle.Primary),
        new ButtonBuilder()
            .setCustomId(`ops:m`)
            .setLabel("Back")
            .setStyle(ButtonStyle.Secondary)
    );

    let components = [phaseSelectRow, adjustPhase, buttonRow, buttonRow2];
    return components;
}