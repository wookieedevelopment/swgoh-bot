

const opsCommon = require ("../common");
const tbCommon = require("../../tb/common");
const database = require("../../../database");
const queueUtils = require ("../../../utils/queue");
const drawingUtils = require ("../../../utils/drawing");
const discordUtils = require ("../../../utils/discord");
const { EmbedBuilder, AttachmentBuilder, ActionRowBuilder, ButtonBuilder, ButtonStyle, MessageFlags } = require("discord.js");
const { createErrorEmbed } = require("../../../utils/discord");
const { logger, formatError } = require("../../../utils/log");
const { CLIENT } = require("../../../utils/discordClient");

module.exports = {
    send: send,
    handleButton: handleButton,
    createAssignmentMessage: createAssignmentMessage
}

async function send(interaction, comlinkGuildId, phase)
{
    // check if there are any unregistered players
    // prompt for confirmation to send
    // load assignments for the phase
    // generate messages for each player
    // queue messages for sending

    const opsData = await opsCommon.getOpsData(comlinkGuildId);
    const phaseData = opsData.phases[phase-1];
    
    if (phaseData.playerAssignments.length === 0)
    {
        await interaction.followUp({ content: `No assignments for Phase ${phase}.` });
        return;
    }
        
    phaseData.playerAssignments.sort((a, b) => a.name?.localeCompare(b.name));
    
    const allycodeUserIdMap = await database.db.any("SELECT allycode,discord_id FROM user_registration WHERE allycode IN ($1:csv)", [phaseData.playerAssignments.map(pa => pa.allycode)]);
    let missingUsersMessage;
    if (allycodeUserIdMap.length !== phaseData.playerAssignments.length)
    {
        // some users aren't registered. Tell admin they need to register
        // the users before sending messages
        const missingUsers = phaseData.playerAssignments.filter(pa => !allycodeUserIdMap.find(a => a.allycode === pa.allycode));
        
        missingUsersMessage = 
`The following players are not registered and will not receive assignments:
- ${missingUsers.map(m => `${m.name} (${m.allycode})`).join("\n- ")}

An officer can register members with \`/guild users register\`, or a player can register themself with \`/register\`.\n\n`;
    }

    const yesNoButtonsRow = new ActionRowBuilder()
        .addComponents(
            new ButtonBuilder().setCustomId(`ops:m:send:y${phase}${comlinkGuildId}`).setLabel("Yes").setStyle(ButtonStyle.Success),
            new ButtonBuilder().setCustomId(`ops:m:send:n${phase}${comlinkGuildId}`).setLabel("No").setStyle(ButtonStyle.Danger)
        );
    await interaction.followUp(
        { 
            ephemeral: true,
            content: `${missingUsersMessage ? missingUsersMessage : "" }Are you sure you wish to send assignments for Phase ${phase} to your guild?`, 
            components: [yesNoButtonsRow], 
            files: [], 
            embeds: [],
            fetchReply: true
    });
}


async function handleButton(interaction, data)
{
    const comlinkGuildId = data.substring(2);
    const phase = Number.parseInt(data.charAt(1));
    const subcommand = data.charAt(0);
    if (subcommand === "y")
    {   
        const qsm = await queueUtils.getQueueSizeMessage(queueUtils.QUEUES.DMS, interaction.client.shard);
        const editMessage = await interaction.followUp({ content: `Processing Send Operation Assignments request... ${qsm}`, components: [], files: [], embeds: [] } );
        await interaction.deleteReply();

        // if configured, post to channel     
        const opsConfig = await opsCommon.loadGuildTbConfig(comlinkGuildId);
        if (opsConfig.post_assignments_on_send != opsCommon.POST_ASSIGNMENTS_ON_SEND_ENUM.NONE && opsConfig.assignments_channel) {
            let channel;
            try
            {
                channel = CLIENT.channels.cache.get(opsConfig.assignments_channel);

                // if configured for planet or both
                if (opsConfig.post_assignments_on_send == opsCommon.POST_ASSIGNMENTS_ON_SEND_ENUM.PLANET ||
                    opsConfig.post_assignments_on_send == opsCommon.POST_ASSIGNMENTS_ON_SEND_ENUM.BOTH)
                {
                    const byPlanetMessage = await opsCommon.createAssignmentsGraphicMessageToPost(comlinkGuildId, phase, opsCommon.OPERATION_VIEWS.PLANET, opsConfig);
                    await channel.send(byPlanetMessage);
                }
                
                // if configure for player or both
                if (opsConfig.post_assignments_on_send == opsCommon.POST_ASSIGNMENTS_ON_SEND_ENUM.PLAYER ||
                    opsConfig.post_assignments_on_send == opsCommon.POST_ASSIGNMENTS_ON_SEND_ENUM.BOTH)
                {
                    const byPlayerMessage = await opsCommon.createAssignmentsGraphicMessageToPost(comlinkGuildId, phase, opsCommon.OPERATION_VIEWS.PLAYER, opsConfig);
                    await channel.send(byPlayerMessage);
                }
            } 
            catch (e) 
            {
                await interaction.followUp({ flags: MessageFlags.Ephemeral, content: `Error posting assignments graphics messages to ${discordUtils.makeChannelTaggable(channel)}:\n${e.message}` });
                logger.error(formatError(e));
            }
        }

        const opsData = await opsCommon.getOpsData(comlinkGuildId);
        const phaseData = opsData.phases[phase-1];
        const allycodeUserIdMap = await database.db.any("SELECT allycode,discord_id FROM user_registration WHERE allycode IN ($1:csv) AND discord_id IS NOT NULL", [phaseData.playerAssignments.map(pa => pa.allycode)]);

        const messagesToSend = [];
        for (const pa of phaseData.playerAssignments)
        {
            const user = allycodeUserIdMap.find(u => u.allycode === pa.allycode)?.discord_id;
            if (!user) continue; // not registered
            
            // don't include operations that were skipped
            pa.assignedReqs = pa.assignedReqs
                        .filter(r => phaseData.planets
                                        .find(p => 
                                            p.planet === r.planet 
                                            && p.operations[r.operation-1] !== opsCommon.OPERATION_PLAN_TYPE.SKIP
                                            )
                                );

            const message = await this.createAssignmentMessage(phase, pa);

            messagesToSend.push(queueUtils.createDMData(user, message));
        }

        await queueUtils.queueDMJob(interaction.client.shard, 
            {
                comlinkGuildId: comlinkGuildId,
                userId: interaction.user.id,
                messagesToSend: messagesToSend,
                channelId: editMessage.channelId ?? editMessage.channel.id,
                messageId: editMessage.id,
            },
            {
                expireInHours: 2
            });
    }
    else if (subcommand === "n")
    {
        await interaction.deleteReply();
    }
    else if (subcommand === "p")
    {
        await this.send(interaction, comlinkGuildId, phase);
    }
}

async function createAssignmentMessage(phase, playerAssignments)
{
    playerAssignments.assignedReqs.sort((a, b) => opsCommon.getSortValueForPlanet(a.planet) - opsCommon.getSortValueForPlanet(b.planet));

    const embed = new EmbedBuilder()
        .setTitle(`P${phase} Assignments: ${playerAssignments.name} (${playerAssignments.allycode})`)
        .setDescription(`You have ${playerAssignments.assignedReqs.length} assignments.`)
        .setColor("DarkBlue");

    const fields = [];
    const planetAssignments = [];

    let lastPlanet = null;
    let curPlanetAssignmentsObject = null;

    for (const r of playerAssignments.assignedReqs)
    {
        // the array of assignedReqs is sorted by planet above (DS -> MX -> LS -> Specials)
        if (lastPlanet !== r.planet)
        {
            const planetAssignmentsObject = {
                planet: r.planet,
                assignments: []
            };
            planetAssignments.push(planetAssignmentsObject);

            curPlanetAssignmentsObject = planetAssignmentsObject;
        }

        curPlanetAssignmentsObject.assignments.push(r);
        lastPlanet = r.planet;
    }

    // sort display left to right
    planetAssignments.sort((a, b) => a.planet.ltrSort - b.planet.ltrSort);
    
    for (const p of planetAssignments)
    {
        const field = {
            name: "",
            value: "",
            inline: false
        };

        field.name = `**${p.planet.name} (${p.planet.alignment})**: ${p.assignments.length} unit${p.assignments.length === 1 ? "" : "s"}`;

        p.assignments.sort((a, b) => a.operation - b.operation);
        let value = "";
        for (let aIx = 0; aIx < p.assignments.length; aIx++)
        {
            const asmt = p.assignments[aIx];
            value += `- Op ${asmt.operation} - ${asmt.unit_name}\n`
        }
        field.value = value;
        fields.push(field);
    }

    embed.addFields(fields);

    const canvas = await drawAssignmentsGraphic(planetAssignments);
    const curTime = new Date().getTime();
    const fileName = `ops-P${phase}-${curTime}.png`;
    const file = new AttachmentBuilder(canvas.toBuffer("image/png"), { name: fileName });

    embed.setImage(`attachment://${fileName}`);

    return { embeds: [embed], files: [file] };
}

const ASSIGNMENTS_GRAPHIC_DATA =
{
    maxPortraitsPerPlanet: 10,
    portraitWidth: 70,
    portraitHeight: 70,
    padding: 10,
    planetRowHeight: 80,
    planetHeaderHeight: 30,
    opBoxHeight: 20,
};

const MIN_WIDTH_BY_PLANET = {
    "Mustafar": 120,
    "Corellia": 120,
    "Coruscant": 130,
    "Geonosis": 120,
    "Felucia": 110,
    "Bracca": 100,
    "Dathomir": 120,
    "Tatooine": 120,
    "Kashyyyk": 120,
    "Haven-class Medical Station": 270,
    "Kessel": 100,
    "Lothal": 100,
    "Malachor": 120,
    "Vandor": 100,
    "Ring of Kafrene": 150,
    "Death Star": 130,
    "Hoth": 80,
    "Scarif": 100,
    "Zeffo": 90,
    "Mandalore": 130
}

async function drawAssignmentsGraphic(assignmentsByPlanet)
{
    const totalPlanets = assignmentsByPlanet.length;
    let maxPortraitsPerPlanet = 0;
    let minWidth = 0;
    
    for (let p = 0; p < assignmentsByPlanet.length; p++)
    {
        const count = assignmentsByPlanet[p].assignments.length;
        if (count > maxPortraitsPerPlanet) maxPortraitsPerPlanet = count;
        if (MIN_WIDTH_BY_PLANET[assignmentsByPlanet[p].planet.name] > minWidth) minWidth = MIN_WIDTH_BY_PLANET[assignmentsByPlanet[p].planet.name];
    }

    const width = ASSIGNMENTS_GRAPHIC_DATA.padding*2 + Math.max(minWidth, maxPortraitsPerPlanet*(ASSIGNMENTS_GRAPHIC_DATA.portraitWidth) + (maxPortraitsPerPlanet-1)*ASSIGNMENTS_GRAPHIC_DATA.padding);
    const height = totalPlanets * (ASSIGNMENTS_GRAPHIC_DATA.planetRowHeight + ASSIGNMENTS_GRAPHIC_DATA.planetHeaderHeight) + ASSIGNMENTS_GRAPHIC_DATA.padding * (totalPlanets+1);

    const { canvas, context } = drawingUtils.createBlankCanvas(width, height, "#000");

    for (let p = 0; p < assignmentsByPlanet.length; p++)
    {
        const planet = assignmentsByPlanet[p].planet;
        const assignments = assignmentsByPlanet[p].assignments;

        let y = ASSIGNMENTS_GRAPHIC_DATA.padding + p*(ASSIGNMENTS_GRAPHIC_DATA.planetRowHeight + ASSIGNMENTS_GRAPHIC_DATA.planetHeaderHeight + ASSIGNMENTS_GRAPHIC_DATA.padding);
        // write planet name
        context.font = `15pt "Noto Sans","Noto Sans SC","Noto Sans Armenian","Noto Sans Thai","Noto Sans Arabic","Noto Sans Korean"`;
        context.textAlign = "left";
        context.textBaseline = "top";
        context.fillStyle = "#fff";
        context.fillText(`${planet.name} (${planet.alignment})`,
            ASSIGNMENTS_GRAPHIC_DATA.padding,
            y,
            width - 2*ASSIGNMENTS_GRAPHIC_DATA.padding
            );

        y += ASSIGNMENTS_GRAPHIC_DATA.planetHeaderHeight;
        for (let a = 0; a < assignments.length; a++)
        {

            const unitIconImg = drawingUtils.getLoadedPortraitImage(assignments[a].unit_def_id);
            context.fillStyle = tbCommon.ALIGNMENT_COLOR_MAP[assignments[a].alignment].string;

            // draw the border
            context.fillRect(ASSIGNMENTS_GRAPHIC_DATA.padding + a*(ASSIGNMENTS_GRAPHIC_DATA.portraitWidth+10), 
                y, 
                ASSIGNMENTS_GRAPHIC_DATA.portraitWidth+2, 
                ASSIGNMENTS_GRAPHIC_DATA.portraitHeight+2);

            // draw the unit icon
            context.drawImage(unitIconImg, 
                ASSIGNMENTS_GRAPHIC_DATA.padding + a*(ASSIGNMENTS_GRAPHIC_DATA.portraitWidth + 10) + 1, 
                y+1, 
                ASSIGNMENTS_GRAPHIC_DATA.portraitWidth, ASSIGNMENTS_GRAPHIC_DATA.portraitHeight);

            // draw the box to contain the Op# text
            context.fillStyle = "rgba(0, 0, 0, 0.8)"
            context.fillRect(
                ASSIGNMENTS_GRAPHIC_DATA.padding + a*(ASSIGNMENTS_GRAPHIC_DATA.portraitWidth+10) + 1, 
                y + ASSIGNMENTS_GRAPHIC_DATA.portraitHeight - 19, 
                ASSIGNMENTS_GRAPHIC_DATA.portraitWidth, 
                ASSIGNMENTS_GRAPHIC_DATA.opBoxHeight);

            // draw the Op# text
            context.font = `12pt "Noto Sans"`;
            context.textAlign = "center";
            context.fillStyle = "#fff";
            context.textBaseline = "middle";
            context.fillText(`Op #${assignments[a].operation}`,
                ASSIGNMENTS_GRAPHIC_DATA.padding + a*(ASSIGNMENTS_GRAPHIC_DATA.portraitWidth+10) + ASSIGNMENTS_GRAPHIC_DATA.portraitWidth/2,
                y + ASSIGNMENTS_GRAPHIC_DATA.portraitHeight - ASSIGNMENTS_GRAPHIC_DATA.opBoxHeight/2
            )
        }
    }

    return canvas;
}