
const { EmbedBuilder, StringSelectMenuBuilder, ActionRowBuilder, ButtonBuilder, ButtonStyle } = require("discord.js");
const opsCommon = require ("../common");
const tbCommon = require("../../tb/common");

module.exports = {
    start: async function(interaction, comlinkGuildId)
    {
        let phase = interaction.values[0];

        let guildPlan = await opsCommon.getGuildTbPlanData(comlinkGuildId);

        return createWizardResponse(phase, guildPlan, comlinkGuildId);
    },

    set: async function(interaction, phaseAndComlinkGuildId)
    {
        let phase = phaseAndComlinkGuildId.charAt(0);
        let comlinkGuildId = phaseAndComlinkGuildId.substring(2);

        let plan = await opsCommon.getGuildTbPlanData(comlinkGuildId);

        let curRareOnly = plan[phase][0].rareOnly;
        
        // remove all special planets
        if (interaction.values[0] === "None")
        {
            plan[phase] = plan[phase].filter(p => !p.planet.specialPlanet);
        }
        else
        {
            let planets = interaction.values.map(v => {
                return {
                    rareOnly: curRareOnly, 
                    planet: tbCommon.PLANETS.find(p => p.name === v),
                    operations: new Array(6).fill(opsCommon.OPERATION_PLAN_TYPE.FILL)
                }
            });

            if (planets[0].planet.specialPlanet)
            {                
                let ixToReplace = plan[phase].findIndex(p => p.planet === planets[0].planet);

                if (ixToReplace === -1) 
                {
                    // add planet
                    planets[0].preloadStrategy = opsCommon.PRELOAD_STRATEGY[0];
                    plan[phase] = plan[phase].concat(planets); 
                }
                else 
                {
                    // remove planet
                    plan[phase].splice(ixToReplace, 1);
                }
            }
            else 
            {
                let ixToReplace = plan[phase].findIndex(p => p.planet.alignment === planets[0].planet.alignment && !p.planet.specialPlanet);
                planets[0].preloadStrategy = plan[phase][ixToReplace].preloadStrategy ?? opsCommon.PRELOAD_STRATEGY[0];
                plan[phase][ixToReplace] = planets[0];
            }
            
            await opsCommon.saveGappedOperationsForPhase(comlinkGuildId, phase, planets[0].planet);
        }

        plan[phase].sort((a, b) => { return opsCommon.getSortValueForPlanet(a.planet) - opsCommon.getSortValueForPlanet(b.planet)});
        await opsCommon.saveGuildTbPlanData(comlinkGuildId, { plan: plan });
        
        return createWizardResponse(phase, plan, comlinkGuildId);
    }    
}

function createWizardResponse(phase, guildPlan, comlinkGuildId)
{

    let response = { embeds: [], components: [], files: [], content: null };
    let embed = new EmbedBuilder()
        .setTitle(`Set Planets for Phase ${phase}`)
        .setDescription(`Current planets: \n> ${guildPlan[phase].map(p => tbCommon.getLabelForPlanet(p.planet)).join("\n> ")}`);

    response.embeds.push(embed);
    response.components = generateComponents(phase, comlinkGuildId);

    return response;
}

function generateComponents(phase, comlinkGuildId)
{
    let eligiblePlanetsForPhase = tbCommon.PLANETS.filter(p => p.sector <= phase);
    let dsPlanetSelectRow = new ActionRowBuilder()
        .addComponents(
            new StringSelectMenuBuilder()
                .setCustomId(`ops:m:wizset:${phase}D${comlinkGuildId}`)
                .setPlaceholder('Choose a Dark Side Planet')
                .setMaxValues(1)
                .addOptions(eligiblePlanetsForPhase.filter(p => p.alignment === tbCommon.ALIGNMENTS.DS && !p.specialPlanet).map(p => { return { label: tbCommon.getLabelForPlanet(p), value: p.name }; }))
        );
    let mxPlanetSelectRow = new ActionRowBuilder()
        .addComponents(
            new StringSelectMenuBuilder()
                .setCustomId(`ops:m:wizset:${phase}M${comlinkGuildId}`)
                .setPlaceholder('Choose a Mixed Planet')
                .setMaxValues(1)                
                .addOptions(eligiblePlanetsForPhase.filter(p => p.alignment === tbCommon.ALIGNMENTS.MX && !p.specialPlanet).map(p => { return { label: tbCommon.getLabelForPlanet(p), value: p.name }; }))

        );
    let lsPlanetSelectRow = new ActionRowBuilder()
        .addComponents(
            new StringSelectMenuBuilder()
                .setCustomId(`ops:m:wizset:${phase}L${comlinkGuildId}`)
                .setPlaceholder('Choose a Light Side Planet')
                .setMaxValues(1)
                .addOptions(eligiblePlanetsForPhase.filter(p => p.alignment === tbCommon.ALIGNMENTS.LS && !p.specialPlanet).map(p => { return { label: tbCommon.getLabelForPlanet(p), value: p.name }; }))
        );
    let bonusPlanetSelectRow = new ActionRowBuilder()
        .addComponents(
            new StringSelectMenuBuilder()
                .setCustomId(`ops:m:wizset:${phase}B${comlinkGuildId}`)
                .setPlaceholder('Choose any bonus planets')
                .setMinValues(0)
                .addOptions([{ label: "None", value: "None" }].concat(eligiblePlanetsForPhase.filter(p => p.specialPlanet).map(p => { return { label: tbCommon.getLabelForPlanet(p), value: p.name }; })))
        );

    let buttonRow = new ActionRowBuilder()
        .addComponents(
            new ButtonBuilder()
                .setCustomId(`ops:m`)
                .setLabel("Back")
                .setStyle(ButtonStyle.Secondary),
        );

    let components = [dsPlanetSelectRow, mxPlanetSelectRow, lsPlanetSelectRow, bonusPlanetSelectRow, buttonRow];
    return components;
}