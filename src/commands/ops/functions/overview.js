
const discordUtils = require ("../../../utils/discord");
const opsCommon = require("../common");
const tbCommon = require("../../tb/common");
const { EmbedBuilder, ActionRowBuilder, StringSelectMenuBuilder, ButtonBuilder, ButtonStyle } = require("discord.js");

module.exports = {
    overview: async function(guild)
    {
        
        // load tb ops data
        let opsData = await opsCommon.getOpsData(guild.comlinkGuildId);

        let description = "";

        for (let phase = 0; phase < opsData.phases.length; phase++)
        {
            let phaseData = opsData.phases[phase];
            if (phaseData?.missing.length > 0) 
            {
                phaseDesc = `${discordUtils.symbols.warning} Needs review`;
            }
            else if (phaseData?.opAssignments.length === 0 && 
                    phaseData.planets.find(p => p.operations.find(o => o != opsCommon.OPERATION_PLAN_TYPE.SKIP))) 
            {
                // there are no assignments and there's at least one operation that isn't skipped
                phaseDesc = `${discordUtils.symbols.fail} Need to Generate Assignments`;
            }
            else phaseDesc = `${discordUtils.symbols.ok}`;
            
            let phasePlanets = "";
            for (let planetData of phaseData.planets)
            {
                let str = "> ";
                if (phaseData.preloadPlanets.find(p => p.planet === planetData.planet))
                {
                    str += "*(Preload)* ";
                }

                phasePlanets += str + tbCommon.getLabelForPlanet(planetData.planet) + "\n";
            }
            description += `Phase ${phase + 1}: ${phaseDesc}\n${phasePlanets}\n`;
        }

        let embed = new EmbedBuilder()
            .setTitle(`${guild.guildName} Operations`)
            .setDescription(description);
        
        let response = {embeds: [embed], content: null };
        response.components = createComponentsForResponse(guild.comlinkGuildId);

        return response;
    }
}

function createComponentsForResponse(comlinkGuildId) {

    let phaseSelectRow = new ActionRowBuilder()
        .addComponents(
            new StringSelectMenuBuilder()
                .setCustomId(`ops:m:load:${comlinkGuildId}`)
                .setPlaceholder("Load Assignments by Phase")
                .addOptions(tbCommon.TB_PHASES.map(p => { return { label: p.label, value: p.n.toString() }; }))
        );

    let exportSelectRow = new ActionRowBuilder()
        .addComponents(
            new StringSelectMenuBuilder()
                .setCustomId(`ops:m:export:${comlinkGuildId}`)
                .setPlaceholder("Export Assignments to JSON")
                .addOptions(tbCommon.TB_PHASES.map(p => { return { label: p.label, value: p.n.toString() }; }))
        );

    let updatePlanetsForPhaseRow = new ActionRowBuilder()
        .addComponents(
            new StringSelectMenuBuilder()
                .setCustomId(`ops:m:wiz:${comlinkGuildId}`)
                .setPlaceholder("Update Planets for Phase ...")
                .addOptions(tbCommon.TB_PHASES.map(p => { return { label: p.label, value: p.n.toString() }; }))
        );

    let buttonRow = new ActionRowBuilder()
        .addComponents(
            new ButtonBuilder()
                .setStyle(ButtonStyle.Success)
                .setCustomId(`ops:m:genall:${comlinkGuildId}`)
                .setLabel("Regenerate Assignments")
        );

    let components = [updatePlanetsForPhaseRow, phaseSelectRow, exportSelectRow, buttonRow];
    return components;
}