

const opsCommon = require ("../common");
const tbCommon = require("../../tb/common");
const { parseAsync } = require("json2csv");
const { AttachmentBuilder, MessageFlags } = require("discord.js");

module.exports = {
    export: async function(interaction, comlinkGuildId, phase)
    {
        if (phase == null) phase = interaction.values[0];
        let phaseAssignments = await opsCommon.loadAssignmentsByPhase( { comlinkGuildId: comlinkGuildId, phase: phase });

        if (phaseAssignments.length === 0)
        {
            await interaction.followUp({ flags: MessageFlags.Ephemeral, content: `No assignments for Phase ${phase}.` });
            return;
        }

        let csvData = [];
        let reqData = await opsCommon.getAllOperationRequirements();
        
        let echobaseData = {
            "phase": "",
            "timestamp": new Date().toJSON(),
            "platoonAssignments": [

            ]
        }

        let dsPhase, mxPhase, lsPhase;

        for (let assignment of phaseAssignments)
        {
            let or = reqData.find(r => r.id === assignment.operation_req_id);

            if (!dsPhase && or.alignment === tbCommon.ALIGNMENTS.DS) dsPhase = or.sector;
            else if (!mxPhase && or.alignment === tbCommon.ALIGNMENTS.MX && or.planet != "Mandalore") mxPhase = or.sector;
            else if (!lsPhase && or.alignment === tbCommon.ALIGNMENTS.LS && or.planet != "Zeffo") lsPhase = or.sector;

            let csvDataObj = {
                "Planet": or.planet,
                "Operation": or.operation,
                "Row": or.row,
                "Col": or.slot,
                "Unit": or.unitName,
                "Player": assignment.player_name ?? null,
                "AllyCode": assignment.allycode
            };

            csvData.push(csvDataObj);

            let ebAssignment = {
                allyCode: assignment.allycode,
                unitBaseId: or.unitDefId,
                zoneId: generateGameZoneIdByReq(or),
                platoonDefinitionId: `tb3-platoon-${or.operation}`
            }

            echobaseData.platoonAssignments.push(ebAssignment);
        }
        
        echobaseData.phase = `${dsPhase ?? "x"}/${mxPhase ?? "x"}/${lsPhase ?? "x"}`;
        
        const csvParsingOptions = { defaultValue: 0 }
        let csvText = await parseAsync(csvData, csvParsingOptions);
        let buffer = Buffer.from(csvText);
        let csvAttachment = new AttachmentBuilder(buffer, { name: `rote_ops_P${phase}_${new Date().getTime()}.csv`});
        

        let jsonText = JSON.stringify(echobaseData);
        let jsonBuffer = Buffer.from(jsonText);
        let jsonAttachment = new AttachmentBuilder(jsonBuffer, { name: `wookieebot-ops-P${phase}-${new Date().toJSON()}.json`});

        await interaction.followUp({ flags: MessageFlags.Ephemeral, files: [csvAttachment, jsonAttachment] });
    }
            
}

function generateGameZoneIdByReq(req)
{
    let conflictNumber;
    switch (req.alignment)
    {
        case tbCommon.ALIGNMENTS.LS:
            conflictNumber = "01";
            break;
        case tbCommon.ALIGNMENTS.DS:
            conflictNumber = "02";
            break;
        case tbCommon.ALIGNMENTS.MX:
            conflictNumber = "03";
            break;
    }

    let isBonus = (req.planet === "Zeffo" || req.planet === "Mandalore");

    return `tb3_mixed_phase0${req.sector}_conflict${conflictNumber}${isBonus ? "_bonus" : ""}_recon01`

    // conflict01 = light
    // conflict02 = dark
    // conflict03 = mixed
}