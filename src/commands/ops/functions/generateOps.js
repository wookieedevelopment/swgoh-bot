const guildUtils = require ("../../../utils/guild");
const swapiUtils = require ("../../../utils/swapi");
const discordUtils = require("../../../utils/discord");
const constants = require("../../../utils/constants");
const opsCommon = require ("../common");
const opsOverview = require ("./overview");
const database = require("../../../database");
const tbCommon = require("../../../commands/tb/common");
const queueUtils = require ("../../../utils/queue");
const { CLIENT } = require('../../../utils/discordClient');
const { logger, formatError } = require("../../../utils/log");
const loadOps = require("./loadOps");

const MAX_ASSIGNMENTS_PER_PLAYER_PER_PLANET = 10;
const MAX_ASSIGNMENTS_PER_PLAYER = 30;
const OPERATION_PRIORITY = {
    MOST_POINTS_FIRST: 1,
    EASIEST_FIRST: 2
}

if (CLIENT.shard) queueUtils.BOSS.work(queueUtils.getQueueName(queueUtils.QUEUES.OPS, CLIENT.shard), { newJobCheckIntervalSeconds: 15 }, loadGuildDataJobHandler);

module.exports = {
    generateAll: async function(interaction, comlinkGuildId, phase)
    {
        let qsm = await queueUtils.getQueueSizeMessage(queueUtils.QUEUES.OPS, interaction.client.shard);
        const editMessage = await interaction.editReply({ content: `Processing Ops Generate request... ${qsm}`, components: [], files: [], embeds: [] } );
        
        let tbConfig = await opsCommon.loadGuildTbConfig(comlinkGuildId);

        await queueUtils.queueOpsJob(interaction.client.shard, 
        {
            fn: "generateAll",
            phase: phase,
            comlinkGuildId: comlinkGuildId,
            channelId: editMessage.channelId ?? editMessage.channel.id,
            messageId: editMessage.id,
            userId: interaction.user.id,
            planetsToUse: interaction.values,
            rareThreshold: tbConfig.rare_threshold,
            rareByDefault: tbConfig.rare_by_default,
            playersPerImage: tbConfig.players_per_image,
            priority: null,
            cacheInteractionId: interaction.id 
        },
        {
            expireInHours: 2
        });
    },
    getAssignmentsAndMissingFromReqs: getAssignmentsAndMissingFromReqs
}

async function generateForPhase(guildData, plan, previousAssignments, newAssignments, phase, { rareThreshold = null, exclude = null, priority = null })
{
    let planetReqs = await opsCommon.getFilteredReqsForPlanByPhase(plan, previousAssignments, newAssignments, phase);

    let planetStrategies = {};
    for (let planetPlan of plan[phase])
    {
        planetStrategies[planetPlan.planet.name] = 
            { 
                preloadStrategy: planetPlan.preloadStrategy, 
                operationPlans: [...planetPlan.operations],
                rareOnly: planetPlan.rareOnly,
                planet: planetPlan.planet
            };
    }


    let opsConfig = await opsCommon.loadGuildTbConfig(guildData.comlinkGuildId);

    if (!exclude) exclude = opsConfig.players.filter(p => !p.available && (p.phase == null || p.phase == phase)).map(p => p.allycode);
    if (!rareThreshold) rareThreshold = opsConfig.rareThreshold;

    let resultsObj = await getAssignmentsAndMissingFromReqs(
        planetReqs, 
        guildData,
        planetStrategies, 
        rareThreshold, 
        exclude, 
        priority,
        opsConfig.tiebreaker);
        
    await fixFillPlanTypes(guildData.comlinkGuildId, plan, phase, resultsObj);

    return resultsObj;
}

async function fixFillPlanTypes(comlinkGuildId, plan, phase, results)
{
    let changeMade = false;
    for (let planetPlan of plan[phase])
    {
        let planetInNextPhase = (phase != 6) && plan[phase+1].find(p => p.planet === planetPlan.planet);
        for (let o = 0; o < planetPlan.operations.length; o++)
        {
            if (phase === 6)
            {
                // always fine to skip
                if (planetPlan.operations[o] === opsCommon.OPERATION_PLAN_TYPE.SKIP) continue;

                let matchingOperationAssignmentDetails = results.operationAssignments.find (oa => (oa.planet === planetPlan.planet.name) && (oa.operation === o+1));
                let notFilledAssignment = matchingOperationAssignmentDetails?.reqs.find(r => !r.assignment || r.assignment.allycode == opsCommon.PRELOAD_ALLYCODE);

                // no preloading in P6
                if (planetPlan.operations[o] === opsCommon.OPERATION_PLAN_TYPE.PRELOAD)
                {
                    // check if all assignments are filled 
                    // if so, then change to .FILL
                    // if not, change to .SKIP
                    if (notFilledAssignment)
                    {
                        planetPlan.operations[o] = opsCommon.OPERATION_PLAN_TYPE.SKIP;
                        changeMade = true;
                    }
                    else
                    {
                        planetPlan.operations[o] = opsCommon.OPERATION_PLAN_TYPE.FILL;
                        changeMade = true;
                    }
                }

                // decided not to automatically change FILL to SKIP for operations that cannot be completed, since
                // maybe someone is planning to upgrade a unit before the phase

                continue;
            }

            // check if planet is in the next phase and operation is not set to preload
            if (planetInNextPhase && (planetPlan.operations[o] != opsCommon.OPERATION_PLAN_TYPE.PRELOAD))
            {
                // if planet is in next phase and op can possibly be completed but isn't, change to PRELOAD
                // if planet is in next phase and op can't be completed, don't change to SKIP, since maybe someone's planning to upgrade?
                let matchingOperationAssignmentDetails = results.operationAssignments.find(oa => (oa.planet === planetPlan.planet.name) && (oa.operation === o+1));
                if (!matchingOperationAssignmentDetails) continue;

                let notFilledAssignment = matchingOperationAssignmentDetails.reqs.find(r => !r.assignment || r.assignment.allycode == opsCommon.PRELOAD_ALLYCODE);
                
                if (notFilledAssignment) {
                    planetPlan.operations[o] = opsCommon.OPERATION_PLAN_TYPE.PRELOAD;
                    changeMade = true;
                }
            }
            else if (!planetInNextPhase && (planetPlan.operations[o] === opsCommon.OPERATION_PLAN_TYPE.PRELOAD))
            {
                // planet isn't in next phase, so make sure all ops are either FILL or SKIP
                let matchingOperationAssignmentDetails = results.operationAssignments.find (oa => (oa.planet === planetPlan.planet.name) && (oa.operation === o+1));
                let notFilledAssignment = matchingOperationAssignmentDetails?.reqs.find(r => !r.assignment || r.assignment.allycode == opsCommon.PRELOAD_ALLYCODE);
                
                // they're not filling an assignment, so skip the operation
                if (notFilledAssignment)
                {
                    planetPlan.operations[o] = opsCommon.OPERATION_PLAN_TYPE.SKIP;
                    changeMade = true;
                }
                else
                {
                    // all assignments are filled, so this shouldn't be set to preload
                    planetPlan.operations[o] = opsCommon.OPERATION_PLAN_TYPE.FILL;
                    changeMade = true;
                }

            }
        }

    }

    if (changeMade)
    {
        await opsCommon.saveGuildTbPlanData(comlinkGuildId, { plan: plan })
    }
}


const MAX_COST = 500000;
const COST_PRELOAD = 250000;


async function getAssignmentsAndMissingFromReqs(reqs, guildData, planetStrategies = {}, rareThreshold, allycodesToExclude, priority, tiebreaker = opsCommon.OPERATION_SELECTION_TIEBREAKER.RANDOM)
{
    // first, find the rare units.
    // Second, go operation by operation, check if they can be filled, catalog the "cost" in terms of rare units
    // sort by cost ASC
    // fill operations, cataloging assignments and missing

    // set up players for storing operation assignment data
    guildData.players.forEach((p) => {
        p.tbops = {
            rareAssignmentsCount: 0,
            glAssignmentsCount: 0,
            assignments: null,
            assignmentsByPlanet: {}
        }
    });

    let unitsToFill = {};
    let allOperations = [];
    let availabilityByUnit = {};
    for (var req of reqs)
    {
        let op = allOperations.find(o => o.planet === req.planet && o.sector === req.sector && o.operation === req.operation);
        if (!op)
        {
            op = {
                planet: req.planet,
                sector: req.sector,
                reqs: new Array(),
                operation: req.operation,
                cost: 0 // for later
            };

            allOperations.push(op);
        }

        op.reqs.push(req);

        if (!unitsToFill[opsCommon.getModifiedUnitDefId(req.unitDefId, req.minRelicLevel)]) 
        {
            let unitToFillObj = createAndPopulateUnitToFillObj(guildData.players, req, allycodesToExclude);

            unitsToFill[opsCommon.getModifiedUnitDefId(req.unitDefId, req.minRelicLevel)] = unitToFillObj;
        } else {
            unitsToFill[opsCommon.getModifiedUnitDefId(req.unitDefId, req.minRelicLevel)].needed++;
        }
    }

    let assignments = {};

    let madePreAssignment = false;
    let utfKeys = Object.keys(unitsToFill);
    // loop through all unitToFill and sort the playersMatched by rareAssignmentsCount ASC and relicLevel ASC (if relevant)
    for (let key of utfKeys)
    {
        let unitToFillObj = unitsToFill[key];
        
        availabilityByUnit[key] = {
            needed: unitToFillObj.needed,
            available: unitToFillObj.playersMatched.length
        };

        // if the same unit is required at different relic levels, then handle it later 
        let utfKeysWithSameDefId = utfKeys.filter(k => k.startsWith(unitToFillObj.unitDefId));
        if (utfKeysWithSameDefId.length > 1)
        {
            let isOk = false;
            for (let otherUtfKey of utfKeysWithSameDefId)
            {
                if (key === otherUtfKey) continue; // no need to check the current one
                if (unitsToFill[otherUtfKey].length > 1)
                {
                    isOk = true;
                    // it's ok, because there's more than one possible for another assignment
                    continue;
                }

                isOk = false;
                break;
            }

            if (!isOk) continue;
        }

        // pre-assign when there's only one match and there's only 1 needed.
        // later, handle if the player has > 10 "one match" assignments on a single planet
        if (unitToFillObj.playersMatched.length === 1 && unitToFillObj.needed === 1)
        {
            let req = reqs.find(r => r.unitDefId === unitToFillObj.unitDefId && r.minRelicLevel === unitToFillObj.minRelicLevel);
            let match = unitToFillObj.playersMatched[0];

            if (canMakeAssignment(assignments, match, req))
            {

                makeAssignment(assignments, match, req);

                unitToFillObj.needed--;
                unitToFillObj.playersAssigned.push(match);
    
                madePreAssignment = true;
    
                // no need to sort since only one match
                continue;
            }
        }
        
        await sortMatchingPlayers(unitToFillObj, undefined, undefined, tiebreaker);
    }
    
    let playersToSkip = [];
    // if someone is assigned too many, then remove all assignments for them and make sure we don't try them again as a preassignment
    playersToSkip.push(...removeAllAssignmentsForPlayersWithTooMany(guildData.players));
    
    if (Object.keys(planetStrategies).length > 0)
    {
        reqs.sort((a, b) => 
        {
            let aPlan = planetStrategies[a.planet]?.operationPlans[a.operation - 1] ?? opsCommon.OPERATION_PLAN_TYPE.FILL; // reqs has a 1 index, operationPlans array has 0 index
            let bPlan = planetStrategies[b.planet]?.operationPlans[b.operation - 1] ?? opsCommon.OPERATION_PLAN_TYPE.FILL;

            // .FILL is 1, .PRELOAD is 2. We want to preassign to "FILL" spots first
            return aPlan - bPlan;
        });
    }

    // go through all planets. Including a depth in case the logic is fucked in some way and we'd otherwise get an infinite loop
    let depth = 0;
    do {
        madePreAssignment = false;
        for (let req of reqs)
        {
            if (req.assignment) continue;

            let key = opsCommon.getModifiedUnitDefId(req.unitDefId, req.minRelicLevel);
            let unitToFillObj = unitsToFill[key];

            let viablePlayersForPlanet = unitToFillObj.playersViableForPlanet(req.planet);
            if (viablePlayersForPlanet.length === 1 && unitToFillObj.needed === 1)
            {
                
                // if the same unit is required at different relic levels, then handle it later 
                let utfKeysWithSameDefId = utfKeys.filter(k => k.startsWith(unitToFillObj.unitDefId));
                if (utfKeysWithSameDefId.length > 1)
                {
                    let isOk = false;
                    for (let otherUtfKey of utfKeysWithSameDefId)
                    {
                        if (key === otherUtfKey) continue; // no need to check the current one
                        if (unitsToFill[otherUtfKey].length > 1)
                        {
                            isOk = true;
                            // it's ok, because there's more than one possible for another assignment
                            continue;
                        }

                        isOk = false;
                        break;
                    }

                    if (!isOk) continue;
                }
                
                if (!canMakeAssignment(assignments, viablePlayersForPlanet[0], req))
                {
                    unitToFillObj.nextAssignmentIndex++;
                    continue;
                }

                if (playersToSkip.indexOf(viablePlayersForPlanet[0]) != -1) continue;

                makeAssignment(assignments, viablePlayersForPlanet[0], req);
                unitToFillObj.playersAssigned.push(viablePlayersForPlanet[0]);
                unitToFillObj.needed--;
                madePreAssignment = true;
            }
        }
        
        // if someone is assigned too many, then remove all assignments for them
        playersToSkip.push(...removeAllAssignmentsForPlayersWithTooMany(guildData.players));

    } while (madePreAssignment && ++depth < 10);

    if (depth > 0)
    {
        logger.info(`TB Ops for ${guildData.guildName} (${guildData.comlinkGuildId}) (${swapiUtils.getPlayerAllycode(guildData.players[0])}) ran with depth ${depth}`);
    }

    // at this point, unitsToFill contains a count of fulfillment for every req, 
    // so we can easily see what's under the needed amount. The "rare" units are those under needed and above zero
    allOperations = recalcOperationsCosts(allOperations, 0, unitsToFill, priority, planetStrategies);

    // now we can fill the operations, cataloging assignments
    // when we find the first operation that can't be filled, we catalog the missing along with closest
    let missing = [];
    for (var opIx = 0; opIx < allOperations.length; opIx++)
    {
        let op = allOperations[opIx];
        // let anyFleetsInOperation = op.reqs.find(r => unitsToFill[getModifiedUnitDefId(r.unitDefId, r.minRelicLevel)].combatType === constants.COMBAT_TYPES.FLEET) != undefined;
        for (var req of op.reqs)
        {
            if (req.assignment) continue;

            let unitKey = opsCommon.getModifiedUnitDefId(req.unitDefId, req.minRelicLevel);
            let unitToFillObj = unitsToFill[unitKey];

            // handle preload strategy for skipping stuff
            if (planetStrategies[req.planet] && 
                planetStrategies[req.planet].operationPlans[req.operation-1] === opsCommon.OPERATION_PLAN_TYPE.PRELOAD)
            { 
                if ((planetStrategies[req.planet].preloadStrategy === opsCommon.PRELOAD_STRATEGY_ENUM.SKIP_COMMON_UNITS ||
                    (planetStrategies[req.planet].preloadStrategy === opsCommon.PRELOAD_STRATEGY_ENUM.SKIP_COMMON_SHIPS && (unitToFillObj.combatType === constants.COMBAT_TYPES.FLEET /* || !anyFleetsInOperation */)))
                    && unitToFillObj.playersMatched.length > 0 
                    && unitToFillObj.playersMatched.length >= availabilityByUnit[unitKey].needed + (rareThreshold ?? 5))
                {
                        req.assignment = {
                            playerName: opsCommon.PRELOAD_NAME,
                            allycode: opsCommon.PRELOAD_ALLYCODE
                        }
                        
                        continue;
                }

                if (planetStrategies[req.planet].preloadStrategy === opsCommon.PRELOAD_STRATEGY_ENUM.SKIP_ALL_SHIPS && unitToFillObj.combatType === constants.COMBAT_TYPES.FLEET)
                {
                    req.assignment = {
                        playerName: opsCommon.PRELOAD_NAME,
                        allycode: opsCommon.PRELOAD_ALLYCODE
                    }

                    continue;
                }
            }

            if (planetStrategies[req.planet].rareOnly && rareThreshold != null && unitToFillObj.playersMatched.length > 0 && unitToFillObj.playersMatched.length >= availabilityByUnit[unitKey].needed + rareThreshold)
            {
                req.assignment = { 
                    playerName: opsCommon.UNASSIGNED_NAME,
                    allycode: opsCommon.UNASSIGNED_ALLYCODE
                };
                continue;
            }

            await sortMatchingPlayers(unitToFillObj, planetStrategies, req, tiebreaker);
            let match;
            let matchedNotAssigned = unitToFillObj.playersMatchedNotAssigned();
            for (let ix = 0; ix < matchedNotAssigned.length; ix++)
            {
                match = matchedNotAssigned[ix];

                if (!canMakeAssignment(assignments, match, req)) {
                    match = null;
                    continue;
                }

                break;
            }

            // no match at all
            if (!match) 
            {
                if (planetStrategies[req.planet] && 
                    planetStrategies[req.planet].operationPlans[req.operation-1] === opsCommon.OPERATION_PLAN_TYPE.PRELOAD && 
                    unitToFillObj.playersMatched.length > 0)
                {
                    // it's a preload, and the guild has at least 1 total, so not missing
                    req.assignment = {
                        playerName: opsCommon.PRELOAD_NAME,
                        allycode: opsCommon.PRELOAD_ALLYCODE
                    };

                    continue;
                }

                // can't fill any more, so catalog the closest
                if (unitToFillObj.playersClose.length > 0)
                {
                    req.closest = unitToFillObj.playersClose.slice(0, 2);
                }

                missing.push(req);  

                continue;
            }

            // there was a match, so make the assignment
            makeAssignment(assignments, match, req);
            unitToFillObj.playersAssigned.push(match);
            unitToFillObj.needed--;
        }

        allOperations = recalcOperationsCosts(allOperations, opIx + 1, unitsToFill, priority, planetStrategies);
    }

    return { 
        missing: missing, 
        operationAssignments: allOperations, 
        playerAssignments: assignments,
        availabilityByUnit: availabilityByUnit
    };
}

function createAndPopulateUnitToFillObj(players, req, allycodesToExclude)
{
    let unitToFillObj = {
        needed: 1,
        unitDefId: req.unitDefId,
        unitName: req.unitName,
        isGL: req.isGL,
        minRelicLevel: req.minRelicLevel,
        combatType: 0,
        nextAssignmentIndex: 0,
        playersMatched: new Array(),
        playersClose: new Array(),
        playersAssigned: new Array(),
        playersMatchedNotAssigned: function() { return this.playersMatched.filter(p => !this.playersAssigned.find(a => a === p)); },
        isLessThanNeeded: function() { return (this.playersMatched.length - this.playersAssigned.length) > 0 && this.playersMatched.length < this.needed; },
        isExactFill: function() { return (this.playersMatched.length > 0) && (this.playersMatched.length - this.playersAssigned.length === this.needed); },
            // (this.playersMatched.length - this.nextAssignmentIndex) > 0 && this.playersMatched.length === this.needed; },
        isImpossible: function() { return this.playersMatched.length === this.playersAssigned.length },
        playersViableForPlanet: function(planet) { return this.playersMatchedNotAssigned().filter(player => (player.tbops?.assignmentsByPlanet[planet]?.length ?? 0) < MAX_ASSIGNMENTS_PER_PLAYER_PER_PLANET); }
    };

    players.forEach(p => {
        // check if player should be excluded
        if (allycodesToExclude?.find(ac => ac === swapiUtils.getPlayerAllycode(p))) return;

        let roster = swapiUtils.getPlayerRoster(p);
        let ru = roster.find(u => swapiUtils.getUnitDefId(u) === req.unitDefId);

        if (!ru) return;

        // it's a ship
        if (swapiUtils.getUnitCombatType(ru) === constants.COMBAT_TYPES.FLEET)
        {
            unitToFillObj.combatType = constants.COMBAT_TYPES.FLEET;
            if (swapiUtils.getUnitRarity(ru) >= 7) unitToFillObj.playersMatched.push(p); // 7*, match
            else unitToFillObj.playersClose.push(
                { 
                    allycode: swapiUtils.getPlayerAllycode(p),
                    playerName: swapiUtils.getPlayerName(p), 
                    rarity: swapiUtils.getUnitRarity(ru) 
                }); // otherwise, they're considered close
            
            return;
        }

        unitToFillObj.combatType = constants.COMBAT_TYPES.SQUAD;

        // it's a character at least at the right relic level
        if (swapiUtils.getUnitRelicLevel(ru) - 2 >= req.minRelicLevel) {
            unitToFillObj.playersMatched.push(p);
            return;
        }

        // considered close if G13
        if (swapiUtils.getUnitGearLevel(ru) >= 13) {
            unitToFillObj.playersClose.push(
                { 
                    playerName: swapiUtils.getPlayerName(p), 
                    relicLevel: swapiUtils.getUnitRelicLevel(ru) - 2
                }
            );
        }
    });

    // if the requirement is within a rarity threshold, then increase the "rareAssignmentsCount" score of each player that matches
    if (unitToFillObj.playersMatched.length > 0 && unitToFillObj.playersMatched.length < unitToFillObj.needed + 5)
    {
        unitToFillObj.playersMatched.forEach((p) => p.tbops.rareAssignmentsCount++);
    }

    // randomize the order of all "playersClose" so it's not sorted by when players joined the guild
    unitToFillObj.playersClose.sort((a, b) => Math.random() - 0.5 ); 

    // sort playersClose by rarity/relic
    if (unitToFillObj.combatType === constants.COMBAT_TYPES.FLEET) {
        unitToFillObj.playersClose.sort((a, b) => b.rarity - a.rarity)
    }
    else 
    {
        // sort closest from high to low
        unitToFillObj.playersClose.sort((a, b) => b.relicLevel - a.relicLevel);
    }

    return unitToFillObj;
}

function removeAllAssignmentsForPlayersWithTooMany(players)
{
    let playersWithAssignmentsRemoved = [];
    for (let p of players)
    {
        if (p.tbops.assignments === null || p.tbops.assignments.length === 0) continue;

        if (p.tbops.assignments.length > MAX_ASSIGNMENTS_PER_PLAYER)
        {
            // remove them all
            for (let reqToUnassign of p.tbops.assignments)
            {
                reqToUnassign.assignment = null;
            }

            p.tbops.assignments.splice(0, p.tbops.assignments.length);

            p.tbops.assignmentsByPlanet = {};

            playersWithAssignmentsRemoved.push(p);
            continue;
        }

        for (let planet of Object.keys(p.tbops.assignmentsByPlanet))
        {
            if (p.tbops.assignmentsByPlanet[planet]?.length > MAX_ASSIGNMENTS_PER_PLAYER_PER_PLANET) 
            {
                // remove all for this planet
                for (let a = 0; a < p.tbops.assignments.length; )
                {
                    let req = p.tbops.assignments[a];
                    if (req.planet === planet)
                    {
                        req.assignment = null;
                        p.tbops.assignments.splice(a, 1);
                    } else {
                        a++;
                    }
                }

                p.tbops.assignmentsByPlanet[planet] = new Array();
            
                playersWithAssignmentsRemoved.push(p);
            }
        }
    }

    return playersWithAssignmentsRemoved;
}

function canMakeAssignment(assignments, player, req)
{
    let existingAssignmentsForPlayer = assignments[swapiUtils.getPlayerAllycode(player)];
                
    // verify whether 
    // 1. the player has max assignments
    // 2. the player has max assignments for the planet
    // 3. the unit has already been assigned to a player. This is relevant when there are
    //    multiple sectors in play (different relic levels) for the same unit
    if (existingAssignmentsForPlayer?.hasMaxAssignments() || 
        existingAssignmentsForPlayer?.hasMaxAssignmentsForPlanet(req.planet) ||
        existingAssignmentsForPlayer?.reqsAssigned.find(ar => ar.unitDefId === req.unitDefId))
    {
        return false;
    }

    return true;
}

function makeAssignment(assignments, match, req)
{
    if (!assignments[swapiUtils.getPlayerAllycode(match)])
    { 
        let assignmentsObj = {
            reqsAssigned: [req],
            playerName: swapiUtils.getPlayerName(match),
            playerAllycode: swapiUtils.getPlayerAllycode(match),
            hasMaxAssignmentsForPlanet: function(p) { return this.reqsAssigned.filter(r => r.planet === p).length === MAX_ASSIGNMENTS_PER_PLAYER_PER_PLANET; },
            hasMaxAssignments: function() { return this.reqsAssigned.length === MAX_ASSIGNMENTS_PER_PLAYER; }
        }
        
        assignments[swapiUtils.getPlayerAllycode(match)] = assignmentsObj;

        match.tbops.assignments = assignmentsObj.reqsAssigned;
    }
    else 
    {
        assignments[swapiUtils.getPlayerAllycode(match)].reqsAssigned.push(req);
    }

    if (req.isGL) match.tbops.glAssignmentsCount++;

    req.assignment = { 
        playerName: swapiUtils.getPlayerName(match), 
        allycode: swapiUtils.getPlayerAllycode(match)
    };

    if (match.tbops.assignmentsByPlanet[req.planet])
    {
        match.tbops.assignmentsByPlanet[req.planet].push(req);
    } 
    else 
    {
        match.tbops.assignmentsByPlanet[req.planet] = [req];

    }
}

function recalcOperationsCosts(allOperations, startIndex, unitsToFill, priority, planetStrategies)
{
    if (startIndex >= allOperations.length) return allOperations;

    for (let opIx = startIndex; opIx < allOperations.length; opIx++)
    {
        let op = allOperations[opIx];
        op.cost = 0; // reset it 
        if (planetStrategies[op.planet]?.operationPlans[op.operation - 1] === opsCommon.OPERATION_PLAN_TYPE.PRELOAD) op.cost = COST_PRELOAD;

        let reqsQuantity = {};
        for (var req of op.reqs)
        {
            if (req.assignment) continue;
            let modDefId = opsCommon.getModifiedUnitDefId(req.unitDefId, req.minRelicLevel);
            if (reqsQuantity[modDefId]) reqsQuantity[modDefId]++;
            else reqsQuantity[modDefId] = 1;
        }

        for (let modDefId of Object.keys(reqsQuantity))
        {
            let utf = unitsToFill[modDefId];
            let qtyNeededForOp = reqsQuantity[modDefId];
            let qtyMatchesNotAssigned = utf.playersMatchedNotAssigned().length;
            // this is to handle if a unit is required more than once for a single operation
            if (qtyMatchesNotAssigned < qtyNeededForOp)
            {
                op.cost = MAX_COST;
                break;
            }

            // if (qtyMatchesNotAssigned === qtyNeededForOp) op.cost++;

            // if there are assignments that only a single player can fill, do those earlier
            if (utf.isExactFill()) op.cost += qtyNeededForOp;

            // when there's less than the number needed remaining, fill it later
            if (utf.isLessThanNeeded()) 
            {
                op.cost += (16*qtyNeededForOp);
            }
        }

        // it's impossible, so don't bother looking further
        if (op.cost === MAX_COST) continue;
    }

    return subSort(allOperations, startIndex, allOperations.length, (priority === OPERATION_PRIORITY.MOST_POINTS_FIRST) ? SORT_BY_POINTS_FUNC : SORT_BY_EASIEST_FUNC);
}

let subSort = (arr, i, n, sortFx) => [].concat(...arr.slice(0, i), ...arr.slice(i, i + n).sort(sortFx), ...arr.slice(i + n, arr.length));

const SORT_BY_POINTS_FUNC = (a, b) => {
    if (a.cost === MAX_COST) return 1;
    if (b.cost === MAX_COST) return -1;

    if (a.order != null && b.order != null) return a.order - b.order;
    if (a.sector === b.sector) return a.cost - b.cost;
    
    return b.sector - a.sector;
}
const SORT_BY_EASIEST_FUNC = (a, b) => {
    return a.cost - b.cost;
}

async function sortMatchingPlayers(unitToFillObj, planetStrategies, req, tiebreaker)
{
    if (tiebreaker == null || tiebreaker == undefined) tiebreaker = opsCommon.OPERATION_SELECTION_TIEBREAKER.RANDOM;

    let planetsSameRelicRequirements = true;
    if (planetStrategies)
    {
        let planets = Object.values(planetStrategies).map(s => s.planet);
        let minRelicLevel = tbCommon.minRelicLevelByPlanet(planets[0]);
        for (let p = 1; p < planets.length; p++)
        {
            if (tbCommon.minRelicLevelByPlanet(planets[p]) != minRelicLevel) { planetsSameRelicRequirements = false; break; }
        }
    }
    
    if (!req)
    {
        unitToFillObj.playersMatched.sort(
            (a, b) => 
            {
                let omiCountDiff = 0, relicLevelDiff = 0;
                let aUnit = swapiUtils.getPlayerRoster(a).find(u => swapiUtils.getUnitDefId(u) === unitToFillObj.unitDefId);
                let bUnit = swapiUtils.getPlayerRoster(b).find(u => swapiUtils.getUnitDefId(u) === unitToFillObj.unitDefId);
                if (unitToFillObj.combatType === constants.COMBAT_TYPES.SQUAD) 
                {
                    let aTBOmiCount = getPlayerTBOmiCountForUnit(a, aUnit);
                    let bTBOmiCount = getPlayerTBOmiCountForUnit(b, bUnit);

                    // units with omicrons should be lower priority to place (sort by omi count, low to high)
                    omiCountDiff = aTBOmiCount - bTBOmiCount;
                    relicLevelDiff = swapiUtils.getUnitRelicLevel(aUnit) - swapiUtils.getUnitRelicLevel(bUnit);
                }
                
                if (!planetsSameRelicRequirements && relicLevelDiff != 0) return relicLevelDiff;
                if (omiCountDiff != 0) return omiCountDiff;
                if (a.tbops.rareAssignmentsCount != b.tbops.rareAssignmentsCount) return a.tbops.rareAssignmentsCount - b.tbops.rareAssignmentsCount;
                if (unitToFillObj.isGL && (a.tbops.glAssignmentsCount != b.tbops.glAssignmentsCount)) return a.tbops.glAssignmentsCount - b.tbops.glAssignmentsCount;

                // tiebreaker, configured or random
                switch (tiebreaker)
                {
                    case opsCommon.OPERATION_SELECTION_TIEBREAKER.HIGHEST_UNIT_GP:
                        return (swapiUtils.getUnitGP(bUnit) - swapiUtils.getUnitGP(aUnit)) ?? (Math.random() - 0.5);
                    case opsCommon.OPERATION_SELECTION_TIEBREAKER.LOWEST_UNIT_GP:
                        return (swapiUtils.getUnitGP(aUnit) - swapiUtils.getUnitGP(bUnit)) ?? (Math.random() - 0.5);
                    case opsCommon.OPERATION_SELECTION_TIEBREAKER.HIGHEST_PLAYER_GP:
                        return (swapiUtils.getGP(b) - swapiUtils.getGP(a)) ?? (Math.random() - 0.5);
                    case opsCommon.OPERATION_SELECTION_TIEBREAKER.LOWEST_UNIT_GP:
                        return (swapiUtils.getGP(a) - swapiUtils.getGP(b)) ?? (Math.random() - 0.5);
                    case opsCommon.OPERATION_SELECTION_TIEBREAKER.RANDOM:
                    default:
                        return Math.random() - 0.5;
                }
            }); 
    
    } 
    else if (unitToFillObj.playersAssigned.length < unitToFillObj.playersMatched.length - 1)
    {
        
        // grouping rules are used to prioritize who should be assigned or not assigned a unit based on 
        // what else they are assigned
        // if player is assigned all units in the match_all column, then prioritize them to be assigned units in the give_all column and to keep units in the keep_all column
        // if a player is assigned any unit in the match_any column, then prioritize them to be assigned units in the give_all column and to keep units in the keep_all column
        // make sure that these are handled after the calculations about rare units so that a player isn't assigned too much of common units
        const GROUPING_RULES = await opsCommon.getAllOperationGroupingRules();

        let applicableRules = GROUPING_RULES.filter(r => 
            (!r.planet || planetStrategies[r.planet]) &&   // if there's a planet, make sure planet is in strategies generated
                (
                    r.keep_all?.find(k => k === req.unitDefId)     // is the req unit in keep_all 
                    || r.give_all?.find(g => g === req.unitDefId)   // is the req unit in give_all
                )
            );

        // determine the minimum relic level requirement across all planets being generated
        // this is used when filtering the player's roster to see whether they have the eligible units
        // let minRelicLevelByAlignment = {};
        // let planetNames = Object.keys(planetStrategies);
        // for (let p of planetNames)
        // {
        //     let planet = planetStrategies[p].planet;
        //     let minRelicLevel = tbCommon.minRelicLevelBySector(planet.sector);
        //     minRelicLevelByAlignment[constants.UNIT_ALIGNMENTS.NEUTRAL] = Math.min(minRelicLevelByAlignment[constants.UNIT_ALIGNMENTS.NEUTRAL] ?? 100, minRelicLevel);

        //     switch (planet.alignment)
        //     {
        //         case tbCommon.ALIGNMENTS.DS:
        //             minRelicLevelByAlignment[constants.UNIT_ALIGNMENTS.DS] = Math.min(minRelicLevelByAlignment[constants.UNIT_ALIGNMENTS.DS] ?? 100, minRelicLevel);
        //             break;
        //         case tbCommon.ALIGNMENTS.LS:
        //             minRelicLevelByAlignment[constants.UNIT_ALIGNMENTS.LS] = Math.min(minRelicLevelByAlignment[constants.UNIT_ALIGNMENTS.LS] ?? 100, minRelicLevel);
        //             break;
        //         case tbCommon.ALIGNMENTS.MX:
        //             minRelicLevelByAlignment[constants.UNIT_ALIGNMENTS.DS] = Math.min(minRelicLevelByAlignment[constants.UNIT_ALIGNMENTS.DS] ?? 100, minRelicLevel);
        //             minRelicLevelByAlignment[constants.UNIT_ALIGNMENTS.LS] = Math.min(minRelicLevelByAlignment[constants.UNIT_ALIGNMENTS.LS] ?? 100, minRelicLevel);
        //             break;
        //     }
        // }

        let minRelicLevelForAllAssignments = 100;
        let planetNames = Object.keys(planetStrategies);
        
        for (let p of planetNames)
        {
            minRelicLevelForAllAssignments = Math.min(minRelicLevelForAllAssignments, tbCommon.minRelicLevelBySector(planetStrategies[p].planet.sector));
        }
        
        // there's more than one player that's matched and not assigned, so resort the players to balance out the assignments
        unitToFillObj.playersMatched.sort(
            (a, b) => 
            {
                let omiCountDiff = 0, relicLevelDiff = 0;
                let aUnit = swapiUtils.getPlayerRoster(a).find(u => swapiUtils.getUnitDefId(u) === unitToFillObj.unitDefId);
                let bUnit = swapiUtils.getPlayerRoster(b).find(u => swapiUtils.getUnitDefId(u) === unitToFillObj.unitDefId);
                if (unitToFillObj.combatType === constants.COMBAT_TYPES.SQUAD) 
                {
                    let aTBOmiCount = getPlayerTBOmiCountForUnit(a, aUnit);
                    let bTBOmiCount = getPlayerTBOmiCountForUnit(b, bUnit);

                    // units with omicrons should be lower priority to place (sort by omi count, low to high)
                    omiCountDiff = aTBOmiCount - bTBOmiCount;
                    relicLevelDiff = swapiUtils.getUnitRelicLevel(aUnit) - swapiUtils.getUnitRelicLevel(bUnit);
                }

                // check grouping rules to see if there's a keep or give rule that has the "req" unit in it
                let aRuleMatchCount = 0, bRuleMatchCount = 0;
                for (let rule of applicableRules)
                {
                    aRuleMatchCount += calcRuleMatchCountChange(a, req, rule, minRelicLevelForAllAssignments);
                    bRuleMatchCount += calcRuleMatchCountChange(b, req, rule, minRelicLevelForAllAssignments);
                }

                // the more "give" rules they match, the higher priority their assignment
                let ruleMatchCountDiff = bRuleMatchCount - aRuleMatchCount;
                if (ruleMatchCountDiff != 0) return ruleMatchCountDiff;
                
                // assign units to players that have the lowest relic level first if there are planets with different relic reqs
                if (!planetsSameRelicRequirements && relicLevelDiff != 0) return relicLevelDiff;

                // players with omis are prioritized later
                if (omiCountDiff != 0) return omiCountDiff;

                // assign units to players that have the fewest units assigned to the planet first
                let planetAssignmentsA = a.tbops.assignmentsByPlanet[req.planet]?.length ?? 0;
                let planetAssignmentsB = b.tbops.assignmentsByPlanet[req.planet]?.length ?? 0;

                if (planetAssignmentsA != planetAssignmentsB) return planetAssignmentsA - planetAssignmentsB;
                
                // assign GLs to players that have the fewest GLs first
                if (unitToFillObj.isGL && (a.tbops.glAssignmentsCount != b.tbops.glAssignmentsCount)) return a.tbops.glAssignmentsCount - b.tbops.glAssignmentsCount;
                
                // tiebreaker, configured or random
                switch (tiebreaker)
                {
                    case opsCommon.OPERATION_SELECTION_TIEBREAKER.HIGHEST_UNIT_GP:
                        return (swapiUtils.getUnitGP(bUnit) - swapiUtils.getUnitGP(aUnit)) ?? (Math.random() - 0.5);
                    case opsCommon.OPERATION_SELECTION_TIEBREAKER.LOWEST_UNIT_GP:
                        return (swapiUtils.getUnitGP(aUnit) - swapiUtils.getUnitGP(bUnit)) ?? (Math.random() - 0.5);
                    case opsCommon.OPERATION_SELECTION_TIEBREAKER.HIGHEST_PLAYER_GP:
                        return (swapiUtils.getGP(b) - swapiUtils.getGP(a)) ?? (Math.random() - 0.5);
                    case opsCommon.OPERATION_SELECTION_TIEBREAKER.LOWEST_PLAYER_GP:
                        return (swapiUtils.getGP(a) - swapiUtils.getGP(b)) ?? (Math.random() - 0.5);
                    case opsCommon.OPERATION_SELECTION_TIEBREAKER.RANDOM:
                    default:
                        return Math.random() - 0.5;
                }
            }); 
    }
    
}

function calcRuleMatchCountChange(player, req, rule, minRelicLevelForAllAssignments)
{
    let match = true;

    if (rule.has)
    {
        let minRelicLevel = rule.has.minRelicLevel ?? minRelicLevelForAllAssignments;
        if (rule.has.factions?.length > 0)
        {
            // the rule requires them to have at least minimumCount of units that match all factions
            // and are at least minRelicLevel (if provided)

            // find all units in the player's roster that have 
            // all the factions that are listed in the rule
            let matchingRosterUnits = player.rosterUnit.filter(
                    u => 
                        swapiUtils.getUnitRarity(u) >= tbCommon.MIN_RARITY &&
                        (
                            swapiUtils.getUnitCombatType(u) === constants.COMBAT_TYPES.FLEET || // ships just need MIN_RARITY
                            swapiUtils.getUnitRelicLevel(u) - 2 >= minRelicLevel // relic level check
                        )
                        && rule.has.factions.filter(f => u.categoryId?.find(c => c === f))?.length === rule.has.factions.length // factions check
                );
            
            // if the player doesn't have enough units at the required relic level, then no match
            if (matchingRosterUnits.length < rule.has.minimumCount) match = false;
        }
        
        if (match && rule.has.units?.length > 0)
        {
            // the rule requires them to have these units at least at minRelicLevel (if provided)

            // check that every unit in the "has.units" clause is at least at minRelicLevel in
            // the players roster (count is equal)
            let matchingRosterUnits = rule.has.units.filter(fu => 
                player.rosterUnit.find(
                    u =>
                        swapiUtils.getUnitDefId(u) === fu &&
                        swapiUtils.getUnitRarity(u) >= tbCommon.MIN_RARITY &&
                        (
                            swapiUtils.getUnitCombatType(u) === constants.COMBAT_TYPES.FLEET || // ships just need MIN_RARITY
                            swapiUtils.getUnitRelicLevel(u) - 2 >= minRelicLevel // relic level check
                        )
                    )
                );
            
            // if the player doesn't have enough units at the required relic level, then no match
            if (matchingRosterUnits.length != rule.has.units.length) match = false;
        }

        if (match && rule.has.omicrons?.length > 0)
        {
            let playerTBOmis = swapiUtils.getPlayerOmicrons(player, swapiUtils.OMICRON_MODE.TERRITORY_BATTLE_BOTH_OMICRON);
            // rule requires that the player has these omicrons
            let matchingOmicrons = rule.has.omicrons.filter(o => playerTBOmis?.find(po => po === o));

            if (matchingOmicrons.length !== rule.has.omicrons.length) match = false;
        }

        // either not have the omicrons in xomicron or not have the unit at minRelicLevel in order to match
        if (match && rule.has.xomicrons?.length > 0)
        {
            let playerTBOmis = swapiUtils.getPlayerOmicrons(player, swapiUtils.OMICRON_MODE.TERRITORY_BATTLE_BOTH_OMICRON);
            let matchingOmicrons = rule.has.xomicrons.filter(o => playerTBOmis?.find(po => po === o));

            // it's a match if missing an omicron, so we only care to dig further
            // if they have the omicrons
            if (matchingOmicrons.length === rule.has.xomicrons.length)
            {
                // check if any of the units that have the relevant omicrons 
                // are below min_rarity or below minRelicLevel
                let failingUnits = swapiUtils.getPlayerRoster(player)
                                        .filter(u => 
                                            u.omicron_abilities?.find(a => rule.has.xomicrons.find(o => o === a))
                                            && (
                                                swapiUtils.getUnitRarity(u) < tbCommon.MIN_RARITY
                                                && swapiUtils.getUnitRelicLevel(u) - 2 < minRelicLevel
                                                )
                                            );

                // if one or more units is not eligible, then it's a match.
                // if all units are eligible (length == 0), then it's NOT a match for the rule
                if (failingUnits.length === 0) match = false;
            }
        }

        if (match && rule.has.xunits?.length > 0)
        {
            // if they have any of the xunits that are not eligible for the planet, then it matches the rule
            // all units need to be min_rarity
            // they need to either be a ship or be a character at or above minreliclevel
            let matchingUnits = swapiUtils.getPlayerRoster(player)
                                    .filter(u => 
                                                swapiUtils.getUnitRarity(u) >= tbCommon.MIN_RARITY 
                                                && rule.has.xunits.find(xu => xu === swapiUtils.getUnitDefId(u))
                                                && (
                                                    swapiUtils.getUnitCombatType(u) === constants.COMBAT_TYPES.FLEET ||
                                                    swapiUtils.getUnitRelicLevel(u) - 2 >= minRelicLevel
                                                )
                                            );

            // if they have all of the units available in the xunits list, then it's not a match for the rule
            if (matchingUnits.length === rule.has.xunits.length) match = false;
        }
    }

    if (match && rule.match_any && !rule.match_any.find(u => player.tbops.assignments?.find(asmt => asmt.unitDefId === u)))
    {
        // the player has not been assigned any of the units in match_any for the rule, 
        // so the rule does not apply
        match = false;
    }

    if (match && rule.match_all && rule.match_all.filter(u => player.tbops.assignments?.find(asmt => asmt.unitDefId === u)).length != rule.match_all.length)
    {
        // the player has not been assigned all of the units in match_all,
        // so the rule does not apply
        match = false;
    }

    if (match) {
        // they match the rule. 
        // If it's a "keep" rule, then decrease count.
        // If it's a "give" rule, then increase count.
        if (rule.keep_all?.find(k => k === req.unitDefId)) return -1;
        else return 1;
    }

    return 0;
}

function getPlayerTBOmiCountForUnit(player, unit)
{
    let tbOmiCount = 0;
    let tbOmis = swapiUtils.getPlayerOmicrons(player, swapiUtils.OMICRON_MODE.TERRITORY_BATTLE_BOTH_OMICRON);
    if (unit && (tbOmis?.length ?? 0) > 0)
    {
        for (let o = 0; o < (unit.omicron_abilities?.length ?? 0); o++)
        {
            if (tbOmis.indexOf(unit.omicron_abilities[o]) != -1) tbOmiCount++;
        }
    }

    return tbOmiCount;
}

async function loadGuildDataJobHandler(job)
{
    try 
    {
        const channel = CLIENT.channels.cache.get(job.data.channelId);
        const editMessage = (await channel.messages.fetch({ message: job.data.messageId }));

        try 
        {

            editMessage.edit({ content: "Processing your request now...", components: [], files: [], embeds: [] });

            let guildData = await guildUtils.loadGuildPlayerData(
                { 
                    comlinkGuildId: job.data.comlinkGuildId, 
                    editMessageId: job.data.messageId, 
                    editMessageChannelId: job.data.channelId 
                });
            
            let response;

            if (job.data.fn === "generateAll")
            {
                let plan = await opsCommon.getGuildTbPlanData(job.data.comlinkGuildId);

                let phase = parseInt(job.data.phase);
                let assignments = {};

                // go through phase requested and all future phases
                let previousAssignments = await opsCommon.getOpsData(job.data.comlinkGuildId);
                for (let p = !isNaN(phase) ? phase : 1; p <= tbCommon.TB_PHASES.length; p++)
                {
                    await editMessage.edit({ content: `Generating assignments for Phase ${p}/${tbCommon.TB_PHASES.length}... `, components: [], files: [], embeds: [] })
                    let resultsObj = await generateForPhase(
                        guildData, plan, previousAssignments, assignments, p, 
                        { rareThreshold: job.data.rareThreshold, priority: job.data.priority, exclude: job.data.allycodesToExclude });
                    assignments[p] = resultsObj;
                }

                await opsCommon.saveTbAssignments(job.data.comlinkGuildId, assignments);

                if (!isNaN(phase)) {
                    response = await loadOps.load(editMessage, opsCommon.OPERATION_VIEWS.PLANET, job.data.comlinkGuildId, phase);
                } else {
                    response = await opsOverview.overview(guildData);
                }
            }
            else
            {
                throw new Error(`Invalid function passed to ops.loadGuildDataJobHandler: ${job.data.fn}`);
            }
            
            await editMessage.edit(response);
        }
        catch (e)
        {
            logger.error(formatError(e));
            await editMessage.reply({ embeds: [discordUtils.createErrorEmbed("Error", "An error occurred processing the request. Please try again or connect with WookieeBot developers for help.")] });
        }
    } 
    catch (e)
    {
        let discordUser = await CLIENT.users.fetch(job.data.userId);
        await discordUser.send(
            {
                content: `WookieeBot may be missing permissions in channel <#${job.data.channelId}>. Please verify  \`/check-permissions\``
            }
        );
    }
}