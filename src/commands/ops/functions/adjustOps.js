
const { ActionRowBuilder, ButtonBuilder, StringSelectMenuBuilder, ButtonStyle, EmbedBuilder } = require("discord.js");
const opsCommon = require ("../common");
const tbCommon = require ("../../tb/common");

module.exports = {
    handleButton: async function(interaction)
    {

    },
    adjust: async function(interaction, phase, comlinkGuildId)
    {
        await interaction.editReply({ content: "Loading assignments, please wait...", files: [], embeds: [], components: [] });

        let planet = tbCommon.PLANETS.find(p => p.name === interaction.values[0]);
        
        let response = await generateAdjustResponse(comlinkGuildId, phase, planet);

        return response;
    },
    preload: async function(interaction, data)
    {
        return await adjustOpsAndGetResponse(interaction.values, data, opsCommon.OPERATION_PLAN_TYPE.PRELOAD);
    },
    include: async function(interaction, data)
    {
        return await adjustOpsAndGetResponse(interaction.values, data, opsCommon.OPERATION_PLAN_TYPE.FILL);
    },
    exclude: async function(interaction, data)
    {
        return await adjustOpsAndGetResponse(interaction.values, data, opsCommon.OPERATION_PLAN_TYPE.SKIP);
    },
    fillAll: async function(data)
    {
        return await adjustOpsAndGetResponse("ALL", data, opsCommon.OPERATION_PLAN_TYPE.FILL);
    },
    preloadAll: async function(data)
    {
        return await adjustOpsAndGetResponse("ALL", data, opsCommon.OPERATION_PLAN_TYPE.PRELOAD);
    },
    excludeAll: async function(data)
    {
        return await adjustOpsAndGetResponse("ALL", data, opsCommon.OPERATION_PLAN_TYPE.SKIP);
    },
    setPreloadStrategy: async function(interaction, data)
    {
        let requestData = parseData(data);

        let preloadStrategy = opsCommon.PRELOAD_STRATEGY.find(p => p.value === interaction.values[0]);
    
        let tbPlan = await opsCommon.getGuildTbPlanData(requestData.comlinkGuildId);

        let planetPlan = tbPlan[requestData.phase].find(p => p.planet === requestData.planet);

        planetPlan.preloadStrategy = preloadStrategy;

        await opsCommon.saveGuildTbPlanData(requestData.comlinkGuildId, { plan: tbPlan });

        let response = await generateAdjustResponse(requestData.comlinkGuildId, requestData.phase, requestData.planet);
    
        return response;
    }
}

async function adjustOpsAndGetResponse(opsToAdjust, data, adjustToType)
{
    let requestData = parseData(data);
    
    let tbPlan = await opsCommon.getGuildTbPlanData(requestData.comlinkGuildId);

    let planetPlan = tbPlan[requestData.phase].find(p => p.planet === requestData.planet);
    
    if (opsToAdjust === "ALL") 
    {
        planetPlan.operations.fill(adjustToType);
    }
    else
    {
        for (let o = 0; o < planetPlan.operations.length; o++)
        {
            if (opsToAdjust.find(i => i === o.toString())) planetPlan.operations[o] = adjustToType;
        }
    }

    await opsCommon.saveGuildTbPlanData(requestData.comlinkGuildId, { plan: tbPlan });

    let response = await generateAdjustResponse(requestData.comlinkGuildId, requestData.phase, requestData.planet);

    return response;
}

async function generateAdjustResponse(comlinkGuildId, phase, planet)
{
    let opsConfig = await opsCommon.loadGuildTbConfig(comlinkGuildId);
    let opsData = await opsCommon.getOpsData(comlinkGuildId);
    let tbPlan = await opsCommon.getGuildTbPlanData(comlinkGuildId);

    opsData.phases[phase-1].planets = opsData.phases[phase-1].planets.filter(p => p.planet === planet);

    let response = await opsCommon.getShowAssignmentsByPlanetResponse(opsData, phase, { rareThreshold: opsConfig.rare_threshold, rareByDefault: opsConfig.rare_by_default, includeNames: false, showTotalAvailable: true });

    if (phase != 6)
    {
        let metadataEmbed = new EmbedBuilder()
            .setTitle("Configuration")
            .setDescription(`Preload Strategy: ${tbPlan[phase].find(p => p.planet === planet).preloadStrategy.label}`);

        response.embeds.push(metadataEmbed);
    }
    
    response.components = generateComponents(comlinkGuildId, phase, opsData.phases[phase-1].planets[0].planet, tbPlan);

    return response;
}


const DATA_REGEX = /(\d)@(.+)@(.+)/
function parseData(data)
{
    let matches = DATA_REGEX.exec(data);

    return {
        phase: parseInt(matches[1]),
        planet: tbCommon.PLANETS.find(p => p.name === matches[2]),
        comlinkGuildId: matches[3]
    };
}

function generateComponents(comlinkGuildId, phase, planet, tbPlan)
{
    let curOps = tbPlan[phase].find(p => p.planet=== planet).operations;
    let includeOpsRow = new ActionRowBuilder()
        .addComponents(
            new StringSelectMenuBuilder()
                .setCustomId(`ops:m:fill:${phase}@${planet.name}@${comlinkGuildId}`)
                .setPlaceholder("Choose Ops to FILL")
                .addOptions(curOps.map((o, ix) => { return { label: (ix+1).toString(), value: ix.toString() } }))
                .setMaxValues(6)
        );
    let preloadOpsRow = new ActionRowBuilder()
        .addComponents(
            new StringSelectMenuBuilder()
                .setCustomId(`ops:m:pre:${phase}@${planet.name}@${comlinkGuildId}`)
                .setPlaceholder("Choose Ops to PRELOAD")
                .addOptions(curOps.map((o, ix) => { return { label: (ix+1).toString(), value: ix.toString() } }))
                .setMaxValues(6)
        );
    let excludeOpsRow = new ActionRowBuilder()
        .addComponents(
            new StringSelectMenuBuilder()
                .setCustomId(`ops:m:skip:${phase}@${planet.name}@${comlinkGuildId}`)
                .setPlaceholder("Choose Ops to SKIP")
                .addOptions(curOps.map((o, ix) => { return { label: (ix+1).toString(), value: ix.toString() } }))
                .setMaxValues(6)
        );
    let preloadStrategyRow = new ActionRowBuilder()
        .addComponents(
            new StringSelectMenuBuilder()
                .setCustomId(`ops:m:plstrat:${phase}@${planet.name}@${comlinkGuildId}`)
                .setPlaceholder("Select Preload Strategy")
                .addOptions(opsCommon.PRELOAD_STRATEGY.map(p => { return { label: p.label, value: p.value } }))
            );

    let buttonRow = new ActionRowBuilder()
        .addComponents(
            new ButtonBuilder()
                .setCustomId(`ops:m:fall:${phase}@${planet.name}@${comlinkGuildId}`)
                .setLabel("Fill All")
                .setStyle(ButtonStyle.Success));
    
    if (phase !== 6)
    {
        buttonRow.addComponents(
            new ButtonBuilder()
                .setCustomId(`ops:m:pall:${phase}@${planet.name}@${comlinkGuildId}`)
                .setLabel("Preload All")
                .setStyle(ButtonStyle.Primary));
    }

    buttonRow.addComponents(
            new ButtonBuilder()
                .setCustomId(`ops:m:xall:${phase}@${planet.name}@${comlinkGuildId}`)
                .setLabel("Skip All")
                .setStyle(ButtonStyle.Danger),
            new ButtonBuilder()
                .setCustomId(`ops:m:load-planet:${phase}${comlinkGuildId}`)
                .setLabel("Back")
                .setStyle(ButtonStyle.Secondary)
        );

    
    let components;
    if (phase === 6)
    {
        components = [includeOpsRow, excludeOpsRow, buttonRow];
    }
    else
    {
        components = [includeOpsRow, preloadOpsRow, excludeOpsRow, preloadStrategyRow, buttonRow];
    }

    return components;
}