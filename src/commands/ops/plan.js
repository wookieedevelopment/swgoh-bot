
const { SlashCommandSubcommandGroupBuilder } = require("@discordjs/builders");
const opsSave = require("./plan/saveTbPlan");
const opsLoad = require("./plan/loadTbPlan");
const opsDeletePlan = require("./plan/deletePlan");
const opsListTbPlans = require("./plan/listTbPlans");

module.exports = {
    data: new SlashCommandSubcommandGroupBuilder()
        .setName("plan")
        .setDescription("TB Plan Management")
        .addSubcommand(opsSave.data)
        .addSubcommand(opsLoad.data)
        .addSubcommand(opsDeletePlan.data)
        .addSubcommand(opsListTbPlans.data),
    interact: async function(interaction)
    {
      let subcommand = interaction.options.getSubcommand();

      switch (subcommand)
      {
        case opsSave.data.name:
            await opsSave.interact(interaction);
            break;
        case opsLoad.data.name:
            await opsLoad.interact(interaction);
            break;
        case opsDeletePlan.data.name:
            await opsDeletePlan.interact(interaction);
            break;
        case opsListTbPlans.data.name:
            await opsListTbPlans.interact(interaction);
            break;
      }
    },
    autocomplete: async function(interaction)
    {
        let subcommand = interaction.options.getSubcommand();
  
        switch (subcommand)
        {
            case opsLoad.data.name:
                await opsLoad.autocomplete(interaction);
                break;
            case opsDeletePlan.data.name:
                await opsDeletePlan.autocomplete(interaction);
                break;
        }
    }
}