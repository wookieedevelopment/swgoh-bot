const tbCommon = require("../tb/common");
const got = require("got");
const opsCommon = require("./common");
const playerUtils = require("../../utils/player");
const parse = require("csv-parse/lib/sync");
const { SlashCommandSubcommandBuilder } = require('@discordjs/builders');

const GENERIC_ERROR_MESSAGE = "Please ensure the format is the same as either the .csv or .json downloaded with the 'Export Assignments' dropdown or button in `/ops manage`.";

module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("import")
        .setDescription("Import operation assignments from a CSV or JSON file.")
        .addIntegerOption(o =>
            o.setName("phase")
                .setDescription("Phase")
                .setRequired(true)
                .addChoices(
                    ...tbCommon.TB_PHASE_CHOICES
                ))
        .addAttachmentOption(o =>
            o.setName("file")
                .setDescription("Assignments file")
                .setRequired(true)
        ),
    permissionLevel: playerUtils.UserRoles.GuildAdmin,
    interact: async function (interaction) {

        let { botUser, guildId, error } = await playerUtils.authorize(interaction, this.permissionLevel);

        if (error) {
            await interaction.editReply(error);
            return;
        }

        let phase = interaction.options.getInteger("phase");
        if (phase < tbCommon.TB_PHASES[0].n || phase > tbCommon.TB_PHASES[tbCommon.TB_PHASES.length - 1].n) {
            await interaction.editReply("Invalid phase. Choose from the options.");
            return;
        }

        let guild = botUser.guilds.find(g => g.guildId === guildId);

        let attachment = interaction.options.getAttachment("file");

        let buffer;
        try {
            buffer = await getAttachmentBuffer(attachment);
        }
        catch (error) {
            await interaction.editReply({ content: error.message });
            return;
        }

        let parsedFile;
        try {
            parsedFile = parseFile(buffer);
        } catch (e) {
            let em = GENERIC_ERROR_MESSAGE;
            if (e.message.startsWith(FORMAT_ERROR_PREFIX)) {
                em = `${e.message}\n\n${em}`;
            }
            else
            {
                em = `Invalid file. ${em}`;
            }
            await interaction.editReply(em);
            return;
        }

        await importAssignments(guild, phase, parsedFile);

        await interaction.editReply(`Imported new assignments for Phase ${phase}.`);
    }
}

const FORMAT_ERROR_PREFIX = 'Format Error: ';
function parseFile(attachment) {
    let data;
    try {
        data = JSON.parse(attachment.body);
        return data;
    }
    catch (e) { /* Not JSON */ }

    // not proper JSON format, so check CSV
    data = parse(attachment.body, { columns: true });

    // check for required columns
    if (data.length === 0) throw new Error(`${FORMAT_ERROR_PREFIX}No data in CSV file.`);
    if (data[0].AllyCode == undefined) throw new Error(`${FORMAT_ERROR_PREFIX}AllyCode column is missing.`);
    if (data[0].Planet == undefined) throw new Error(`${FORMAT_ERROR_PREFIX}Planet column is missing.`);
    if (data[0].Operation == undefined) throw new Error(`${FORMAT_ERROR_PREFIX}Operation column is missing.`);
    if (data[0].Row == undefined) throw new Error(`${FORMAT_ERROR_PREFIX}Row column is missing.`);
    if (data[0].Col == undefined) throw new Error(`${FORMAT_ERROR_PREFIX}Col column is missing.`);

    return data;
}

async function getAttachmentBuffer(attachment) {
    if (!attachment) {
        throw new Error("You must upload a valid operations assignments .csv or .json file. Download with the 'Export Assignments' dropdown or button in `/ops manage`");
    }

    if (attachment.size >= 1024 * 256) // 256KB limit
    {
        throw new Error("This cannot be right. The file is too big.");
    }

    var ssUrl = attachment.url;
    var buffer = await got(ssUrl);
    return buffer;
}

async function importAssignments(guild, phase, assignmentsData) {
    const planName = "default";
    const planDate = new Date();

    const allOperations = await opsCommon.getAllOperationRequirements();
    const oldPhaseAssignments = await opsCommon.loadAssignmentsByPhase( { comlinkGuildId: comlinkGuildId, phase: phase });


    const assignmentsToSave = {};
    const operationAssignments = new Array();
    assignmentsToSave[phase] = {
        operationAssignments: operationAssignments
    };

    // format:
    /*
        {
            operationAssignments: {
                [
                    { reqs: [] },
                    { reqs: [] },
                    { reqs: [] },
                    { reqs: [] },
                    { reqs: [] },
                    { reqs: [] }
                ]
            }
        }
    */

    if (Array.isArray(assignmentsData))
    {
        // loop through array and put the assignments on the phase
        for (let ad of assignmentsData)
        {
            
            operationAssignments.push({

            });
        }
    }

    await opsCommon.saveTbAssignments(guild.comlinkGuildId, assignmentsToSave);
}