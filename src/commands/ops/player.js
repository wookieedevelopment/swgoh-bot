const { SlashCommandSubcommandBuilder } = require('@discordjs/builders');
const playerUtils = require("../../utils/player");
const database = require("../../database");
const guildUtils = require("../../utils/guild.js");
const opsCommon = require("./common");
const tbCommon = require("../tb/common.js");

const ALL_PLAYERS_CHOICE = { name: "All Players", value: "0" };

module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("player")
        .setDescription("Manager player participation during TB.")
        .addStringOption(o =>
            o.setName("player")
            .setDescription("Player to adjust")
            .setRequired(true)
            .setAutocomplete(true))
        .addBooleanOption(o => 
            o.setName("available")
            .setDescription("True to set to available, False to for unavailable (exclude)")
            .setRequired(true))
        .addIntegerOption(o =>
            o.setName("phase")
            .setDescription(`Toggle availability for a single phase. Otherwise applies to all phases.`)
            .setRequired(false)
            .addChoices(...tbCommon.TB_PHASE_CHOICES)),
    permissionLevel: playerUtils.UserRoles.GuildAdmin,
    interact: async function(interaction)
    {
        let player = interaction.options.getString("player");
        let available = interaction.options.getBoolean("available");
        let phase = interaction.options.getInteger("phase");

        let { botUser, guildId, error } = await playerUtils.authorize(interaction, this.permissionLevel)
        if (error) {
            await interaction.editReply(error);
            return;
        }
        
        let guild = botUser.guilds.find(g => g.guildId === guildId);

        if (!guild)
        {
            await interaction.editReply("You are not a WookieeBot admin for a guild linked to this channel or server.");
            return;
        }
        
        let response = await this.setPlayerAvailability(guild, player, phase, available);

        await interaction.editReply(response);
    },
    autocomplete: async function (interaction)
    {
        let { botUser, guildId, error } = await playerUtils.authorize(interaction, this.permissionLevel)
        if (error) {
            await interaction.respond([{ name: "You do not have permission to do this.", value: "wwwwwwww" }]);
            return;
        }
        
        const focusedValue = interaction.options.getFocused();

        let names = await database.db.any(
`select player_name, allycode
from guild_players 
WHERE guild_id = $1 AND (player_name ILIKE $2 OR allycode LIKE $2)
LIMIT 24
`, [guildId, `%${focusedValue}%`]
        )

		await interaction.respond(
            [ALL_PLAYERS_CHOICE].concat(
                names.map(choice => ({ name: `${choice.player_name ? choice.player_name : "Unknown Player"} (${choice.allycode})`, value: choice.allycode }))
            )
        );
    },
    setPlayerAvailability: async function(guild, player, phase, available)
    {
        if (player == ALL_PLAYERS_CHOICE.value)
        {
            if (!available) return { content: `Cannot toggle all players to not available. This is only used to make all players Available.`};

            let query = `DELETE FROM tb_guild_player WHERE comlink_guild_id = $1`
            if (phase != null)
            {
                query += " AND phase = $2";
            }
            
            await database.db.any(query,
                [guild.comlinkGuildId, phase]);

            return { content: `All players marked as available for ${phase == null ? "all phases." : `Phase ${phase}`}` };
        }


        playerUtils.cleanAndVerifyStringAsAllyCode(player);

        let query = "";
        let phasesToSet = new Array();
        if (phase == null)
        {
            phasesToSet = tbCommon.TB_PHASES.map(p => p.n);
            query = `DELETE FROM tb_guild_player WHERE comlink_guild_id = $1 AND allycode = $2; `;
        }
        else
        {
            if (Number.isInteger(phase)) phasesToSet = [phase];
            else return { content: `Invalid phase. Choose a valid phase.` };
        }


        for (let p of phasesToSet)
        {
            query += `
            INSERT INTO tb_guild_player (comlink_guild_id, allycode, phase, available)
            VALUES ($1, $2, ${p}, $3)
            ON CONFLICT (comlink_guild_id, allycode, phase) DO UPDATE SET available = EXCLUDED.available;
            `;
        }

        await database.db.any(query,
        [guild.comlinkGuildId, player, available]);

        let playerName = await playerUtils.getPlayerNameByAllycode(player);

        return { content: `**${playerName} (${player})** set to **${available ? "Available" : "Not Available"}** for ${phase == null ? "the whole TB" : `Phase ${phase}`}.`};
    }
}