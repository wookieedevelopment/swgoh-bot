const { SlashCommandSubcommandBuilder } = require('@discordjs/builders');
const playerUtils = require("../../utils/player");
const database = require("../../database");
const unitsUtils = require('../../utils/units');
const { logger, formatError } = require("../../utils/log");

module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("x")
        .setDescription("Remove an operation assignment.")
        .addIntegerOption(o => 
            o.setName("phase")
            .setDescription("The phase with the assignment.")
            .setRequired(true)
            .setMinValue(1)
            .setMaxValue(6))
        .addIntegerOption(o => 
            o.setName("op")
            .setDescription("The operation assignment to remove.")
            .setRequired(true)
            .setAutocomplete(true)),
    permissionLevel: playerUtils.UserRoles.GuildAdmin,
    interact: async function(interaction)
    {
        let { botUser, guildId, error } = await playerUtils.authorize(interaction, this.permissionLevel);     
        if (error) {
            await interaction.editReply(error);
            return;
        }

        let op = interaction.options.getInteger("op");
        
        let guildOpData = await database.db.oneOrNone("SELECT * FROM tb_ops WHERE tb_ops_id = $1", [op]);

        if (!guildOpData) throw new Error(`Operation not found. If this persists, contact the developer.`);
        if (guildOpData.comlink_guild_id !== botUser.guilds.find(g => g.guildId === guildId)?.comlinkGuildId) 
        {
            logger.error(`Illegal attempt to unassign tb_ops_id ${op} by ${interaction.user.id}.`);

            throw new Error(`You do not have permission to do that. Use the autocomplete suggestions.`);
        }

        await database.db.any("UPDATE tb_ops SET allycode = NULL WHERE tb_ops_id = $1", op);

        await interaction.editReply("Assignment removed.");
    },
    autocomplete: async function(interaction)
    {
        let { botUser, guildId, error } = await playerUtils.authorize(interaction, this.permissionLevel);     
        if (error)
        {
            await interaction.respond([{ name: error, value: -1 }]);
            return;
        }

        let phase = interaction.options.getInteger("phase");
        const op = interaction.options.getFocused();

        let defIdFilter = unitsUtils.getUnitDefIdByNickname(op, false);

        let matches = await database.db.any(
`SELECT planet || '-' || operation || ' // ' || unit_name || ' // ' || player_name name, tb_ops_id value
FROM tb_ops
JOIN guild_players USING (allycode)
JOIN operation_req USING (operation_req_id)
WHERE guild_id = $1
AND (player_name ILIKE $2 OR unit_name ILIKE $2 OR planet ILIKE $2${defIdFilter?.length > 0 ? `OR unit_def_id IN ($4:csv)` : ""})
AND phase = $3
ORDER BY player_name, unit_name
LIMIT 25`, [guildId, `%${op}%`, phase, defIdFilter]);

        
		await interaction.respond(matches);
    }
}