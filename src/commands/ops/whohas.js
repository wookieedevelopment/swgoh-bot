const { SlashCommandSubcommandBuilder } = require('@discordjs/builders');
const swapiUtils = require("../../utils/swapi");
const queueUtils = require("../../utils/queue");
const database = require("../../database");
const unitsUtils = require('../../utils/units');
const playerUtils = require('../../utils/player');
const guildUtils = require("../../utils/guild");
const discordUtils = require("../../utils/discord");
const { logger, formatError } = require("../../utils/log");
const constants = require('../../utils/constants');
const { EmbedBuilder, Embed } = require('discord.js');
const { CLIENT } = require("../../utils/discordClient");

const QUEUE_FUNCTION_KEY = "ops:whohas";

module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("whohas")
        .setDescription("Check who can fill a requirement and who is close")
        .addStringOption(o => 
            o.setName("op")
            .setDescription("The operation unit.")
            .setRequired(true)
            .setAutocomplete(true))
        .addStringOption(o => 
            o.setName("allycode")
            .setDescription("Allycode in the guild to check. Default: yours")
            .setRequired(false))
        .addIntegerOption(o =>
            o.setName("alt")
            .setDescription("Alt account (/user me)")
            .setRequired(false)
            .setAutocomplete(true)),
    interact: async function(interaction)
    {
        let op = interaction.options.getString("op");
        let allycode = interaction.options.getString("allycode");
        let altNum = interaction.options.getInteger("alt");
        
        let includeTag = false;
        if (!allycode)
        {
            let { botUser, guildId, error } = await playerUtils.authorize(interaction);
            if (error) {
                await interaction.editReply({ embeds: [discordUtils.createErrorEmbed("Error", error)] });
                return;
            }
            
            let alt = await playerUtils.chooseBestAlt(botUser, guildId, altNum, true);

            if (!alt)
            {
                let errorEmbed = discordUtils.createErrorEmbed("Not in Guild", `Account is not in a guild. Choose a different alt, specify an ally code, join a guild, or register.`);
                await interaction.editReply({ embeds: [errorEmbed] });
                return;
            }

            allycode = alt.allycode;

            includeTag = true;
        }
        
        allycode = playerUtils.cleanAndVerifyStringAsAllyCode(allycode);

        let playerData = await swapiUtils.getPlayer(allycode);

        if (!playerData)
        {
            await interaction.editReply({ content: `No player found with allycode *${allycode}*.`})
            return;
        }

        if (!swapiUtils.getPlayerGuildId(playerData))
        {
            await interaction.editReply({ content: `${swapiUtils.getPlayerName(playerData)} (${allycode}) is not in a guild.`});
            return;
        }

        
        let { unitDefId, minRelicLevel } = this.splitOpValue(op);
        let cacheNeeded = await this.isGuildCachingNeeded(playerData);

        if (cacheNeeded) {
            // queue caching activity
            let qsm = await queueUtils.getQueueSizeMessage(queueUtils.QUEUES.OTHER, interaction.client.shard);
            const editMessage = await interaction.editReply(`Cached guild rosters are out of date. Queued **/ops whohas** player cache request. ${qsm}`);
            
            await queueUtils.queueOtherJob(interaction.client.shard, 
            {
                functionKey: QUEUE_FUNCTION_KEY,
                data: { 
                    comlinkGuildId: swapiUtils.getPlayerGuildId(playerData),
                    unitDefId: unitDefId,
                    minRelicLevel: minRelicLevel,
                    includeTag: includeTag,
                    allycode: allycode
                },
                channelId: editMessage.channelId ?? editMessage.channel.id,
                messageId: editMessage.id,
                userId: interaction.user.id,
            },
            {
                expireInHours: 4
            });

            return;
        }

        let response = await getWhoHasList(unitDefId, minRelicLevel, allycode, includeTag);

        await interaction.editReply(response);
    },
    isGuildCachingNeeded: async function(playerInGuild)
    {
        let guildData = await guildUtils.getGuildDataFromAPI({ comlinkGuildId: swapiUtils.getPlayerGuildId(playerInGuild) }, { useCache: false, includeRecentGuildActivityInfo: true });
        let playerRosterAllycodesCount = await database.db.one("SELECT COUNT(*) FROM (SELECT DISTINCT allycode FROM player_roster WHERE allycode IN ($1:csv) AND timestamp > NOW() - interval '12 hours') sq", [swapiUtils.getGuildRoster(guildData).map(gr => gr.allyCode)], r => parseInt(r.count));
        
        // check if all the players in the guild are in player_roster (assume they're recent enough)
        if (swapiUtils.getGuildRoster(guildData).length == playerRosterAllycodesCount) return false;

        return true;
    },
    splitOpValue: function(ov)
    {
        let spl = ov.split("::");
        return { unitDefId: spl[0], minRelicLevel: parseInt(spl[1]) };
    },
    autocomplete: async function(interaction)
    {
		const focusedOption = interaction.options.getFocused(true);

        let choices;

        if (focusedOption.name === "op")
        {
            let op = focusedOption.value;
            let defIdFilter = unitsUtils.getUnitDefIdByNickname(op, false);

            choices = await database.db.any(
`SELECT DISTINCT planet || ' // ' || ' R' || min_relic_level || ' // ' || unit_name name, unit_def_id || '::' || min_relic_level value
FROM operation_req
WHERE (unit_name ILIKE $1 OR planet ILIKE $1${defIdFilter?.length > 0 ? `OR unit_def_id IN ($2:csv)` : ""})
ORDER BY name
LIMIT 25`, [`%${op}%`, defIdFilter]);
        }
        else if (focusedOption.name === "alt")
        {
            const focusedValue = interaction.options.getFocused();
            choices = await playerUtils.getAltAutocompleteOptions(focusedValue, interaction.user.id);
        }


		await interaction.respond(choices);
    }
}

async function getWhoHasList(unitDefId, minRelicLevel, allycode, includeTag)
{
    const unitsList = await swapiUtils.getUnitsList();
    const unitData = unitsList.find(u => swapiUtils.getUnitDefId(u) === unitDefId);
    let isShip = swapiUtils.getUnitCombatType(unitData) === constants.COMBAT_TYPES.FLEET;

    let playersMatchingAndClose;
    if (isShip)
    {
        playersMatchingAndClose = await database.db.multiResult(
            `SELECT allycode, player_name, rarity, timestamp
            FROM player_roster
            JOIN guild_players USING (allycode)
            WHERE allycode IN (
                    SELECT allycode 
                    FROM guild_players 
                    WHERE guild_id IN (
                        SELECT guild_id 
                        FROM guild_players 
                        WHERE allycode = $1
                    )
                ) 
                AND unit_def_id = $2 
                AND rarity >= 7
            ORDER BY player_name;
            
            SELECT allycode, player_name, rarity, timestamp
            FROM player_roster
            JOIN guild_players USING (allycode)
            WHERE allycode IN (
                SELECT allycode 
                FROM guild_players 
                WHERE guild_id IN (
                    SELECT guild_id 
                    FROM guild_players 
                    WHERE allycode = $1
                    )
                ) 
                AND unit_def_id = $2
                AND rarity >= 3
                AND rarity < 7
            ORDER BY rarity DESC, player_name
            LIMIT 10;`, [allycode, unitDefId]);
    }
    else
    {
        playersMatchingAndClose = await database.db.multiResult(
            `SELECT allycode, player_name, relic_level, timestamp
            FROM player_roster
            JOIN guild_players USING (allycode)
            WHERE allycode IN (
                    SELECT allycode 
                    FROM guild_players 
                    WHERE guild_id IN (
                        SELECT guild_id 
                        FROM guild_players 
                        WHERE allycode = $1
                    )
                ) 
                AND unit_def_id = $2 
                AND relic_level >= $3
            ORDER BY relic_level DESC, player_name;
            
            SELECT allycode, player_name, relic_level, timestamp
            FROM player_roster
            JOIN guild_players USING (allycode)
            WHERE allycode IN (
                SELECT allycode 
                FROM guild_players 
                WHERE guild_id IN (
                    SELECT guild_id 
                    FROM guild_players 
                    WHERE allycode = $1
                    )
                ) 
                AND unit_def_id = $2
                AND relic_level >= 2
                AND relic_level < $3
            ORDER BY relic_level DESC, player_name
            LIMIT 10;`, [allycode, unitDefId, minRelicLevel+2]);

    }

    let playersMatching = playersMatchingAndClose[0].rows;
    let playersClose = playersMatchingAndClose[1].rows;
    let playersMatchingString, playersCloseString;
    let matchingTimestamp, closeTimestamp;
    if (playersMatching.length === 0)
    {
        playersMatchingString = "No players in the guild have this unit.";
    }
    else
    {
        playersMatchingString = `* ` + playersMatching.map(p => `${p.player_name}${discordUtils.LTR} (${p.allycode}): ${isShip ? `${p.rarity}*` : `R${p.relic_level-2}`}`).join("\n* ");
        matchingTimestamp = playersMatching.reduce((p, c) => { return p < c.timestamp ? p : c.timestamp; } );
    }

    if (playersClose.length === 0)
    {
        playersCloseString = "No players in the guild are close to this requirement.";
    }
    else
    {            
        playersCloseString = `* ` + playersClose.map(p => `${p.player_name}${discordUtils.LTR} (${p.allycode}): ${isShip ? `${p.rarity}*` : `R${p.relic_level-2}`}`).join("\n* ");
        closeTimestamp = playersClose.reduce((p, c) => { return p < c.timestamp ? p : c.timestamp; } );
    }

    let matchingEmbed = new EmbedBuilder()
        .setColor("Green")
        .setTitle(`${playersMatching.length} Player${(playersMatching.length != 1) ? "s" : ""} Matching`)
        .setDescription(playersMatchingString);
    if (matchingTimestamp != null) matchingEmbed.setFooter({ text: `Using data from ${discordUtils.convertDateToString(matchingTimestamp)} or later.` });

    let closeEmbed = new EmbedBuilder()
        .setColor("Yellow")            
        .setTitle(`${playersClose.length} Closest Player${(playersClose.length != 1) ? "s" : ""}`)
        .setDescription(playersCloseString);
    if (closeTimestamp != null) closeEmbed.setFooter({ text: `Using data from ${discordUtils.convertDateToString(closeTimestamp)} or later.` });

    let content = `Looking for players with ${isShip ? "7*" : `R${minRelicLevel}`} ${unitData.name}`;
    let response = { content: content, embeds: [matchingEmbed, closeEmbed] };

    if (includeTag && playersMatching.length > 0)
    {
        let tags = await database.db.any("SELECT discord_id FROM user_registration WHERE allycode IN ($1:csv)", [playersMatching.map(p => p.allycode)]);
        let tagText;
        
        if (tags.length > 0) tagText = "`" + tags.map(t => discordUtils.makeIdTaggable(t.discord_id)).join(" ") + "`";
        else tagText = "Matching players are not registered.";

        let handyTagEmbed = new EmbedBuilder()
            .setColor("Blurple")
            .setTitle("Convenient Tagging Text for Matching Players")
            .setFooter({ text: "Copy the text above to tag all matching (registered) players. On mobile, you may need to remove the ` at the start and end."})
            .setDescription(tagText);
        response.embeds.push(handyTagEmbed);
    }

    return response;
}

async function generateOpsWhohasResponse(jobData)
{
    const channel = CLIENT.channels.cache.get(jobData.channelId);

    const editMessage = (await channel.messages.fetch({ message: jobData.messageId }));
    await editMessage.edit({ content: `Caching players for guild. Please wait...` });
    
    await guildUtils.loadGuildPlayerData(
        { 
            comlinkGuildId: jobData.data.comlinkGuildId, 
            cacheTimeHours: 12,
            editMessageChannelId: jobData.channelId,
            editMessageId: jobData.messageId 
        });

    
    let response = await getWhoHasList(jobData.data.unitDefId, jobData.data.minRelicLevel, jobData.data.allycode, jobData.data.includeTag);

    await editMessage.edit(response);
}

queueUtils.addOtherFunction(QUEUE_FUNCTION_KEY,  generateOpsWhohasResponse);