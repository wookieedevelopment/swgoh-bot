const { SlashCommandSubcommandBuilder } = require('@discordjs/builders');
const discordUtils = require("../../utils/discord");
const playerUtils = require("../../utils/player");
const database = require("../../database");
const tbCommon = require("../tb/common");
const opsCommon = require("./common");
const { logger, formatError } = require("../../utils/log");
const { ModalBuilder, ActionRowBuilder, TextInputBuilder } = require('discord.js');

module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("test")
        .setDescription("Test operation readiness for a set of planets")
        .addIntegerOption(o => 
            o.setName("allycode")
            .setDescription("Allycode in the guild to check (default: yours)")
            .setRequired(false))
        .addIntegerOption(o =>
            o.setName("alt")
            .setDescription("Alt account (/user me)")
            .setRequired(false)
            .setAutocomplete(true)),
    interact: async function(interaction)
    {
        let missingPermissions = discordUtils.getMissingBotPermissions(interaction, interaction.channel);
        if (missingPermissions)
        {
            await interaction.editReply(discordUtils.convertMissingBotPermissionsArrayToResponse(missingPermissions));
            return;
        }

        let allycode = interaction.options.getString("allycode");
        let altNum = interaction.options.getInteger("alt");
        let comlinkGuildId;
        
        if (!allycode)
        {
            let { botUser, guildId, error } = await playerUtils.authorize(interaction);
            if (error) {
                await interaction.editReply({ embeds: [discordUtils.createErrorEmbed("Error", error)] });
                return;
            }
            
            let alt = await playerUtils.chooseBestAlt(botUser, guildId, altNum, true);

            if (!alt)
            {
                let errorEmbed = discordUtils.createErrorEmbed("Not in Guild", `Account is not in a guild. Choose a different alt, specify an ally code, join a guild, or register.`);
                await interaction.editReply({ embeds: [errorEmbed] });
                return;
            }

            allycode = alt.allycode;
            comlinkGuildId = alt.comlinkGuildId;
        }
        
        allycode = playerUtils.cleanAndVerifyStringAsAllyCode(allycode);

        let response = {};

        addComponentsToResponse(response, comlinkGuildId);
        
        let guildOpData = await database.db.oneOrNone("SELECT * FROM tb_ops WHERE tb_ops_id = $1", [op]);

        if (!guildOpData) throw new Error(`Operation not found. If this persists, contact the developer.`);
        if (guildOpData.comlink_guild_id !== botUser.guilds.find(g => g.guildId === guildId)?.comlinkGuildId) 
        {
            logger.error(`Illegal attempt to unassign tb_ops_id ${op} by ${interaction.user.id}.`);

            throw new Error(`You do not have permission to do that. Use the autocomplete suggestions.`);
        }

        await database.db.any("UPDATE tb_ops SET allycode = NULL WHERE tb_ops_id = $1", op);

        await interaction.editReply("Assignment removed.");
    },
    autocomplete: async function(interaction)
    {
        const focusedValue = interaction.options.getFocused();
        await interaction.respond((await playerUtils.getAltAutocompleteOptions(focusedValue, interaction.user.id)));
    }
}

function addComponentsToResponse(response, comlinkGuildId)
{
    
    let buttonRow1 = new ActionRowBuilder();
    buttonRow1.addComponents(
        new ButtonBuilder()
            .setCustomId(`ops:t:1:${comlinkGuildId}`)
            .setLabel("Sector 1")
            .setStyle(ButtonStyle.Success),
        new ButtonBuilder()
            .setCustomId(`ops:t:2:${comlinkGuildId}`)
            .setLabel("Sector 2")
            .setStyle(ButtonStyle.Success),
        new ButtonBuilder()
            .setCustomId(`ops:t:3:${comlinkGuildId}`)
            .setLabel("Sector 3")
            .setStyle(ButtonStyle.Success)
    );

    let buttonRow2 = new ActionRowBuilder();
    buttonRow2.addComponents(
        new ButtonBuilder()
            .setCustomId(`ops:t:4:${comlinkGuildId}`)
            .setLabel("Sector 4")
            .setStyle(ButtonStyle.Success),
        new ButtonBuilder()
            .setCustomId(`ops:t:5:${comlinkGuildId}`)
            .setLabel("Sector 5")
            .setStyle(ButtonStyle.Success),
        new ButtonBuilder()
            .setCustomId(`ops:t:6:${comlinkGuildId}`)
            .setLabel("Sector 6")
            .setStyle(ButtonStyle.Success)
    )

    let multiSelectRow = new ActionRowBuilder();
    let selectOptions = tbCommon.ALL_PLANET_CHOICES.map(p => { return { label: p.name, value: p.value }; });
    multiSelectRow.addComponents(
        new StringSelectMenuBuilder()
            .setCustomId(`ops:t:sel:${comlinkGuildId}`)
            .setPlaceholder('Choose up to five planets')
            .setMinValues(1)
            .setMaxValues(5)
            .addOptions(selectOptions)
    )

    let showAssignmentsButtonRow = new ActionRowBuilder();
    showAssignmentsButtonRow.addComponents(
        new ButtonBuilder()
            .setCustomId(`ops:t:aplanet:${customIdAppend}:${comlinkGuildId}`)
            .setLabel("Assignments by Planet")
            .setStyle(ButtonStyle.Primary),
        new ButtonBuilder()
            .setCustomId(`ops:t:aplayer:${customIdAppend}:${comlinkGuildId}`)
            .setLabel("Assignments by Player")
            .setStyle(ButtonStyle.Primary),
        new ButtonBuilder()
            .setCustomId(`ops:t:json:${customIdAppend}:${comlinkGuildId}`)
            .setLabel("Export Assignments")
            .setStyle(ButtonStyle.Secondary)
    );

    response.components = [buttonRow1, buttonRow2, multiSelectRow, showAssignmentsButtonRow]
}