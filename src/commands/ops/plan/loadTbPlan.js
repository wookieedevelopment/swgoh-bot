const opsCommon = require("../common.js");
const playerUtils = require("../../../utils/player.js");
const database = require("../../../database.js");
const { SlashCommandSubcommandBuilder } = require("@discordjs/builders");
const { ActionRowBuilder, ButtonBuilder, EmbedBuilder } = require('discord.js');
const { logger, formatError } = require("../../../utils/log.js");

module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("load")
        .setDescription("Load a previously saved TB plan. Unsaved changes will be lost.")
        .addStringOption(o =>
            o.setName("name")
            .setDescription("The name of the plan to load.")
            .setRequired(true)
            .setAutocomplete(true)),
    permissionLevel: playerUtils.UserRoles.GuildAdmin,
    interact: async function(interaction)
    {
        let { botUser, guildId, error } = await playerUtils.authorize(interaction, this.permissionLevel)
        if (error) {
            await interaction.editReply(error);
            return;
        }
        const guild = botUser.guilds.find(g => g.guildId === guildId);

        let planName = interaction.options.getString("name");

        let planExists = await this.planExists(guild.comlinkGuildId, planName);

        if (!planExists)
        {
            await interaction.editReply("You do not have a plan by that name.");
            return;
        }

        let response = await this.loadPlan(guild.comlinkGuildId, planName);
        await interaction.editReply(response);
    },
    autocomplete: async function(interaction)
    {
        let { botUser, guildId, error } = await playerUtils.authorize(interaction, this.permissionLevel)
        if (error) {
            await interaction.respond([{ name: "You do not have permission to do this.", value: "wwwwwwww" }]);
            return;
        }
        const guild = botUser.guilds.find(g => g.guildId === guildId);

        const focusedValue = interaction.options.getFocused();

        const names = await database.db.any(
`select DISTINCT plan_name
from tb_plan_ops_saved
WHERE comlink_guild_id = $1 AND plan_name ILIKE $2
LIMIT 25
`, [guild.comlinkGuildId, `%${focusedValue}%`]);

        await interaction.respond(
            names
            .filter(n => n.plan_name.length <= opsCommon.MAX_PLAN_NAME_LENGTH)
            .map(choice => (
            { 
                name: choice.plan_name, 
                value: choice.plan_name 
            })));
    },
    planExists: async function(comlinkGuildId, planName)
    {
        let existingPlan = await database.db.any("SELECT COUNT(*) FROM tb_plan_ops_saved WHERE comlink_guild_id = $1 AND plan_name = $2", [comlinkGuildId, planName])
        return existingPlan[0].count > 0;
    },
    loadPlan: async function(comlinkGuildId, planName)
    {
        let newTbOpsData = (await database.db.any(`
        SELECT tb_ops_json FROM tb_plan_ops_saved WHERE comlink_guild_id = $1 AND plan_name = $2`,
        [comlinkGuildId, planName]));

        let tbOpsJson = newTbOpsData[0].tb_ops_json;
        for (const r of tbOpsJson) { r.comlink_guild_id = comlinkGuildId; r.plan_name = planName;    }

        const tbOpsScript = database.pgp.helpers.insert(newTbOpsData[0].tb_ops_json, database.columnSets.tbOpsCS); 

        await database.db.any(
            `DELETE FROM tb_plan WHERE comlink_guild_id = $1;
            DELETE FROM tb_ops WHERE comlink_guild_id = $1;
            INSERT INTO tb_plan (comlink_guild_id, planets_by_phase, plan_name)
            (SELECT comlink_guild_id, planets_by_phase, plan_name
            FROM tb_plan_ops_saved
            WHERE comlink_guild_id = $1 AND plan_name = $2);
            ${tbOpsScript}
            `, [comlinkGuildId, planName]
        );

        return `Plan **${planName}** loaded.`;
    }
}
