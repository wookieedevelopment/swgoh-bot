const playerUtils = require("../../../utils/player");
const database = require("../../../database");
const { SlashCommandSubcommandBuilder } = require("@discordjs/builders");

module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("list")
        .setDescription("List saved TB plans."),
    permissionLevel: playerUtils.UserRoles.GuildAdmin,
    interact: async function(interaction)
    {
        let { botUser, guildId, error } = await playerUtils.authorize(interaction, this.permissionLevel)
        if (error) {
            await interaction.editReply(error);
            return;
        }
        const guild = botUser.guilds.find(g => g.guildId === guildId);

        let response = await this.getPlansList(guild.comlinkGuildId);
        await interaction.editReply(response);
    },
    getPlansList: async function(comlinkGuildId)
    {
        let plans = await database.db.any(
`SELECT DISTINCT plan_name
FROM tb_plan_ops_saved
WHERE comlink_guild_id = $1`, [comlinkGuildId]
        )

        if (plans.length == 0) return "You do not have any saved TB plans. Create one with `/ops manage` and save it with `/ops plan save`";

        return `Your saved TB plans:\n${plans.map((p, ix) => `${(ix + 1).toString()}) ${p.plan_name}`).join("\n")}`
    }
}
