const opsCommon = require("../common.js");
const playerUtils = require("../../../utils/player.js");
const database = require("../../../database.js");
const { SlashCommandSubcommandBuilder } = require("@discordjs/builders");
const { ActionRowBuilder, ButtonBuilder, EmbedBuilder, ButtonStyle, ComponentType } = require('discord.js');
const { logger, formatError } = require("../../../utils/log.js");

const YES_BUTTON_NAME = "ops:delete:yes";
const NO_BUTTON_NAME = "ops:delete:no";


module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("delete")
        .setDescription("Delete saved TB plan")
        .addStringOption(o =>
            o.setName("name")
            .setDescription("Name of plan to delete")
            .setMinLength(1)
            .setMaxLength(100)
            .setRequired(true)
            .setAutocomplete(true)),
    permissionLevel: playerUtils.UserRoles.GuildAdmin,
    interact: async function(interaction)
    {
        let { botUser, guildId, error } = await playerUtils.authorize(interaction, this.permissionLevel)
        if (error) {
            await interaction.editReply(error);
            return;
        }
        const guild = botUser.guilds.find(g => g.guildId === guildId);

        let planName = interaction.options.getString("name");

        let planExists = await this.planExists(guild.comlinkGuildId, planName);

        if (!planExists)
        {
            await interaction.editReply(`There is no plan by the name **${planName}**.`);
            return;
        }

        const filter = async i => {
            return i.user.id === interaction.user.id;
        };

        // ask about replacing existing plan
        let message = await interaction.editReply(
            { 
                content: `Are you sure you want to delete plan **${planName}**? This cannot be undone.`,
                components: [
                    new ActionRowBuilder().addComponents(
                        new ButtonBuilder()
                            .setCustomId(YES_BUTTON_NAME)
                            .setLabel("Yes")
                            .setStyle(ButtonStyle.Success),
                        new ButtonBuilder()
                            .setCustomId(NO_BUTTON_NAME)
                            .setLabel("No")
                            .setStyle(ButtonStyle.Danger)
                    )
                ]
            });

        message.awaitMessageComponent({ filter, componentType: ComponentType.Button, time: 300000 })
            .then(async i => {
                if (i.customId == NO_BUTTON_NAME)
                    await interaction.editReply({ content: "Delete cancelled.", components: [] });
                else
                    await this.handleButton(interaction)
            })
            .catch(err => logger.error(formatError(err)));
    },
    autocomplete: async function(interaction)
    {
        let { botUser, guildId, error } = await playerUtils.authorize(interaction, this.permissionLevel)
        if (error) {
            await interaction.respond([{ name: "You do not have permission to do this.", value: "wwwwwwww" }]);
            return;
        }
        const guild = botUser.guilds.find(g => g.guildId === guildId);

        const focusedValue = interaction.options.getFocused();

        let names = await database.db.any(`
SELECT DISTINCT plan_name
FROM tb_plan_ops_saved
WHERE comlink_guild_id = $1 AND plan_name ILIKE $2
LIMIT 25`,
[guild.comlinkGuildId, `%${focusedValue}%`])

		await interaction.respond(
			names
                .filter(n => n.plan_name.length < opsCommon.MAX_PLAN_NAME_LENGTH)
                .map(choice => ({ name: choice.plan_name, value: choice.plan_name }))
		);
    },
    handleButton: async function(interaction)
    {
        let { botUser, guildId, error } = await playerUtils.authorize(interaction, this.permissionLevel)
        if (error) {
            await interaction.followUp(error);
            return;
        }
        const guild = botUser.guilds.find(g => g.guildId === guildId);

        let planName = interaction.options.getString("name");
        
        await this.deletePlan(guild.comlinkGuildId, planName);

        await interaction.editReply({ content: `Plan **${planName}** deleted.`, components: [] });
    },
    planExists: async function(comlinkGuildId, planName)
    {
        let existingPlan = await database.db.any("SELECT COUNT(*) FROM tb_plan_ops_saved WHERE comlink_guild_id = $1 AND plan_name = $2", [comlinkGuildId, planName])
        return existingPlan[0].count > 0;
    },
    deletePlan: async function(comlinkGuildId, planName)
    {
        if (!planName || planName > opsCommon.MAX_PLAN_NAME_LENGTH)
        {
            return `Plan name must be between 1 and ${opsCommon.MAX_PLAN_NAME_LENGTH} characters.`;
        }
        
        await database.db.any(
`DELETE FROM tb_plan_ops_saved WHERE comlink_guild_id = $1 AND plan_name = $2`, [comlinkGuildId, planName]
        )
    }
}
