const opsCommon = require("../common.js");
const playerUtils = require("../../../utils/player.js");
const database = require("../../../database.js");
const { SlashCommandSubcommandBuilder } = require("@discordjs/builders");
const { ActionRowBuilder, ButtonBuilder, EmbedBuilder, ButtonStyle, ComponentType } = require('discord.js');
const { logger, formatError } = require("../../../utils/log.js");

const YES_BUTTON_NAME = "ops:save:yes"
const NO_BUTTON_NAME = "ops:save:no"

module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("save")
        .setDescription("Save a TB plan")
        .addStringOption(o =>
            o.setName("name")
            .setDescription("A name to help you identify the plan later.")
            .setMaxLength(opsCommon.MAX_PLAN_NAME_LENGTH)
            .setMinLength(1)
            .setRequired(true)),
    permissionLevel: playerUtils.UserRoles.GuildAdmin,
    interact: async function(interaction)
    {
        let { botUser, guildId, error } = await playerUtils.authorize(interaction, this.permissionLevel)
        if (error) {
            await interaction.editReply(error);
            return;
        }
        
        const guild = botUser.guilds.find(g => g.guildId === guildId);

        let planName = interaction.options.getString("name");

        let planExists = await this.planExists(guild.comlinkGuildId, planName);

        if (!planExists)
        {
            let response = await this.savePlan(guild.comlinkGuildId, planName);
            await interaction.editReply(response);
            return;
        }

        const filter = async i => {
            return i.user.id === interaction.user.id;
        };

        // ask about replacing existing plan
        const message = await interaction.editReply(
            { 
                content: `Plan **${planName}** already exists. Replace existing plan?`,
                components: [
                    new ActionRowBuilder().addComponents(
                        new ButtonBuilder()
                            .setCustomId(YES_BUTTON_NAME)
                            .setLabel("Yes")
                            .setStyle(ButtonStyle.Success),
                        new ButtonBuilder()
                            .setCustomId(NO_BUTTON_NAME)
                            .setLabel("No")
                            .setStyle(ButtonStyle.Danger)
                    )
                ]
            });

        message.awaitMessageComponent({ filter, componentType: ComponentType.Button, time: 300000 })
            .then(async i => {
                if (i.customId === NO_BUTTON_NAME)
                    await interaction.editReply({ content: "Save cancelled.", components: [] });
                else
                    await this.handleButton(interaction);
            })
            .catch(err => logger.error(formatError(err)));
    },
    handleButton: async function(interaction)
    {
        let { botUser, guildId, error } = await playerUtils.authorize(interaction, this.permissionLevel)
        if (error) {
            await interaction.followUp(error);
            return;
        }

        const guild = botUser.guilds.find(g => g.guildId === guildId);

        const planName = interaction.options.getString("name");
        
        await this.savePlan(guild.comlinkGuildId, planName);

        await interaction.editReply({ content: `Plan **${planName}** saved. Load it anytime in the future with **/ops plan load**.`, components: [] });
    },
    planExists: async function(comlinkGuildId, planName)
    {
        let existingPlan = await database.db.any("SELECT COUNT(*) FROM tb_plan_ops_saved WHERE comlink_guild_id = $1 AND plan_name = $2", [comlinkGuildId, planName])
        return existingPlan[0].count > 0;
    },
    savePlan: async function(comlinkGuildId, planName)
    {
        if (!planName || planName.length > opsCommon.MAX_PLAN_NAME_LENGTH)
        {
            return `Plan name must be between 1 and ${opsCommon.MAX_PLAN_NAME_LENGTH} characters.`;
        }

        const tbOpsData = await database.db.any(`
            SELECT timestamp,phase,operation_req_id,preload,allycode,total_needed,total_available
            FROM tb_ops
            WHERE comlink_guild_id = $1`,
            [comlinkGuildId]);
        
        await database.db.any(
            `DELETE FROM tb_plan_ops_saved WHERE comlink_guild_id = $1 AND plan_name = $2;
            INSERT INTO tb_plan_ops_saved (comlink_guild_id, planets_by_phase, plan_name, tb_ops_json)
            (SELECT comlink_guild_id, planets_by_phase, $2, $3
            FROM tb_plan
            WHERE comlink_guild_id = $1)
            `, [comlinkGuildId, planName, JSON.stringify(tbOpsData)]
        )

        return `Plan **${planName}** saved.`;
    }
}
