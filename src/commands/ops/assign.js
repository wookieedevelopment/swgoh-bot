const { SlashCommandSubcommandBuilder } = require('@discordjs/builders');
const swapiUtils = require("../../utils/swapi");
const opsCommon = require("./common");
const database = require("../../database");
const unitsUtils = require('../../utils/units');
const playerUtils = require('../../utils/player');
const guildUtils = require("../../utils/guild");
const { logger, formatError } = require("../../utils/log");

module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("a")
        .setDescription("Assign an operation unit to a player.")
        .addIntegerOption(o => 
            o.setName("phase")
            .setDescription("The phase of the assignment.")
            .setRequired(true)
            .setMinValue(1)
            .setMaxValue(6))
        .addIntegerOption(o => 
            o.setName("op")
            .setDescription("The operation unit to assign.")
            .setRequired(true)
            .setAutocomplete(true))
        .addStringOption(o =>
            o.setName("player")
            .setDescription("The player to assign")
            .setRequired(true)
            .setAutocomplete(true)),
    permissionLevel: playerUtils.UserRoles.GuildAdmin,
    interact: async function(interaction)
    {
        let { botUser, guildId, error } = await playerUtils.authorize(interaction, this.permissionLevel);     
        if (error) {
            await interaction.editReply(error);
            return;
        }

        let oldTbOpsId = interaction.options.getInteger("op");
        let allycode = interaction.options.getString("player");
        let phase = interaction.options.getInteger("phase");
        
        if (!allycode)
        {
            await interaction.editReply({ content: "Please provide a player: `/op a phase:# op:ASDF player:XXX`" });
            return;
        }

        await interaction.editReply({ content: `Verifying player has unit...`});
        let playerData = await swapiUtils.getPlayer(allycode);

        if (!playerData)
        {
            await interaction.editReply({ content: `No player found with allycode *${allycode}*.`});
            return;
        }


        // check that the player is in the guild
        if (guildId !== (await guildUtils.getGuildIdByAllyCode(allycode)))
        {
            await interaction.editReply({ content: `${swapiUtils.getPlayerName(playerData)} (${allycode}) is not in your guild. If this is incorrect, run \`/guild refresh roster\` and try again.`});
            return;
        }

        let unitDefId;
        // check if the player can fill the op
        try {
            unitDefId = await this.verifyHasUnit(playerData, oldTbOpsId);
        } catch (e) {
            await interaction.editReply({ content: e.message });
            return;
        }

        await interaction.editReply({ content: `Verified. Updating assignment...`});

        const comlinkGuildId = swapiUtils.getPlayerGuildId(playerData);

        /*
            things to do in this process:
            1. if the player has the unit assigned to another operation in the same phase, then clear out
               that assignment (unit in the new phase)
            2. add the assignment in the new phase, copying over any precalculated data about number of tbat unit available
               in the new phase (or setting equal to 1 if this is the first unit).
            3. update total_needed column based on new counts for the old phase
            4. delete the old assignment if it was in a different phase
            5. update total_needed for the new phase
        */
        let updatedData = await database.db.multi(
            `
            with tb_ops_to_update as (
                select tb_ops_id, operation_req_id, allycode
                from tb_ops 
                left join operation_req using (operation_req_id)
                where 
                    unit_def_id = (SELECT unit_def_id FROM operation_req WHERE operation_req_id = (SELECT operation_req_id FROM tb_ops WHERE tb_ops_id = $2)) and
                    allycode = $1 AND
                    comlink_guild_id = $4 AND
                    phase = $3
            )
            update tb_ops
            set allycode = null
            where tb_ops_id = (select tb_ops_id from tb_ops_to_update)
            returning (select planet as old_planet from operation_req opr where opr.operation_req_id = (select operation_req_id from tb_ops_to_update)),
                    (select operation as old_operation from operation_req opr where opr.operation_req_id = (select operation_req_id from tb_ops_to_update)),
                    (select player_name as old_player from guild_players where allycode = (select allycode from tb_ops_to_update));

            INSERT INTO tb_ops (comlink_guild_id, plan_name, timestamp, phase, operation_req_id, preload, allycode, total_needed, total_available)
            SELECT 
                comlink_guild_id,
                plan_name,
                timestamp,
                $3,
                operation_req_id,
                preload,
                $1,
                (SELECT COUNT(*) FROM tb_ops WHERE comlink_guild_id = $4 AND phase = $3 AND operation_req_id IN (SELECT operation_req_id FROM operation_req WHERE unit_def_id = $5)),
                total_available
            FROM tb_ops
            WHERE tb_ops_id = $2
            ON CONFLICT (comlink_guild_id, phase, operation_req_id) 
                DO UPDATE
                SET allycode = $1
            RETURNING
                (SELECT unit_name FROM operation_req WHERE unit_def_id = $5 LIMIT 1),
                (SELECT player_name as new_player from guild_players where allycode = $1);

            UPDATE tb_ops
            SET total_needed = total_needed-1
            WHERE tb_ops_id IN 
                (SELECT tb_ops_id FROM tb_ops
                WHERE comlink_guild_id = $4 
                      AND phase = (SELECT phase FROM tb_ops WHERE tb_ops_id = $2) 
                      AND operation_req_id IN (SELECT operation_req_id FROM operation_req WHERE unit_def_id = $5));
            
            DELETE FROM tb_ops
            WHERE tb_ops_id = $2 and phase <> $3
            RETURNING phase as old_phase;

            WITH tb_ops_to_update as (
                select tb_ops_id
                from tb_ops 
                WHERE 
                    comlink_guild_id = $4
                    AND phase = $3
                    AND operation_req_id IN (SELECT operation_req_id FROM operation_req WHERE unit_def_id = $5)
            )
            UPDATE tb_ops
            SET total_needed = (SELECT COUNT(*) FROM tb_ops_to_update)
            WHERE tb_ops_id IN (SELECT tb_ops_id FROM tb_ops_to_update);
            `, 
                    [allycode, oldTbOpsId, phase, comlinkGuildId, unitDefId]);

        let responseText = "";
        if (updatedData[3].length > 0 && phase != updatedData[3][0].old_phase)
        {
            responseText += `Assignment moved from Phase ${updatedData[3][0].old_phase} to Phase ${phase}.\n`;
        }

        if (updatedData[0].length > 0)
        {
            responseText += `Removed assignment ${updatedData[1][0].unit_name} from ${updatedData[0][0].old_planet} Operation ${updatedData[0][0].old_operation} for ${updatedData[0][0].old_player} in Phase ${updatedData[3][0]?.old_phase ?? phase}.\n`;
        }

        responseText += `Assigned ${updatedData[1][0].unit_name} to ${updatedData[1][0].new_player} in Phase ${phase}.`;
        
        await interaction.editReply({ content: responseText });
    },
    verifyHasUnit: async function(playerData, op)
    {
        let guildOpData = await database.db.oneOrNone("SELECT * FROM tb_ops WHERE tb_ops_id = $1", [op]);

        if (!guildOpData) throw new Error(`Operation not found. If this persists, contact the developer.`);
        if (guildOpData.comlink_guild_id != swapiUtils.getPlayerGuildId(playerData)) 
        {
            logger.error(`Illegal attempt to assign tb_ops_id ${op} by ${interaction.user.id}.`);

            throw new Error(`You do not have permission to do that. Use the autocomplete suggestions.`);
        }

        const allOps = await opsCommon.getAllOperationRequirements();
        let opMatch = allOps.find(o => o.id === guildOpData.operation_req_id);
        
        let {errorCode, unit} = swapiUtils.findUnit(playerData, opMatch.unitDefId, { minRelicLevel: opMatch.minRelicLevel, minRarity: 7 });

        switch (errorCode)
        {
            case swapiUtils.FIND_UNIT_ERROR_CODES.CORRUPT_PLAYER_DATA:
                throw new Error(`Could not find unit ${opMatch.unitName} for ${swapiUtils.getPlayerName(playerData)}.`);
            case swapiUtils.FIND_UNIT_ERROR_CODES.NOT_UNLOCKED:
                throw new Error(`${swapiUtils.getPlayerName(playerData)} does not have ${opMatch.unitName}.`);
            case swapiUtils.FIND_UNIT_ERROR_CODES.TOO_LOW_RELIC:
                throw new Error(`${swapiUtils.getPlayerName(playerData)} does not have ${opMatch.unitName} at a high enough relic level.
Required: R${opMatch.minRelicLevel.toString()}
Current Level: R${swapiUtils.getUnitRelicLevel(unit) - 2}`);
            case swapiUtils.FIND_UNIT_ERROR_CODES.TOO_LOW_RARITY:
                throw new Error(`${swapiUtils.getPlayerName(playerData)} does not have ${opMatch.unitName} at 7*.`);
            default:
                return opMatch.unitDefId;
        }
    },
    autocomplete: async function(interaction)
    {
		const focusedOption = interaction.options.getFocused(true);

        let { botUser, guildId, error } = await playerUtils.authorize(interaction, this.permissionLevel)
        if (error) {
            let errVal = (focusedOption.name === "op") ? -1 : "x";
            await interaction.respond([{ name: error, value: errVal }]);
            return;
        }

        let choices;

        if (focusedOption.name === "op")
        {
            let phase = interaction.options.getInteger("phase");
            let op = focusedOption.value;
            let defIdFilter = unitsUtils.getUnitDefIdByNickname(op, false);

            choices = await database.db.any(
`SELECT planet || '-' || operation || ' // ' || unit_name || ' // ' || COALESCE(player_name, 'Unassigned') || ' (Currently in Phase ' || phase || ')' name, tb_ops_id value
FROM tb_ops
LEFT OUTER JOIN guild_players USING (allycode)
JOIN operation_req USING (operation_req_id)
WHERE tb_ops.comlink_guild_id = $1
AND ((player_name IS NOT NULL AND player_name ILIKE $2) OR unit_name ILIKE $2 OR planet ILIKE $2${defIdFilter?.length > 0 ? `OR unit_def_id IN ($4:csv)` : ""})
AND planet IN (SELECT DISTINCT planet FROM tb_ops LEFT JOIN operation_req using (operation_req_id) WHERE comlink_guild_id = $1 AND phase = $3) 
ORDER BY player_name NULLS FIRST, planet, unit_name
LIMIT 25`, [botUser.guilds.find(g => g.guildId === guildId)?.comlinkGuildId, `%${op}%`, phase, defIdFilter]);
        }
        else if (focusedOption.name === "player")
        {
            let names = await database.db.any(
                `select player_name, allycode
                from guild_players 
                WHERE guild_id = $1 AND (player_name ILIKE $2 OR allycode LIKE $2)
                LIMIT 25
                `, [guildId, `%${focusedOption.value}%`]
                        );
                        
            choices = names.map(choice => ({ name: `${choice.player_name ? choice.player_name : "Unknown Player"} (${choice.allycode})`, value: choice.allycode }));
        }


		await interaction.respond(choices);
    }
}