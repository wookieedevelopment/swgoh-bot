const { SlashCommandSubcommandBuilder, EmbedBuilder } = require('@discordjs/builders');
const playerUtils = require("../../utils/player");
const opsCommon = require("./common");
const guildUtils = require("../../utils/guild");
const swapiUtils = require("../../utils/swapi");
const discordUtils = require("../../utils/discord");
const database = require("../../database");
const tools = require("../../utils/tools");
const { parseAsync } = require('json2csv');
const { AttachmentBuilder } = require('discord.js');
const constants = require('../../utils/constants');
const entitlements = require('../../utils/entitlements');

module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("rares")
        .setDescription("Check what rare units a player has for TB operations")
        .addStringOption(o => 
            o.setName("allycode")
            .setDescription("Ally Code of player to test")
            .setRequired(false))
        .addStringOption(o =>
            o.setName("member")
            .setDescription("Member of your guild")
            .setRequired(false)
            .setAutocomplete(true))
        .addIntegerOption(o =>
            o.setName("rare-threshold")
            .setDescription(`Rare threshold (1-20, default: your guild's /ops config or ${opsCommon.CONFIG_DEFAULTS.RARE_THRESHOLD})`)
            .setRequired(false)
            .setMinValue(1)
            .setMaxValue(20))
        ,
    interact: async function(interaction)
    {   
        let isSubscriber = await playerUtils.isSubscriberInteraction(interaction);
        if (!isSubscriber && interaction.user.id != '516747838793711632' /* cucs */)
        {
            await interaction.editReply(entitlements.generateNotSubscribedResponseObject());
            return;
        }

        let allycodeToTest = interaction.options.getString("member") ?? interaction.options.getString("allycode");

        // if the user provided an allycode, make sure it's ok
        if (allycodeToTest) allycodeToTest = tools.cleanAndVerifyStringAsAllyCode(allycodeToTest);

        let rareThreshold = interaction.options.getInteger("rare-threshold");
        
        let { botUser, guildId, error } = await playerUtils.authorize(interaction);

        let bestAlt = await playerUtils.chooseBestAlt(botUser, guildId, null, true);
        let guildIdToTestAgainst = bestAlt.comlinkGuildId;
        let guildName = bestAlt.guildName;

        if (rareThreshold == null)
        {
            rareThreshold = await getRareThreshold(guildIdToTestAgainst);
        }

        const editMessage = await interaction.editReply("Checking cached data...");
        let allGuildUnitsCount = await getGuildUnitsCountCachedData(guildIdToTestAgainst);

        let guildData;
        if (!allGuildUnitsCount)
        {
            await editMessage.edit("Cached data too old. Reloading guild data...");
            // load data, pass it later to getGuildRareOpUnits
            guildData = await guildUtils.loadGuildPlayerData(
                { 
                    comlinkGuildId: guildIdToTestAgainst, 
                    editMessageId: editMessage.id, 
                    editMessageChannelId: editMessage.channelId ?? editMessage.channel.id
                });

        }

        await editMessage.edit("Determining guild rare units...");

        // this will also update the allGuildUnitsCount in database
        let guildRareUnits = await getGuildRareOpUnits(guildData, rareThreshold, allGuildUnitsCount);

        let response;
        if (allycodeToTest)
        {
            await editMessage.edit("Checking what rare units the player has...");
            let { player, playerRareUnits } = await findPlayerRareUnits({ allycodeToTest: allycodeToTest }, guildRareUnits);

            response = await generateResponseForPlayer(player, playerRareUnits, guildRareUnits, guildName, rareThreshold);    
        }
        else
        {
            await editMessage.edit("Checking how many rare units each player in your guild has...");
            if (!guildData)
            {
                await editMessage.edit("Loading guild data...");
                guildData = await guildUtils.loadGuildPlayerData(
                    { 
                        comlinkGuildId: guildIdToTestAgainst, 
                        editMessageId: editMessage.id, 
                        editMessageChannelId: editMessage.channelId ?? editMessage.channel.id
                    });
            }

            let allPlayersRareUnits = await findAllPlayersRareUnits(guildData, guildRareUnits);
            response = await generateResponseForGuild(guildName, guildRareUnits, allPlayersRareUnits, rareThreshold);
        }

        await editMessage.edit(response);
    },
    autocomplete: async function(interaction)
    {
        let { botUser, guildId, error } = await playerUtils.authorize(interaction);
        let guilds;
        if (!guildId)
        {
            guilds = botUser.guilds.map(g => g.guild_id);
        }
        else
        {
            guilds = [guildId];
        }

        const focusedValue = interaction.options.getFocused();

        let names = await database.db.any(
`select player_name, allycode
from guild_players 
WHERE guild_id IN ($1:csv) AND (player_name ILIKE $2 OR allycode LIKE $2)
LIMIT 25
`, [guilds, `%${focusedValue}%`]
        )

		await interaction.respond(
			names.map(choice => ({ name: `${choice.player_name} (${choice.allycode})`, value: choice.allycode }))
		);
    },
}

async function getRareThreshold(comlinkGuildId)
{
    let opsConfig = await opsCommon.loadGuildTbConfig(comlinkGuildId);

    return opsConfig.rare_threshold;
}

async function getGuildUnitsCountCachedData(comlinkGuildId)
{
    let cachedData =await database.db.any(
        `SELECT data, timestamp 
        FROM guild_units_count_cache 
        WHERE comlink_guild_id = $1 AND timestamp >= now() - interval '12 hours'`, [comlinkGuildId]);
        
    if (cachedData?.length > 0) return cachedData[0].data;

    return null;
}

async function saveGuildUnitsCountCacheData(comlinkGuildId, allGuildUnits)
{
    await database.db.any(`
    INSERT INTO guild_units_count_cache (comlink_guild_id, data, timestamp)
    VALUES ($1, $2, $3)
    ON CONFLICT (comlink_guild_id) DO UPDATE SET data = EXCLUDED.data, timestamp = EXCLUDED.timestamp`,
    [comlinkGuildId, JSON.stringify(allGuildUnits), new Date()]);
}

async function getGuildRareOpUnits(guildData, rareThreshold, allGuildUnits)
{
    // load all operations units
    // loop through all operations units and find any that have < rareThreshold players that can complete and >0 players

    let guildRareUnits;
    
    if (allGuildUnits?.length > 0)
    {
        // use the cache to find rare units
        guildRareUnits = allGuildUnits.filter(u => u.countInGuild <= rareThreshold);
        return guildRareUnits;
    }

    let operationReqs = await opsCommon.getAllOperationRequirements();
    let unitsList = await swapiUtils.getUnitsList();
    guildRareUnits = [];
    allGuildUnits = []; 
    saveToCache = true;  
    
    for (let r = 0; r < operationReqs.length; r++)
    {
        let req = operationReqs[r];
        
        // if it was already looked at, increment count & add the planet
        let matchingInGuild = allGuildUnits.find(gu => gu.unitDefId === req.unitDefId && (gu.minRelicLevel == null || gu.minRelicLevel === req.minRelicLevel));
        if (matchingInGuild)
        {
            matchingInGuild.totalNeeded++;
            if (!matchingInGuild.planets.find(p => p === req.planet)) matchingInGuild.planets.push(req.planet);

            continue;
        }

        let unit = unitsList.find(u => swapiUtils.getUnitDefId(u) === req.unitDefId);
        let isShip = swapiUtils.getUnitCombatType(unit) === constants.COMBAT_TYPES.FLEET;

        let countInGuild = guildData.players.filter(
                                p => 
                                    swapiUtils.getPlayerRoster(p).find(u => 
                                        swapiUtils.getUnitDefId(u) === req.unitDefId
                                        && (
                                            (isShip && swapiUtils.getUnitRarity(u) === 7) ||  
                                            (!isShip && swapiUtils.getUnitRelicLevel(u) - 2 >= req.minRelicLevel)
                                        ))
                                ).length;
        
        let obj = {
            unitDefId: req.unitDefId,
            unitName: tools.removeAccents(req.unitName),
            planets: [req.planet],
            minRelicLevel: isShip ? null : req.minRelicLevel,
            countInGuild: countInGuild,
            totalNeeded: 1
        };

        allGuildUnits.push(obj);
        if (countInGuild <= rareThreshold)
        {
            guildRareUnits.push(obj);
        }
    }

    if (saveToCache) await saveGuildUnitsCountCacheData(guildData.comlinkGuildId, allGuildUnits);

    return guildRareUnits;
}

async function findPlayerRareUnits({ allycodeToTest, playerData }, guildRareUnits)
{
    if (allycodeToTest)
    {
        // get player from API
        playerData = await swapiUtils.getPlayer(allycodeToTest);
    }

    let matchingRares = [];

    // loop through guildRareUnits and see what the player has
    for (let gru = 0; gru < guildRareUnits.length; gru++)
    {
        let guildRareUnitObj = guildRareUnits[gru];
        let unitInRoster = swapiUtils.getPlayerRoster(playerData).find(u => 
            swapiUtils.getUnitDefId(u) === guildRareUnitObj.unitDefId
            && swapiUtils.getUnitRelicLevel(u) - 2 >= guildRareUnitObj.minRelicLevel);

        if (!unitInRoster) continue;

        matchingRares.push(guildRareUnitObj);
    }

    return { player: playerData, playerRareUnits: matchingRares };
}

async function findAllPlayersRareUnits(guildData, guildRareUnits)
{
    let allPlayersRareUnits = [];
    for (let p of guildData.players)
    {
        let { player, playerRareUnits } = await findPlayerRareUnits({ playerData: p }, guildRareUnits);
        allPlayersRareUnits.push({
            allycode: swapiUtils.getPlayerAllycode(p),
            name: swapiUtils.getPlayerName(p),
            rareUnitCount: playerRareUnits.length
        });
    }

    return allPlayersRareUnits;
}

async function generateGuildUnitCsvAttachment(guildUnits)
{
    const csvParsingOptions = { 
        defaultValue: 0,
        fields: ["unitName", "unitDefId", "minRelicLevel", "planets", "totalNeeded", "countInGuild"],
        encoding: 'utf8'
    };
    let csvText = await parseAsync(guildUnits, csvParsingOptions);
    let buffer = Buffer.from(csvText);
    let csvAttachment = new AttachmentBuilder(buffer, { name: `guild_rare_ops_units_${new Date().getTime()}.csv`});

    return csvAttachment;
}

async function generateResponseForPlayer(player, playerRareUnits, guildRareUnits, guildName, rareThreshold)
{
    let response = {};

    let embed = new EmbedBuilder()
        .setTitle(`${playerRareUnits.length} Rare TB Unit${playerRareUnits.length === 1 ? "" : "s"} - ${player.name} (${swapiUtils.getPlayerAllycode(player)})`)
        .setColor(0x9050FF);

    if (playerRareUnits.length == 0)
    {
        embed.setDescription("Player has no units that are considered rare for your guild.");   
    }
    else
    {
        let desc = "- " + playerRareUnits.
            map(u => 
`**R${u.minRelicLevel} ${u.unitName}**
  - Guild Count: ${u.countInGuild}
  - Planets: ${u.planets.join(", ")}`).
            join("\n- ");

        if (desc.length > discordUtils.MAX_DISCORD_EMBED_DESCRIPTION_LENGTH)
        {
            desc = desc.slice(0, discordUtils.MAX_DISCORD_EMBED_DESCRIPTION_LENGTH);
            let startOfLastUnit = desc.lastIndexOf("**R", 3800);
            desc = desc.slice(0, startOfLastUnit - 2); // -2 for "- "

            desc += `Plus more. Total: ${playerRareUnits.length} units that are rare for your guild.`;
        }
        embed.setDescription(desc);
    }

    embed.setFooter({ text: `Guild: ${guildName}\nRare threshold: ${rareThreshold}`})

    response.embeds = [embed];

    let csvAttachment = await generateGuildUnitCsvAttachment(guildRareUnits);
    
    response.files = [csvAttachment];
    response.content = `Any operations where **${guildName}** has <= ${rareThreshold} of a unit in the roster are considered rare.
Attached .csv file is a list of all rare units for your guild.`;
    
    return response;
}

async function generateResponseForGuild(guildName, guildRareUnits, allPlayersRareUnits, rareThreshold)
{
    let response = {};

    let embed = new EmbedBuilder()
        .setTitle(`Count of Rare TB Operations Units - ${guildName}`)
        .setColor(0x9050FF);

    allPlayersRareUnits.sort((a, b) => b.rareUnitCount - a.rareUnitCount);

    let desc = "1. " + allPlayersRareUnits.
        map(p => `${p.name} (${p.allycode}): ${p.rareUnitCount}`).
        join("\n- ");

    embed.setDescription(desc);
    embed.setFooter({ text: `Guild: ${guildName}\nRare threshold: ${rareThreshold}`})
    
    response.embeds = [embed];

    let csvAttachment = await generateGuildUnitCsvAttachment(guildRareUnits);
    
    response.files = [csvAttachment];
    response.content = `Any operations where **${guildName}** has <= ${rareThreshold} of a unit in the roster are considered rare`;
    return response;
}