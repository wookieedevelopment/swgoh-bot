const { SlashCommandSubcommandBuilder } = require('@discordjs/builders');
const playerUtils = require("../../utils/player");
const database = require("../../database");
const guildUtils = require("../../utils/guild.js");
const opsCommon = require("./common");
const tbCommon = require("../tb/common.js");
const { EmbedBuilder } = require('discord.js');

module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("player-list")
        .setDescription("List player participation settings for your guild"),
    permissionLevel: playerUtils.UserRoles.User,  
    interact: async function(interaction)
    {
        let { botUser, guildId, error } = await playerUtils.authorize(interaction, playerUtils.UserRoles.GuildAdmin)
        if (error) {
            await interaction.editReply(error);
            return;
        }
        let guild = botUser.guilds.find(g => g.guildId === guildId);

        let response = await this.listPlayerAvailability(guild);

        await interaction.editReply(response);
    },
    listPlayerAvailability: async function(guild)
    {
        let data = await database.db.any(
            `SELECT allycode,phase,available 
            FROM tb_guild_player 
            WHERE comlink_guild_id = $1 AND available = FALSE`,
            [guild.comlinkGuildId]);

        if (data.length === 0) return { content: "All members are listed as available for all phases."};

        let guildData = await guildUtils.getGuildDataFromAPI({ comlinkGuildId: guild.comlinkGuildId }, { useCache: false });
        let embed = new EmbedBuilder()
            .setTitle(`Players NOT Available for TB Ops: ${guild.guildName}`)
            .setColor("Blue")
            .setFooter({ text: "Adjust player availability with `/ops player`" });

        let notAvailableForWholeTB = data
                    .filter(d => d.phase == null 
                                && data.filter(d2 => d2.allycode === d.allycode).length === tbCommon.TB_PHASES.length)
                    .map(d => d.allycode);
        if (notAvailableForWholeTB.length > 0)
        {
            let field = 
                { 
                    name: ":globe_with_meridians: Full TB: Not Available", 
                    value: "- " + guildUtils.getGuildRoster(guildData)
                            .filter(m => notAvailableForWholeTB.find(p => p.allycode === m.allyCode))
                            .map(m => `${m.name} (${m.allyCode})`)
                            .join("\n- ")
                };

            embed.addFields(field);
        }

        for (let phase of tbCommon.TB_PHASES)
        {
            let unavailablePlayers = data.filter(p => p.phase === phase.n);
            let value;

            if (unavailablePlayers.length == 0)
            {
                value = `All players are available for ${phase.label}.`;
            }
            else
            {
                let matchingPlayerText = guildUtils.
                            getGuildRoster(guildData).
                            filter(m => unavailablePlayers.find(p => p.allycode === m.allyCode)).
                            map(m => `${m.name} (${m.allyCode})`);

                matchingPlayerText.sort();
                value = "- " + matchingPlayerText.join("\n- "); 
            }


            let field = {
                name: `Phase ${phase.n} (${unavailablePlayers.length} player${unavailablePlayers == 1 ? "" : "s"})`,
                value: value
            };

            embed.addFields(field);
        }

        return { embeds: [embed]};
    }
}