const { SlashCommandSubcommandBuilder } = require('@discordjs/builders');
const playerUtils = require("../../utils/player");
const database = require("../../database");
const discordUtils = require("../../utils/discord.js");
const opsCommon = require("./common");
const { ChannelType } = require('discord.js');

module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("config")
        .setDescription("Configure TB operations settings for your guild.")
        .addIntegerOption(o =>
            o.setName("rare-threshold")
            .setDescription("Unit availability for determining if an assignment is rare.")
            .setRequired(false)
            .setMaxValue(15)
            .setMinValue(1))
        .addBooleanOption(o => 
            o.setName("rare-by-default")
            .setDescription("Show rare only view by default for assignments")
            .setRequired(false))
        .addIntegerOption(o =>
            o.setName("players-per-image")
            .setDescription(`Number of players to show per image in the players view (default: ${opsCommon.CONFIG_DEFAULTS.PLAYERS_PER_IMAGE}).`)
            .setRequired(false)
            .setMaxValue(50)
            .setMinValue(10))
        .addChannelOption(o => 
            o.setName("assignments-channel")
            .setDescription("Channel to post assignments graphics in")
            .setRequired(false)
            .addChannelTypes(ChannelType.GuildText))
        .addIntegerOption(o => 
            o.setName("post-assignments-on-send")
            .setDescription("Post to assignments-channel when you Send Assignments")
            .setRequired(false)
            .addChoices(...opsCommon.POST_ASSIGNMENTS_ON_SEND_OPTIONS))
        .addStringOption(o =>
            o.setName("tiebreaker")
            .setDescription("Set tiebreaker for operation assignment selection (default: random)")
            .setRequired(false)
            .addChoices(
                { name: opsCommon.OPERATION_SELECTION_TIEBREAKER.RANDOM, value: opsCommon.OPERATION_SELECTION_TIEBREAKER.RANDOM },
                { name: opsCommon.OPERATION_SELECTION_TIEBREAKER.HIGHEST_UNIT_GP, value: opsCommon.OPERATION_SELECTION_TIEBREAKER.HIGHEST_UNIT_GP },
                { name: opsCommon.OPERATION_SELECTION_TIEBREAKER.LOWEST_UNIT_GP, value: opsCommon.OPERATION_SELECTION_TIEBREAKER.LOWEST_UNIT_GP },
                { name: opsCommon.OPERATION_SELECTION_TIEBREAKER.HIGHEST_PLAYER_GP, value: opsCommon.OPERATION_SELECTION_TIEBREAKER.HIGHEST_PLAYER_GP },
                { name: opsCommon.OPERATION_SELECTION_TIEBREAKER.LOWEST_PLAYER_GP, value: opsCommon.OPERATION_SELECTION_TIEBREAKER.LOWEST_PLAYER_GP }
            )),
    permissionLevel: playerUtils.UserRoles.GuildAdmin,
    interact: async function(interaction)
    {
        let rt = interaction.options.getInteger("rare-threshold");
        let rareByDefault = interaction.options.getBoolean("rare-by-default");
        let playersPerImage = interaction.options.getInteger("players-per-image");
        let tiebreaker = interaction.options.getString("tiebreaker");
        let assignmentsChannel = interaction.options.getChannel("assignments-channel");
        let postAssignmentsOnSend = interaction.options.getInteger("post-assignments-on-send");

        let { botUser, guildId, error } = await playerUtils.authorize(interaction, playerUtils.UserRoles.GuildAdmin)
        if (error) {
            await interaction.editReply(error);
            return;
        }
        
        let guild = botUser.guilds.find(g => g.guildId === guildId);

        if (!guild)
        {
            await interaction.editReply("You are not a WookieeBot admin for a guild linked to this channel or server.");
            return;
        }

        let response = "";
        if (rt != null || 
            rareByDefault != null || 
            playersPerImage != null || 
            tiebreaker != null || 
            assignmentsChannel != null || 
            postAssignmentsOnSend != null)
        {
            let successMessages = await this.updateConfigSettings(guild.comlinkGuildId, {
                rareThreshold: rt,
                rareByDefault: rareByDefault,
                playersPerImage: playersPerImage,
                tiebreaker: tiebreaker,
                assignmentsChannel: assignmentsChannel,
                postAssignmentsOnSend: postAssignmentsOnSend
            });

            response = `${successMessages.join("\n")}\n\n`;
        }
        
        response += await this.showCurrentConfig(guild.comlinkGuildId);

        await interaction.editReply(response);
    },
    updateConfigSettings: async function(comlinkGuildId, { rareThreshold, rareByDefault, playersPerImage, tiebreaker, assignmentsChannel, postAssignmentsOnSend })
    {
        await opsCommon.insertOrUpdateTbConfigData(comlinkGuildId, { rareThreshold, rareByDefault, playersPerImage, tiebreaker, assignmentsChannel, postAssignmentsOnSend });

        let responses = [];
        if (rareThreshold != undefined && rareThreshold != null)
        {
            responses.push(`${discordUtils.symbols.pass} Unit Rarity Threshold set to ${rareThreshold}.`);
        }
        
        if (rareByDefault != undefined && rareByDefault != null)
        {
            responses.push(`${discordUtils.symbols.pass} Show Rare Only by Default set to ${rareByDefault ? "True" : "False"}.`);

            // special situation, update all current tb plan settings to have rareOnly set to true
            if (rareByDefault)
            {
                let plan = await opsCommon.getGuildTbPlanData(comlinkGuildId);

                let phases = Object.keys(plan);
                for (let phase of phases)
                {
                    let phaseData = plan[phase];
                    for (let planetDataIx = 0; planetDataIx < phaseData.length; planetDataIx++)
                    {
                        phaseData[planetDataIx].rareOnly = true;
                    }
                }

                await opsCommon.saveGuildTbPlanData(comlinkGuildId, { plan: plan });
            }
        }
        
        if (playersPerImage != undefined && playersPerImage != null)
        {
            responses.push(`${discordUtils.symbols.pass} Players per Image set to ${playersPerImage}.`);
        }
        
        if (tiebreaker != undefined && tiebreaker != null)
        {
            let allowedValues = Object.values(opsCommon.OPERATION_SELECTION_TIEBREAKER);
            if (allowedValues.indexOf(tiebreaker) != -1)
                responses.push(`${discordUtils.symbols.pass} Tiebreaker set to ${tiebreaker}.`);
            else 
                responses.push(`${discordUtils.symbols.fail} Invalid tiebreaker. Choose from the list.`)
        }

        if (assignmentsChannel != undefined && assignmentsChannel != null) {
            responses.push(`${discordUtils.symbols.pass} Assignments channel set to ${assignmentsChannel}.`);
        }

        if (postAssignmentsOnSend != undefined && postAssignmentsOnSend != null) {
            let msg = `Assignments ${postAssignmentsOnSend ? "will" : "will not"} be posted to your assignments channel when you use Send Assignments to Guild.`;
            responses.push(discordUtils.symbols.pass + msg)
        }

        return responses;
    },
    showCurrentConfig: async function(comlinkGuildId)
    {
        let config = await opsCommon.loadGuildTbConfig(comlinkGuildId);

        let paosText = opsCommon.POST_ASSIGNMENTS_ON_SEND_OPTIONS.find(o => o.value === config.post_assignments_on_send).name;
        
        return `Current TB Operations Configuration Settings:
> Unit Rarity Threshold: ${config.rare_threshold}
> Show Rare Only by Default: ${config.rare_by_default ? "Yes" : "No"}
> Players Shown per Image: ${config.players_per_image}
> Operation Assignments Tiebreaker: ${config.tiebreaker}
> Assignments channel: ${discordUtils.makeChannelTaggable(config.assignments_channel) ?? "Not Configured"}
> Post to Assignments Channel on send: ${paosText}

Update values with **/ops config**`;
    }
}