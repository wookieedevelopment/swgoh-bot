const { SlashCommandSubcommandBuilder, EmbedBuilder } = require('@discordjs/builders');
const playerUtils = require("../../utils/player");
const guildUtils = require("../../utils/guild");
const database = require("../../database");
const tbCommon = require("../tb/common");
const opsCommon = require("./common");
const sendOps = require("./functions/sendOps");

const PHASE_CHOICES = tbCommon.TB_PHASES.map(p => { return { name: p.label, value: p.n }; });

module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("show")
        .setDescription("Show assignments for a player in your guild (default: yours)")
        .addIntegerOption(o =>
            o.setName("phase")
            .setDescription("Phase")
            .setRequired(true)
            .addChoices(
                ...PHASE_CHOICES
            ))
        .addStringOption(o =>
            o.setName("player")
            .setDescription("Player whose assignments to show. Choose from the list or specify ally code.")
            .setRequired(false)
            .setAutocomplete(true)),
    permissionLevel: playerUtils.UserRoles.User,
    interact: async function(interaction)
    {
        let { botUser, guildId, error } = await playerUtils.authorize(interaction, this.permissionLevel);

        if (error) {
            await interaction.editReply(error);
            return;
        }

        let phase = interaction.options.getInteger("phase");
        let allycode = interaction.options.getString("player");
        
        let guild = botUser.guilds.find(g => g.guildId === guildId);
        let comlinkGuildId = guild.comlinkGuildId;

        try {
            allycode = await checkAllycode(allycode, guild);
        } catch (e)
        {
            await interaction.editReply({ embeds: [new EmbedBuilder().setTitle("Error").setColor(0x500000).setDescription(e.message)] });
            return;
        }

        let response = await this.getShowOpsResponse(comlinkGuildId, phase, allycode);
        await interaction.editReply(response);
    },
    getShowOpsResponse: async function(comlinkGuildId, phase, allycode)
    {
        let opsData = await opsCommon.getOpsData(comlinkGuildId);
        let phaseData = opsData.phases[phase-1];
        
        let playerAssignments = phaseData.playerAssignments.find(a => a.allycode === allycode);

        if (!playerAssignments || playerAssignments.length === 0)
        {
            return { content: `No assignments for Phase ${phase}.` };
        }

        let message = await sendOps.createAssignmentMessage(phase, playerAssignments);
        return message;
    },
    autocomplete: async function(interaction)
    {
        let { botUser, guildId, error } = await playerUtils.authorize(interaction, this.permissionLevel)
        if (error) {
            await interaction.respond([{ name: "You do not have permission to do this.", value: "x" }]);
            return;
        }

        const focusedValue = interaction.options.getFocused();

        let names = await database.db.any(
`select player_name, allycode
from guild_players 
WHERE guild_id = $1 AND (player_name ILIKE $2 OR allycode LIKE $2)
LIMIT 25
`, [guildId, `%${focusedValue}%`]
        )

		await interaction.respond(
			names.map(choice => ({ name: `${choice.player_name ? choice.player_name : "Unknown Player"} (${choice.allycode})`, value: choice.allycode }))
		);
    },
}

async function checkAllycode(allycode, guild)
{
    if (allycode)
    {
        allycode = playerUtils.cleanAndVerifyStringAsAllyCode(allycode);
        
        var guildPlayers = await guildUtils.getGuildIdAndAllyCodesForGuildByGuildId(guild.guildId);
        if (!guildPlayers || !guildPlayers.allycodes || guildPlayers.allycodes.length == 0)
        {
            throw new Error("Failed to load guild data. Is your guild properly registered?");
        }

        if (guildPlayers.allycodes.indexOf(allycode) === -1)
        {
            throw new Error(`${allycode} is not in your guild.\n\nChoose a player in your guild to see a guildmate's assignments, or use **/ops show** to see your own assignments.`);
        }

        return allycode;
    }
    else
    {
        // choose the first alt of the player in the guild
        return guild.allycodes.at(0);
    }
}