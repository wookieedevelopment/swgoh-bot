const tbCommon = require("../tb/common");
const opsCommon = require("./common");
const playerUtils = require("../../utils/player");
const { SlashCommandSubcommandBuilder } = require('@discordjs/builders');
const { ChannelType } = require("discord.js");
const { CLIENT } = require("../../utils/discordClient");

module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("post")
        .setDescription("Post operations assignments for your guild to a channel.")
        .addIntegerOption(o =>
            o.setName("phase")
                .setDescription("Phase")
                .setRequired(true)
                .addChoices(
                    ...tbCommon.TB_PHASE_CHOICES
                ))
        .addIntegerOption(o =>
            o.setName("type")
                .setDescription("Type of visual (by player or by planet)")
                .addChoices(
                    { name: "By Player", value: opsCommon.OPERATION_VIEWS.PLAYER },
                    { name: "By Planet", value: opsCommon.OPERATION_VIEWS.PLANET }
                )
                .setRequired(true)
        )
        .addChannelOption(o =>
            o.setName("channel")
                .setDescription("Channel to post to")
                .setRequired(false)
                .addChannelTypes(ChannelType.GuildText)
        ),
    permissionLevel: playerUtils.UserRoles.GuildAdmin,
    interact: async function (interaction) {

        const { botUser, guildId, error } = await playerUtils.authorize(interaction, this.permissionLevel);

        if (error) {
            await interaction.editReply(error);
            return;
        }

        const guild = botUser.guilds.find(g => g.guildId === guildId);

        let channel = interaction.options.getChannel("channel");
        const phase = interaction.options.getInteger("phase");
        const type = interaction.options.getInteger("type");

        if (phase < tbCommon.TB_PHASES[0].n || phase > tbCommon.TB_PHASES[tbCommon.TB_PHASES.length - 1].n) {
            await interaction.editReply("Invalid phase. Choose from the options.");
            return;
        }
        
        const opsConfig = await opsCommon.loadGuildTbConfig(guild.comlinkGuildId);

        if (!channel && opsConfig.assignments_channel) {
            channel = CLIENT.channels.cache.get(opsConfig.assignments_channel);
        }

        if (!channel) {
            await interaction.editReply("Specify a channel (`channel`) or configure a default with `/ops config assignments-channel`.");
            return;
        }

        const messageToPost = await opsCommon.createAssignmentsGraphicMessageToPost(guild.comlinkGuildId, phase, type, opsConfig);

        await channel.send(messageToPost);
        await interaction.editReply(`Phase ${phase} assignment graphics ${type === opsCommon.OPERATION_VIEWS.PLANET ? "by planet" : "by player"} posted to ${channel}.`);
    }
}