const { SlashCommandSubcommandBuilder, EmbedBuilder, ActionRowBuilder, ButtonBuilder, SelectMenuBuilder, StringSelectMenuBuilder } = require('@discordjs/builders');
const playerUtils = require("../../utils/player");
const discordUtils = require("../../utils/discord");
const generateOps = require("./functions/generateOps");
const loadOps = require("./functions/loadOps");
const sendOps = require("./functions/sendOps");
const adjustOps = require("./functions/adjustOps");
const exportOps = require("./functions/exportOps");
const opsWizard = require("./functions/opsWizard");
const toggleRareOnly = require("./functions/toggleRareOnly");
const opsOverview = require("./functions/overview");
const opsCommon = require("./common");
const { ActionRow, ButtonStyle } = require('discord.js');

module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("manage")
        .setDescription("Manage TB operations for your guild."),
    permissionLevel: playerUtils.UserRoles.GuildAdmin,
    interact: async function(interaction)
    {
        let missingPermissions = discordUtils.getMissingBotPermissions(interaction, interaction.channel);
        if (missingPermissions)
        {
            await interaction.editReply(discordUtils.convertMissingBotPermissionsArrayToResponse(missingPermissions));
            return;
        }
        
        let { botUser, guildId, error } = await playerUtils.authorize(interaction, this.permissionLevel);

        if (error) {
            await interaction.editReply(error);
            return;
        }

        let guild;

        if (guildId)
        {
            guild = botUser.guilds.find(g => g.guildId === guildId);
        }
        else 
        {
            guild = botUser.guilds.find(g => g.guildAdmin);
        }
        
        await interaction.editReply({ content: `Loading guild data for *${guild.guildName}*...`, components: [], files: [], embeds: [] });

        let response = await opsOverview.overview(guild);

        await interaction.editReply(response);
    },
    handleButton: async function(interaction)
    { 
        if (interaction.customId === "ops:m") {
            await this.interact(interaction);
            return;
        }
        
        let { botUser, guildId, error } = await playerUtils.authorize(interaction, this.permissionLevel)
        if (error) {
            await interaction.followUp(error);
            return;
        }

        let regex = /ops:m:(.+):(.+)/

        let matches = regex.exec(interaction.customId);

        if (!matches || matches.length < 3) {
            // something went wrong
            logger.error("ops:m:handleButton interaction.customId doesn't match. customId is: " + interaction.customId);
            await interaction.editReply({ content: "Something went wrong, unfortunately. I'm working on it, but please try again." });
            return;
        }

        let data = matches[2];
        let cmd = matches[1];

        let response;

        switch (cmd)
        {
            case "genphase":
                await generateOps.generateAll(interaction, data.substring(1), parseInt(data.charAt(0)));
                return;
            case "genall":
                await generateOps.generateAll(interaction, data);
                return;
            case "export":
                await exportOps.export(interaction, data.substring(1), parseInt(data.charAt(0)));
                return;
            case "send":
                await sendOps.handleButton(interaction, data);
                break;
            case "load-planet":
                response = await loadOps.load(interaction, opsCommon.OPERATION_VIEWS.PLANET, data.substring(1), parseInt(data.charAt(0)));
                break;
            case "load-player":
                response = await loadOps.load(interaction, opsCommon.OPERATION_VIEWS.PLAYER, data.substring(1), parseInt(data.charAt(0)));
                break;
            case "rare":
                response = await toggleRareOnly.toggleRareOnly(interaction, data.substring(1), parseInt(data.charAt(0)));
                break;
            case "fall":
                response = await adjustOps.fillAll(data);
                break;
            case "pall":
                response = await adjustOps.preloadAll(data);
                break;
            case "xall":
                response = await adjustOps.excludeAll(data);
                break;
        }
        
        if (response) await interaction.editReply(response);
    },
    onSelect: async function(interaction)
    {
        await interaction.deferUpdate();
        
        let { botUser, guildId, error } = await playerUtils.authorize(interaction, this.permissionLevel)
        if (error) {
            await interaction.followUp(error);
            return;
        }
        
        let regex = /ops:m:(.+):(.+)/

        let matches = regex.exec(interaction.customId);
        
        if (!matches || matches.length < 3) {
            // something went wrong
            logger.error("ops:m:onSelect interaction.customId doesn't match. customId is: " + interaction.customId);
            await interaction.editReply({ content: "Something went wrong, unfortunately. I'm working on it, but please try again." });
            return;
        }

        let data = matches[2];
        let cmd = matches[1];

        let response;

        switch (cmd)
        {
            case "load":
                response = await loadOps.load(interaction, opsCommon.OPERATION_VIEWS.PLANET, data);
                break;
            case "adjust":
                response = await adjustOps.adjust(interaction, parseInt(data.charAt(0)), data.substring(1));
                break;
            case "pre":
                response = await adjustOps.preload(interaction, data);
                break;
            case "fill":
                response = await adjustOps.include(interaction, data);
                break;
            case "skip":
                response = await adjustOps.exclude(interaction, data);
                break;
            case "export":
                await exportOps.export(interaction, data);
                break;
            case "wiz":
                response = await opsWizard.start(interaction, data);
                break;
            case "wizset":
                response = await opsWizard.set(interaction, data);
                break;
            case "plstrat":
                response = await adjustOps.setPreloadStrategy(interaction, data);
                break;
        }

        if (response) await interaction.editReply(response);
    }
}