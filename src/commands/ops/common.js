
const NodeCache = require('node-cache');
const metadataCache = require ("../../utils/metadataCache");
const unitsUtils = require ("../../utils/units");
const guildUtils = require ("../../utils/guild");
const swapiUtils = require ("../../utils/swapi");
const database = require("../../database");
const { createCanvas, loadImage } = require("canvas");
const { Buffer } = require ("buffer");
const { EmbedBuilder, AttachmentBuilder } = require('discord.js');
const { parseAsync } = require('json2csv');
const tbCommon = require("../../commands/tb/common");

const opsDataCache = new NodeCache({useClones: false});

const PORTRAIT_WIDTH = 70;
const PORTRAIT_HEIGHT = 70;
const SPACE_BETWEEN_PORTRAITS = 40;
const UNASSIGNED_NAME = "ANY";
const UNASSIGNED_ALLYCODE = "0";
const PRELOAD_NAME = "DO NOT FILL";
const PRELOAD_ALLYCODE = "1";
const NOT_GENERATED_ALLYCODE = "2";
const NOT_GENERATED_NAME = "Regenerate";
const MAX_PLAN_NAME_LENGTH = 100;


const OPERATION_SELECTION_TIEBREAKER = {
    RANDOM: "Random",
    HIGHEST_PLAYER_GP: "Highest Player GP",
    LOWEST_PLAYER_GP: "Lowest Player GP",
    HIGHEST_UNIT_GP: "Highest Unit GP",
    LOWEST_UNIT_GP: "Lowest Unit GP"
};

const POST_ASSIGNMENTS_ON_SEND_ENUM = {
    NONE: 0,
    PLANET: 1,
    PLAYER: 2,
    BOTH: 3
};

const POST_ASSIGNMENTS_ON_SEND_OPTIONS = [
    { name: "Do not post", value: POST_ASSIGNMENTS_ON_SEND_ENUM.NONE },
    { name: "By Planet", value: POST_ASSIGNMENTS_ON_SEND_ENUM.PLANET },
    { name: "By Player", value: POST_ASSIGNMENTS_ON_SEND_ENUM.PLAYER },
    { name: "Both by Planet and by Player", value: POST_ASSIGNMENTS_ON_SEND_ENUM.BOTH }
];

const CONFIG_DEFAULTS = {
    RARE_THRESHOLD: 5,
    RARE_BY_DEFAULT: false,
    PLAYERS_PER_IMAGE: 10,
    TIEBREAKER: OPERATION_SELECTION_TIEBREAKER.RANDOM,
    ASSIGNMENTS_CHANNEL: null,
    POST_ASSIGNMENTS_ON_SEND: POST_ASSIGNMENTS_ON_SEND_ENUM.NONE
}

const OPERATION_VIEWS = {
    PLANET: 0,
    PLAYER: 1
}

const OPERATION_PLAN_TYPE = {
    SKIP: 0,
    FILL: 1,
    PRELOAD: 2
};

const PRELOAD_STRATEGY = [
    { label: "Skip Common Ships", value: "0" },
    { label: "Skip All Ships", value: "1" },
    { label: "Skip Common Units", value: "2" },
    { label: "Set Manually", value: "3" }
];

const PRELOAD_STRATEGY_ENUM = {
    SKIP_COMMON_SHIPS: PRELOAD_STRATEGY[0],
    SKIP_ALL_SHIPS: PRELOAD_STRATEGY[1],
    SKIP_COMMON_UNITS: PRELOAD_STRATEGY[2],
    MANUAL: PRELOAD_STRATEGY[3]
};

module.exports = {
    OPERATION_PLAN_TYPE: OPERATION_PLAN_TYPE,
    PRELOAD_STRATEGY: PRELOAD_STRATEGY,
    PRELOAD_STRATEGY_ENUM: PRELOAD_STRATEGY_ENUM,
    UNASSIGNED_ALLYCODE: UNASSIGNED_ALLYCODE,
    UNASSIGNED_NAME: UNASSIGNED_NAME,
    PRELOAD_ALLYCODE: PRELOAD_ALLYCODE,
    PRELOAD_NAME: PRELOAD_NAME,
    NOT_GENERATED_ALLYCODE: NOT_GENERATED_ALLYCODE,
    NOT_GENERATED_NAME: NOT_GENERATED_NAME,
    CONFIG_DEFAULTS: CONFIG_DEFAULTS,
    OPERATION_VIEWS: OPERATION_VIEWS,
    OPERATION_SELECTION_TIEBREAKER: OPERATION_SELECTION_TIEBREAKER,
    MAX_PLAN_NAME_LENGTH: MAX_PLAN_NAME_LENGTH,
    POST_ASSIGNMENTS_ON_SEND_ENUM: POST_ASSIGNMENTS_ON_SEND_ENUM,
    POST_ASSIGNMENTS_ON_SEND_OPTIONS: POST_ASSIGNMENTS_ON_SEND_OPTIONS,
    OpsDataCache: opsDataCache,
    getGuildTbPlanData: getGuildTbPlanData,
    saveGuildTbPlanData: saveGuildTbPlanData,
    saveGappedOperationsForPhase: saveGappedOperationsForPhase,
    getOpsData: getOpsData,
    getAllOperationRequirements: getAllOperationRequirements,
    getAllOperationGroupingRules: getAllOperationGroupingRules,
    getFilteredReqsByPlanets: getFilteredReqsByPlanets,
    getFilteredReqsByPlanetAndOperation: getFilteredReqsByPlanetAndOperation,
    getFilteredReqsForPlanByPhase: getFilteredReqsForPlanByPhase,
    getShowAssignmentsByPlanetResponse: getShowAssignmentsByPlanetResponse,
    getShowAssignmentsByPlayerResponse: getShowAssignmentsByPlayerResponse,
    loadGuildData: loadGuildData,
    getSortValueForPlanet: getSortValueForPlanet,
    loadAssignmentsByPhase: loadAssignmentsByPhase,
    loadGuildTbConfig: loadGuildTbConfig,
    insertOrUpdateTbConfigData: insertOrUpdateTbConfigData,
    createAssignmentsGraphicMessageToPost: createAssignmentsGraphicMessageToPost,
    getModifiedUnitDefId: getModifiedUnitDefId,
    saveTbAssignments: saveTbAssignments
}

async function loadGuildData({ comlinkGuildId = null, allycode = null }, interaction)
{
    let guild = await guildUtils.getGuildDataFromAPI({ allycode: allycode, comlinkGuildId: comlinkGuildId }, { useCache: true, cacheTimeHours: 12 });
    
    let guildAllycodes = guildUtils.getGuildAllycodesForAPIGuild(guild);
    let guildPlayerData = (await swapiUtils.getPlayers({
        allycodes: guildAllycodes, 
        useCache: true, 
        cacheTimeHours: 12, 
        updateStatusCallback: async (allycodes, players, errors) => { 
            await interaction.editReply(`Retrieved ${players.length}/${allycodes.length} rosters for **${guildUtils.getGuildName(guild)}**. Errors: ${errors.length}`); 
        }
    })).players;

    let guildData = {
        comlinkGuildId: swapiUtils.getGuildId(guild),
        guildName: swapiUtils.getGuildName(guild),
        players: guildPlayerData
    }

    return guildData;
}

async function loadGuildTbConfig(comlinkGuildId)
{
    let configData = await database.db.multiResult("SELECT * FROM guild_tb_config WHERE comlink_guild_id = $1; SELECT allycode,phase,available FROM tb_guild_player WHERE comlink_guild_id = $1", [comlinkGuildId]);
    
    let opsConfig = configData[0].rows?.at(0);
    if (!opsConfig)
    {
        opsConfig = { 
            rare_threshold: CONFIG_DEFAULTS.RARE_THRESHOLD, 
            rare_by_default: CONFIG_DEFAULTS.RARE_BY_DEFAULT,
            players_per_image: CONFIG_DEFAULTS.PLAYERS_PER_IMAGE,
            tiebreaker: CONFIG_DEFAULTS.TIEBREAKER,
            assignments_channel: CONFIG_DEFAULTS.ASSIGNMENTS_CHANNEL,
            post_assignments_on_send: CONFIG_DEFAULTS.POST_ASSIGNMENTS_ON_SEND

        };
        await insertOrUpdateTbConfigData(comlinkGuildId, 
            { 
                rareThreshold: opsConfig.rare_threshold, 
                rareByDefault: opsConfig.rare_by_default,
                playersPerImage: opsConfig.players_per_image,
                tiebreaker: opsConfig.tiebreaker,
                assignmentsChannel: opsConfig.assignments_channel,
                postAssignmentsOnSend: opsConfig.post_assignments_on_send
            });
    }

    opsConfig.players = configData[1].rows;

    return opsConfig;
}

async function insertOrUpdateTbConfigData(comlinkGuildId, { rareThreshold, rareByDefault, playersPerImage, tiebreaker, assignmentsChannel, postAssignmentsOnSend })
{
    let params = ["comlink_guild_id"];
    let args = [comlinkGuildId];
    if (rareThreshold != undefined) { params.push("rare_threshold"); args.push(rareThreshold); }
    if (rareByDefault != undefined) { params.push("rare_by_default"); args.push(rareByDefault); }
    if (playersPerImage != undefined) { params.push("players_per_image"); args.push(playersPerImage); }
    if (assignmentsChannel != undefined) { params.push("assignments_channel"); args.push(assignmentsChannel.id); }
    if (postAssignmentsOnSend != undefined) { params.push("post_assignments_on_send"); args.push(postAssignmentsOnSend); }

    if (tiebreaker != undefined && Object.values(OPERATION_SELECTION_TIEBREAKER).indexOf(tiebreaker) != -1) 
    {
        params.push("tiebreaker"); 
        args.push(tiebreaker); 
    }

    let values = "$1";
    let onConflict = [];
    for (let v = 1; v < params.length; v++) 
    {
        values += `, $${(v+1).toString()}`;
        onConflict.push(`${params[v]} = $${(v+1).toString()}`);
    }

    await database.db.none(
        `INSERT INTO guild_tb_config (${params.join(", ")})
        VALUES(${values})
        ON CONFLICT (comlink_guild_id) DO UPDATE SET ${onConflict.join(", ")}`, args);
}

async function loadAssignmentsByPhase({ comlinkGuildId = null, phase = null })
{
    if (!comlinkGuildId || !phase) return null;

    let assignments = (await database.db.any(`
        SELECT operation_req_id, preload, tb_ops.allycode, player_name
        FROM tb_ops
        LEFT JOIN guild_players ON guild_players.allycode = tb_ops.allycode
        WHERE comlink_guild_id = $1 AND phase = $2
    `, [comlinkGuildId, phase]));

    return assignments;
}

async function getGuildTbPlanData(comlinkGuildId)
{
    let planetsByPhase = (await database.db.oneOrNone(`
        SELECT planets_by_phase
        FROM tb_plan
        WHERE comlink_guild_id = $1`, [comlinkGuildId]))?.planets_by_phase;

    if (!planetsByPhase) return null;

    let plan = {};

    for (let phase of Object.keys(planetsByPhase))
    {
        plan[phase] = planetsByPhase[phase].map(pbp => { 
            return {
                rareOnly: pbp.rareOnly,
                preloadStrategy: PRELOAD_STRATEGY.find(p => p.value === pbp.preloadStrategy) ?? PRELOAD_STRATEGY[0],
                planet: tbCommon.PLANETS.find(p => p.name === pbp.planet),
                operations: pbp.operations
            }
        });
    }

    return plan;
}

async function saveGappedOperationsForPhase(comlinkGuildId, phase, planet)
{
    const now = new Date();
    await database.db.any(`
    INSERT INTO tb_ops (comlink_guild_id, plan_name, timestamp, phase, operation_req_id, preload, allycode, total_needed, total_available)
    (SELECT
        $1,
        $2,
        $3,
        $4,
        operation_req_id,
        false,
        $5,
        -1,
        -1
    FROM operation_req
    WHERE planet = $6)
    ON CONFLICT DO NOTHING`,
    [
        comlinkGuildId, 
        'default',
        now,
        phase,
        NOT_GENERATED_ALLYCODE,
        planet.name
    ]);
}

async function saveGuildTbPlanData(comlinkGuildId, { plan })
{
    if (!comlinkGuildId || !plan) return;

    let planetNamesByPhase = {};

    for (let key of Object.keys(plan))
    {
        planetNamesByPhase[key] = plan[key].map(pbp => 
            { 
                return {
                    rareOnly: pbp.rareOnly, 
                    preloadStrategy: pbp.preloadStrategy.value,
                    planet: pbp.planet.name, 
                    operations: pbp.operations 
                }
            });
    }

    await database.db.any(`
    INSERT INTO tb_plan (comlink_guild_id, planets_by_phase)
    VALUES ($1, $2)
    ON CONFLICT (comlink_guild_id) DO UPDATE SET planets_by_phase = EXCLUDED.planets_by_phase
    `, [comlinkGuildId, planetNamesByPhase]);

    
}

function createDefaultPlanetByPhaseData(sector) {
    return tbCommon.PLANETS.filter(p => p.sector === sector && !p.specialPlanet).map(p => { 
        return {
            rareOnly: false,
            preloadStrategy: PRELOAD_STRATEGY[0],
            planet: p,
            operations: new Array(6).fill(OPERATION_PLAN_TYPE.FILL)
        };
    })
}

async function getOpsData(comlinkGuildId)
{
    let obj = {
        phases: new Array(6),
        players: null,
        comlinkGuildId: comlinkGuildId
    };

    let plan = await getGuildTbPlanData(comlinkGuildId);

    if (!plan)
    {
        plan = {
            "1": createDefaultPlanetByPhaseData(1),
            "2": createDefaultPlanetByPhaseData(2),
            "3": createDefaultPlanetByPhaseData(3),
            "4": createDefaultPlanetByPhaseData(4),
            "5": createDefaultPlanetByPhaseData(5),
            "6": createDefaultPlanetByPhaseData(6)
        };

        await saveGuildTbPlanData(comlinkGuildId, { plan: plan })
    }

    let opAssignments = await database.db.any(`
        SELECT phase, tb_ops.operation_req_id, preload, tb_ops.allycode, total_available, total_needed, player_name, alignment, sector, planet, operation, row, slot, unit_name, unit_def_id, min_relic_level, is_gl
        FROM tb_ops
        LEFT JOIN operation_req ON (tb_ops.operation_req_id = operation_req.operation_req_id)
        LEFT JOIN guild_players ON (tb_ops.allycode = guild_players.allycode)
        WHERE comlink_guild_id = $1`, [comlinkGuildId]);

    for (let p = 0; p < obj.phases.length; p++)
    {
        let playerAssignments = [];
        let phaseOpAssignments = opAssignments.filter(a => (a.phase === p+1));
        for (let a of phaseOpAssignments)
        {
            a.planet = tbCommon.PLANETS.find(p => p.name === a.planet);
            if (!a.allycode || a.allycode === PRELOAD_ALLYCODE || a.allycode === UNASSIGNED_ALLYCODE || a.allycode == NOT_GENERATED_ALLYCODE) continue;
            let playerAssignmentObject = playerAssignments.find(p => p.allycode === a.allycode);

            if (!playerAssignmentObject)
            {
                playerAssignmentObject = {
                    allycode: a.allycode,
                    name: a.player_name,
                    assignedReqs: new Array()
                };

                playerAssignments.push(playerAssignmentObject);
            }

            playerAssignmentObject.assignedReqs.push(a);
        }

        obj.phases[p] = {
            planets: plan[p+1],
            preloadPlanets: (p === obj.phases.length - 1) ? [] : plan[p+2].filter(nextPhasePlanet => plan[p+1].find(cp => cp.planet === nextPhasePlanet.planet)),
            missing: opAssignments.filter(a => (a.phase === p+1) && a.allycode === null),
            opAssignments: phaseOpAssignments,
            playerAssignments: playerAssignments
        };
    }

    return obj;
}

async function getFilteredReqsForPlanByPhase(plan, previousAssignments, newAssignments, phase)
{
    const opReqs = await getAllOperationRequirements();

    let filteredReqs = [];

    for (let planetData of plan[phase])
    {
        let planetInPriorPhase = false;
        if (phase > 1) {
            for (let priorPhase = phase-1; priorPhase >= 1; priorPhase--)
            {
                // always check newAssignments first for the phase, otherwise go to the previousAssignments
                if (newAssignments[priorPhase])
                {                    
                    planetInPriorPhase = newAssignments[priorPhase]?.operationAssignments?.find(a => a.planet === planetData.planet.name);
                }
                else
                {
                    // previousAssignments are zero-indexed
                    planetInPriorPhase = previousAssignments.phases[priorPhase-1].planets.find(p => p.planet === planetData.planet);
                }

                if (planetInPriorPhase) break;
            }
        }

        let matchingReqs = opReqs.filter(r => {
            if (r.planet !== planetData.planet.name) return false;

            // if it was filled in any previous phase, then exclude it from this phase
            if (planetInPriorPhase)
            {
                for (let priorPhase = phase-1; priorPhase >= 1; priorPhase--)
                {
                    // always check newAssignments first for the phase, otherwise go to the previousAssignments
                    if (newAssignments[priorPhase])
                    {
                        if (newAssignments[priorPhase]?.operationAssignments?.find(o => 
                            o.planet === planetData.planet.name && 
                            o.operation === r.operation && 
                            o.reqs.find(oar => oar.id === r.id && oar.assignment && oar.assignment.allycode != PRELOAD_ALLYCODE))) return false;        
                    }
                    else
                    {
                        // zero-indexed phases
                        if (previousAssignments.phases[priorPhase-1]?.opAssignments.find(a => a.operation_req_id === r.id && a.allycode && a.allycode != PRELOAD_ALLYCODE)) return false;
                    }
                }
            } 
            return planetData.operations[r.operation-1] != OPERATION_PLAN_TYPE.SKIP;
        });

        filteredReqs = filteredReqs.concat(matchingReqs);
    }

    return filteredReqs;
}

async function getFilteredReqsByPlanets(planets, dsOps, mxOps, lsOps)
{
    const opReqs = await getAllOperationRequirements();

    let planetReqs = opReqs.filter(r => {
        if (!planets.find(p => p === r.planet)) return false;

        // only include specified operations, if any
        if (dsOps && r.alignment === tbCommon.ALIGNMENTS.DS)
        {
            if (dsOps.indexOf(r.operation) === -1) return false; 
        } 
        else if (lsOps && r.alignment === tbCommon.ALIGNMENTS.LS)
        {
            if (lsOps.indexOf(r.operation) === -1) return false;
        }
        else if (mxOps && r.alignment === tbCommon.ALIGNMENTS.MX)
        {
            if (mxOps.indexOf(r.operation) === -1) return false;
        }

        return true;
    });

    return planetReqs;
}

async function getFilteredReqsByPlanetAndOperation(planet, operation)
{
    const opReqs = await getAllOperationRequirements();

    return opReqs.filter(r => r.planet === planet && r.operation === operation);
}

async function getAllOperationRequirements()
{
    let data = metadataCache.get(metadataCache.CACHE_KEYS.operationRequirements);
    if (data == undefined)
    {
        data = await database.db.any(
            `SELECT * FROM operation_req
            ORDER BY sector,planet,operation,row,slot
            `)

        metadataCache.set(metadataCache.CACHE_KEYS.operationRequirements, data, 2592000000); // 30 days, whatever
    }
    
    // create a clone of the reqs so that we can modify them as needed throughout the process
    let opReqs = data.map(r => {
        return {
            id: r.operation_req_id,
            alignment: r.alignment,
            sector: r.sector,
            planet: r.planet,
            operation: r.operation,
            row: r.row,
            slot: r.slot,
            unitName: r.unit_name,
            unitDefId: r.unit_def_id,
            minRelicLevel: r.min_relic_level,
            isGL: r.is_gl
        }
    });
    
    return opReqs;
}

const SORT_OP_ASSIGNMENTS = (a, b) => {
    if (a.operation != b.operation) return a.operation - b.operation;
    if (a.row != b.row) return a.row - b.row;
    return a.slot - b.slot;
};

function createBlankCanvas(width, height, bgColor)
{
    const canvas = createCanvas(width, height);
    const context = canvas.getContext('2d')
    
    context.fillStyle = bgColor;
    context.fillRect(0, 0, width, height);

    return { canvas, context };
}

function drawPlayerName(context, name, borderColor, backgroundColor, textColor, nameX, nameY, nameMaxWidth, borderX, borderY, borderWidth, borderHeight)
{
    // draw border
    context.fillStyle = borderColor;
    context.fillRect(borderX, borderY, borderWidth, borderHeight);

    // draw box inside border
    context.fillStyle = backgroundColor;
    context.fillRect(borderX + 1, borderY + 1, borderWidth-2, borderHeight-2);
    
    context.font = `12pt "Noto Sans","Noto Sans SC","Noto Sans Armenian","Noto Sans Thai","Noto Sans Arabic","Noto Sans Korean"`;
    context.textAlign = "center";
    context.fillStyle = textColor;
    context.fillText(
        name,
        nameX, nameY, nameMaxWidth);
}

function findPreviousAssignmentPhase(priorPhaseAssignmentsData, reqId)
{
    return priorPhaseAssignmentsData?.findIndex(p => p.opAssignments.find(o => o.operation_req_id === reqId && o.allycode && o.allycode != PRELOAD_ALLYCODE));
}

async function getShowAssignmentsByPlanetResponse(opsData, phaseToShow, 
    { 
        rareThreshold = CONFIG_DEFAULTS.RARE_THRESHOLD,
        rareByDefault = CONFIG_DEFAULTS.RARE_BY_DEFAULT,
        players = null,
        includeNames = true,
        showTotalAvailable = false
    })
{    
    const width = 1200;
    const headerHeight = 75;
    const height = 1100 + headerHeight;
    let embeds = [], files = [];

    let phaseAssignmentsData = opsData.phases[phaseToShow-1];
    let priorPhaseAssignmentsData = (phaseToShow === 1) ? null : opsData.phases.slice(0, phaseToShow-1);

    for (var planetData of phaseAssignmentsData.planets)
    {
        let planetOpAssignments = phaseAssignmentsData.opAssignments.filter(o => o.planet === planetData.planet);

        if (planetOpAssignments.length === 0)
        {
            planetOpAssignments = await getFilteredReqsByPlanets([planetData.planet.name]);
        }

        planetOpAssignments.sort(SORT_OP_ASSIGNMENTS);
        
        const { canvas, context} = createBlankCanvas(width, height, "rgb(0,0,0)");

        
        // draw border
        context.fillStyle = "rgb(3,7,15)";
        context.fillRect(0, 54, width, 2);
        context.fillStyle = "rgb(5,10,30)";
        context.fillRect(0, 52, width, 2);
        context.fillStyle = "rgb(8,15,40)";
        context.fillRect(0, 50, width, 2);

        // draw box inside border
        context.fillStyle = "rgb(10,20,50)";
        context.fillRect(0, 0, width, 50);

        // draw header
        context.font = "20pt Arial"
        context.textAlign = "center";
        context.fillStyle = "#fff";
        context.fillText(
            `P${phaseToShow} Operations :: ${planetData.planet.name}`,
            width/2, 35);

        // 6 operations per planet
        for (var o = 0; o < 6; o++)
        {
            let fillTypeText;
            switch (planetData.operations[o])
            {
                case OPERATION_PLAN_TYPE.FILL: fillTypeText = "FILL"; break;
                case OPERATION_PLAN_TYPE.PRELOAD: fillTypeText = "PRELOAD"; break;
                case OPERATION_PLAN_TYPE.SKIP: fillTypeText = "SKIP"; break;
            }

            context.font = "20pt Arial"
            context.textAlign = "center"
            context.fillStyle = "#fff"
            context.fillText(`Operation ${o+1} - ${fillTypeText}`,
                Math.floor(o/3) * ((PORTRAIT_WIDTH+SPACE_BETWEEN_PORTRAITS) * 5 + 100) + (PORTRAIT_WIDTH + SPACE_BETWEEN_PORTRAITS) * 2.5,
                headerHeight + Math.floor(o%3)*3 * (PORTRAIT_HEIGHT + SPACE_BETWEEN_PORTRAITS) + (o%3+0.5)*SPACE_BETWEEN_PORTRAITS);

            let operationAssignments = planetOpAssignments.filter(asmt => asmt.operation === (o+1));
            let defaultOperationRequirements = await getFilteredReqsByPlanetAndOperation(planetData.planet.name, o+1);
            
            // skip this operation
            if (planetData.operations[o] === OPERATION_PLAN_TYPE.SKIP)
            {
                let x = SPACE_BETWEEN_PORTRAITS/2 + Math.floor(o/3) * ((PORTRAIT_WIDTH+SPACE_BETWEEN_PORTRAITS) * 5 + 100);
                if (showTotalAvailable) x -= SPACE_BETWEEN_PORTRAITS/2 + 1;
                let y = headerHeight + SPACE_BETWEEN_PORTRAITS + (o%3) * ((PORTRAIT_HEIGHT + SPACE_BETWEEN_PORTRAITS) * 3 + SPACE_BETWEEN_PORTRAITS);
                context.fillStyle = "rgba(200,0,0,.3)";
                context.fillRect(x, y, 
                    (PORTRAIT_WIDTH + SPACE_BETWEEN_PORTRAITS)*5 - SPACE_BETWEEN_PORTRAITS + 2 + (showTotalAvailable ? SPACE_BETWEEN_PORTRAITS : 0), 
                    (PORTRAIT_HEIGHT + SPACE_BETWEEN_PORTRAITS)*3 - SPACE_BETWEEN_PORTRAITS + 2 + (showTotalAvailable ? 12 : 0));
            }
            // preload this operation
            else if (!includeNames && planetData.operations[o] === OPERATION_PLAN_TYPE.PRELOAD)
            {
                let x = SPACE_BETWEEN_PORTRAITS/2 + Math.floor(o/3) * ((PORTRAIT_WIDTH+SPACE_BETWEEN_PORTRAITS) * 5 + 100);
                if (showTotalAvailable) x -= SPACE_BETWEEN_PORTRAITS/2 + 1;
                let y = headerHeight + SPACE_BETWEEN_PORTRAITS + (o%3) * ((PORTRAIT_HEIGHT + SPACE_BETWEEN_PORTRAITS) * 3 + SPACE_BETWEEN_PORTRAITS);
                context.fillStyle = "rgba(250,250,00,.3)";
                context.fillRect(x, y, 
                    (PORTRAIT_WIDTH + SPACE_BETWEEN_PORTRAITS)*5 - SPACE_BETWEEN_PORTRAITS + 2 + (showTotalAvailable ? SPACE_BETWEEN_PORTRAITS : 0), 
                    (PORTRAIT_HEIGHT + SPACE_BETWEEN_PORTRAITS)*3 - SPACE_BETWEEN_PORTRAITS + 2 + (showTotalAvailable ? 12 : 0));
            }

            for (var r = 0; r < defaultOperationRequirements.length; r++)
            {
                let x = SPACE_BETWEEN_PORTRAITS/2 + Math.floor(o/3) * ((PORTRAIT_WIDTH+SPACE_BETWEEN_PORTRAITS) * 5 + 100) + r%5 * (PORTRAIT_WIDTH + SPACE_BETWEEN_PORTRAITS);
                let y = headerHeight + Math.floor((o%3)*3 + r/5) * (PORTRAIT_HEIGHT + SPACE_BETWEEN_PORTRAITS) + (o%3 + 1)*SPACE_BETWEEN_PORTRAITS;
                let or = operationAssignments.find(or => (or.operation_req_id ?? or.id) === defaultOperationRequirements[r].id) ?? defaultOperationRequirements[r];
                let portrait = await unitsUtils.getPortraitForUnitByDefId(or.unit_def_id ?? or.unitDefId);

                let img = portrait ?? './src/img/no-portrait.png'
                context.fillStyle = "rgb(255, 255, 255)"
                context.fillRect(x, y, PORTRAIT_WIDTH+2, PORTRAIT_HEIGHT+2);

                const unitIconImg = await loadImage(img);
                context.drawImage(unitIconImg, 
                    x + 1, 
                    y + 1, 
                    PORTRAIT_WIDTH, PORTRAIT_HEIGHT);

                
                let priorAssignmentPhase = findPreviousAssignmentPhase(priorPhaseAssignmentsData, or.operation_req_id ?? or.id);

                if (priorAssignmentPhase != null && priorAssignmentPhase != -1)
                {

                    context.fillStyle = "rgba(255, 255, 255, .4)";
                    context.fillRect(x+1, y+1, PORTRAIT_WIDTH, PORTRAIT_HEIGHT);

                    let nameBackgroundColor = "#888", playerTextColor = "#000";
//                    let playerName = matchingLastPhaseAssignment.player_name ?? matchingLastPhaseAssignment.playerName;
                    let playerName = `Filled in P${priorAssignmentPhase+1}`;

                    drawPlayerName(
                        context,
                        playerName,
                        "#222",
                        nameBackgroundColor,
                        playerTextColor,
                        x + PORTRAIT_WIDTH/2,
                        y + PORTRAIT_HEIGHT + 7,
                        PORTRAIT_WIDTH + SPACE_BETWEEN_PORTRAITS - 7,
                        x - SPACE_BETWEEN_PORTRAITS/2 + 1,
                        y + PORTRAIT_HEIGHT - 10,
                        PORTRAIT_WIDTH + SPACE_BETWEEN_PORTRAITS - 1,
                        22
                        );
                }
                else if (includeNames && planetData.operations[o] !== OPERATION_PLAN_TYPE.SKIP)
                {
                    let nameBackgroundColor, playerName, playerTextColor = "#fff";
                    if (or.allycode === PRELOAD_ALLYCODE)
                    {
                        nameBackgroundColor = "rgba(220,220,0,1)";
                        playerTextColor = "#000"
                        playerName = PRELOAD_NAME;
                    } 
                    else if (or.allycode === NOT_GENERATED_ALLYCODE)
                    {
                        nameBackgroundColor = "rgba(220,220,0,1)";
                        playerTextColor = "#000"
                        playerName = NOT_GENERATED_NAME;
                    }
                    else if (or.allycode === UNASSIGNED_ALLYCODE || (planetData.rareOnly && (or.total_available > or.total_needed + rareThreshold)))
                    {
                        playerName = UNASSIGNED_NAME;
                        nameBackgroundColor = "rgb(0,0,0)";
                    }
                    else if (or.player_name ?? or.playerName) 
                    {
                        nameBackgroundColor = planetData.rareOnly ? "rgb(30, 100, 0)" : "rgb(0, 0, 0)";
                        playerName = or.player_name ?? or.playerName;
                    }
                    else if (or.total_available === 0)
                    {
                        // there are none in the guild
                        nameBackgroundColor = "rgba(130,0,0,1)";
                        playerName = "N O N E";
                    }
                    else if (phaseToShow === 6)
                    {
                        // it's a P6 operation with an assignment that can't be filled
                        nameBackgroundColor = "rgba(130,0,0,1)";
                        if (or.total_available) playerName = `${or.total_available} in Guild`;
                        else playerName = "Cannot Fill"
                    }
                    else if (planetData.operations[o] === OPERATION_PLAN_TYPE.PRELOAD)
                    {
                        nameBackgroundColor = "rgba(220,220,0,1)";
                        playerTextColor = "#000"
                        playerName = PRELOAD_NAME;
                    }
                    else if (planetData.operations[o] === OPERATION_PLAN_TYPE.FILL)
                    {
                        nameBackgroundColor = "rgba(130,0,0,1)";
                        playerName = (or.total_available < or.total_needed ? "Not Enough" : "Unassigned");
                    }
                    else
                    {
                        nameBackgroundColor = "rgba(220,220,0,1)";
                        playerTextColor = "#000"
                        playerName = "???";
                    }

                    drawPlayerName(
                        context,
                        playerName,
                        "rgb(90, 0, 90)",
                        nameBackgroundColor,
                        playerTextColor,
                        x + PORTRAIT_WIDTH/2,
                        y + PORTRAIT_HEIGHT + 7,
                        PORTRAIT_WIDTH + SPACE_BETWEEN_PORTRAITS - 7,
                        x - SPACE_BETWEEN_PORTRAITS/2 + 1,
                        y + PORTRAIT_HEIGHT - 10,
                        PORTRAIT_WIDTH + SPACE_BETWEEN_PORTRAITS - 1,
                        22
                        );

                } 
                else if (showTotalAvailable && or.total_available != null)
                {
                    let textBackgroundColor, availableTextColor = "#fff";

                    if (or.total_available === 0)
                    {
                        textBackgroundColor = "rgba(130,0,0,1)";
                    }
                    else if (or.total_available < or.total_needed)
                    {
                        textBackgroundColor = "rgba(220,220,0,1)";
                        availableTextColor = "#000";
                    }
                    else
                    {
                        textBackgroundColor = "rgb(0,0,0)";
                    }

                    drawPlayerName(
                        context,
                        `${or.total_available} in Guild`,
                        "rgb(90, 0, 90)",
                        textBackgroundColor,
                        availableTextColor,
                        x + PORTRAIT_WIDTH/2,
                        y + PORTRAIT_HEIGHT + 7,
                        PORTRAIT_WIDTH + SPACE_BETWEEN_PORTRAITS - 7,
                        x - SPACE_BETWEEN_PORTRAITS/2 + 1,
                        y + PORTRAIT_HEIGHT - 10,
                        PORTRAIT_WIDTH + SPACE_BETWEEN_PORTRAITS - 1,
                        22
                        );
                }
            }
        }
        
        let embed = new EmbedBuilder()
            .setTitle(`P${phaseToShow} Operation Assignments - ${tbCommon.getLabelForPlanet(planetData.planet)}`)
            .setColor(tbCommon.ALIGNMENT_COLOR_MAP[planetData.planet.alignment].number);
    
        let fileName = `tb-ops-${planetData.planet.name.replaceAll(" ", "_")}.png`
        const file = new AttachmentBuilder(canvas.toBuffer("image/png"), { name: fileName });
        embed.setImage("attachment://" + fileName)
        files.push(file);
        embeds.push(embed);
    }

    return { embeds: embeds, files: files, content: null, components: [] };
}

function getSortValueForPlanet(p)
{
    if (p.specialPlanet)
    {
        let sortValue = 3*p.sector;
        
        switch (p.alignment)
        {
            case tbCommon.ALIGNMENTS.DS: return sortValue+0;
            case tbCommon.ALIGNMENTS.MX: return sortValue+1;
            case tbCommon.ALIGNMENTS.LS: return sortValue+2;
        }
    }
    else
    {
        switch (p.alignment)
        {
            case tbCommon.ALIGNMENTS.DS: return 0;
            case tbCommon.ALIGNMENTS.MX: return 1;
            case tbCommon.ALIGNMENTS.LS: return 2;
        }
    }

    return 0;
}

async function getShowAssignmentsByPlayerResponse(opsData, phaseToShow,
    { 
        rareThreshold = CONFIG_DEFAULTS.RARE_THRESHOLD,
        rareByDefault = CONFIG_DEFAULTS.RARE_BY_DEFAULT,
        playersShownPerImage = CONFIG_DEFAULTS.PLAYERS_PER_IMAGE
    })
{
    let phaseData = opsData.phases[phaseToShow-1];

    let embeds = [], files = [], csvData = [];
        
    let filteredAssignments = phaseData.playerAssignments.filter(p => p.name);
    if (filteredAssignments.length === 0)
    {
        let embed = new EmbedBuilder()
            .setTitle(`P${phaseToShow} Player Operation Assignments`)
            .setColor('DarkGreen')
            .setDescription("No assignments. If you are not expecting this, please double-check your TB plan (`/ops manage`).");
        embeds.push(embed);
        return { embeds: embeds };
    }

    filteredAssignments.sort((a, b) => a.name.localeCompare(b.name));

    let totalImages = Math.ceil(filteredAssignments.length / playersShownPerImage);

    const padding = 10;
    const curTime = new Date().getTime();

    for (let imageNumber = 0; imageNumber < totalImages; imageNumber++)
    {
        let playersInImage = filteredAssignments.slice(imageNumber*playersShownPerImage, (imageNumber+1)*playersShownPerImage);

        let portraitRows = 0;
        let maxPortraitsPerPlanet = 0;

        for (let assignmentObject of playersInImage)
        {
            let planetsCount = 0;
            for (let p of tbCommon.PLANETS)
            {
                let match = assignmentObject.assignedReqs.filter(r => r.planet === p);
                if (match.length > maxPortraitsPerPlanet) maxPortraitsPerPlanet = match.length;
                if (match.length > 0) planetsCount++;
            }
    
            portraitRows += planetsCount;
        }

        const width = Math.max(250, padding + maxPortraitsPerPlanet*(PORTRAIT_WIDTH + 10) + 1);
        const height = playersInImage.length*(65 + PORTRAIT_HEIGHT + 10) + portraitRows*10 + (portraitRows - playersInImage.length)*(PORTRAIT_HEIGHT + 40);
        let curPosY = 0;
        const { canvas, context } = createBlankCanvas(width, height, "#000");

        let embed = new EmbedBuilder()
            .setTitle(`P${phaseToShow} Player Assignments: ${playersInImage[0].name} - ${playersInImage[playersInImage.length-1].name}`)
            .setColor('DarkGreen');
        embeds.push(embed);

        for (var playerAssignmentObjectIx in playersInImage)
        {
            let playerAssignmentObject = playersInImage[playerAssignmentObjectIx];

            // sort assignments
            playerAssignmentObject.assignedReqs.sort((a, b) => {
                let planetCompare = getSortValueForPlanet(a.planet) - getSortValueForPlanet(b.planet);
                if (planetCompare != 0) return planetCompare;
                if (a.sector != b.sector) return a.sector - b.sector;
                if (a.operation != b.operation) return a.operation - b.operation;
                if (a.row != b.row) return a.row - b.row;
                return a.slot - b.slot;
            });

            context.fillStyle = "rgb(200, 200, 200)"
            context.fillRect(0, curPosY, width, 10);

            curPosY += 40;

            context.font = `20pt "Noto Sans","Noto Sans SC","Noto Sans Armenian","Noto Sans Thai","Noto Sans Arabic","Noto Sans Korean"`
            context.textAlign = "left"
            context.fillStyle = "#fff"
            context.fillText(`${playerAssignmentObject.name}`,
                padding,
                curPosY,
                width - 2*padding
                );
            
            curPosY += 25;

            let curPlanet;
            let reqsInPlanet = 0;
            for (let assignment of playerAssignmentObject.assignedReqs)
            {
                if (assignment.planet != curPlanet)
                {
                    // new row, but not the first for a player
                    if (reqsInPlanet > 0) curPosY += PORTRAIT_HEIGHT + 40;
                    curPlanet = assignment.planet;
                    reqsInPlanet = 0;

                    context.font = `15pt "Noto Sans","Noto Sans SC","Noto Sans Armenian","Noto Sans Thai","Noto Sans Arabic","Noto Sans Korean"`
                    context.textAlign = "left";
                    context.fillStyle = "#fff";
                    context.fillText(`${assignment.planet.name} (${assignment.alignment})`,
                        padding,
                        curPosY,
                        width - 2*padding
                        );

                    curPosY += 10;
                }

                let portrait = await unitsUtils.getPortraitForUnitByDefId(assignment.unit_def_id);
                let img = portrait ?? './src/img/no-portrait.png';
                context.fillStyle = tbCommon.ALIGNMENT_COLOR_MAP[assignment.alignment].string;
                context.fillRect(padding + reqsInPlanet*(PORTRAIT_WIDTH+10), curPosY, PORTRAIT_WIDTH+2, PORTRAIT_HEIGHT+2);

                const unitIconImg = await loadImage(img);
                context.drawImage(unitIconImg, 
                    padding + reqsInPlanet*(PORTRAIT_WIDTH + 10) + 1, 
                    curPosY + 1, 
                    PORTRAIT_WIDTH, PORTRAIT_HEIGHT);

                    
                context.fillStyle = "rgb(0, 0, 0)"
                context.fillRect(padding + reqsInPlanet*(PORTRAIT_WIDTH+10) + 1, curPosY + PORTRAIT_HEIGHT - 19, PORTRAIT_WIDTH, 20);

                context.font = "12pt Arial"
                context.textAlign = "center"
                context.fillStyle = "#fff"
                context.fillText(`Op #${assignment.operation}`,
                    padding + reqsInPlanet*(PORTRAIT_WIDTH+10) + PORTRAIT_WIDTH/2,
                    curPosY + PORTRAIT_HEIGHT - 3
                )

                let csvDataObj = {
                    "Player": playerAssignmentObject.name,
                    "Planet": assignment.planet.name,
                    "Operation": assignment.operation,
                    "Row": assignment.row,
                    "Col": assignment.slot,
                    "Unit": assignment.unit_name
                };
                
                csvData.push(csvDataObj);

                reqsInPlanet++;
            }

            curPosY += PORTRAIT_HEIGHT + 10;

        }

        let fileName = `tb-ops-by-player-P${phaseToShow ?? "?"}-${curTime}-${imageNumber+1}-of-${totalImages+1}.png`
        const file = new AttachmentBuilder(canvas.toBuffer("image/png"), { name: fileName });
        embed.setImage("attachment://" + fileName)
        files.push(file);
            
    }   

    if (csvData.length > 0)
    {
        const csvParsingOptions = { defaultValue: 0 }
        let csvText = await parseAsync(csvData, csvParsingOptions);
        let buffer = Buffer.from(csvText);
        let csvAttachment = new AttachmentBuilder(buffer, { name: `rote_ops_by_player_P${phaseToShow ?? "?"}_${curTime}.csv`});

        files.push(csvAttachment);
    }
    
    return { embeds: embeds, files: files };
}

async function getAllOperationGroupingRules()
{
    let data = metadataCache.get(metadataCache.CACHE_KEYS.operationGroupingRules);
    if (data == undefined)
    {
        data = await database.db.any(
            `SELECT * FROM operation_grouping_rule
            ORDER BY planet,priority`)

        metadataCache.set(metadataCache.CACHE_KEYS.operationGroupingRules, data, 2592000000); // 30 days, whatever
    }
    
    return data;
}


async function createAssignmentsGraphicMessageToPost(comlinkGuildId, phase, type, opsConfig) {
    let message;
    const opsData = await getOpsData(comlinkGuildId);
    switch (type) {
        case OPERATION_VIEWS.PLANET:
            message = await getShowAssignmentsByPlanetResponse(opsData, phase, { rareThreshold: opsConfig.rare_threshold, rareByDefault: opsConfig.rare_by_default });
            break;
        case OPERATION_VIEWS.PLAYER:
            message = await getShowAssignmentsByPlayerResponse(opsData, phase, { rareThreshold: opsConfig.rare_threshold, rareByDefault: opsConfig.rare_by_default, playersShownPerImage: opsConfig.players_per_image });
            break;
    }

    // remove any csv files
    message.files = message.files?.filter(f => !f.name.endsWith("csv"));

    return message;
}

function getModifiedUnitDefId(unitDefId, minRelicLevel)
{
    return `${unitDefId}_R${minRelicLevel.toString()}`;
}

const DELETE_BY_PHASE_QUERY_TEMPLATE = `DELETE FROM tb_ops WHERE comlink_guild_id = $1 AND phase IN ($2:csv); `;
async function saveTbAssignments(comlinkGuildId, assignments)
{
    let keys = Object.keys(assignments);

    var tbOpsValues = new Array();
    const now = new Date();
    for (let phase of keys)
    {
        let phaseData = assignments[phase];

        for (let opIx = 0; opIx < phaseData.operationAssignments.length; opIx++)
        {
            let opObj = phaseData.operationAssignments[opIx];

            for (let reqAssignmentIx = 0; reqAssignmentIx < opObj.reqs.length; reqAssignmentIx++)
            {
                let reqAssignment = opObj.reqs[reqAssignmentIx];
                let availabilityData = phaseData.availabilityByUnit[getModifiedUnitDefId(reqAssignment.unitDefId, reqAssignment.minRelicLevel)];
                tbOpsValues.push({
                    comlink_guild_id: comlinkGuildId,
                    plan_name: "default",
                    timestamp: now,
                    phase: phase,
                    operation_req_id: reqAssignment.id,
                    preload: false,
                    allycode: reqAssignment.assignment?.allycode ?? null,
                    total_needed: availabilityData.needed,
                    total_available: availabilityData.available
                });
            }
        }

    }

    let tbOpsScript = "";
    if (tbOpsValues.length > 0)
    {
        tbOpsScript = database.pgp.helpers.insert(tbOpsValues, database.columnSets.tbOpsCS) + 
                    ' ON CONFLICT(comlink_guild_id,phase,operation_req_id) DO UPDATE SET ' +
                    database.columnSets.tbOpsCS.assignColumns({from: 'EXCLUDED', skip: ['comlink_guild_id', 'phase', 'operation_req_id']});
    }

    // refresh static data & insert new dynamic data
    await database.db.none(DELETE_BY_PHASE_QUERY_TEMPLATE + tbOpsScript, [comlinkGuildId, keys]);
}