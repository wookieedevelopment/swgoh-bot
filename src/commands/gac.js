const gacCalc = require("./gac/calc");
const { SlashCommandBuilder } = require('@discordjs/builders');

module.exports = {
    data: new SlashCommandBuilder()
        .setName("gac")
        .setDescription("GAC Utilities")
        .addSubcommand(gacCalc.data)
        ,
    interact: async function(interaction)
    {
        let subcommand = interaction.options.getSubcommand();
        switch (subcommand)
        {
            case gacCalc.data.name:
                await gacCalc.interact(interaction);
                break;
        }
    },
    autocomplete: async function(interaction)
    {
        try { subcommand = interaction.options.getSubcommand(); } catch {}
        if (subcommand)
        {
            switch (subcommand) {
                case gacCalc.data.name:
                    await gacCalc.autocomplete(interaction);
                    break;
            }
            return;
        }
    }
}