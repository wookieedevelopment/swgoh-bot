var data = require("../data/trivia_data.json");
const discordUtils = require ("../utils/discord")
const { SlashCommandBuilder } = require('@discordjs/builders');
const { ActionRowBuilder, ButtonBuilder, EmbedBuilder, ButtonStyle } = require('discord.js');

let buttonRow = new ActionRowBuilder().addComponents(
    new ButtonBuilder()
    .setCustomId("another_fact")
    .setLabel("Another Fact")
    .setStyle(ButtonStyle.Primary)
);     

module.exports = {
    data: new SlashCommandBuilder()
        .setName("fact")
        .setDescription("Generate facts about the Star Wars universe"),
    name: "fact",
    description: "Generate facts about the Star Wars universe",

    interact: async function(interaction)
    {
        let embed = this.getRandomFactEmbed();
        await interaction.editReply({ content: discordUtils.makeIdTaggable(interaction.user.id), embeds: [embed], components: [buttonRow] });
    },
    handleButton: async function(interaction)
    {
        const embed = this.getRandomFactEmbed();
        await interaction.followUp({ content: discordUtils.makeIdTaggable(interaction.user.id), embeds: [embed], components: [buttonRow] })
    },
    execute: async function(client, message, args) {
        let embed = this.getRandomFactEmbed();
        await message.channel.send({ content: discordUtils.makeIdTaggable(message.author.id), embeds: [embed] });
    },
    getRandomFactEmbed: function(userId)
    {
        let fact = data.facts[Math.floor(Math.random()*data.facts.length)];
        let embed = new EmbedBuilder().setTitle(fact.intro).setColor(0xD2691E).setDescription(fact.fact);
        return embed;
    }
}