const discordUtils = require("../../utils/discord");
const unitsUtils = require("../../utils/units");
const guildUtils = require("../../utils/guild");
const database = require("../../database");
const { SlashCommandSubcommandBuilder } = require("@discordjs/builders");
const metadataCache = require("../../utils/metadataCache");

module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("match")
        .setDescription("Show what unit or units will match the text")
        .addStringOption(o => 
            o.setName("text")
            .setDescription("Text to match to a unit")
            .setRequired(true)),
    interact: async function(interaction) {
        let text = interaction.options.getString("text");
        
        let matches = await unitsUtils.getUnitMatchesByNickname(text);

        if (!matches || matches.length === 0)
        {
            await interaction.editReply({ content: `No units matching \`${text}\`` });
            return;
        }

        let content = 
`${matches.length} unit${matches.length == 1 ? "" : "s"} matching \`${text}\`:
${matches.map(u => `- ${u.name}`).join("\n")}
`;

        await interaction.editReply({ content: content });
    },
}