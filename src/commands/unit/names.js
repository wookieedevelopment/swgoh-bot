const discordUtils = require("../../utils/discord");
const unitsUtils = require("../../utils/units");
const swapiUtils = require("../../utils/swapi");
const database = require("../../database");
const { SlashCommandSubcommandBuilder } = require("@discordjs/builders");
const metadataCache = require("../../utils/metadataCache");

module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("names")
        .setDescription("Show all nicknames (if any) for a unit")
        .addStringOption(o => 
            o.setName("unit")
            .setDescription("The unit")
            .setAutocomplete(true)
            .setRequired(true)),
    interact: async function(interaction) {
        let unit = interaction.options.getString("unit");
        
        const unitNicknames = metadataCache.get(metadataCache.CACHE_KEYS.unitNicknames);
        const unitsList = await swapiUtils.getUnitsList();
        const unitName = unitsList.find(u => swapiUtils.getUnitDefId(u) === unit).name;

        let nicknames = new Array();
        for (let key of Object.keys(unitNicknames.nicknames))
        {
            if (unitNicknames.nicknames[key].toLowerCase() == unit.toLowerCase())
            {
                nicknames.push(key);
            }
        }

        if (nicknames.length == 0)
        {
            await interaction.editReply({ content: `No nicknames yet for ${unitName}.` });
            return;
        }

        let content = 
`Nicknames for **${unitName}**:
${nicknames.map(n => `- ${n}`).join("\n")}
`;
        await interaction.editReply({ content: content });
    },
    autocomplete: async function(interaction) {
        const focusedValue = interaction.options.getFocused();
        const choices = await unitsUtils.getUnitAutocompleteOptions(focusedValue);

        await interaction.respond(choices);
    }
}