const drawingUtils = require("../../../utils/drawing");
const swapiUtils = require("../../../utils/swapi.js");
const datacronUtils = require("../../../utils/datacron.js");
const unitsUtils = require("../../../utils/units.js");
const { drawText } = require("canvas-txt");
const { logger, formatError } = require("../../../utils/log.js");
const { createCanvas, loadImage } = require('canvas');
const { AttachmentBuilder } = require("discord.js");
const { EmbedBuilder } = require("@discordjs/builders");

module.exports = {
    drawDatacron: drawDatacron,
    getDatacronsEmbeds: getDatacronsEmbeds
}

const EMBED_COLOR = 0x559922;
const CANVAS_DATA = {
    
    unitPortraitHeight: 125,
    unitPortraitWidth: 125,
    unitPortraitImageHeight: 75,
    unitPortraitImageWidth: 75,
    portraitRowSpacerHeight: 10,
    portraitPadding: 8,
    maxPortraitsPerRow: 10,
    smallCircleWidth: 30,
    smallCircleHeight: 30,
    smallCircleOffsetX: 47.5, // CANVAS_DATA.unitPortraitWidth/2 - CANVAS_DATA.smallCircleWidth/2,
    smallCircleOffsetY: 60, // CANVAS_DATA.unitPortraitImageHeight - CANVAS_DATA.smallCircleHeight/2,
    footerHeight: 40,
    characterPortraitColumnWidth: 150,
    characterPortraitRowHeight: 180
};

async function drawDatacron(ctx, dc, x, y) {
    // Draw the image
    const result = await datacronUtils.getMatchingDCsByWookieeBotDatacronId(dc.datacronId);


    if (!result || result.img == null)
    {
        logger.error(`Image not found for character: ${dc.character}`);
    }
    else
    {
        let imageSource = result.img;
        // Check if the image path exists
    
        await drawingUtils.loadAndDrawImage(ctx, imageSource, x + (CANVAS_DATA.unitPortraitImageWidth/3), y, CANVAS_DATA.unitPortraitImageWidth, CANVAS_DATA.unitPortraitImageHeight);    
    }
    
    ctx.drawImage(drawingUtils.getImage(drawingUtils.IMAGES.SMALL_CIRCLE), 
        x + CANVAS_DATA.smallCircleOffsetX, 
        y + CANVAS_DATA.smallCircleOffsetY, 
        CANVAS_DATA.smallCircleWidth, CANVAS_DATA.smallCircleHeight);

    ctx.fillStyle = "#fff";

    drawText(ctx, dc.shortDescription,
        {
            x: x,
            y: y + (CANVAS_DATA.unitPortraitHeight / 2) + CANVAS_DATA.smallCircleHeight +2,
            width: CANVAS_DATA.unitPortraitWidth,
            height: 1,
            fontSize: 13,
            //fontStyle: "bold",
            font: "Arial",
            vAlign: "top"
        });

    // Draw the count text
    drawText(ctx, `${dc.allycodes.length}`,
        {
            x: x,
            y: y + CANVAS_DATA.unitPortraitImageHeight,
            width: CANVAS_DATA.unitPortraitWidth,
            height: 1,
            vAlign: "middle",
            align: "center",
            fontStyle: "bold",
            fontSize: 13
        });
}

async function getDatacronsEmbeds(guildPlayerData)
{
    let twDatacronData = await getDatacronData(guildPlayerData);
    let embeds = [],
        files = [];

    const testcanvas = createCanvas();
    let testctx = testcanvas.getContext('2d');

    for (let setId of twDatacronData.sets) {
        for (let level = 9; level >= 3; level -= 3) {
            let dcsInSetByLevel = twDatacronData.datacronsByAbility.filter(
                (d) => d.setId === setId && d.level === level && d.datacronId != null
            );

            if (dcsInSetByLevel.length === 0) continue;
    
            let x = 10;
            let y = 10;
            // Calculate the canvas size based on the number of datacrons
            const numRows = Math.ceil(dcsInSetByLevel.length / CANVAS_DATA.maxPortraitsPerRow);
            let rowHeights = [];
            
    
            // get the height for each row
            for (let rowIndex = 0; rowIndex < numRows; rowIndex++) {
                let maxDescriptionHeight = 0;
                // Loop through the data and generate images for this row
                for (let i = rowIndex * CANVAS_DATA.maxPortraitsPerRow; i < (rowIndex + 1) * CANVAS_DATA.maxPortraitsPerRow && i < dcsInSetByLevel.length; i++) {
                    const dc = dcsInSetByLevel[i];
                    
                    let { height } = drawText(testctx, dc.shortDescription,
                        {
                            x: 0,
                            y: 0,
                            width: CANVAS_DATA.unitPortraitWidth,
                            height: 1,
                            fontSize: 13,
                            font: "Arial",
                            vAlign: "top"
                        });

                    maxDescriptionHeight = Math.max(maxDescriptionHeight, height);
                }
    
                rowHeights.push(maxDescriptionHeight + CANVAS_DATA.unitPortraitHeight + CANVAS_DATA.smallCircleHeight/2);
            }
            
            // Calculate the canvas height based on the row heights
            let height = rowHeights.reduce((acc, height) => acc + height + CANVAS_DATA.portraitRowSpacerHeight, 0) + CANVAS_DATA.footerHeight;
            let width = 10;
            // Create a new canvas for the entire embed
            if (CANVAS_DATA.maxPortraitsPerRow > dcsInSetByLevel.length){
                width += dcsInSetByLevel.length * (CANVAS_DATA.unitPortraitWidth + CANVAS_DATA.portraitPadding);
            }
            else
            {
                width += CANVAS_DATA.maxPortraitsPerRow * (CANVAS_DATA.unitPortraitWidth + CANVAS_DATA.portraitPadding);
            }

            const canvas = createCanvas(width, height);
            const ctx = canvas.getContext("2d");

            ctx.drawImage(drawingUtils.getImage(drawingUtils.IMAGES.BG_REC), 0, 0, canvas.width, canvas.height);
            ctx.fillStyle = "#fff";
            // Loop through the data and generate images for the entire embed
            x = 10;
            y = 10;
            for (let rowIndex = 0; rowIndex < numRows; rowIndex++) {
                for (let i = rowIndex * CANVAS_DATA.maxPortraitsPerRow; i < (rowIndex + 1) * CANVAS_DATA.maxPortraitsPerRow && i < dcsInSetByLevel.length; i++) {
                    const dc = dcsInSetByLevel[i];
                    await drawDatacron(ctx, dc, x, y);
                    x += CANVAS_DATA.unitPortraitWidth + CANVAS_DATA.portraitPadding;
                }
    
                x = 10; // Start a new row
                y += rowHeights[rowIndex] + CANVAS_DATA.portraitRowSpacerHeight;
            }
    
            // Embed with the canvas image
            const dcimage = new AttachmentBuilder(canvas.toBuffer(), {
                name: `datacron_image_${setId}_${level}.png`,
            });
            files.push(dcimage);
            embeds.push(
                new EmbedBuilder()
                    .setColor(EMBED_COLOR)
                    .setTitle(`>>>>  Set ${setId}, Level ${level}  <<<< ${guildPlayerData.name}`)
                    .setImage(`attachment://${dcimage.name}`)
            );
        }
    }

    let rowsNeeded = Math.ceil(twDatacronData.datacronsByCharacter.length/CANVAS_DATA.maxPortraitsPerRow);
    let width = CANVAS_DATA.portraitPadding*2 + Math.min(CANVAS_DATA.maxPortraitsPerRow, twDatacronData.datacronsByCharacter.length) * CANVAS_DATA.characterPortraitColumnWidth;
    let height = CANVAS_DATA.portraitPadding*2 + rowsNeeded * CANVAS_DATA.characterPortraitRowHeight;
    // Create a canvas
    const canvas = createCanvas(width, height);
    const ctx = canvas.getContext('2d');

    ctx.drawImage(drawingUtils.getImage(drawingUtils.IMAGES.BG_REC), 0, 0);

    // Set initial coordinates and font style
    let x = CANVAS_DATA.portraitPadding;
    let y = CANVAS_DATA.portraitPadding;
    ctx.font = "25pt Noto Sans";
    ctx.textAlign = "center";
    ctx.textBaseline = "middle";
    ctx.fillStyle = "#fff";
    let dcTotal = 0;
    // Loop through the data and generate images
    for (const dc of twDatacronData.datacronsByCharacter) {
        
        const unitPortrait = (await unitsUtils.getPortraitForUnitByDefId(dc.character)) ?? drawingUtils.getImage(drawingUtils.IMAGES.NO_PORTRAIT);

        const image = await loadImage(unitPortrait);
        
        // Calculate the scaling factors for the image
        const scaleX = CANVAS_DATA.unitPortraitWidth / image.width;
        const scaleY = CANVAS_DATA.unitPortraitHeight / image.height;
        const scale = Math.min(scaleX, scaleY);

        // Calculate the dimensions for the scaled image
        const scaledWidth = image.width * scale;
        const scaledHeight = image.height * scale;

        // Calculate the position to center the scaled image inside dcBG
        const imageX = x + (CANVAS_DATA.unitPortraitWidth - scaledWidth ) / 2;
        const imageY = y + (CANVAS_DATA.unitPortraitHeight - scaledHeight ) / 2;

        // Save the current canvas state
        ctx.save();

        // Create a clipping path for a rounded rectangle
        ctx.beginPath();
        ctx.arc(x + CANVAS_DATA.unitPortraitWidth / 2, y + CANVAS_DATA.unitPortraitHeight / 2, CANVAS_DATA.unitPortraitWidth / 2, 0, Math.PI * 2, true);
        ctx.closePath();
        ctx.clip();

        // Draw the scaled image on the canvas
        ctx.drawImage(image, imageX, imageY, scaledWidth, scaledHeight);

        // Restore the canvas state
        ctx.restore();

        const smallcircX = imageX + 25;
        const smallcircY = imageY + 90;
        const smallcircWidth = scaledWidth - 55;
        const smallcircHeight = scaledHeight - 55;
        ctx.drawImage(drawingUtils.getImage(drawingUtils.IMAGES.SMALL_CIRCLE), 
            smallcircX, 
            smallcircY, 
            smallcircWidth,
            smallcircHeight);

        // Draw the count text
        ctx.fillStyle = '#FFFFFF';

        // Your count text
        const countText = `${dc.allycodes.length}`;
        dcTotal += dc.allycodes.length

        ctx.fillText(countText, smallcircX + smallcircWidth/2, smallcircY + smallcircHeight/2);

        // new coordinates for next column
        x += CANVAS_DATA.characterPortraitColumnWidth;
        if (x > canvas.width - CANVAS_DATA.characterPortraitColumnWidth)
        {
            // new row
            x = CANVAS_DATA.portraitPadding;
            y += CANVAS_DATA.characterPortraitRowHeight;
        }    
    }

    // Embed with the canvas image
    const lvl9img = new AttachmentBuilder(canvas.toBuffer(),  { name: 'lvl9datacron_image.png'});
    const imageEmbed = new EmbedBuilder()
        .setColor(EMBED_COLOR)
        .setTitle(`Character (Level 9) Datacrons`)
        .setDescription(`Characters: ${twDatacronData.datacronsByCharacter.length}`)
        .setFooter({text:'ALL Level 9 abilities (characters) are counted ONCE per player.' +`\n`+ `Total DCs: ${dcTotal}`})
        .setImage(`attachment://${lvl9img.name}`);

    embeds.push(imageEmbed);
    files.push(lvl9img);

    return { embeds: embeds, files: files };
}

async function getDatacronData(guildPlayerData)
{

    let datacronData = {
        datacronsByAbility: [], // unique players with each level 9 ability and a total for all other abilities
        datacronsByCharacter: [], // unique players with a level 9 ability for a specific character. For example, # of players with a LV ability (any)
        sets: []
    };
    
    let allUnits = await swapiUtils.getUnitsList();

    //let wookieeBotDcData = await swapiUtils.getWookieeBotDatacronData();

    guildPlayerData.roster?.forEach(p => {
        swapiUtils.getPlayerDatacrons(p).forEach(dc => {

            if (!datacronData.sets.find(s => s === dc.setId)) datacronData.sets.push(dc.setId);

            // loop through all affixes for all player datacrons and add to collection of datacrons with a count. 
            dc.affix.filter(a => a.abilityId != null && a.abilityId.length > 0).forEach((affix, ix) => {
                
                let reportDcData = datacronData.datacronsByAbility.find(d => d.datacronId === affix.wookieeBotDatacronId);

                if (!reportDcData) {
                    reportDcData = { 
                        setId: dc.setId,
                        datacronId: affix.wookieeBotDatacronId,
                        shortDescription: affix.short_description,
                        allycodes: [swapiUtils.getPlayerAllycode(p)],
                        level: (ix+1)*3,
                        count: 1
                    }
    
                    datacronData.datacronsByAbility.push(reportDcData);
                } else if (ix < 2 || !reportDcData.allycodes.find(a => a === swapiUtils.getPlayerAllycode(p))) {
                    // count it if it's not a character-specific ability OR
                    // if it is a character-specific ability and the player hasn't been counted already for it
                    reportDcData.count++;
                    reportDcData.allycodes.push(swapiUtils.getPlayerAllycode(p))
                }

                if (ix === 2)
                {
                    let reportCharacterDcData = datacronData.datacronsByCharacter.find(d => d.character === dc.meta.character);

                    if (!reportCharacterDcData) {
                        let unit = allUnits.find(u => swapiUtils.getUnitDefId(u) === dc.meta.character);
                    
                        if (unit) {
                            reportCharacterDcData = {
                                character: dc.meta.character,
                                unitName: unit.name,
                                allycodes: [swapiUtils.getPlayerAllycode(p)]
                            }
                    
                            datacronData.datacronsByCharacter.push(reportCharacterDcData);
                        } else {
                            // unit not found. Not sure how this could be
                            logger.error(`Unit not found for character: ${dc.meta.character}`);
                        }
                    }  
                    else if (!reportCharacterDcData.allycodes.find(a => a === swapiUtils.getPlayerAllycode(p)))
                    {
                        // record the character only once per player
                        reportCharacterDcData.allycodes.push(swapiUtils.getPlayerAllycode(p));
                    }
                }
            });

        })
    });

    // set ascending, then level descending, then count descending
    datacronData.datacronsByAbility.sort((a, b) => {
        if (a.setId === b.setId)
        {
            if (a.level === b.level)
            {
                return b.count - a.count;
            }
            
            return b.level - a.level;
        }

        return a.setId - b.setId;
    });

    // descending order by count
    datacronData.datacronsByCharacter.sort((a, b) => {
        return b.allycodes.length - a.allycodes.length;
    })

    return datacronData;
}