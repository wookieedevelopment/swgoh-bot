const { EmbedBuilder, ActionRowBuilder, ButtonBuilder, ButtonStyle, StringSelectMenuBuilder, AttachmentBuilder } = require("discord.js");
const discordUtils = require("../../utils/discord.js");
const twUtils = require("../tw/utils.js");
const guildUtils = require("../../utils/guild.js");
const playerUtils = require("../../utils/player.js");
const swapiUtils = require("../../utils/swapi.js");
const datacronUtils = require("../../utils/datacron.js");
const database = require("../../database.js");
const { SlashCommandSubcommandBuilder } = require("@discordjs/builders");
const metadataCache = require('../../utils/metadataCache.js');
const scoutDatacrons = require("./queries/scoutDatacrons.js");

const PLAYER_CACHE_MAX_HOURS = 12;
const EMBED_COLOR = 0x559922;
const QUERY_CATEGORIES = {
    SUMMARY: "SUMMARY",
    GENERAL: "GENERAL",
    DATACRON: "DATACRON",
    CUSTOM: "CUSTOM"
}

const RENDER_TYPES = {
    COUNT: "COUNT",
    REPORT: "REPORT"
}

const PRESET_REPORT_ORDER = {
    OVERVIEW: -1000,
    TW_OMICONS: -250,
    DATACRONS: -500
}

module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("guild")
        .setDescription("Scout a guild.")
        .addStringOption(o =>
            o.setName("allycode")
            .setDescription("Ally code of someone in the guild to scout.")
            .setRequired(true)),
    interact: async function(interaction)
    {
        let allycode = interaction.options.getString("allycode");

        let response = await this.scout(allycode, async (statusMessage) => { await interaction.editReply(statusMessage); });
        await interaction.editReply(response);
    },
    scout: async function(allycode, updateStatusCallback)
    {
        let allycodeParsed = playerUtils.cleanAndVerifyStringAsAllyCode(allycode);

        let embeds = await getQueryEmbeds((await getAllQueries())[0], allycodeParsed, updateStatusCallback);
        
        let response = 
            { 
                embeds: embeds,
                components:  await getScoutEmbedComponents(allycodeParsed)
            };
        
        return response;
    },
    onSelect: async function(interaction)
    {
        await interaction.reply({ content: "Running calculation, please wait...", components: [], embeds: [] });

        let regex = /scout:[s|g|d|c]:(\d{9})/
        let matches = regex.exec(interaction.customId)

        if (!matches || matches.length < 2)
        {
            await interaction.editReply({ content: "An error occurred, please try again." });
            return;
        }

        let allycode = matches[1];
        let queryId = interaction.values[0];
        let query = (await getAllQueries()).find(q => q.id === parseInt(queryId));   
        
        let embeds = await getQueryEmbeds(query, allycode, async (statusMessage) => { await interaction.editReply(statusMessage); });
        let files = [];
        if (embeds.files){
            files = embeds.files;
            embeds = embeds.embeds;
        }
        await interaction.followUp(
            { 
                embeds: embeds, 
                files: files,
                components: await getScoutEmbedComponents(allycode) 
            });
    }
}

async function getAllQueries()
{
    
    let queries = metadataCache.get(metadataCache.CACHE_KEYS.twScoutQueries);
    if (queries == undefined)
    {
        let data = await database.db.any(
            `SELECT tw_scout_query_id, category, query_name, view_type, queries, sort_order FROM tw_scout_query
            ORDER BY category DESC, sort_order ASC, query_name ASC NULLS LAST;
            `)

        queries = data.map(r => {
            return {
                id: r.tw_scout_query_id,
                order: r.sort_order,
                name: r.query_name,
                view: r.view_type,
                category: r.category,
                queries: r.queries
            }
        })

        metadataCache.set(metadataCache.CACHE_KEYS.twScoutQueries, queries, 2592000000); // 30 days, whatever
    }
    
    return queries;
}

async function getQueryEmbeds(query, allycode, updateStatusCallback)
{
    let guildData = await guildUtils.getGuildDataFromAPI({ allycode: allycode }, { useCache: true })

    let embeds = await getCachedQueryEmbed(query, guildData);
    if (embeds && embeds.length > 0) return embeds;

    // nothing cached, so pull the player rosters
    let guildPlayerData = await getGuildPlayerData(guildData, updateStatusCallback);
    
    embeds = [];

    switch (query.order)
    {
        case PRESET_REPORT_ORDER.OVERVIEW:
            embeds.push(await getGuildOverviewEmbed(guildPlayerData));
            break;
        case PRESET_REPORT_ORDER.TW_OMICONS:
            embeds.push(await getTWOmicronsEmbed(guildPlayerData));
            break;
        case PRESET_REPORT_ORDER.DATACRONS:
            embeds = await scoutDatacrons.getDatacronsEmbeds(guildPlayerData);
            break;
        default:
            if (query.view === RENDER_TYPES.COUNT)
            {
                embeds.push(await getCountQueryEmbed(query, guildPlayerData));
            }
            else if (query.view === RENDER_TYPES.REPORT)
            {
                embeds.push(await getReportQueryEmbed(query, guildPlayerData));
            }
            break;
    }

    // cache results so next time is faster
    await cacheQueryEmbed(query, guildPlayerData, embeds);
    
    return embeds;
}

async function cacheQueryEmbed(query, guildPlayerData, embeds)
{
    await database.db.any(
                `INSERT INTO tw_scout (guild_id, query_id, embeds_obj, timestamp) 
                VALUES ($1, $2, $3, $4)
                ON CONFLICT (guild_id, query_id)
                DO UPDATE SET embeds_obj = $3, timestamp = $4`,
        [guildPlayerData.id, query.id, JSON.stringify(embeds), new Date()])
}

async function getCachedQueryEmbed(query, guildData)
{
    let embeds = await database.db.oneOrNone(`
            SELECT embeds_obj 
            FROM tw_scout 
            WHERE guild_id = $1 AND query_id = $2 AND timestamp > NOW() - interval '${PLAYER_CACHE_MAX_HOURS} hours'
            ORDER BY timestamp DESC LIMIT 1`,
        [swapiUtils.getGuildId(guildData), query.id]);

    return embeds?.embeds_obj;
}

async function getScoutEmbedComponents(allycode)
{
    const summaryCalcRow = new ActionRowBuilder()
        .addComponents(
            new StringSelectMenuBuilder()
                .setCustomId(`scout:s:${allycode}`)
                .setPlaceholder("Choose a Summary Report:") 
                .addOptions(
                    (await getAllQueries()).filter(q => q.category === QUERY_CATEGORIES.SUMMARY).map((v, k) => { return { label: v.name, value: v.id.toString() }})
                )
        )
    const generalCalcRow = new ActionRowBuilder()
        .addComponents(
            new StringSelectMenuBuilder()
                .setCustomId(`scout:g:${allycode}`)
                .setPlaceholder("Choose a General Report:") 
                .addOptions(
                    (await getAllQueries()).filter(q => q.category === QUERY_CATEGORIES.GENERAL).map((v, k) => { return { label: v.name, value: v.id.toString() }})
                )
        )
    const datacronCalcRow = new ActionRowBuilder()
        .addComponents(
            new StringSelectMenuBuilder()
                .setCustomId(`scout:d:${allycode}`)
                .setPlaceholder("Choose a Datacron Report:") 
                .addOptions(
                    (await getAllQueries()).filter(q => q.category === QUERY_CATEGORIES.DATACRON).map((v, k) => { return { label: v.name, value: v.id.toString() }})
                )
        )
    // const customCalcRow = new ActionRowBuilder()
    //     .addComponents(
    //         new StringSelectMenuBuilder()
    //             .setCustomId(`scout:c:${allycode}`)
    //             .setPlaceholder("Choose a Custom Report:") 
    //             .addOptions(
    //                 QUERIES.filter(q => q.category === QUERY_CATEGORIES.CUSTOM).map((v, k) => { return { label: v.name, value: v.id.toString() }})
    //             )
    //     )

    // let buttonRow = new ActionRowBuilder();
    // buttonRow.addComponents(
    //     new ButtonBuilder()
    //         .setCustomId("scout-button-1")
    //         .setLabel("Button 1")
    //         .setStyle(ButtonStyle.Primary),
    //     new ButtonBuilder()
    //         .setCustomId("scout-button-2")
    //         .setLabel("Button 2")
    //         .setStyle(ButtonStyle.Success)
    // );

    return [summaryCalcRow, generalCalcRow, datacronCalcRow /*, customCalcRow*/]; 
}

async function getGuildPlayerData(guildData, updateStatusCallback)
{
    let allycodes = guildUtils.getGuildRoster(guildData)?.map(m => m.allyCode);

    await updateStatusCallback(`Retrieving player roster data for **${guildUtils.getGuildName(guildData)}**...`)

    return {
        name: guildUtils.getGuildName(guildData),
        id: swapiUtils.getGuildId(guildData),
        roster: (await swapiUtils.getPlayers({
            allycodes: allycodes, 
            useCache: true, 
            cacheTimeHours: PLAYER_CACHE_MAX_HOURS, 
            updateStatusCallback: async (allycodes, players, errors) => { 
                await updateStatusCallback(`Retrieved ${players.length}/${allycodes.length} rosters. Errors: ${errors.length}`); 
            }
        })).players
    }
}

async function getTWOmicronsEmbed(guildPlayerData)
{
    let twOmiData = await getTWOmicronData(guildPlayerData);

    let displayText = 
`\`\`\`
Unit                | Ability              | # 
--------------------|----------------------|---
` + twOmiData.omicrons.map(od => (od.count === 0) ? "" : `${discordUtils.fit(od.unitName, 19)} | ${discordUtils.fit(od.abilityName, 20)} | ${discordUtils.fit(od.count, 2)}`).join("\r\n")
+ `
\`\`\``

    let embed = new EmbedBuilder()
        .setTitle(`TW Omicrons - ${guildPlayerData.name}`)
        .setDescription(displayText)
        .setColor(EMBED_COLOR);

    return embed;
}

async function getTWOmicronData(guildPlayerData)
{
    let twOmiData = {
        omicrons: []
    };

    let allAbilities = await swapiUtils.getAbilitiesList();
    let allUnits = await swapiUtils.getUnitsList();

    guildPlayerData.roster?.forEach(p => {
        p.overview?.omicrons[swapiUtils.OMICRON_MODE.TERRITORY_WAR_OMICRON]?.forEach(o => {
            let omiData = twOmiData.omicrons.find(omi => omi.base_id === o);

            if (!omiData) {
                let ability = allAbilities.find(a => a.base_id === o);
                let unit = allUnits.find(u => u.base_id === ability.character_base_id);

                omiData = { 
                    base_id: o,
                    abilityName: ability.name,
                    unitName: unit.name,
                    count: 1
                }
                twOmiData.omicrons.push(omiData);
            } else {
                omiData.count++;
            }
        })
    });

    twOmiData.omicrons.sort((a, b) => b.count - a.count);

    return twOmiData;
}

const META_SHIP_DEF_IDS = ["CAPITALEXECUTOR", "CAPITALPROFUNDITY", "CAPITALLEVIATHAN"];
const GL_DEF_IDS = ["GLREY", "SUPREMELEADERKYLOREN", "GRANDMASTERLUKE", "SITHPALPATINE", "JEDIMASTERKENOBI", "LORDVADER", "JABBATHEHUTT", "GLLEIA", "GLAHSOKATANO"];

async function getGuildOverviewEmbed(guildPlayerData)
{

    let guildInfo = getGuildInfo(guildPlayerData);

    let summaryField = {
        name: "Summary",
        value: `\`\`\`
|    Name: ${guildInfo.name}
| Members: ${guildInfo.membersCount}
|      GP: ${guildInfo.totalGP.toLocaleString()}
| TW Omis: ${guildInfo.twOmicronCount}
|  L9 DCs: ${guildInfo.level9DatacronCount}
\`\`\``,
        inline: false
    };

    let unitsList = await swapiUtils.getUnitsList();
    let glTableText = GL_DEF_IDS.map((glId, ix) => 
        { 
            let glUnit = unitsList.find(u => swapiUtils.getUnitDefId(u) === glId)
            let count = guildInfo.galacticLegends[ix];
            return `| ${discordUtils.fit(glUnit.name, 26)} | ${count}`; 
        }).join("\r\n");

    let glsField = {
        name: "Galactic Legends",
        value: `\`\`\`
| Galactic Legend            | #
|----------------------------|----
${glTableText}
\`\`\``,
        inline: false
    }

    let shipsTableText = META_SHIP_DEF_IDS.map((shipId, ix) => 
        { 
            let shipUnit = unitsList.find(u => swapiUtils.getUnitDefId(u) === shipId)
            let total = guildInfo.metaShips[ix].total;
            let count4 = guildInfo.metaShips[ix].countByStar[3];
            let count5 = guildInfo.metaShips[ix].countByStar[4];
            let count6 = guildInfo.metaShips[ix].countByStar[5];
            let count7 = guildInfo.metaShips[ix].countByStar[6];
            return `| ${discordUtils.fit(shipUnit.name, 15)} | ${discordUtils.fit(total, 2)} | ${discordUtils.fit(count4, 2)} | ${discordUtils.fit(count5, 2)} | ${discordUtils.fit(count6, 2)} | ${count7}`; 
        }).join("\r\n");

    let metaShipsField = {
        name: "Meta Ships",
        value: `\`\`\`
| Meta Ship       | #  | 4* | 5* | 6* | 7* 
|-----------------|----|----|----|----|----
${shipsTableText}
\`\`\``,
        inline: false
    }
    
    let topGACPlayersText = guildInfo.topGACPlayers.map((p) => `| ${discordUtils.fit(swapiUtils.getPlayerName(p), 24)}${discordUtils.LTR} | ${swapiUtils.getPlayerSkillRating(p) ?? "N/A"}`).join("\r\n")
    let topGACRatingsField = {
        name: "Top GAC Players",
        value: `\`\`\`
| Top GAC Player           | Rating 
|--------------------------|--------
${topGACPlayersText}
\`\`\``,
        inline: false
    }
    
    let embed = new EmbedBuilder()
        .setTitle(`TW Scouting - ${guildPlayerData.name} - Overview`)
        .addFields(summaryField, glsField, metaShipsField, topGACRatingsField)
        .setColor(EMBED_COLOR);

    if (guildPlayerData.roster && guildPlayerData.roster.length > 0)
    {
        let allycodesTextForInsightsLink = guildPlayerData.roster.map(r => swapiUtils.getPlayerAllycode(r)).join("%2C");
        let insightsAttackLink = `https://swgoh.gg/gac/insight/squads/?a_ally_code=${allycodesTextForInsightsLink}&squad_size=5`
        let insightsDefenseLink = `https://swgoh.gg/gac/insight/squads/?d_ally_code=${allycodesTextForInsightsLink}&squad_size=5`

        let insightsAttackField = {
            name: "Attack Insights (Requires swgoh.gg Premium)",
            value: `[GAC Attack Insights](${insightsAttackLink})`
        }

        let insightsDefenseField = {
            name: "Defense Insights (Requires swgoh.gg Premium)",
            value: `[GAC Defense Insights](${insightsDefenseLink})`
        }
        embed.addFields(insightsAttackField, insightsDefenseField);
    }

    return embed;
}

function getGuildInfo(guildPlayerData)
{
    let guildInfo = {
        name: guildPlayerData.name,
        membersCount: guildPlayerData.roster?.length ?? 0,
        totalGP: 0,
        twOmicronCount: 0,
        level9DatacronCount: 0,
        galacticLegends: new Array(GL_DEF_IDS.length).fill(0),
        metaShips: META_SHIP_DEF_IDS.map(f => { return { defId: f, total: 0, countByStar: [0, 0, 0, 0, 0, 0, 0] }; }),
        topGACPlayers: []
    }

    guildPlayerData.roster?.forEach(p => {
        guildInfo.twOmicronCount += swapiUtils.getPlayerOmicronCount(p, swapiUtils.OMICRON_MODE.TERRITORY_WAR_OMICRON);
        guildInfo.totalGP += parseInt(swapiUtils.getGP(p));
        guildInfo.level9DatacronCount += swapiUtils.getPlayerDatacrons(p)?.filter(dc => dc.meta.character != null)?.length ?? 0;

        swapiUtils.getPlayerGalacticLegends(p).forEach((glId) => guildInfo.galacticLegends[GL_DEF_IDS.indexOf(glId)]++);

        //swapiUtils.getPlayerGalacticLegends(p).forEach((glId, ix) => guildInfo.galacticLegends[ix]++);

        META_SHIP_DEF_IDS.forEach((mfId, ix) => {
            let ship = swapiUtils.getPlayerRoster(p).find(u => swapiUtils.getUnitDefId(u) === mfId);
            if (ship) {
                guildInfo.metaShips[ix].countByStar[swapiUtils.getUnitRarity(ship) - 1]++;
                guildInfo.metaShips[ix].total++;
            }
        });
    })

    guildInfo.topGACPlayers = [...new Set(guildPlayerData.roster)].sort((a, b) => swapiUtils.getPlayerSkillRating(b) - swapiUtils.getPlayerSkillRating(a)).slice(0, 15);

    return guildInfo;
}

async function getCountQueryEmbed(query, guildPlayerData)
{
    /* output:
    [Query 1, shown like /tw team list]
    Count: [Result]
    
    [Query 2, shown like /tw team list]
    Count: [Result]
    
    [Query 3, shown like /tw team list]
    Count: [Result]

    ...
    */
   
    let fields = [];

    for (let qi in query.queries)
    {
        let q = query.queries[qi];
        let team = await twUtils.parseTeam(q.query, true);
        team.datacronsRequired = q.datacronRequirements?.abilities;
        let count = await getTeamCount(guildPlayerData, team);
    
        let displayText = `${await getTeamDisplayText(team)}`;

        
        fields.push({
            name: `>>>>  Query ${parseInt(qi)+1}. Count: ${count}  <<<<`,
            value: displayText,
            inline: false
        });
    }

    let embed = new EmbedBuilder()
        .setTitle(`TW Scouting - ${guildPlayerData.name} - ${query.name}`)
        .setDescription(`Checked ${guildPlayerData.roster.length} rosters. Data no older than ${PLAYER_CACHE_MAX_HOURS} hours.`)
        .addFields(fields)
        .setColor(EMBED_COLOR);

    return embed;
}

async function getTeamCount(guildPlayerData, team)
{
    let count = 0;
    await Promise.all(guildPlayerData.roster?.map( async (playerData) => 
    {
        if ((await twUtils.isTeamViable(playerData, team)).viable) count++;
    }))

    return count;
}

async function getTeamDisplayText(team)
{
    let text = "";
    let unitsList = await swapiUtils.getUnitsList();

    team.units.forEach(unit => {

        let featuresText = "";
        if (unit.features.length > 0 && unit.features[0] != twUtils.features.nullFeature)
        {
            featuresText += " (";
            for (var f = 0; f < unit.features.length; f++)
            {
                let value = unit.features[f].value;
                let unitMatch = unitsList.find(u => swapiUtils.getUnitDefId(u) === value);
                if (unitMatch) value = unitMatch.name;

                featuresText += twUtils.getFeatureString(unit.features[f].key, unit.features[f].comparison, value)
                if (f != unit.features.length - 1) featuresText += " & "
            }
            featuresText += ")";
        }

        text += `> ${unit.name}${featuresText}\n`;
    });

    
    if (team.datacronsRequired?.length > 0)
    {
        let datacronStrings = await database.db.any("SELECT datacron_id, short_description FROM datacron WHERE datacron_id IN ($1:csv)", [team.datacronsRequired]);
        text += "**Datacron:**\r\n";
        text += team.datacronsRequired?.map(dcId => { 
            return "> " + (datacronStrings.find(dc => dc.datacron_id === dcId)?.short_description ?? "Unknown");
        }).join(" -AND-\r\n") ?? "";
    }

    return text;
}

async function getReportQueryEmbed(query, guildPlayerData)
{
    /* output:
    [Query, shown like /tw team list]
    Player    | Stat1 | Stat2 | Stat3
    ----------|-------|-------|------
    
    Sort by stat1 desc (highest to lowest), then by stat2, etc.
    */
   
    let fields = [];
    let team = await twUtils.parseTeam(query.queries[0].query, true);

    let statsList = [];
    team.units.forEach(u => {
        u.features.forEach(uf => {
            let feature = twUtils.features.options.find(f => f.key.toLowerCase() == uf.key.toLowerCase());
            statsList.push({ unit: u, feature: feature });
        })
    })

    let queryDisplayText = await getTeamDisplayText(team);

    fields.push({
        name: `>>>>  Query  <<<<`,
        value: queryDisplayText,
        inline: false
    });

    const PLAYER_NAME_DISPLAY_LENGTH = 14;
    let guildInfoDisplayLine1 = `------        `;
    let guildInfoDisplayLine2 = `Player        `;
    let guildInfoDisplayLine3 = `--------------`;
    
    const unitNicknames = metadataCache.get(metadataCache.CACHE_KEYS.unitNicknames);
    for (let i = 0; i < 4 && i < statsList.length; i++)
    {
        let nickname = unitNicknames.lowerUnitNicknamesArray.find(nn => nn.defId === statsList[i].unit.defId)?.nickname;
        if (!nickname) nickname = statsList[i].unit.name;
        guildInfoDisplayLine1 += ` | ${discordUtils.fit(nickname.toUpperCase(), 4)}`
        guildInfoDisplayLine2 += ` | ${discordUtils.fit(statsList[i].feature.abbreviation, 4)}`
        guildInfoDisplayLine3 += "-|-----"
    }

    // loop through all players, get values for unit/feature queries
    const playerQueryResults = [];
    guildPlayerData.roster?.forEach(p => {
        const queryResults = {
            rowText: `${discordUtils.fit(swapiUtils.getPlayerName(p), PLAYER_NAME_DISPLAY_LENGTH)}${discordUtils.LTR}`,
            stats: [0, 0, 0, 0]
        }
        for (var si = 0; si < 4 && si < statsList.length; si++)
        {
            let statToShow = statsList[si];
            let rosterUnit = swapiUtils.getPlayerRoster(p).find(u => swapiUtils.getUnitDefId(u) === statToShow.unit.defId);
            let valueText;

            let value = statToShow.feature.getUnitStat(rosterUnit);

            if (value != null && value != twUtils.NO_STAT_FOUND)
            {
                let rounded = value, suffix = "";
                if (value > 1000000) { rounded = Number(Math.round(value/1000000.0 + "e2") + "e-2"); suffix = "M" }
                else if (value > 1000) { rounded = Number(Math.round(value/1000.0)); suffix = "K" }

                valueText = (rounded.toString() + suffix).padEnd(4);
                queryResults.stats[si] = value;
            }
            
            if (valueText == null)
            {
                valueText = " -- ";
                queryResults.stats[si] = -9999999;
            }
            queryResults.rowText += ` | ${valueText}`;
        }

        playerQueryResults.push(queryResults);
    });
    
    // sort values
    playerQueryResults.sort((a, b) => {
        for (let si = 0; si < a.stats.length; si++)
        {
            if (a.stats[si] === b.stats[si]) continue;

            return b.stats[si] - a.stats[si];
        }
        return 0;
    })

    // populate display
    let playerReportText = playerQueryResults.map(qr => qr.rowText).join("\r\n");

    let embed = new EmbedBuilder()
        .setTitle(`TW Scouting - ${guildPlayerData.name} - ${query.name}`)
        .setDescription(
`\`\`\`
${guildInfoDisplayLine1}
${guildInfoDisplayLine2}
${guildInfoDisplayLine3}
${playerReportText}
\`\`\``)
        .addFields(fields)
        .setColor(EMBED_COLOR);

    return embed;
}