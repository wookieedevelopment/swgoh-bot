const { SlashCommandSubcommandBuilder, EmbedBuilder, AttachmentBuilder, Embed } = require("discord.js");
const playerUtils = require("../../utils/player");
const guildUtils = require("../../utils/guild");
const swapiUtils = require("../../utils/swapi");
const discordUtils = require("../../utils/discord");
const unitsUtils = require("../../utils/units");
const queueUtils = require("../../utils/queue");
const { CLIENT } = require("../../utils/discordClient");
const STATS = require("../../data/stats.json");
const MOD_SETS = require("../../data/mod_sets.json");
const comlink = require("../../utils/comlink");
const drawingUtils = require("../../utils/drawing");
const { CanvasTable, CTConfig } = require("canvas-table");
const { logger, formatError } = require("../../utils/log");
const twUtils = require("../tw/utils");
const twManagerUtils = require("../tw/twManager/utils");
const constants = require("../../utils/constants");
const { drawText }  = require("canvas-txt");
const entitlements = require("../../utils/entitlements");

const QUEUE_FUNCTION_KEY = "scout:mods";
const MIN_GEAR_LEVEL_TO_CONSIDER = 7;
const MAX_ALLYCODES_FOR_FORCE_CACHE = 25;

module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("mods")
        .setDescription("Scout mods for players or guilds.")
        .addStringOption(o =>
            o.setName("allycodes")
            .setDescription("List of Ally Codes to query.")
            .setMaxLength(1000)
            .setRequired(true))
        .addStringOption(o =>
            o.setName("units")
            .setDescription("Comma-separated list of units to query.")
            .setRequired(true))
        .addBooleanOption(o =>
            o.setName("guild")
            .setDescription("Check the full guild for the Ally Codes provided")
            .setRequired(false))
        .addBooleanOption(o =>
            o.setName("only-fully-modded")
            .setDescription("Only include units that are fully modded (default: true)")
            .setRequired(false))
        .addStringOption(o =>
            o.setName("filter")
            .setDescription("Only search players whose rosters match the filter.")
            .setRequired(false))
        .addBooleanOption(o =>
            o.setName("force-cache")
            .setDescription(`Pull new game data (only when <${MAX_ALLYCODES_FOR_FORCE_CACHE} allycodes total)`)
            .setRequired(false)),
    interact: async function(interaction)
    {
        let missingPermissions = discordUtils.getMissingBotPermissions(interaction, interaction.channel);
        if (missingPermissions)
        {
            await interaction.editReply(discordUtils.convertMissingBotPermissionsArrayToResponse(missingPermissions));
            return;
        }

        let isSubscriber = await playerUtils.isSubscriberInteraction(interaction);
        if (!isSubscriber)
        {
            await interaction.editReply(entitlements.generateNotSubscribedResponseObject());
            return;
        }

        let allycodes = interaction.options.getString("allycodes");
        let queryGuild = interaction.options.getBoolean("guild");
        let onlyFullyModded = interaction.options.getBoolean("only-fully-modded") ?? true;
        let units = interaction.options.getString("units");
        let filter = interaction.options.getString("filter");
        let forceCache = interaction.options.getBoolean("force-cache") ?? false;

        let unitDefIds = await unitsUtils.parseUnitsStringAsDefIds(units);
        let uniqueDefIds = [...new Set(unitDefIds)];

        if (uniqueDefIds.length === 0)
        {
            await interaction.editReply({ content: `No matching unit names found. Please write more of the unit name. If there is a missing abbreviation/nickname, please contact the developer.` });
            return;
        }

        let { allycodeList, errors } = await parseAllycodes(allycodes, queryGuild);

        if (errors.length > 0)
        {
            await interaction.editReply({ content: `One or more errors were encountered:\n- ${errors.join("\n- ")}` });
            return;
        }

        if (queryGuild && allycodeList.length > 250)
        {
            await interaction.editReply({ content: `Please limit to maximum of 5 guilds.`});
            return;
        }

        let msgPrepend = "";
        if (forceCache && allycodeList.length > MAX_ALLYCODES_FOR_FORCE_CACHE) 
        {
            msgPrepend = `${discordUtils.symbols.warning} \`force-cache\` ignored. Max Ally Codes ${MAX_ALLYCODES_FOR_FORCE_CACHE}. Your request is for ${allycodeList.length} Ally Codes.\n`;
            forceCache = false;
        }

        let qsm = await queueUtils.getQueueSizeMessage(queueUtils.QUEUES.OTHER, interaction.client.shard);
        const editMessage = await interaction.editReply(`${msgPrepend}Processing scout mods. ${qsm}`);

        await queueUtils.queueOtherJob(interaction.client.shard, 
        {
            functionKey: QUEUE_FUNCTION_KEY,
            data: { 
                type: "MODS",
                forceCache: forceCache, 
                onlyFullyModded: onlyFullyModded,
                allycodes: allycodeList, 
                unitDefIds: uniqueDefIds,
                filter: filter,
            },
            channelId: editMessage.channelId ?? editMessage.channel.id,
            messageId: editMessage.id,
            userId: interaction.user.id,
        },
        {
            expireInHours: 4
        });
    }
}

async function scoutModsJobHandler(job)
{
    let channel, editMessage;
    try 
    {
        channel = CLIENT.channels.cache.get(job.channelId);
        editMessage = (await channel.messages.fetch({ message: job.messageId }));
    }
    catch (e)
    {
        let discordUser = await CLIENT.users.fetch(job.userId);
        await discordUser.send(
            {
                content: `WookieeBot may be missing permissions in channel <#${job.channelId}>. Please verify with \`/check-permissions\``
            }
        );

        return;
    }

    try 
    {
        await editMessage.edit({ content: "Processing your `/scout mods` request now...", components: [], files: [], embeds: [] });

        let playersData = await swapiUtils.getPlayers({
            allycodes: job.data.allycodes,
            useCache: !job.data.forceCache,
            updateStatusCallback: async (allycodes, players, errors) => { 
                let status = `Retrieved ${players.length}/${allycodes.length} rosters. Errors: ${errors.length}`;
                await editMessage.edit(status);
            }
        });

        let { filterString, players } = await applyFilterToPlayers(playersData.players, job.data.filter);

        let response;

        if (players.length == 0)
        {
            let embed = new EmbedBuilder()
                .setTitle("No Players Matching Filter")
                .setDescription(`No players match the filter provided:\n${filterString}`)
                .setColor("DarkRed");

            response = { embeds: [embed] };
        }
        else
        {
            let modData = await catalogUnitMods(players, job.data.unitDefIds, job.data.onlyFullyModded);

            if (modData.length === 0)
            {
                response = { content: `No matching players.` }
            }
            else
            {
                response = await generateResponseFromModData(modData);
            }

            if (filterString?.length > 0)
            {
                let playersNotMatching = playersData.players.length - players.length;
                let embed = new EmbedBuilder()
                    .setTitle("Filter")
                    .setDescription(
`Filter:
${filterString}

Data from ${players.length}/${playersData.players.length} scanned players.
${playersNotMatching === 0 ? "All players match filter." : `${playersNotMatching} player${playersNotMatching == 1 ? "" : "s"} did not match filter.`}
`)
                    .setColor("DarkGreen");

                response.embeds = [embed];
            }
        }

        response.content = `<@${job.userId}>` + (response.content ? `\n${response.content}` : "") + `\n${playersData.players.length < 20 ? `**Players:**\n${playersData.players.map(p => `- ${swapiUtils.getPlayerName(p)} (${swapiUtils.getPlayerAllycode(p)})`).join("\n")}` : `**${playersData.players.length} Players**` }\n**Only fully modded:** ${job.data.onlyFullyModded ? "Yes" : "No"}\n\n`;
        if (playersData.errors.length > 0)
        {
            response.content += `
There were errors retrieving player data:
- ${playersData.errors.join("\n- ")}`;
        } 
        await editMessage.reply(response);
    }
    catch (e)
    {
        logger.error(formatError(e));
        await editMessage.reply(discordUtils.createErrorEmbed("Error", "An error occurred processing the request. Please try again or connect with WookieeBot developers for help."));
    }
}

queueUtils.addOtherFunction(QUEUE_FUNCTION_KEY,  scoutModsJobHandler);

async function applyFilterToPlayers(players, filter)
{
    let retObj = { filterString: null, players: players };
    if (!filter || filter.trim().length == 0) return retObj;

    // create a "team" from the filter
    let team = await twUtils.parseTeam(filter);

    let matches = new Array();

    // catalog all players that have the team
    try {
        await Promise.all(
            players.map(async (player) =>
            {
                if ((await twUtils.isTeamViable(player, team)).viable) 
                    matches.push(player);
            })
        );
    } catch (err)
    {
        logger.error(formatError(err))
        throw new Error("An error occurred checking the filter. Please try again.");    
    }

    retObj.players = [...new Set(matches)];
    
    let filterString = await twManagerUtils.createFilterString(team);
    filterString = "- " + filterString.replace(/;/g, "\n- ");

    retObj.filterString = filterString;

    return retObj;
}

const STATS_TO_SEARCH = [
    { key: "Health", statId: swapiUtils.STATS.MAX_HEALTH, func: (u) => u.stats.final[swapiUtils.STATS.MAX_HEALTH] ?? 0 },
    { key: "Protection", statId: swapiUtils.STATS.MAX_SHIELD, func: (u) => u.stats.final[swapiUtils.STATS.MAX_SHIELD] ?? 0 },
    { key: "EHP: (H+P)/(1-A)", statId: swapiUtils.STATS.EXTRA.EHP, func: (u) => playerUtils.calculateEHP(u.stats.final[swapiUtils.STATS.MAX_HEALTH] ?? 0, u.stats.final[swapiUtils.STATS.MAX_SHIELD] ?? 0, u.stats.final[swapiUtils.STATS.ARMOR] ?? 0) },
    { key: "Speed", statId: swapiUtils.STATS.SPEED, func: (u) => u.stats.final[swapiUtils.STATS.SPEED] ?? 0 },
    { key: "Potency", statId: swapiUtils.STATS.POTENCY, func: (u) => u.stats.final[swapiUtils.STATS.POTENCY] ?? 0 },
    { key: "Tenacity", statId: swapiUtils.STATS.TENACITY, func: (u) => u.stats.final[swapiUtils.STATS.TENACITY] ?? 0 },
    { key: "Crit. Damage", statId: swapiUtils.STATS.CRITICAL_DAMAGE, func: (u) => u.stats.final[swapiUtils.STATS.CRITICAL_DAMAGE] ?? 0 },
    { key: "Crit. Chance", statId: swapiUtils.STATS.ATTACK_CRITICAL_RATING, func: (u) => u.stats.final[swapiUtils.STATS.ATTACK_CRITICAL_RATING] ?? 0 },
    { key: "Phys. Damage", statId: swapiUtils.STATS.PHYSICAL_DAMAGE, func: (u) => u.stats.final[swapiUtils.STATS.PHYSICAL_DAMAGE] ?? 0 },
    { key: "Armor", statId: swapiUtils.STATS.ARMOR, func: (u) => u.stats.final[swapiUtils.STATS.ARMOR] ?? 0 },
    { key: "Crit. Avoidance", statId: swapiUtils.STATS.PHYSICAL_CRITICAL_AVOIDANCE, func: (u) => u.stats.final[swapiUtils.STATS.PHYSICAL_CRITICAL_AVOIDANCE] ?? 0 },
    { key: "Accuracy", statId: swapiUtils.STATS.PHYSICAL_ACCURACY, func: (u) => u.stats.final[swapiUtils.STATS.PHYSICAL_ACCURACY] ?? 0 }
];

const MAX_MODS_PER_UNIT = 6;

async function catalogUnitMods(players, unitDefIds, onlyFullyModded)
{
    let modData = new Array();
    let unitData = await swapiUtils.getUnitsList();
    let modSetData = await comlink.getModSetData();

    for (let defId of unitDefIds)
    {
        let unit = unitData.find(u => swapiUtils.getUnitDefId(u) === defId);
        let isCharacter = swapiUtils.getUnitCombatType(unit) === constants.COMBAT_TYPES.SQUAD;

        let matchingUnits = players.map(
                    p => swapiUtils.getPlayerRoster(p).find(
                        u => swapiUtils.getUnitDefId(u) === defId 
                             && (!isCharacter || swapiUtils.getUnitGearLevel(u) >= MIN_GEAR_LEVEL_TO_CONSIDER)
                             && (!isCharacter || !onlyFullyModded || u.equippedStatMod.length === MAX_MODS_PER_UNIT))
                    ).filter(u => u !== undefined);

        if (matchingUnits.length === 0) continue;

        let unitModData = {
            defId: defId,
            unitName: unit.name,
            count: matchingUnits.length,
            averages: {
                relic: 0,
                gear: 0
            },
            stats: {},
            modCombos: [],
            secondaries: {},
            primaries: {}
        };
        
        unitModData.primaries[constants.MOD_SLOTS.SQUARE] = [];
        unitModData.primaries[constants.MOD_SLOTS.ARROW] = [];
        unitModData.primaries[constants.MOD_SLOTS.DIAMOND] = [];
        unitModData.primaries[constants.MOD_SLOTS.TRIANGLE] = [];
        unitModData.primaries[constants.MOD_SLOTS.CIRCLE] = [];
        unitModData.primaries[constants.MOD_SLOTS.CROSS] = [];

        if (isCharacter)
        {
            let totalRelic = 0, totalGear = 0;
            for (let u of matchingUnits) {
                totalGear += swapiUtils.getUnitGearLevel(u);
                totalRelic += swapiUtils.getUnitRelicLevel(u) - 2;
            }
            unitModData.averages.gear = Math.round(totalGear/matchingUnits.length);
            unitModData.averages.relic = Math.round(totalRelic/matchingUnits.length);

            let modSetTypes = Object.keys(MOD_SETS.enum);
            for (let u of matchingUnits)
            {
                let sets = [];
                let modSetIds = u.equippedStatMod.map(m => modSetData.modDefinitionIdMap[m.definitionId].setId);
                for (let t of modSetTypes)
                {
                    let modsInSet = modSetIds.filter(msi => msi === t);
                    let fullSetCount = Math.floor(modsInSet.length / MOD_SETS.data[MOD_SETS.enum[t]].setSize);
                    if (fullSetCount > 0) sets = sets.concat(new Array(fullSetCount).fill(t));
                }

                // no full sets, don't record
                if (sets.length == 0) continue;

                u.equippedStatMod.forEach((m) => {
                    if (m.primaryStat)
                    {
                        
                        let p = unitModData.primaries[m.setId.slot].find(s => s.unitStatId === m.primaryStat.stat.unitStatId);
                        if (!p)
                        {
                            p = {
                                unitStatId: m.primaryStat.stat.unitStatId,
                                count: 0
                            };

                            unitModData.primaries[m.setId.slot].push(p);
                        }

                        p.count++;
                    }

                    if (!m.secondaryStat || m.secondaryStat.length == 0) return;
                    
                    let stats = m.secondaryStat.map(s => s.stat.unitStatId);
                    for (let s of stats) 
                    {
                        if (unitModData.secondaries[s]) unitModData.secondaries[s]++;
                        else unitModData.secondaries[s] = 1;
                    }
                })


                let existingCombo = unitModData.modCombos.find(mc => mc.sets.length == sets.length && (sets.filter(s => mc.sets.find(mcs => mcs === s)).length === sets.length));
                if (existingCombo) existingCombo.count++;
                else
                {
                    // sets that require more quantities first
                    sets.sort((a, b) => MOD_SETS.data[MOD_SETS.enum[b]].setSize - MOD_SETS.data[MOD_SETS.enum[a]].setSize);
                    unitModData.modCombos.push({
                        count: 1,
                        sets: sets,
                        names: sets.map(s => MOD_SETS.data[MOD_SETS.enum[s]].name)
                    });
                }
            }

            unitModData.modCombos.sort((a, b) => b.count - a.count);
        }

        if (matchingUnits.length > 0)
        {
            for (let s of STATS_TO_SEARCH)
            {
                matchingUnits.sort((a, b) => s.func(a) - s.func(b));

                let median = s.func(matchingUnits[Math.floor(matchingUnits.length/2)]);

                let avg = matchingUnits.reduce((p, c) => p + s.func(c), 0) / matchingUnits.length;

                unitModData.stats[s.key] = {
                    average: avg,
                    median: median,
                    high: s.func(matchingUnits[matchingUnits.length-1]),
                    low: s.func(matchingUnits[0])
                }
            }
        }

        modData.push(unitModData);
    }

    return modData;
}

async function generateResponseFromModData(modData)
{
    if (modData.length === 0)
    {
        return { content: "No players match. Double-check allycodes, or units may not be modded." };
    }

    let buffer = await drawScoutModsResultToBuffer(modData);
    
    let fileName = "scout-mods-" + Date.now() + ".png"
    const file = new AttachmentBuilder(buffer, { name: fileName });
    return { files: [file] };
}

async function parseAllycodes(allycodes, guild)
{
    let spl = allycodes.split(/[,\ ;:|]+/g);

    let allycodeList = new Array();
    let errors = new Array();

    for (let a of spl)
    {
        try {
            let allycode = playerUtils.cleanAndVerifyStringAsAllyCode(a);

            if (allycodeList.find(ac => ac === allycode)) continue;

            if (guild)
            {
                let guildData = await guildUtils.getGuildDataFromAPI(
                    { allycode: allycode }, 
                    { useCache: false, includeRecentGuildActivityInfo: true });
                let acs = guildUtils.getGuildAllycodesForAPIGuild(guildData);
                allycodeList = allycodeList.concat(acs);
            }
            else
            {
                allycodeList.push(allycode);
            }
        } catch (e)
        {
            errors.push(e.message);
        }
    }

    return { allycodeList, errors };
}

const CANVAS_DATA = {
    canvasBgColor: "#000",
    unitRowHeight: 600,
    unitImageColumnWidth: 200,
    statsTableRowHeight: 20,
    statsTableNameColumnWidth: 70,
    statsTableStatColumnWidth: 70,
    statsTableWidth: 670, 
    modSetsWidth: 230,
    modSetIconWidth: 40,
    modSetComboRowHeight: 50,
    primariesWidth: 250,
    primariesRowHeight: 70,
    primariesSlotIconWidthHeight: 40,
    secondariesWidth: 250,
    padding: 10,
    paddingBetweenDataColumns: 15,
};

async function drawScoutModsResultToBuffer(modData)
{
    const width = CANVAS_DATA.padding*2
                    + CANVAS_DATA.unitImageColumnWidth
                    + CANVAS_DATA.statsTableWidth
                    + CANVAS_DATA.modSetsWidth
                    + CANVAS_DATA.primariesWidth
                    + CANVAS_DATA.secondariesWidth
                    + CANVAS_DATA.paddingBetweenDataColumns*3;
    
    const height = CANVAS_DATA.padding*2 + CANVAS_DATA.unitRowHeight*modData.length;

    const { canvas, context } = drawingUtils.createBlankCanvas(width, height, CANVAS_DATA.canvasBgColor);

    for (let m = 0; m < modData.length; m++)
    {
        let yRowTop = CANVAS_DATA.padding + CANVAS_DATA.unitRowHeight*m;

        await drawUnitRow(context, yRowTop, modData[m]);
    }

    
    const buffer = canvas.toBuffer('image/png');

    return buffer;
}

const STATS_TABLE_COLUMNS = [
    { title: "Stat", options: { maxWidth: 80, minWidth: 80 } },
    { title: "Average", options: { maxWidth: 50, minWidth: 50 } },
    { title: "Median", options: { maxWidth: 50, minWidth: 50 } },
    { title: "High", options: { maxWidth: 50, minWidth: 50 } },
    { title: "Low", options: { maxWidth: 50, minWidth: 50 } }
];

const STATS_TABLE_OPTIONS = {
    borders: {
        column: { width: 1, color: "#aaa" },
        header: { width: 2, color: "#aaa" }, // set to false to hide the header
        row: { width: 1, color: "#aaa" },
        table: { width: 2, color: "#aaa" }
    },
    background: "#000",
    fit: false,
    cell: {
        fontSize: 10,
        color: "#ccc",
        padding: { left: 3, right: 3, bottom: 5, top: 3 },
        lineHeight: 1.2
    },
    header: {
        background: "#000055",
        fontSize: 10,
        color: "#ccc",
        padding: { left: 3, right: 3, bottom: 5, top: 3 },
        lineHeight: 1.2
    },
    fader: {
        right: false, top: false, bottom: false, left: false
    },
    padding: {
        right: 0, top: 0, bottom: 0, left: 0
    }
}

async function drawUnitRow(context, yRowTop, unitModData)
{
    // unit portrait
    let portrait = drawingUtils.getLoadedPortraitImage(unitModData.defId);
    let portraitSize = CANVAS_DATA.unitImageColumnWidth;
    context.drawImage(portrait,
        CANVAS_DATA.padding + (CANVAS_DATA.unitImageColumnWidth - portraitSize)/2,
        yRowTop, // top of unit row
        portraitSize,
        portraitSize);

    context.textBaseline = "middle";
    context.fillStyle = '#ccc';
    context.font = `20px "Noto Sans Extra Bold"`;
    context.textAlign = "left";
    context.fillText(
`Count: ${unitModData.count}
Avg. Gear: ${unitModData.averages.gear}
Avg. Relic: ${unitModData.averages.relic}`, 
        CANVAS_DATA.padding + CANVAS_DATA.unitImageColumnWidth/6,
        yRowTop + portraitSize + 20)

    // stats table
    await drawStatsTable(context, yRowTop, unitModData);

    // mod sets
    await drawModSets(context, yRowTop, unitModData);

    // primaries
    await drawPrimaries(context, yRowTop, unitModData);

    // secondaries
    await drawSecondaries(context, yRowTop, unitModData);
}

async function drawStatsTable(context, yRowTop, unitModData)
{
    let data = [];
    for (let s of STATS_TO_SEARCH)
    {
        let row = [
            s.key,
            unitsUtils.formatStat(unitModData.stats[s.key].average, s.statId).toString(),
            unitsUtils.formatStat(unitModData.stats[s.key].median, s.statId).toString(),
            unitsUtils.formatStat(unitModData.stats[s.key].high, s.statId).toString(),
            unitsUtils.formatStat(unitModData.stats[s.key].low, s.statId).toString(),
        ];

        data.push(row);
    };

    const config = { 
        data: data,
        columns: STATS_TABLE_COLUMNS,
        options: STATS_TABLE_OPTIONS,
        event: null
    };

    const tableCanvas = drawingUtils.createBlankCanvas(CANVAS_DATA.statsTableWidth, CANVAS_DATA.unitRowHeight, CANVAS_DATA.canvasBgColor);
    const ct = new CanvasTable(tableCanvas.canvas, config);
    await ct.generateTable();
    let buffer = await ct.renderToBuffer();

    await drawingUtils.loadAndDrawImage(context, buffer, CANVAS_DATA.padding + CANVAS_DATA.unitImageColumnWidth + CANVAS_DATA.paddingBetweenDataColumns, yRowTop, tableCanvas.canvas.width, tableCanvas.canvas.height)

}

async function drawModSets(context, yRowTop, unitModData)
{
    let startingX = CANVAS_DATA.padding + CANVAS_DATA.unitImageColumnWidth + CANVAS_DATA.statsTableWidth + CANVAS_DATA.paddingBetweenDataColumns*2;
    context.textBaseline = "middle";
    context.fillStyle = '#FFCC00';
    context.font = `20px "Noto Sans Extra Bold"`;
    context.textAlign = "left";
    context.fillText("Mod Set Combos", startingX, yRowTop + 20);


    for (let mcIx = 0; mcIx < unitModData.modCombos.length && mcIx < 8; mcIx++)
    {
        let combo = unitModData.modCombos[mcIx];
        for (let setIx = 0; setIx < combo.sets.length; setIx++)
        {
            let set = combo.sets[setIx];
            let img;
            switch (parseInt(set))
            {
                case MOD_SETS.data.CRIT_CHANCE.id:
                    img = drawingUtils.getImage(drawingUtils.IMAGES.MOD_SETS.CRITICAL_CHANCE);
                    break;
                case MOD_SETS.data.CRIT_DAMAGE.id:
                    img = drawingUtils.getImage(drawingUtils.IMAGES.MOD_SETS.CRITICAL_DAMAGE);
                    break;
                case MOD_SETS.data.DEFENSE.id:
                    img = drawingUtils.getImage(drawingUtils.IMAGES.MOD_SETS.DEFENSE);
                    break;
                case MOD_SETS.data.HEALTH.id:
                    img = drawingUtils.getImage(drawingUtils.IMAGES.MOD_SETS.HEALTH);
                    break;
                case MOD_SETS.data.OFFENSE.id:
                    img = drawingUtils.getImage(drawingUtils.IMAGES.MOD_SETS.OFFENSE);
                    break;
                case MOD_SETS.data.POTENCY.id:
                    img = drawingUtils.getImage(drawingUtils.IMAGES.MOD_SETS.POTENCY);
                    break;
                case MOD_SETS.data.SPEED.id:
                    img = drawingUtils.getImage(drawingUtils.IMAGES.MOD_SETS.SPEED);
                    break;
                case MOD_SETS.data.TENACITY.id:
                    img = drawingUtils.getImage(drawingUtils.IMAGES.MOD_SETS.TENACITY);
                    break;
            }

            context.font = `20px "Noto Sans"`;
            context.textBaseline = "middle";
            context.fillStyle = '#ccc';
            context.fillText(combo.count + " x", startingX, yRowTop + 40 + (mcIx + 0.5) * CANVAS_DATA.modSetComboRowHeight);
            context.drawImage(img,
                startingX + 43 + setIx * 50,
                yRowTop + 40 + (50-CANVAS_DATA.modSetIconWidth)/2 + mcIx * CANVAS_DATA.modSetComboRowHeight,
                CANVAS_DATA.modSetIconWidth,
                CANVAS_DATA.modSetIconWidth);
        }
    }
}

let PRIMARY_DISPLAY = [
    // {
    //     slotId: constants.MOD_SLOTS.SQUARE,
    //     image: drawingUtils.IMAGES.MOD_SPOTS.SQUARE
    // },
    {
        slotId: constants.MOD_SLOTS.ARROW,
        image: drawingUtils.IMAGES.MOD_SPOTS.ARROW
    },
    // {
    //     slotId: constants.MOD_SLOTS.DIAMOND,
    //     image: drawingUtils.IMAGES.MOD_SPOTS.DIAMOND
    // },
    {
        slotId: constants.MOD_SLOTS.TRIANGLE,
        image: drawingUtils.IMAGES.MOD_SPOTS.TRIANGLE
    },
    {
        slotId: constants.MOD_SLOTS.CIRCLE,
        image: drawingUtils.IMAGES.MOD_SPOTS.CIRCLE
    },
    {
        slotId: constants.MOD_SLOTS.CROSS,
        image: drawingUtils.IMAGES.MOD_SPOTS.CROSS
    },
];
async function drawPrimaries(context, yRowTop, unitModData)
{
    let startingX = CANVAS_DATA.padding 
            + CANVAS_DATA.unitImageColumnWidth 
            + CANVAS_DATA.statsTableWidth 
            + CANVAS_DATA.modSetsWidth
            + CANVAS_DATA.paddingBetweenDataColumns*3;
    context.textBaseline = "middle";
    context.fillStyle = '#098Fcc';
    context.font = `20px "Noto Sans Extra Bold"`;
    context.textAlign = "left";
    context.fillText("Primaries (Top 2)", startingX, yRowTop + 20);

    context.font = `20px "Noto Sans"`;
    context.textBaseline = "middle";
    context.textAlign = "left";
    context.fillStyle = '#ccc';
    for (let pdIx = 0; pdIx < PRIMARY_DISPLAY.length; pdIx++)
    {
        // sort primary stat by # of hits DESC
        unitModData.primaries[PRIMARY_DISPLAY[pdIx].slotId].sort((a, b) => b.count - a.count);
                
        context.drawImage(drawingUtils.getImage(PRIMARY_DISPLAY[pdIx].image),
            startingX,
            yRowTop + 45 + (pdIx)*CANVAS_DATA.primariesRowHeight,
            CANVAS_DATA.primariesSlotIconWidthHeight,
            CANVAS_DATA.primariesSlotIconWidthHeight);

        let primariesStrings = unitModData.primaries[PRIMARY_DISPLAY[pdIx].slotId]
            .slice(0, 2)
            .map(p => `${STATS[p.unitStatId]} (${p.count})`);
            
        let startingY = yRowTop + 55 + pdIx*CANVAS_DATA.primariesRowHeight;

        for (let strIx = 0; strIx < primariesStrings.length; strIx++)
        {
            context.fillText(primariesStrings[strIx], 
                startingX + CANVAS_DATA.primariesSlotIconWidthHeight + 10, 
                startingY + 25*strIx);
        }
    }
}

async function drawSecondaries(context, yRowTop, unitModData)
{
    let startingX = CANVAS_DATA.padding 
            + CANVAS_DATA.unitImageColumnWidth 
            + CANVAS_DATA.statsTableWidth 
            + CANVAS_DATA.modSetsWidth
            + CANVAS_DATA.primariesWidth
            + CANVAS_DATA.paddingBetweenDataColumns*4;
    context.textBaseline = "middle";
    context.fillStyle = '#098F26';
    context.font = `20px "Noto Sans Extra Bold"`;
    context.textAlign = "left";
    context.fillText("Top Secondaries", startingX, yRowTop + 20);


    // sort secondary stats by # of hits DESC
    let secondaryStats = Object.keys(unitModData.secondaries);
    secondaryStats.sort((a, b) => unitModData.secondaries[b] - unitModData.secondaries[a]);

    for (let s = 0; s < secondaryStats.length && s < 12; s++)
    {
        context.font = `20px "Noto Sans"`;
        context.textBaseline = "middle";
        context.fillStyle = '#ccc';
        context.fillText(`${unitModData.secondaries[secondaryStats[s]]} x ${STATS[secondaryStats[s]]}`, 
            startingX, 
            yRowTop + 60 + s * 40);
    }
}