const { EmbedBuilder } = require("discord.js");
const got = require("got");
const { SlashCommandBuilder } = require('@discordjs/builders');

//const PERMISSIONS = BigInt(0x0000000040) + BigInt(0x0000000400) + BigInt(0x0000000800) + BigInt(0x0000008000) + BigInt(0x0000010000) + BigInt(0x0080000000);
const INVITE_LINK = "https://discord.com/api/oauth2/authorize?client_id=537868729732562944&scope=bot%20applications.commands&permissions=517544070208";
module.exports = {
    data: new SlashCommandBuilder()
        .setName("invite")
        .setDescription("Invite WookieeBot to your server"),
    name: "invite",
    description: "Invite WookieeBot to your server",
    interact: async function(interaction)
    {
        await interaction.editReply(`Invite WookieeBot to your server:\r\n${INVITE_LINK}`);
    },
    execute: async function(client, message, args) {
        await message.author.send(`Invite WookieeBot to your server:\r\n${INVITE_LINK}`);
    }
}