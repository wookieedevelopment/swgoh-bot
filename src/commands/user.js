const discordUtils = require("../utils/discord.js");
const userHelp = require("./user/help");
const userMe = require("./user/me");
const userTimeZone = require("./user/timezone");
const { UserManager } = require("discord.js");
const { SlashCommandBuilder } = require('@discordjs/builders');

module.exports = {
    data: new SlashCommandBuilder()
        .setName("user")
        .setDescription("User Management")
        .addSubcommand(userHelp.data)
        .addSubcommand(userMe.data)
//      .addSubcommand(userTimeZone.data)
        ,
    interact: async function(interaction)
    {

        let subcommand = interaction.options.getSubcommand();
        switch (subcommand)
        {
            case userMe.data.name:
                await userMe.interact(interaction);
                break;
            // case "settimezoneoffset":
            //     await userTimeZone.setOffset(interaction);
            //     break;
            case userHelp.data.name:
                await userHelp.interact(interaction);
                break;            
        }
    },
    autocomplete: async function(interaction)
    {
    },
}