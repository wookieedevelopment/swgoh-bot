const leaderboardShow = require("./leaderboard/show");
const leaderboardGuild = require("./leaderboard/guild");
const leaderboardPlayer = require("./leaderboard/player");
const { SlashCommandBuilder } = require('@discordjs/builders');

module.exports = {
    data: new SlashCommandBuilder()
        .setName("lb")
        .setDescription("Leaderboard")
        .addSubcommand(leaderboardShow.data)
        .addSubcommand(leaderboardGuild.data)
        .addSubcommand(leaderboardPlayer.data),
    interact: async function(interaction)
    {
        let subcommand = interaction.options.getSubcommand();
        switch (subcommand)
        {
            case leaderboardShow.data.name:
                await leaderboardShow.interact(interaction);
                break;
            case leaderboardGuild.data.name:
                await leaderboardGuild.interact(interaction);
                break;
            case leaderboardPlayer.data.name:
                await leaderboardPlayer.interact(interaction);
                break;
        }
    },
    handleButton: async function(interaction)
    {  
    },
    onSelect: async function(interaction)
    {
    },
    autocomplete: async function(interaction)
    {
        let subcommand;
        try { subcommand = interaction.options.getSubcommand(); } catch {}
        if (subcommand)
        {
            switch (subcommand) {
                case leaderboardGuild.data.name:
                    await leaderboardGuild.autocomplete(interaction);
                    break;
                case leaderboardShow.data.name:
                    await leaderboardShow.autocomplete(interaction);
                    break;
                case leaderboardPlayer.data.name:
                    await leaderboardPlayer.autocomplete(interaction);
                    break;
            }
            return;
        }
    }
}