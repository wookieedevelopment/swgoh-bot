const tbOps = require("./tb/ops");
const tbCombat = require ("./tb/combat")
const tbReady = require("./tb/ready")
const tbSpecials = require("./tb/specials");
const tbTeams = require("./tb/teams");
const { SlashCommandBuilder } = require('@discordjs/builders');

module.exports = {
    data: new SlashCommandBuilder()
        .setName("tb")
        .setDescription("Territory Battles")
        .addSubcommand(tbOps.data)
        .addSubcommand(tbCombat.data)
        .addSubcommand(tbReady.data)
        .addSubcommand(tbTeams.data)
        .addSubcommandGroup(tbSpecials.data)
        ,
    interact: async function(interaction)
    {
        let group, subcommand;
        try { group = interaction.options.getSubcommandGroup(); } catch {}

        if (group)
        {
            switch (group) {
                case tbSpecials.data.name:
                    await tbSpecials.interact(interaction);
                    break;
            }
            return;
        }

        try { subcommand = interaction.options.getSubcommand(); } catch {}
        
        switch (subcommand)
        {
            case tbOps.data.name:
                await tbOps.interact(interaction);
                break;
            case tbCombat.data.name:
                await tbCombat.interact(interaction);
                break;
            case tbReady.data.name:
                await tbReady.interact(interaction);
                break;
            case tbTeams.data.name:
                await tbTeams.interact(interaction);
                break;
        }
    },
    handleButton: async function(interaction)
    {  
        if (interaction.customId.startsWith("tb:ops:"))
            await tbOps.handleButton(interaction)
    },
    onSelect: async function(interaction)
    {
        if (interaction.customId.startsWith("tb:ops:"))
            await tbOps.onSelect(interaction);
        else if (interaction.customId.startsWith("tb:combat:"))
            await tbCombat.onSelect(interaction);
        else if (interaction.customId.startsWith("tb:specials:"))
            await tbSpecials.onSelect(interaction);
        else if (interaction.customId.startsWith("tb:teams:"))
            await tbTeams.onSelect(interaction);
    },
    isEphemeral: function(interaction)
    {
        if (interaction.options.getSubcommand() == tbTeams.data.name) return tbTeams.isEphemeral(interaction);
        return false;
    },
    autocomplete: async function(interaction)
    {
        let group, subcommand;
        try { group = interaction.options.getSubcommandGroup(); } catch {}

        if (group)
        {
            switch (group) {
                case tbSpecials.data.name:
                    await tbSpecials.autocomplete(interaction);
                    break;
            }
            return;
        }

        try { subcommand = interaction.options.getSubcommand(); } catch {}
        if (subcommand)
        {
            switch (subcommand)
            {
                case tbOps.data.name:
                    await tbOps.autocomplete(interaction);
                    break;
                case tbCombat.data.name:
                    await tbCombat.autocomplete(interaction);
                    break;
                case tbReady.data.name:
                    await tbReady.autocomplete(interaction);
                    break;
                case tbTeams.data.name:
                    await tbTeams.autocomplete(interaction);
                    break;
            }
            return;
        }
    }
}