const discordUtils = require("../utils/discord");
const playerUtils = require("../utils/player");
const database = require("../database");
const drawingUtils = require("../utils/drawing");
const { SlashCommandBuilder } = require("@discordjs/builders");
const { logger, formatError } = require("../utils/log.js");
const swapi = require("../utils/swapi");
const comlink = require("../utils/comlink");
const constants = require("../utils/constants");
const { loadImage } = require('canvas');
const { AttachmentBuilder, MessageFlags } = require("discord.js");
const unitsUtils = require("../utils/units");
const readyUtils = require("../utils/ready");
const criteriaCommon = require("../commands/guild/criteria/common.js");
const UNIT_DATA = require("../data/unit_data.json");

const LOCKED_TEXT = `🔒`;
const RED_X = '❌';
const GREEN_CHECK = '✅';

async function readyResultAsPercentText(player, checkData)
{
  if (!checkData) return "";
  const readyResult = await calculateReady(player, checkData);

  const percentreadyNum = parseFloat(readyResult.overallPercentReady.toFixed(1));
  return !isNaN(percentreadyNum) ? `${percentreadyNum}%` : '';
}
async function readyResultAsEmoji(player, checkData)
{
  if (!checkData) return null;
  const readyResult = await calculateReady(player, checkData);

  if (readyResult.ready) return GREEN_CHECK;
  return RED_X;
}
async function calculateReady(player, checkData)
{
  try
  {
    let unitDefinitions = await readyUtils.getUnitDefinitionForRequirements(checkData);
    let readyResult = await readyUtils.getReadiness(player, checkData, {}, unitDefinitions);

    return readyResult;
  } catch (error)
  {
    logger.error(`Error calculating percent ready for ${checkData.value}:`, error);
    return '';
  }
}

const CANVAS_DATA = {

  unitPortraitHeight: 125,
  unitPortraitWidth: 125,
  unitPortraitImageHeight: 75,
  unitPortraitImageWidth: 75,
  portraitRowSpacerHeight: 10,
  portraitPadding: 8,
  maxPortraitsPerRow: 10,
  smallCircleWidth: 30,
  smallCircleHeight: 30,
  smallCircleOffsetX: 47.5,
  smallCircleOffsetY: 60,
  footerHeight: 40,
  characterPortraitColumnWidth: 150,
  characterPortraitRowHeight: 180
};

module.exports = {
  data: new SlashCommandBuilder()
    .setName("player")
    .setDescription("Pull data for a player")
    .addStringOption(o =>
      o.setName("allycode")
        .setDescription("The player's Ally Code")
        .setRequired(true)),
  interact: async function (interaction)
  {
    let allycode = interaction.options.getString("allycode");
    allycode = playerUtils.cleanAndVerifyStringAsAllyCode(allycode);

    let { botUser, guildId, error } = await playerUtils.authorize(interaction);
    const guild = botUser.guilds.find(g => g.guildId === guildId);
    const comlinkGuildId = guild.comlinkGuildId;
    // const guildAutoEvalPlayers = (await database.db.oneOrNone("SELECT auto_eval_players FROM guild WHERE guild_id = $1", [guildId]))?.auto_eval_players;

    const player = await swapi.getPlayer(allycode);

    const playerName = swapi.getPlayerName(player);
    const galacticPower = swapi.getGP(player);

    let criteria = await criteriaCommon.getGuildCriteria(comlinkGuildId);

    const modq = playerUtils.calculateModScore(player, constants.MOD_SCORE_TYPES.WOOKIEEBOT).toFixed(2).toString() ?? "No Data"

    const revaText = getUnitText(player, "THIRDSISTER", false);
    const watText = getUnitText(player, "WATTAMBOR", true);


    const revaReady = await readyResultAsEmoji(player, readyUtils.TB_CHECK_FUNCTION_MAPPING.Reva);
    const watReady = await readyResultAsEmoji(player, readyUtils.TB_CHECK_FUNCTION_MAPPING.WAT);
    const zeffoReady = await readyResultAsEmoji(player, readyUtils.TB_CHECK_FUNCTION_MAPPING.LS2);
    const mandaloreReady = await readyResultAsEmoji(player, readyUtils.TB_CHECK_FUNCTION_MAPPING.TATOOINEKRAYT);
    const playerReadyData = {
      revaText: revaText,
      watText: watText,
      revaReady: revaReady,
      watReady: watReady,
      zeffoReady: zeffoReady,
      mandaloreReady: mandaloreReady
    }

    const playerTwOmicrons = swapi.getPlayerOmicrons(player, swapi.OMICRON_MODE.TERRITORY_WAR_OMICRON);

    const playerDatacrons = await swapi.getPlayerDatacrons(player);
    const datacronCounts = {};
    let characterDatacronCount = 0;

    playerDatacrons.forEach(datacron =>
    {
      if (!datacron.meta.character) return;
      datacronCounts[datacron.meta.character] = (datacronCounts[datacron.meta.character] ?? 0) + 1;
      characterDatacronCount++;
    });

    const files = [];

    const width = 1500;
    let height = 1700 + (CANVAS_DATA.unitPortraitHeight * Math.ceil(playerTwOmicrons.length / 9))
    if (characterDatacronCount > 0) height += (CANVAS_DATA.unitPortraitHeight * Math.ceil(characterDatacronCount / 9) + (CANVAS_DATA.unitPortraitHeight * 2))
    const { canvas, context } = drawingUtils.createBlankCanvas(width, height, "#fff");

    drawBackgroundImage(context);
    await drawPlayerSummaryInfo(context, player);
    const glCount = await drawGalacticLegendsBlock(context, player);
    await drawCapitalShipsBlock(context, player);
    const missedConquestUnitCount = await drawConquestUnitsBlock(context, player);
    await drawTBReadinessBlock(context, player, criteria, playerReadyData);
    let currentY = await drawTWOmicronsBlock(context, player, playerTwOmicrons);
    currentY = await drawDatacronsBlock(context, player, currentY, datacronCounts);


    const playerProfile = new AttachmentBuilder(canvas.toBuffer(), { name: `${playerName}.png` });
    files.push(playerProfile);

    const embeds = [];
    const criteriaEmbed = await generateCriteriaEmbed(player);
    embeds.push(criteriaEmbed);

    await interaction.editReply({ embeds: embeds, flags: MessageFlags.Ephemeral, files: files });
  }
};

function getUnitText(player, unitDefId, includeRarity, lockedText)
{
  let unit = swapi.findUnit(player, unitDefId);

  if (!unit) return lockedText;

  if (swapi.getUnitCombatType(unit) === constants.COMBAT_TYPES.FLEET)
  {
    return `${swapi.getUnitRarity(unit)}⭐`;
  }

  let unitText;

  if (swapi.getUnitGearLevel(unit) == 13) unitText = `R${swapi.getUnitRelicLevel(unit) - 2}`;
  else unitText = `G${swapi.getUnitGearLevel(unit)}`;

  if (includeRarity) unitText += `/${swapi.getUnitRarity(unit)}⭐`;

  return unitText;
}

function drawBackgroundImage(context)
{
  context.textAlign = "center";
  context.font = '40px Arial';
  context.drawImage(drawingUtils.getImage(drawingUtils.IMAGES.BG_REC), 0, 0, context.canvas.width, context.canvas.height);
}

async function drawPlayerSummaryInfo(context, player)
{
  const modq = playerUtils.calculateModScore(player, constants.MOD_SCORE_TYPES.WOOKIEEBOT).toFixed(2).toString() ?? "No Data"
  const squadArenaRank = swapi.getSquadArenaRank(player);
  const fleetArenaRank = swapi.getFleetArenaRank(player);
  const raid = await database.db.oneOrNone("SELECT end_time_seconds, score, raid_id FROM player_raid_score WHERE player_id = $1 ORDER BY end_time_seconds DESC LIMIT 1", [player.playerId]);


  context.fillStyle = "#fff";
  x = 10; y = 10;
  context.fillText(`${swapi.getPlayerName(player)} (${swapi.getPlayerAllycodeWithDashes(player)})`, context.canvas.width / 2, y + 25)
  context.font = '25px Arial';
  context.fillText(`Stats:`, 75, y + 100)
  context.fillText(`GP: ${swapi.getGP(player).toLocaleString('en')}`, context.canvas.width / 6, y + 100)
  context.fillText(`Arena: ${squadArenaRank} / Fleet: ${fleetArenaRank}`, context.canvas.width / 3, y + 100)
  context.fillText(`ModQ: ${modq}`, context.canvas.width / 2, y + 100)
  let raidstring = ``
  if (raid)
  { raidstring = `${raid.raid_id}: ${raid.score.toLocaleString()}` }
  else
  { raidstring = 'No Raid Data' }
  context.fillText(`Last Raid: ${raidstring}`, context.canvas.width / 2 + 500, y + 100);
}

async function drawGalacticLegendsBlock(context, player)
{

  const glCount = swapi.getPlayerGalacticLegends(player).length;
  const unitsList = await swapi.getUnitsList();
  const allGalacticLegends = unitsList.filter(u => u.is_galactic_legend);

  context.font = '25px Arial';
  context.textAlign = "center";
  context.fillText(`GL Count: ${glCount}`, context.canvas.width / 2 + 200, 110);
  context.fillText(`Galactic\nLegends:`, 75, CANVAS_DATA.smallCircleOffsetY + 150);

  context.font = '20px Arial';
  let glList = {};
  let x = 10;
  for (const unit of allGalacticLegends)
  {
    const unitDefId = swapi.getUnitDefId(unit);

    let percentreadynum = await readyResultAsPercentText(player, readyUtils.CHECK_FUNCTION_MAPPING_DEF_ID_LOOKUP[unitDefId]);

    let unitText = getUnitText(player, unitDefId, false, LOCKED_TEXT);
    glList[unitDefId] = unitText;

    const unitIconImg = await drawingUtils.getLoadedPortraitImage(unitDefId);
    const offsetX = CANVAS_DATA.smallCircleOffsetX + x + 100;
    const offsetY = CANVAS_DATA.smallCircleOffsetY + 100;

    context.drawImage(unitIconImg, offsetX, offsetY, CANVAS_DATA.unitPortraitHeight, CANVAS_DATA.unitPortraitWidth);

    if (glList[unitDefId] === LOCKED_TEXT)
    {
      const boxHeight = 20;
      const boxWidth = CANVAS_DATA.unitPortraitWidth;
      const boxX = offsetX;
      const boxY = offsetY + CANVAS_DATA.unitPortraitHeight - boxHeight;
      const text = glList[unitDefId];
      const textWidth = context.measureText(text).width;
      const textX = boxX + (boxWidth - textWidth) / 2;
      const textY = boxY + (boxHeight + 15) / 2;
      context.textAlign = "left";
      context.fillStyle = 'white';
      context.fillText(`${percentreadynum}`, (boxX + (boxWidth - context.measureText(`${percentreadynum}`).width) / 2), textY + 20);

      context.globalAlpha = 0.25;
      context.fillStyle = 'red';
      context.fillRect(offsetX, offsetY, CANVAS_DATA.unitPortraitHeight, CANVAS_DATA.unitPortraitWidth);
      context.globalAlpha = 1.0;

    } else
    {
      const boxHeight = 20;
      const boxWidth = CANVAS_DATA.unitPortraitWidth;
      const boxX = offsetX;
      const boxY = offsetY + CANVAS_DATA.unitPortraitHeight - boxHeight;

      context.fillStyle = 'black';
      context.fillRect(boxX, boxY, boxWidth, boxHeight);
      context.strokeStyle = 'green';
      context.lineWidth = 2;
      context.strokeRect(boxX, boxY, boxWidth, boxHeight);
      context.fillStyle = 'white';
      const text = glList[unitDefId];
      const textWidth = context.measureText(text).width;
      const textX = boxX + (boxWidth - textWidth) / 2;
      const textY = boxY + (boxHeight + 15) / 2;
      context.textAlign = "left";
      context.fillText(text, textX, textY);
    }

    context.fillStyle = 'white';
    x += 15 + CANVAS_DATA.unitPortraitWidth;
  }

  return glCount;
}

async function drawCapitalShipsBlock(context, player)
{
  let x = 15;
  context.font = '25px Arial';
  context.textAlign = 'left';
  context.fillText(`Capital\nShips:`, 75, CANVAS_DATA.smallCircleOffsetY + 350);

  const unitsList = await swapi.getUnitsList();
  const allCapitalShips = unitsList.filter(u => u.is_capital_ship);

  context.textAlign = "center";

  context.font = '20px Arial';
  let csList = {};
  for (const unit of allCapitalShips)
  {
    const unitDefId = swapi.getUnitDefId(unit);
    const { errorCode, unit } = swapi.findUnit(player, unitDefId);

    let percentReadyText = await readyResultAsPercentText(player, readyUtils.CHECK_FUNCTION_MAPPING_DEF_ID_LOOKUP[unitDefId]);

    csList[unitDefId] = (unit ? `${swapi.getUnitRarity(unit)}⭐` : LOCKED_TEXT);

    const unitIconImg = await drawingUtils.getLoadedPortraitImage(unitDefId);
    const offsetX = CANVAS_DATA.smallCircleOffsetX + x + 100;
    const offsetY = CANVAS_DATA.smallCircleOffsetY + y + 300;

    context.drawImage(unitIconImg, offsetX, offsetY, CANVAS_DATA.unitPortraitHeight, CANVAS_DATA.unitPortraitWidth);

    if (csList[unitDefId] === LOCKED_TEXT)
    {
      const boxHeight = 20;
      const boxWidth = CANVAS_DATA.unitPortraitWidth;
      const boxX = offsetX;
      const boxY = offsetY + CANVAS_DATA.unitPortraitHeight - boxHeight;
      const text = csList[unitDefId];
      const textWidth = context.measureText(text).width;
      const textX = boxX + (boxWidth - textWidth) / 2;
      const textY = boxY + (boxHeight + 15) / 2;
      context.globalAlpha = 0.25;
      context.fillStyle = 'red';
      context.fillRect(offsetX, offsetY, CANVAS_DATA.unitPortraitHeight, CANVAS_DATA.unitPortraitWidth);
      context.globalAlpha = 1.0;
      context.textAlign = "left";
      context.fillStyle = 'white';
      context.fillText(percentReadyText, (boxX + (boxWidth - context.measureText(percentReadyText).width) / 2), textY + 20);
    } else
    {

      const boxHeight = 20;
      const boxWidth = CANVAS_DATA.unitPortraitWidth;
      const boxX = offsetX;
      const boxY = offsetY + CANVAS_DATA.unitPortraitHeight - boxHeight;

      context.fillStyle = 'black';
      context.fillRect(boxX, boxY, boxWidth, boxHeight);
      context.strokeStyle = 'green';
      context.lineWidth = 2;
      context.strokeRect(boxX, boxY, boxWidth, boxHeight);
      context.fillStyle = 'white';
      const text = csList[unitDefId];
      const textWidth = context.measureText(text).width;
      const textX = boxX + (boxWidth - textWidth) / 2;
      const textY = boxY + (boxHeight + 15) / 2;
      context.textAlign = "left";
      context.fillText(text, textX, textY);
    }


    context.fillStyle = 'white';
    x += 15 + CANVAS_DATA.unitPortraitWidth;


    if (x + CANVAS_DATA.unitPortraitWidth > context.canvas.width - CANVAS_DATA.unitPortraitWidth)
    {
      x = 15;
      y += CANVAS_DATA.unitPortraitHeight + 20;
    }
  }
}

async function drawConquestUnitsBlock(context, player)
{
  const allUnits = await swapi.getUnitsList();

  const conquestUnits = UNIT_DATA.ConquestUnits;
  conquestUnits.sort((a, b) => a.order - b.order);


  let missedConquestUnitCount = 0

  x = 15;
  y = CANVAS_DATA.smallCircleOffsetY + 1100;

  for (const unit of conquestUnits)
  {
    context.font = '20px Arial';
    const unitText = getUnitText(player, unit.defId, true, RED_X);
    if (unitText === RED_X) missedConquestUnitCount++;

    const img = (await unitsUtils.getPortraitForUnitByDefId(unit.defId)) ?? drawingUtils.getImage(drawingUtils.IMAGES.NO_PORTRAIT);

    const unitIconImg = await loadImage(img);
    const offsetX = CANVAS_DATA.smallCircleOffsetX + x + 100;
    const offsetY = y;
    context.drawImage(unitIconImg, offsetX, offsetY, CANVAS_DATA.unitPortraitHeight, CANVAS_DATA.unitPortraitWidth);

    if (unitText === RED_X)
    {
      context.globalAlpha = 0.25;
      context.fillStyle = 'red';
      context.fillRect(offsetX, offsetY, CANVAS_DATA.unitPortraitHeight, CANVAS_DATA.unitPortraitWidth);
      context.globalAlpha = 1.0;

    } else if (unitText !== RED_X)
    {
      const boxHeight = 20;
      const boxWidth = CANVAS_DATA.unitPortraitWidth;
      const boxX = offsetX;
      const boxY = offsetY + CANVAS_DATA.unitPortraitHeight - boxHeight;

      context.fillStyle = 'black';
      context.fillRect(boxX, boxY, boxWidth, boxHeight);

      context.strokeStyle = 'green';
      context.lineWidth = 2;
      context.strokeRect(boxX, boxY, boxWidth, boxHeight);
      context.fillStyle = 'white';
      const textWidth = context.measureText(unitText).width;
      const textX = boxX + (boxWidth - textWidth) / 2;
      const textY = boxY + (boxHeight + 15) / 2;
      context.textAlign = "left";
      context.fillText(unitText, textX, textY);
    }
    context.fillStyle = 'white';
    x += 15 + CANVAS_DATA.unitPortraitWidth;
    if (x + CANVAS_DATA.unitPortraitWidth > context.canvas.width - CANVAS_DATA.unitPortraitWidth)
    {
      x = 15;
      y += CANVAS_DATA.unitPortraitHeight + 20;
    }
  }

  context.font = '25px Arial';
  context.textAlign = "center";
  context.fillText(`Conquest:`, 75, CANVAS_DATA.smallCircleOffsetY + 1150);

  return missedConquestUnitCount;
}

async function drawTBReadinessBlock(context, player, criteria, playerReadyData)
{
  x = 15;
  context.font = '25px Arial';

  context.textAlign = "center";
  context.fillText(`TB Unlocks:`, 75, CANVAS_DATA.smallCircleOffsetY + 950)
  context.fillText(`TB Ready:`, 75, CANVAS_DATA.smallCircleOffsetY + 750)
  context.font = '20px Arial';
  context.textAlign = "left";

  // reva block
  const unitIconImgTHIRDSISTER = await drawingUtils.getLoadedPortraitImage("THIRDSISTER");
  let offsetXReva = CANVAS_DATA.smallCircleOffsetX + x + 95;
  let offsetYReva = CANVAS_DATA.smallCircleOffsetY + 900;

  context.drawImage(unitIconImgTHIRDSISTER, offsetXReva, offsetYReva, CANVAS_DATA.unitPortraitHeight, CANVAS_DATA.unitPortraitWidth);

  if (!playerReadyData.revaText)
  {

    context.globalAlpha = 0.25;
    context.fillStyle = 'red';
    context.fillRect(offsetXReva, offsetYReva, CANVAS_DATA.unitPortraitHeight, CANVAS_DATA.unitPortraitWidth);
    context.globalAlpha = 1.0;

  }
  else
  {
    const boxHeight = 20;
    const boxWidth = CANVAS_DATA.unitPortraitWidth;
    const boxX = offsetXReva;
    const boxY = offsetYReva + CANVAS_DATA.unitPortraitHeight - boxHeight;

    context.fillStyle = 'black';
    context.fillRect(boxX, boxY, boxWidth, boxHeight);

    context.strokeStyle = 'green';
    context.lineWidth = 2;
    context.strokeRect(boxX, boxY, boxWidth, boxHeight);

    context.fillStyle = 'white';
    const text = playerReadyData.revaText;
    const textWidth = context.measureText(text).width;
    const textX = boxX + (boxWidth - textWidth) / 2;
    const textY = boxY + (boxHeight + context.measureText("M").actualBoundingBoxAscent) / 2;

    context.fillText(text, textX, textY);

  }

  x = 15;
  context.textAlign = "left";

  // wat block
  const unitIconImgWATTAMBOR = await drawingUtils.getLoadedPortraitImage("WATTAMBOR");
  let offsetXWat = CANVAS_DATA.smallCircleOffsetX + (x * 2) + 95 + CANVAS_DATA.unitPortraitWidth;
  let offsetYWat = CANVAS_DATA.smallCircleOffsetY + 900;

  context.drawImage(unitIconImgWATTAMBOR, offsetXWat, offsetYWat, CANVAS_DATA.unitPortraitHeight, CANVAS_DATA.unitPortraitWidth);

  if (!playerReadyData.watText)
  {
    context.globalAlpha = 0.25;
    context.fillStyle = 'red';
    context.fillRect(offsetXWat, offsetYWat, CANVAS_DATA.unitPortraitHeight, CANVAS_DATA.unitPortraitWidth);
    context.globalAlpha = 1.0;
  }
  else
  {
    const boxHeight = 20;
    const boxWidth = CANVAS_DATA.unitPortraitWidth;
    const boxX = offsetXWat;
    const boxY = offsetYWat + CANVAS_DATA.unitPortraitHeight - boxHeight;

    context.fillStyle = 'black';
    context.fillRect(boxX, boxY, boxWidth, boxHeight);
    context.strokeStyle = 'green';
    context.lineWidth = 2;
    context.strokeRect(boxX, boxY, boxWidth, boxHeight);
    context.fillStyle = 'white';
    const text = playerReadyData.watText;
    const textWidth = context.measureText(text).width;
    let textX = boxX + (boxWidth - textWidth) / 2;
    const textY = boxY + (boxHeight + context.measureText("M").actualBoundingBoxAscent) / 2;

    context.fillText(text, textX, textY);
  }

  x = 15;

  // reva ready block
  offsetXReva = CANVAS_DATA.smallCircleOffsetX + x + 95;
  offsetYReva = CANVAS_DATA.smallCircleOffsetY + 700;
  context.drawImage(drawingUtils.getImage(drawingUtils.IMAGES.REVA), offsetXReva, offsetYReva, CANVAS_DATA.unitPortraitHeight - 15, CANVAS_DATA.unitPortraitWidth - 15);

  percentreadynum = await readyResultAsPercentText(player, readyUtils.TB_CHECK_FUNCTION_MAPPING.Reva);

  if (playerReadyData.revaReady === '')
  {
    context.globalAlpha = 0.25;
    context.fillStyle = 'red';
    context.fillRect(offsetXReva, offsetYReva, CANVAS_DATA.unitPortraitHeight - 15, CANVAS_DATA.unitPortraitWidth - 15);
    context.globalAlpha = 1.0;
    context.textAlign = "left";
    context.fillStyle = 'white';
    context.fillText(`${percentreadynum}`, (offsetXReva + (CANVAS_DATA.unitPortraitHeight - 15 - context.measureText(`${percentreadynum}`).width) / 2), offsetYReva + CANVAS_DATA.unitPortraitHeight + 20);
  }
  context.textAlign = "center";
  let textX = offsetXReva + (CANVAS_DATA.unitPortraitWidth - 15) / 2;
  context.fillText(`${playerReadyData.revaReady}`, textX, offsetYReva + CANVAS_DATA.unitPortraitHeight - 5);
  context.textAlign = "left";
  context.fillText(`${percentreadynum}`, (offsetXReva + (CANVAS_DATA.unitPortraitHeight - 15 - context.measureText(`${percentreadynum}`).width) / 2), offsetYReva + CANVAS_DATA.unitPortraitHeight + 20);

  // wat ready block
  context.textAlign = "left";
  offsetXWat = CANVAS_DATA.smallCircleOffsetX + (x * 2) + 95 + CANVAS_DATA.unitPortraitWidth;
  offsetYWat = CANVAS_DATA.smallCircleOffsetY + 700;
  context.drawImage(drawingUtils.getImage(drawingUtils.IMAGES.WAT), offsetXWat, offsetYWat, CANVAS_DATA.unitPortraitHeight - 15, CANVAS_DATA.unitPortraitWidth - 15);

  let percentreadynum = await readyResultAsPercentText(player, readyUtils.TB_CHECK_FUNCTION_MAPPING.WAT);

  if (playerReadyData.watReady === '')
  {

    context.globalAlpha = 0.25;
    context.fillStyle = 'red';
    context.fillRect(offsetXWat, offsetYWat, CANVAS_DATA.unitPortraitHeight - 15, CANVAS_DATA.unitPortraitWidth - 15);
    context.globalAlpha = 1.0;
    context.textAlign = "left";
    context.fillStyle = 'white';
    context.fillText(`${percentreadynum}`, (offsetXWat + (CANVAS_DATA.unitPortraitHeight - 15 - context.measureText(`${percentreadynum}`).width) / 2), offsetYWat + CANVAS_DATA.unitPortraitHeight + 20);
  }
  context.fillStyle = 'white';
  context.textAlign = "center";
  textX = offsetXWat + (CANVAS_DATA.unitPortraitWidth - 15) / 2;
  context.fillText(`${playerReadyData.watReady}`, textX, offsetYWat + CANVAS_DATA.unitPortraitHeight - 5)
  context.textAlign = "left";
  context.fillStyle = 'white';
  context.fillText(`${percentreadynum}`, (offsetXWat + (CANVAS_DATA.unitPortraitHeight - 15 - context.measureText(`${percentreadynum}`).width) / 2), offsetYWat + CANVAS_DATA.unitPortraitHeight + 20);

  // zeffo ready block

  const cerecalfriendly = {
    "CEREJUNDA": "CERE JUNDA",
    "JEDIKNIGHTCAL": "JEDI KNIGHT CAL",
    "CALKESTIS": "CAL KESTIS"
  };
  percentreadynum = await readyResultAsPercentText(player, readyUtils.TB_CHECK_FUNCTION_MAPPING.LS2);
  let cerejundaReady = false;
  let otherReadyCount = 0;
  unitDetails = {};

  for (const unitId in cerecalfriendly)
  {
    const unitName = cerecalfriendly[unitId];
    const { errorCode, unit } = swapi.findUnit(player, unitId);

    if (unit)
    {
      const gearLevel = unit.gear_level;
      const relicLevel = unit.relic_level;
      const adjustedRelicLevel = Math.max(relicLevel - 2, 0);

      if (unitId === 'CEREJUNDA')
      {
        cerejundaReady = relicLevel >= 9;
      } else if (relicLevel >= 9)
      {
        otherReadyCount++;
      }
      if (gearLevel == 13) unitDetails[unitId] = `R${adjustedRelicLevel}`;
      else unitDetails[unitId] = `G${gearLevel}`;
    } else
    {
      unitDetails[unitId] = '';
    }
  }

  const zeffoReady = cerejundaReady && otherReadyCount > 0 ? GREEN_CHECK : RED_X;

  const drawUnit = async (unitId, unitName, unitStatus, offsetX, offsetY) =>
  {
    const unitIconImg = await drawingUtils.getLoadedPortraitImage(unitId);

    context.drawImage(unitIconImg, offsetX, offsetY, CANVAS_DATA.unitPortraitHeight, CANVAS_DATA.unitPortraitWidth);

    if (unitStatus === '')
    {
      context.globalAlpha = 0.25;
      context.fillStyle = 'red';
      context.fillRect(offsetX, offsetY, CANVAS_DATA.unitPortraitHeight, CANVAS_DATA.unitPortraitWidth);
      context.globalAlpha = 1.0;
      context.fillStyle = 'white';
    } else
    {
      const boxHeight = 20;
      const boxWidth = CANVAS_DATA.unitPortraitWidth;
      const boxX = offsetX;
      const boxY = offsetY + CANVAS_DATA.unitPortraitHeight - boxHeight;

      context.fillStyle = 'black';
      context.fillRect(boxX, boxY, boxWidth, boxHeight);

      context.strokeStyle = 'green';
      context.lineWidth = 2;
      context.strokeRect(boxX, boxY, boxWidth, boxHeight);

      context.fillStyle = 'white';
      const textWidth = context.measureText(unitStatus).width;
      const textX = boxX + (boxWidth - textWidth) / 2;
      const textY = boxY + (boxHeight + 15) / 2;
      context.textAlign = "left";
      context.fillText(unitStatus, textX, textY);
    }
  };


  let unitIds = Object.keys(cerecalfriendly);
  let startX = CANVAS_DATA.smallCircleOffsetX + (x * 4) + 95 + (CANVAS_DATA.unitPortraitWidth * 3);
  let currentX = startX;
  let currentY = CANVAS_DATA.smallCircleOffsetY + 700;

  for (let i = 0; i < unitIds.length; i++)
  {
    const unitId = unitIds[i];
    const unitName = cerecalfriendly[unitId];
    const unitStatus = unitDetails[unitId];

    await drawUnit(unitId, unitName, unitStatus, currentX, currentY);

    currentX += CANVAS_DATA.unitPortraitWidth + 15;
    if (currentX + CANVAS_DATA.unitPortraitWidth > context.canvas.width)
    {
      currentX = startX;
      currentY += CANVAS_DATA.unitPortraitHeight + 20;
    }
  }

  context.fillText(`Zeffo Ready: ${zeffoReady}`, startX + 5 + CANVAS_DATA.unitPortraitWidth, currentY + CANVAS_DATA.unitPortraitHeight + 20);

  context.fillStyle = 'white';
  context.textAlign = "center";
  context.fillText(`${percentreadynum}`, (startX + 5 + CANVAS_DATA.unitPortraitWidth) + ((context.measureText(`${zeffoReady}`).width + ((context.measureText(`${percentreadynum}`).width)))), currentY + CANVAS_DATA.unitPortraitHeight + 40);
  context.textAlign = "left";

  // mandalore ready block
  const bobamFriendly = {
    "THEMANDALORIANBESKARARMOR": "THE MANDALORIAN (BESKAR ARMOR)",
    "MANDALORBOKATAN": "MANDALORE BO-KATAN"
  };
  percentreadynum = await readyResultAsPercentText(player, readyUtils.TB_CHECK_FUNCTION_MAPPING.TATOOINEKRAYT);
  let beskarMandoReady = false;
  let boKatanReady = false;
  let unitDetails = {};

  for (const unitId in bobamFriendly)
  {
    const unitName = bobamFriendly[unitId];
    const { errorCode, unit } = swapi.findUnit(player, unitId);

    if (unit)
    {
      const gearLevel = unit.gear_level;
      const relicLevel = unit.relic_level;
      const adjustedRelicLevel = Math.max(relicLevel - 2, 0);

      if (unitId === 'THEMANDALORIANBESKARARMOR')
      {
        beskarMandoReady = relicLevel >= 9;
      } else if (unitId === 'MANDALORBOKATAN')
      {
        boKatanReady = relicLevel >= 9;
      }
      if (gearLevel == 13) unitDetails[unitId] = `R${adjustedRelicLevel}`;
      else unitDetails[unitId] = `G${gearLevel}`;
    } else
    {
      unitDetails[unitId] = '';
    }
  }

  let mandaloreReady = beskarMandoReady && boKatanReady ? GREEN_CHECK : RED_X;


  unitIds = Object.keys(bobamFriendly);
  startX = CANVAS_DATA.smallCircleOffsetX + (x * 9) + 95 + (CANVAS_DATA.unitPortraitWidth * 7);
  currentX = startX;
  currentY = CANVAS_DATA.smallCircleOffsetY + 700;

  for (let i = 0; i < unitIds.length; i++)
  {
    const unitId = unitIds[i];
    const unitName = bobamFriendly[unitId];
    const unitStatus = unitDetails[unitId];

    await drawUnit(unitId, unitName, unitStatus, currentX, currentY);

    currentX += CANVAS_DATA.unitPortraitWidth + 15;
    if (currentX + CANVAS_DATA.unitPortraitWidth > context.canvas.width)
    {
      currentX = startX;
      currentY += CANVAS_DATA.unitPortraitHeight + 20;
    }
  }
  context.fillText(`Mandalore Ready: ${mandaloreReady}`, startX + (CANVAS_DATA.unitPortraitWidth / 2), currentY);
  context.fillStyle = 'white';
  context.fillText(`${percentreadynum}`, (startX + (CANVAS_DATA.unitPortraitWidth / 2)) + ((context.measureText(`${percentreadynum}`).width + context.measureText(`${mandaloreReady}`).width) / 2), currentY + 20);
  context.textAlign = "left";
}

async function drawTWOmicronsBlock(context, player, playerTwOmicrons)
{
  context.font = '25px Arial';
  context.textAlign = "center";
  context.fillText(`TW Omis:`, 75, CANVAS_DATA.smallCircleOffsetY + 1500);


  if (playerTwOmicrons.length === 0)
  {

    context.textAlign = "center";
    context.font = '20px Arial';
    context.fillText("None", CANVAS_DATA.smallCircleOffsetX + x + 100, CANVAS_DATA.smallCircleOffsetY + 1550);
    return;
  }

  const abilitiesList = await swapi.getAbilitiesList();
  const unitIds = new Set();
  for (const skillBaseId of playerTwOmicrons)
  {
    let ability = abilitiesList.find(a => a.base_id === skillBaseId);
    unitIds.add({ unit_def_id: ability.character_base_id, skillname: ability.name });
  }

  let startX = CANVAS_DATA.smallCircleOffsetX + x + 95;
  let currentX = startX;
  let currentY = CANVAS_DATA.smallCircleOffsetY + 1450;
  for (const unit of unitIds)
  {
    if (currentX + CANVAS_DATA.unitPortraitWidth > context.canvas.width)
    {
      currentX = startX;
      currentY += CANVAS_DATA.unitPortraitHeight + 40;
    }

    const unitId = unit.unit_def_id;
    const unitStatus = unit.skillname;
    const unitIconImg = await drawingUtils.getLoadedPortraitImage(unitId);
    context.drawImage(unitIconImg, currentX, currentY, CANVAS_DATA.unitPortraitHeight, CANVAS_DATA.unitPortraitWidth);

    const ellipsis = '..';
    context.font = '20px Arial';
    let textWidth = context.measureText(unitStatus).width;
    const ellipsisWidth = context.measureText(ellipsis).width;

    let truncatedText = unitStatus;
    let truncated = false;
    while (textWidth > CANVAS_DATA.unitPortraitWidth - ellipsisWidth && truncatedText.length > 2)
    {
      truncatedText = truncatedText.slice(0, -2);
      textWidth = context.measureText(truncatedText).width;
      truncated = true;
    }

    if (textWidth > CANVAS_DATA.unitPortraitWidth - ellipsisWidth)
    {
      truncatedText = truncatedText.slice(0, -2);
      truncated = true;
    }
    if (truncated) truncatedText = truncatedText + ellipsis;


    context.textAlign = "center";
    const textX = currentX + (CANVAS_DATA.unitPortraitWidth / 2);
    const textY = currentY + CANVAS_DATA.unitPortraitHeight + 20;
    context.fillText(truncatedText, textX, textY);

    currentX += CANVAS_DATA.unitPortraitWidth + 15;
  }

  currentY += CANVAS_DATA.unitPortraitHeight + 40;

  return currentY;
}

async function drawDatacronsBlock(context, player, currentY, datacronCounts)
{
  let startX = CANVAS_DATA.smallCircleOffsetX + x + 95;
  let currentX = startX;

  if (Object.keys(datacronCounts).length === 0)
  {

    context.textAlign = "center";
    context.font = '25px Arial';
    context.fillText("None", CANVAS_DATA.smallCircleOffsetX + x + 100, CANVAS_DATA.smallCircleOffsetY + 1850);
    context.fillText(`Lvl9 DCs:`, 75, CANVAS_DATA.smallCircleOffsetY + 1550);
    return;
  }
  context.fillText(`Level 9 DCs:`, 75, currentY + 50);
  for (const [unitDefId, count] of Object.entries(datacronCounts))
  {
    const unitIconImg = await drawingUtils.getLoadedPortraitImage(unitDefId);

    context.drawImage(unitIconImg, currentX, currentY, CANVAS_DATA.unitPortraitHeight, CANVAS_DATA.unitPortraitWidth);

    context.textAlign = "center";
    context.font = '20px Arial';
    const text = `${count}`;
    const textX = currentX + (CANVAS_DATA.unitPortraitWidth / 2);
    const textY = currentY + CANVAS_DATA.unitPortraitHeight + 20;

    context.fillText(text, textX, textY);

    currentX += CANVAS_DATA.unitPortraitWidth + 15;

    if (currentX + CANVAS_DATA.unitPortraitWidth > context.canvas.width)
    {
      currentX = startX;
      currentY += CANVAS_DATA.unitPortraitHeight + 40;
    }
  }

  return currentY;
}


const criteriaFriendly = [
  { key: 'gp_min', value: "Min GP" },
  { key: 'modq_min', value: "Min Mod Quality" },
  { key: 'gl_count', value: "Min GL Count" },
  { key: 'missed_conquest_units_count', value: "Max Missed Conquest Units" },
  { key: 'has_reva', value: 'Reva 🔓' },
  { key: 'has_wat', value: 'Wat 🔓' },
  { key: 'wat_ready', value: "Wat Ready" },
  { key: 'reva_ready', value: "Reva Ready" },
  { key: 'zeffo_ready', value: "Zeffo Ready" },
  { key: 'mandalore_ready', value: "Mandalore Ready" },
  { key: 'tw_omi_count', value: "Min. TW Omi Count" },
  { key: 'dc9_count', value: "Min. Lvl9 DC Count" }
];

async function generateCriteriaEmbed(player)
{

  const criteriaKeys = [
    { key: 'gp_min', value: Math.floor((galacticPower / 1000000) * 10) / 10 },
    { key: 'modq_min', value: modq },
    { key: 'gl_count', value: glCount },
    { key: 'missed_conquest_units_count', value: missedConquestUnitCount },
    { key: 'has_reva', value: playerReadyData.revaText !== '' },
    { key: 'has_wat', value: playerReadyData.watText !== '' },
    { key: 'wat_ready', value: playerReadyData.watReady === GREEN_CHECK },
    { key: 'reva_ready', value: playerReadyData.revaReady === GREEN_CHECK },
    { key: 'zeffo_ready', value: playerReadyData.zeffoReady.startsWith(GREEN_CHECK) },
    { key: 'mandalore_ready', value: playerReadyData.mandaloreReady.startsWith(GREEN_CHECK) },
    { key: 'tw_omi_count', value: playerTwOmicrons.length },
    { key: 'dc9_count', value: Object.keys(datacronCounts).length }
  ];

  const friendlyNameMap = new Map(criteriaFriendly.map(c => [c.key, c.value]));

  const CriteriaCheck = {};

  criteriaKeys.forEach(c =>
  {
    let requiredValue = criteria[c.key];
    if (c.key === 'missed_conquest_units_count')
    {
      requiredValue = conquestUnits.length - requiredValue;
    }
    const meetsCriteria = (typeof c.value === 'boolean')
      ? c.value
      : parseFloat(c.value) >= parseFloat(requiredValue);

    CriteriaCheck[c.key] = {
      currentValue: c.value,
      requiredValue: requiredValue,
      status: meetsCriteria ? GREEN_CHECK : RED_X
    };
  });
  let criteriatest = criteria
  criteriatest.missed_conquest_units_count = conquestUnits.length - criteria.missed_conquest_units_count;
  criteriatest.comlink_guild_id = false;

  const validCriteria = Object.keys(criteriatest).filter(key =>
    criteriatest[key] !== false &&
    criteriatest[key] !== 0 &&
    criteriatest[key] !== '0.0' &&
    criteriatest[key] !== '0.00' &&
    key !== 'req_conquest_count'
  );

  const percentPerCriteria = 100 / validCriteria.length;

  let totalGuildMatch = 0;
  validCriteria.forEach(c =>
  {
    let requiredValue = criteria[c];

    if (c === 'missed_conquest_units_count')
    {
      requiredValue = conquestUnits.length - requiredValue;
    }
    let meetsCriteria = CriteriaCheck[c].status
    if (meetsCriteria.startsWith(GREEN_CHECK)) meetsCriteria = true
    else meetsCriteria = false
    if ((meetsCriteria && criteria[c] !== false) || (meetsCriteria && criteria[c] !== 0))
    {
      totalGuildMatch += percentPerCriteria;
    }
  });


  let requiredcriteriadescription = '';
  let notcriteriadescription = '';

  for (const key in CriteriaCheck)
  {
    if (CriteriaCheck.hasOwnProperty(key))
    {
      let currentValueText, requiredValueText, comparisonSymbol;
      let currentValue = CriteriaCheck[key].currentValue;
      let requiredValue = CriteriaCheck[key].requiredValue;
      if (!isNaN(parseFloat(currentValue)) && !isNaN(parseFloat(requiredValue)))
      {
        let currentNum = parseFloat(currentValue);

        let requiredNum = parseFloat(requiredValue);
        if (currentNum > requiredNum)
        {
          comparisonSymbol = '>';
        } else if (currentNum < requiredNum)
        {
          comparisonSymbol = '<';
        } else
        {
          comparisonSymbol = '=';
        }
        currentValueText = currentNum;
        requiredValueText = requiredNum;
      } else
      {
        if (currentValue == "❌") currentValue = false
        if (currentValue == "✅") currentValue = true
        currentValueText = currentValue === true ? '' : (currentValue === false ? '' : currentValue);
        requiredValueText = requiredValue === true ? '' : (requiredValue === false ? '' : requiredValue);
        comparisonSymbol = '';
      }
      if (requiredValue == 0.00 || requiredValue == 0.0) requiredValue = 0
      let descriptionText = `${CriteriaCheck[key].status} ${friendlyNameMap.get(key)} ${currentValueText}${comparisonSymbol ? ` ${comparisonSymbol} ` : ' '}${requiredValueText}\n`;

      if (key === 'missed_conquest_units_count' && !criteria.req_conquest_count)
      {
        notcriteriadescription += descriptionText + '\n';
      } else if (key === 'missed_conquest_units_count' && criteria.req_conquest_count)
      {
        requiredcriteriadescription += descriptionText + '\n';
      } else if (requiredValue !== false && requiredValue !== 0)
      {
        requiredcriteriadescription += descriptionText + '\n';
      } else
      {
        notcriteriadescription += descriptionText + '\n';
      }
    }
  }
  let criteriametdescription = ''
  if (validCriteria.length > 0) criteriametdescription = `Percent met: ${totalGuildMatch.toFixed(2)}%`
  const criteriaEmbed = {
    color: 0x579d10,
    title: `Criteria Check for ${guildName}\n${criteriametdescription}`,
    description: `[${playerName} (${allycode})](https://swgoh.gg/p/${allycode}/) Last Active: <t:${player.lastActivityTime / 1000}:R>`,
    fields: [
      {
        name: 'Required Criteria',
        value: requiredcriteriadescription || `No criteria required or it is set to 0.\nMinimum allowed missed conquest units = ${conquestUnits.length}`,
        inline: false
      },
      {
        name: 'Other Checks',
        value: notcriteriadescription || 'All criteria required. \`/guild criteria set\` to edit required criteria',
        inline: false
      }
    ],
    image: { url: `attachment://${playerName}` }
  };

  return criteriaEmbed;
}