const playerUtils = require("../../utils/player");
const discordUtils = require("../../utils/discord");
const readyUtils = require("../../utils/ready");
const raidCommon = require("./raidCommon");
const database = require ("../../database");
const { SlashCommandSubcommandBuilder } = require('@discordjs/builders');
const queueUtils = require ("../../utils/queue");
const { CLIENT } = require("../../utils/discordClient");

const CHECK_CHOICES = [
    { name: readyUtils.RAID_CHECK_FUNCTION_MAPPING.ALLNABOO.desc, value: readyUtils.RAID_CHECK_FUNCTION_MAPPING.ALLNABOO.value },
    { name: readyUtils.RAID_CHECK_FUNCTION_MAPPING.NABOOGUNGANS.desc, value: readyUtils.RAID_CHECK_FUNCTION_MAPPING.NABOOGUNGANS.value },
    { name: readyUtils.RAID_CHECK_FUNCTION_MAPPING.NABOOQA.desc, value: readyUtils.RAID_CHECK_FUNCTION_MAPPING.NABOOQA.value },
    { name: readyUtils.RAID_CHECK_FUNCTION_MAPPING.NABOOLU.desc, value: readyUtils.RAID_CHECK_FUNCTION_MAPPING.NABOOLU.value },
    { name: readyUtils.RAID_CHECK_FUNCTION_MAPPING.NABOOKB.desc, value: readyUtils.RAID_CHECK_FUNCTION_MAPPING.NABOOKB.value },
    { name: readyUtils.RAID_CHECK_FUNCTION_MAPPING.NABOOMAUL.desc, value: readyUtils.RAID_CHECK_FUNCTION_MAPPING.NABOOMAUL.value },
    { name: readyUtils.RAID_CHECK_FUNCTION_MAPPING.NABOOB2.desc, value: readyUtils.RAID_CHECK_FUNCTION_MAPPING.NABOOB2.value },
    { name: readyUtils.RAID_CHECK_FUNCTION_MAPPING.NABOOSEPSITH.desc, value: readyUtils.RAID_CHECK_FUNCTION_MAPPING.NABOOSEPSITH.value },
    { name: readyUtils.RAID_CHECK_FUNCTION_MAPPING.NABOOGR.desc, value: readyUtils.RAID_CHECK_FUNCTION_MAPPING.NABOOGR.value },
    { name: readyUtils.RAID_CHECK_FUNCTION_MAPPING.ALLENDOR.desc, value: readyUtils.RAID_CHECK_FUNCTION_MAPPING.ALLENDOR.value },
    { name: readyUtils.RAID_CHECK_FUNCTION_MAPPING.ENDOR_REBELS.desc, value: readyUtils.RAID_CHECK_FUNCTION_MAPPING.ENDOR_REBELS.value },
    { name: readyUtils.RAID_CHECK_FUNCTION_MAPPING.ENDOR_IT.desc, value: readyUtils.RAID_CHECK_FUNCTION_MAPPING.ENDOR_IT.value },
    { name: readyUtils.RAID_CHECK_FUNCTION_MAPPING.ENDOR_EWOKS.desc, value: readyUtils.RAID_CHECK_FUNCTION_MAPPING.ENDOR_EWOKS.value },
    { name: readyUtils.RAID_CHECK_FUNCTION_MAPPING.HC.desc, value: readyUtils.RAID_CHECK_FUNCTION_MAPPING.HC.value },
    { name: readyUtils.RAID_CHECK_FUNCTION_MAPPING.Jawas.desc, value: readyUtils.RAID_CHECK_FUNCTION_MAPPING.Jawas.value },
    { name: readyUtils.RAID_CHECK_FUNCTION_MAPPING.JKROR.desc, value: readyUtils.RAID_CHECK_FUNCTION_MAPPING.JKROR.value },
    { name: readyUtils.RAID_CHECK_FUNCTION_MAPPING.Maul.desc, value: readyUtils.RAID_CHECK_FUNCTION_MAPPING.Maul.value },
    { name: readyUtils.RAID_CHECK_FUNCTION_MAPPING.Tuskens.desc, value: readyUtils.RAID_CHECK_FUNCTION_MAPPING.Tuskens.value },
    { name: readyUtils.RAID_CHECK_FUNCTION_MAPPING.Mandalorians.desc, value: readyUtils.RAID_CHECK_FUNCTION_MAPPING.Mandalorians.value },
    { name: readyUtils.RAID_CHECK_FUNCTION_MAPPING.OR.desc, value: readyUtils.RAID_CHECK_FUNCTION_MAPPING.OR.value }
];

module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("ready")
        .setDescription("Check readiness for various Raid teams")
        .addStringOption(o => 
            o.setName("team")
            .setDescription("The team to check")
            .setRequired(true)
            .addChoices(...CHECK_CHOICES))
        .addIntegerOption(o =>
            o.setName("tier")
            .setDescription("The Difficulty to check")
            .setRequired(true)
            .addChoices(...raidCommon.DIFFICULTY_CHOICES))
        .addStringOption(o => 
            o.setName("allycode")
            .setDescription("An allycode in the guild (default: yours, for registered users)")
            .setRequired(false))
        .addIntegerOption(o =>
            o.setName("alt")
            .setDescription("Alt account (see /user me)")
            .setRequired(false)
            .setAutocomplete(true)),
    interact: async function(interaction)
    {
        let missingPermissions = discordUtils.getMissingBotPermissions(interaction, interaction.channel);
        if (missingPermissions)
        {
            await interaction.editReply(discordUtils.convertMissingBotPermissionsArrayToResponse(missingPermissions));
            return;
        }

        let team = interaction.options.getString("team");
        let tier = interaction.options.getInteger("tier");
        let allycode = interaction.options.getString("allycode");
        let altNum = interaction.options.getInteger("alt");
        
        if (!allycode)
        {
            let { botUser, guildId, error } = await playerUtils.authorize(interaction);
            if (error) {
                await interaction.editReply({ embeds: [discordUtils.createErrorEmbed("Error", error)] });
                return;
            }
            
            let alt = await playerUtils.chooseBestAlt(botUser, guildId, altNum, true);

            if (!alt)
            {
                let errorEmbed = discordUtils.createErrorEmbed("Not in Guild", `Account is not in a guild. Choose a different alt, specify an ally code, join a guild, or register.`);
                await interaction.editReply({ embeds: [errorEmbed] });
                return;
            }

            allycode = alt.allycode;
        }
        
        allycode = playerUtils.cleanAndVerifyStringAsAllyCode(allycode);

        let qsm = await queueUtils.getQueueSizeMessage(queueUtils.QUEUES.READY, interaction.client.shard);
        const editMessage = await interaction.editReply(`Processing raid ready check. ${qsm}`);

        await queueUtils.queueReadyJob(interaction.client.shard,
            {
                type: "RAID",
                team: team,
                tier: tier, 
                allycode: allycode, 
                messageId: editMessage.id, 
                channelId: editMessage.channelId ?? editMessage.channel.id,
                userId: interaction.user.id 
            });
    },
    autocomplete: async function(interaction) {
        const focusedValue = interaction.options.getFocused();
        await interaction.respond((await playerUtils.getAltAutocompleteOptions(focusedValue, interaction.user.id)));
    }
}