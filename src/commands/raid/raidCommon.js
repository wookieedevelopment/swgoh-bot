

const DIFFICULTY_CHOICES = [
    { name: "[7] R9 minimum", value: 7},
    { name: "[6] R8 minimum", value: 6},
    { name: "[5] R7 minimum", value: 5},
    { name: "[4] R5 minimum", value: 4},
    { name: "[3] R3 minimum", value: 3},
    { name: "[2] R1 minimum", value: 2},
    { name: "[1] G12 minimum", value: 1},
    { name: "[0] Base Difficulty (5★ G8 minimum)", value: 0}
];


const DIFFICULTY_MINIMUMS = {
    0: {
        minRarity: 5,
        minGearLevel: 7,
        maxScore: 300000
    },
    1: {
        minRarity: 7,
        minGearLevel: 12,
        maxScore: 450000
    },
    2: {
        minRarity: 7,
        minGearLevel: 13,
        minRelicLevel: 1,
        maxScore: 600000
    },
    3: {
        minRarity: 7,
        minGearLevel: 13,
        minRelicLevel: 3,
        maxScore: 900000
    },
    4: {
        minRarity: 7,
        minGearLevel: 13,
        minRelicLevel: 5,
        maxScore: 1200000
    },
    5: {
        minRarity: 7,
        minGearLevel: 13,
        minRelicLevel: 7,
        maxScore: 1800000
    },
    6: {
        minRarity: 7,
        minGearLevel: 13,
        minRelicLevel: 8,
        maxScore: 2700000
    }
}

const DIFFICULTY_MINIMUMS_NABOO = {
    0: {
        minRarity: 5,
        minGearLevel: 7,
        maxScore: 300000
    },
    1: {
        minRarity: 7,
        minGearLevel: 12,
        maxScore: 450000
    },
    2: {
        minRarity: 7,
        minGearLevel: 13,
        minRelicLevel: 1,
        maxScore: 600000
    },
    3: {
        minRarity: 7,
        minGearLevel: 13,
        minRelicLevel: 3,
        maxScore: 900000
    },
    4: {
        minRarity: 7,
        minGearLevel: 13,
        minRelicLevel: 5,
        maxScore: 1200000
    },
    5: {
        minRarity: 7,
        minGearLevel: 13,
        minRelicLevel: 7,
        maxScore: 1800000
    },
    6: {
        minRarity: 7,
        minGearLevel: 13,
        minRelicLevel: 8,
        maxScore: 2700000
    },
    7: {
        minRarity: 7,
        minGearLevel: 13,
        minRelicLevel: 9,
        maxScore: 3600000
    }
}

const FILLER_TEAM = 
{
    KRAYT: { qualityByTier: [50, 30, 20, 10, 10, 10, 10] },
    ENDOR: { qualityByTier: [50, 30, 20, 10, 10, 10, 10] },
    NABOO: { qualityByTier: [25, 10, 5, 0, 0, 0, 0] },
}

const MAX_POINTS_PER_TEAM_KRAYT = 2700000;
const MAX_ATTEMPTS_KRAYT = 5;
const MAX_TOTAL_SCORE_KRAYT = MAX_POINTS_PER_TEAM_KRAYT*MAX_ATTEMPTS_KRAYT;

const MAX_POINTS_PER_TEAM_ENDOR = 2700000;
const MAX_ATTEMPTS_ENDOR = 8;
const MAX_TOTAL_SCORE_ENDOR = MAX_POINTS_PER_TEAM_ENDOR*MAX_ATTEMPTS_ENDOR;

const MAX_POINTS_PER_TEAM_NABOO = 3600000;
const MAX_ATTEMPTS_NABOO = 5;
const MAX_TOTAL_SCORE_NABOO = MAX_POINTS_PER_TEAM_NABOO*MAX_ATTEMPTS_NABOO;

const findKraytRaid = (guildApiData) => guildApiData?.recentRaidResult.find(r => r.raidId === KRAYT.raidId);
const findEndorRaid = (guildApiData) => guildApiData?.recentRaidResult.find(r => r.raidId === ENDOR.raidId);
const findNabooRaid = (guildApiData) => guildApiData?.recentRaidResult.find(r => r.raidId === NABOO.raidId);

const KRAYT = {
    emoji: ":dragon:",
    KEY: "KRAYT",
    description: "Krayt",
    raidId: "kraytdragon",
    color: 'rgba(230,190,70, 1)',
    MAX_DIFFICULTY: 6,
    DIFFICULTY_MINIMUMS: DIFFICULTY_MINIMUMS,
    MAX_POINTS_PER_TEAM: MAX_POINTS_PER_TEAM_KRAYT,
    MAX_ATTEMPTS: MAX_ATTEMPTS_KRAYT,
    MAX_TOTAL_SCORE: MAX_TOTAL_SCORE_KRAYT,
    MIN_RECOMMENDED_GEAR: 10,
    EXTRA_RELIC_SCALING_FACTOR: 0.4,
    FILLER_TEAM: FILLER_TEAM.KRAYT,
    findRaid: findKraytRaid
};

const ENDOR = {
    emoji: ":bear:",
    KEY: "ENDOR",
    description: "Endor (Speeder Bike)",
    raidId: "speederbike",
    color: 'rgba(0,100,30, 1)',
    MAX_DIFFICULTY: 6,
    DIFFICULTY_MINIMUMS: DIFFICULTY_MINIMUMS,
    MAX_POINTS_PER_TEAM: MAX_POINTS_PER_TEAM_ENDOR,
    MAX_ATTEMPTS: MAX_ATTEMPTS_ENDOR,
    MAX_TOTAL_SCORE: MAX_TOTAL_SCORE_ENDOR,
    MIN_RECOMMENDED_GEAR: 10,
    EXTRA_RELIC_SCALING_FACTOR: 0.1,
    FILLER_TEAM: FILLER_TEAM.ENDOR,
    ROUND_FN: (x) => { return Math.round(x / 500) * 500; },
    findRaid: findEndorRaid
};

const NABOO = {
    emoji: ":crystal_ball:",
    KEY: "NABOO",
    description: "Battle for Naboo",
    raidId: "naboo",
    color: 'rgba(100,40,20, 1)',
    MAX_DIFFICULTY: 7,
    DIFFICULTY_MINIMUMS: DIFFICULTY_MINIMUMS_NABOO,
    MAX_POINTS_PER_TEAM: MAX_POINTS_PER_TEAM_NABOO,
    MAX_ATTEMPTS: MAX_ATTEMPTS_NABOO,
    MAX_TOTAL_SCORE: MAX_TOTAL_SCORE_NABOO,
    MIN_RECOMMENDED_GEAR: 10,
    EXTRA_RELIC_SCALING_FACTOR: 0.1,
    FILLER_TEAM: FILLER_TEAM.NABOO,
    ROUND_FN: (x) => { return Math.round(x / 1000) * 1000; },
    findRaid: findNabooRaid
};

module.exports = {
    KRAYT: KRAYT,
    ENDOR: ENDOR,
    NABOO: NABOO,
    DIFFICULTY_CHOICES: DIFFICULTY_CHOICES,
    DEFAULT_RAID: NABOO
}