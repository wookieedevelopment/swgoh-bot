const { SlashCommandSubcommandBuilder } = require('@discordjs/builders');
const queueUtils = require("../../utils/queue");
const recommendationsUtils = require("../../utils/recommendations");
const raidCommon = require("./raidCommon");
const guildUtils = require("../../utils/guild");
const playerUtils = require("../../utils/player");
const discordUtils = require("../../utils/discord");
const swapiUtils = require("../../utils/swapi");
const { EmbedBuilder, AttachmentBuilder, ActionRowBuilder, ButtonBuilder, StringSelectMenuBuilder, ButtonStyle } = require("discord.js");
const { createCanvas, loadImage } = require("canvas");
const NodeCache = require('node-cache');
const { logger, formatError } = require('../../utils/log');
const { parseAsync } = require('json2csv');
const { CLIENT } = require('../../utils/discordClient');


const raidGuildCaches = 
 {
    "KRAYT": new NodeCache({useClones: false}),
    "ENDOR": new NodeCache({useClones: false}),
    "NABOO": new NodeCache({useClones: false})
 } 

const ARROW_DOWN_IMG = './src/img/arrow_dn.png';
const ARROW_UP_IMG = './src/img/arrow_up.png';

const SORT_BY_OPTION = {
    NAME: "Name",
    ESTIMATE: "Estimate",
    ACTUAL: "Actual",
    DIFF: "Difference",
    DIFF_PERCENT: "Difference %"
};

module.exports = {
    raidGuildJobHandler: raidGuildJobHandler,
    data: new SlashCommandSubcommandBuilder()
        .setName("guild")
        .setDescription("Estimate total score for a raid for a guild")
        .addStringOption(o => 
            o.setName("allycode")
            .setDescription("Ally Code in the guild to estimate")
            .setRequired(false))
        .addStringOption(o =>
            o.setName("raid")
            .setDescription("The raid to check (Default: featured raid)")
            .setRequired(false)
            .addChoices(
                { name: raidCommon.NABOO.description, value: raidCommon.NABOO.KEY },
                { name: raidCommon.ENDOR.description, value: raidCommon.ENDOR.KEY },
                { name: raidCommon.KRAYT.description, value: raidCommon.KRAYT.KEY }
            ))
        .addIntegerOption(o =>
            o.setName("effort")
            .setDescription("Effort Level (Default: Medium)")
            .addChoices(
                { name: recommendationsUtils.EFFORT.LOW.name, value: recommendationsUtils.EFFORT.LOW.value },
                { name: recommendationsUtils.EFFORT.MEDIUM.name, value: recommendationsUtils.EFFORT.MEDIUM.value },
                { name: recommendationsUtils.EFFORT.HIGH.name, value: recommendationsUtils.EFFORT.HIGH.value }
            )
            .setRequired(false))
        .addNumberOption(o => 
            o.setName("deviation")
            .setDescription("Standard deviation cutoff for red/green coloring (default: 0.75)")
            .setRequired(false)
            .setMinValue(0)
            .setMaxValue(5))
        .addIntegerOption(o =>
            o.setName("alt")
            .setDescription("Alt account (/user me)")
            .setRequired(false)
            .setAutocomplete(true)),
    interact: async function(interaction)
    {
        let missingPermissions = discordUtils.getMissingBotPermissions(interaction, interaction.channel);
        if (missingPermissions)
        {
            await interaction.editReply(discordUtils.convertMissingBotPermissionsArrayToResponse(missingPermissions));
            return;
        }
        
        let raid = interaction.options.getString("raid") ?? raidCommon.DEFAULT_RAID.KEY;
        let allycode = interaction.options.getString("allycode");
        let standardDeviationMultiple = Math.round((interaction.options.getNumber("deviation") ?? 0.75)*100) / 100;
        let effort = recommendationsUtils.mapValueToEffort(interaction.options.getInteger("effort"));
        let altNum = interaction.options.getInteger("alt");

        let comlinkGuildId = null;
        if (!allycode)
        {
            let { botUser, guildId, error } = await playerUtils.authorize(interaction);
            if (error) {
                logger.error(formatError(error));
                await interaction.editReply({ embeds: [discordUtils.createErrorEmbed("Error", error)] });
                return;
            }

            let alt = await playerUtils.chooseBestAlt(botUser, guildId, altNum, true);

            if (!alt)
            {
                let errorEmbed = discordUtils.createErrorEmbed("Not in Guild", `Account is not in a guild. Choose a different alt, specify an ally code, join a guild, or register.`);
                await interaction.editReply({ embeds: [errorEmbed] });
                return;
            }

            comlinkGuildId = botUser.guilds.find(g => g.guildId === alt.guildId).comlinkGuildId;
            allycode = alt.allycode;
        }
        
        allycode = playerUtils.cleanAndVerifyStringAsAllyCode(allycode);
        
        if (!effort)
        {
            effort = recommendationsUtils.mapValueToEffort((await guildUtils.getGuildDefaultRaidEffort({ comlinkGuildId: comlinkGuildId, allycode: allycode }))) ?? recommendationsUtils.EFFORT.MEDIUM;
        }

        let response = await generateResponse(interaction, { raid: raid, comlinkGuildId: comlinkGuildId, allycode: allycode, sortBy: SORT_BY_OPTION.ESTIMATE, standardDeviationMultiple: standardDeviationMultiple, effort: effort });
        if (response) await interaction.editReply(response);
    },
    autocomplete: async function(interaction) {
        const focusedValue = interaction.options.getFocused();
        await interaction.respond((await playerUtils.getAltAutocompleteOptions(focusedValue, interaction.user.id)));
    },
    onSelect: async function(interaction)
    {
        await interaction.update({ content: "Sorting...", embeds: [], files: [], components: [] });

        let regex = /raid:g:sort:([\d]+(\.[\d]*)?):(\d):(.+):(.+)/

        let matches = regex.exec(interaction.customId);

        if (!matches || matches.length < 6) {
            // something went wrong
            logger.error("raid:g:sort interaction.customId doesn't match. customId is: " + interaction.customId);
            await interaction.editReply({ content: "Something went wrong, unfortunately. I'm working on it, but please try running `/raid guild` again." });
            return;
        }

        let raid = matches[4];
        let comlinkGuildId = matches[5];
        let standardDeviationMultiple = matches[1];
        let effort = recommendationsUtils.mapValueToEffort(parseInt(matches[3]));

        let response = await generateResponse(interaction, { raid: raid, comlinkGuildId: comlinkGuildId, sortBy: interaction.values[0], standardDeviationMultiple: standardDeviationMultiple, effort:effort });
        if (response)
        {
            response.content = null;
            await interaction.editReply(response);
        }
    }
}

// async function generateCsvResponse(interaction, raid, comlinkGuildId, effort)
// {
//     let estimatesData = undefined;
//     if (comlinkGuildId) estimatesData = raidGuildCaches[raid].get(getCacheKey(comlinkGuildId, effort));

//     if (estimatesData === undefined)
//     {
//         let guildPlayerData = await guildUtils.loadGuildPlayerData({ comlinkGuildId: comlinkGuildId, includeRecentGuildActivityInfo: true }, interaction);

//         estimatesData = await estimateGuildScores(guildPlayerData, raid, effort, interaction);
//         raidGuildCaches[raid].set(getCacheKey(guildPlayerData.comlinkGuildId, effort), estimatesData, 60*60*4);
//     }
    
//     const csvParsingOptions = { defaultValue: 0 }
//     let csvText = await parseAsync(estimatesData.playersAndScores, csvParsingOptions);
//     let buffer = Buffer.from(csvText);
    
//     let csvAttachment = new AttachmentBuilder(buffer, { name: `${raid.toLowerCase()}_${comlinkGuildId}_${new Date().getTime()}.csv`});
//     return { files: [csvAttachment] };
// }

function getCacheKey(comlinkGuildId, effort = recommendationsUtils.EFFORT.MEDIUM)
{
    return `${comlinkGuildId}::${effort.value}`;
}

async function generateResponse(interaction, { raid, allycode, comlinkGuildId, sortBy, standardDeviationMultiple, effort, errors })
{
    let estimatesData = undefined;

    if (comlinkGuildId) estimatesData = raidGuildCaches[raid].get(getCacheKey(comlinkGuildId, effort));

    if (estimatesData === undefined)
    {
        let qsm = await queueUtils.getQueueSizeMessage(queueUtils.QUEUES.RAID_BULK, interaction.client.shard);
        const editMessage = await interaction.editReply(`Processing \`/raid guild\`. ${qsm}`);

        await queueUtils.queueRaidBulkJob(interaction.client.shard,
            {
                jobType: "Guild",
                raid: raid,
                allycode: allycode,
                comlinkGuildId: comlinkGuildId,
                sortBy: sortBy,
                standardDeviationMultiple: standardDeviationMultiple,
                effort: effort.value,
                messageId: editMessage.id, 
                channelId: editMessage.channelId ?? editMessage.channel.id,
                userId: interaction.user.id 
            });

        return;
    }

    sortEstimates(estimatesData, sortBy);

    let guildEstimateCanvas = await generateScoresGraphic(estimatesData.playersAndScores, estimatesData.totals, sortBy, standardDeviationMultiple);

    let buffer = guildEstimateCanvas.toBuffer("image/png");
        
    let fileName = `guild-${raid.toLowerCase()}-estimate-${new Date().getTime()}.png`
    const file = new AttachmentBuilder(buffer, { name: fileName });

    let meanText = +(Math.round(estimatesData.totals.mean + "e+2") + "e-2").toLocaleString("en");
    let standardDeviationText = +(Math.round(estimatesData.totals.standardDeviation + "e+2") + "e-2").toLocaleString("en")
    
    let embed = new EmbedBuilder()
        .setTitle(`Estimated Scores for ${estimatesData.guildName}`)
        .setDescription(
`Raid: ${raidCommon[raid].description}
Effort: ${effort.shortName}
Estimated Guild Score: ${estimatesData.totals.estimate.toLocaleString("en")}
Last Actual Guild Score: ${estimatesData.totals.guildActual.toLocaleString("en")}
Average Diff. % Between Estimated and Actual: ${meanText}%
Standard Deviation: ${standardDeviationText}%

${raidCommon[raid].emoji} Estimates are based on the current guild members.
${raidCommon[raid].emoji} Player actuals are the score in the last raid *in this guild*.
${raidCommon[raid].emoji} Total Actual is the last actual guild score. This *will not match* the sum of players if your membership changed.
${raidCommon[raid].emoji} Negative difference indicates a player may be underperforming.
${raidCommon[raid].emoji} Red/Green indicates a player performed below/above **${standardDeviationMultiple}** standard deviations from the average Diff. %.`)
    embed.setImage("attachment://" + fileName);

    const csvParsingOptions = { defaultValue: 0 }
    let csvText = await parseAsync(estimatesData.playersAndScores, csvParsingOptions);
    let csvBuffer = Buffer.from(csvText);
    
    const csvAttachment = new AttachmentBuilder(csvBuffer, { name: `${raid.toLowerCase()}_${estimatesData.guildComlinkId}_${new Date().getTime()}.csv`});
    
    let response = { embeds: [embed], files: [file, csvAttachment] };
    addComponents(response, raid, estimatesData.guildComlinkId, standardDeviationMultiple, effort);

    return response;
}

function addComponents(response, raid, comlinkGuildId, standardDeviationMultiple, effort)
{   
    let sortRow = new ActionRowBuilder();
    sortRow.addComponents(
        new StringSelectMenuBuilder()
            .setCustomId(`raid:g:sort:${standardDeviationMultiple}:${effort.value}:${raid}:${comlinkGuildId}`)
            .setPlaceholder('Sort by...')
            .addOptions([
                { label: SORT_BY_OPTION.NAME, value: SORT_BY_OPTION.NAME },
                { label: SORT_BY_OPTION.ESTIMATE, value: SORT_BY_OPTION.ESTIMATE },
                { label: SORT_BY_OPTION.ACTUAL, value: SORT_BY_OPTION.ACTUAL },
                { label: SORT_BY_OPTION.DIFF, value: SORT_BY_OPTION.DIFF },
                { label: SORT_BY_OPTION.DIFF_PERCENT, value: SORT_BY_OPTION.DIFF_PERCENT },
            ])
    );

    response.components = [sortRow, /*buttonRow*/];
}

function getEstimatedStatusMessage(count, guildPlayerData)
{
    let errorsString = guildPlayerData.errors?.map(e => `${e.allycode}: ${e.message}`).join("\n- ");
    return `Estimated ${count}/${swapiUtils.getGuildRoster(guildPlayerData.guildApiData).length} players for **${guildPlayerData.guildName}**. 
Errors: ${guildPlayerData.errors?.length ?? 0}${errorsString.length > 0 ? `\n- ${errorsString}` : ""}`;
}

async function estimateGuildScores(guildPlayerData, raid, effort, editMessage)
{
    let estimatesData = {
        guildComlinkId: guildPlayerData.comlinkGuildId,
        guildName: guildPlayerData.guildName,
        playersAndScores: [],
        totals: { estimate: 0, actual: 0, diff: 0, guildActual: 0, mean: 0, standardDeviation: 0 }
    }
    
    let raidData = raidCommon[raid].findRaid(guildPlayerData.guildApiData);

    estimatesData.totals.guildActual = raidData?.raidMember.reduce((p, v) => p + parseInt(v.memberProgress), 0) ?? 0;

    let totalDiffPercent = 0, playersToCalcDiffPercentMean = 0;

    for (let pi = 0; pi < guildPlayerData.players.length; pi++)
    {
        let p = guildPlayerData.players[pi];

        if (pi%10 === 0)
        {
            await editMessage.edit(getEstimatedStatusMessage(pi, guildPlayerData));
        }

        let rec = await recommendationsUtils.generateRaidRecommendations(p, 
            {
                DIFFICULTY_MINIMUMS: raidCommon[raid].DIFFICULTY_MINIMUMS,
                MAX_ATTEMPTS: raidCommon[raid].MAX_ATTEMPTS,
                MAX_TOTAL_SCORE: raidCommon[raid].MAX_TOTAL_SCORE,
                REC_TYPE: recommendationsUtils.REC_TYPE[raid],
                MIN_RECOMMENDED_GEAR: raidCommon[raid].MIN_RECOMMENDED_GEAR,
                ROUND_FN: raidCommon[raid].ROUND_FN,
                FILLER_TEAM: raidCommon[raid].FILLER_TEAM,
                effort: effort,
                extraRelicScalingFactor: raidCommon[raid].EXTRA_RELIC_SCALING_FACTOR
            });

        let lastActualScore = raidData?.raidMember.find(pr => pr.playerId === p.playerId)?.memberProgress;
        if (lastActualScore != null) lastActualScore = parseInt(lastActualScore);

        let diff = (lastActualScore != null) ? lastActualScore - rec.points : null;
        let diffPercent = null;
        if (rec.points != 0)
        {
            diffPercent = (lastActualScore != null) ? +(Math.round(((diff / rec.points) * 100.0) + "e+1") + "e-1") : null;
        } else {
            diffPercent = Number.MAX_SAFE_INTEGER;
        }

        estimatesData.playersAndScores.push(
            { 
                name: swapiUtils.getPlayerName(p), 
                allycode: swapiUtils.getPlayerAllycode(p),
                estimatedScore: rec.points, 
                lastActualScore: lastActualScore, 
                diff: diff,
                diffPercent: diffPercent
            });
            
        estimatesData.totals.estimate += rec.points;
        estimatesData.totals.actual += lastActualScore ?? 0;
        estimatesData.totals.diff += diff ?? 0;

        if (diffPercent != null && diffPercent != Number.MAX_SAFE_INTEGER) {
            totalDiffPercent += diffPercent;
            playersToCalcDiffPercentMean++;
        }
    }
    
    await editMessage.edit(getEstimatedStatusMessage(guildPlayerData.players.length, guildPlayerData));
    
    if (playersToCalcDiffPercentMean > 0)
    {
        estimatesData.totals.mean = totalDiffPercent / playersToCalcDiffPercentMean;
        estimatesData.totals.standardDeviation = Math.sqrt(estimatesData.playersAndScores.filter(s => s.diffPercent != null && s.diffPercent != Number.MAX_SAFE_INTEGER).map(x => ((x.diffPercent ?? 0) - estimatesData.totals.mean)**2).reduce((a, b) => a + b) / playersToCalcDiffPercentMean);
    }

    return estimatesData;
}

function sortEstimates(estimatesData, sortBy)
{
    switch (sortBy)
    {
        case SORT_BY_OPTION.ESTIMATE:
            estimatesData.playersAndScores.sort((a, b) => b.estimatedScore - a.estimatedScore);
            break;
        case SORT_BY_OPTION.ACTUAL:
            estimatesData.playersAndScores.sort((a, b) => (b.lastActualScore ?? 0) - (a.lastActualScore ?? 0));
            break;
        case SORT_BY_OPTION.DIFF:
            estimatesData.playersAndScores.sort((a, b) => (b.diff ?? 0) - (a.diff ?? 0));
            break;
        case SORT_BY_OPTION.DIFF_PERCENT:
            estimatesData.playersAndScores.sort((a, b) => ((b.diffPercent ?? 0) - (a.diffPercent ?? 0)) || (a.name.localeCompare(b.name)));
            break;
        case SORT_BY_OPTION.NAME:
            estimatesData.playersAndScores.sort((a, b) => a.name.localeCompare(b.name));
            break;
    }
}

const nameColWidth = 220;
const headerRowHeight = 28;
const playerRowHeight = 24;
const scoreColWidth = 100;
const totalRowHeight = 30;
const separatorColor = "#bbbb00";
const rowColorEven = "#001";
const rowColorOdd = "#002"
const width = nameColWidth + (scoreColWidth * 4) + 4;
const arrowWidth = 18;
const arrowHeight = 19;
async function generateScoresGraphic(playersAndScores, totals, sortBy, standardDeviationMultiple)
{
    const height = headerRowHeight + playerRowHeight*(playersAndScores.length) + totalRowHeight;

    const canvas = createCanvas(width, height);
    const context = canvas.getContext('2d');
    context.fillStyle = "rgb(0, 0, 0)";
    context.fillRect(0, 0, width, height);

    context.font = `12pt "Noto Sans","Noto Sans SC","Noto Sans Armenian","Noto Sans Thai","Noto Sans Arabic","Noto Sans Korean"`
    context.textAlign = "left"
    context.textBaseline = "middle";
    context.fillStyle = "#fff"
    
    context.fillText("Player", 2, 2 + playerRowHeight / 2);
    context.fillText("Estimate", 2 + nameColWidth, 2 + playerRowHeight / 2);
    context.fillText("Actual", 2 + nameColWidth + scoreColWidth, 2 + playerRowHeight / 2);
    context.fillText("Difference", 2 + nameColWidth + scoreColWidth*2, 2 + playerRowHeight / 2);
    context.fillText("Diff. %", 2 + nameColWidth + scoreColWidth*3, 2 + playerRowHeight / 2);
    
    let arrowUrl, arrowX;
    switch (sortBy)
    {
        case SORT_BY_OPTION.NAME:
            arrowUrl = ARROW_UP_IMG;
            arrowX = nameColWidth - 170;
            break;
        case SORT_BY_OPTION.ESTIMATE:
            arrowUrl = ARROW_DOWN_IMG;
            arrowX = nameColWidth + scoreColWidth - 30;
            break;
        case SORT_BY_OPTION.ACTUAL:
            arrowUrl = ARROW_DOWN_IMG;
            arrowX = nameColWidth + scoreColWidth*2 - 45;
            break;
        case SORT_BY_OPTION.DIFF:
            arrowUrl = ARROW_DOWN_IMG;
            arrowX = nameColWidth + scoreColWidth*3 - 18;
            break;
        case SORT_BY_OPTION.DIFF_PERCENT:
            arrowUrl = ARROW_DOWN_IMG;
            arrowX = nameColWidth + scoreColWidth*4 - 45;
            break;
    }

    const arrowImg = await loadImage(arrowUrl);
    context.drawImage(arrowImg, arrowX, 5, 
        arrowWidth, arrowHeight);

    context.fillStyle = separatorColor;
    context.fillRect(2,playerRowHeight,width - 4, 2)

    for (let pIx = 0; pIx < playersAndScores.length; pIx++)
    {
        drawPlayerRow(canvas, pIx, playersAndScores[pIx], totals, standardDeviationMultiple);
    }

    context.fillStyle = separatorColor;
    context.fillRect(2,height - totalRowHeight,width - 4, 2);
    
    context.fillStyle = "#fff";
    context.fillText("Total", 2, height - totalRowHeight/2);
    context.fillText(totals.estimate.toLocaleString("en"), 2 + nameColWidth, height - totalRowHeight/2);
    context.fillText(totals.guildActual.toLocaleString("en"), 2 + nameColWidth + scoreColWidth, height - totalRowHeight/2);

    context.fillStyle = (totals.diff === 0) ? "#fff" : ((totals.diff > 0) ? "green" : "red");
    let diffText = totals.diff.toLocaleString("en");
    let diffPercentText = +(Math.round((totals.diff / totals.estimate * 100.0) + "e+1") + "e-1").toLocaleString("en") + "%";
    if (totals.diff > 0) 
    {
        diffText = `+${diffText}`;
        diffPercentText = `+${diffPercentText}`;
    }
    context.fillText(diffText, 2 + nameColWidth + scoreColWidth*2, height - totalRowHeight/2);
    context.fillText(diffPercentText, 2 + nameColWidth + scoreColWidth*3, height - totalRowHeight/2);

    return canvas;
}

function drawPlayerRow(canvas, curPlayerIndex, curPlayer, totals, standardDeviationMultiple)
{
    const context = canvas.getContext('2d');

    context.fillStyle = (curPlayerIndex % 2 === 1) ? rowColorOdd : rowColorEven;
    context.fillRect(2, headerRowHeight + (curPlayerIndex)*playerRowHeight, width - 4, playerRowHeight);

    context.font = `12pt "Noto Sans","Noto Sans SC","Noto Sans Armenian","Noto Sans Thai","Noto Sans Arabic","Noto Sans Korean"`
    context.textAlign = "left"
    context.textBaseline = "middle";
    context.fillStyle = "#fff"

    context.fillText(curPlayer.name,
        2,
        headerRowHeight + (curPlayerIndex+0.5)*playerRowHeight,
        nameColWidth - 2);

    context.fillText(curPlayer.estimatedScore.toLocaleString("en"),
        nameColWidth + 2,
        headerRowHeight + (curPlayerIndex+0.5)*playerRowHeight,
        scoreColWidth - 2
        );

    let lastActualScoreText = (curPlayer.lastActualScore != null) ? parseInt(curPlayer.lastActualScore).toLocaleString("en") : "Unknown";

    if (lastActualScoreText === "Unknown") context.fillStyle = "#aaa";
    context.fillText(lastActualScoreText,
        nameColWidth + scoreColWidth + 2,
        headerRowHeight + (curPlayerIndex+0.5)*playerRowHeight,
        scoreColWidth - 2
        );

    let diffText;

    if (curPlayer.diff == null) context.fillStyle = "#aaa";
    else if (curPlayer.diff > 0 || (curPlayer.diffPercent >= (totals.mean + (standardDeviationMultiple*totals.standardDeviation)))) context.fillStyle = "green";
    else if (curPlayer.diffPercent > totals.mean - (standardDeviationMultiple*totals.standardDeviation)) context.fillStyle = "#fff";
    else if (curPlayer.diff < 0) context.fillStyle = "red";
    
    if (curPlayer.diff != null)
    {
        diffText = curPlayer.diff.toLocaleString("en");
        if (curPlayer.diff > 0) diffText = `+${diffText}`;
    } else {
        diffText = "Unknown";
    }

    context.fillText(diffText,
        nameColWidth + scoreColWidth*2 + 2,
        headerRowHeight + (curPlayerIndex+0.5)*playerRowHeight,
        scoreColWidth - 2
        );

    let diffPercentText;
    if (curPlayer.diffPercent === Number.MAX_SAFE_INTEGER)
    {
        diffPercentText = "∞";
    }
    else if (curPlayer.diffPercent != null)
    {
        diffPercentText = curPlayer.diffPercent.toLocaleString("en", {minimumFractionDigits: 1}) + "%";
        if (curPlayer.diffPercent > 0) diffPercentText = `+${diffPercentText}`;
    } else {
        diffPercentText = "Unknown";
    }

    context.fillText(diffPercentText,
        nameColWidth + scoreColWidth*3 + 2,
        headerRowHeight + (curPlayerIndex+0.5)*playerRowHeight,
        scoreColWidth - 2
        );
}

async function raidGuildJobHandler(jobBatch)
{
    const job = jobBatch[0];
    let channel, editMessage;
    try 
    {
        channel = CLIENT.channels.cache.get(job.data.channelId);
        editMessage = (await channel.messages.fetch({ message: job.data.messageId }));
    }
    catch (e)
    {
        let discordUser = await CLIENT.users.fetch(job.data.userId);
        await discordUser.send(
            {
                content: `WookieeBot may be missing permissions in channel <#${job.data.channelId}>. Please verify with \`/check-permissions\``
            }
        );

        return;
    }

    try 
    {
        await editMessage.edit({ content: "Processing your request now...", components: [], files: [], embeds: [] });

        let guildPlayerData = await guildUtils.loadGuildPlayerData(
            { 
                allycode: job.data.allycode, 
                comlinkGuildId: job.data.comlinkGuildId,
                editMessageId: job.data.messageId,
                editMessageChannelId: job.data.channelId,
                includeRecentGuildActivityInfo: true 
            });

        let effort = recommendationsUtils.mapValueToEffort(job.data.effort);
        let estimatesData = await estimateGuildScores(guildPlayerData, job.data.raid, effort, editMessage);
        raidGuildCaches[job.data.raid].set(getCacheKey(guildPlayerData.comlinkGuildId, effort), estimatesData, 60*60*4);

        let response = await generateResponse(null, 
            { 
                allycode: job.data.allycode, 
                comlinkGuildId: guildPlayerData.comlinkGuildId, 
                effort: effort,
                raid: job.data.raid,
                sortBy: job.data.sortBy,
                standardDeviationMultiple: job.data.standardDeviationMultiple,
                errors: guildPlayerData.errors
            });

        response.content = `<@${job.data.userId}>`; 
        await editMessage.reply(response);
    }
    catch (e)
    {
        logger.error(formatError(e));
        await editMessage.reply(discordUtils.createErrorEmbed("Error", "An error occurred processing the request. Please try again or connect with WookieeBot developers for help."));
    }
}