const { SlashCommandSubcommandBuilder } = require('@discordjs/builders');
const recommendationsUtils = require("../../utils/recommendations");
const playerUtils = require("../../utils/player");
const guildUtils = require("../../utils/guild");
const swapiUtils = require("../../utils/swapi");
const unitsUtils = require("../../utils/units");
const { EmbedBuilder, AttachmentBuilder } = require('discord.js');
const raidCommon = require('./raidCommon');
const discordUtils = require("../../utils/discord");
const queueUtils = require("../../utils/queue");
const { CLIENT } = require('../../utils/discordClient');
const comlink = require('../../utils/comlink');
const _ = require("lodash");
const entitlements = require('../../utils/entitlements');

const RECOMMENDATION_TYPE = {
    UNIT: { name: "Unit", value: "U" },
    TEAM: { name: "Team", value: "T" }
}

const SORT_TYPE = {
    CRYSTALS_PER_POINT: 1,
    MAX_POINTS: 2
}

module.exports = {
    raidRecJobHandler: raidRecJobHandler,
    data: new SlashCommandSubcommandBuilder()
        .setName("rec")
        .setDescription("Get recommendations on raid units to upgrade")
        .addStringOption(o =>
            o.setName("type")
            .setDescription("Type of recommendation")
            .addChoices(
                { name: RECOMMENDATION_TYPE.UNIT.name, value: RECOMMENDATION_TYPE.UNIT.value },
                { name: RECOMMENDATION_TYPE.TEAM.name, value: RECOMMENDATION_TYPE.TEAM.value }
            )
            .setRequired(true))
        .addStringOption(o => 
            o.setName("allycode")
            .setDescription("Ally Code to find recommendations for")
            .setRequired(false))
        .addStringOption(o =>
            o.setName("raid")
            .setDescription("The raid to check (Default: featured raid)")
            .setRequired(false)
            .addChoices(
                { name: raidCommon.NABOO.description, value: raidCommon.NABOO.KEY },
                { name: raidCommon.ENDOR.description, value: raidCommon.ENDOR.KEY },
                { name: raidCommon.KRAYT.description, value: raidCommon.KRAYT.KEY }
            ))
        .addIntegerOption(o =>
            o.setName("effort")
            .setDescription("Effort Level (Default: Guild Default or Medium)")
            .addChoices(
                { name: recommendationsUtils.EFFORT.LOW.name, value: recommendationsUtils.EFFORT.LOW.value },
                { name: recommendationsUtils.EFFORT.MEDIUM.name, value: recommendationsUtils.EFFORT.MEDIUM.value },
                { name: recommendationsUtils.EFFORT.HIGH.name, value: recommendationsUtils.EFFORT.HIGH.value }
            )
            .setRequired(false))
        .addIntegerOption(o => 
            o.setName("sort")
            .setDescription("How results should be sorted (default: by crystals per point)")
            .addChoices(
                { name: "Crystals per Point", value: SORT_TYPE.CRYSTALS_PER_POINT },
                { name: "Max Point Gain", value: SORT_TYPE.MAX_POINTS }
            )
        )
        .addStringOption(o => 
            o.setName("exclude")
            .setDescription("List of units to exclude, separated by commas.")
            .setRequired(false))
        .addStringOption(o => 
            o.setName("include")
            .setDescription("List of units, at least one of which MUST be included, separated by commas.")
            .setRequired(false))
        .addIntegerOption(o => 
            o.setName("tier")
            .setDescription("Tier to simulate (min tier for Unit recs)")
            .addChoices(...raidCommon.DIFFICULTY_CHOICES)
            .setRequired(false))
        .addIntegerOption(o =>
            o.setName("alt")
            .setDescription("Alt account (/user me)")
            .setRequired(false)
            .setAutocomplete(true)),
    interact: async function(interaction)
    {
        let missingPermissions = discordUtils.getMissingBotPermissions(interaction, interaction.channel);
        if (missingPermissions)
        {
            await interaction.editReply(discordUtils.convertMissingBotPermissionsArrayToResponse(missingPermissions));
            return;
        }

        let isSubscriber = await playerUtils.isSubscriberInteraction(interaction);
        if (!isSubscriber)
        {
            await interaction.editReply(entitlements.generateNotSubscribedResponseObject());

            return;
        }
        
        let raid = interaction.options.getString("raid") ?? raidCommon.DEFAULT_RAID.KEY;
        let allycode = interaction.options.getString("allycode");
        let effort = interaction.options.getInteger("effort");
        let sort = interaction.options.getInteger("sort") ?? SORT_TYPE.CRYSTALS_PER_POINT;
        let alt = interaction.options.getInteger("alt");
        let type = interaction.options.getString("type");
        let exclude = interaction.options.getString("exclude");
        let include = interaction.options.getString("include");
        let tier = interaction.options.getInteger("tier");

        let { botUser, guildId, error } = await playerUtils.authorize(interaction);
        
        if (allycode) allycode = playerUtils.cleanAndVerifyStringAsAllyCode(allycode);
        allycode = playerUtils.chooseBestAllycode(botUser, guildId, allycode, alt);

        if (!allycode)
        {
            await interaction.editReply({ content: "Please provide an allycode: `/raid max allycode:123456789` or `/register`" });
            return;
        }

        
        let playerData = await comlink.getPlayerArenaProfileByAllycode(allycode);

        if (!playerData)
        {
            await interaction.editReply({ content: `No player found with allycode *${allycode}*.`})
            return;
        }

        if (!effort)
        {
            effort = (await guildUtils.getGuildDefaultRaidEffort({ allycode: allycode })) ?? recommendationsUtils.EFFORT.MEDIUM.value;
        }

        let qsm = await queueUtils.getQueueSizeMessage(queueUtils.QUEUES.RAID_BULK, interaction.client.shard);
        const editMessage = await interaction.editReply(`Processing raid simulation. ${qsm}`);

        await queueUtils.queueRaidBulkJob(interaction.client.shard,
            {
                jobType: "Rec",
                type: type,
                allycode: allycode,
                effort: effort,
                sort: sort,
                raid: raid, 
                exclude: exclude,
                include: include,
                tier: tier,
                messageId: editMessage.id, 
                channelId: editMessage.channelId ?? editMessage.channel.id,
                userId: interaction.user.id 
            });
    },
    autocomplete: async function(interaction) {
        const focusedValue = interaction.options.getFocused();
        await interaction.respond((await playerUtils.getAltAutocompleteOptions(focusedValue, interaction.user.id)));
    }
}

async function getUnitUpgradeRecommendationsResponse(raid, playerData, effort, sort, minTierToConsider, defIdsToExclude, defIdsToInclude, messageCallback) {
    let currentEstimate = await getRaidEstimate(playerData, raid, effort);
    minTierToConsider = minTierToConsider ?? 0;

    // push each unit up to the next tier one by one, and run the recommendations algorithm
    let bestUpgrade = {
        estimatedScore: currentEstimate.points,
        unitToUpgrade: null,
        tierToUpgrade: 0,
        crystalsPerPoint: 0,
        upgradeCostInCrystals: 0,
        diffFromCurrentScore: 0
    };

    let simResults = [];

    let DIFFICULTY_MINIMUMS = raidCommon[raid].DIFFICULTY_MINIMUMS;
    let maxRelicTierForRaid = DIFFICULTY_MINIMUMS[raidCommon[raid].MAX_DIFFICULTY].minRelicLevel;
    for (let u = 0; u < recommendationsUtils.ALLOWED_UNITS_BY_RAID[raid].length; u++)
    {
        let raidUnit = recommendationsUtils.ALLOWED_UNITS_BY_RAID[raid][u];
        if (defIdsToExclude?.length > 0 && defIdsToExclude.find(d => d === swapiUtils.getUnitDefId(raidUnit))) continue; // skip excluded units
        if (defIdsToInclude?.length > 0 && !defIdsToInclude.find(d => d === swapiUtils.getUnitDefId(raidUnit))) continue; // skip units that aren't included

        let playerUnit = swapiUtils.getPlayerRoster(playerData).find(u => swapiUtils.getUnitDefId(u) === swapiUtils.getUnitDefId(raidUnit));

        if (!playerUnit) continue;
        if ((swapiUtils.getUnitRelicLevel(playerUnit) ?? 0) -2 >= maxRelicTierForRaid) continue;

        let tierMatch = -1;
        for (let tier = raidCommon[raid].MAX_DIFFICULTY; tier >= 0; tier--)
        {
            // find the current tier for the unit
            if (DIFFICULTY_MINIMUMS[tier].minRelicLevel)
            {
                if (swapiUtils.getUnitRelicLevel(playerUnit) - 2 >= DIFFICULTY_MINIMUMS[tier].minRelicLevel) tierMatch = tier;
                else continue;
            }
            
            if (swapiUtils.getUnitGearLevel(playerUnit) >= DIFFICULTY_MINIMUMS[tier].minGearLevel) tierMatch = tier;
            else continue;

            if (swapiUtils.getUnitRarity(playerUnit) >= DIFFICULTY_MINIMUMS[tier].minRarity) tierMatch = tier;

            if (tierMatch != -1) break;
        }

        let simulatedTier = Math.max(minTierToConsider, tierMatch+1);
        playerUnit.raidTierOverride = simulatedTier;

        await messageCallback(`Simulating ${playerUnit.name} to ${getTierString(DIFFICULTY_MINIMUMS, simulatedTier)}...`);

        let bestTeamCombination = await getRaidEstimate(playerData, raid, effort);

        let mockUnit = recommendationsUtils.createMockUnit(
            swapiUtils.getUnitDefId(raidUnit),
            7,
            raidCommon[raid].DIFFICULTY_MINIMUMS[simulatedTier].minGearLevel,
            raidCommon[raid].DIFFICULTY_MINIMUMS[simulatedTier].minRelicLevel
            );
        let relicUpgradeCost = calcRelicUpgradeCostInCrystals(playerData, [mockUnit]);
        let cpp = relicUpgradeCost/(bestTeamCombination.points - currentEstimate.points);
        if (isNaN(cpp) || !isFinite(cpp)) cpp = 0;

        simResults.push({
            unitToUpgrade: playerUnit,
            tierToUpgrade: simulatedTier,
            estimatedScore: bestTeamCombination.points,
            diffFromCurrentScore: bestTeamCombination.points - currentEstimate.points,
            upgradeCostInCrystals: relicUpgradeCost,
            crystalsPerPoint: cpp
        })
    
        playerUnit.raidTierOverride = null;
    }
    
    let embeds = [];
    if (defIdsToExclude && defIdsToExclude.length > 0)
    {
        let excludedEmbed = new EmbedBuilder()
            .setTitle("Excluded Units")
            .setDescription(await getUnitsListMessage(defIdsToExclude));

        embeds.push(excludedEmbed);
    }
    
    if (defIdsToInclude && defIdsToInclude.length > 0)
    {
        let includedEmbed = new EmbedBuilder()
            .setTitle("Included Units")
            .setDescription(await getUnitsListMessage(defIdsToInclude));

        embeds.push(includedEmbed);
    }

    if (simResults.length === 0)
    {
        // no units recommended
        let resultsEmbed = new EmbedBuilder()
            .setTitle("Congrats!")
            .setDescription("You've already upgraded all units to the max tier.");

        embeds.push(resultsEmbed);
        return { embeds };
    }

    await messageCallback(`Simulations complete!`);
    
    if (sort === SORT_TYPE.MAX_POINTS)
    {
        // sort by score descending and tier ascending
        simResults.sort((a, b) => (b.estimatedScore - a.estimatedScore) || (a.simulatedTier - b.simulatedTier));
    }
    else if (sort == SORT_TYPE.CRYSTALS_PER_POINT)
    {
        simResults.sort((a, b) => 
            ((b.diffFromCurrentScore > 0 ? 1 : 0) - (a.diffFromCurrentScore > 0 ? 1 : 0)) ||
            (a.crystalsPerPoint - b.crystalsPerPoint) || (b.estimatedScore - a.estimatedScore));
    }

    if (simResults[0].diffFromCurrentScore > 0 )
    {
        bestUpgrade.estimatedScore = simResults[0].estimatedScore;
        bestUpgrade.tierToUpgrade = simResults[0].tierToUpgrade;
        bestUpgrade.unitToUpgrade = simResults[0].unitToUpgrade;
        bestUpgrade.crystalsPerPoint = simResults[0].crystalsPerPoint;
        bestUpgrade.upgradeCostInCrystals = simResults[0].upgradeCostInCrystals;
        bestUpgrade.diffFromCurrentScore = simResults[0].diffFromCurrentScore;
    }

    if (bestUpgrade.unitToUpgrade == null)
    {
        let resultsEmbed = new EmbedBuilder()
            .setTitle("None Found")
            .setDescription("WookieeBot could not find a recommendation.");

        embeds.push(resultsEmbed);
        return { embeds };
    }

    let tableResults = `
\`\`\`
Unit                    | Upg. | Score      | CPP
----------------------------------------------------
${simResults.map(r => `${discordUtils.fit(r.unitToUpgrade.name, 23)} | ${discordUtils.fit(getTierString(DIFFICULTY_MINIMUMS, r.tierToUpgrade), 4)} | ${discordUtils.fit(r.estimatedScore.toLocaleString("en"), 10)} | ${+(Math.round(r.crystalsPerPoint + "e+2") + "e-2").toLocaleString("en")}`).join("\n")}
\`\`\`
`

    let tierString = "";

    let mins = raidCommon[raid].DIFFICULTY_MINIMUMS[bestUpgrade.tierToUpgrade];
    if (mins.minRelicLevel) tierString = `Relic ${mins.minRelicLevel}`;
    else if (mins.minGearLevel) tierString = `Gear ${mins.minGearLevel}`;

    let embedRec = new EmbedBuilder()
        .setTitle(`Recommended Upgrade for ${swapiUtils.getPlayerName(playerData)}${discordUtils.LTR} (${swapiUtils.getPlayerAllycode(playerData)})`)
        .setDescription(
`Effort: ${effort.shortName}
Current Estimate: ${currentEstimate.points.toLocaleString("en")}

Unit to Upgrade: ${bestUpgrade.unitToUpgrade.name}
Upgrade to: ${tierString}

Estimate after Upgrade: ${bestUpgrade.estimatedScore.toLocaleString('en')}
Estimated Points Gained: ${bestUpgrade.diffFromCurrentScore.toLocaleString('en')}
Crystal Cost Total (relics only): ${bestUpgrade.upgradeCostInCrystals.toLocaleString("en")}
Crystal Cost per Point (relics only): ${+(Math.round(bestUpgrade.crystalsPerPoint + "e+2") + "e-2")}
`).setFooter({ text: "Crystal Calculations include all relic materials and signal data. Current inventory cannot be accounted for at this time." });

    let embedTable = new EmbedBuilder()
        .setTitle(`All Results`)
        .setDescription(tableResults)
        .setFooter({ text: "CPP = Crystals per Point" });

    embeds.push(embedRec);
    embeds.push(embedTable);
    
    return { embeds: embeds, content: null };
}


async function getTeamUpgradeRecommendationsResponse(raid, playerData, effort, sort, tier, defIdsToExclude, defIdsToInclude, messageCallback) {
    let currentEstimate = await getRaidEstimate(playerData, raid, effort);
    let DIFFICULTY_MINIMUMS = raidCommon[raid].DIFFICULTY_MINIMUMS;

    let difficulty = tier ?? raidCommon[raid].MAX_DIFFICULTY;
    let relicTierForRaid = DIFFICULTY_MINIMUMS[difficulty].minRelicLevel;

    let tierString = getTierString(DIFFICULTY_MINIMUMS, difficulty);
    let roster = swapiUtils.getPlayerRoster(playerData);
    let unitsList = await swapiUtils.getUnitsList();
    
    // push each team up to max tier and run the recommendations algorithm
    let bestUpgrade = {
        estimatedScore: currentEstimate.points,
        teamToUpgrade: null,
        upgradeCostInCrystals: 0,
        crystalsPerPoint: 0,
        diffFromCurrentScore: 0
    };

    let teams = (await recommendationsUtils.getRaidTeams(recommendationsUtils.REC_TYPE[raid])).teams;

    let scores = new Array();

    // 1. get estimates for all teams
    // 2. sort teams by how much benefit they provide over current score 
    //    (remove the lowest scoring team and add the estimated points from this team as a shitty estimate)
    // 3. loop through from biggest possible "impact" to lowest. 
    //    Skip all teams where the possible benefit is less than the current max benefit found (break loop when encountering one)

    let currentLowestScoringTeam = currentEstimate.combination?.reduce((prev, cur) => (prev.expectedScore < cur.expectedScore) ? prev : cur)
                                   ?? currentEstimate.fillerTeams?.reduce((prev, cur) => (prev < cur) ? prev : cur)
                                   ?? { expectedScore: 0 };
                                   
    for (let t = 0; t < teams.length; t++)
    {
        // if the team includes any units that are excluded, skip it
        if (defIdsToExclude?.length > 0 && defIdsToExclude.find(ex => teams[t].allUnits.find(u => u === ex))) continue;
        // if team does not include a unit that is included, skip it
        if (defIdsToInclude?.length > 0 && !defIdsToInclude.find(i => teams[t].allUnits.find(u => u === i))) continue;

        // check if the player already has all these units at the higher tier possible. If so, skip
        let shouldConsider = teams[t].allUnits.find(teamUnitDefId => {
            let unitInRoster = roster.find(u => swapiUtils.getUnitDefId(u) === teamUnitDefId);

            if ((swapiUtils.getUnitRelicLevel(unitInRoster) ?? 0) - 2 < relicTierForRaid)
            {
                return true;
            }

            return false;
        })

        // skip the team because all units are at or above the max relic tier for the raid
        if (!shouldConsider) continue;

        let mockTeam = recommendationsUtils.findViableTeam(
            playerData,
            teams[t],
            difficulty,
            DIFFICULTY_MINIMUMS,
            [{ raidTeamId: teams[t].raidTeamId, tier: difficulty }]
        );

        let score = recommendationsUtils.getExpectedScore(
            teams[t], 
            difficulty, // max tier
            DIFFICULTY_MINIMUMS,
            raidCommon[raid].MIN_RECOMMENDED_GEAR,
            mockTeam,
            raidCommon[raid].EXTRA_RELIC_SCALING_FACTOR,
            recommendationsUtils.calculateModScalingFactor(playerData),
            raidCommon[raid].ROUND_FN,
            effort
            );
        
        // don't consider anything that's absolutely going to lower the score
        if (score - currentLowestScoringTeam.expectedScore < 0) continue;

        const upgradeCost = calcRelicUpgradeCostInCrystals(playerData, mockTeam);
        scores.push({
            raidTeam: teams[t],
            expectedScore: score,
            diffFromCurLowest: score - currentLowestScoringTeam.expectedScore,
            upgradeCostInCrystals: upgradeCost
        });
    }

    // loop through scores and do the recommendation calcs
    let simResults = [];
    for (let t = 0; t < scores.length; t++)
    {
        let team = scores[t].raidTeam;

        await messageCallback(`Simulating **${team.allUnits.map(u => unitsList.find(unit => swapiUtils.getUnitDefId(unit) == u).name).join(", ")}** to ${tierString}...`);

        let bestTeamCombination = await getRaidEstimate(playerData, raid, effort, [{ raidTeamId: team.raidTeamId, tier: difficulty }]);
        let cpp = scores[t].upgradeCostInCrystals/(bestTeamCombination.points - currentEstimate.points);
        if (isNaN(cpp) || !isFinite(cpp)) cpp = 0;

        simResults.push({
            teamToUpgrade: team,
            estimatedScore: bestTeamCombination.points,
            diffFromCurrentScore: bestTeamCombination.points - currentEstimate.points,
            upgradeCostInCrystals: scores[t].upgradeCostInCrystals,
            crystalsPerPoint: cpp
        });

    }

    let embeds = [];
    if (defIdsToExclude && defIdsToExclude.length > 0)
    {
        let excludedEmbed = new EmbedBuilder()
            .setTitle("Excluded Units")
            .setDescription(await getUnitsListMessage(defIdsToExclude));

        embeds.push(excludedEmbed);
    }

    if (defIdsToInclude && defIdsToInclude.length > 0)
    {
        let includedEmbed = new EmbedBuilder()
            .setTitle("Included Units")
            .setDescription(await getUnitsListMessage(defIdsToInclude));

        embeds.push(includedEmbed);
    }

    if (simResults.length === 0)
    {
        // no teams recommended
        let resultsEmbed = new EmbedBuilder()
            .setTitle("Congrats!")
            .setDescription(`You've already upgraded all the teams known to WookieeBot to ${tierString} or higher.`);

        embeds.push(resultsEmbed);
        return { embeds: embeds };
    }

    await messageCallback(`Simulations complete!`);
    
    if (sort === SORT_TYPE.MAX_POINTS)
    {
        // sort by score descending
        simResults.sort((a, b) => b.estimatedScore - a.estimatedScore);
    }
    else if (sort === SORT_TYPE.CRYSTALS_PER_POINT)
    {
        simResults.sort((a, b) => 
            ((b.diffFromCurrentScore > 0 ? 1 : 0) - (a.diffFromCurrentScore > 0 ? 1 : 0)) ||
            (a.crystalsPerPoint - b.crystalsPerPoint) || (b.estimatedScore - a.estimatedScore));
    }

    simResults = _.uniqWith(simResults, (arrVal, othVal) => { return arrVal.teamToUpgrade.allUnits.length == othVal.teamToUpgrade.allUnits.length && _.isEmpty(_.xor(arrVal.teamToUpgrade.allUnits, othVal.teamToUpgrade.allUnits)) })

    if (simResults[0].diffFromCurrentScore > 0)
    {
        bestUpgrade.estimatedScore = simResults[0].estimatedScore;
        bestUpgrade.diffFromCurrentScore = simResults[0].diffFromCurrentScore;
        bestUpgrade.teamToUpgrade = simResults[0].teamToUpgrade;
        bestUpgrade.upgradeCostInCrystals = simResults[0].upgradeCostInCrystals;
        bestUpgrade.crystalsPerPoint = simResults[0].crystalsPerPoint;
    }
    if (bestUpgrade.teamToUpgrade == null)
    {
        let resultsEmbed = new EmbedBuilder()
            .setTitle("None Found")
            .setDescription("WookieeBot could not find a team upgrade recommendation.");

        embeds.push(resultsEmbed);
        return { embeds: embeds };
    }

    let topTeams = simResults.slice(0, 10);
    let tableResults = `
${topTeams.map((r, i) => `${i+1}. ${r.teamToUpgrade.allUnits.map(u => unitsList.find(unit => swapiUtils.getUnitDefId(unit) === u).name).join(", ")}: ${r.estimatedScore.toLocaleString("en")} (${+(Math.round(r.crystalsPerPoint + "e+2") + "e-2").toLocaleString("en")} CPP)`).join("\n")}
`

    let embedRec = new EmbedBuilder()
        .setTitle(`Recommended Team to Upgrade for ${swapiUtils.getPlayerName(playerData)}${discordUtils.LTR} (${swapiUtils.getPlayerAllycode(playerData)})`)
        .setDescription(
`Effort: ${effort.shortName}
Current Estimate: ${currentEstimate.points.toLocaleString("en")}

Team to Upgrade: ${bestUpgrade.teamToUpgrade.allUnits.map(u => unitsList.find(unit => swapiUtils.getUnitDefId(unit) == u).name).join(", ")}
Upgrade to: ${tierString}

Estimate after Upgrade: ${bestUpgrade.estimatedScore.toLocaleString('en')}
Estimated Points Gained: ${bestUpgrade.diffFromCurrentScore.toLocaleString('en')}
Crystal Cost Total (relics only): ${bestUpgrade.upgradeCostInCrystals.toLocaleString("en")}
Crystal Cost per Point (relics only): ${+(Math.round(bestUpgrade.crystalsPerPoint + "e+2") + "e-2")}
`).setFooter({ text: "Crystal Calculations include all relic materials and signal data. Current inventory cannot be accounted for at this time." });

    let embedTable = new EmbedBuilder()
        .setTitle(`Top ${topTeams.length} Simulated Teams`)
        .setDescription(tableResults)
        .setFooter({ text: "CPP = Crystals per Point" });

    embeds.push(embedRec);
    embeds.push(embedTable);

    return { embeds: embeds, content: null };
}

const RELIC_MATERIALS = {
    CARBONITE: { crystalCost: 5, name: "Carbonite Circuit Board" },
    BRONZIUM: { crystalCost: 20, name: "Bronzium Wiring" },
    CHROMIUM: { crystalCost: 40, name: "Chromium Transistor" },
    AURODIUM: { crystalCost: 100, name: "Aurodium Heatsink" },
    ELECTRIUM: { crystalCost: 115, name: "Electrium Conductor" },
    ZINBIDDLE: { crystalCost: 140, name: "Zinbiddle Card" },
    IMPULSEDETECTOR: { crystalCost: 180, name: "Impulse Detector" },
    AEROMAGNIFIER: { crystalCost: 250, name: "Aeromagnifier" },
    GYRDAKEYPAD: { crystalCost: 250, name: "Gyrda Keypad" },
    DROIDBRAIN: { crystalCost: 300, name: "Droid Brain" },

    // signal data
    FRAGMENTEDSIGNALDATA: { crystalCost: 15, name: "Fragmented Signal Data" },
    INCOMPLETESIGNALDATA: { crystalCost: 20, name: "Incomplete Signal Data" },
    FLAWEDSIGNALDATA: { crystalCost: 30, name: "Flawed Signal Data" }
}

const CRYSTALS_PER_MATERIAL = {
    CARBONITE: 5,
    BRONZIUM: 20,
    CHROMIUM: 40,
    AURODIUM: 100,
    ELECTRIUM: 115,
    ZINBIDDLE: 140,
    IMPULSEDETECTOR: 180,
    AEROMAGNIFIER: 250,
    GYRDAKEYPAD: 250,
    DROIDBRAIN: 300,
    FRAGMENTEDSIGNALDATA: 15,
    INCOMPLETESIGNALDATA: 20,
    FLAWEDSIGNALDATA: 30
};

const RELIC_MATERIALS_PER_LEVEL = [
    [], // 0
    [ // 1
        { material: RELIC_MATERIALS.CARBONITE, quantity: 40 }
    ],
    [ // 2
        { material: RELIC_MATERIALS.FRAGMENTEDSIGNALDATA, quantity: 15 },
        { material: RELIC_MATERIALS.CARBONITE, quantity: 30 },
        { material: RELIC_MATERIALS.BRONZIUM, quantity: 40 }
    ],
    [ // 3
        { material: RELIC_MATERIALS.FRAGMENTEDSIGNALDATA, quantity: 20 },
        { material: RELIC_MATERIALS.INCOMPLETESIGNALDATA, quantity: 15 },
        { material: RELIC_MATERIALS.CARBONITE, quantity: 30 },
        { material: RELIC_MATERIALS.BRONZIUM, quantity: 40 },
        { material: RELIC_MATERIALS.CHROMIUM, quantity: 20 }
    ],
    [ // 4
        { material: RELIC_MATERIALS.FRAGMENTEDSIGNALDATA, quantity: 20 },
        { material: RELIC_MATERIALS.INCOMPLETESIGNALDATA, quantity: 25 },
        { material: RELIC_MATERIALS.CARBONITE, quantity: 30 },
        { material: RELIC_MATERIALS.BRONZIUM, quantity: 40 },
        { material: RELIC_MATERIALS.CHROMIUM, quantity: 40 }
    ],
    [ // 5
        { material: RELIC_MATERIALS.FRAGMENTEDSIGNALDATA, quantity: 20 },
        { material: RELIC_MATERIALS.INCOMPLETESIGNALDATA, quantity: 25 },
        { material: RELIC_MATERIALS.FLAWEDSIGNALDATA, quantity: 15 },
        { material: RELIC_MATERIALS.CARBONITE, quantity: 30 },
        { material: RELIC_MATERIALS.BRONZIUM, quantity: 40 },
        { material: RELIC_MATERIALS.CHROMIUM, quantity: 30 },
        { material: RELIC_MATERIALS.AURODIUM, quantity: 20 }
    ],
    [ // 6
        { material: RELIC_MATERIALS.FRAGMENTEDSIGNALDATA, quantity: 20 },
        { material: RELIC_MATERIALS.INCOMPLETESIGNALDATA, quantity: 25 },
        { material: RELIC_MATERIALS.FLAWEDSIGNALDATA, quantity: 25 },
        { material: RELIC_MATERIALS.CARBONITE, quantity: 20 },
        { material: RELIC_MATERIALS.BRONZIUM, quantity: 30 },
        { material: RELIC_MATERIALS.CHROMIUM, quantity: 30 },
        { material: RELIC_MATERIALS.AURODIUM, quantity: 20 },
        { material: RELIC_MATERIALS.ELECTRIUM, quantity: 20 }
    ],
    [ // 7
        { material: RELIC_MATERIALS.FRAGMENTEDSIGNALDATA, quantity: 20 },
        { material: RELIC_MATERIALS.INCOMPLETESIGNALDATA, quantity: 25 },
        { material: RELIC_MATERIALS.FLAWEDSIGNALDATA, quantity: 35 },
        { material: RELIC_MATERIALS.CARBONITE, quantity: 20 },
        { material: RELIC_MATERIALS.BRONZIUM, quantity: 30 },
        { material: RELIC_MATERIALS.CHROMIUM, quantity: 20 },
        { material: RELIC_MATERIALS.AURODIUM, quantity: 20 },
        { material: RELIC_MATERIALS.ELECTRIUM, quantity: 20 },
        { material: RELIC_MATERIALS.ZINBIDDLE, quantity: 10 }
    ],
    [ // 8
        { material: RELIC_MATERIALS.FRAGMENTEDSIGNALDATA, quantity: 20 },
        { material: RELIC_MATERIALS.INCOMPLETESIGNALDATA, quantity: 25 },
        { material: RELIC_MATERIALS.FLAWEDSIGNALDATA, quantity: 45 },
        { material: RELIC_MATERIALS.CHROMIUM, quantity: 20 },
        { material: RELIC_MATERIALS.AURODIUM, quantity: 20 },
        { material: RELIC_MATERIALS.ELECTRIUM, quantity: 20 },
        { material: RELIC_MATERIALS.ZINBIDDLE, quantity: 20 },
        { material: RELIC_MATERIALS.IMPULSEDETECTOR, quantity: 20 },
        { material: RELIC_MATERIALS.AEROMAGNIFIER, quantity: 20 }
    ],
    [ // 9
        { material: RELIC_MATERIALS.FRAGMENTEDSIGNALDATA, quantity: 30 },
        { material: RELIC_MATERIALS.INCOMPLETESIGNALDATA, quantity: 30 },
        { material: RELIC_MATERIALS.FLAWEDSIGNALDATA, quantity: 55 },
        { material: RELIC_MATERIALS.CHROMIUM, quantity: 20 },
        { material: RELIC_MATERIALS.AURODIUM, quantity: 20 },
        { material: RELIC_MATERIALS.ELECTRIUM, quantity: 20 },
        { material: RELIC_MATERIALS.ZINBIDDLE, quantity: 20 },
        { material: RELIC_MATERIALS.IMPULSEDETECTOR, quantity: 20 },
        { material: RELIC_MATERIALS.AEROMAGNIFIER, quantity: 20 },
        { material: RELIC_MATERIALS.GYRDAKEYPAD, quantity: 20 },
        { material: RELIC_MATERIALS.DROIDBRAIN, quantity: 20 }
    ]
]

function calcRelicUpgradeCostInCrystals(playerData, team)
{
    let cost = 0;
    for (let u of team)
    {
        let relicTarget = Math.max(0, swapiUtils.getUnitRelicLevel(u) - 2);
        let playerUnit = swapiUtils.getPlayerRoster(playerData).find(unit => swapiUtils.getUnitDefId(unit) === swapiUtils.getUnitDefId(u));
        
        let relicCurrent = Math.max(0, playerUnit ? swapiUtils.getUnitRelicLevel(playerUnit) - 2 : 0);

        for (let l = relicCurrent; l <= relicTarget; l++)
        {
            for (let req of RELIC_MATERIALS_PER_LEVEL[l])
            {
                cost += req.material.crystalCost * req.quantity;
            }
        }
    }

    return cost;
}

async function getRaidEstimate(playerData, raid, effort, teamsToSimulate = null)
{
    return (await recommendationsUtils.generateRaidRecommendations(playerData, 
        {
            DIFFICULTY_MINIMUMS: raidCommon[raid].DIFFICULTY_MINIMUMS,
            MAX_ATTEMPTS: raidCommon[raid].MAX_ATTEMPTS,
            MAX_TOTAL_SCORE: raidCommon[raid].MAX_TOTAL_SCORE,
            REC_TYPE: recommendationsUtils.REC_TYPE[raid],
            MIN_RECOMMENDED_GEAR: raidCommon[raid].MIN_RECOMMENDED_GEAR,
            ROUND_FN: raidCommon[raid].ROUND_FN,
            FILLER_TEAM: raidCommon[raid].FILLER_TEAM,
            effort: effort,
            extraRelicScalingFactor: raidCommon[raid].EXTRA_RELIC_SCALING_FACTOR,
            teamsToSimulate: teamsToSimulate
        }));
}

async function getUnitsListMessage(defIds)
{
    if (!defIds) return "";
    let unitsList = await swapiUtils.getUnitsList();

    let names = [];
    for (let d of defIds)
    {
        let u = unitsList.find(u => swapiUtils.getUnitDefId(u) === d);
        if (!u) continue;

        names.push(u.name);
    }

    names.sort();

    return `* ${names.join("\n* ")}`;
}

function getTierString(DIFFICULTY_MINIMUMS, tier)
{
    return DIFFICULTY_MINIMUMS[tier].minRelicLevel ? `R${DIFFICULTY_MINIMUMS[tier].minRelicLevel}` : `G${DIFFICULTY_MINIMUMS[tier].minGearLevel}`;
}

async function raidRecJobHandler(jobBatch)
{
    const job = jobBatch[0];
    let channel, editMessage;
    try 
    {
        channel = CLIENT.channels.cache.get(job.data.channelId);
        editMessage = (await channel.messages.fetch({ message: job.data.messageId }));
    }
    catch (e)
    {
        let discordUser = await CLIENT.users.fetch(job.data.userId);
        await discordUser.send(
            {
                content: `WookieeBot may be missing permissions in channel <#${job.data.channelId}>. Please verify with \`/check-permissions\``
            }
        );

        return;
    }
    
    try 
    {
        await editMessage.edit({ content: "Processing your request now...", components: [], files: [], embeds: [] });

        let playerData = await swapiUtils.getPlayer(job.data.allycode);

        editMessage.edit({ content: "Simulating upgrades..." });
    
        let defIdsToExclude = await unitsUtils.parseUnitsStringAsDefIds(job.data.exclude);
        let defIdsToInclude = await unitsUtils.parseUnitsStringAsDefIds(job.data.include);
        let effort = recommendationsUtils.mapValueToEffort(job.data.effort);
        let response;
        switch (job.data.type)
        {
            case RECOMMENDATION_TYPE.UNIT.value:
                response = await getUnitUpgradeRecommendationsResponse(
                    job.data.raid,
                    playerData,
                    effort, 
                    job.data.sort,
                    job.data.tier,
                    defIdsToExclude,
                    defIdsToInclude,
                    async (message) => { await editMessage.edit(message); }
                );
                break;
            case RECOMMENDATION_TYPE.TEAM.value:
                response = await getTeamUpgradeRecommendationsResponse(
                    job.data.raid,
                    playerData,
                    effort, 
                    job.data.sort,
                    job.data.tier,
                    defIdsToExclude,
                    defIdsToInclude,
                    async (message) => { await editMessage.edit(message); }
                );
                break;
        }
        
        response.content = `<@${job.data.userId}>`; 
        await editMessage.reply(response);
    }
    catch (e)
    {
        logger.error(formatError(e));
        await editMessage.reply(discordUtils.createErrorEmbed("Error", "An error occurred processing the request. Please try again or connect with WookieeBot developers for help."));
    }
}