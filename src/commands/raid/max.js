const { SlashCommandSubcommandBuilder } = require('@discordjs/builders');
const database = require("../../database");
const recommendationsUtils = require("../../utils/recommendations");
const playerUtils = require("../../utils/player");
const guildUtils = require("../../utils/guild");
const swapiUtils = require("../../utils/swapi");
const unitsUtils = require("../../utils/units");
const { EmbedBuilder, AttachmentBuilder, ActionRowBuilder, ButtonBuilder, ButtonStyle, StringSelectMenuBuilder, MessageFlags } = require('discord.js');
const { createCanvas, Image, loadImage } = require("canvas");
const { Buffer } = require ("buffer")
const constants = require("../../utils/constants");
const raidCommon = require('./raidCommon');
const discordUtils = require("../../utils/discord");

module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("max")
        .setDescription("Get recommendations on raid teams")
        .addStringOption(o => 
            o.setName("allycode")
            .setDescription("Ally Code to find recommendations for")
            .setRequired(false)
            .setAutocomplete(true))
        .addStringOption(o =>
            o.setName("raid")
            .setDescription("The raid to check (Default: featured raid)")
            .setRequired(false)
            .addChoices(
                { name: raidCommon.NABOO.description, value: raidCommon.NABOO.KEY },
                { name: raidCommon.ENDOR.description, value: raidCommon.ENDOR.KEY },
                { name: raidCommon.KRAYT.description, value: raidCommon.KRAYT.KEY }
            ))
        .addIntegerOption(o =>
            o.setName("effort")
            .setDescription("Effort Level (Default: Guild Default or Medium)")
            .addChoices(
                { name: recommendationsUtils.EFFORT.LOW.name, value: recommendationsUtils.EFFORT.LOW.value },
                { name: recommendationsUtils.EFFORT.MEDIUM.name, value: recommendationsUtils.EFFORT.MEDIUM.value },
                { name: recommendationsUtils.EFFORT.HIGH.name, value: recommendationsUtils.EFFORT.HIGH.value }
            )
            .setRequired(false))
        .addBooleanOption(o =>
            o.setName("guild")
            .setDescription("Add dropdowns for guild members.")
            .setRequired(false)
        )
        .addUserOption(o =>
            o.setName("user")
            .setDescription("Discord user")
            .setRequired(false)
        )
        .addIntegerOption(o =>
            o.setName("alt")
            .setDescription("Alt account (/user me)")
            .setRequired(false)
            .setAutocomplete(true)),
    interact: async function(interaction)
    {
        let raid = interaction.options.getString("raid") ?? raidCommon.DEFAULT_RAID.KEY;
        let allycode = interaction.options.getString("allycode");
        let effort = recommendationsUtils.mapValueToEffort(interaction.options.getInteger("effort"));
        let alt = interaction.options.getInteger("alt");
        let user = interaction.options.getUser("user");
        const showGuildMembers = interaction.options.getBoolean("guild");

        let { botUser, guildId, error } = await playerUtils.authorize(interaction);
        
        if (user)
        {
            botUser = await playerUtils.getUser(user.id);
        }

        if (allycode) allycode = playerUtils.cleanAndVerifyStringAsAllyCode(allycode);
        allycode = playerUtils.chooseBestAllycode(botUser, guildId, allycode, alt);

        if (!allycode)
        {
            await interaction.editReply({ content: "Please provide an allycode: `/raid max allycode:123456789` or `/register`" });
            return;
        }

        
        let playerData = await swapiUtils.getPlayer(allycode);

        // // temp for beta testing
        // if (interaction.user.id !== "276185061970149377" && playerData.guildId !== 'bmNztIGKQnCvyXNBwZ12NA')
        // {
        //     await interaction.editReply({ content: "`/raid max` recommends teams to maximize your score for the raid based on your roster. It is currently in closed beta. Please stay tuned."})
        //     return;
        // }

        if (!playerData)
        {
            await interaction.editReply({ content: `No player found with allycode **${allycode}**.`})
            return;
        }

        if (!effort)
        {
            effort = recommendationsUtils.mapValueToEffort((await guildUtils.getGuildDefaultRaidEffort({ allycode: allycode }))) ?? recommendationsUtils.EFFORT.MEDIUM;
        }

        let response = await getRecommendationsResponse(
            raid,
            playerData,
            effort
        );

        if (showGuildMembers)
        {
            await addGuildMembersDropDowns(
                response,
                swapiUtils.getPlayerGuildId(playerData),
                raid,
                effort
            )
        }
        
        await interaction.editReply(response);
    },
    autocomplete: async function(interaction) {

		const focusedOption = interaction.options.getFocused(true);

        let choices;
        if (focusedOption.name == "alt")
        {
            choices = await playerUtils.getAltAutocompleteOptions(focusedOption.value, interaction.user.id);
        }
        else if (focusedOption.name == "allycode")
        {
            // autocomplete for guild members
            choices = await playerUtils.getGuildPlayersAutocompleteOptions(focusedOption.value, interaction.user.id);        
        }
        
        await interaction.respond(choices);
    },
    onSelect: async function(interaction)
    {
        await interaction.deferUpdate();

        let regex = /raid:m:guild\d:(.+):(\d)/;
        let matches = regex.exec(interaction.customId);
        
        if (!matches || matches.length != 3) {
            // something went wrong
            logger.error("raid:max:onSelect interaction.customId doesn't match regex. customId is: " + interaction.customId);
            await interaction.followUp({ content: "Something went wrong, unfortunately. Please contact the developer." });
            return;
        }

        const raid = matches[1];
        let effort = recommendationsUtils.mapValueToEffort(parseInt(matches[2]));
        const allycode = interaction.values[0];
        const playerName = await playerUtils.getPlayerNameByAllycode(allycode);

        await interaction.editReply({ content: `Running \`/raid max\` for **${playerName}**...`, embeds: [], components: [], files: []})

        let playerData = await swapiUtils.getPlayer(allycode);

        if (!playerData)
        {
            await interaction.editReply({ content: `No player found with allycode **${allycode}**.`})
            return;
        }
        

        if (!effort)
        {
            effort = recommendationsUtils.mapValueToEffort((await guildUtils.getGuildDefaultRaidEffort({ allycode: allycode }))) ?? recommendationsUtils.EFFORT.MEDIUM;
        }

        let response = await getRecommendationsResponse(
            raid,
            playerData,
            effort
        );

        await addGuildMembersDropDowns(
            response,
            swapiUtils.getPlayerGuildId(playerData),
            raid,
            effort
        );

        response.content = null;
        
        await interaction.editReply(response);
    },
    handleButton: async function(interaction) {
        
        await interaction.deferReply({ flags: MessageFlags.Ephemeral});
        let regex = /raid:m:(.+):(.+)/;
        let matches = regex.exec(interaction.customId);
        
        if (!matches || matches.length != 3) {
            // something went wrong
            logger.error("raid:max:handleButton interaction.customId doesn't match regex. customId is: " + interaction.customId);
            await interaction.followUp({ content: "Something went wrong, unfortunately. Please contact the developer." });
            return;
        }

        let raidKey = matches[1];
        let raidTeamDefinitions = await recommendationsUtils.getRaidTeams(raidKey);
        let raidTeamsAndTiers = matches[2].split(",").map(t => {
            let slashIx = t.indexOf("/");
            let raidTeamId = parseInt(t.substring(0, slashIx));

            return { 
                tier: parseInt(t.substring(slashIx+1)),
                teamDefinition: raidTeamDefinitions.teams.find(t => t.raidTeamId === raidTeamId)
            };
        });

        let embed = new EmbedBuilder()
            .setColor("Blue")
            .setTitle("Team Notes");
        
        let allUnits = await swapiUtils.getUnitsList();

        for (let team of raidTeamsAndTiers)
        {
            let leaderUnit = allUnits.find(u => swapiUtils.getUnitDefId(u) === team.teamDefinition.leaderUnitId);

            let tierNote = team.teamDefinition.tierData ? team.teamDefinition.tierData[team.tier].note : null;
            if (tierNote != null && tierNote.trim().length == 0) tierNote = null;

            let field = {
                name: leaderUnit.name,
                value: tierNote ?? "No gameplay notes yet",
                inline: false
            };
            
            embed.addFields(field);
        }

        embed.setFooter({ text: "Derived scores are extrapolated from actual team performance at other tiers/effort levels. Please help me fill in the blanks on the WookieeTools server: https://discord.com/invite/KcHzz2v"})

        await interaction.editReply({ embeds: [embed] });
    }
}

async function addGuildMembersDropDowns(response, guildId, raid, effort)
{
    
    let membersActionRows = [];

    const guildData = await guildUtils.getGuildDataFromAPI({ comlinkGuildId: guildId }, { useCache: false });
    const guildRoster = guildUtils.getGuildRoster(guildData);
    let options = guildRoster.map(m => { return { label: `${swapiUtils.getPlayerName(m)} (${swapiUtils.getPlayerAllycode(m)})`, value: swapiUtils.getPlayerAllycode(m) }})
    options.sort((a, b) => a.label.localeCompare(b.label));

    for (let m = 0; m < options.length; m += discordUtils.MAX_DISCORD_DROPDOWN_ITEMS)
    {
        let slicedOptions = options.slice(m, m + discordUtils.MAX_DISCORD_DROPDOWN_ITEMS);
        let row = new ActionRowBuilder()
            .addComponents(
                new StringSelectMenuBuilder()
                    .setCustomId(`raid:m:guild${Math.floor(m/discordUtils.MAX_DISCORD_DROPDOWN_ITEMS)}:${raid}:${effort.value}`)
                    .setMaxValues(1)
                    .setPlaceholder(`Choose a guild member (${slicedOptions[0].label[0].toUpperCase()} - ${slicedOptions[slicedOptions.length-1].label[0].toUpperCase()})`)
                    .addOptions(slicedOptions)
            );

        membersActionRows.push(row);
    }

    if (response.components?.length > 0) response.components = response.components.concat(membersActionRows);
    else response.components = membersActionRows;

}

async function getRecommendationsResponse(raid, playerData, effort) {
    let bestTeamCombination = await recommendationsUtils.generateRaidRecommendations(playerData, 
        {
            DIFFICULTY_MINIMUMS: raidCommon[raid].DIFFICULTY_MINIMUMS,
            MAX_ATTEMPTS: raidCommon[raid].MAX_ATTEMPTS,
            MAX_TOTAL_SCORE: raidCommon[raid].MAX_TOTAL_SCORE,
            REC_TYPE: recommendationsUtils.REC_TYPE[raid],
            MIN_RECOMMENDED_GEAR: raidCommon[raid].MIN_RECOMMENDED_GEAR,
            ROUND_FN: raidCommon[raid].ROUND_FN,
            FILLER_TEAM: raidCommon[raid].FILLER_TEAM,
            effort: effort,
            extraRelicScalingFactor: raidCommon[raid].EXTRA_RELIC_SCALING_FACTOR
        });

    let lastScore;
    
    let comlinkGuildId = swapiUtils.getPlayerGuildId(playerData);
    if (comlinkGuildId) // make sure player is in a guild
    {
        let guildData = await guildUtils.getGuildDataFromAPI({ comlinkGuildId: swapiUtils.getPlayerGuildId(playerData) }, { useCache: true, includeRecentGuildActivityInfo: true });
            
        let raidaidData = raidCommon[raid].findRaid(guildData);
        lastScore = raidaidData?.raidMember.find(pr => pr.playerId === playerData.playerId)?.memberProgress;
        if (lastScore) lastScore = parseInt(lastScore);
    }

    let embed = new EmbedBuilder()
        .setTitle(`Teams to Run for ${swapiUtils.getPlayerName(playerData)}${discordUtils.LTR} (${swapiUtils.getPlayerAllycode(playerData)})`)
        .setDescription(
`Raid: ${raidCommon[raid].description}
Effort: ${effort.shortName}
Estimated Score: ${bestTeamCombination.points.toLocaleString('en')}
Last Actual Score: ${lastScore ? lastScore.toLocaleString("en") : "Unknown"}
`);

    let canvas = await createTeamsCanvas(playerData, bestTeamCombination, lastScore, effort);

    let fileName = `${raid.toLowerCase()}-teams-${swapiUtils.getPlayerAllycode(playerData)}.png`
    const file = new AttachmentBuilder(canvas.toBuffer("image/png"), { name: fileName });
    embed.setImage("attachment://" + fileName);

    
    let files = [file];
    let embeds = [embed];

    let response = { embeds: embeds, files: files };

    if (bestTeamCombination.combination != null && bestTeamCombination.combination.length > 0)
    {
        let buttonRow = new ActionRowBuilder()
                .addComponents(
                    new ButtonBuilder()
                        .setCustomId(`raid:m:${recommendationsUtils.REC_TYPE[raid]}:${bestTeamCombination.combination.map(c => `${c.teamDefinition.raidTeamId}/${c.tier}`).join(",")}`)
                        .setLabel("Show Team Notes")
                        .setStyle(ButtonStyle.Success)
                );
        response.components = [buttonRow];
    }

    return response;
}

const PORTRAIT_WIDTH = 70;
const PORTRAIT_HEIGHT = 70;
const PADDING = 10;
const ROW_HEIGHT = 120;
const HEADER_HEIGHT = ROW_HEIGHT - PORTRAIT_HEIGHT - PADDING;

async function createTeamsCanvas(playerData, bestTeamCombination, lastActualScore, effort)
{
    let teams = bestTeamCombination.combination;
    const width = 5*(PORTRAIT_WIDTH + PADDING) + PADDING + (effort ? PADDING : 0);
    const height = ((teams?.length ?? 0) + 1.2)*ROW_HEIGHT + (bestTeamCombination.fillerScore ? ROW_HEIGHT/2 : 0);

    const canvas = createCanvas(width, height);
    const context = canvas.getContext('2d');

    context.fillStyle = "rgb(0, 0, 0)";
    context.fillRect(0, 0, width, height);
    
    context.font = `20pt "Noto Sans","Noto Sans SC","Noto Sans Armenian","Noto Sans Thai","Noto Sans Arabic","Noto Sans Korean"`
    context.textAlign = "left";
    context.textBaseline = "middle";
    context.fillStyle = "#fff";
    context.fillText(`${swapiUtils.getPlayerName(playerData)} (${swapiUtils.getPlayerAllycode(playerData)}) (${effort.shortName})`,
        PADDING, ROW_HEIGHT/4, width - PADDING*2);

    if (teams) {       
        for (let teamIndex = 0; teamIndex < teams.length; teamIndex++)
        {
            let team = teams[teamIndex];

            let omiUnits = [];        
            if (team.teamDefinition.omicronsRequired?.length > 0)
            {
                // figure out if the unit has the ability
                let abilitiesList = await swapiUtils.getAbilitiesList();
                for (let oId of team.teamDefinition.omicronsRequired)
                {
                    let ability = abilitiesList.find(a => a.base_id === oId);
                    omiUnits.push(ability.character_base_id);
                }
            }

            let zetaUnits = [];        
            if (team.teamDefinition.zetasRequired?.length > 0)
            {
                // figure out if the unit has the ability
                let abilitiesList = await swapiUtils.getAbilitiesList();
                for (let zId of team.teamDefinition.zetasRequired)
                {
                    let ability = abilitiesList.find(a => a.base_id === zId);
                    zetaUnits.push(ability.character_base_id);
                }
            }

            const extrapolated = team.teamDefinition.scoreByTier[team.tier][effort.value].dataPointsCount === 0;
            context.font = "15pt Noto Sans";
            context.textAlign = "left";
            context.textBaseline = "middle";
            context.fillStyle = "#fff";
            context.fillText(`Tier ${team.tier} - Estimate: ${team.expectedScore.toLocaleString("en")}${extrapolated ? " (derived)" : ""}`,
                PADDING,
                (teamIndex + 0.5) * ROW_HEIGHT + HEADER_HEIGHT/2,
                width - PADDING*2
            );

            let curPosY = (teamIndex + 0.5) * ROW_HEIGHT + HEADER_HEIGHT;

            for (let unitIx = 0; unitIx < team.teamDefinition.allUnits.length; unitIx++)
            {
                let unitDefId = team.teamDefinition.allUnits[unitIx];
                
                let portrait = await unitsUtils.getPortraitForUnitByDefId(unitDefId);
                let img = portrait ?? './src/img/no-portrait.png';
                context.fillStyle = (unitIx == 0) ? "gold" : "black";
                context.fillRect(PADDING + unitIx * (PORTRAIT_WIDTH + PADDING), curPosY, PORTRAIT_WIDTH+2, PORTRAIT_HEIGHT+2);

                const unitIconImg = await loadImage(img);
                context.drawImage(unitIconImg, 
                    PADDING + unitIx*(PORTRAIT_WIDTH + PADDING) + 1, 
                    curPosY + 1, 
                    PORTRAIT_WIDTH, PORTRAIT_HEIGHT);
                    
                if (omiUnits.indexOf(unitDefId) != -1)
                {
                    const omicronImg = await loadImage(constants.OMICRON_IMG_URL);
                    context.drawImage(omicronImg, 
                        PADDING + unitIx*(PORTRAIT_WIDTH + PADDING) + 45, 
                        curPosY + PORTRAIT_HEIGHT - 20);
                }
                    
                if (zetaUnits.indexOf(unitDefId) != -1)
                {
                    const zetaImg = await loadImage(constants.ZETA_IMG_URL);
                    context.drawImage(zetaImg, 
                        PADDING + unitIx*(PORTRAIT_WIDTH + PADDING) + 45, 
                        curPosY + PORTRAIT_HEIGHT - 20);
                }
            }
        }

        context.fillStyle = "gold";       
        context.fillRect(PADDING, height - ROW_HEIGHT*(bestTeamCombination.fillerScore ? 1.15 : 0.6), width - PADDING*2, 1);
    }
    else
    {
        
        context.font = "15pt Noto Sans";
        context.textAlign = "left";
        context.textBaseline = "middle";
        context.fillStyle = "#fff";
        
        context.fillText(`No known teams.`, PADDING, 0.33 * ROW_HEIGHT + HEADER_HEIGHT/2);
    }

    context.font = "15pt Noto Sans";
    context.textAlign = "left";
    context.textBaseline = "middle";
    context.fillStyle = "#fff";

    if (bestTeamCombination.fillerScore)
    {

        context.fillText(`Total estimated score (no filler): ${(bestTeamCombination.points - bestTeamCombination.fillerScore).toLocaleString("en")}`,
            PADDING, height + 4 - ROW_HEIGHT);
        
        context.fillText(`Unknown filler teams estimate: ${bestTeamCombination.fillerScore.toLocaleString("en")}`,
            PADDING, height + 4 - ROW_HEIGHT*0.75);
    }

    context.fillStyle = "gold";
    context.fillText(`Total estimated score: ${bestTeamCombination.points.toLocaleString("en")}`,
        PADDING, height + 4 - ROW_HEIGHT/2
    );

    context.fillStyle = "white";
    context.fillText(`Last actual score: ${lastActualScore ? lastActualScore.toLocaleString("en") : "Unknown"}`,
        PADDING, height + 4 - ROW_HEIGHT/4
    );

    return canvas;
}