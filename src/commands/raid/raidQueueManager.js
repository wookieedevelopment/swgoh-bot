
const { CLIENT } = require('../../utils/discordClient');
const queueUtils = require("../../utils/queue");
const raidGuild = require("./guild");
const raidRec = require("./rec");

const QUEUE_NAME = "RAID_BULK";

queueUtils.addStartupFunction(setup);

module.exports = {
    QUEUE_NAME: QUEUE_NAME,
    setup: setup
}

function setup(BOSS, getQueueName)
{
    if (CLIENT.shard) BOSS.work(getQueueName(QUEUE_NAME, CLIENT.shard), {newJobCheckIntervalSeconds: 10, batchSize: 1}, raidQueueJobHandler);
}

async function raidQueueJobHandler(jobBatch)
{
    const job = jobBatch[0];

    if (job.data.jobType == "Guild")
    {
        await raidGuild.raidGuildJobHandler(jobBatch);
    }
    else if (job.data.jobType == "Rec")
    {
        await raidRec.raidRecJobHandler(jobBatch);
    }
}