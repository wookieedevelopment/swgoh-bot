const raidReady = require("./raid/ready");
const raidMax = require("./raid/max");
const raidGuild = require("./raid/guild");
const raidRec = require("./raid/rec");
const raidQueueManager = require("./raid/raidQueueManager");
const { SlashCommandBuilder } = require('@discordjs/builders');

module.exports = {
    data: new SlashCommandBuilder()
        .setName("raid")
        .setDescription("Raid Tools")
        .addSubcommand(raidReady.data)
        .addSubcommand(raidMax.data)
        .addSubcommand(raidGuild.data)
        .addSubcommand(raidRec.data),
    interact: async function(interaction)
    {
        let subcommand = interaction.options.getSubcommand();
        switch (subcommand)
        {
            case raidReady.data.name:
                await raidReady.interact(interaction);
                break;
            case raidMax.data.name:
                await raidMax.interact(interaction);
                break;
            case raidGuild.data.name:
                await raidGuild.interact(interaction);
                break;
            case raidRec.data.name:
                await raidRec.interact(interaction);
                break;
        }
    },
    handleButton: async function(interaction)
    {  
        if (interaction.customId.startsWith("raid:m"))
            await raidMax.handleButton(interaction);
    },
    onSelect: async function(interaction)
    {
        if (interaction.customId.startsWith("raid:g:"))
            await raidGuild.onSelect(interaction);
        if (interaction.customId.startsWith("raid:m:"))
            await raidMax.onSelect(interaction);
    },
    autocomplete: async function(interaction)
    {
        let group, subcommand;
        try { subcommand = interaction.options.getSubcommand(); } catch {}
        if (subcommand)
        {
            switch (subcommand) {
                case raidReady.data.name:
                    await raidReady.autocomplete(interaction);
                    break;
                case raidMax.data.name:
                    await raidMax.autocomplete(interaction);
                    break;
                case raidGuild.data.name:
                    await raidGuild.autocomplete(interaction);
                    break;
                case raidRec.data.name:
                    await raidRec.autocomplete(interaction);
                    break;
            }
            return;
        }
    }
}