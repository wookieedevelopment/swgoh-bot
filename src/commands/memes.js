const data = require("../data/memes_data.json");
const { EmbedBuilder } = require("discord.js");
const { SlashCommandBuilder, SlashCommandSubcommandGroupBuilder } = require('@discordjs/builders');

const memeKeys = Object.keys(data);

module.exports = {
    data: new SlashCommandBuilder()
        .setName("meme")
        .setDescription("WookieeBot meme generator")
        .addSubcommand(s => 
            s.setName("send")
            .setDescription("Show a meme")
            .addStringOption(o => 
                o.setName("name")
                .setDescription("Name of the meme.")
                .setRequired(true)
                .setAutocomplete(true)))
        .addSubcommand(s =>
            s.setName("list")
            .setDescription("List possible memes.")),
    name: "memes",
    description: "WookieeBot meme generator",
    interact: async function(interaction)
    {
        let subcommand = interaction.options.getSubcommand();

        if (subcommand == "list")
        {
            let response = this.getList();
            await interaction.editReply(response);
            return;
        }
        
        let name = interaction.options.getString("name").toLowerCase();

        let content = data[name];
        if (!content) {
            await interaction.editReply("No meme found by that name.");
            return;
        }

        await interaction.channel.send(content);
        await interaction.editReply(`Sent **${name}**. Only you can see this message.`);
    },
    autocomplete: async function(interaction)
    {
        const focusedValue = interaction.options.getFocused();
		const filtered = memeKeys.filter(m => m.includes(focusedValue.toLowerCase()));

		await interaction.respond(
			filtered.map(choice => ({ name: choice, value: choice })).slice(0,25)
		);
    },
    check: function(cmd){
        return cmd in data;
    },
    post: async function(message, cmd){
        await message.channel.send(data[cmd]);
    },
    list: async function(message) {
        let response = this.getList();
        await message.channel.send(response);
    },
    getList: function()
    {
        let list = Object.keys(data);

        let embed = new EmbedBuilder()
            .setTitle("Wookieebot Memes")
            .setColor(0xD2691E)
            .setDescription(list.sort().toString().split(',').join("\r\n"));

        return { embeds: [embed] };
    },
    execute: async function(client, message, args) {
        if (args == "list" || args == null) {
            await this.list(message);
            return;
        }
        await message.channel.send("_Command has an invalid parameter_");
    }
}