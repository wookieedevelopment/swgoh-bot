const { EmbedBuilder } = require("discord.js");
const database = require("../database");
const countersAdd = require("./counters/add")
const countersUpdate = require("./counters/update")
const countersDelete = require("./counters/delete")
const countersList = require("./counters/list")
const countersImport = require("./counters/import")
const countersExport = require("./counters/export")
const counterUtils = require("./counters/counterUtils")
const countersHelp = require("./counters/help")
const countersShow = require("./counters/show")
const { SlashCommandBuilder } = require('@discordjs/builders');

module.exports = {
    data: new SlashCommandBuilder()
        .setName("counters")
        .setDescription("Guild-specific counter write-ups")
        .addSubcommand(countersAdd.data)
        .addSubcommand(countersUpdate.data)
        .addSubcommand(countersDelete.data)
        .addSubcommand(countersList.data)
        .addSubcommand(countersHelp.data)
        .addSubcommand(countersShow.data)
        .addSubcommand(countersImport.data)
        .addSubcommand(countersExport.data),
    shouldDefer: function(interaction)
    {
        try {
            let subc = interaction.options.getSubcommand();
            if (subc == "add" || subc == "update") return false;
        } catch {}

        return true;
    },
    isEphemeral: function(subcommandName)
    {
        if (subcommandName == countersImport.data.name || subcommandName == countersExport.data.name) return true;
        return false;
    },
    interact: async function(interaction)
    {
        let subcommand = interaction.options.getSubcommand();
        switch (subcommand)
        {
            case countersShow.data.name:
                await countersShow.interact(interaction);
                break;
            case countersAdd.data.name:
                await countersAdd.interact(interaction);
                break;
            case countersUpdate.data.name:
                await countersUpdate.interact(interaction);
                break;
            case countersList.data.name:
                await countersList.interact(interaction);
                break;
            case countersDelete.data.name:
                await countersDelete.interact(interaction);
                break;
            case countersList.data.name:
                await countersList.interact(interaction);
                break;
            case countersHelp.data.name:
                await countersHelp.interact(interaction);
                break;
            case countersImport.data.name:
                await countersImport.interact(interaction);
                break;
            case countersExport.data.name:
                await countersExport.interact(interaction);
                break;
            
        }
    },
    handleModal: async function(interaction)
    {
        let subcommand = interaction.customId.slice("counters:".length);

        switch (subcommand)
        {
            case countersAdd.data.name:
                await countersAdd.handleModal(interaction);
                break;
            case countersUpdate.data.name:
                await countersUpdate.handleModal(interaction);
                break;
        }
    },
    autocomplete: async function(interaction)
    {
        let subcommand = interaction.options.getSubcommand();
        switch (subcommand)
        {
            case countersShow.data.name:
                await countersShow.autocomplete(interaction);
                break;
            case countersDelete.data.name:
                await countersDelete.autocomplete(interaction);
                break;
            case countersUpdate.data.name:
                await countersUpdate.autocomplete(interaction);
                break;
        }
    },
    findCounter: async function(guildId, counterName)
    {
        let r = await database.db.oneOrNone("SELECT name, short_desc, text FROM guild_writeup WHERE guild_id = $1 AND name = $2 AND type = $3", [guildId, counterName, counterUtils.counterWriteUpType])

        if (!r)
            return null;

        return {
            name: r.name,
            shortDescription: r.short_desc,
            text: r.text
        };
    },
    showCounter: async function(input, counter)
    {
        if (!counter) {
            await input.channel.send("Your guild does not have a counter by that name. Use c.list to see all your counters.");
            return;
        }

        var embedToSend = new EmbedBuilder()
            .setTitle(counter.name)
            .setColor(0x347712)
            .setDescription("**" + counter.shortDescription + "**\r\n\r\n" + counter.text)

        await input.channel.send({ embeds: [embedToSend] });
    }
}