const twHelp = require("./tw/help.js");
const twConfig = require("./tw/config");
const twSendAssignments = require("./tw/sendAssignments")
const twExclude = require("./tw/exclude")
const twInclude = require("./tw/include")
const twTestTeam = require("./tw/testTeam")
const twTeam = require("./tw/team")
const twPlan = require("./tw/plan")
const { SlashCommandBuilder } = require('@discordjs/builders');
const player = require("./tw/player.js");
const showTwPlanZone = require("./tw/showTwPlanZone.js");
const downloadPlan = require("./tw/downloadPlan.js");
const showAssignmentDetails = require("./tw/showAssignmentDetails.js");
const twPlanReport = require("./tw/planReport.js")
const { MessageFlags } = require("discord.js");

module.exports = {
    data: new SlashCommandBuilder()
        .setName("tw")
        .setDescription("TW Defense Planner")
        .addSubcommand(twExclude.data)
        .addSubcommand(twInclude.data)
        .addSubcommandGroup(twTeam.data)
        .addSubcommand(twTestTeam.data)
        .addSubcommandGroup(twPlan.data)
        .addSubcommand(twHelp.data)
        .addSubcommand(twConfig.data)
        .addSubcommand(twSendAssignments.data)
        .addSubcommandGroup(player.data),
    shouldDefer: function(interaction)
    {
        try {
            let subc = interaction.options.getSubcommand();
            if (subc == twConfig.data.name) return false;
        } catch {}

        return true;
    },
    interact: async function(interaction)
    {
        let group, subcommand;
        try { group = interaction.options.getSubcommandGroup(); } catch {}

        if (group)
        {
            switch (group) {
                case twTeam.data.name:
                    await twTeam.interact(interaction, true);
                    break;
                case twPlan.data.name:
                    await twPlan.interact(interaction);
                    break;
                case player.data.name:
                    await player.interact(interaction);
                    break;
            }
            return;
        }

        try { subcommand = interaction.options.getSubcommand(); } catch {}

        switch (subcommand)
        {
            case twInclude.data.name:
                await twInclude.interact(interaction);
                break;
            case twExclude.data.name:
                await twExclude.interact(interaction);
                break;
            case twTestTeam.data.name:
                await twTestTeam.interact(interaction, true);
                break;
            case twHelp.data.name:
                await twHelp.interact(interaction);
                break;
            case twConfig.data.name:
                await twConfig.interact(interaction);
                break;
            case twSendAssignments.data.name:
                await twSendAssignments.interact(interaction);
                break;
            default:
                await interaction.editReply("That is not a valid command. Huac ooac. ('Uh oh.').")
                break;         
        }
    },
    autocomplete: async function(interaction)
    {
        let group, subcommand;
        try { group = interaction.options.getSubcommandGroup(); } catch {}

        if (group)
        {
            switch (group) {
                case twPlan.data.name:
                    await twPlan.autocomplete(interaction);
                    break;
                case twTeam.data.name:
                    await twTeam.autocomplete(interaction, true);
                    break;
                case player.data.name:
                    await player.autocomplete(interaction);
                    break;
            }
            return;
        }

        try { subcommand = interaction.options.getSubcommand(); } catch {}

        switch (subcommand)
        {
            case twInclude.data.name:
                await twInclude.autocomplete(interaction);
                break;
            case twExclude.data.name:
                await twExclude.autocomplete(interaction);
                break;
            case twTestTeam.data.name:
                await twTestTeam.autocomplete(interaction, true);
                break;
        }
    },
    handleModal: async function(interaction)
    {
        let subcommand = interaction.customId.slice("tw:".length);

        switch (subcommand)
        {
            case twConfig.data.name:
                await twConfig.handleModal(interaction);
                break;
        }
    },
    onSelect: async function(interaction)
    {
        if (interaction.customId === "tw:plan:zone")
            await showTwPlanZone.onSelect(interaction);
    },
    handleButton: async function(interaction)
    {  
        if (interaction.customId == "tw_def_assignment_details")
        {
            await interaction.deferReply();
            await showAssignmentDetails.onButtonClick(interaction);
            return;
        }

        if (interaction.customId === "tw_def_json_csv")
        {
            await downloadPlan.onButtonClick(interaction);
            return;
        }

        if (interaction.customId === "tw_def_report")
        {
            await interaction.deferReply();
            await twPlanReport.interact(interaction);
            return;
        }
    }
}