const { ContextMenuCommandBuilder, ApplicationCommandType } = require('discord.js');
const gungan = require("./gungan");

module.exports = {
    data: new ContextMenuCommandBuilder()
            .setName('Translate to Gungan')
            .setType(ApplicationCommandType.Message),
    interact: async function(interaction)
    {
        let oc = await interaction.channel.messages.fetch(interaction.targetId);

        if (oc?.content)
        {
            let translation = gungan.translateToGungan(oc.content);
            await interaction.editReply({ content: translation });
        }
        else
        {
            await interaction.editReply({ content: "Meesa could notsa readen da message. Mabee issa broke-ed?"});
        }
    }
}