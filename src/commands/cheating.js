const playerUtils = require("../utils/player.js");
const discordUtils = require("../utils/discord.js");
const cheatingGuild = require("./cheating/guild.js");
const cheatingUtils = require("./cheating/utils.js");
const { SlashCommandBuilder } = require('@discordjs/builders');
const searchGuild = require("./cheating/search");


module.exports = {
    data: new SlashCommandBuilder()
        .setName("cheating")
        .setDescription("Check players for potential cheating.")
        .addSubcommand(subcommand =>
            subcommand.setName("player")
            .setDescription("Check a player for cheating.")
            .addStringOption(o => 
                o.setName("allycode")
                .setDescription("Ally Code of player to check")
                .setRequired(true))
            .addBooleanOption(o =>
                o.setRequired(false)
                .setName("details")
                .setDescription("Show unit usage details for all events.")))
        .addSubcommand(subcommand =>
            subcommand.setName("guild")
            .setDescription("Check a guild for potential cheaters.")
            .addStringOption(o => 
                o.setName("allycode")
                .setDescription("Ally Code for a player in the guild")
                .setRequired(true)))
        .addSubcommand(searchGuild.data),
    interact: async function(interaction)
    {
        let subcommand = interaction.options.getSubcommand();
        if (subcommand === searchGuild.data.name)
        {
            await searchGuild.interact(interaction);
            return;
        }
        
        let allycode = interaction.options.getString("allycode");
        let details = false || interaction.options.getBoolean("details");

        try { allycode = playerUtils.cleanAndVerifyStringAsAllyCode(allycode); }
        catch (err) { await interaction.editReply({ embeds: [discordUtils.createErrorEmbed("Rwww. ('No.').", err.message.toString())] }); return; }

        switch (subcommand)
        {
            case "player":
                await interaction.editReply("Cheat check underway...")
                let embed = await cheatingUtils.checkOnePlayer(allycode, details) 
                await interaction.followUp({ embeds: [embed] })
                break;
            case "guild":
                await cheatingGuild.interact(interaction, allycode);
                break;
        }

    },
    execute: async function(client, input, args)
    {
        let cmd = discordUtils.splitCommandInput(input);

        if (cmd.length == 1) {
            await this.checkPlayerFromMessage(client, input, args);
            return;
        }

        switch (cmd[1])
        {
            case "guild":
                await cheatingGuild.execute(input, args);
                break;
            default: break;
        }
    },
    checkPlayerFromMessage: async function(client, input, args)
    {
        if (!client || !input) return;
        
        if (!args || !args.length) {
            await input.reply({ embeds: [discordUtils.createErrorEmbed("Rwww. ('No.').", "You didn't give me an ally code!")] });
            return;
        }
        
        var playerId = args[0].replace(/-/g, "");
        var detailed = false;
    
        if (args.length > 1)
        {
            for (var argIndex = 1; argIndex < args.length; argIndex++)
            {
                if (args[argIndex] == "d" || args[argIndex] == "details")
                {
                    detailed = true;
                }
            }
        }
    
        if (playerId.length != 9)
        {
            await input.reply({ embeds: [discordUtils.createErrorEmbed("Rwww. ('No.').", "You gave me an invalid ally code (" + playerId + ")!")] });
            return;
        }
        
        await input.reply("Cheat check underway...");

        let resultsEmbed = await cheatingUtils.checkOnePlayer(playerId, detailed);
        if (resultsEmbed) await input.channel.send({ embeds: [resultsEmbed] });
    }
}