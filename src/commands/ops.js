/* 
 * flow:
 * 1. /ops manage
 *  a. loads all guild player data into memory, organizing it for searching by unit, etc.
 *  b. choose planets multiselect or load ops
 *  c. after choosing planets or loading, show ops. Components will be: 
 *     i. dropdown for preload planet 
 *     ii. dropdown for preload operation
 *     iii. dropdown for save as phase X
 * 2. /ops x -- remove an assignment, autocomplete by unit name, planet, etc.
 * 3. /ops a -- assign an op, autocomplete by unit name, planet, etc.
 * 4. /ops post -- post in channel & send DMs with assignments for a phase
 * 5. /ops config -- change configuration for ops tools (preload strategy, channel, etc.)
 */

const { SlashCommandBuilder } = require('@discordjs/builders');
const opsManage = require("./ops/manage");
const opsHelp = require("./ops/help");
const opsConfig = require("./ops/config");
const opsAssign = require("./ops/assign");
const opsUnassign = require("./ops/unassign");
const opsPost = require("./ops/post");
const opsWhohas = require("./ops/whohas");
const opsRares = require("./ops/rares");
const opsShow = require("./ops/show");
const opsPlayer = require("./ops/player");
const opsPlayerList = require("./ops/player-list");
const opsImport = require("./ops/import");
const opsPlan = require("./ops/plan");
const opsTest = require("./ops/test");

module.exports = {
    data: new SlashCommandBuilder()
        .setName("ops")
        .setDescription("TB operations commands")
        .addSubcommandGroup(opsPlan.data)
        .addSubcommand(opsManage.data)
        .addSubcommand(opsHelp.data)
        .addSubcommand(opsConfig.data)
        .addSubcommand(opsAssign.data)
        .addSubcommand(opsUnassign.data)
        .addSubcommand(opsPost.data)
        .addSubcommand(opsWhohas.data)
        .addSubcommand(opsShow.data)
        .addSubcommand(opsPlayer.data)
        .addSubcommand(opsPlayerList.data)
        .addSubcommand(opsRares.data)
        // .addSubcommand(opsTest.data)
        // .addSubcommand(opsImport.data)
        ,
    interact: async function(interaction)
    {
        let group, subcommand;
        try { group = interaction.options.getSubcommandGroup(); } catch {}

        if (group)
        {
            switch (group) {
                case opsPlan.data.name:
                    await opsPlan.interact(interaction);
                    break;
            }
            return;
        }

        try { subcommand = interaction.options.getSubcommand(); } catch {}
        
        switch (subcommand)
        {
            case opsManage.data.name:
                await opsManage.interact(interaction);
                break;
            case opsHelp.data.name:
                await opsHelp.interact(interaction);
                break;
            case opsConfig.data.name:
                await opsConfig.interact(interaction);
                break;
            case opsAssign.data.name:
                await opsAssign.interact(interaction);
                break;
            case opsUnassign.data.name:
                await opsUnassign.interact(interaction);
                break;
            case opsPost.data.name:
                await opsPost.interact(interaction);
                break;
            case opsWhohas.data.name:
                await opsWhohas.interact(interaction);
                break;
            case opsShow.data.name:
                await opsShow.interact(interaction);
                break;
            case opsRares.data.name:
                await opsRares.interact(interaction);
                break;
            case opsPlayer.data.name:
                await opsPlayer.interact(interaction);
                break;
            case opsPlayerList.data.name:
                await opsPlayerList.interact(interaction);
                break;
            case opsTest.data.name:
                await opsTest.interact(interaction);
                break;
            case opsImport.data.name:
                await opsImport.interact(interaction);
                break;
        }
    },
    handleButton: async function(interaction)
    { 
        if (interaction.customId.startsWith("ops:m")) await opsManage.handleButton(interaction);
        else if (interaction.customId.startsWith("ops:t")) await opsTest.handleButton(interaction);
    },
    onSelect: async function(interaction)
    {
        if (interaction.customId.startsWith("ops:m")) await opsManage.onSelect(interaction);
        else if (interaction.customId.startsWith("ops:t")) await opsTest.onSelect(interaction);
    },
    autocomplete: async function(interaction)
    {
        let group, subcommand;
        try { group = interaction.options.getSubcommandGroup(); } catch {}

        if (group)
        {
            switch (group) {
                case opsPlan.data.name:
                    await opsPlan.autocomplete(interaction);
                    break;
            }
            return;
        }

        try { subcommand = interaction.options.getSubcommand(); } catch {}

        switch (subcommand)
        {
            case opsUnassign.data.name:
                await opsUnassign.autocomplete(interaction);
                break;
            case opsAssign.data.name:
                await opsAssign.autocomplete(interaction);
                break;
            case opsWhohas.data.name:
                await opsWhohas.autocomplete(interaction);
                break;
            case opsShow.data.name:
                await opsShow.autocomplete(interaction);
                break;
            case opsRares.data.name:
                await opsRares.autocomplete(interaction);
                break;
            case opsPlayer.data.name:
                await opsPlayer.autocomplete(interaction);
                break;
        }
    }
}