const discordUtils = require("../utils/discord");
const insults = require("../data/insults.json");
const { SlashCommandBuilder } = require('@discordjs/builders');

module.exports = {
    data: new SlashCommandBuilder()
        .setName("roast")
        .setDescription("Good-natured roasting")
        .addUserOption(o => 
            o.setName("user")
            .setDescription("A discord user to roast.")
            .setRequired(true)),
    interact: async function(interaction) 
    {    
        let roast = insults[Math.floor(Math.random()*insults.length)];
        let user = interaction.options.getUser("user");
        await interaction.editReply(discordUtils.makeIdTaggable(user.id) + ": " + roast);
    }
}