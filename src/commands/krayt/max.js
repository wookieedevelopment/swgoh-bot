const { SlashCommandSubcommandBuilder } = require('@discordjs/builders');
const database = require("../../database");
const recommendationsUtils = require("../../utils/recommendations");
const kraytCommon = require("./kraytCommon");
const playerUtils = require("../../utils/player");
const guildUtils = require("../../utils/guild");
const swapiUtils = require("../../utils/swapi");
const unitsUtils = require("../../utils/units");
const { EmbedBuilder, AttachmentBuilder } = require('discord.js');
const { createCanvas, Image, loadImage } = require("canvas");
const { Buffer } = require ("buffer")
const constants = require("../../utils/constants");
const discordUtils = require("../../utils/discord");


module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("max")
        .setDescription("Get recommendations on raid teams")
        .addStringOption(o => 
            o.setName("allycode")
            .setDescription("Ally Code to find recommendations for")
            .setRequired(false)),
    interact: async function(interaction)
    {
        let allycode = interaction.options.getString("allycode");

        if (allycode) {
            allycode = playerUtils.cleanAndVerifyStringAsAllyCode(allycode);
        } else {
            
            let { botUser, guildId, error } = await playerUtils.authorize(interaction);

            if (guildId)
            {
                allycode = await database.db.oneOrNone(
                    `SELECT allycode FROM guild_players 
                    WHERE guild_id = $1 AND allycode in (select allycode from user_registration where discord_id = $2) 
                    LIMIT 1`, [guildId, interaction.user.id], r => r?.allycode);
            }
            else if (botUser.allycodes?.at(0))
            {
                allycode = botUser.allycodes?.at(0);
            }
        }

        if (!allycode)
        {
            await interaction.editReply({ content: "Please provide an allycode: **/krayt max allycode:123456789**." });
            return;
        }

        // this doesn't use caching... figure out if we need to adjust
        let playerData = await swapiUtils.getPlayer(allycode, constants.CacheTypes.QUERY, false);

        // // temp for beta testing
        // if (interaction.user.id !== "276185061970149377" && playerData.guildId !== 'bmNztIGKQnCvyXNBwZ12NA')
        // {
        //     await interaction.editReply({ content: "`/krayt max` recommends teams to maximize your score for the Krayt raid based on your roster. It is currently in closed beta. Please stay tuned."})
        //     return;
        // }

        if (!playerData)
        {
            await interaction.editReply({ content: `No player found with allycode *${allycode}*.`})
            return;
        }

        let response = await getRecommendationsResponse(
            playerData
        )
        
        await interaction.editReply(response);
    }
}

async function getRecommendationsResponse(playerData) {
    let bestTeamCombination = await recommendationsUtils.generateRaidRecommendations(playerData, 
        {
            DIFFICULTY_MINIMUMS: kraytCommon.DIFFICULTY_MINIMUMS,
            MAX_ATTEMPTS: kraytCommon.MAX_ATTEMPTS,
            MAX_TOTAL_SCORE: kraytCommon.MAX_TOTAL_SCORE,
            REC_TYPE: recommendationsUtils.REC_TYPE.KRAYT,
            MIN_RECOMMENDED_GEAR: kraytCommon.MIN_RECOMMENDED_GEAR
        });

    let teamText, lastScore;
    
    let comlinkGuildId = swapiUtils.getPlayerGuildId(playerData);
    if (comlinkGuildId) // make sure player is in a guild
    {
        let guildData = await guildUtils.getGuildDataFromAPI({ comlinkGuildId: swapiUtils.getPlayerGuildId(playerData) }, { useCache: true, includeRecentGuildActivityInfo: true });
            
        let kraytRaidData = kraytCommon.findKraytRaid(guildData);
        lastScore = kraytRaidData?.raidMember.find(pr => pr.playerId === playerData.playerId)?.memberProgress;
        if (lastScore) lastScore = parseInt(lastScore);
    }


    if (!bestTeamCombination?.combination || bestTeamCombination.combination.length === 0)
    {
        teamText = "No teams known to WookieeBot. Please suggest some to add!\n";
    } else {
        teamText = 
        "```ini\n" + 
        bestTeamCombination.combination.map(t => {
            let teamLeadName = t.playerViableTeam.find(u => swapiUtils.getUnitDefId(u) === t.teamDefinition.leaderUnitId).name;
            let otherTeamMembersNames = t.playerViableTeam.filter(u => swapiUtils.getUnitDefId(u) !== t.teamDefinition.leaderUnitId).map(vtu => vtu.name).join(", ");
    
            let teamString = `${teamLeadName} (L)${(otherTeamMembersNames.length > 0) ? `, ${otherTeamMembersNames}` : ""}`
    
            return `[Tier ${t.tier}] ${teamString}`
        }).join("\n\n") + "\n```";
    }
    teamText += `
${bestTeamCombination.fillerScore ? `Unknown Filler Teams Estimate: ${bestTeamCombination.fillerScore.toLocaleString('en')}` : ""}
Estimated Score: ${bestTeamCombination.points.toLocaleString('en')}
Last Actual Score: ${lastScore ? lastScore.toLocaleString("en") : "Unknown"}`;

    let embed = new EmbedBuilder()
        .setTitle(`Teams to Run for ${swapiUtils.getPlayerName(playerData)}${discordUtils.LTR} (${swapiUtils.getPlayerAllycode(playerData)})`)
        .setDescription(teamText);

    let canvas = await createTeamsCanvas(playerData, bestTeamCombination, lastScore);

    let fileName = `krayt-teams-${swapiUtils.getPlayerAllycode(playerData)}.png`
    const file = new AttachmentBuilder(canvas.toBuffer("image/png"), { name: fileName });
    embed.setImage("attachment://" + fileName);
    
    let files = [file];
    let embeds = [embed];

    return { embeds: embeds, files: files };
}

const PORTRAIT_WIDTH = 70;
const PORTRAIT_HEIGHT = 70;
const PADDING = 10;
const ROW_HEIGHT = 120;
const HEADER_HEIGHT = ROW_HEIGHT - PORTRAIT_HEIGHT - PADDING;

async function createTeamsCanvas(playerData, bestTeamCombination, lastActualScore)
{
    let teams = bestTeamCombination.combination;
    const width = 5*(PORTRAIT_WIDTH + PADDING) + PADDING;
    const height = ((teams?.length ?? 0) + 1.2)*ROW_HEIGHT + (bestTeamCombination.fillerScore ? ROW_HEIGHT/2 : 0);

    const canvas = createCanvas(width, height);
    const context = canvas.getContext('2d');

    context.fillStyle = "rgb(0, 0, 0)";
    context.fillRect(0, 0, width, height);
    
    context.font = `20pt "Noto Sans","Noto Sans SC","Noto Sans Armenian","Noto Sans Thai","Noto Sans Arabic","Noto Sans Korean"`
    context.textAlign = "left";
    context.textBaseline = "middle";
    context.fillStyle = "#fff";
    context.fillText(`${swapiUtils.getPlayerName(playerData)} (${swapiUtils.getPlayerAllycode(playerData)})`,
        PADDING, ROW_HEIGHT/4, width);

    if (teams) {       
        for (let teamIndex = 0; teamIndex < teams.length; teamIndex++)
        {
            let team = teams[teamIndex];

            let omiUnits = [];        
            if (team.teamDefinition.omicronsRequired?.length > 0)
            {
                // figure out if the unit has the ability
                let abilitiesList = await swapiUtils.getAbilitiesList();
                for (let oId of team.teamDefinition.omicronsRequired)
                {
                    let ability = abilitiesList.find(a => a.base_id === oId);
                    omiUnits.push(ability.character_base_id);
                }
            }

            context.font = "15pt Noto Sans";
            context.textAlign = "left";
            context.textBaseline = "middle";
            context.fillStyle = "#fff";
            context.fillText(`Tier ${team.tier} - Estimate: ${team.expectedScore.toLocaleString("en")}`,
                PADDING,
                (teamIndex + 0.5) * ROW_HEIGHT + HEADER_HEIGHT/2
            );

            let curPosY = (teamIndex + 0.5) * ROW_HEIGHT + HEADER_HEIGHT;

            for (let unitIx = 0; unitIx < team.teamDefinition.allUnits.length; unitIx++)
            {
                let unitDefId = team.teamDefinition.allUnits[unitIx];
                
                let portrait = await unitsUtils.getPortraitForUnitByDefId(unitDefId);
                let img = portrait ?? './src/img/no-portrait.png';
                context.fillStyle = (unitIx == 0) ? "gold" : "black";
                context.fillRect(PADDING + unitIx * (PORTRAIT_WIDTH + PADDING), curPosY, PORTRAIT_WIDTH+2, PORTRAIT_HEIGHT+2);

                const unitIconImg = await loadImage(img);
                context.drawImage(unitIconImg, 
                    PADDING + unitIx*(PORTRAIT_WIDTH + PADDING) + 1, 
                    curPosY + 1, 
                    PORTRAIT_WIDTH, PORTRAIT_HEIGHT);
                    
                if (omiUnits.indexOf(unitDefId) != -1)
                {
                    const omicronImg = await loadImage(constants.OMICRON_IMG_URL);
                    context.drawImage(omicronImg, 
                        PADDING + unitIx*(PORTRAIT_WIDTH + PADDING) + 45, 
                        curPosY + PORTRAIT_HEIGHT - 20);
                }
            }
        }

        context.fillStyle = "gold";       
        context.fillRect(PADDING, height - ROW_HEIGHT*(bestTeamCombination.fillerScore ? 1.15 : 0.6), width - PADDING*2, 1);
    }
    else
    {
        
        context.font = "15pt Noto Sans";
        context.textAlign = "left";
        context.textBaseline = "middle";
        context.fillStyle = "#fff";
        
        context.fillText(`No known teams.`, PADDING, 0.33 * ROW_HEIGHT + HEADER_HEIGHT/2);
    }

    context.font = "15pt Noto Sans";
    context.textAlign = "left";
    context.textBaseline = "middle";
    context.fillStyle = "#fff";

    if (bestTeamCombination.fillerScore)
    {

        context.fillText(`Total estimated score (no filler): ${(bestTeamCombination.points - bestTeamCombination.fillerScore).toLocaleString("en")}`,
            PADDING, height + 4 - ROW_HEIGHT);
        
        context.fillText(`Unknown filler teams estimate: ${bestTeamCombination.fillerScore.toLocaleString("en")}`,
            PADDING, height + 4 - ROW_HEIGHT*0.75);
    }

    context.fillStyle = "gold";
    context.fillText(`Total estimated score: ${bestTeamCombination.points.toLocaleString("en")}`,
        PADDING, height + 4 - ROW_HEIGHT/2
    );

    context.fillStyle = "white";
    context.fillText(`Last actual score: ${lastActualScore ? lastActualScore.toLocaleString("en") : "Unknown"}`,
        PADDING, height + 4 - ROW_HEIGHT/4
    );

    return canvas;
}