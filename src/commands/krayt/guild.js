const { SlashCommandSubcommandBuilder } = require('@discordjs/builders');
const database = require("../../database");
const recommendationsUtils = require("../../utils/recommendations");
const kraytCommon = require("./kraytCommon");
const guildUtils = require("../../utils/guild");
const playerUtils = require("../../utils/player");
const swapiUtils = require("../../utils/swapi");
const { EmbedBuilder, AttachmentBuilder, ActionRowBuilder, ButtonBuilder, StringSelectMenuBuilder, ButtonStyle } = require("discord.js");
const { createCanvas, loadImage } = require("canvas");
const NodeCache = require('node-cache');
const { logger } = require('../../utils/log');
const { parseAsync } = require('json2csv');

const kraytGuildCache = new NodeCache({useClones: false});
const ARROW_DOWN_IMG = './src/img/arrow_dn.png';
const ARROW_UP_IMG = './src/img/arrow_up.png';

const SORT_BY_OPTION = {
    NAME: "Name",
    ESTIMATE: "Estimate",
    ACTUAL: "Actual",
    DIFF: "Difference",
    DIFF_PERCENT: "Difference %"
};

module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("guild")
        .setDescription("Estimate total score for the Krayt raid for a guild")
        .addStringOption(o => 
            o.setName("allycode")
            .setDescription("Ally Code to find recommendations for")
            .setRequired(false))
        .addNumberOption(o => 
            o.setName("deviation")
            .setDescription("Standard deviation cutoff for red/green coloring (default: 0.75)")
            .setRequired(false)
            .setMinValue(0)
            .setMaxValue(5)),
    interact: async function(interaction)
    {
        let allycode = interaction.options.getString("allycode");
        let standardDeviationMultiple = Math.round((interaction.options.getNumber("deviation") ?? 0.75)*100) / 100;

        if (allycode) {
            allycode = playerUtils.cleanAndVerifyStringAsAllyCode(allycode);
        } else {            
            let { botUser, guildId, error } = await playerUtils.authorize(interaction);

            if (guildId)
            {
                allycode = await database.db.oneOrNone(
                    `SELECT allycode FROM guild_players 
                    WHERE guild_id = $1 AND allycode in (select allycode from user_registration where discord_id = $2) 
                    LIMIT 1`, [guildId, interaction.user.id], r => r?.allycode);
            }
            else if (botUser.allycodes?.at(0))
            {
                allycode = botUser.allycodes?.at(0);
            }
        }

        if (!allycode)
        {
            await interaction.editReply({ content: "Please provide an allycode: **/krayt guild allycode:123456789**." });
            return;
        }

        let response = await generateResponse(interaction, { allycode: allycode, sortBy: SORT_BY_OPTION.ESTIMATE, standardDeviationMultiple: standardDeviationMultiple });
        await interaction.editReply(response);
        return;
    },
    onSelect: async function(interaction)
    {
        await interaction.update({ content: "Sorting...", embeds: [], files: [], components: [] });

        let regex = /kr:g:sort:([\d]+(\.[\d]*)?):(.+)/

        let matches = regex.exec(interaction.customId);

        if (!matches || matches.length < 4) {
            // something went wrong
            logger.error("kr:g:sort interaction.customId doesn't match. customId is: " + interaction.customId);
            await interaction.editReply({ content: "Something went wrong, unfortunately. I'm working on it, but please try running `/krayt guild` again." });
            return;
        }

        let comlinkGuildId = matches[3];
        let standardDeviationMultiple = matches[1];

        let response = await generateResponse(interaction, { comlinkGuildId: comlinkGuildId, sortBy: interaction.values[0], standardDeviationMultiple: standardDeviationMultiple });

        response.content = null;

        await interaction.editReply(response);
    },
    handleButton: async function(interaction)
    {
        if (interaction.customId.startsWith("kr:g:csv:"))
        {
            // show assignments
            await this.handleExportAsCSVButton(interaction);
            return;
        }
    },
    handleExportAsCSVButton: async function(interaction)
    {
        let regex = /kr:g:csv:(.+)/

        let matches = regex.exec(interaction.customId);

        if (!matches || matches.length < 2) {
            // something went wrong
            logger.error("kr:g:csv interaction.customId doesn't match. customId is: " + interaction.customId);
            await interaction.editReply({ content: "Something went wrong, unfortunately. I'm working on it, but please try running `/krayt guild` again." });
            return;
        }

        let comlinkGuildId = matches[1];

        
        let response = await generateCsvResponse(interaction, comlinkGuildId);
        
        await interaction.editReply(response);
    }
}

async function generateCsvResponse(interaction, comlinkGuildId)
{
    let estimatesData = undefined;
    if (comlinkGuildId) estimatesData = kraytGuildCache.get(comlinkGuildId);

    if (estimatesData === undefined)
    {
        let guildPlayerData = await guildUtils.loadGuildPlayerData({ comlinkGuildId: comlinkGuildId, includeRecentGuildActivityInfo: true }, interaction);

        estimatesData = await estimateGuildScores(guildPlayerData);
        kraytGuildCache.set(guildPlayerData.comlinkGuildId, estimatesData, 60*60*4);
    }
    
    const csvParsingOptions = { defaultValue: 0 }
    let csvText = await parseAsync(estimatesData.playersAndScores, csvParsingOptions);
    let buffer = Buffer.from(csvText);
    
    let csvAttachment = new AttachmentBuilder(buffer, { name: `krayt_${comlinkGuildId}_${new Date().getTime()}.csv`});
    return { files: [csvAttachment] };
}

async function generateResponse(interaction, { allycode, comlinkGuildId, sortBy, standardDeviationMultiple })
{
    let estimatesData = undefined;

    if (comlinkGuildId) estimatesData = kraytGuildCache.get(comlinkGuildId);

    if (estimatesData === undefined)
    {
        let guildPlayerData = await guildUtils.loadGuildPlayerData({ allycode: allycode, comlinkGuildId: comlinkGuildId, includeRecentGuildActivityInfo: true }, interaction);

        estimatesData = await estimateGuildScores(guildPlayerData);
        kraytGuildCache.set(guildPlayerData.comlinkGuildId, estimatesData, 60*60*4);
    }

    sortEstimates(estimatesData, sortBy);

    let guildEstimateCanvas = await generateScoresGraphic(estimatesData.playersAndScores, estimatesData.totals, sortBy, standardDeviationMultiple);

    let buffer = guildEstimateCanvas.toBuffer("image/png");
        
    let fileName = `guild-krayt-estimate-${new Date().getTime()}.png`
    const file = new AttachmentBuilder(buffer, { name: fileName });

    let meanText = +(Math.round(estimatesData.totals.mean + "e+2") + "e-2").toLocaleString("en");
    let standardDeviationText = +(Math.round(estimatesData.totals.standardDeviation + "e+2") + "e-2").toLocaleString("en")
    
    let embed = new EmbedBuilder()
        .setTitle(`Estimated Scores for ${estimatesData.guildName}`)
        .setDescription(
`Estimated Guild Score: ${estimatesData.totals.estimate.toLocaleString("en")}
Last Actual Guild Score: ${estimatesData.totals.guildActual.toLocaleString("en")}
Average Diff. % Between Estimated and Actual: ${meanText}%
Standard Deviation: ${standardDeviationText}%

:dragon: Estimates are based on the current guild members.
:dragon: Player actuals are the score in the last raid *in this guild*.
:dragon: Total Actual is the last actual guild score. This *will not match* the sum of players if your membership changed.
:dragon: Negative difference indicates a player may be underperforming.
:dragon: Red/Green indicates a player performed below/above **${standardDeviationMultiple}** standard deviations from the average Diff. %.`)
    embed.setImage("attachment://" + fileName);
    
    const csvParsingOptions = { defaultValue: 0 }
    let csvText = await parseAsync(estimatesData.playersAndScores, csvParsingOptions);
    let csvBuffer = Buffer.from(csvText);
    
    const csvAttachment = new AttachmentBuilder(csvBuffer, { name: `krayt_${estimatesData.guildComlinkId}_${new Date().getTime()}.csv`});
    
    let response = { embeds: [embed], files: [file, csvAttachment] };
    
    addComponents(response, estimatesData.guildComlinkId, standardDeviationMultiple);

    return response;
}

function addComponents(response, comlinkGuildId, standardDeviationMultiple)
{   
    let sortRow = new ActionRowBuilder();
    sortRow.addComponents(
        new StringSelectMenuBuilder()
            .setCustomId(`kr:g:sort:${standardDeviationMultiple}:${comlinkGuildId}`)
            .setPlaceholder('Sort by...')
            .addOptions([
                { label: SORT_BY_OPTION.NAME, value: SORT_BY_OPTION.NAME },
                { label: SORT_BY_OPTION.ESTIMATE, value: SORT_BY_OPTION.ESTIMATE },
                { label: SORT_BY_OPTION.ACTUAL, value: SORT_BY_OPTION.ACTUAL },
                { label: SORT_BY_OPTION.DIFF, value: SORT_BY_OPTION.DIFF },
                { label: SORT_BY_OPTION.DIFF_PERCENT, value: SORT_BY_OPTION.DIFF_PERCENT },
            ])
    );

    
    // let buttonRow = new ActionRowBuilder();
    // buttonRow.addComponents(
    //     new ButtonBuilder()
    //         .setCustomId(`kr:g:csv:${comlinkGuildId}`)
    //         .setLabel("Export (.csv)")
    //         .setStyle(ButtonStyle.Secondary)
    // );

    response.components = [sortRow, /*buttonRow*/];
}

async function estimateGuildScores(guildPlayerData)
{
    let estimatesData = {
        guildComlinkId: guildPlayerData.comlinkGuildId,
        guildName: guildPlayerData.guildName,
        playersAndScores: [],
        totals: { estimate: 0, actual: 0, diff: 0, guildActual: 0, mean: 0, standardDeviation: 0 }
    }
    let kraytRaidData = kraytCommon.findKraytRaid(guildPlayerData.guildApiData);

    estimatesData.totals.guildActual = kraytRaidData?.raidMember.reduce((p, v) => p + parseInt(v.memberProgress), 0) ?? 0;

    let totalDiffPercent = 0, playersToCalcDiffPercentMean = 0;

    for (let p of guildPlayerData.players)
    {
        let rec = await recommendationsUtils.generateRaidRecommendations(p, 
            {
                DIFFICULTY_MINIMUMS: kraytCommon.DIFFICULTY_MINIMUMS,
                MAX_ATTEMPTS: kraytCommon.MAX_ATTEMPTS,
                MAX_TOTAL_SCORE: kraytCommon.MAX_TOTAL_SCORE,
                REC_TYPE: recommendationsUtils.REC_TYPE.KRAYT,
                MIN_RECOMMENDED_GEAR: kraytCommon.MIN_RECOMMENDED_GEAR
            });

        let lastActualScore = kraytRaidData?.raidMember.find(pr => pr.playerId === p.playerId)?.memberProgress;
        if (lastActualScore) lastActualScore = parseInt(lastActualScore);

        let diff = (lastActualScore ? lastActualScore - rec.points : null);
        let diffPercent = null;
        if (rec.points != 0)
        {
            diffPercent = diff ? +(Math.round(((diff / rec.points) * 100.0) + "e+1") + "e-1") : null;
        } else {
            diffPercent = Number.MAX_SAFE_INTEGER;
        }

        estimatesData.playersAndScores.push(
            { 
                name: swapiUtils.getPlayerName(p), 
                allycode: swapiUtils.getPlayerAllycode(p),
                estimatedScore: rec.points, 
                lastActualScore: lastActualScore, 
                diff: diff,
                diffPercent: diffPercent
            });
            
        estimatesData.totals.estimate += rec.points;
        estimatesData.totals.actual += lastActualScore ?? 0;
        estimatesData.totals.diff += diff ?? 0;

        if (diffPercent != null && diffPercent != Number.MAX_SAFE_INTEGER) {
            totalDiffPercent += diffPercent;
            playersToCalcDiffPercentMean++;
        }
    }
    
    if (playersToCalcDiffPercentMean > 0)
    {
        estimatesData.totals.mean = totalDiffPercent / playersToCalcDiffPercentMean;
        estimatesData.totals.standardDeviation = Math.sqrt(estimatesData.playersAndScores.filter(s => s.diffPercent != null && s.diffPercent != Number.MAX_SAFE_INTEGER).map(x => ((x.diffPercent ?? 0) - estimatesData.totals.mean)**2).reduce((a, b) => a + b) / playersToCalcDiffPercentMean);
    }

    return estimatesData;
}

function sortEstimates(estimatesData, sortBy)
{
    switch (sortBy)
    {
        case SORT_BY_OPTION.ESTIMATE:
            estimatesData.playersAndScores.sort((a, b) => b.estimatedScore - a.estimatedScore);
            break;
        case SORT_BY_OPTION.ACTUAL:
            estimatesData.playersAndScores.sort((a, b) => (b.lastActualScore ?? 0) - (a.lastActualScore ?? 0));
            break;
        case SORT_BY_OPTION.DIFF:
            estimatesData.playersAndScores.sort((a, b) => (b.diff ?? 0) - (a.diff ?? 0));
            break;
        case SORT_BY_OPTION.DIFF_PERCENT:
            estimatesData.playersAndScores.sort((a, b) => ((b.diffPercent ?? 0) - (a.diffPercent ?? 0)) || (a.name.localeCompare(b.name)));
            break;
        case SORT_BY_OPTION.NAME:
            estimatesData.playersAndScores.sort((a, b) => a.name.localeCompare(b.name));
            break;
    }
}

const nameColWidth = 220;
const headerRowHeight = 28;
const playerRowHeight = 24;
const scoreColWidth = 100;
const totalRowHeight = 30;
const separatorColor = "#bbbb00";
const rowColorEven = "#001";
const rowColorOdd = "#002"
const width = nameColWidth + (scoreColWidth * 4) + 4;
const arrowWidth = 18;
const arrowHeight = 19;
async function generateScoresGraphic(playersAndScores, totals, sortBy, standardDeviationMultiple)
{
    const height = headerRowHeight + playerRowHeight*(playersAndScores.length) + totalRowHeight;

    const canvas = createCanvas(width, height);
    const context = canvas.getContext('2d');
    context.fillStyle = "rgb(0, 0, 0)";
    context.fillRect(0, 0, width, height);

    context.font = `12pt "Noto Sans","Noto Sans SC","Noto Sans Armenian","Noto Sans Thai","Noto Sans Arabic","Noto Sans Korean"`
    context.textAlign = "left"
    context.textBaseline = "middle";
    context.fillStyle = "#fff"
    
    context.fillText("Player", 2, 2 + playerRowHeight / 2);
    context.fillText("Estimate", 2 + nameColWidth, 2 + playerRowHeight / 2);
    context.fillText("Actual", 2 + nameColWidth + scoreColWidth, 2 + playerRowHeight / 2);
    context.fillText("Difference", 2 + nameColWidth + scoreColWidth*2, 2 + playerRowHeight / 2);
    context.fillText("Diff. %", 2 + nameColWidth + scoreColWidth*3, 2 + playerRowHeight / 2);
    
    let arrowUrl, arrowX;
    switch (sortBy)
    {
        case SORT_BY_OPTION.NAME:
            arrowUrl = ARROW_UP_IMG;
            arrowX = nameColWidth - 170;
            break;
        case SORT_BY_OPTION.ESTIMATE:
            arrowUrl = ARROW_DOWN_IMG;
            arrowX = nameColWidth + scoreColWidth - 30;
            break;
        case SORT_BY_OPTION.ACTUAL:
            arrowUrl = ARROW_DOWN_IMG;
            arrowX = nameColWidth + scoreColWidth*2 - 45;
            break;
        case SORT_BY_OPTION.DIFF:
            arrowUrl = ARROW_DOWN_IMG;
            arrowX = nameColWidth + scoreColWidth*3 - 18;
            break;
        case SORT_BY_OPTION.DIFF_PERCENT:
            arrowUrl = ARROW_DOWN_IMG;
            arrowX = nameColWidth + scoreColWidth*4 - 45;
            break;
    }

    const arrowImg = await loadImage(arrowUrl);
    context.drawImage(arrowImg, arrowX, 5, 
        arrowWidth, arrowHeight);

    context.fillStyle = separatorColor;
    context.fillRect(2,playerRowHeight,width - 4, 2)

    for (let pIx = 0; pIx < playersAndScores.length; pIx++)
    {
        drawPlayerRow(canvas, pIx, playersAndScores[pIx], totals, standardDeviationMultiple);
    }

    context.fillStyle = separatorColor;
    context.fillRect(2,height - totalRowHeight,width - 4, 2);
    
    context.fillStyle = "#fff";
    context.fillText("Total", 2, height - totalRowHeight/2);
    context.fillText(totals.estimate.toLocaleString("en"), 2 + nameColWidth, height - totalRowHeight/2);
    context.fillText(totals.guildActual.toLocaleString("en"), 2 + nameColWidth + scoreColWidth, height - totalRowHeight/2);

    context.fillStyle = (totals.diff === 0) ? "#fff" : ((totals.diff > 0) ? "green" : "red");
    let diffText = totals.diff.toLocaleString("en");
    let diffPercentText = +(Math.round((totals.diff / totals.estimate * 100.0) + "e+1") + "e-1").toLocaleString("en") + "%";
    if (totals.diff > 0) 
    {
        diffText = `+${diffText}`;
        diffPercentText = `+${diffPercentText}`;
    }
    context.fillText(diffText, 2 + nameColWidth + scoreColWidth*2, height - totalRowHeight/2);
    context.fillText(diffPercentText, 2 + nameColWidth + scoreColWidth*3, height - totalRowHeight/2);

    return canvas;
}

function drawPlayerRow(canvas, curPlayerIndex, curPlayer, totals, standardDeviationMultiple)
{
    const context = canvas.getContext('2d');

    context.fillStyle = (curPlayerIndex % 2 === 1) ? rowColorOdd : rowColorEven;
    context.fillRect(2, headerRowHeight + (curPlayerIndex)*playerRowHeight, width - 4, playerRowHeight);

    context.font = `12pt "Noto Sans","Noto Sans SC","Noto Sans Armenian","Noto Sans Thai","Noto Sans Arabic","Noto Sans Korean"`
    context.textAlign = "left"
    context.textBaseline = "middle";
    context.fillStyle = "#fff"

    context.fillText(curPlayer.name,
        2,
        headerRowHeight + (curPlayerIndex+0.5)*playerRowHeight,
        nameColWidth - 2);

    context.fillText(curPlayer.estimatedScore.toLocaleString("en"),
        nameColWidth + 2,
        headerRowHeight + (curPlayerIndex+0.5)*playerRowHeight,
        scoreColWidth - 2
        );

    let lastActualScoreText = curPlayer.lastActualScore ? parseInt(curPlayer.lastActualScore).toLocaleString("en") : "Unknown";

    if (lastActualScoreText === "Unknown") context.fillStyle = "#aaa";
    context.fillText(lastActualScoreText,
        nameColWidth + scoreColWidth + 2,
        headerRowHeight + (curPlayerIndex+0.5)*playerRowHeight,
        scoreColWidth - 2
        );

    let diffText;

    if (curPlayer.diff == null) context.fillStyle = "#aaa";
    else if (curPlayer.diff > 0 || (curPlayer.diffPercent >= (totals.mean + (standardDeviationMultiple*totals.standardDeviation)))) context.fillStyle = "green";
    else if (curPlayer.diffPercent > totals.mean - (standardDeviationMultiple*totals.standardDeviation)) context.fillStyle = "#fff";
    else if (curPlayer.diff < 0) context.fillStyle = "red";
    
    if (curPlayer.diff != null)
    {
        diffText = curPlayer.diff.toLocaleString("en");
        if (curPlayer.diff > 0) diffText = `+${diffText}`;
    } else {
        diffText = "Unknown";
    }

    context.fillText(diffText,
        nameColWidth + scoreColWidth*2 + 2,
        headerRowHeight + (curPlayerIndex+0.5)*playerRowHeight,
        scoreColWidth - 2
        );

    let diffPercentText;
    if (curPlayer.diffPercent === Number.MAX_SAFE_INTEGER)
    {
        diffPercentText = "∞";
    }
    else if (curPlayer.diffPercent != null)
    {
        diffPercentText = curPlayer.diffPercent.toLocaleString("en", {minimumFractionDigits: 1}) + "%";
        if (curPlayer.diffPercent > 0) diffPercentText = `+${diffPercentText}`;
    } else {
        diffPercentText = "Unknown";
    }

    context.fillText(diffPercentText,
        nameColWidth + scoreColWidth*3 + 2,
        headerRowHeight + (curPlayerIndex+0.5)*playerRowHeight,
        scoreColWidth - 2
        );
}
