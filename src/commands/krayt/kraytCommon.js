const DIFFICULTY_MINIMUMS = {
    0: {
        minRarity: 5,
        minGearLevel: 7,
        maxScore: 300000
    },
    1: {
        minRarity: 7,
        minGearLevel: 12,
        maxScore: 450000
    },
    2: {
        minRarity: 7,
        minGearLevel: 13,
        minRelicLevel: 1,
        maxScore: 600000
    },
    3: {
        minRarity: 7,
        minGearLevel: 13,
        minRelicLevel: 3,
        maxScore: 900000
    },
    4: {
        minRarity: 7,
        minGearLevel: 13,
        minRelicLevel: 5,
        maxScore: 1200000
    },
    5: {
        minRarity: 7,
        minGearLevel: 13,
        minRelicLevel: 7,
        maxScore: 1800000
    },
    6: {
        minRarity: 7,
        minGearLevel: 13,
        minRelicLevel: 8,
        maxScore: 2700000
    }
}

const MAX_POINTS_PER_TEAM = 2700000;
const MAX_ATTEMPTS = 5;
const MAX_TOTAL_SCORE = MAX_POINTS_PER_TEAM*MAX_ATTEMPTS;

const findKraytRaid = (guildApiData) => guildApiData?.recentRaidResult.find(r => r.raidId === "kraytdragon");

module.exports = {
    DIFFICULTY_MINIMUMS: DIFFICULTY_MINIMUMS,
    MAX_POINTS_PER_TEAM: MAX_POINTS_PER_TEAM,
    MAX_ATTEMPTS: MAX_ATTEMPTS,
    MAX_TOTAL_SCORE: MAX_TOTAL_SCORE,
    MIN_RECOMMENDED_GEAR: 10,
    findKraytRaid: findKraytRaid
}