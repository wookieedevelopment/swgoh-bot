const playerUtils = require("../../utils/player");
const readyUtils = require("../../utils/ready");
const queueUtils = require ("../../utils/queue");
const raidCommon = require ("../raid/raidCommon");
const database = require ("../../database");
const { SlashCommandSubcommandBuilder } = require('@discordjs/builders');

const CHECK_FUNCTION_MAPPING = {
    HC: { 
        value: "HC", 
        desc: "(Team) Jabba Hutt Cartel",
        reqs: {
            units: [
                { defId: "JABBATHEHUTT", req: readyUtils.UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "BOUSHH", req: readyUtils.UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "KRRSANTAN", req: readyUtils.UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "UNDERCOVERLANDO", req: readyUtils.UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "BOBAFETT", req: readyUtils.UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "CADBANE", req: readyUtils.UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "GREEDO", req: readyUtils.UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "EMBO", req: readyUtils.UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "GAMORREANGUARD", req: readyUtils.UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "HUMANTHUG", req: readyUtils.UNIT_REQUIREMENT_TYPE.OPTIONAL }
            ],
            teamSize: 5
        }
    },
    Tuskens: {
        value: "Tuskens", 
        desc: "(Team) Tuskens",
        reqs: {
            units: [
                { defId: "TUSKENCHIEFTAIN", req: readyUtils.UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "TUSKENHUNTRESS", req: readyUtils.UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "TUSKENRAIDER", req: readyUtils.UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "URORRURRR", req: readyUtils.UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "TUSKENSHAMAN", req: readyUtils.UNIT_REQUIREMENT_TYPE.REQUIRED }
            ],
            teamSize: 5
        }
    },
    Maul: {
        value: "Maul", 
        desc: "(Team) Maul Mandos",
        reqs: {
            units: [
                { defId: "MAULS7", req: readyUtils.UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "THEMANDALORIANBESKARARMOR", req: readyUtils.UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "CANDEROUSORDO", req: readyUtils.UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "BOKATAN", req: readyUtils.UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "ARMORER", req: readyUtils.UNIT_REQUIREMENT_TYPE.REQUIRED }
            ],
            teamSize: 5
        }
    },
    Jawas: {
        value: "Jawas", 
        desc: "(Team) Jawas",
        reqs: {
            units: [
                { defId: "CHIEFNEBIT", req: readyUtils.UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "DATHCHA", req: readyUtils.UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "JAWASCAVENGER", req: readyUtils.UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "JAWAENGINEER", req: readyUtils.UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "JAWA", req: readyUtils.UNIT_REQUIREMENT_TYPE.REQUIRED }
            ],
            teamSize: 5
        }
    },
    JKROR: {
        value: "JKROR",
        desc: "(Team) JKR Old Republic",
        reqs: {
            units: [
                { defId: "JEDIKNIGHTREVAN", req: readyUtils.UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "BASTILASHAN", req: readyUtils.UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "JOLEEBINDO", req: readyUtils.UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "ZAALBAR", req: readyUtils.UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "MISSIONVAO", req: readyUtils.UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "JUHANI", req: readyUtils.UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "50RT", req: readyUtils.UNIT_REQUIREMENT_TYPE.OPTIONAL }
            ],
            teamSize: 5
        }
    },
    Mandalorians: {
        value: "Mandalorians", 
        desc: "(Faction) Mandalorians",
        reqs: {
            units: [
                { defId: "MAULS7", req: readyUtils.UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "JANGOFETT", req: readyUtils.UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "CANDEROUSORDO", req: readyUtils.UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "THEMANDALORIANBESKARARMOR", req: readyUtils.UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "ARMORER", req: readyUtils.UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "BOKATAN", req: readyUtils.UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "GARSAXON", req: readyUtils.UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "IMPERIALSUPERCOMMANDO", req: readyUtils.UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "THEMANDALORIAN", req: readyUtils.UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "SABINEWRENS3", req: readyUtils.UNIT_REQUIREMENT_TYPE.OPTIONAL }
            ],
            teamSize: 5
        }
    },
    OR: {
        value: "OR", 
        desc: "(Faction) Old Republic",
        reqs: {
            units: [
                { defId: "JEDIKNIGHTREVAN", req: readyUtils.UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "BASTILASHAN", req: readyUtils.UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "JOLEEBINDO", req: readyUtils.UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "JUHANI", req: readyUtils.UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "CARTHONASI", req: readyUtils.UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "ZAALBAR", req: readyUtils.UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "MISSIONVAO", req: readyUtils.UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "CANDEROUSORDO", req: readyUtils.UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "T3_M4", req: readyUtils.UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "50RT", req: readyUtils.UNIT_REQUIREMENT_TYPE.OPTIONAL }
            ],
            teamSize: 5
        }
    },
};

const CHECK_CHOICES = [
    { name: CHECK_FUNCTION_MAPPING.HC.desc, value: CHECK_FUNCTION_MAPPING.HC.value },
    { name: CHECK_FUNCTION_MAPPING.Jawas.desc, value: CHECK_FUNCTION_MAPPING.Jawas.value },
    { name: CHECK_FUNCTION_MAPPING.JKROR.desc, value: CHECK_FUNCTION_MAPPING.JKROR.value },
    { name: CHECK_FUNCTION_MAPPING.Maul.desc, value: CHECK_FUNCTION_MAPPING.Maul.value },
    { name: CHECK_FUNCTION_MAPPING.Tuskens.desc, value: CHECK_FUNCTION_MAPPING.Tuskens.value },
    { name: CHECK_FUNCTION_MAPPING.Mandalorians.desc, value: CHECK_FUNCTION_MAPPING.Mandalorians.value },
    { name: CHECK_FUNCTION_MAPPING.OR.desc, value: CHECK_FUNCTION_MAPPING.OR.value }
];

module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("ready")
        .setDescription("Check readiness for various Krayt Dragon Raid teams")
        .addStringOption(o => 
            o.setName("team")
            .setDescription("The team to check")
            .setRequired(true)
            .addChoices(...CHECK_CHOICES))
        .addIntegerOption(o =>
            o.setName("tier")
            .setDescription("The Difficulty to check")
            .setRequired(true)
            .addChoices(...raidCommon.DIFFICULTY_CHOICES))
        .addStringOption(o => 
            o.setName("allycode")
            .setDescription("An allycode in the guild (default: yours, for registered users)")
            .setRequired(false)
            ),
    interact: async function(interaction)
    {
        let team = interaction.options.getString("team");
        let tier = interaction.options.getInteger("tier");
        let allycode = interaction.options.getString("allycode");

        if (allycode) {
            allycode = playerUtils.cleanAndVerifyStringAsAllyCode(allycode);
        } else {
            
            let { botUser, guildId, error } = await playerUtils.authorize(interaction);

            if (guildId)
            {
                allycode = await database.db.oneOrNone(
                    `SELECT allycode FROM guild_players 
                    WHERE guild_id = $1 AND allycode in (select allycode from user_registration where discord_id = $2) 
                    LIMIT 1`, [guildId, interaction.user.id], r => r?.allycode);
            }
            else if (botUser.allycodes?.at(0))
            {
                allycode = botUser.allycodes?.at(0);
            }
        }

        if (!allycode)
        {
            await interaction.editReply({ content: "Please provide an allycode: **/krayt ready team:XXX tier:N allycode:123456789**." });
            return;
        }
        
        let qsm = await queueUtils.getQueueSizeMessage(queueUtils.QUEUES.READY, interaction.client.shard);
        const editMessage = await interaction.editReply(`Processing raid ready check. ${qsm}`);

        await queueUtils.queueReadyJob( interaction.client.shard,
            {
                type: "RAID",
                team: team,
                tier: tier, 
                allycode: allycode, 
                messageId: editMessage.id, 
                channelId: editMessage.channelId ?? editMessage.channel.id,
                userId: interaction.user.id 
            });
    }
}