const got = require("got");
const discordUtils = require ("../utils/discord")
const { SlashCommandBuilder } = require('@discordjs/builders');
const { ActionRowBuilder, EmbedBuilder, ButtonStyle, ButtonBuilder } = require('discord.js');

let buttonRow = new ActionRowBuilder().addComponents(
    new ButtonBuilder()
    .setCustomId("another_dad_joke")
    .setLabel("Tell Me Another!")
    .setStyle(ButtonStyle.Primary)
);     

module.exports = {
    data: new SlashCommandBuilder()
        .setName("dadjoke")
        .setDescription("WookieeBot joke generator"),
    name: "dadjoke",
    aliases: [ "dj" ],
    description: "WookieeBot joke generator",
    interact: async function(interaction)
    {
        const joke = await this.getJoke();
        await interaction.editReply({ content: `${discordUtils.makeIdTaggable(interaction.user.id)}: ${joke.joke}`, components: [buttonRow]});
    },
    handleButton: async function(interaction)
    {
        const joke = await this.getJoke();
        await interaction.followUp({ content: `${discordUtils.makeIdTaggable(interaction.user.id)}: ${joke.joke}`, components: [buttonRow] })
    },
    execute: async function(client, message, args) {
        const joke = await this.getJoke();
        await message.channel.send(joke.joke);
    },
    getJoke: async function()
    {
        return await got("https://icanhazdadjoke.com/").json();
    }
}