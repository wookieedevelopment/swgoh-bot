const { PermissionsBitField, SlashCommandBuilder } = require("discord.js");
const discordUtils = require("../utils/discord");

module.exports = {
    data: new SlashCommandBuilder()
        .setName("check-permissions")
        .setDescription("Check that the bot has the proper permissions in a channel.")
        .addChannelOption(o => 
            o.setName("channel")
            .setDescription("A channel in which to test the bot's permissions (default: the current channel).")
            .setRequired(false)
            ),
    interact: async function(interaction)
    {
        let channel = interaction.options.getChannel("channel") ?? interaction.channel;
        let response = verifyPermissions(interaction, channel);
        await interaction.editReply(response);
    }
}

function verifyPermissions(interaction, channel)
{
    let missingPermissions = discordUtils.getMissingBotPermissions(interaction, channel);

    if (missingPermissions)
    {
        return discordUtils.convertMissingBotPermissionsArrayToResponse(missingPermissions);
    }

    return { content: discordUtils.symbols.pass + " WookieeBot has the proper permissions."};
}