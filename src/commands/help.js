const { EmbedBuilder } = require("discord.js");
const discordUtils = require("../utils/discord");
const { SlashCommandBuilder } = require('@discordjs/builders');

module.exports = {
    data: new SlashCommandBuilder()
        .setName("help")
        .setDescription("WookieeBot Help"),
    name: "help",
    description: "WookieeBot Help",
    interact: async function(interaction)
    {

        let generalCommandsField = {
            name: "// General Commands //",
            value: 
`**invite**: Get a a link to invite WookieeBot to your server.
**cheating**: Show likelihood a player/guild cheated (based on current roster) to acquire 7★ event characters.
**report**: Show reports (Note: many require registration)
**guide**: Show guides for parts of the SWGOH
**tb**: TB utilities
**tw scout**: TW guild scouting (Note: other TW commands require registration)
**gac**: GAC comparisons & tools
**user**: Info about yourself
**krayt**: Krayt raid tools
`
        }

        let registeredField = {
            name: "// Registered Guilds Only //",
            value:
`**tw**: TW defense planner
**twmanager**: TW offense manager
**guild**: Manage guild
**counters**: Counters for your guild`,
            inline: false
        }
    
        let funField = {
            name: "// Fun //",
            value: "**fact**: Show a random fact about the Star Wars universe\r\n" +
                   "**meme**: Generate memes\r\n" +
                   "**dadjoke**: Tell a random dad joke.\r\n" + 
                   "**praise**: Send praise to a user.\r\n" + 
                   "**talk**: Talk like a Wookiee.",
            inline: false
        }

        let joinField = {
            name: "// Join Us on Discord! //",
            value: discordUtils.WOOKIEEBOT_DISCORD_LINK,
            inline: false
        }

        let embed = new EmbedBuilder()
            .setTitle("WookieeBot Help")
            .setColor(0xD2691E)
            .setURL(discordUtils.WOOKIEEBOT_DISCORD_LINK)
            .addFields(generalCommandsField, funField, registeredField, joinField);

        await interaction.editReply({ embeds: [embed] });
    },
    execute: async function(client, input, args) {
        
        let inviteField = {
            name: "// invite //", 
            value: "**^invite**: Sends a direct message with a link to invite WookieeBot to your server.",
            inline: false
        };
    
    
        let cheatingField = {
            name: "// cheating //", 
            value: 
            "Evaluate the likelihood the player cheated (based on current roster) to acquire 7★ legendary/journey characters." + 
            "\r\n\r\n" +
            "  **^cheating allycode**: For those with a 20%+ likelihood of cheating, show a breakdown of which characters were likely used to acquire." +
            "\r\n" +  
            "  **^cheating allycode -d**: Show the breakdown of which characters were likely used for *all* legendary/journey characters." +
            "\r\n" +
            "  **^cheating.guild allycode**: Shows all players in the guild of the provided allycode that have a 60%+ likelihood of cheating at any checked events.",
            inline: false
        };
    
    
        let guidesField = {
            name: "// guide //",
            value:
                "**^guide**: Show a list of guides that can be provided." +
                "\r\n" +
                "Example: **^guide kam** (provides the KAM guide)" ,
            inline: false
        };

        let registeredField = {
            name: "// registered guilds only //",
            value:
`**^report**: Show reports
**^tw**: TW defense manager
**^twm**: TW offense manager
**^guild**: Manage guild
**^c**: Counters for your guild
**^user**: Manage users`,
            inline: false
        }
    
        let funField = {
            name: "// fun //",
            value: "**^fact**: Show a random fact about the Star Wars universe\r\n" +
                   "**^meme**: Show a list of memes that can be triggered after the ^ prefix\r\n" +
                   "**^dadjoke** or **^dj** or **/dadjoke**: Tell a random dad joke.\r\n" + 
                   "/praise: Send praise to a user.",
            inline: false
        }

        let joinField = {
            name: "// Join Us on Discord! //",
            value: "https://discord.com/invite/KcHzz2v",
            inline: false
        }

        let embed = new EmbedBuilder()
            .setTitle("WookieeBot Help")
            .setColor(0xD2691E)
            .setURL("https://discord.com/invite/KcHzz2v")
            .addFields(inviteField, cheatingField, guidesField, funField, registeredField, joinField);

        await input.channel.send({ embeds: [embed] });
    }
}