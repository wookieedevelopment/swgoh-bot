const { SlashCommandBuilder } = require("@discordjs/builders");
const scoutGuild = require("./scout/scoutGuild");
const scoutMods = require("./scout/scoutMods");
const queueUtils = require("../utils/queue");
const twUtils = require("./tw/utils");
const { CLIENT } = require("../utils/discordClient");

const helpMessage =
`\`/scout guild\`: show summary information about an Ally Code's guild, with detailed drill-downs for datacrons, omicrons, and useful queries/calculations.

\`scout mods\`: Only available to subscribers. Get mod summary information for players or guilds (max 250 players at a time).
- Provide one or more allycodes, separated by commas (,)
- Provide one or more units, separated by commas (,). Nicknames are accepted (e.g., DR = Darth Revan)
- Set \`guild:True\` to query the full guilds for those players
- Set \`force-cache:True\` to pull new data from the game (max 25 players)
- Supply a \`filter\` to further refine whose modding is analyzed. This follows the same format as TW teams:
 - \`jmk(g=13, z=6); ki-adi (r>4, z=1); ahsoka tano (g=13, z=1)\`: JMK at G13 and 6 zetas with KAM at R5+ and 1 zeta and Ahsoka at G13 and 1 Zeta
 - \`dr(speed>340)\`: Darth Revan with speed > 340
 - \`wrecker(speed>hunter)\`: Wrecker faster than Hunter

Unit names can be nicknames or substrings of the full name. In case of conflicts, use the exact name (e.g., "Ahsoka Tano" matches Snips, not "Commander Ahsoka Tano").

${"Valid features:\r\n- " + twUtils.features.options.map(f => `${f.key}: ${f.description}`).join("\r\n- ")}
`;

module.exports = {
    data: new SlashCommandBuilder()
        .setName("scout")
        .setDescription("Scout a guild (TW, etc.)")
        .addSubcommand(scoutGuild.data)
        .addSubcommand(scoutMods.data)
        .addSubcommand(c =>
            c.setName("help")
            .setDescription("Help with this command.")),
    interact: async function (interaction) {

        let subcommand = interaction.options.getSubcommand();

        switch (subcommand) {
            case scoutGuild.data.name:
                await scoutGuild.interact(interaction);
                break;
            case scoutMods.data.name:
                await scoutMods.interact(interaction);
                break;
            case "help":
                await this.help(interaction);
                break;
        }
    },
    onSelect: async function (interaction) {
        await scoutGuild.onSelect(interaction);
    },
    help: async function (interaction) {
        await interaction.editReply(helpMessage);
    }
}