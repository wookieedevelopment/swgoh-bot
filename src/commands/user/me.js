const discordUtils = require("../../utils/discord");
const playerUtils = require("../../utils/player");
const swapi = require("../../utils/swapi");
const guildUtils = require("../../utils/guild");
const database = require("../../database");
const guild = require("../../utils/guild");
const { SlashCommandSubcommandBuilder } = require("@discordjs/builders");
const { ActionRowBuilder, ButtonBuilder, ButtonStyle, EmbedBuilder, AttachmentBuilder, StringSelectMenuBuilder } = require('discord.js');
const { logger, formatError } = require("../../utils/log.js");


const EMBEDS = {
    TIMEOUT: createUSERMEembed('No action received. Cancelled.'),
    WAIT: createUSERMEembed('Verifying...'),
    ERROR: (message) => createUSERMEembed(message ?? "An error occurred. Please try again or request help on the WookieeTools server."),
    NA: () => createUSERMEembed("Who are you? Please register using `/register` or request help on the WookieeTools server."),
    PROMPT: (description, fields) => createUSERMEembed(description, fields),
};

function createUSERMEembed(description, fields) {
    let embed = new EmbedBuilder()
        .setTitle(`Profile - /user me`)
        .setAuthor({ name: 'WookieeBot', iconURL: 'https://cdn.discordapp.com/attachments/1126720068512927804/1134180144135819294/wb.png', url: 'https://discord.com/invite/KcHzz2v' });

    if (description && description.length > 0) embed.setDescription(description);
    if (fields && fields.length > 0) embed.addFields(fields.map(field => ({ name: field.name, value: field.value, inline: field.inline })));

    return embed;
}

async function generateResponse(botUser) {
    const fields = [];

    const guildDictionary = botUser.guilds.reduce((acc, guild) => {
        guild.allycodes.forEach(allycode => {
            const roles = [];
            if (guild.guildBotAdmin) roles.push('Bot Admin');
            if (guild.guildAdmin) roles.push('Guild Admin');
            
            acc[allycode] = {
                guildName: guild.guildName,
                permissions: roles.length > 0 ? roles.join(', ') : 'Member',
            };
        });
        return acc;
    }, {});

    for (let i = 0; i < botUser.alts.length; i++)
    {
        let alt = botUser.alts[i];
        let fieldName;
        if (i === 0) {
            fieldName = "Primary Account";
        }
        else 
        {
            fieldName = `Alt #${i.toString()}`
        }

        let permissions = guildDictionary[alt.allycode]?.permissions;

        fields.push({ 
            name: fieldName,
            value:
`Name: ${alt.name}
Ally Code: ${discordUtils.addDashesToAllycode(alt.allycode)}
Guild: ${alt.guildName ?? "No Guild"}${permissions ? `: ${permissions}` : ""}`
        });
    }

    fields.push({ name: "For support with WookieeBot:", value: '[Join us on Discord](https://discord.com/invite/KcHzz2v)'});

    const embed = EMBEDS.PROMPT(null, fields);
    
    let response = { embeds: [embed], files: [] }; 
    if (botUser.alts.length > 1)
    {
        const row = new ActionRowBuilder()
            .addComponents(
                new StringSelectMenuBuilder()
                    .setCustomId(`user:me:changemain`)
                    .setPlaceholder("Switch Primary Account to...") 
                    .addOptions(
                        botUser.alts.slice(1).map(p => { return { label: p.name, value: p.allycode }})
                    )
            );

        response.components = [row];
    }   
    return response;
}

module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("me")
        .setDescription("Show information about yourself"),
    interact: async function (interaction) {
        let { botUser, guildId, error } = await playerUtils.authorize(interaction);
        if (error) {
            await interaction.editReply({ embeds: [discordUtils.createErrorEmbed("Error", error)] });
            return;
        }

        if (!botUser.allycodes || botUser.allycodes.length === 0) {
            await interaction.editReply({ embeds: [EMBEDS.NA()] });
            return;
        }
        
        //commented out until we find a better way of pulling WookieeBot server subscribers. 
        //const responseString = botUser.guilds && botUser.guilds.some(g => g.guildBotAdmin)
          //  ? "You are not subscribed to WookieeBot.\r\n\r\n"
          //  : "🥳 You are subscribed to WookieeBot - thank you! 🍻\r\n\r\n";
        
        let response = await generateResponse(botUser);
        
        const responseMessageRef = await interaction.editReply(response);

        if (botUser.allycodes.length <= 1) return;
        
        const collectorFilter = i => { i.deferUpdate(); return i.user.id === interaction.user.id; };

        try {
            const confirmation = await responseMessageRef.awaitMessageComponent({ filter: collectorFilter, time: 300_000 });

            let selectedAllycode;
            if (confirmation.values.length > 0) selectedAllycode = confirmation.values[0];
            else return;
            
            await database.db.any(
                `UPDATE user_registration 
                SET alt = (SELECT alt FROM user_registration WHERE allycode = $1) 
                WHERE allycode = 
                    (SELECT allycode 
                    FROM user_registration 
                    WHERE alt = 0 
                    AND discord_id = (SELECT discord_id FROM user_registration WHERE allycode = $1)
                    );

                UPDATE user_registration SET alt = 0 WHERE allycode = $1;
                `, [selectedAllycode]);

                
            await this.interact(interaction);

        } catch (e) {
            if (e.message.endsWith("time")) 
            {
                response.components = [];
                await interaction.editReply(response);
            }
            else
            {
                logger.error('Error during /user me wait: ' + formatError(e));
                await interaction.editReply({ embeds: [EMBEDS.ERROR()], components: [], files: [] });
            }
        }
    }
};
