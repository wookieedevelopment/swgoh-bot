const playerUtils = require("../../utils/player");
const database = require("../../database");

module.exports = {
    name: "user.settimezoneoffset",
    description: "Set your timezone",
    setOffset: async function(input, args) {
        let { botUser, guildId, error } = await playerUtils.authorize(input)
        if (error) {
            await input.channel.send(error);
            return;
        }

        if (!args || args.length == 0)
        {
            await input.reply("You must specify the offset in hours, minutes, and seconds (h:m:s). For example:\r\n" + 
                        "03:00:00\r\n-08:30:00")
            return;
        }
        let regex = /^([-+]?\d{1,2}):(\d{1,2}):(\d{1,2})$/;
        let match = regex.exec(args[0]);

        if (!match)
        {
            await input.reply("You must specify the offset in hours, minutes, and seconds (h\\:m\\:s) For example:\r\n" + 
                        "```03:00:00\r\n-08:30:00```")
            return;
        }

        await database.db.any("UPDATE user_registration SET utc_offset = $1 WHERE discord_id = $2", [match[0], botUser.userId]);

        await input.reply("Timezone offset updated to " + match[0])
   }
}