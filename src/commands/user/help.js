const { SlashCommandSubcommandBuilder } = require("@discordjs/builders");
const { EmbedBuilder } = require("discord.js");
const discordUtils = require("../../utils/discord.js");



module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("help")
        .setDescription("WookieeBot User Management Help"),
    interact: async function(interaction)
    {
        let embed = this.getHelpEmbed();
        await interaction.editReply({ embeds: [embed] });
    },
    execute: async function(input)
    {
        let embed = this.getHelpEmbed();
        await input.channel.send({ embeds: [embed] });
    },
    getHelpEmbed: function(input) {

        let allUserCommandsField = {
            name: "Player Commands",
            value:
`**/user me**: show info about your registration`,
            inline: false
        };

        let embed = new EmbedBuilder()
            .setTitle("WookieeBot User Management")
            .setColor(0xD2691E)
            .addFields(allUserCommandsField);

        return embed;
    }

}
