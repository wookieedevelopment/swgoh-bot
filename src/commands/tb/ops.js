
const { SlashCommandSubcommandBuilder } = require('@discordjs/builders');
const database = require("../../database");
const metadataCache = require("../../utils/metadataCache");
const guildUtils = require("../../utils/guild");
const playerUtils = require("../../utils/player");
const swapiUtils = require("../../utils/swapi");
const cacheUtils = require ("../../utils/cache")
const discordUtils = require("../../utils/discord");
const unitsUtils = require("../../utils/units")
const constants = require("../../utils/constants");
const queueUtils = require ("../../utils/queue");
const { logger, formatError } = require("../../utils/log");
const { EmbedBuilder, ActionRowBuilder, ButtonBuilder, ButtonStyle, AttachmentBuilder, StringSelectMenuBuilder } = require('discord.js');
const { createCanvas, Image, loadImage } = require("canvas");
const { Buffer } = require ("buffer")
const tbCommon = require ("./common")
const { parseAsync } = require('json2csv');
const { CLIENT } = require('../../utils/discordClient');

const MAX_MISSING_TO_SHOW = 90;
const MAX_ASSIGNMENTS_PER_PLAYER_PER_PLANET = 10;
const MAX_ASSIGNMENTS_PER_PLAYER = 30;
const PORTRAIT_WIDTH = 70;
const PORTRAIT_HEIGHT = 70;
const SPACE_BETWEEN_PORTRAITS = 40;
const UNASSIGNED_NAME = "ANY";
const UNASSIGNED_ALLYCODE = "0";
const OPERATION_PRIORITY = {
    MOST_POINTS_FIRST: 1,
    EASIEST_FIRST: 2
};

if (CLIENT.shard) queueUtils.BOSS.work(queueUtils.getQueueName(queueUtils.QUEUES.OPS_OLD, CLIENT.shard), { newJobCheckIntervalSeconds: 20 }, loadGuildDataJobHandler);


module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("ops")
        .setDescription("Get ready to complete operations")
        .addStringOption(o => 
            o.setName("allycode")
            .setDescription("Ally Code of player in guild")
            .setRequired(false))
        .addIntegerOption(o => 
            o.setName("sector")
            .setDescription("A sector (horizontal slice) to consider.")
            .setRequired(false)
            .setMinValue(1)
            .setMaxValue(6))
        .addStringOption(o => 
            o.setName("light")
            .setDescription("A light side planet to consider.")
            .addChoices(...tbCommon.LS_PLANET_CHOICES)
            .setRequired(false))
        .addStringOption(o => 
            o.setName("mix")
            .setDescription("A mixed alignment planet to consider.")
            .addChoices(...tbCommon.MX_PLANET_CHOICES)
            .setRequired(false))
        .addStringOption(o => 
            o.setName("dark")
            .setDescription("A dark side planet to consider.")
            .addChoices(...tbCommon.DS_PLANET_CHOICES)
            .setRequired(false))
        .addStringOption(o => 
            o.setName("ls-ops")
            .setDescription("Specific light side ops to consider. Ex.: 135 or 1,3,5")
            .setMaxLength(11)
            .setRequired(false))
        .addStringOption(o => 
            o.setName("mx-ops")
            .setDescription("Specific mixed alignment ops to consider. Ex.: 135 or 1,3,5")
            .setMaxLength(11)
            .setRequired(false))
        .addStringOption(o => 
            o.setName("ds-ops")
            .setDescription("Specific dark side ops to consider. Ex.: 135 or 1,3,5")
            .setMaxLength(11)
            .setRequired(false))
        .addStringOption(o =>
            o.setName("exclude")
            .setDescription("Allycodes to exclude from all ops, separated by commas.")
            .setRequired(false))
        .addStringOption(o =>
            o.setName("rare-only")
            .setDescription("Only check readiness and assign rare units (guild has # or fewer eligible).")
            .setRequired(false)
            .addChoices(
                { name: "True (default: 5)", value: "5" },
                { name: "1", value: "1" },
                { name: "2", value: "2" },
                { name: "3", value: "3" },
                { name: "4", value: "4" },
                { name: "5", value: "5" },
                { name: "6", value: "6" },
                { name: "7", value: "7" },
                { name: "8", value: "8" },
                { name: "9", value: "9" },
                { name: "10", value: "10" }
            )
            )
        .addIntegerOption(o => 
            o.setName("priority")
            .setDescription("Prioritization options for operations. Default: Most Points First")
            .setRequired(false)
            .addChoices(
                { name: "Most Points first", value: 1 },
                { name: "Easiest to complete first", value: 2 }
            ))
        .addIntegerOption(o =>
            o.setName("alt")
            .setDescription("Alt account (/user me)")
            .setRequired(false)
            .setAutocomplete(true)),
    interact: async function(interaction)
    {
        let missingPermissions = discordUtils.getMissingBotPermissions(interaction, interaction.channel);
        if (missingPermissions)
        {
            await interaction.editReply(discordUtils.convertMissingBotPermissionsArrayToResponse(missingPermissions));
            return;
        }
        
        let allycode = interaction.options.getString("allycode");
        let altNum = interaction.options.getInteger("alt");
        
        if (!allycode)
        {
            let { botUser, guildId, error } = await playerUtils.authorize(interaction);
            if (error) {
                await interaction.editReply({ embeds: [discordUtils.createErrorEmbed("Error", error)] });
                return;
            }
            
            let alt = await playerUtils.chooseBestAlt(botUser, guildId, altNum, true);

            if (!alt)
            {
                let errorEmbed = discordUtils.createErrorEmbed("Not in Guild", `Account is not in a guild. Choose a different alt, specify an ally code, join a guild, or register.`);
                await interaction.editReply({ embeds: [errorEmbed] });
                return;
            }

            allycode = alt.allycode;
        }
        
        allycode = playerUtils.cleanAndVerifyStringAsAllyCode(allycode);

        let sector = interaction.options.getInteger("sector");
        let dsPlanet = interaction.options.getString("dark");
        let lsPlanet = interaction.options.getString("light");
        let mxPlanet = interaction.options.getString("mix");
        let lsOps = interaction.options.getString("ls-ops");
        let mxOps = interaction.options.getString("mx-ops");
        let dsOps = interaction.options.getString("ds-ops");
        let exclude = interaction.options.getString("exclude");
        let rareOnly = interaction.options.getString("rare-only");
        if (rareOnly) rareOnly = parseInt(rareOnly);
        let priority = interaction.options.getInteger("priority") ?? OPERATION_PRIORITY.MOST_POINTS_FIRST;

        let response, customIdAppend;
        
        let guild = await guildUtils.getGuildDataFromAPI({ allycode: allycode }, { useCache: true, cacheTimeHours: 4 });

        let planetsToUse;
        if (!dsPlanet && !lsPlanet && !mxPlanet && !sector) {
            
            planetsToUse = tbCommon.PLANETS.filter(p => p.sector === 1).map(p => p.name);
        }
        else
        {
            planetsToUse = [];
            if (dsPlanet) planetsToUse.push(dsPlanet);
            else if (sector) planetsToUse.push(tbCommon.PLANETS.find(p => p.sector === sector && p.alignment === tbCommon.ALIGNMENTS.DS && !p.specialPlanet).name)
    
            if (mxPlanet) planetsToUse.push(mxPlanet);
            else if (sector) planetsToUse.push(tbCommon.PLANETS.find(p => p.sector === sector && p.alignment === tbCommon.ALIGNMENTS.MX && !p.specialPlanet).name)
    
            if (lsPlanet) planetsToUse.push(lsPlanet);
            else if (sector) planetsToUse.push(tbCommon.PLANETS.find(p => p.sector === sector && p.alignment === tbCommon.ALIGNMENTS.LS && !p.specialPlanet).name)        
        }

        if (lsOps) lsOps = lsOps.replace(/\D/g, "");
        if (dsOps) dsOps = dsOps.replace(/\D/g, "");
        if (mxOps) mxOps = mxOps.replace(/\D/g, "");

        let excludeArray;
        if (exclude)
        {
            excludeArray = exclude.replace(/[^0-9,]/g, "").split(",");
        }
        
        customIdAppend = await getCustomIdTextByPlanets(planetsToUse);
        let metadata = generateTbOpsMetadata(dsOps, mxOps, lsOps, excludeArray, rareOnly, priority);

        let cachedData = await cacheUtils.loadTbAssignmentsFromCache(customIdAppend, swapiUtils.getGuildId(guild), metadata);

        if (cachedData)
        {        
            response = await getResponseForPlanets(planetsToUse, { cachedData: cachedData, dsOps: dsOps, lsOps: lsOps, mxOps: mxOps, allycodesToExclude: excludeArray, rareOnly: rareOnly, priority: priority });
            
            addComponentsToResponse(response, swapiUtils.getGuildId(guild), customIdAppend);
            let reply = await interaction.editReply(response);
            
            await saveTbOpsRequestData(reply, excludeArray, lsOps, dsOps, mxOps, rareOnly, priority);
        } else {
            let qsm = await queueUtils.getQueueSizeMessage(queueUtils.QUEUES.OPS_OLD, interaction.client.shard);
            const editMessage = await interaction.editReply(`Processing TB Ops request. ${qsm}`);
            
            await queueUtils.queueTbOpsJob(interaction.client.shard, 
            {
                fn: "interact",
                comlinkGuildId: swapiUtils.getGuildId(guild),
                channelId: editMessage.channelId ?? editMessage.channel.id,
                messageId: editMessage.id,
                userId: interaction.user.id,
                planetsToUse: planetsToUse,
                dsOps: dsOps,
                lsOps: lsOps,
                mxOps: mxOps,
                allycodesToExclude: excludeArray,
                rareOnly: rareOnly,
                priority: priority,
                customIdAppend: customIdAppend
            },
            {
                expireInHours: 2
            });
        }

    },
    autocomplete: async function(interaction) {
        const focusedValue = interaction.options.getFocused();
        await interaction.respond((await playerUtils.getAltAutocompleteOptions(focusedValue, interaction.user.id)));
    },
    handleShowAssignmentsByPlanetButton: async function(interaction)
    {
        let regex = /tb:ops:aplanet:(((LS|MX|DS)[1-6]){1}((LS|MX|DS)[1-6]){0,1}((LS|MX|DS)[1-6]){0,1}):(.+)/

        let matches = regex.exec(interaction.customId);
        let response;

        if (!matches || matches.length != 9) {
            // something went wrong
            logger.error("tb:ops:aplanet:handleButton interaction.customId doesn't match. customId is: " + interaction.customId);
            await interaction.editReply({ content: "Something went wrong, unfortunately. Please contact the developer." });
            return;
        }

        let reqs = await decomposeAlignmentSectorCodes([matches[2], matches[4], matches[6]]);
        let comlinkGuildId = matches[8];

        let { exclude, dsOps, mxOps, lsOps, rareOnly, priority } = await loadRequestDataByMessageId(interaction.message.id);

        let metadata = generateTbOpsMetadata(dsOps, mxOps, lsOps, exclude, rareOnly, priority);

        let cachedData = await cacheUtils.loadTbAssignmentsFromCache(matches[1], comlinkGuildId, metadata);

        let guildData;

        if (cachedData)
        {
            response = await getShowAssignmentsByPlanetResponse(reqs, 
                { 
                    cachedData: cachedData, 
                    guildData: guildData,
                    dsOps: dsOps,
                    lsOps: lsOps,
                    mxOps: mxOps,
                    allycodesToExclude: exclude,
                    rareOnly: rareOnly,
                    priority: priority
                });

            addComponentsToResponse(response, comlinkGuildId, matches[1]);

            let reply = await interaction.editReply(response);
            
            await saveTbOpsRequestData(reply, exclude, lsOps, dsOps, mxOps, rareOnly, priority);
            
            return;
        }

        
        let qsm = await queueUtils.getQueueSizeMessage(queueUtils.QUEUES.OPS_OLD, interaction.client.shard);
        const editMessage = await interaction.editReply(`Processing TB Ops request. ${qsm}`);
        
        await queueUtils.queueTbOpsJob(interaction.client.shard, 
        {
            fn: "handleShowAssignmentsByPlanetButton",
            comlinkGuildId: comlinkGuildId,
            channelId: editMessage.channelId ?? editMessage.channel.id,
            messageId: editMessage.id,
            userId: interaction.user.id,
            planetsToUse: reqs,
            dsOps: dsOps,
            lsOps: lsOps,
            mxOps: mxOps,
            allycodesToExclude: exclude,
            rareOnly: rareOnly,
            priority: priority,
            customIdAppend: matches[1]
        },
        {
            expireInHours: 2
        });
    },
    handleShowAssignmentsByPlayerButton: async function(interaction)
    {
        let regex = /tb:ops:aplayer:(((LS|MX|DS)[1-6]){1}((LS|MX|DS)[1-6]){0,1}((LS|MX|DS)[1-6]){0,1}):(.+)/

        let matches = regex.exec(interaction.customId);

        if (!matches || matches.length != 9) {
            // something went wrong
            logger.error("tb:ops:aplayer:handleButton interaction.customId doesn't match. customId is: " + interaction.customId);
            await interaction.editReply({ content: "Something went wrong, unfortunately. Please contact the developer." });
            return;
        }
        
        let reqs = await decomposeAlignmentSectorCodes([matches[2], matches[4], matches[6]]);
        let comlinkGuildId = matches[8];
        
        let { exclude, dsOps, mxOps, lsOps, rareOnly, priority } = await loadRequestDataByMessageId(interaction.message.id);

        let metadata = generateTbOpsMetadata(dsOps, mxOps, lsOps, exclude, rareOnly, priority);
        let cachedData = await cacheUtils.loadTbAssignmentsFromCache(matches[1], comlinkGuildId, metadata);

        let guildData;

        if (cachedData)
        {
            let response = await getShowAssignmentsByPlayerResponse(reqs, 
                { 
                    cachedData: cachedData, 
                    guildData: guildData,
                    dsOps: dsOps,
                    lsOps: lsOps,
                    mxOps: mxOps,
                    allycodesToExclude: exclude,
                    rareOnly: rareOnly,
                    priority: priority
                });
    
            addComponentsToResponse(response, comlinkGuildId, matches[1]);
    
            let reply = await interaction.editReply(response);
            
            await saveTbOpsRequestData(reply, exclude, lsOps, dsOps, mxOps, rareOnly, priority);

            return;
        }
        
        let qsm = await queueUtils.getQueueSizeMessage(queueUtils.QUEUES.OPS_OLD, interaction.client.shard);
        const editMessage = await interaction.editReply(`Processing TB Ops request. ${qsm}`);
        
        await queueUtils.queueTbOpsJob(interaction.client.shard, 
        {
            fn: "handleShowAssignmentsByPlayerButton",
            comlinkGuildId: comlinkGuildId,
            channelId: editMessage.channelId ?? editMessage.channel.id,
            messageId: editMessage.id,
            userId: interaction.user.id,
            planetsToUse: reqs,
            dsOps: dsOps,
            lsOps: lsOps,
            mxOps: mxOps,
            allycodesToExclude: exclude,
            rareOnly: rareOnly,
            priority: priority,
            customIdAppend: matches[1]
        },
        {
            expireInHours: 2
        });
    },
    handleAssignmentsAsJsonButton: async function(interaction)
    {
        let regex = /tb:ops:ebdl:(((LS|MX|DS)[1-6]){1}((LS|MX|DS)[1-6]){0,1}((LS|MX|DS)[1-6]){0,1}):(.+)/

        let matches = regex.exec(interaction.customId);

        if (!matches || matches.length != 9) {
            // something went wrong
            logger.error("tb:ops:ebdl:handleButton interaction.customId doesn't match. customId is: " + interaction.customId);
            await interaction.editReply({ content: "Something went wrong, unfortunately. Please contact the developer." });
            return;
        }
        
        let codes = [matches[2], matches[4], matches[6]];
        let reqs = await decomposeAlignmentSectorCodes(codes);
        let comlinkGuildId = matches[8];
        
        let { exclude, dsOps, mxOps, lsOps, rareOnly, priority } = await loadRequestDataByMessageId(interaction.message.id);

        let metadata = generateTbOpsMetadata(dsOps, mxOps, lsOps, exclude, rareOnly, priority);
        let cachedData = await cacheUtils.loadTbAssignmentsFromCache(matches[1], comlinkGuildId, metadata);

        if (!cachedData)
        {
            await interaction.editReply({ content: "Your assignments data was somehow lost. Please regenerate assignments and then tap the button again."});
            return;
        }
       
        let response = await mapAssignmentsToEchoBaseFormat(cachedData.operationAssignments, codes, matches[1]);

        await interaction.editReply(response);
    },
    handleSendAssignmentsButton: async function (interaction)
    {
    
    },
    handlePostAssignmentsButton: async function(interaction)
    {
    
    },
    handleButton: async function(interaction)
    {
        if (interaction.customId.startsWith("tb:ops:aplanet:"))
        {
            // show assignments
            await this.handleShowAssignmentsByPlanetButton(interaction);
            return;
        }

        if (interaction.customId.startsWith("tb:ops:aplayer:"))
        {
            // show assignments
            await this.handleShowAssignmentsByPlayerButton(interaction);
            return;
        }

        if (interaction.customId.startsWith("tb:ops:ebdl:"))
        {
            // download assignments as JSON compatible with echobase
            await this.handleAssignmentsAsJsonButton(interaction);
            return;
        }

        if (interaction.customId.startsWith("tb:ops:send"))
        {
            // send assignments as DM
            await this.handleSendAssignmentsButton(interaction);
            return;
        }

        if (interaction.customId.startsWith("tb:ops:post"))
        {
            // post assignments to a channel
            await this.handlePostAssignmentsButton(interaction);
            return;
        }

        let regex = /tb:ops:([1-6]):(.+)/

        let matches = regex.exec(interaction.customId);

        if (!matches || matches.length < 3) {
            // something went wrong
            logger.error("tb:ops:handleButton interaction.customId doesn't match. customId is: " + interaction.customId);
            await interaction.editReply({ content: "Something went wrong, unfortunately. I'm working on it, but please try again." });
            return;
        }

        let sector = parseInt(matches[1]);
        let comlinkGuildId = matches[2];
        let customIdAppend = await getCustomIdTextBySector(sector);

        let { exclude, dsOps, mxOps, lsOps, rareOnly, priority } = await loadRequestDataByMessageId(interaction.message.id);

        let metadata = generateTbOpsMetadata(dsOps, mxOps, lsOps, exclude, rareOnly, priority);
        let cachedData = await cacheUtils.loadTbAssignmentsFromCache(
            customIdAppend, 
            comlinkGuildId, 
            metadata);

        let planetsToUse = tbCommon.PLANETS.filter(p => p.sector === sector && !p.specialPlanet).map(p => p.name);
        if (cachedData)
        {
            let response = await getResponseForPlanets(planetsToUse, 
                { 
                    cachedData: cachedData, 
                    dsOps: dsOps,
                    mxOps: mxOps,
                    lsOps: lsOps,
                    allycodesToExclude: exclude,
                    rareOnly: rareOnly,
                    priority: priority
                });
    
            addComponentsToResponse(response, comlinkGuildId, customIdAppend);
            
            let reply = await interaction.editReply(response);
            
            await saveTbOpsRequestData(reply, exclude, lsOps, dsOps, mxOps, rareOnly, priority);

            return;
        }
        
        let qsm = await queueUtils.getQueueSizeMessage(queueUtils.QUEUES.OPS_OLD, interaction.client.shard);
        const editMessage = await interaction.editReply(`Processing TB Ops request. ${qsm}`);
        
        await queueUtils.queueTbOpsJob(interaction.client.shard, 
        {
            fn: "handleButton",
            comlinkGuildId: comlinkGuildId,
            channelId: editMessage.channelId ?? editMessage.channel.id,
            messageId: editMessage.id,
            userId: interaction.user.id,
            planetsToUse: planetsToUse,
            dsOps: dsOps,
            lsOps: lsOps,
            mxOps: mxOps,
            allycodesToExclude: exclude,
            rareOnly: rareOnly,
            priority: priority,
            customIdAppend: customIdAppend
        },
        {
            expireInHours: 2
        });
    },
    onSelect: async function(interaction)
    {
        await interaction.deferReply();

        let regex = /tb:ops:sel:(.+)/

        let matches = regex.exec(interaction.customId);

        if (!matches || matches.length < 2) {
            // something went wrong
            logger.error("tb:ops:onSelect interaction.customId doesn't match. customId is: " + interaction.customId);
            await interaction.editReply({ content: "Something went wrong, unfortunately. I'm working on it, but please try again." });
            return;
        }

        let comlinkGuildId = matches[1];
        let { exclude, dsOps, mxOps, lsOps, rareOnly, priority } = await loadRequestDataByMessageId(interaction.message.id);

        let metadata = generateTbOpsMetadata(dsOps, mxOps, lsOps, exclude, rareOnly, priority);
        let customIdAppend = await getCustomIdTextByPlanets(interaction.values);
        let cachedData = await cacheUtils.loadTbAssignmentsFromCache(
            customIdAppend, 
            comlinkGuildId, 
            metadata);

        if (cachedData)
        {
        
            let response = await getResponseForPlanets(interaction.values, 
                { 
                    cachedData: cachedData, 
                    dsOps: dsOps,
                    mxOps: mxOps,
                    lsOps: lsOps,
                    allycodesToExclude: exclude,
                    rareOnly: rareOnly,
                    priority: priority
                });
    
            addComponentsToResponse(response, comlinkGuildId, customIdAppend);
        
            let reply = await interaction.editReply(response);
            
            await saveTbOpsRequestData(reply, exclude, lsOps, dsOps, mxOps, rareOnly, priority);
            return;
        }
        
        let qsm = await queueUtils.getQueueSizeMessage(queueUtils.QUEUES.OPS_OLD, interaction.client.shard);
        const editMessage = await interaction.editReply(`Processing TB Ops request. ${qsm}`);
        
        await queueUtils.queueTbOpsJob(interaction.client.shard, 
        {
            fn: "onSelect",
            comlinkGuildId: comlinkGuildId,
            channelId: editMessage.channelId ?? editMessage.channel.id,
            messageId: editMessage.id,
            userId: interaction.user.id,
            planetsToUse: interaction.values,
            dsOps: dsOps,
            lsOps: lsOps,
            mxOps: mxOps,
            allycodesToExclude: exclude,
            rareOnly: rareOnly,
            priority: priority,
            customIdAppend: customIdAppend
        },
        {
            expireInHours: 2
        });
    }
}

async function getCustomIdTextByPlanets(planets)
{
    let opReqs = await getAllOperationRequirements();
    let str = "";
    for (let planet of planets) 
    {
        let mreq = opReqs.find(r => r.planet === planet);
        str += `${mreq.alignment}${mreq.sector}`
    }
    return str;
}

async function getCustomIdTextBySector(sector)
{
    let opReqs = await getAllOperationRequirements();
    let reqsForSector = [
        opReqs.find(r => r.sector === sector && r.alignment == tbCommon.ALIGNMENTS.DS),
        opReqs.find(r => r.sector === sector && r.alignment == tbCommon.ALIGNMENTS.MX),
        opReqs.find(r => r.sector === sector && r.alignment == tbCommon.ALIGNMENTS.LS)
    ]
    return getCustomIdTextByReqs(reqsForSector);
}

function getCustomIdTextByReqs(reqs)
{
    return reqs.map(r => `${r.alignment}${r.sector}`).join("");
}

async function decomposeAlignmentSectorCodes(codes)
{
    let reqs = new Array();
    let opReqs = await getAllOperationRequirements();
    for (let code of codes)
    {
        if (!code) continue;
        reqs.push(opReqs.find(r => code === `${r.alignment}${r.sector}`));
    }

    return reqs;
}

async function getFilteredReqsByPlanets(planets, dsOps, mxOps, lsOps)
{
    const opReqs = await getAllOperationRequirements();

    let planetReqs = opReqs.filter(r => {
        if (!planets.find(p => p === r.planet)) return false;

        // only include specified operations, if any
        if (dsOps && r.alignment === tbCommon.ALIGNMENTS.DS)
        {
            if (dsOps.indexOf(r.operation) === -1) return false; 
        } 
        else if (lsOps && r.alignment === tbCommon.ALIGNMENTS.LS)
        {
            if (lsOps.indexOf(r.operation) === -1) return false;
        }
        else if (mxOps && r.alignment === tbCommon.ALIGNMENTS.MX)
        {
            if (mxOps.indexOf(r.operation) === -1) return false;
        }

        return true;
    });

    return planetReqs;
}

async function getFilteredReqsByReqs(reqs, lsOps, dsOps, mxOps)
{
    let opReqs = await getAllOperationRequirements();
    let filteredReqs = new Array();
    for (var req of reqs)
    {
        filteredReqs.push(...opReqs.filter(r => {
            if (r.planet !== req.planet) return false;

            // only include specified operations, if any
            if (dsOps && r.alignment === tbCommon.ALIGNMENTS.DS && dsOps.indexOf(r.operation) === -1) return false;
            else if (lsOps && r.alignment === tbCommon.ALIGNMENTS.LS && lsOps.indexOf(r.operation) === -1) return false;
            else if (mxOps && r.alignment === tbCommon.ALIGNMENTS.MX && mxOps.indexOf(r.operation) === -1) return false;

            return true;        
        }));
    }

    return filteredReqs;
}

const MAX_COST = 10000;

function getModifiedUnitDefId(unitDefId, minRelicLevel)
{
    return `${unitDefId}_R${minRelicLevel.toString()}`;
}

async function getAssignmentsAndMissingFromReqs2(reqs, guildData, allycodesToExclude, rareOnly, priority)
{
    // first, find the rare units.
    // Second, go operation by operation, check if they can be filled, catalog the "cost" in terms of rare units
    // sort by cost ASC
    // fill operations, cataloging assignments and missing

    // set up players for storing operation assignment data
    guildData.players.forEach((p) => {
        p.tbops = {
            rareAssignmentsCount: 0,
            glAssignmentsCount: 0,
            assignments: null,
            assignmentsByPlanet: {}
        }
    });

    let unitsToFill = {};
    let allOperations = [];
    for (var req of reqs)
    {
        let op = allOperations.find(o => o.planet === req.planet && o.sector === req.sector && o.operation === req.operation);
        if (!op)
        {
            op = {
                planet: req.planet,
                sector: req.sector,
                reqs: new Array(),
                operation: req.operation,
                cost: 0 // for later
            };

            allOperations.push(op);
        }

        op.reqs.push(req);

        if (!unitsToFill[getModifiedUnitDefId(req.unitDefId, req.minRelicLevel)]) 
        {
            let unitToFillObj = createAndPopulateUnitToFillObj(guildData.players, req, allycodesToExclude);

            unitsToFill[getModifiedUnitDefId(req.unitDefId, req.minRelicLevel)] = unitToFillObj;
        } else {
            unitsToFill[getModifiedUnitDefId(req.unitDefId, req.minRelicLevel)].needed++;
        }
    }

    let assignments = {};

    let madePreAssignment = false;
    let utfKeys = Object.keys(unitsToFill);
    // loop through all unitToFill and sort the playersMatched by rareAssignmentsCount ASC and relicLevel ASC (if relevant)
    for (let key of utfKeys)
    {
        let unitToFillObj = unitsToFill[key];

        // if the same unit is required at different relic levels, then handle it later 
        let utfKeysWithSameDefId = utfKeys.filter(k => k.startsWith(unitToFillObj.unitDefId));
        if (utfKeysWithSameDefId.length > 1)
        {
            let isOk = false;
            for (let otherUtfKey of utfKeysWithSameDefId)
            {
                if (key === otherUtfKey) continue; // no need to check the current one
                if (unitsToFill[otherUtfKey].length > 1)
                {
                    isOk = true;
                    // it's ok, because there's more than one possible for another assignment
                    continue;
                }

                isOk = false;
                break;
            }

            if (!isOk) continue;
        }

        // pre-assign when there's only one match and there's only 1 needed.
        // later, handle if the player has > 10 "one match" assignments on a single planet
        if (unitToFillObj.playersMatched.length === 1 && unitToFillObj.needed === 1)
        {
            let req = reqs.find(r => r.unitDefId === unitToFillObj.unitDefId && r.minRelicLevel === unitToFillObj.minRelicLevel);
            let match = unitToFillObj.playersMatched[0];

            if (canMakeAssignment(assignments, match, req))
            {

                makeAssignment(assignments, match, req);

                unitToFillObj.needed--;
                unitToFillObj.playersAssigned.push(match);
    
                madePreAssignment = true;
    
                // no need to sort since only one match
                continue;
            }
        }
        
        await sortMatchingPlayers(unitToFillObj);
    }
    
    let playersToSkip = [];
    // if someone is assigned too many, then remove all assignments for them and make sure we don't try them again as a preassignment
    playersToSkip.push(...removeAllAssignmentsForPlayersWithTooMany(guildData.players));

    // go through all planets. Including a depth in case the logic is fucked in some way and we'd otherwise get an infinite loop
    let depth = 0;
    do {
        madePreAssignment = false;
        for (let req of reqs)
        {
            if (req.assignment) continue;

            let key = getModifiedUnitDefId(req.unitDefId, req.minRelicLevel);
            let unitToFillObj = unitsToFill[key];

            let viablePlayersForPlanet = unitToFillObj.playersViableForPlanet(req.planet);
            if (viablePlayersForPlanet.length === 1 && unitToFillObj.needed === 1)
            {
                
                // if the same unit is required at different relic levels, then handle it later 
                let utfKeysWithSameDefId = utfKeys.filter(k => k.startsWith(unitToFillObj.unitDefId));
                if (utfKeysWithSameDefId.length > 1)
                {
                    let isOk = false;
                    for (let otherUtfKey of utfKeysWithSameDefId)
                    {
                        if (key === otherUtfKey) continue; // no need to check the current one
                        if (unitsToFill[otherUtfKey].length > 1)
                        {
                            isOk = true;
                            // it's ok, because there's more than one possible for another assignment
                            continue;
                        }

                        isOk = false;
                        break;
                    }

                    if (!isOk) continue;
                }
                
                if (!canMakeAssignment(assignments, viablePlayersForPlanet[0], req))
                {
                    unitToFillObj.nextAssignmentIndex++;
                    continue;
                }

                if (playersToSkip.indexOf(viablePlayersForPlanet[0]) != -1) continue;

                makeAssignment(assignments, viablePlayersForPlanet[0], req);
                unitToFillObj.playersAssigned.push(viablePlayersForPlanet[0]);
                unitToFillObj.needed--;
                madePreAssignment = true;
            }
        }
        
        // if someone is assigned too many, then remove all assignments for them
        playersToSkip.push(...removeAllAssignmentsForPlayersWithTooMany(guildData.players));

    } while (madePreAssignment && ++depth < 10);

    if (depth > 0)
    {
        logger.info(`TB Ops for ${guildData.guildName} (${guildData.comlinkGuildId}) (${swapiUtils.getPlayerAllycode(guildData.players[0])}) ran with depth ${depth}`);
    }

    // at this point, unitsToFill contains a count of fulfillment for every req, 
    // so we can easily see what's under the needed amount. The "rare" units are those under needed and above zero
    allOperations = recalcOperationsCosts(allOperations, 0, unitsToFill, priority);

    // now we can fill the operations, cataloging assignments
    // when we find the first operation that can't be filled, we catalog the missing along with closest
    let missing = [];
    for (var opIx = 0; opIx < allOperations.length; opIx++)
    {
        let op = allOperations[opIx];
        for (var req of op.reqs)
        {
            if (req.assignment) continue;

            let unitToFillObj = unitsToFill[getModifiedUnitDefId(req.unitDefId, req.minRelicLevel)];

            if (rareOnly && unitToFillObj.playersMatched.length > 0 && unitToFillObj.playersMatched.length >= unitToFillObj.needed + rareOnly)
            {
                req.assignment = { 
                    playerName: UNASSIGNED_NAME,
                    allycode: UNASSIGNED_ALLYCODE
                };
                continue;
            }

            await sortMatchingPlayers(unitToFillObj, req);
            let match;
            let matchedNotAssigned = unitToFillObj.playersMatchedNotAssigned();
            for (let ix = 0; ix < matchedNotAssigned.length; ix++)
            {
                match = matchedNotAssigned[ix];

                if (!canMakeAssignment(assignments, match, req)) continue;

                break;
            }

            // no match at all
            if (!match) 
            {
                // can't fill any more, so catalog the closest
                if (unitToFillObj.playersClose.length > 0)
                {
                    req.closest = unitToFillObj.playersClose.slice(0, 2);
                }

                missing.push(req);  

                continue;
            }

            // there was a match, so make the assignment
            makeAssignment(assignments, match, req);
            unitToFillObj.playersAssigned.push(match);
            unitToFillObj.needed--;
        }

        allOperations = recalcOperationsCosts(allOperations, opIx + 1, unitsToFill, priority);
    }

    return { missing: missing, operationAssignments: allOperations, playerAssignments: assignments };
}

function createAndPopulateUnitToFillObj(players, req, allycodesToExclude)
{
    let unitToFillObj = {
        needed: 1,
        unitDefId: req.unitDefId,
        unitName: req.unitName,
        isGL: req.isGL,
        minRelicLevel: req.minRelicLevel,
        combatType: 0,
        nextAssignmentIndex: 0,
        playersMatched: new Array(),
        playersClose: new Array(),
        playersAssigned: new Array(),
        playersMatchedNotAssigned: function() { return this.playersMatched.filter(p => !this.playersAssigned.find(a => a === p)); },
        isLessThanNeeded: function() { return (this.playersMatched.length - this.playersAssigned.length) > 0 && this.playersMatched.length < this.needed; },
        isExactFill: function() { return (this.playersMatched.length > 0) && (this.playersMatched.length - this.playersAssigned.length === this.needed); },
            // (this.playersMatched.length - this.nextAssignmentIndex) > 0 && this.playersMatched.length === this.needed; },
        isImpossible: function() { return this.playersMatched.length === this.playersAssigned.length },
        playersViableForPlanet: function(planet) { return this.playersMatchedNotAssigned().filter(player => (player.tbops?.assignmentsByPlanet[planet]?.length ?? 0) < MAX_ASSIGNMENTS_PER_PLAYER_PER_PLANET); }
    };

    players.forEach(p => {
        // check if player should be excluded
        if (allycodesToExclude?.find(ac => ac === swapiUtils.getPlayerAllycode(p))) return;

        let roster = swapiUtils.getPlayerRoster(p);
        let ru = roster.find(u => swapiUtils.getUnitDefId(u) === req.unitDefId);

        if (!ru) return;

        // it's a ship
        if (swapiUtils.getUnitCombatType(ru) === constants.COMBAT_TYPES.FLEET)
        {
            unitToFillObj.combatType = constants.COMBAT_TYPES.FLEET;
            if (swapiUtils.getUnitRarity(ru) >= 7) unitToFillObj.playersMatched.push(p); // 7*, match
            else unitToFillObj.playersClose.push(
                { 
                    allycode: swapiUtils.getPlayerAllycode(p),
                    playerName: swapiUtils.getPlayerName(p), 
                    rarity: swapiUtils.getUnitRarity(ru) 
                }); // otherwise, they're considered close
            
            return;
        }

        unitToFillObj.combatType = constants.COMBAT_TYPES.SQUAD;

        // it's a character at least at the right relic level
        if (swapiUtils.getUnitRelicLevel(ru) - 2 >= req.minRelicLevel) {
            unitToFillObj.playersMatched.push(p);
            return;
        }

        // considered close if G13
        if (swapiUtils.getUnitGearLevel(ru) >= 13) {
            unitToFillObj.playersClose.push(
                { 
                    playerName: swapiUtils.getPlayerName(p), 
                    relicLevel: swapiUtils.getUnitRelicLevel(ru) - 2
                }
            );
        }
    });

    // if the requirement is within a rarity threshold, then increase the "rareAssignmentsCount" score of each player that matches
    if (unitToFillObj.playersMatched.length > 0 && unitToFillObj.playersMatched.length < unitToFillObj.needed + 5)
    {
        unitToFillObj.playersMatched.forEach((p) => p.tbops.rareAssignmentsCount++);
    }

    // randomize the order of all "playersClose" so it's not sorted by when players joined the guild
    unitToFillObj.playersClose.sort((a, b) => Math.random() - 0.5 ); 

    // sort playersClose by rarity/relic
    if (unitToFillObj.combatType === constants.COMBAT_TYPES.FLEET) {
        unitToFillObj.playersClose.sort((a, b) => b.rarity - a.rarity)
    }
    else 
    {
        // sort closest from high to low
        unitToFillObj.playersClose.sort((a, b) => b.relicLevel - a.relicLevel);
    }

    return unitToFillObj;
}

function removeAllAssignmentsForPlayersWithTooMany(players)
{
    let playersWithAssignmentsRemoved = [];
    for (let p of players)
    {
        if (p.tbops.assignments === null || p.tbops.assignments.length === 0) continue;

        if (p.tbops.assignments.length > MAX_ASSIGNMENTS_PER_PLAYER)
        {
            // remove them all
            for (let reqToUnassign of p.tbops.assignments)
            {
                reqToUnassign.assignment = null;
            }

            p.tbops.assignments.splice(0, p.tbops.assignments.length);

            p.tbops.assignmentsByPlanet = {};

            playersWithAssignmentsRemoved.push(p);
            continue;
        }

        for (let planet of Object.keys(p.tbops.assignmentsByPlanet))
        {
            if (p.tbops.assignmentsByPlanet[planet]?.length > MAX_ASSIGNMENTS_PER_PLAYER_PER_PLANET) 
            {
                // remove all for this planet
                for (let a = 0; a < p.tbops.assignments.length; )
                {
                    let req = p.tbops.assignments[a];
                    if (req.planet === planet)
                    {
                        req.assignment = null;
                        p.tbops.assignments.splice(a, 1);
                    } else {
                        a++;
                    }
                }

                p.tbops.assignmentsByPlanet[planet] = new Array();
            
                playersWithAssignmentsRemoved.push(p);
            }
        }
    }

    return playersWithAssignmentsRemoved;
}

function canMakeAssignment(assignments, player, req)
{
    let existingAssignmentsForPlayer = assignments[swapiUtils.getPlayerAllycode(player)];
                
    // verify whether 
    // 1. the player has max assignments
    // 2. the player has max assignments for the planet
    // 3. the unit has already been assigned to a player. This is relevant when there are
    //    multiple sectors in play (different relic levels) for the same unit
    if (existingAssignmentsForPlayer?.hasMaxAssignments() || 
        existingAssignmentsForPlayer?.hasMaxAssignmentsForPlanet(req.planet) ||
        existingAssignmentsForPlayer?.reqsAssigned.find(ar => ar.unitDefId === req.unitDefId))
    {
        return false;
    }

    return true;
}

function makeAssignment(assignments, match, req)
{
    if (!assignments[swapiUtils.getPlayerAllycode(match)])
    { 
        let assignmentsObj = {
            reqsAssigned: [req],
            playerName: swapiUtils.getPlayerName(match),
            playerAllycode: swapiUtils.getPlayerAllycode(match),
            hasMaxAssignmentsForPlanet: function(p) { return this.reqsAssigned.filter(r => r.planet === p).length === MAX_ASSIGNMENTS_PER_PLAYER_PER_PLANET; },
            hasMaxAssignments: function() { return this.reqsAssigned.length === MAX_ASSIGNMENTS_PER_PLAYER; }
        }
        
        assignments[swapiUtils.getPlayerAllycode(match)] = assignmentsObj;

        match.tbops.assignments = assignmentsObj.reqsAssigned;
    }
    else 
    {
        assignments[swapiUtils.getPlayerAllycode(match)].reqsAssigned.push(req);
    }

    if (req.isGL) match.tbops.glAssignmentsCount++;

    req.assignment = { 
        playerName: swapiUtils.getPlayerName(match), 
        allycode: swapiUtils.getPlayerAllycode(match)
    };

    if (match.tbops.assignmentsByPlanet[req.planet])
    {
        match.tbops.assignmentsByPlanet[req.planet].push(req);
    } 
    else 
    {
        match.tbops.assignmentsByPlanet[req.planet] = [req];

    }
}

function recalcOperationsCosts(allOperations, startIndex, unitsToFill, priority)
{
    if (startIndex >= allOperations.length) return allOperations;

    for (let opIx = startIndex; opIx < allOperations.length; opIx++)
    {
        let op = allOperations[opIx];
        let reqsQuantity = {};
        for (var req of op.reqs)
        {
            if (req.assignment) continue;
            let modDefId = getModifiedUnitDefId(req.unitDefId, req.minRelicLevel);
            if (reqsQuantity[modDefId]) reqsQuantity[modDefId]++;
            else reqsQuantity[modDefId] = 1;
        }

        for (let modDefId of Object.keys(reqsQuantity))
        {
            let utf = unitsToFill[modDefId];
            let qtyNeededForOp = reqsQuantity[modDefId];
            let qtyMatchesNotAssigned = utf.playersMatchedNotAssigned().length;
            // this is to handle if a unit is required more than once for a single operation
            if (qtyMatchesNotAssigned < qtyNeededForOp)
            {
                op.cost = MAX_COST;
                break;
            }

            // if (qtyMatchesNotAssigned === qtyNeededForOp) op.cost++;

            // if there are assignments that only a single player can fill, do those earlier
            if (utf.isExactFill()) op.cost += qtyNeededForOp;

            // when there's less than the number needed remaining, fill it later
            if (utf.isLessThanNeeded()) 
            {
                op.cost += (16*qtyNeededForOp);
            }
        }

        // it's impossible, so don't bother looking further
        if (op.cost === MAX_COST) continue;
    }

    return subSort(allOperations, startIndex, allOperations.length, (priority === OPERATION_PRIORITY.MOST_POINTS_FIRST) ? SORT_BY_POINTS_FUNC : SORT_BY_EASIEST_FUNC);
}

const SORT_BY_POINTS_FUNC = (a, b) => {
    if (a.cost === MAX_COST) return 1;
    if (b.cost === MAX_COST) return -1;

    if (a.order != null && b.order != null) return a.order - b.order;
    if (a.sector === b.sector) return a.cost - b.cost;
    
    return b.sector - a.sector;
}
const SORT_BY_EASIEST_FUNC = (a, b) => {
    return a.cost - b.cost;
}

async function sortMatchingPlayers(unitToFillObj, req)
{
    if (!req)
    {
        unitToFillObj.playersMatched.sort(
            (a, b) => 
            {
                let omiCountDiff = 0, relicLevelDiff = 0;
                if (unitToFillObj.combatType === constants.COMBAT_TYPES.SQUAD) 
                {
                    let aUnit = swapiUtils.getPlayerRoster(a).find(u => swapiUtils.getUnitDefId(u) === unitToFillObj.unitDefId);
                    let bUnit = swapiUtils.getPlayerRoster(b).find(u => swapiUtils.getUnitDefId(u) === unitToFillObj.unitDefId);
    
                    // units with omicrons should be lower priority to place (sort by omi count, low to high)
                    omiCountDiff = (aUnit?.omicron_abilities?.length ?? 0) - (bUnit?.omicron_abilities?.length ?? 0);
                    relicLevelDiff = swapiUtils.getUnitRelicLevel(aUnit) - swapiUtils.getUnitRelicLevel(bUnit);
                }
                

                if (omiCountDiff != 0) return omiCountDiff;
                if (a.tbops.rareAssignmentsCount != b.tbops.rareAssignmentsCount) return a.tbops.rareAssignmentsCount - b.tbops.rareAssignmentsCount;
                if (unitToFillObj.isGL && (a.tbops.glAssignmentsCount != b.tbops.glAssignmentsCount)) return a.tbops.glAssignmentsCount - b.tbops.glAssignmentsCount;
                if (relicLevelDiff != 0) return relicLevelDiff;

                return Math.random() - 0.5;
            }); 
    
    } 
    else if (unitToFillObj.playersAssigned.length < unitToFillObj.playersMatched.length - 1)
    {
        // there's more than one player that's matched and not assigned, so resort the players to balance out the assignments
        unitToFillObj.playersMatched.sort(
            (a, b) => 
            {
                let omiCountDiff = 0, relicLevelDiff = 0;
                if (unitToFillObj.combatType === constants.COMBAT_TYPES.SQUAD) 
                {
                    let aUnit = swapiUtils.getPlayerRoster(a).find(u => swapiUtils.getUnitDefId(u) === unitToFillObj.unitDefId);
                    let bUnit = swapiUtils.getPlayerRoster(b).find(u => swapiUtils.getUnitDefId(u) === unitToFillObj.unitDefId);

                    // units with omicrons should be lower priority to place (sort by omi count, low to high)
                    omiCountDiff = (aUnit?.omicron_abilities?.length ?? 0) - (bUnit?.omicron_abilities?.length ?? 0);
                    relicLevelDiff = swapiUtils.getUnitRelicLevel(aUnit) - swapiUtils.getUnitRelicLevel(bUnit);
                }
                
                if (omiCountDiff != 0) return omiCountDiff;

                let planetAssignmentsA = a.tbops.assignmentsByPlanet[req.planet]?.length ?? 0;
                let planetAssignmentsB = b.tbops.assignmentsByPlanet[req.planet]?.length ?? 0;

                if (planetAssignmentsA != planetAssignmentsB) return planetAssignmentsA - planetAssignmentsB;
                
                if (unitToFillObj.isGL && (a.tbops.glAssignmentsCount != b.tbops.glAssignmentsCount)) return a.tbops.glAssignmentsCount - b.tbops.glAssignmentsCount;
                if (relicLevelDiff != 0) return relicLevelDiff;

                return Math.random() - 0.5;
            }); 
    }
    
}

let subSort = (arr, i, n, sortFx) => [].concat(...arr.slice(0, i), ...arr.slice(i, i + n).sort(sortFx), ...arr.slice(i + n, arr.length));

async function getFields(missingReqs, maxMissingToShow, opsToInclude)
{
    let unitsList = await swapiUtils.getUnitsList();

    let fields = [];

    let trimmedMissingReqs = missingReqs.slice(0, maxMissingToShow);

    for (let op = 1; op <= 6; op++)
    {
        if (opsToInclude && opsToInclude.indexOf(op.toString()) === -1) continue;

        let missingOpReqs = trimmedMissingReqs.filter(mreq => mreq.operation === op);
        let tableContents = missingOpReqs.map(mreq => {
                let closestString = "\n";
                let minString;
                if (mreq.closest && mreq.closest.length > 0)
                {
                    if (mreq.closest[0].relicLevel != null) minString = `R${mreq.minRelicLevel.toString()}`;
                    else minString = "7*";

                    for (var c = 0; c < mreq.closest.length; c++)
                    {
                        let cReq = mreq.closest[c];
                        if (c%2 === 0) closestString += "+> ";
                        let append = cReq.rarity ? cReq.rarity + "*" : "R" + cReq.relicLevel;
                        closestString += `${cReq.playerName.slice(0, 15)} (${append})` 
                        if (c%2 === 0 && c != mreq.closest.length - 1) closestString += ", ";
                    }
                } 
                else {
                    let unit = unitsList.find(u => swapiUtils.getUnitDefId(u) === mreq.unitDefId);
                    if (swapiUtils.getUnitCombatType(unit) === constants.COMBAT_TYPES.FLEET) minString = "7*";
                    else minString = `R${mreq.minRelicLevel.toString()}`;

                    closestString += "-> None close"
                }

                return `${minString} ${mreq.unitName.substring(0, 25)}${closestString}`;
            }).join("\n");

        let description;
        let missingOpsCount = missingReqs.filter(mr => mr.operation === op).length;
        if (tableContents)
        {
            description = 
`\`\`\`diff
${tableContents}
\`\`\``;
        } 
        else if (missingOpsCount > 0) 
        {
            description = "Too much to show.";
        }
        else {
            description = "All set!"
        }

        fields.push({
            name: `Op #${op} - Missing ${missingOpsCount}/15`,
            value: description,
            inline: false
        })
    }
    return fields;
}

async function getResponseForPlanets(planets, 
    { 
        cachedData = null, 
        guildData = null, 
        dsOps = null, 
        mxOps = null, 
        lsOps = null, 
        allycodesToExclude = null, 
        rareOnly = null, 
        priority = OPERATION_PRIORITY.MOST_POINTS_FIRST }
    )
{
    let resultsObj;
    let planetReqs = await getFilteredReqsByPlanets(planets, dsOps, mxOps, lsOps);
    if (cachedData != null)
    {
        resultsObj = cachedData;
    } else {
        resultsObj = await getAssignmentsAndMissingFromReqs2(planetReqs, guildData, allycodesToExclude, rareOnly, priority);
        let metadata = generateTbOpsMetadata(dsOps, mxOps, lsOps, allycodesToExclude, rareOnly, priority);
        await cacheUtils.cacheTbAssignments(
            await getCustomIdTextByPlanets(planets),
            metadata,
            resultsObj.missing,
            resultsObj.operationAssignments,
            resultsObj.playerAssignments,
            guildData.comlinkGuildId
        );
    }
    
    let embeds = [];

    let tooManyToShow = false;

    for (var p of planets) 
    {    
        let filteredMissing = planets.length > 1 ? resultsObj.missing.filter(mreq => mreq.planet === p) : resultsObj.missing;

        let firstReq = planetReqs.find(pl => pl.planet === p);
        let alignment = firstReq.alignment;
        let sector = firstReq.sector;

        let opsToInclude = null;
        switch (alignment)
        {
            case tbCommon.ALIGNMENTS.DS: opsToInclude = dsOps; break;
            case tbCommon.ALIGNMENTS.MX: opsToInclude = mxOps; break;
            case tbCommon.ALIGNMENTS.LS: opsToInclude = lsOps; break;
        }

        let maxMissingToShow = MAX_MISSING_TO_SHOW/planets.length;
        let fields = await getFields(filteredMissing, maxMissingToShow, opsToInclude);

        if (filteredMissing.length > maxMissingToShow) tooManyToShow = true;

        let embed = new EmbedBuilder()
            .setTitle(`Missing Units: (${alignment} ${sector}) ${p}`)
            .setFields(fields)
            .setColor("Yellow");

        embeds.push(embed);
    }

    if (tooManyToShow || resultsObj.missing.length > MAX_MISSING_TO_SHOW)
    {
        embeds.push(getTooManyMissingAssignmentsEmbed())    
    }

    return {
        embeds: embeds
    };
}

function getTooManyMissingAssignmentsEmbed()
{
    return new EmbedBuilder()
        .setTitle("Too Many Missing")
        .setDescription(`Only showing ${MAX_MISSING_TO_SHOW/3} missing assignments per planet. To see all, choose one planet from the menu below.`)
        .setColor("Red");
}

async function getAllOperationRequirements()
{
    
    let data = metadataCache.get(metadataCache.CACHE_KEYS.operationRequirements);
    if (data == undefined)
    {
        data = await database.db.any(
            `SELECT * FROM operation_req
            ORDER BY sector,planet,operation,row,slot
            `)

        metadataCache.set(metadataCache.CACHE_KEYS.operationRequirements, data, 2592000000); // 30 days, whatever
    }
    
    let opReqs = data.map(r => {
        return {
            id: r.operation_req_id,
            alignment: r.alignment,
            sector: r.sector,
            planet: r.planet,
            operation: r.operation,
            row: r.row,
            slot: r.slot,
            unitName: r.unit_name,
            unitDefId: r.unit_def_id,
            minRelicLevel: r.min_relic_level,
            isGL: r.is_gl
        }
    });
    
    return opReqs;
}


async function getShowAssignmentsByPlanetResponse(reqs, 
    { 
        cachedData = null, 
        guildData = null, 
        dsOps = null, 
        mxOps = null, 
        lsOps = null, 
        allycodesToExclude = null, 
        rareOnly = null, 
        priority = OPERATION_PRIORITY.MOST_POINTS_FIRST 
    })
{
    let resultsObj;
    if (cachedData != null)
    {
        resultsObj = cachedData;
    } else {
        let filteredReqs = await getFilteredReqsByReqs(reqs);
        resultsObj = await getAssignmentsAndMissingFromReqs2(filteredReqs, guildData, allycodesToExclude, rareOnly, priority);

        let metadata = generateTbOpsMetadata(dsOps, mxOps, lsOps, allycodesToExclude, rareOnly, priority);
        await cacheUtils.cacheTbAssignments(
            await getCustomIdTextByReqs(reqs),
            metadata,
            resultsObj.missing,
            resultsObj.operationAssignments,
            resultsObj.playerAssignments,
            guildData.comlinkGuildId
        );
    }
    
    const width = 1200;
    const height = 1100;
    let embeds = [], files = [];
    let csvData = [];
    let alignmentSectorString = "";

    for (var req of reqs)
    {
        let filteredOpAssignments = resultsObj.operationAssignments.filter(o => o.planet === req.planet);
        filteredOpAssignments.sort((a, b) => {
            if (a.operation != b.operation) return a.operation - b.operation;
            if (a.row != b.row) return a.row - b.row;
            return a.slot - b.slot;
        })
        
        const canvas = createCanvas(width, height);
        const context = canvas.getContext('2d')
        
        context.fillStyle = "rgb(0, 0, 0)"
        context.fillRect(0, 0, width, height)
        

        for (var o = 0; o < filteredOpAssignments.length; o++)
        {

            let opAssignment = filteredOpAssignments[o];

            context.font = "20pt Arial"
            context.textAlign = "center"
            context.fillStyle = "#fff"
            context.fillText(`Operation ${opAssignment.operation}`,
                Math.floor(o/3) * ((PORTRAIT_WIDTH+SPACE_BETWEEN_PORTRAITS) * 5 + 100) + (PORTRAIT_WIDTH + SPACE_BETWEEN_PORTRAITS) * 2.5,
                Math.floor(o%3)*3 * (PORTRAIT_HEIGHT + SPACE_BETWEEN_PORTRAITS) + (o%3+0.5)*SPACE_BETWEEN_PORTRAITS);


            // first, check whether all assignments are filled. If any aren't, we're going to scratch the operation
            let opFilled = opAssignment.reqs.filter(r => r.assignment == null).length === 0;

            for (var r = 0; r < opAssignment.reqs.length; r++)
            {
                let x = SPACE_BETWEEN_PORTRAITS/2 + Math.floor(o/3) * ((PORTRAIT_WIDTH+SPACE_BETWEEN_PORTRAITS) * 5 + 100) + r%5 * (PORTRAIT_WIDTH + SPACE_BETWEEN_PORTRAITS);
                let y = Math.floor((o%3)*3 + r/5) * (PORTRAIT_HEIGHT + SPACE_BETWEEN_PORTRAITS) + (o%3 + 1)*SPACE_BETWEEN_PORTRAITS;
                let or = opAssignment.reqs[r];
                let portrait = await unitsUtils.getPortraitForUnitByDefId(or.unitDefId);

                let img = portrait ?? './src/img/no-portrait.png'
                context.fillStyle = "rgb(255, 255, 255)"
                context.fillRect(x, y, PORTRAIT_WIDTH+2, PORTRAIT_HEIGHT+2);

                const unitIconImg = await loadImage(img);
                context.drawImage(unitIconImg, 
                    x + 1, 
                    y + 1, 
                    PORTRAIT_WIDTH, PORTRAIT_HEIGHT);

                let csvDataObj;
                if (opFilled)
                {
                    context.fillStyle = "rgb(90, 0, 90)"
                    context.fillRect(x - SPACE_BETWEEN_PORTRAITS/2 + 1, y + PORTRAIT_HEIGHT - 10, PORTRAIT_WIDTH + SPACE_BETWEEN_PORTRAITS - 1, 22);

                    if (rareOnly)
                    {
                        context.fillStyle = (or.assignment.allycode === UNASSIGNED_ALLYCODE) ? "rgb(0, 0, 0)" : "rgb(30, 100, 0)";
                    } else {
                        context.fillStyle = "rgb(0, 0, 0)"
                    }
                    context.fillRect(x - SPACE_BETWEEN_PORTRAITS/2 + 2, y + PORTRAIT_HEIGHT - 9, PORTRAIT_WIDTH + SPACE_BETWEEN_PORTRAITS - 3, 20);

                    context.font = `12pt "Noto Sans","Noto Sans SC","Noto Sans Armenian","Noto Sans Thai","Noto Sans Arabic","Noto Sans Korean"`
                    context.textAlign = "center"
                    context.fillStyle = "#fff"
                    context.measureText()
                    context.fillText(
                        or.assignment.playerName,
                        x + PORTRAIT_WIDTH/2, 
                        y + PORTRAIT_HEIGHT + 7,
                        PORTRAIT_WIDTH + SPACE_BETWEEN_PORTRAITS - 7);

                    csvDataObj = {
                        "Planet": req.planet,
                        "Operation": or.operation,
                        "Row": or.row,
                        "Col": or.slot,
                        "Unit": or.unitName,
                        "Player": or.assignment.playerName
                    };
                    
                } 
                else 
                {
                    csvDataObj = {
                        "Planet": req.planet,
                        "Operation": or.operation,
                        "Row": or.row,
                        "Col": or.slot,
                        "Unit": or.unitName,
                        "Player": null
                    };
                    
                }
                csvData.push(csvDataObj);
            }

            if (!opFilled)
            {
                let x = SPACE_BETWEEN_PORTRAITS/2 + Math.floor(o/3) * ((PORTRAIT_WIDTH+SPACE_BETWEEN_PORTRAITS) * 5 + 100);
                let y = SPACE_BETWEEN_PORTRAITS + (o%3) * ((PORTRAIT_HEIGHT + SPACE_BETWEEN_PORTRAITS) * 3 + SPACE_BETWEEN_PORTRAITS);
                context.fillStyle = "rgba(200,0,0,.3)"
                context.fillRect(x, y, 
                    (PORTRAIT_WIDTH + SPACE_BETWEEN_PORTRAITS)*5 - SPACE_BETWEEN_PORTRAITS + 2, 
                    (PORTRAIT_HEIGHT + SPACE_BETWEEN_PORTRAITS)*3 - SPACE_BETWEEN_PORTRAITS + 2);
            }
        }
        
        let embed = new EmbedBuilder()
            .setTitle(`Operation Assignments - (${req.alignment} ${req.sector}) ${req.planet}`)
            .setColor(tbCommon.ALIGNMENT_COLOR_MAP[req.alignment].number);
    
        let fileName = `tb-ops-${req.alignment}${req.sector}.png`
        const file = new AttachmentBuilder(canvas.toBuffer("image/png"), { name: fileName });
        embed.setImage("attachment://" + fileName)
        files.push(file);
        embeds.push(embed);

        alignmentSectorString += `${req.alignment}${req.sector}`;
    }

    if (csvData.length > 0)
    {
        const csvParsingOptions = { defaultValue: 0 }
        let csvText = await parseAsync(csvData, csvParsingOptions);
        let buffer = Buffer.from(csvText);
        let csvAttachment = new AttachmentBuilder(buffer, { name: `rote_ops_${alignmentSectorString}_${new Date().getTime()}.csv`});

        files.push(csvAttachment);
    }
    return { embeds: embeds, files: files };
}

function getSortValueForAlignment(a)
{
    switch (a)
    {
        case "DS": return 0;
        case "MX": return 1;
        case "LS": return 2;
    }

    return 0;
}

async function getShowAssignmentsByPlayerResponse(reqs, { cachedData = null, guildData = null, dsOps = null, mxOps = null, lsOps = null, allycodesToExclude = null, rareOnly = null, priority = OPERATION_PRIORITY.MOST_POINTS_FIRST })
{
    let resultsObj;
    if (cachedData != null)
    {
        resultsObj = cachedData;
    } else {
        let filteredReqs = await getFilteredReqsByReqs(reqs);
        resultsObj = await getAssignmentsAndMissingFromReqs2(filteredReqs, guildData, allycodesToExclude, rareOnly, priority);

        let metadata = generateTbOpsMetadata(dsOps, mxOps, lsOps, allycodesToExclude, rareOnly, priority);
        await cacheUtils.cacheTbAssignments(
            await getCustomIdTextByReqs(reqs),
            metadata,
            resultsObj.missing,
            resultsObj.operationAssignments,
            resultsObj.playerAssignments,
            guildData.comlinkGuildId
        );
    }
    
    let allycodes = Object.keys(resultsObj.playerAssignments).sort((a, b) => resultsObj.playerAssignments[a].playerName.localeCompare(resultsObj.playerAssignments[b].playerName));

    let impossibleOps = [];
    for (let o of resultsObj.operationAssignments)
    {
        // first, check whether all assignments are filled. If any aren't, we're going to scratch the operation
        let opFilled = o.reqs.filter(r => r.assignment == null).length === 0;
        if (!opFilled) // impossible
        {
            impossibleOps.push({ planet: o.planet, operation: o.operation });
        }
    }

    let portraitRows = 0;
    let maxAssignmentsPerAlignment = 0;
    let allycodesWithAssignments = new Array();;
    for (var a of allycodes)
    {
        let pa = resultsObj.playerAssignments[a];

        let possibleReqsAssigned = pa.reqsAssigned.filter(a => !impossibleOps.find(o => o.planet === a.planet && o.operation === a.operation));

        if (possibleReqsAssigned.length === 0) continue;

        let alignmentsCount = 0;

        for (var alignment in tbCommon.ALIGNMENT_COLOR_MAP)
        {
            let assignments = possibleReqsAssigned.filter(r => r.alignment === alignment);
            if (assignments.length === 0) continue;

            if (assignments.length > maxAssignmentsPerAlignment) maxAssignmentsPerAlignment = assignments.length;
            alignmentsCount++;
        }

        allycodesWithAssignments.push(a);
        portraitRows += alignmentsCount;
    }

    const padding = 10;
    const width = Math.max(250, padding + maxAssignmentsPerAlignment * (PORTRAIT_WIDTH + 10));
    const height = allycodesWithAssignments.length*60 + portraitRows*(PORTRAIT_HEIGHT) + (portraitRows - allycodesWithAssignments.length)*10;

    let embeds = [], files = [], csvData = [];
        
    let embed = new EmbedBuilder()
        .setTitle(`Player Operation Assignments - ${reqs.map(r => `${r.alignment} ${r.sector}`).join(", ")}`)
        .setColor('DarkGreen');
    embeds.push(embed);

    if (allycodesWithAssignments.length === 0)
    {
        embed.setDescription("No assignments. Your guild cannot complete any operations.")
        return { embeds: embeds };
    }


    let curPosY = 0;
    const canvas = createCanvas(width, height);
    const context = canvas.getContext('2d');
    
    context.fillStyle = "rgb(0, 0, 0)"
    context.fillRect(0, 0, width, height)

    for (var acIx in allycodesWithAssignments)
    {
        let allycode = allycodesWithAssignments[acIx];
        let pa = resultsObj.playerAssignments[allycode];
        let possibleReqsAssigned = pa.reqsAssigned.filter(a => !impossibleOps.find(o => o.planet === a.planet && o.operation === a.operation));

        if (possibleReqsAssigned.length === 0) continue;

        // sort assignments
        possibleReqsAssigned.sort((a, b) => {
            let alignCompare = getSortValueForAlignment(a.alignment) - getSortValueForAlignment(b.alignment);
            if (alignCompare != 0) return alignCompare;
            if (a.sector != b.sector) return a.sector - b.sector;
            if (a.operation != b.operation) return a.operation - b.operation;
            if (a.row != b.row) return a.row - b.row;
            return a.slot - b.slot;
        });

        context.fillStyle = "rgb(200, 200, 200)"
        context.fillRect(0, curPosY, width, 10);

        curPosY += 40;


        context.font = `20pt "Noto Sans","Noto Sans SC","Noto Sans Armenian","Noto Sans Thai","Noto Sans Arabic","Noto Sans Korean"`
        context.textAlign = "left"
        context.fillStyle = "#fff"
        context.fillText(`${pa.playerName}`,
            padding,
            curPosY,
            width
            )
        
        curPosY += 10;

        let curAlignment;
        let reqsInAlignment = 0;
        for (var a = 0; a < possibleReqsAssigned.length; a++)
        {
            let assignment = possibleReqsAssigned[a];
            
            if (assignment.alignment != curAlignment)
            {
                // new row, but not the first for a player
                if (reqsInAlignment > 0) curPosY += PORTRAIT_HEIGHT + 10;
                curAlignment = assignment.alignment;
                reqsInAlignment = 0;
            }

            let portrait = await unitsUtils.getPortraitForUnitByDefId(assignment.unitDefId);
            let img = portrait ?? './src/img/no-portrait.png';
            context.fillStyle = tbCommon.ALIGNMENT_COLOR_MAP[assignment.alignment].string;
            context.fillRect(padding + reqsInAlignment*(PORTRAIT_WIDTH+10), curPosY, PORTRAIT_WIDTH+2, PORTRAIT_HEIGHT+2);

            const unitIconImg = await loadImage(img);
            context.drawImage(unitIconImg, 
                padding + reqsInAlignment*(PORTRAIT_WIDTH + 10) + 1, 
                curPosY + 1, 
                PORTRAIT_WIDTH, PORTRAIT_HEIGHT);

                
            context.fillStyle = "rgb(0, 0, 0)"
            context.fillRect(padding + reqsInAlignment*(PORTRAIT_WIDTH+10) + 1, curPosY + PORTRAIT_HEIGHT - 19, PORTRAIT_WIDTH, 20);

            context.font = "15pt Arial"
            context.textAlign = "center"
            context.fillStyle = "#fff"
            context.fillText(`${assignment.alignment} ${assignment.sector}.${assignment.operation}`,
                padding + reqsInAlignment*(PORTRAIT_WIDTH+10) + PORTRAIT_WIDTH/2,
                curPosY + PORTRAIT_HEIGHT - 1
            )

            let csvDataObj = {
                "Player": pa.playerName,
                "Planet": assignment.planet,
                "Operation": assignment.operation,
                "Row": assignment.row,
                "Col": assignment.slot,
                "Unit": assignment.unitName
            };
            
            csvData.push(csvDataObj);

            reqsInAlignment++;
        }

        curPosY += PORTRAIT_HEIGHT + 10;

    }

    let customIdAppend = getCustomIdTextByReqs(reqs);
    let fileName = `tb-ops-${customIdAppend}.png`
    const file = new AttachmentBuilder(canvas.toBuffer("image/png"), { name: fileName });
    embed.setImage("attachment://" + fileName)
    files.push(file);

    if (csvData.length > 0)
    {
        const csvParsingOptions = { defaultValue: 0 }
        let csvText = await parseAsync(csvData, csvParsingOptions);
        let buffer = Buffer.from(csvText);
        let csvAttachment = new AttachmentBuilder(buffer, { name: `rote_ops_by_player_${customIdAppend}_${new Date().getTime()}.csv`});

        files.push(csvAttachment);
    }
    
    return { embeds: embeds, files: files };
}

function addComponentsToResponse(response, comlinkGuildId, customIdAppend)
{
    let buttonRow1 = new ActionRowBuilder();
    buttonRow1.addComponents(
        new ButtonBuilder()
            .setCustomId(`tb:ops:1:${comlinkGuildId}`)
            .setLabel("Sector 1")
            .setStyle(ButtonStyle.Success),
        new ButtonBuilder()
            .setCustomId(`tb:ops:2:${comlinkGuildId}`)
            .setLabel("Sector 2")
            .setStyle(ButtonStyle.Success),
        new ButtonBuilder()
            .setCustomId(`tb:ops:3:${comlinkGuildId}`)
            .setLabel("Sector 3")
            .setStyle(ButtonStyle.Success)
    );

    let buttonRow2 = new ActionRowBuilder();
    buttonRow2.addComponents(
        new ButtonBuilder()
            .setCustomId(`tb:ops:4:${comlinkGuildId}`)
            .setLabel("Sector 4")
            .setStyle(ButtonStyle.Success),
        new ButtonBuilder()
            .setCustomId(`tb:ops:5:${comlinkGuildId}`)
            .setLabel("Sector 5")
            .setStyle(ButtonStyle.Success),
        new ButtonBuilder()
            .setCustomId(`tb:ops:6:${comlinkGuildId}`)
            .setLabel("Sector 6")
            .setStyle(ButtonStyle.Success)
    )

    let multiSelectRow = new ActionRowBuilder();
    let selectOptions = tbCommon.ALL_PLANET_CHOICES_NOT_BONUS.map(p => { return { label: p.name, value: p.value }; });
    multiSelectRow.addComponents(
        new StringSelectMenuBuilder()
            .setCustomId(`tb:ops:sel:${comlinkGuildId}`)
            .setPlaceholder('Choose up to three planets')
            .setMinValues(1)
            .setMaxValues(3)
            .addOptions(selectOptions)
    )

    let showAssignmentsButtonRow = new ActionRowBuilder();
    showAssignmentsButtonRow.addComponents(
        new ButtonBuilder()
            .setCustomId(`tb:ops:aplanet:${customIdAppend}:${comlinkGuildId}`)
            .setLabel("Assignments by Planet")
            .setStyle(ButtonStyle.Primary),
        new ButtonBuilder()
            .setCustomId(`tb:ops:aplayer:${customIdAppend}:${comlinkGuildId}`)
            .setLabel("Assignments by Player")
            .setStyle(ButtonStyle.Primary),
        new ButtonBuilder()
            .setCustomId(`tb:ops:ebdl:${customIdAppend}:${comlinkGuildId}`)
            .setLabel("Assignments as JSON")
            .setStyle(ButtonStyle.Secondary)
    );

    response.components = [buttonRow1, buttonRow2, multiSelectRow, showAssignmentsButtonRow]
}


function addSendAssignmentsComponentsToResponse(response, comlinkGuildId, customIdAppend)
{
    let sendAssignmentsButtonRow = new ActionRowBuilder();
    sendAssignmentsButtonRow.addComponents(
        new ButtonBuilder()
            .setCustomId(`tb:ops:send:${customIdAppend}:${comlinkGuildId}`)
            .setLabel("Send Assignments")
            .setStyle(ButtonStyle.Success),
        new ButtonBuilder()
            .setCustomId(`tb:ops:post:${customIdAppend}:${comlinkGuildId}`)
            .setLabel("Post Assignments to Channel")
            .setStyle(ButtonStyle.Success)
    )

    response.components.push(sendAssignmentsButtonRow);
}

function generateTbOpsMetadata(dsOps, mxOps, lsOps, allycodesToExclude, rareOnly, priority)
{
    let obj = {
        dsOps: dsOps,
        mxOps: mxOps,
        lsOps: lsOps,
        allycodesToExclude: allycodesToExclude,
        rareOnly: rareOnly,
        priority: priority
    };

    return JSON.stringify(obj);
}

async function saveTbOpsRequestData(interaction, exclude, lsOps, dsOps, mxOps, rareOnly, priority)
{
    await database.db.any(
        `INSERT INTO tb_ops_request_data (discord_interaction_id, exclude, ls_ops, ds_ops, mx_ops, rarity_level, priority, timestamp)
        VALUES ($1, $2, $3, $4, $5, $6, $7, $8)
        ON CONFLICT (discord_interaction_id) DO UPDATE SET exclude = $2, ls_ops = $3, ds_ops = $4, mx_ops = $5, rarity_level = $6, priority = $7, timestamp = $8`,
        [interaction.id, exclude ? JSON.stringify(exclude) : null, lsOps, dsOps, mxOps, rareOnly, priority, new Date()]
    )
}

async function loadRequestDataByMessageId(id) {
    let requestData = await database.db.oneOrNone("SELECT ls_ops, ds_ops, mx_ops, exclude, rarity_level, priority FROM tb_ops_request_data WHERE discord_interaction_id = $1", id);

    let exclude, lsOps, dsOps, mxOps, rareOnly;
    if (requestData) {
        exclude = requestData.exclude ? JSON.parse(requestData.exclude) : null;
        lsOps = requestData.ls_ops;
        dsOps = requestData.ds_ops;
        mxOps = requestData.mx_ops;
        rareOnly = requestData.rarity_level;
        priority = requestData.priority;
    }

    return { exclude, dsOps, mxOps, lsOps, rareOnly, priority };
}

async function mapAssignmentsToEchoBaseFormat(assignmentsByOperation, codes, customIdAppend)
{
    let echobaseData = {
        "phase": "",
        "timestamp": new Date().toJSON(),
        "platoonAssignments": [

        ]
    }

    let dsPhase = codes.find(c => c?.startsWith(tbCommon.ALIGNMENTS.DS))?.charAt(2) ?? "x";
    let mxPhase = codes.find(c => c?.startsWith(tbCommon.ALIGNMENTS.MX))?.charAt(2) ?? "x";
    let lsPhase = codes.find(c => c?.startsWith(tbCommon.ALIGNMENTS.LS))?.charAt(2) ?? "x";

    echobaseData.phase = `${dsPhase}/${mxPhase}/${lsPhase}`;
    
    for (let operation of assignmentsByOperation)
    {
        for (let req of operation.reqs)
        {
            if (!req.assignment) continue;

            let ebAssignment = {
                allyCode: req.assignment.allycode,
                unitBaseId: req.unitDefId,
                zoneId: generateGameZoneIdByReq(req),
                platoonDefinitionId: `tb3-platoon-${operation.operation}`
            }

            echobaseData.platoonAssignments.push(ebAssignment);
        }
    }

    if (echobaseData.platoonAssignments.length == 0) return { content: "There are no assignments." };

    let jsonText = JSON.stringify(echobaseData);
    let buffer = Buffer.from(jsonText);
    let jsonAttachment = new AttachmentBuilder(buffer, { name: `wookieebot-ops-${customIdAppend}-${new Date().toJSON()}.json`});

    let files = [jsonAttachment];

    return { files: files };
}

function generateGameZoneIdByReq(req)
{
    let conflictNumber;
    switch (req.alignment)
    {
        case tbCommon.ALIGNMENTS.LS:
            conflictNumber = "01";
            break;
        case tbCommon.ALIGNMENTS.DS:
            conflictNumber = "02";
            break;
        case tbCommon.ALIGNMENTS.MX:
            conflictNumber = "03";
            break;
    }

    return `tb3_mixed_phase0${req.sector}_conflict${conflictNumber}_recon01`

    // conflict01 = light
    // conflict02 = dark
    // conflict03 = mixed
}


async function loadGuildDataJobHandler(job)
{
    const channel = CLIENT.channels.cache.get(job.data.channelId);
    const editMessage = (await channel.messages.fetch({ message: job.data.messageId }));
    editMessage.edit({ content: "Processing your request now..."});

    let guildData = await guildUtils.loadGuildPlayerData({ comlinkGuildId: job.data.comlinkGuildId, editMessageId: job.data.messageId, editMessageChannelId: job.data.channelId });
    let response;
    
    if (job.data.fn === "interact" || job.data.fn === "handleButton" || job.data.fn === "onSelect")
    {
        response = await getResponseForPlanets(
            job.data.planetsToUse, 
            { 
                guildData: guildData, 
                dsOps: job.data.dsOps, 
                lsOps: job.data.lsOps, 
                mxOps: job.data.mxOps, 
                allycodesToExclude: job.data.allycodesToExclude, 
                rareOnly: job.data.rareOnly, 
                priority: job.data.priority
             });
    } 
    else if (job.data.fn === "handleShowAssignmentsByPlanetButton")
    {
        saveInteractionData = true;
        response = await getShowAssignmentsByPlanetResponse(job.data.planetsToUse, 
            { 
                guildData: guildData, 
                dsOps: job.data.dsOps, 
                lsOps: job.data.lsOps, 
                mxOps: job.data.mxOps, 
                allycodesToExclude: job.data.allycodesToExclude, 
                rareOnly: job.data.rareOnly, 
                priority: job.data.priority
            });
    }
    else if (job.data.fn === "handleShowAssignmentsByPlayerButton")
    {
        saveInteractionData = true;
        
        response = await getShowAssignmentsByPlayerResponse(job.data.planetsToUse, 
            { 
                guildData: guildData, 
                dsOps: job.data.dsOps, 
                lsOps: job.data.lsOps, 
                mxOps: job.data.mxOps, 
                allycodesToExclude: job.data.allycodesToExclude, 
                rareOnly: job.data.rareOnly, 
                priority: job.data.priority
            });

    }
    
    response.content = `<@${job.data.userId}>`;

    addComponentsToResponse(response, job.data.comlinkGuildId, job.data.customIdAppend);
    let interactionToSave = await editMessage.reply(response);
    await saveTbOpsRequestData(interactionToSave, job.data.allycodesToExclude, job.data.lsOps, job.data.dsOps, job.data.mxOps, job.data.rareOnly, job.data.priority);
}