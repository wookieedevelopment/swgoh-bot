const { SlashCommandSubcommandBuilder, AttachmentBuilder, EmbedBuilder, ActionRowBuilder, StringSelectMenuBuilder } = require("discord.js");
const playerUtils = require("../../utils/player");
const discordUtils = require("../../utils/discord");
const swapiUtils = require("../../utils/swapi");
const tbCommon = require("./common");

const STRINGS = {
    SUGGESTIONS: `No teams known. Suggestions? Share here: ${discordUtils.WOOKIEEBOT_DISCORD_LINK}`
};

module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("teams")
        .setDescription("List alternative teams for a ROTE TB planet.")
        .addStringOption(o => 
            o.setName("planet")
            .setDescription("A ROTE TB planet")
            .addChoices(...tbCommon.ALL_PLANET_CHOICES)
            .setRequired(true))
        .addStringOption(o => 
            o.setName("allycode")
            .setDescription("Allycode of a player to check eligibility (default: you)")
            .setRequired(false)
            .setAutocomplete(true))
        .addIntegerOption(o =>
            o.setName("alt")
            .setDescription("Alt account (see /user me)")
            .setRequired(false)
            .setAutocomplete(true))
        .addBooleanOption(o =>
            o.setName("show")
            .setDescription("Show this to everyone")
            .setRequired(false)
        ),
    interact: async function(interaction)
    {
        let planetName = interaction.options.getString("planet");
        let allycode = interaction.options.getString("allycode");
    
        let alt = interaction.options.getInteger("alt");
    
        let { botUser, guildId, error } = await playerUtils.authorize(interaction);
        
        if (allycode) allycode = playerUtils.cleanAndVerifyStringAsAllyCode(allycode);
        allycode = playerUtils.chooseBestAllycode(botUser, guildId, allycode, alt); 

        if (!allycode)
        {
            await interaction.editReply({ content: "Please provide an allycode: `/tb teams allycode:123456789` or `/register`" });
            return;
        }

        await interaction.editReply({ content: `Loading teams for ${planetName}...`, embeds: [], files: [] });
        const response = await getTbTeamsResponse(allycode, planetName);
        await interaction.editReply(response);
    },
    isEphemeral: function(interaction)
    {
        return !(interaction.options.getBoolean("show") ?? false);
    },
    autocomplete: async function(interaction)
    {
        const focusedOption = interaction.options.getFocused(true);

        let choices;
        if (focusedOption.name == "alt")
        {
            choices = await playerUtils.getAltAutocompleteOptions(focusedOption.value, interaction.user.id);
        }
        else if (focusedOption.name == "allycode")
        {
            choices = await playerUtils.getGuildPlayersAutocompleteOptions(focusedOption.value, interaction.user.id);        
        }
        
        await interaction.respond(choices);
    },
    onSelect: async function(interaction)
    {
        if (interaction.customId.startsWith("tb:teams:sel"))
        {
            await handleSelectPlanetResponse(interaction);
        }
    }
}

async function getTbTeamsResponse(allycode, planetName)
{
    let playerData = await swapiUtils.getPlayer(allycode);

    if (!playerData)
    {
        await interaction.editReply({ content: `No player found with allycode **${allycode}**.`})
        return;
    }

    let planet = tbCommon.PLANETS.find(p => p.name === planetName);
    let teamsList = await tbCommon.getTbCombatTeamsForPlanets([planet]);

    // sort by mission ascending, then quality descending
    teamsList.sort((a, b) => (a.mission - b.mission) || (b.quality - a.quality));

    let embeds = await createTeamsEmbeds(playerData, planet, teamsList);
    
    let imageEmbed = new EmbedBuilder()
            .setTitle(`${planetName} Missions`)
            .setColor(tbCommon.ALIGNMENT_COLOR_MAP[planet.alignment].number);
    let fileName = `${planetName.replace(/ /g, "_")}-labelled.png`;
    const file = new AttachmentBuilder(`./src/img/rote/${fileName}`);
    imageEmbed.setImage(`attachment://${fileName}`);
    embeds.push(imageEmbed);

    const files = [file];

    let content = `TB teams & eligibility for ${swapiUtils.getPlayerName(playerData)} (${swapiUtils.getPlayerAllycode(playerData)}): ${planetName}`;

    let response = { content: content, embeds: embeds, files: files };
    addComponentsToResponse(response, allycode);

    return response;
}

function addComponentsToResponse(response, allycode)
{
    let selectRow = new ActionRowBuilder();
    let selectOptions = tbCommon.ALL_PLANET_CHOICES.map(p => { return { label: p.name, value: p.value }; });
    selectRow.addComponents(
        new StringSelectMenuBuilder()
            .setCustomId(`tb:teams:sel:${allycode}`)
            .setPlaceholder('Choose a planet')
            .setMaxValues(1)
            .addOptions(selectOptions)
    );

    response.components = [selectRow];
}

async function createTeamsEmbeds(playerData, planet, teamsList)
{
    // get list of missions from database
    // loop through, create embed for each mission
    // each embed contains stoplight for all team options

    const missions = await tbCommon.getAllTbMissions();
    const missionsForPlanet = missions.filter(m => m.planet === planet);

    let embeds = [];
    for (let mission of missionsForPlanet)
    {

        let missionNotes = mission?.notes;
        let embed = new EmbedBuilder()
            .setTitle(`${mission.icon ? `${mission.icon} ` : "" }Mission ${mission.mission}${missionNotes ? ` - ${missionNotes}` : ""}`)
            .setColor(tbCommon.ALIGNMENT_COLOR_MAP[planet.alignment].number);

        let eligibleField = { name: `${discordUtils.symbols.ok} Eligible`, value: "" };
        let ineligibleField = { name: `${discordUtils.symbols.fail} Ineligible`, value: "" };


        let teamsForMission = teamsList.filter(t => t.mission === mission.mission);

        if (teamsForMission.length == 0)
        {
            embed.setDescription(STRINGS.SUGGESTIONS);
            embeds.push(embed);
            continue;
        }

        for (let team of teamsForMission)
        {
            let icon;
            if (team.quality >= 70) icon = ":green_circle:";
            else if (team.quality >= 40) icon = ":yellow_circle:";
            else icon = ":red_circle:";

            try {
                let text = await tbCommon.getTbTeamText(team, false);

                let matchingTeam = await tbCommon.findMatchingTbTeamForPlayer(playerData, team, planet, { minRelicLevelOverride: mission.minRelicLevelOverride });
                let fieldToUpdate = matchingTeam ? eligibleField : ineligibleField;
                fieldToUpdate.value += `> ${icon} ${text}\n`;
            } catch (e) {}
        }

        if (eligibleField.value == "") eligibleField.value = "None";
        if (ineligibleField.value == "") ineligibleField.value = "None";
        
        embed.addFields(eligibleField, ineligibleField);

        embeds.push(embed);
    }
    return embeds;
}


async function handleSelectPlanetResponse(interaction)
{
    await interaction.deferUpdate();

    const planetName = interaction.values[0];
    await interaction.editReply({ content: `Loading teams for ${planetName}...`, embeds: [], files: [] });
    let regex = /tb:teams:sel:([0-9]{9})/

    let matches = regex.exec(interaction.customId);

    if (!matches || matches.length < 2) {
        // something went wrong
        logger.error("tb:teams:sel interaction.customId doesn't match. customId is: " + interaction.customId);
        response = { content: "Something went wrong, unfortunately. I'm working on it, but please try again." };

        await interaction.editReply(response);
        return;
    }

    let allycode = matches[1];

    const response = await getTbTeamsResponse(allycode, planetName);
    await interaction.editReply(response);
}