const playerUtils = require("../../../utils/player");
const discordUtils = require("../../../utils/discord");
const swapiUtils = require("../../../utils/swapi");
const guildUtils = require("../../../utils/guild");
const database = require("../../../database");
const tbCommon = require("../common");
const { SlashCommandSubcommandBuilder } = require('@discordjs/builders');
const { EmbedBuilder, ActionRowBuilder, StringSelectMenuBuilder } = require("discord.js");

module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("p")
        .setDescription("Check player TB special readiness")
        .addStringOption(o => 
            o.setName("allycode")
            .setDescription("Allycode of the player to check")
            .setRequired(false)
            .setAutocomplete(true))
        .addBooleanOption(o =>
            o.setName("guild")
            .setDescription("True: add dropdowns for selecting other guild members")
            .setRequired(false))
        .addIntegerOption(o =>
            o.setName("alt")
            .setDescription("Alt account (see /user me)")
            .setRequired(false)
            .setAutocomplete(true)),
    interact: async function(interaction)
    {
        let allycode = interaction.options.getString("allycode");
        const showGuildMembers = interaction.options.getBoolean("guild");
        let alt = interaction.options.getInteger("alt");        

        let { botUser, guildId, error } = await playerUtils.authorize(interaction);
        
        if (allycode) allycode = playerUtils.cleanAndVerifyStringAsAllyCode(allycode);
        allycode = playerUtils.chooseBestAllycode(botUser, guildId, allycode, alt);

        if (!allycode)
        {
            await interaction.editReply({ content: "Please provide an allycode: `/raid max allycode:123456789` or `/register`" });
            return;
        }


        
        let playerData = await swapiUtils.getPlayer(allycode);
        if (!playerData)
        {
            await interaction.editReply({ content: `No player found with allycode **${allycode}**.`})
            return;
        }

        let response = await getTbSpecialsResponse(
            playerData
        );

        if (showGuildMembers)
        {
            await addGuildMembersDropDowns(
                response,
                swapiUtils.getPlayerGuildId(playerData)
            )
        }
        
        await interaction.editReply(response);
    },
    autocomplete: async function(interaction) {

		const focusedOption = interaction.options.getFocused(true);

        let choices;
        if (focusedOption.name == "alt")
        {
            choices = await playerUtils.getAltAutocompleteOptions(focusedOption.value, interaction.user.id);
        }
        else if (focusedOption.name == "allycode")
        {
            // todo: autocomplete for guild members
            const names = await database.db.any(
                `
                SELECT * FROM 
                (
                    (SELECT player_name, allycode, 1 o
                    FROM guild_players
                    WHERE guild_id IN (SELECT guild_id FROM guild_players WHERE allycode IN (SELECT allycode FROM user_registration WHERE discord_id = $1))
                    AND (player_name ILIKE $2 OR allycode ILIKE $2)
                    ORDER BY player_name ASC)
                    UNION
                    (SELECT player_name, allycode, 2 o
                    FROM guild_players
                    WHERE guild_id IN (SELECT guild_id FROM guild_players WHERE allycode IN (SELECT allycode FROM user_registration WHERE discord_id = $1))
                    AND (player_name ILIKE $3 OR allycode ILIKE $3))
                ) q
                ORDER BY o ASC, player_name ASC
                LIMIT 25
                `, [interaction.user.id, `${focusedOption.value.trim()}%`, `_%${focusedOption.value.trim()}%`]
            );

            if (names.length == 0)
            {
                choices = [{ name: "No matching players in guild.", value: "x" }]
            }
            else
            {
                choices = names.map(choice => ({ name: `${choice.player_name} (${discordUtils.addDashesToAllycode(choice.allycode)})`, value: choice.allycode }));
            }
        
        }
        
        await interaction.respond(choices);
    },
    onSelect: async function(interaction)
    {
        await interaction.deferUpdate();

        const allycode = interaction.values[0];
        const playerName = await playerUtils.getPlayerNameByAllycode(allycode);

        await interaction.editReply({ content: `Running \`/tb specials p\` for **${playerName}**...`, embeds: [], components: [], files: []})

        let playerData = await swapiUtils.getPlayer(allycode);

        if (!playerData)
        {
            await interaction.editReply({ content: `No player found with allycode **${allycode}**.`})
            return;
        }
        
        let response = await getTbSpecialsResponse(
            playerData
        );

        await addGuildMembersDropDowns(
            response,
            swapiUtils.getPlayerGuildId(playerData)
        );

        response.content = null;
        
        await interaction.editReply(response);
    },
}

async function getTbSpecialsResponse(playerData)
{
    let { specialMissionEligibility, income } = await tbCommon.estimateSpecialMissionIncomeForPlayer(playerData);

    let embed = new EmbedBuilder().setTitle(`**${playerData.name} (${swapiUtils.getPlayerAllycode(playerData)})** TB Special Missions`)

    let missionsText = specialMissionEligibility.map(m => `${m.eligible ? discordUtils.symbols.ok : discordUtils.symbols.fail} **${m.mission.planet.name}:** ${m.mission.notes}`).join("\n");
    const missionsField = {
        name: "Eligibility",
        value: missionsText,
        inline: false
    };

    let incomeTypes = Object.keys(income);
    incomeTypes.sort();
    let incomeText = "";

    for (const t of incomeTypes)
    {
        incomeText += `- ${tbCommon.TB_REWARDS[t].name}: ${income[t]}\n`;
    }

    if (incomeText.length == 0) incomeText = "None"

    const incomeField = {
        name: "Estimated Special Mission Income",
        value: incomeText,
        inline: false
    };

    embed.addFields(missionsField, incomeField);

    const response = { embeds: [embed] };
    return response;
}

async function addGuildMembersDropDowns(response, guildId)
{
    
    let membersActionRows = [];

    const guildData = await guildUtils.getGuildDataFromAPI({ comlinkGuildId: guildId }, { useCache: false });
    const guildRoster = guildUtils.getGuildRoster(guildData);
    let options = guildRoster.map(m => { return { label: `${swapiUtils.getPlayerName(m)} (${swapiUtils.getPlayerAllycode(m)})`, value: swapiUtils.getPlayerAllycode(m) }})
    options.sort((a, b) => a.label.localeCompare(b.label));

    for (let m = 0; m < options.length; m += discordUtils.MAX_DISCORD_DROPDOWN_ITEMS)
    {
        let slicedOptions = options.slice(m, m + discordUtils.MAX_DISCORD_DROPDOWN_ITEMS);
        let row = new ActionRowBuilder()
            .addComponents(
                new StringSelectMenuBuilder()
                    .setCustomId(`tb:specials:players${Math.floor(m/discordUtils.MAX_DISCORD_DROPDOWN_ITEMS)}`)
                    .setMaxValues(1)
                    .setPlaceholder(`Choose a guild member (${slicedOptions[0].label[0].toUpperCase()} - ${slicedOptions[slicedOptions.length-1].label[0].toUpperCase()})`)
                    .addOptions(slicedOptions)
            );

        membersActionRows.push(row);
    }

    if (response.components?.length > 0) response.components = response.components.concat(membersActionRows);
    else response.components = membersActionRows;

}