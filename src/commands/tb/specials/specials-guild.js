const playerUtils = require("../../../utils/player");
const discordUtils = require("../../../utils/discord");
const queueUtils = require("../../../utils/queue");
const guildUtils = require("../../../utils/guild");
const swapiUtils = require("../../../utils/swapi");
const tbCommon = require("../common");
const database = require("../../../database");
const { SlashCommandSubcommandBuilder } = require('@discordjs/builders');
const { EmbedBuilder } = require("discord.js");
const { cleanAndVerifyStringAsAllyCode } = require("../../../utils/tools");

const QUEUE_FUNCTION_KEY = "tb:specials:g";

module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("g")
        .setDescription("Check Guild TB special readiness")
        .addStringOption(o => 
            o.setName("allycode")
            .setDescription("Allycode of a player in a guild to check")
            .setRequired(false)
            .setAutocomplete(true))
        .addStringOption(o =>
            o.setName("guild")
            .setDescription("Select a guild")
            .setRequired(false)
            .setAutocomplete(true))
        .addIntegerOption(o =>
            o.setName("alt")
            .setDescription("Alt account (see /user me)")
            .setRequired(false)
            .setAutocomplete(true)),
    interact: async function(interaction)
    {
        let allycode = interaction.options.getString("allycode");
        let comlinkGuildId = interaction.options.getString("guild");
        let altNum = interaction.options.getInteger("alt");
        
        if (!comlinkGuildId && !allycode)
        {
            let { botUser, guildId, error } = await playerUtils.authorize(interaction);
            allycode = playerUtils.chooseBestAllycode(botUser, guildId, allycode, altNum);    

            if (!allycode)
            {
                await interaction.editReply({ content: "Please provide an allycode or guild, or `/register` to use without." });
                return;
            }

            let alt = botUser.alts.find(a => a.allycode == allycode);
            comlinkGuildId = alt.comlinkGuildId;

            if (!comlinkGuildId)
            {
                await interaction.editReply({ content: `Your alt ${alt.name} (${alt.allycode}) is not in a guild.` });
                return;
            }
        }
        else if (allycode)
        {
            allycode = cleanAndVerifyStringAsAllyCode(allycode);
            let playerData = await swapiUtils.getPlayer(allycode);

            if (!playerData)
            {
                await interaction.editReply({ content: `No player found with allycode ${allycode}.` });
                return;
            }

            comlinkGuildId = swapiUtils.getPlayerGuildId(playerData);

            if (!comlinkGuildId)
            {
                await interaction.editReply({ content: `Player ${swapiUtils.getPlayerName(playerData)} (${allycode}) is not in a guild.` });
                return;
            }
        }


        let qsm = await queueUtils.getQueueSizeMessage(queueUtils.QUEUES.OTHER, interaction.client.shard);
        const editMessage = await interaction.editReply(`Processing Guild Specials request. ${qsm}`);
        
        await queueUtils.queueOtherJob(interaction.client.shard, 
        {
            functionKey: QUEUE_FUNCTION_KEY,
            data: { 
                comlinkGuildId: comlinkGuildId
            },
            channelId: editMessage.channelId ?? editMessage.channel.id,
            messageId: editMessage.id,
            userId: interaction.user.id,
        },
        {
            expireInHours: 2
        });
    },
    autocomplete: async function(interaction) {

		const focusedOption = interaction.options.getFocused(true);

        let choices;
        if (focusedOption.name == "alt")
        {
            choices = await playerUtils.getAltAutocompleteOptions(focusedOption.value, interaction.user.id);
        }
        else if (focusedOption.name == "guild")
        {
            choices = await discordUtils.getGuildAutocompleteOptions(focusedOption.value);
        }
        else if (focusedOption.name == "allycode")
        {
            const names = await database.db.any(
                `
                SELECT * FROM 
                (
                    (SELECT player_name, allycode, 1 o
                    FROM guild_players
                    WHERE guild_id IN (SELECT guild_id FROM guild_players WHERE allycode IN (SELECT allycode FROM user_registration WHERE discord_id = $1))
                    AND (player_name ILIKE $2 OR allycode ILIKE $2)
                    ORDER BY player_name ASC)
                    UNION
                    (SELECT player_name, allycode, 2 o
                    FROM guild_players
                    WHERE guild_id IN (SELECT guild_id FROM guild_players WHERE allycode IN (SELECT allycode FROM user_registration WHERE discord_id = $1))
                    AND (player_name ILIKE $3 OR allycode ILIKE $3))
                ) q
                ORDER BY o ASC, player_name ASC
                LIMIT 25
                `, [interaction.user.id, `${focusedOption.value.trim()}%`, `_%${focusedOption.value.trim()}%`]
            );

            if (names.length == 0)
            {
                choices = [{ name: "No matching players in guild.", value: "x" }]
            }
            else
            {
                choices = names.map(choice => ({ name: `${choice.player_name} (${discordUtils.addDashesToAllycode(choice.allycode)})`, value: choice.allycode }));
            }
        
        }
        
        await interaction.respond(choices);
    }
}

async function generateSpecialsGuildResponse(jobData)
{
    let guildPlayerData = await guildUtils.loadGuildPlayerData({ comlinkGuildId: jobData.data.comlinkGuildId, editMessageId: jobData.messageId, editMessageChannelId: jobData.channelId });
    let embed = new EmbedBuilder()
                .setTitle(`**${guildPlayerData.guildName}** TB Special Missions`);

            
    const specialMissionTotals = {};
    const incomeTotals = {};
    for (let p of guildPlayerData.players)
    {
        let { income, specialMissionEligibility } = await tbCommon.estimateSpecialMissionIncomeForPlayer(p);

        for (let m of specialMissionEligibility)
        {
            let missionUID = `**${m.mission.planet.name}:** ${m.mission.notes}`;
            if (specialMissionTotals[missionUID] == null) specialMissionTotals[missionUID] = 0;

            if (!m.eligible) continue;

            specialMissionTotals[missionUID]++;
        }
        
        let incomeTypes = Object.keys(income);

        for (const t of incomeTypes)
        {
            if (incomeTotals[t] != null) incomeTotals[t] += income[t];
            else incomeTotals[t] = income[t];
        }
    }

    let missions = Object.keys(specialMissionTotals);
    missions.sort();
    let missionsText = "";

    for (const mKey of missions)
    {
        missionsText += `- ${mKey}: ${specialMissionTotals[mKey]}/${guildPlayerData.players.length}\n`;
    }

    const missionsField = {
        name: "Eligibility",
        value: missionsText,
        inline: false
    };


    let incomeTypes = Object.keys(incomeTotals);
    incomeTypes.sort();
    let incomeText = "";

    for (const t of incomeTypes)
    {
        incomeText += `- ${tbCommon.TB_REWARDS[t].name}: ${incomeTotals[t].toLocaleString('en')}\n`;
    }

    if (incomeText.length == 0) incomeText = "None"

    const incomeField = {
        name: "Estimated Guild Special Mission Income",
        value: incomeText,
        inline: false
    };

    embed.addFields(missionsField, incomeField);

    const response = { embeds: [embed] };
    return response;
}

queueUtils.addOtherFunction(QUEUE_FUNCTION_KEY,  generateSpecialsGuildResponse);