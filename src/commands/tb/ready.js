const playerUtils = require("../../utils/player");
const readyUtils = require("../../utils/ready");
const queueUtils = require ("../../utils/queue");
const discordUtils = require ("../../utils/discord");
const database = require ("../../database")
const { SlashCommandSubcommandBuilder } = require('@discordjs/builders');

const CHECK_CHOICES = [
    { name: readyUtils.TB_CHECK_FUNCTION_MAPPING.Reva.desc, value: readyUtils.TB_CHECK_FUNCTION_MAPPING.Reva.value },
    { name: readyUtils.TB_CHECK_FUNCTION_MAPPING.LS2.desc, value: readyUtils.TB_CHECK_FUNCTION_MAPPING.LS2.value },
    { name: readyUtils.TB_CHECK_FUNCTION_MAPPING.LS3.desc, value: readyUtils.TB_CHECK_FUNCTION_MAPPING.LS3.value },
    { name: readyUtils.TB_CHECK_FUNCTION_MAPPING.ZEFFO.desc, value: readyUtils.TB_CHECK_FUNCTION_MAPPING.ZEFFO.value },
    { name: readyUtils.TB_CHECK_FUNCTION_MAPPING.MX1.desc, value: readyUtils.TB_CHECK_FUNCTION_MAPPING.MX1.value },
    { name: readyUtils.TB_CHECK_FUNCTION_MAPPING.TATOOINEKRAYT.desc, value: readyUtils.TB_CHECK_FUNCTION_MAPPING.TATOOINEKRAYT.value },
    { name: readyUtils.TB_CHECK_FUNCTION_MAPPING.MX4.desc, value: readyUtils.TB_CHECK_FUNCTION_MAPPING.MX4.value },
    { name: readyUtils.TB_CHECK_FUNCTION_MAPPING.MX5.desc, value: readyUtils.TB_CHECK_FUNCTION_MAPPING.MX5.value },
    { name: readyUtils.TB_CHECK_FUNCTION_MAPPING.DS3.desc, value: readyUtils.TB_CHECK_FUNCTION_MAPPING.DS3.value },
    { name: readyUtils.TB_CHECK_FUNCTION_MAPPING.DS4.desc, value: readyUtils.TB_CHECK_FUNCTION_MAPPING.DS4.value },
    { name: readyUtils.TB_CHECK_FUNCTION_MAPPING.GI.desc, value: readyUtils.TB_CHECK_FUNCTION_MAPPING.GI.value },
    { name: readyUtils.TB_CHECK_FUNCTION_MAPPING.KAM501.desc, value: readyUtils.TB_CHECK_FUNCTION_MAPPING.KAM501.value },
    { name: readyUtils.TB_CHECK_FUNCTION_MAPPING.KAMBB.desc, value: readyUtils.TB_CHECK_FUNCTION_MAPPING.KAMBB.value },
    { name: readyUtils.TB_CHECK_FUNCTION_MAPPING.WAT.desc, value: readyUtils.TB_CHECK_FUNCTION_MAPPING.WAT.value },
];


module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("ready")
        .setDescription("Check readiness for various TB missions")
        .addStringOption(o => 
            o.setName("mission")
            .setDescription("The mission to check readiness for.")
            .setRequired(true)
            .addChoices(...CHECK_CHOICES))
        .addStringOption(o => 
            o.setName("allycode")
            .setDescription("An allycode in the guild to check (default: yours, for registered users)")
            .setRequired(false)
            )
        .addIntegerOption(o =>
            o.setName("alt")
            .setDescription("Alt account (/user me)")
            .setRequired(false)
            .setAutocomplete(true)),
    interact: async function(interaction)
    {
        let missingPermissions = discordUtils.getMissingBotPermissions(interaction, interaction.channel);
        if (missingPermissions)
        {
            await interaction.editReply(discordUtils.convertMissingBotPermissionsArrayToResponse(missingPermissions));
            return;
        }
        
        let mission = interaction.options.getString("mission");
        let allycode = interaction.options.getString("allycode");
        let altNum = interaction.options.getInteger("alt");
        
        if (!allycode)
        {
            let { botUser, guildId, error } = await playerUtils.authorize(interaction);
            if (error) {
                await interaction.editReply({ embeds: [discordUtils.createErrorEmbed("Error", error)] });
                return;
            }
            
            let alt = await playerUtils.chooseBestAlt(botUser, guildId, altNum, true);

            if (!alt)
            {
                let errorEmbed = discordUtils.createErrorEmbed("Not in Guild", `Account is not in a guild. Choose a different alt, specify an ally code, join a guild, or register.`);
                await interaction.editReply({ embeds: [errorEmbed] });
                return;
            }

            allycode = alt.allycode;
        }
        
        allycode = playerUtils.cleanAndVerifyStringAsAllyCode(allycode);
        
        let qsm = await queueUtils.getQueueSizeMessage(queueUtils.QUEUES.READY, interaction.client.shard);
        const editMessage = await interaction.editReply(`Processing TB ready check. ${qsm}`);

        await queueUtils.queueReadyJob(interaction.client.shard,
            {
                type: "TB",
                mission: mission, 
                allycode: allycode, 
                messageId: editMessage.id, 
                channelId: editMessage.channelId ?? editMessage.channel.id,
                userId: interaction.user.id 
            });
    },
    autocomplete: async function(interaction) {
        const focusedValue = interaction.options.getFocused();
        await interaction.respond((await playerUtils.getAltAutocompleteOptions(focusedValue, interaction.user.id)));
    }
}