
const { SlashCommandSubcommandBuilder } = require('@discordjs/builders');
const database = require("../../database");
const metadataCache = require("../../utils/metadataCache");
const guildUtils = require("../../utils/guild");
const playerUtils = require("../../utils/player");
const swapiUtils = require("../../utils/swapi");
const queueUtils = require ("../../utils/queue")
const discordUtils = require("../../utils/discord");
const unitsUtils = require("../../utils/units")
const constants = require("../../utils/constants");
const { logger, formatError } = require("../../utils/log");
const { EmbedBuilder, ActionRowBuilder, AttachmentBuilder, StringSelectMenuBuilder, MessageFlags } = require('discord.js');
const tbCommon = require ("./common");
const comlink = require('../../utils/comlink');
const { CLIENT } = require("../../utils/discordClient");

const QUEUE_FUNCTION_KEY = "tb:combat";

const ZEFFO = tbCommon.PLANETS.find(p => p.name === "Zeffo");
const MANDALORE = tbCommon.PLANETS.find(p => p.name === "Mandalore");

const NULL_TEAM = {
    units: new Array(),
    template: {
        teamName: "NULL",
        quality: 0,
        unitsRequired: new Array()
    },
    allUnits: function () { return teamTemplate.unitsRequired.concat(this.units) }
};

const SPECIAL_MISSION_HANDLING = {
    PRIORITIZE: "prioritize",
    SKIP_ALL: "skip all",
    SKIP_DS: "skip DS",
    SKIP_MX: "skip MX",
    SKIP_LS: "skip LS",
    DO_NOT_PRIORITIZE: "do not prioritize"
}


module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("combat")
        .setDescription("Get recommendations on combat teams")
        .addStringOption(o => 
            o.setName("allycode")
            .setDescription("Ally Code to find recommendations for")
            .setRequired(false))
        .addIntegerOption(o => 
            o.setName("sector")
            .setDescription("A sector (horizontal slice) to consider.")
            .setRequired(false)
            .setMinValue(1)
            .setMaxValue(6))
        .addStringOption(o => 
            o.setName("light")
            .setDescription("A light side planet to consider.")
            .addChoices(...tbCommon.LS_PLANET_CHOICES)
            .setRequired(false))
        .addStringOption(o => 
            o.setName("mix")
            .setDescription("A mixed alignment planet to consider.")
            .addChoices(...tbCommon.MX_PLANET_CHOICES)
            .setRequired(false))
        .addStringOption(o => 
            o.setName("dark")
            .setDescription("A dark side planet to consider.")
            .addChoices(...tbCommon.DS_PLANET_CHOICES)
            .setRequired(false))
        .addStringOption(o => 
            o.setName("exclude")
            .setDescription("List of units to exclude, separated by commas.")
            .setRequired(false))
        .addBooleanOption(o => 
            o.setName("zeffo")
            .setDescription("Include Zeffo")
            .setRequired(false))
        .addBooleanOption(o => 
            o.setName("mandalore")
            .setDescription("Include Mandalore")
            .setRequired(false))
        .addStringOption(o =>
            o.setName("specials")
            .setDescription("Alternative handling for special missions. Default: prioritize")
            .setRequired(false)
            .addChoices(
                { name: "prioritize", value: SPECIAL_MISSION_HANDLING.PRIORITIZE },
                { name: "skip all", value: SPECIAL_MISSION_HANDLING.SKIP_ALL },
                { name: "skip DS", value: SPECIAL_MISSION_HANDLING.SKIP_DS },
                { name: "skip MX", value: SPECIAL_MISSION_HANDLING.SKIP_MX },
                { name: "skip LS", value: SPECIAL_MISSION_HANDLING.SKIP_LS },
                { name: "do not prioritize", value: SPECIAL_MISSION_HANDLING.DO_NOT_PRIORITIZE }
            ))
        .addBooleanOption(o =>
            o.setName("guild")
            .setDescription("Add dropdowns for guild members.")
            .setRequired(false)
        )
        .addIntegerOption(o =>
            o.setName("alt")
            .setDescription("Alt account (/user me)")
            .setRequired(false)
            .setAutocomplete(true)),
    interact: async function(interaction)
    {
        let missingPermissions = discordUtils.getMissingBotPermissions(interaction, interaction.channel);
        if (missingPermissions)
        {
            await interaction.editReply(discordUtils.convertMissingBotPermissionsArrayToResponse(missingPermissions));
            return;
        }

        let allycode = interaction.options.getString("allycode");
        let alt = interaction.options.getInteger("alt");
        const showGuildMembers = interaction.options.getBoolean("guild");
        
        let { botUser, guildId, error } = await playerUtils.authorize(interaction);
        
        if (allycode) allycode = playerUtils.cleanAndVerifyStringAsAllyCode(allycode);
        allycode = playerUtils.chooseBestAllycode(botUser, guildId, allycode, alt);

        if (!allycode)
        {
            await interaction.editReply({ content: "Please provide an allycode: `/tb combat allycode:123456789` or `/register`" });
            return;
        }
        

        let dsPlanet = interaction.options.getString("dark");
        let lsPlanet = interaction.options.getString("light");
        let mxPlanet = interaction.options.getString("mix");
        let zeffo = interaction.options.getBoolean("zeffo");
        let mandalore = interaction.options.getBoolean("mandalore");
        let sector = interaction.options.getInteger("sector");
        let exclude = interaction.options.getString("exclude");
        let specialMissionHandling = interaction.options.getString("specials") ?? SPECIAL_MISSION_HANDLING.PRIORITIZE;

        let playerData = await comlink.getPlayerArenaProfileByAllycode(allycode);

        if (!playerData)
        {
            await interaction.editReply({ content: `No player found with allycode *${allycode}*.`})
            return;
        }

        let qsm = await queueUtils.getQueueSizeMessage(queueUtils.QUEUES.OTHER, interaction.client.shard);
        const editMessage = await interaction.editReply({ content: `Processing TB combat for ${swapiUtils.getPlayerName(playerData)}. ${qsm}`, embeds: [], components: [], files: [] });

        await queueUtils.queueOtherJob(interaction.client.shard, 
        {
            functionKey: QUEUE_FUNCTION_KEY,
            data: { 
                allycode: allycode,
                dsPlanet: dsPlanet,
                lsPlanet: lsPlanet,
                mxPlanet: mxPlanet,
                zeffo: zeffo,
                mandalore: mandalore,
                sector: sector,
                exclude: exclude,
                specialMissionHandling: specialMissionHandling,
                showGuildMembers: showGuildMembers
            },
            channelId: editMessage.channelId ?? editMessage.channel.id,
            messageId: editMessage.id,
            userId: interaction.user.id,
        },
        {
            expireInHours: 4
        });
    },
    autocomplete: async function(interaction) {
        const focusedValue = interaction.options.getFocused();
        await interaction.respond((await playerUtils.getAltAutocompleteOptions(focusedValue, interaction.user.id)));
    },
    onSelect: async function(interaction)
    {
        if (interaction.customId.startsWith("tb:combat:guild"))
        {
            await handleSwitchGuildMemberResponse(interaction);
        }
        else
        {
            await handleSelectPlanetsResponse(interaction);
        }
    }
}

async function tbCombatJobHandler(job)
{
    let channel, editMessage;
    try 
    {
        channel = CLIENT.channels.cache.get(job.channelId);
        editMessage = (await channel.messages.fetch({ message: job.messageId }));
    }
    catch (e)
    {
        let discordUser = await CLIENT.users.fetch(job.userId);
        await discordUser.send(
            {
                content: `WookieeBot may be missing permissions in channel <#${job.channelId}>. Please verify with \`/check-permissions\``
            }
        );

        return;
    }
    
    const planetsToUse = getPlanetsToUse(job.data.planetsString, job.data.sector, job.data.dsPlanet, job.data.lsPlanet, job.data.mxPlanet, job.data.zeffo, job.data.mandalore);

    await showFeedbackDuringUpdate(editMessage, planetsToUse, job.data.allycode); 

    let playerData = await swapiUtils.getPlayer(job.data.allycode);
    const { response, possibilitiesString } = await generateCombatRecommendations(
        playerData,
        editMessage,
        {
            planetsToUse: planetsToUse,
            exclude: job.data.exclude,
            specialMissionHandling: job.data.specialMissionHandling
        }
    )

    response.content = createResponseContent(job.userId, playerData, possibilitiesString);
    addComponentsToResponse(response, job.data.allycode, job.data.specialMissionHandling, job.data.showGuildMembers);
    
    if (job.data.showGuildMembers)
    {
        await addGuildMembersDropDowns(
            response,
            swapiUtils.getPlayerGuildId(playerData),
            job.data.specialMissionHandling,
            planetsToUse
        )
    }
    
    let reply = await editMessage.reply(response);
    try {
        await editMessage.delete();
    } catch (e) { logger.error(`Failed to delete /tb combat message: ${formatError(e)}`)}

    // write reply.id to database
    await database.db.any(
        `INSERT INTO tb_combat_request_data (discord_interaction_id, exclude, timestamp)
        VALUES ($1, $2, $3)
        ON CONFLICT (discord_interaction_id) DO UPDATE SET exclude = $2, timestamp = $3`,
        [reply.id, job.data.exclude, new Date()]
    );
}

queueUtils.addOtherFunction(QUEUE_FUNCTION_KEY, tbCombatJobHandler);

function convertLetterToSpecialMissionHandling(letter)
{
    switch(letter)
    {
        case "p": return SPECIAL_MISSION_HANDLING.PRIORITIZE;
        case "s": return SPECIAL_MISSION_HANDLING.SKIP_ALL;
        case "d": return SPECIAL_MISSION_HANDLING.SKIP_DS;
        case "m": return SPECIAL_MISSION_HANDLING.SKIP_MX;
        case "l": return SPECIAL_MISSION_HANDLING.SKIP_LS;
        case "n": return SPECIAL_MISSION_HANDLING.DO_NOT_PRIORITIZE;
    }
}

async function handleSwitchGuildMemberResponse(interaction)
{
    await interaction.deferUpdate();
    let regex = /tb:combat:guild\d:(\w):(.+)/;
 
    let matches = regex.exec(interaction.customId);
 
    if (!matches || matches.length < 3) {
        // something went wrong
        logger.error("tb:combat:guild interaction.customId doesn't match. customId is: " + interaction.customId);
        const response = { content: "Something went wrong, unfortunately. I'm working on it, but please try again." };

        await interaction.editReply(response);
        return;
    }

    const allycode = interaction.values[0];

    let specialMissionHandling = convertLetterToSpecialMissionHandling(matches[1]);
    let planetsString = matches[2];

    let playerData = await comlink.getPlayerArenaProfileByAllycode(allycode);

    if (!playerData)
    {
        await interaction.editReply({ content: `No player found with allycode *${allycode}*.`})
        return;
    }

    let qsm = await queueUtils.getQueueSizeMessage(queueUtils.QUEUES.OTHER, interaction.client.shard);
    const editMessage = await interaction.editReply({ content: `Processing TB combat for ${swapiUtils.getPlayerName(playerData)}. ${qsm}`, embeds: [], components: [], files: [] });

    await queueUtils.queueOtherJob(interaction.client.shard, 
    {
        functionKey: QUEUE_FUNCTION_KEY,
        data: { 
            allycode: allycode,
            planetsString: planetsString,
            specialMissionHandling: specialMissionHandling,
            showGuildMembers: true
        },
        channelId: editMessage.channelId ?? editMessage.channel.id,
        messageId: editMessage.id,
        userId: interaction.user.id,
    },
    {
        expireInHours: 4
    });
}

async function handleSelectPlanetsResponse(interaction)
{
    await interaction.deferUpdate();
    let regex = /tb:combat:sel:([0-9]{9}):(p|s|n|d|m|l):(t|f)/

    let matches = regex.exec(interaction.customId);
    let response;

    if (!matches || matches.length < 4) {
        // something went wrong
        logger.error("tb:combat:onSelect interaction.customId doesn't match. customId is: " + interaction.customId);
        response = { content: "Something went wrong, unfortunately. I'm working on it, but please try again." };

        await interaction.editReply(response);
        return;
    }

    let allycode = matches[1];
    let specialMissionHandling = convertLetterToSpecialMissionHandling(matches[2]);
    let showGuildMembers = matches[3] === "t" ? true : false;

    // load additional data associated with request from database
    let requestData = await database.db.oneOrNone("SELECT exclude FROM tb_combat_request_data WHERE discord_interaction_id = $1", interaction.message.id);

    let exclude;
    if (requestData) exclude = requestData.exclude;

    let dsPlanet, lsPlanet, mxPlanet, zeffo = false, mandalore = false;
    for (let v of interaction.values)
    {
        if (v === ZEFFO.name) { zeffo = true; continue; }
        if (v === MANDALORE.name) { mandalore = true; continue; }

        switch (tbCommon.PLANETS.find(p => p.name === v).alignment)
        {
            case tbCommon.ALIGNMENTS.DS:
                dsPlanet = v;
                break;
            
            case tbCommon.ALIGNMENTS.LS:
                lsPlanet = v;
                break;
        
            case tbCommon.ALIGNMENTS.MX:
                mxPlanet = v;
                break;
                
        }
    }

    let playerData = await comlink.getPlayerArenaProfileByAllycode(allycode);

    if (!playerData)
    {
        await interaction.editReply({ content: `No player found with allycode *${allycode}*.`})
        return;
    }
    
    let qsm = await queueUtils.getQueueSizeMessage(queueUtils.QUEUES.OTHER, interaction.client.shard);
    const editMessage = await interaction.editReply({ content: `Processing TB combat for ${swapiUtils.getPlayerName(playerData)}. ${qsm}`, embeds: [], components: [], files: [] });

    await queueUtils.queueOtherJob(interaction.client.shard, 
    {
        functionKey: QUEUE_FUNCTION_KEY,
        data: { 
            allycode: allycode,
            dsPlanet: dsPlanet,
            lsPlanet: lsPlanet,
            mxPlanet: mxPlanet,
            zeffo: zeffo,
            mandalore: mandalore,
            sector: null,
            exclude: exclude,
            specialMissionHandling: specialMissionHandling,
            showGuildMembers: showGuildMembers
        },
        channelId: editMessage.channelId ?? editMessage.channel.id,
        messageId: editMessage.id,
        userId: interaction.user.id,
    },
    {
        expireInHours: 4
    });
}

async function showFeedbackDuringUpdate(editMessage, planetsToUse, allycode)
{
    const planetNames = planetsToUse.map(p => `- ${tbCommon.getLabelForPlanet(p)}`).join("\n");
    const playerName = await playerUtils.getPlayerNameByAllycode(allycode);

    await editMessage.edit({ content: `Running \`/tb combat\` for ${playerName} on...\n${planetNames}`, embeds: [], components: [], files: []});
}

function createResponseContent(userId, playerData, possibilitiesString)
{
    return `${discordUtils.makeIdTaggable(userId)} Combat Recommendations for **${swapiUtils.getPlayerName(playerData)} (${swapiUtils.getPlayerAllycode(playerData)})** optimized from ${possibilitiesString} eligible combinations.`;
}

function getPlanetsToUse(planetsString, sector, lsPlanet, dsPlanet, mxPlanet, zeffo, mandalore)
{
    if (planetsString?.length > 0)
    {
        const planetsToUse = [];
        let planetCustomIdArray = planetsString.split("+");

        for (let i = 0; i < planetCustomIdArray.length; i++)
        {
            let planet = tbCommon.getPlanetFromCustomId(planetCustomIdArray[i]);
            if (planet)
            {
                planetsToUse.push(planet);
            }
        }
        
        return planetsToUse;
    }

    let planetsToUse;
    if (!dsPlanet && !lsPlanet && !mxPlanet && !zeffo && !mandalore && !sector) {
        planetsToUse = tbCommon.PLANETS.filter(p => p.sector === 1);
    }
    else
    {
        planetsToUse = [];
        if (dsPlanet) planetsToUse.push(tbCommon.PLANETS.find(p => p.name === dsPlanet));
        else if (sector) planetsToUse.push(tbCommon.PLANETS.find(p => p.sector === sector && p.alignment === tbCommon.ALIGNMENTS.DS && !p.specialPlanet));

        if (mxPlanet) planetsToUse.push(tbCommon.PLANETS.find(p => p.name === mxPlanet));
        else if (sector) planetsToUse.push(tbCommon.PLANETS.find(p => p.sector === sector && p.alignment === tbCommon.ALIGNMENTS.MX && !p.specialPlanet));

        if (lsPlanet) planetsToUse.push(tbCommon.PLANETS.find(p => p.name === lsPlanet));
        else if (sector) planetsToUse.push(tbCommon.PLANETS.find(p => p.sector === sector && p.alignment === tbCommon.ALIGNMENTS.LS && !p.specialPlanet));

        if (zeffo) planetsToUse.push(ZEFFO);
        if (mandalore) planetsToUse.push(MANDALORE);
    }

    return planetsToUse;
}

/*
 * catalog by alignment
 * load teams for selected planets
 * reduce the teams to those that match the player's roster
 * 
 * recommendations is an array of missions with an array of possible teams
 * each combination is a selection of one team from each mission
*/

async function generateCombatRecommendations(playerData, editMessage, { planetsToUse = [], exclude = null, specialMissionHandling = SPECIAL_MISSION_HANDLING.PRIORITIZE})
{
    let defIdsToExclude = await unitsUtils.parseUnitsStringAsDefIds(exclude);

    // planetsToUse has three planets to search on. Load team data from the database
    let missionTeams = await tbCommon.getTbCombatTeamsForPlanets(planetsToUse);
    let missionDefinitions = await tbCommon.getAllTbMissions();

    let recommendations = [];
    let possibilities = 1;
    
    // first pass, loop through missionTeams and find all teams that match the player's roster
    for (var p of planetsToUse)
    {
        let missionTeamsForPlanet = missionTeams.filter(t => t.planet === p.name);

        let missionNumber = 1;
        let allTeamsForMission = missionTeamsForPlanet.filter(m => m.mission === missionNumber);
        
        while (allTeamsForMission.length > 0) {

            let missionObj = {
                possibleTeams: [NULL_TEAM],
                assignedTeam: null,
                mission: missionNumber,
                planet: p,
                maxPointsForMission: 0,
                eligible: true,
                qualityMultiplier: 1,
                highQualityTeamCount: function() { return this.possibleTeams.filter(t => t.template.quality >= 70).length; },
                midQualityTeamCount: function() { return this.possibleTeams.filter(t => t.template.quality >= 40 && t.template.quality < 7).length; }
            };
            recommendations.push(missionObj);

            let missionDef = missionDefinitions.find(m => m.planet === p && m.mission === missionNumber);
            if (!missionDef) {
                throw new Error(`No mission definition found for planet ${p.name} and mission ${missionNumber}.`);
            };

            let isSpecial = missionDef.isSpecialMission;

            if (isSpecial)
            {
                missionObj.qualityMultiplier = getQualityMultiplierForSpecialMissionHandling(p, specialMissionHandling);
            }
            
            missionObj.maxPointsForMission = missionDef.maxPointsForMission;

            let isEligible = await tbCommon.isEligibleForMission(playerData, { planet: p, missionNumber: missionNumber, unitDefIdsToExclude: defIdsToExclude });

            if (!isEligible)
            {
                missionObj.eligible = false;
            }
            else if (!isSpecial ||
                (specialMissionHandling === SPECIAL_MISSION_HANDLING.SKIP_DS && p.alignment != tbCommon.ALIGNMENTS.DS) ||
                (specialMissionHandling === SPECIAL_MISSION_HANDLING.SKIP_MX && p.alignment != tbCommon.ALIGNMENTS.MX) ||
                (specialMissionHandling === SPECIAL_MISSION_HANDLING.SKIP_LS && p.alignment != tbCommon.ALIGNMENTS.LS) ||
                (specialMissionHandling !== SPECIAL_MISSION_HANDLING.SKIP_ALL))
            {
                for (var t of allTeamsForMission)
                {
                    let matchingTeam = await tbCommon.findMatchingTbTeamForPlayer(playerData, t, p, { minRelicLevelOverride: missionDef.minRelicLevelOverride, defIdsToExclude: defIdsToExclude });
                    if (!matchingTeam) continue;

                    missionObj.possibleTeams.unshift(matchingTeam); 
                }

                missionObj.possibleTeams.sort((a, b) => b.template.quality - a.template.quality); // sort matching teams by quality desc
                
                possibilities *= Math.max(1, missionObj.possibleTeams.length);
            }
            
            missionNumber++;
            allTeamsForMission = missionTeamsForPlanet.filter(m => m.mission === missionNumber);
        }
    }

    // sort by special missions DESC, then most points DESC, then high quality team count ASC, then mid quality team count ASC, 
    recommendations.sort((a, b) => 
    {
        if (a.possibleTeams.length === 0 && b.possibleTeams.length === 0) return 0;
        if (a.possibleTeams.length === 0) return 1;
        if (b.possibleTeams.length === 0) return -1;

        // if there's a special mission involved, make it higher in the sort, assuming special mission handling isn't set to skip it
        if (a.qualityMultiplier != b.qualityMultiplier) return b.qualityMultiplier - a.qualityMultiplier;

        // higher point missions should be checked first
        if (a.maxPointsForMission != b.maxPointsForMission) return b.maxPointsForMission - a.maxPointsForMission;

        let aHQ = a.highQualityTeamCount();
        let bHQ = b.highQualityTeamCount();
        if (aHQ != bHQ) return aHQ - bHQ;

        let aMQ = a.midQualityTeamCount();
        let bMQ = b.midQualityTeamCount();
        if (aMQ != bMQ) return aMQ - bMQ;

        return b.sector - a.sector;
    });

    const possibilitiesString = possibilities.toLocaleString();
    await editMessage.edit({ content: `Optimizing expected points across ${possibilitiesString} known team combinations...`, embeds: [], components: [], files: []});

    let bestTeamCombination = await findBestTeamCombination(recommendations);
    
    let embeds = [];
    let files = [];

    recommendations.sort((a, b) => {
        return a.mission - b.mission;
    });

    for (let p of planetsToUse)
    {
        let embed = new EmbedBuilder()
            .setTitle(`Teams for (${p.alignment} ${p.sector}) ${p.name}`)
            .setColor(tbCommon.ALIGNMENT_COLOR_MAP[p.alignment].number);

        embeds.push(embed);

        let recsForPlanet = recommendations.filter(r => r.planet === p);

        let fields = [];

        for (let rec of recsForPlanet)
        {
            let missionDef = missionDefinitions.find(m => m.planet === p && m.mission === rec.mission);
            let bestTeam = bestTeamCombination?.find(t => t.mission === rec.mission && t.planet === p);
            let missionNotes = missionDef?.notes;

            let field = { name: `${missionDef.icon ? `${missionDef.icon} ` : "" }Mission ${rec.mission}${missionNotes ? ` - ${missionNotes}` : ""}`, value: null };

            let isIneligbleBecauseOfBetterUseForRequiredUnit = (!bestTeam || !bestTeam.team) && bestTeamCombination?.find(t => t.team?.template.unitsRequired?.find(u => missionDef.unitDefIdRequirements?.find(reqU => reqU === u)));
            if (!rec.eligible || isIneligbleBecauseOfBetterUseForRequiredUnit)
            {
                field.value = `:x: ${!rec.eligible ? "Ineligible" : "Better use for required unit(s)"}`;
            }
            else if (missionDef.isSpecialMission && (
                (specialMissionHandling === SPECIAL_MISSION_HANDLING.SKIP_ALL) || 
                (specialMissionHandling === SPECIAL_MISSION_HANDLING.SKIP_DS && p.alignment === tbCommon.ALIGNMENTS.DS) || 
                (specialMissionHandling === SPECIAL_MISSION_HANDLING.SKIP_MX && p.alignment === tbCommon.ALIGNMENTS.MX) || 
                (specialMissionHandling === SPECIAL_MISSION_HANDLING.SKIP_LS && p.alignment === tbCommon.ALIGNMENTS.LS)
                ))
            {
                field.value = "Skipped"
            }
            else if (!bestTeam || !bestTeam.team || bestTeam.team === NULL_TEAM)
            {
                field.value = ":shrug:";
            }
            else
            {
                let icon;
                if (bestTeam.team.template.quality >= 70) icon = ":green_circle:";
                else if (bestTeam.team.template.quality >= 40) icon = ":yellow_circle:";
                else icon = ":red_circle:";
                field.value = `${icon} ${await tbCommon.getTbTeamText(bestTeam.team, true)}`

                // For future
                // if (rec.possibleTeams.length > 0)
                // {
                //     let promises = rec.possibleTeams.slice(0, 2).map(async (pt, i) => { return `${(i+1).toString()}. ${await getTeamText(pt)}`; } );
                //     let res = await Promise.all(promises);
                //     field.value += `\n\nAlternate teams:\n${res.join("\n")}`;                
                // }
            }

            fields.push(field);
        }

        embed.setFields(fields);

        let fileName = `${p.name.replace(/ /g, "_")}-labelled.png`;
        const file = new AttachmentBuilder(`./src/img/rote/${fileName}`);
        embed.setImage(`attachment://${fileName}`);

        files.push(file);
    }

    return { response: { embeds: embeds, files: files }, possibilitiesString: possibilitiesString };
}

const BASE_THRESHOLD_FOR_POINTS = 0.95;

async function getBestCombo(index, combination, alternatives, unitsInCombination, best)
{
    if (index == alternatives.length) {
        let expectedPoints = getExpectedPoints(combination);
        if (expectedPoints > best.points)
        {
            best.combination = combination.map(a => ({...a}));
            best.points = expectedPoints;
        }

        return;
    } else {
        combination[index] = {
            mission: alternatives[index].mission,
            planet: alternatives[index].planet,
            maxPointsForMission: alternatives[index].maxPointsForMission,
            qualityMultiplier: alternatives[index].qualityMultiplier,
            team: null
        };

        for (let alternative of alternatives[index].possibleTeams)
        {
            let overlap = false;

            // all required units are ... required. This is potentially buggy
            // when there are a limited set of optional units, but hopefully I 
            // can handle it in the database through team combinations
            for (let u of alternative.template.unitsRequired)
            {
                if (unitsInCombination[u]) {
                    overlap = true;
                    for (let uToUnset of alternative.template.unitsRequired)
                    {
                        if (uToUnset === u) break;
                        unitsInCombination[uToUnset] = null;
                    }
                    break;
                }

                unitsInCombination[u] = 1;
            }
            if (overlap) continue;

            combination[index].team = alternative;

            await getBestCombo(index + 1, combination, alternatives, unitsInCombination, best);

            if (best.points > best.maxPoints * best.thresholdForPoints) return; // found a winner

            combination[index].team = null;

            for (let u of alternative.template.unitsRequired)
            {
                unitsInCombination[u] = null;
            }
        }
        // await getBestCombo(index + 1, combination, alternatives, unitsInCombination, best);

        // if (best.points > best.maxPoints * best.thresholdForPoints) return; // found a winner
    }
}

function getQualityMultiplierForSpecialMissionHandling(planet, specialMissionHandling)
{
    switch (specialMissionHandling)
    {
        case SPECIAL_MISSION_HANDLING.PRIORITIZE:
            return 5;
        case SPECIAL_MISSION_HANDLING.SKIP_ALL:
            // don't include
            return 0;
        case SPECIAL_MISSION_HANDLING.SKIP_DS:
            if (planet.alignment === tbCommon.ALIGNMENTS.DS) return 0;
            return 5; // change quality if the special mission isn't DS
        case SPECIAL_MISSION_HANDLING.SKIP_MX:
            if (planet.alignment === tbCommon.ALIGNMENTS.MX) return 0;
            return 5;
        case SPECIAL_MISSION_HANDLING.SKIP_LS:
            if (planet.alignment === tbCommon.ALIGNMENTS.LS) return 0;
            return 5;
        case SPECIAL_MISSION_HANDLING.DO_NOT_PRIORITIZE:
            // no change to quality
            return 1;
    }
}

async function findBestTeamCombination(recommendations)
{
    // calculate a max possible points for the player for the selected planets
    let maxPoints = 0;
    for (var r of recommendations)
    {
        if (r.possibleTeams.length === 0) continue;

        let quality = r.possibleTeams[0].template.quality * r.qualityMultiplier;
        
        maxPoints += quality / MAX_QUALITY * r.maxPointsForMission;
    }

    let alternatives = recommendations.filter(r => r.possibleTeams.length > 0);
    let best = { 
        combination: null, 
        points: 0, 
        maxPoints: maxPoints, 
        thresholdForPoints: BASE_THRESHOLD_FOR_POINTS - alternatives.length * 0.003
    };
    let unitsInCombo = {};
    await getBestCombo(0, [], alternatives, unitsInCombo, best)

    return best.combination;
}

const MAX_QUALITY = 100;
function getExpectedPoints(combination)
{
    let expectedPoints = 0;
    for (var missionAssignment of combination)
    {
        if (!missionAssignment.team) continue;
        
        let quality = missionAssignment.team.template.quality * missionAssignment.qualityMultiplier;
        
        expectedPoints += quality / MAX_QUALITY * missionAssignment.maxPointsForMission;
    
    }    
    return expectedPoints;
}

function getSpecialMissionHandlingAbbrev(specialMissionHandling)
{
    switch (specialMissionHandling)
    {
        case SPECIAL_MISSION_HANDLING.PRIORITIZE: return "p";
        case SPECIAL_MISSION_HANDLING.SKIP_ALL: return "s";
        case SPECIAL_MISSION_HANDLING.SKIP_DS: return "d";
        case SPECIAL_MISSION_HANDLING.SKIP_MX: return "m";
        case SPECIAL_MISSION_HANDLING.SKIP_LS: return "l";
        case SPECIAL_MISSION_HANDLING.DO_NOT_PRIORITIZE: return "n";
    }
}

function addComponentsToResponse(response, allycode, specialMissionHandling, showGuildMembers)
{
    let specialMissionHandlingAbbrev = getSpecialMissionHandlingAbbrev(specialMissionHandling);

    let multiSelectRow = new ActionRowBuilder();
    let selectOptions = tbCommon.ALL_PLANET_CHOICES.map(p => { return { label: p.name, value: p.value }; });
    multiSelectRow.addComponents(
        new StringSelectMenuBuilder()
            .setCustomId(`tb:combat:sel:${allycode}:${specialMissionHandlingAbbrev}:${showGuildMembers ? "t" : "f"}`)
            .setPlaceholder('Choose up to five planets')
            .setMinValues(1)
            .setMaxValues(5)
            .addOptions(selectOptions)
    );

    response.components = [multiSelectRow];
}

async function addGuildMembersDropDowns(response, guildId, specialMissionHandling, planetsToUse)
{
    if (!guildId) return;
    const selectedPlanetsString = planetsToUse.map(p => tbCommon.getCustomIdForPlanet(p)).join("+");
    let membersActionRows = [];
    let specialMissionHandlingAbbrev = getSpecialMissionHandlingAbbrev(specialMissionHandling);

    const guildData = await guildUtils.getGuildDataFromAPI({ comlinkGuildId: guildId }, { useCache: false });
    const guildRoster = guildUtils.getGuildRoster(guildData);
    let options = guildRoster.map(m => { return { label: `${swapiUtils.getPlayerName(m)} (${swapiUtils.getPlayerAllycode(m)})`, value: swapiUtils.getPlayerAllycode(m) }})
    options.sort((a, b) => a.label.localeCompare(b.label));

    for (let m = 0; m < options.length; m += discordUtils.MAX_DISCORD_DROPDOWN_ITEMS)
    {
        let slicedOptions = options.slice(m, m + discordUtils.MAX_DISCORD_DROPDOWN_ITEMS);
        let row = new ActionRowBuilder()
            .addComponents(
                new StringSelectMenuBuilder()
                    .setCustomId(`tb:combat:guild${Math.floor(m/discordUtils.MAX_DISCORD_DROPDOWN_ITEMS)}:${specialMissionHandlingAbbrev}:${selectedPlanetsString}`)
                    .setMaxValues(1)
                    .setPlaceholder(`Choose a guild member (${slicedOptions[0].label[0].toUpperCase()} - ${slicedOptions[slicedOptions.length-1].label[0].toUpperCase()})`)
                    .addOptions(slicedOptions)
            );

        membersActionRows.push(row);
    }

    if (response.components?.length > 0) response.components = response.components.concat(membersActionRows);
    else response.components = membersActionRows;

}