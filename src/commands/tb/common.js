
let metadataCache = require("../../utils/metadataCache")
let database = require ("../../database")
let constants = require ("../../utils/constants")
let swapiUtils = require ("../../utils/swapi");
let unitsUtils = require ("../../utils/units")

const PLANET_CHOICES = 
[
    { name: "(DS 1) Mustafar", value: "Mustafar" },
    { name: "(MX 1) Corellia", value: "Corellia" },
    { name: "(LS 1) Coruscant", value: "Coruscant" },
    { name: "(DS 2) Geonosis", value: "Geonosis" },
    { name: "(MX 2) Felucia", value: "Felucia" },
    { name: "(LS 2) Bracca", value: "Bracca" },
    { name: "(DS 3) Dathomir", value: "Dathomir" },
    { name: "(MX 3) Tatooine", value: "Tatooine" },
    { name: "(LS 3) Kashyyyk", value: "Kashyyyk" },
    { name: "(DS 4) Haven-class Medical Station", value: "Haven-class Medical Station" },
    { name: "(MX 4) Kessel", value: "Kessel" },
    { name: "(LS 4) Lothal", value: "Lothal" },
    { name: "(DS 5) Malachor", value: "Malachor" },
    { name: "(MX 5) Vandor", value: "Vandor" },
    { name: "(LS 5) Ring of Kafrene", value: "Ring of Kafrene" },
    { name: "(DS 6) Death Star", value: "Death Star" },
    { name: "(MX 6) Hoth", value: "Hoth" },
    { name: "(LS 6) Scarif", value: "Scarif" },
    { name: "(BZ 1) Zeffo", value: "Zeffo" },
    { name: "(BZ 2) Mandalore", value: "Mandalore" }
];

const PLANET_CHOICES_NOT_BONUS = PLANET_CHOICES.filter(c => c.name.indexOf("BZ") === -1);

const UNIT_ALIGNMENT_REQUIREMENTS = {
    DS: [1, 3],
    MX: [1, 2, 3],
    LS: [1, 2]
}

const GET3_EMOJI_TAG = "<:get3:1065391509022711920>";
const REVA_EMOJI_TAG = "<:reva:1065475343709913229>";
const ALIGNMENTS = {
    DS: "DS",
    MX: "MX",
    LS: "LS"
}

const ALIGNMENT_COLOR_MAP = 
{
    "LS": { number: 0x3333cc, string: "rgb(51, 51, 204)" },
    "DS": { number: 0x781111, string: "rgb(120, 17, 17)" },
    "MX": { number: 0x999900, string: "rgb(153, 153, 0)" }
}

const TB_REWARDS = {
    GET1: {
        key: "GET1",
        name: "GET1"
    },
    GET2: {
        key: "GET2",
        name: "GET2"
    },
    GET3: {
        key: "GET3",
        name: "GET3"
    },
    THIRDSISTER: {
        key: "THIRDSISTER",
        name: "Third Sister Shards"
    },
}

const PLANET_DATA = 
[
    { name: "Mustafar", alignment: ALIGNMENTS.DS, sector: 1, maxPointsSquadMission: 200000, maxPointsFleetMission: 400000, ltrSort: 2 },
    { name: "Corellia", alignment: ALIGNMENTS.MX, sector: 1, maxPointsSquadMission: 200000, maxPointsFleetMission: 400000, ltrSort: 5 },
    { name: "Coruscant", alignment: ALIGNMENTS.LS, sector: 1, maxPointsSquadMission: 200000, maxPointsFleetMission: 400000, ltrSort: 10 },
    { name: "Geonosis", alignment: ALIGNMENTS.DS, sector: 2, maxPointsSquadMission: 250000, maxPointsFleetMission: 500000, ltrSort: 2 },
    { name: "Felucia", alignment: ALIGNMENTS.MX, sector: 2, maxPointsSquadMission: 250000, maxPointsFleetMission: 500000, ltrSort: 5 },
    { name: "Bracca", alignment: ALIGNMENTS.LS, sector: 2, maxPointsSquadMission: 250000, maxPointsFleetMission: 500000, ltrSort: 10 },
    { name: "Dathomir", alignment: ALIGNMENTS.DS, sector: 3, maxPointsSquadMission: 341250, maxPointsFleetMission: 682500, ltrSort: 2 },
    { name: "Tatooine", alignment: ALIGNMENTS.MX, sector: 3, maxPointsSquadMission: 341250, maxPointsFleetMission: 682500, ltrSort: 5 },
    { name: "Kashyyyk", alignment: ALIGNMENTS.LS, sector: 3, maxPointsSquadMission: 341250, maxPointsFleetMission: 682500, ltrSort: 10 },
    { name: "Zeffo", alignment: ALIGNMENTS.LS, sector: 3, maxPointsSquadMission: 341250, maxPointsFleetMission: 682500, specialPlanet: true, specialPlanetNumber: 1, ltrSort: 9 },
    { name: "Haven-class Medical Station", alignment: ALIGNMENTS.DS, sector: 4, maxPointsSquadMission: 493594, maxPointsFleetMission: 987188, ltrSort: 2 },
    { name: "Kessel", alignment: ALIGNMENTS.MX, sector: 4, maxPointsSquadMission: 493594, maxPointsFleetMission: 987188, ltrSort: 5 },
    { name: "Lothal", alignment: ALIGNMENTS.LS, sector: 4, maxPointsSquadMission: 493594, maxPointsFleetMission: 987188, ltrSort: 10 },
    { name: "Mandalore", alignment: ALIGNMENTS.MX, sector: 4, maxPointsSquadMission: 493594, maxPointsFleetMission: 987188, specialPlanet: true, specialPlanetNumber: 2, ltrSort: 4 },
    { name: "Malachor", alignment: ALIGNMENTS.DS, sector: 5, maxPointsSquadMission: 721744, maxPointsFleetMission: 1443488, ltrSort: 2 },
    { name: "Vandor", alignment: ALIGNMENTS.MX, sector: 5, maxPointsSquadMission: 721744, maxPointsFleetMission: 1443488, ltrSort: 5 },
    { name: "Ring of Kafrene", alignment: ALIGNMENTS.LS, sector: 5, maxPointsSquadMission: 721744, maxPointsFleetMission: 1443488, ltrSort: 10 },
    { name: "Death Star", alignment: ALIGNMENTS.DS, sector: 6, maxPointsSquadMission: 1151719, maxPointsFleetMission: 2303438, ltrSort: 2 },
    { name: "Hoth", alignment: ALIGNMENTS.MX, sector: 6, maxPointsSquadMission: 1151719, maxPointsFleetMission: 2303438, ltrSort: 5 },
    { name: "Scarif", alignment: ALIGNMENTS.LS, sector: 6, maxPointsSquadMission: 1151719, maxPointsFleetMission: 2303438, ltrSort: 10 }
];


const LS_PLANET_CHOICES = PLANET_CHOICES.filter(p => p.name.indexOf("LS") === 1);
const MX_PLANET_CHOICES = PLANET_CHOICES.filter(p => p.name.indexOf("MX") === 1);
const DS_PLANET_CHOICES = PLANET_CHOICES.filter(p => p.name.indexOf("DS") === 1);
const BZ_PLANET_CHOICES = PLANET_CHOICES.filter(p => p.name.indexOf("BZ") === 1);

const TB_PHASES =
[
    { n: 1, label: "Phase 1" },
    { n: 2, label: "Phase 2" },
    { n: 3, label: "Phase 3" },
    { n: 4, label: "Phase 4" },
    { n: 5, label: "Phase 5" },
    { n: 6, label: "Phase 6" }
] ;

const TB_PHASE_CHOICES = TB_PHASES.map(p => { return { name: p.label, value: p.n }; });

const RELIC_LEVEL_BY_SECTOR = {
    1: 5,
    2: 6,
    3: 7,
    4: 8,
    5: 9,
    6: 9
}

const MIN_RARITY = 7;

module.exports = {
    PLANETS: PLANET_DATA,
    LS_PLANET_CHOICES: LS_PLANET_CHOICES,
    MX_PLANET_CHOICES: MX_PLANET_CHOICES,
    DS_PLANET_CHOICES: DS_PLANET_CHOICES,
    BZ_PLANET_CHOICES: BZ_PLANET_CHOICES,
    ALL_PLANET_CHOICES: PLANET_CHOICES,
    ALL_PLANET_CHOICES_NOT_BONUS: PLANET_CHOICES_NOT_BONUS,
    ALIGNMENTS: ALIGNMENTS,
    ALIGNMENT_COLOR_MAP: ALIGNMENT_COLOR_MAP,
    UNIT_ALIGNMENT_REQUIREMENTS: UNIT_ALIGNMENT_REQUIREMENTS,
    TB_PHASES: TB_PHASES,
    TB_PHASE_CHOICES: TB_PHASE_CHOICES,
    MIN_RARITY: MIN_RARITY,
    TB_REWARDS: TB_REWARDS,
    getLabelForPlanet: function (p) { return `(${p.specialPlanet ? "BZ" : p.alignment} ${p.sector}) ${p.name}`; },
    getCustomIdForPlanet: function (p) { return p.specialPlanet ? `BZ${p.specialPlanetNumber}` : `${p.alignment}${p.sector}` },
    getPlanetFromCustomId: function (id) { return PLANET_DATA.find(p => this.getCustomIdForPlanet(p) === id); },
    minRelicLevelBySector: minRelicLevelBySector,
    minRelicLevelByPlanet: function (p) { return RELIC_LEVEL_BY_SECTOR[p.sector]; },
    getAllTbMissions: getAllTbMissions,
    estimateSpecialMissionIncomeForPlayer: estimateSpecialMissionIncomeForPlayer,
    isEligibleForMission: isEligibleForMission,
    getAllTbCombatTeamRecommendations: getAllTbCombatTeamRecommendations,
    getTbCombatTeamsForPlanets: getTbCombatTeamsForPlanets,
    getTbTeamText: getTbTeamText,
    findMatchingTbTeamForPlayer: findMatchingTbTeamForPlayer
};

function minRelicLevelBySector (s) { return RELIC_LEVEL_BY_SECTOR[s]; }

async function getAllTbMissions() {
    let missions = metadataCache.get(metadataCache.CACHE_KEYS.tbMissions);
    if (missions === undefined)
    { 
        let data = await database.db.any(`
            SELECT planet, mission, notes, combat_type, is_special, unit_def_id_rqmt, faction_rqmt, team_size_rqmt, min_relic_level_override, max_points_override, reward
            FROM tb_mission
            ORDER BY planet, mission`);

        missions = data.map(d => {
            let obj = {
                planet: PLANET_DATA.find(p => p.name == d.planet),
                mission: d.mission,
                notes: d.notes,
                combatType: d.combat_type === "SQUAD" ? constants.COMBAT_TYPES.SQUAD : constants.COMBAT_TYPES.FLEET,
                isSpecialMission: d.is_special,
                unitDefIdRequirements: d.unit_def_id_rqmt,
                factionRequirements: d.faction_rqmt,
                teamSizeRequirement: d.team_size_rqmt,
                minRelicLevelOverride: d.min_relic_level_override,
                maxPointsOverride: d.max_points_override,
                reward: d.reward
            };

            // custom icons
            let isRevaMission = d.planet === "Tatooine" && d.mission === 5;
            obj.icon = isRevaMission ? REVA_EMOJI_TAG : (d.is_special ? GET3_EMOJI_TAG : null);

            
            const maxPointsForMission = obj.maxPointsOverride ?? 
                (obj.combatType === constants.COMBAT_TYPES.FLEET 
                    ? obj.planet.maxPointsFleetMission
                    : obj.planet.maxPointsSquadMission
                );

            obj.maxPointsForMission = maxPointsForMission;

            return obj;
        });

        metadataCache.set(metadataCache.CACHE_KEYS.tbMissions, missions, 604800); // 7 days
    }

    return missions;
}

async function getTbCombatTeamsForPlanets(planets)
{
    let allTeams = await getAllTbCombatTeamRecommendations();

    return allTeams.filter(t => planets.find(p => p.name === t.planet));
}

async function getAllTbCombatTeamRecommendations()
{
    let teams = metadataCache.get(metadataCache.CACHE_KEYS.tbCombatTeamRecommendations);
    if (teams === undefined)
    { 
        let data = await database.db.any(`
            SELECT team_name, units_required, unit_options, faction_options, planet, mission, notes, quality, team_size, mission_type, is_special_mission, omicrons_required
            FROM tb_combat_team
            ORDER BY planet, mission, quality, team_name`);

        teams = data.map(d => {
            return {
                teamName: d.team_name,
                unitsRequired: d.units_required,
                unitOptions: d.unit_options,
                factionOptions: d.faction_options,
                planet: d.planet,
                mission: d.mission,
                teamSize: d.team_size,
                notes: d.notes,
                quality: d.quality,
                missionType: d.mission_type === "SQUAD" ? constants.COMBAT_TYPES.SQUAD : constants.COMBAT_TYPES.FLEET,
                isSpecialMission: d.is_special_mission,
                omicronsRequired: d.omicrons_required
            }
        });

        metadataCache.set(metadataCache.CACHE_KEYS.tbCombatTeamRecommendations, teams, 600); // 10 minutes, since I'm changing frequently
    }

    return teams;
}

async function getTbTeamText(team, includeNotes)
{
    let template = team.template ?? team;
    let units = team.units ?? team.unitOptions;
    let omiUnitDefIds = await unitsUtils.getUnitDefIdsByAbilities(team.omicronsRequired);
    let requiredUnitNames = await unitsUtils.getUnitsNames(template.unitsRequired, true, omiUnitDefIds)

    let unitsStr = requiredUnitNames.join(", ");
    if (requiredUnitNames.length < template.teamSize)
    {
        let diff = template.teamSize - requiredUnitNames.length;
        unitsStr += ` +${diff}`
        
        let additionalUnits = "";
        if (units?.length > 0)
        {
            additionalUnits += (await unitsUtils.getUnitsNames(units, true, omiUnitDefIds)).join(", ");
            additionalUnits += ", etc."
        }

        if (units?.length < diff)
        {
            if (template.factionOptions?.length > 0)
            {
                if (additionalUnits.length != 0) additionalUnits += ", ";
                additionalUnits += template.factionOptions.join(", ");
            }
        }
        if (additionalUnits.length > 0)
        {
            unitsStr += ` (${additionalUnits})`;
        }
    }

    return `${unitsStr}${(includeNotes && template.notes) ? `\n> ${template.notes.replace(/\n/g, "\n> ")}` : ""}`
}

async function isEligibleForMission(playerData, { missionDef = null, planet = null, missionNumber = null, unitDefIdsToExclude = null })
{
    // to be eligible for a mission, the player must have 5 units, max 1 GL, of the correct alignment and at the right relic level

    if (missionDef == null)
    {
        let tbMissions = await getAllTbMissions();
        missionDef = tbMissions.find(m => m.planet === planet && m.mission === missionNumber);
    }

    if (planet == null)
    {
        planet = missionDef.planet;
    }

    let minRelicLevel = missionDef.minRelicLevelOverride ?? minRelicLevelBySector(planet.sector);
    let roster = swapiUtils.getPlayerRoster(playerData);
    let targetTeamSize = missionDef.teamSizeRequirement ?? (missionDef.combatType === constants.COMBAT_TYPES.FLEET ? 3 : 5);
    let alignmentRequirements = UNIT_ALIGNMENT_REQUIREMENTS[planet.alignment];

    let filteredRoster;
    if (missionDef.combatType === constants.COMBAT_TYPES.FLEET)
    {
        filteredRoster = roster.filter(u => 
            {
                if (unitDefIdsToExclude?.find(d => d === swapiUtils.getUnitDefId(u))) return false;
                if (swapiUtils.getUnitCombatType(u) !== constants.COMBAT_TYPES.FLEET) return false;
                if (swapiUtils.getUnitRarity(u) < 7) return false;
                if (!alignmentRequirements.find(a => a === u.alignment)) return false;
                if (!unitsUtils.unitHasAllFactions(u, missionDef.factionRequirements)) return false;

                return true;
            });
    }
    else
    {
        filteredRoster = roster.filter(u => 
            {
                if (unitDefIdsToExclude?.find(d => d === swapiUtils.getUnitDefId(u))) return false;
                if (swapiUtils.getUnitCombatType(u) !== constants.COMBAT_TYPES.SQUAD) return false;
                if (swapiUtils.getUnitRelicLevel(u) - 2 < minRelicLevel) return false;
                if (!alignmentRequirements.find(a => a === u.alignment)) return false;
                if (!unitsUtils.unitHasAllFactions(u, missionDef.factionRequirements)) return false;

                return true;
            });
    }

    if (filteredRoster.length < targetTeamSize) return false;

    if (missionDef.unitDefIdRequirements)
    {
        let matchingUnits = filteredRoster.filter(u => missionDef.unitDefIdRequirements.find(r => r === swapiUtils.getUnitDefId(u)));
        if (matchingUnits.length >= targetTeamSize) return true; 
        if (matchingUnits.length != missionDef.unitDefIdRequirements.length) return false;
    }

    let glCount = filteredRoster.filter(u => u.is_galactic_legend).length;
    
    let totalMatchingUnits = filteredRoster.length - (glCount > 1 ? glCount - 1 : 0);

    return totalMatchingUnits >= targetTeamSize;
}

async function findMatchingTbTeamForPlayer(playerData, teamTemplate, planet, { defIdsToExclude, minRelicLevelOverride })
{
    let minRelicLevel = minRelicLevelOverride ?? minRelicLevelBySector(planet.sector);

    // the player must have all units in teamTemplate.unitsRequired
    // player must have all the omicron abilities activated in teamTemplate.omicronsRequired
    // if there are teamTemplate.factionOptions, the player must be able to make a full team (teamTemplate.teamSize) with unitsRequired + factionOptions
    // if there are teamTemplate.unitOptions, the player must be able to make a full team (teamTemplate.teamSize) with unitsRequired + unitOptions
    
    let playerOmis = swapiUtils.getPlayerOmicrons(playerData, swapiUtils.OMICRON_MODE.TERRITORY_BATTLE_BOTH_OMICRON);
    if (teamTemplate.omicronsRequired)
    {
        if (!playerOmis || playerOmis.length === 0) return null; // omi required, but player doesn't have any
        let omiNotFound = teamTemplate.omicronsRequired.find(o => !playerOmis?.find(po => o === po));
        if (omiNotFound) return null;
    }

    // make sure they have the basic required units at the right relic level
    for (let requiredUnitDefId of teamTemplate.unitsRequired)
    {
        if (defIdsToExclude?.find(d => d === requiredUnitDefId)) return null;
        const { errorCode, unit } = swapiUtils.findUnit(playerData, requiredUnitDefId, { minRarity: 7, minRelicLevel: minRelicLevel });
        if (errorCode != null) return null;
    }

    let team = {
        units: new Array(),
        template: teamTemplate,
        allUnits: function () { return teamTemplate.unitsRequired.concat(this.units) }
    }

    let unitsMissing = team.template.teamSize - team.allUnits().length;
    
    // team is full just from the required units
    if (unitsMissing === 0) return team;

    let foundUnits = 0;
    if (team.template.factionOptions)
    {
        // let allUnits = await swapiUtils.getUnitsList();

        // let combatType = allUnits.find(u => swapiUtils.getUnitDefId(u) === team.template.unitsRequired[0]);
        // let possibleMatches = playerRoster.filter(ru => {
        //     if (swapiUtils.getUnitCombatType(ru) !== combatType) return false;

        //     if (swapiUtils.getUnitCombatType(ru) === constants.COMBAT_TYPES.FLEET && swapiUtils.getUnitRarity(ru) < 7) return false;
        //     if (swapiUtils.getUnitCombatType(ru) === constants.COMBAT_TYPES.SQUAD && swapiUtils.getUnitRelicLevel(ru) - 2 < minRelicLevel) return false;
            
        //     if (ru.categories.find(c => team.template.factionOptions.find(f => f === c))) return true;
        //     return false;
        // });

        
        // possibleMatches.sort((a, b) => {
        //     let gpDiff = swapiUtils.getUnitGP(b) - swapiUtils.getUnitGP(a);
        //     if (gpDiff != 0) return gpDiff;
        // });
        
        // team.units = possibleMatches.map(pm => swapiUtils.getUnitDefId(pm));
    }
    else if (team.template.unitOptions)
    {
        for (let option of team.template.unitOptions)
        {
            if (defIdsToExclude?.find(d => d === option)) continue;

            const { errorCode, unit }= swapiUtils.findUnit(playerData, option, { minRarity: 7, minRelicLevel: minRelicLevel });

            if (errorCode != null) continue; // they don't have it

            team.units.push(swapiUtils.getUnitDefId(unit));
        }
    }

    return team;
}

async function estimateSpecialMissionIncomeForPlayer(playerData)
{
    let missions = await getAllTbMissions();
    let specialMissions = missions.filter(m => m.isSpecialMission);

    let specialMissionEligibility = new Array();
    const income = { };

    for (const m of specialMissions)
    {
        let isEligible = await isEligibleForMission(playerData, { missionDef: m });
        
        specialMissionEligibility.push({ mission: m, eligible: isEligible });
        
        if (!isEligible) continue;

        for (let r of m.reward)
        {
            if (income[r.type])
            {
                income[r.type] += r.value;
            }
            else
            {
                income[r.type] = r.value;
            }
        }
    }

    return { specialMissionEligibility, income };
}