const specialsGuild = require("./specials/specials-guild");
const specialsPlayer = require("./specials/specials-player")
const { SlashCommandSubcommandGroupBuilder } = require("@discordjs/builders");

module.exports = {
    data: new SlashCommandSubcommandGroupBuilder()
        .setName("specials")
        .setDescription("TB Specials")
        .addSubcommand(specialsGuild.data)
        .addSubcommand(specialsPlayer.data),
    interact: async function(interaction)
    {
      let subcommand = interaction.options.getSubcommand();

      switch (subcommand)
      {
        case specialsGuild.data.name:
            await specialsGuild.interact(interaction);
            break;
        case specialsPlayer.data.name:
            await specialsPlayer.interact(interaction);
            break;
      }
    },
    autocomplete: async function(interaction)
    {
        let subcommand = interaction.options.getSubcommand();
  
        switch (subcommand)
        {
            case specialsGuild.data.name:
                await specialsGuild.autocomplete(interaction);
                break;
            case specialsPlayer.data.name:
                await specialsPlayer.autocomplete(interaction);
                break;
        }
    },
    onSelect: async function(interaction)
    {
        if (interaction.customId.startsWith("tb:specials:p")) await specialsPlayer.onSelect(interaction);
        else if (interaction.customId.startsWith("tb:specials:g")) await specialsGuild.onSelect(interaction);
    }
}