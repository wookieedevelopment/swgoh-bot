const { EmbedBuilder, ModalBuilder, TextInputBuilder, TextInputStyle, ActionRowBuilder } = require("discord.js");
const database = require("../../database");
const discordUtils = require("../../utils/discord")
const guildUtils = require("../../utils/guild");
const playerUtils = require("../../utils/player");
const counterUtils = require("./counterUtils")
const { SlashCommandBuilder, SlashCommandSubcommandBuilder } = require('@discordjs/builders');
const { logger, formatError } = require("../../utils/log.js");

module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("update")
        .setDescription("Update a guild-specific counter write-up.")
        .addStringOption(o => 
            o.setName("name")
            .setDescription("The name of the counter to update.")
            .setAutocomplete(true)
            .setRequired(true)),
    permissionLevel: playerUtils.UserRoles.GuildAdmin,
    interact: async function(interaction)
    {   
        let { botUser, guildId, error } = await playerUtils.authorize(interaction, this.permissionLevel)
        if (error) {
            await interaction.reply({embeds: [discordUtils.createErrorEmbed("Error", error)]});
            return;
        }

        let name = interaction.options.getString("name");
        let counter = await counterUtils.findCounter(guildId, name);

        if (!counter)
        {
            await interaction.reply("Your guild does not have a counter by that name. Use **/counters list** to see all your counters.");
            return;
        }

        const modal = new ModalBuilder() // We create a Modal
            .setCustomId('counters:update')
            .setTitle('Update Guild Counter')
            .addComponents(
                new ActionRowBuilder()
                    .addComponents(
                        new TextInputBuilder() // We create a Text Input Component
                            .setCustomId(counter.name)
                            .setLabel('Counter Name - add a new counter to change')
                            .setStyle(TextInputStyle.Short)
                            .setMinLength(1)
                            .setMaxLength(50)
                            .setValue(counter.name)
                            .setRequired(false)),
                new ActionRowBuilder()
                    .addComponents(
                        new TextInputBuilder()
                            .setCustomId('description')
                            .setLabel("Short Description")
                            .setStyle(TextInputStyle.Short)
                            .setMinLength(1)
                            .setMaxLength(100)
                            .setPlaceholder('Enter short description (summary)')
                            .setValue(counter.shortDescription ?? "")
                            .setRequired(true)),
                new ActionRowBuilder()
                    .addComponents(
                        new TextInputBuilder()
                            .setCustomId('text')
                            .setLabel('Counter Text')
                            .setStyle(TextInputStyle.Paragraph)
                            .setMinLength(1)
                            .setMaxLength(2000)
                            .setPlaceholder('Enter the counter write-up here.\r\nCan be multiple lines, include formatting, etc.')
                            .setValue(counter.text ?? "")
                            .setRequired(true))
            );

        await interaction.showModal(modal);
    },
    handleModal: async function(interaction)
    {
        await interaction.deferReply();
        const name = interaction.components[0].components[0].customId; // first component is name
        const description = interaction.fields.getTextInputValue('description');
        const text = interaction.fields.getTextInputValue('text');
        
        let { botUser, guildId, error } = await playerUtils.authorize(interaction, this.permissionLevel)
        if (error) {
            await interaction.editReply({embeds: [discordUtils.createErrorEmbed("Error", error)]});
            return;
        }

        let embed = await this.updateCounter(guildId, name, description, text);
        await interaction.editReply({ embeds: [embed]})
    },
    autocomplete: async function(interaction)
    {
        let response = await counterUtils.getAutocompleteResponse(interaction, this.permissionLevel);
        await interaction.respond(response);
    },
    updateCounter: async function(guildId, name, description, text)
    {

        let responseMessage;

        if (!name || name.length > counterUtils.MAX_COUNTER_NAME_LENGTH)
        {
            responseMessage = "Name must be between 1 and 100 characters.";
        }
        else
        {
            try {
                let r = await database.db.result(`
                UPDATE guild_writeup 
                SET name = $2, short_desc = $3, text = $4
                WHERE guild_id = $1 AND name = $2`, [guildId, name.toLowerCase(), description, text, counterUtils.counterWriteUpType]);

                if (r.rowCount == 1)
                {
                    responseMessage = "Guild counter updated for " + name + ".";
                }
                else
                {
                    responseMessage = "Something went wrong. Please try again, or if it is not working, contact the bot owner."
                }
            } catch (error) {
                
                responseMessage = `Failed to update **${name.toLowerCase()}**. Please try again or contact the bot owner.`;
                logger.error(responseMessage + "\r\n" + formatError(error))
            }
        }

        let embed = new EmbedBuilder()
            .setTitle("Result")
            .setColor(0xD2691E)
            .setDescription(responseMessage);

        return embed;
    }
}