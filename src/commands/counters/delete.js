const { EmbedBuilder } = require("discord.js");
const database = require("../../database");
const discordUtils = require("../../utils/discord")
const guildUtils = require("../../utils/guild");
const playerUtils = require("../../utils/player");
const counterUtils = require("./counterUtils")
const { SlashCommandBuilder, SlashCommandSubcommandBuilder } = require('@discordjs/builders');

module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("delete")
        .setDescription("Delete a guild-specific counter write-up.")
        .addStringOption(o => 
            o.setName("name")
            .setDescription("The name of the counter to delete.")
            .setRequired(true)
            .setAutocomplete(true)),
    name: "counters.delete",
    aliases: ["c.delete"],
    description: "Delete a guild-specific counter write-up",
    interact: async function(interaction)
    {
        let name = interaction.options.getString("name");
        let responseEmbed = await this.deleteCounter(interaction, name);
        await interaction.editReply({ embeds: [responseEmbed]})
    },
    autocomplete: async function(interaction)
    {
        let response = await counterUtils.getAutocompleteResponse(interaction, this.permissionLevel);
        await interaction.respond(response);
    },
    execute: async function(client, input, args) {
        if (!args || args.length == 0)
        {
            await input.reply({ embeds: [discordUtils.createErrorEmbed("Command Error", "You have to specify the counter to delete.  See your list of counters with **/counters list**.  For example:\r\n**/counters delete name:CLS**") ] } );
            return;
        }
        var counterName = args[0];

        let responseEmbed = await this.deleteCounter(input, counterName);
        await input.reply({ embeds: [responseEmbed]})
    },
    deleteCounter: async function(interactionOrInput, counterName)
    {
        let { botUser, guildId, error } = await playerUtils.authorize(interactionOrInput, playerUtils.UserRoles.GuildAdmin)
        if (error) {
            return discordUtils.createErrorEmbed("Error", error);
        }

        var deleted = await database.db.result("DELETE FROM guild_writeup WHERE guild_id = $1 AND name = $2 AND type = $3",
            [guildId, counterName.toLowerCase(), counterUtils.counterWriteUpType], r => r.rowCount);

        let responseMessage;
        if (deleted)
        {
            responseMessage = "Deleted your guild counter **" + counterName + "**.";
        }
        else
        {
            responseMessage = "Your guild does not have a counter by that name to delete.";
        }

        let embed = new EmbedBuilder()
            .setTitle("Result")
            .setColor(0xD2691E)
            .setDescription(responseMessage);

        return embed;
    }
}