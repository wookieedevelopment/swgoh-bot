const playerUtils = require("../../utils/player")
const discordUtils = require("../../utils/discord")
const database = require("../../database")

module.exports = {
    MAX_COUNTER_NAME_LENGTH: discordUtils.MAX_DISCORD_AUTOCOMPLETE_CHOICE_LENGTH, 
    counterWriteUpType: "COUNTER",
    getAutocompleteCounterNames: async function(guildId, filter)
    {
        let names = await database.db.any(
            `select name, short_desc
            from guild_writeup
            WHERE guild_id = $1 AND type = $3 AND (name ILIKE $2 OR short_desc ILIKE $2)
            LIMIT 25
            `, [guildId, `%${filter}%`, this.counterWriteUpType]);

        return names;
    },
    getAutocompleteResponse: async function(interaction, permissionLevel)
    {
        let { botUser, guildId, error } = await playerUtils.authorize(interaction, permissionLevel)
        if (error) {
            return [{ name: "You do not have permission.", value: "wwwwwwww" }];
        }

        const focusedValue = interaction.options.getFocused();

        const names = await this.getAutocompleteCounterNames(guildId, focusedValue);

        return names
            .filter(n => n.name.length <= this.MAX_COUNTER_NAME_LENGTH)
            .map(choice => (
            { 
                name: discordUtils.fitForAutocompleteChoice(`${choice.name} :: ${choice.short_desc}`), 
                value: choice.name 
            }));
    },
    findCounter: async function(guildId, counterName)
    {
        let r = await database.db.oneOrNone("SELECT name, short_desc, text FROM guild_writeup WHERE guild_id = $1 AND name = $2 AND type = $3", [guildId, counterName.toLowerCase(), this.counterWriteUpType])

        if (!r)
            return null;

        return {
            name: r.name,
            shortDescription: r.short_desc,
            text: r.text
        };
    }
}