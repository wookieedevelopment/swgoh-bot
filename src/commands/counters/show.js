const { EmbedBuilder } = require("discord.js");
const database = require("../../database");
const guildUtils = require("../../utils/guild");
const playerUtils = require("../../utils/player");
const counterUtils = require("./counterUtils")
const { SlashCommandBuilder, SlashCommandSubcommandBuilder } = require('@discordjs/builders');

module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("show")
        .setDescription("Show a counter.")
        .addStringOption(o => 
            o.setName("name")
            .setDescription("The name of the counter.")
            .setRequired(true)
            .setAutocomplete(true)),
    permissionLevel: playerUtils.UserRoles.User,
    interact: async function(interaction)
    {
        let { botUser, guildId, error } = await playerUtils.authorize(interaction, this.permissionLevel)
        if (error) {
            await interaction.editReply(error);
            return;
        }

        let counterName = interaction.options.getString("name");
        let counter = await counterUtils.findCounter(guildId, counterName);

        if (!counter)
        {
            await interaction.editReply("Your guild does not have a counter by that name. Use **list** to see all your counters.");
            return;
        }

        var embedToSend = new EmbedBuilder()
            .setTitle(counter.name)
            .setColor(0x347712)
            .setDescription("**" + counter.shortDescription + "**\r\n\r\n" + counter.text)

        await interaction.editReply({ embeds: [embedToSend] });
    },
    autocomplete: async function(interaction)
    {
        let response = await counterUtils.getAutocompleteResponse(interaction, this.permissionLevel);
        await interaction.respond(response);
    }
}