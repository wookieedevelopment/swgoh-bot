const { EmbedBuilder, AttachmentBuilder } = require("discord.js");
const discordUtils = require("../../utils/discord.js");
const { SlashCommandBuilder, SlashCommandSubcommandBuilder } = require('@discordjs/builders');

const CSV_TEMPLATE = Buffer.from(`"name","short_desc","text"\n"Example name (max length 50, no new lines)","Example short description (max length 200, no new lines)","Example text (Max length 2000).\nNew lines are permitted.\nQuotes can be used (show as ""double quotes in notepad or equivalent"")"`);
const SAMPLE_CSV_FILE = new AttachmentBuilder(CSV_TEMPLATE, { name: `counters_template.csv` });

module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("help")
        .setDescription("Show help for counters."),
    interact: async function(interaction)
    {
        let adminCommandsField = {
            name: "Guild Counter Administration",
            value:
`**add**: add a counter write-up
**update**: update a counter write-up
**delete**: delete a counter write-up
**export**: export your guild's counters as a CSV
**import**: import counters for your guild in CSV format. Template is attached here.`,
            inline: false
        };

        
        let userCommandsField = {
            name: "Guild Member Commands",
            value:
`**list**: list your guild's counters
**show**: show a counter`,
            inline: false
        };


        let embed = new EmbedBuilder()
            .setTitle("WookieeBot Guild Counters")
            .setColor(0xD2691E)
            .addFields(adminCommandsField, userCommandsField);

        await interaction.editReply({ embeds: [embed], files: [SAMPLE_CSV_FILE] });
    }

}
