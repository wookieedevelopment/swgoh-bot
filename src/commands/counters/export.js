const { EmbedBuilder, AttachmentBuilder } = require("discord.js");
const database = require("../../database");
const playerUtils = require("../../utils/player");
const discordUtils = require("../../utils/discord");
const counterUtils = require("./counterUtils");
const { parseAsync } = require('json2csv');
const { SlashCommandSubcommandBuilder } = require('@discordjs/builders');

module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("export")
        .setDescription("Export guild counters as CSV"),
    permissionLevel: playerUtils.UserRoles.GuildAdmin,
    interact: async function(interaction)
    {
        let { botUser, guildId, error } = await playerUtils.authorize(interaction, this.permissionLevel);     
        if (error)
        {
            await interaction.editReply({ content: error })
            return;
        }

        let file = await this.exportCounters(botUser, guildId);

        await interaction.editReply({ files: [file] });
    },
    exportCounters: async function(botUser, guildId)
    {
        let csvData = await database.db.any(`
            SELECT name, short_desc, text
            FROM guild_writeup 
            WHERE guild_id = $1 AND type = $2`, [guildId, counterUtils.counterWriteUpType]);

        
        const csvParsingOptions = { defaultValue: 0 }
        let csvText = await parseAsync(csvData, csvParsingOptions);

        let csvBuffer = Buffer.from(csvText);

        let guildName = botUser.guilds.find(g => g.guildId == guildId).guildName;

        let csvAttachment = new AttachmentBuilder(csvBuffer, { name: `counters_${guildName}.csv`});

        return csvAttachment;
    }
}