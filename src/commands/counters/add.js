const { EmbedBuilder, ModalBuilder, TextInputBuilder, TextInputStyle, ActionRowBuilder } = require("discord.js");
const database = require("../../database");
const discordUtils = require("../../utils/discord")
const guildUtils = require("../../utils/guild");
const playerUtils = require("../../utils/player");
const counterUtils = require("./counterUtils")
const { SlashCommandSubcommandBuilder } = require('@discordjs/builders');
const { logger, formatError } = require("../../utils/log.js");
const { Formatters } = require('discord.js');

const modal = new ModalBuilder() // We create a Modal
    .setCustomId('counters:add')
    .setTitle('Add a new Guild Counter')
    .addComponents(
        new ActionRowBuilder()
            .addComponents(
                new TextInputBuilder()
                    .setCustomId('name')
                    .setLabel('Counter Name')
                    .setStyle(TextInputStyle.Short)
                    .setMinLength(1)
                    .setMaxLength(50)
                    .setPlaceholder('Enter counter name')
                    .setRequired(true)),
        new ActionRowBuilder()
            .addComponents(
                new TextInputBuilder()
                    .setCustomId('description')
                    .setLabel("Short Description")
                    .setStyle(TextInputStyle.Short)
                    .setMinLength(1)
                    .setMaxLength(100)
                    .setPlaceholder('Enter short description (summary)')
                    .setRequired(true)),
        new ActionRowBuilder()
            .addComponents(
                new TextInputBuilder()
                    .setCustomId('text')
                    .setLabel('Counter Text')
                    .setStyle(TextInputStyle.Paragraph)
                    .setMinLength(1)
                    .setMaxLength(2000)
                    .setPlaceholder('Enter the counter write-up here.\r\nCan be multiple lines, include formatting, etc.')
                    .setRequired(true))
    );

module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("add")
        .setDescription("Add a guild-specific counter write-up."),
    permissionLevel: playerUtils.UserRoles.GuildAdmin,
    interact: async function (interaction) {
        
        await interaction.showModal(modal);

        return;
    },
    handleModal: async function (interaction) {
        await interaction.deferReply();

        let { botUser, guildId, error } = await playerUtils.authorize(interaction, this.permissionLevel)
        if (error) {
            await interaction.editReply({ embeds: [discordUtils.createErrorEmbed("Error", error)] });
            return;
        }

        const name = interaction.fields.getTextInputValue('name');
        const description = interaction.fields.getTextInputValue('description');
        const text = interaction.fields.getTextInputValue('text');

        let embed = await this.addCounter(guildId, name, description, text);
        await interaction.editReply({ embeds: [embed] })
    },
    addCounter: async function (guildId, name, description, text) {

        let responseMessage, color = 0xD2691E;

        if (!name || name.length > counterUtils.MAX_COUNTER_NAME_LENGTH) {
            responseMessage = "Name must be between 1 and 100 characters.";
        }
        else {
            try {
                let r = await database.db.result(`
                INSERT INTO guild_writeup (guild_id, name, short_desc, text, type) VALUES ($1, $2, $3, $4, $5) 
                ON CONFLICT (guild_id,name,type)
                DO NOTHING`, [guildId, name.toLowerCase(), description, text, counterUtils.counterWriteUpType]);

                if (r.rowCount == 1) {
                    color = 0x007700;
                    responseMessage = "Added guild counter " + name + ": " + description + ".";
                }
                else {
                    color = 0x770000;
                    responseMessage =
                        `A counter already exists by that name. Please try again with a different name.
For copy/paste purposes, here is what you entered:
Name: \`${name}\`
Description: \`${description}\`
Text: 
${text}`;

                }
            } catch (error) {

                responseMessage = `Failed to record description for **${name.toLowerCase()}**. Please try again or contact the bot owner.`;
                logger.error(responseMessage + "\r\n" + formatError(error))
                color = 0x770000;
            }
        }

        let embed = new EmbedBuilder()
            .setTitle("Result")
            .setColor(color)
            .setDescription(responseMessage);

        return embed;
    }
}