const { EmbedBuilder, Embed } = require("discord.js");
const database = require("../../database");
const guildUtils = require("../../utils/guild");
const playerUtils = require("../../utils/player");
const discordUtils = require("../../utils/discord");
const counterUtils = require("./counterUtils")
const { SlashCommandBuilder, SlashCommandSubcommandBuilder } = require('@discordjs/builders');

module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("list")
        .setDescription("List your guild's counters.")
        .addStringOption(o => 
            o.setName("filter")
            .setDescription("A filter to limit the counters shown.")
            .setRequired(false)),
    name: "counters.list",
    aliases: ["c.list"],
    description: "List your guild-specific counter write-ups",
    interact: async function(interaction)
    {
        let filter = interaction.options.getString("filter");
        let str = await this.getListString(interaction, filter);

        if (str.data)
        {
            await interaction.editReply({ embeds: [str] });
            return;
        }

        let messagesArray = discordUtils.splitMessage(str, "\r\n");
        for (let m = 0; m < messagesArray.length; m++)
        {
            if (m == 0)
                await interaction.editReply(messagesArray[m]);
            else 
                await interaction.followUp(messagesArray[m])
        }
        
        // let embed = new EmbedBuilder()
        //     .setTitle("Available Counters")
        //     .setColor(0xD2691E)
        //     .setDescription(listString);

        // return embed;

        // await interaction.editReply({ embeds: [embed] });
    },
    getListString: async function(interaction, filter)
    {
        let { botUser, guildId, error } = await playerUtils.authorize(interaction, playerUtils.UserRoles.User)
        if (error) {
            return discordUtils.createErrorEmbed("Error", error);
        }

        let countersList;
        if (!filter)
        {
            countersList = await database.db.any(
                `SELECT name, short_desc 
                FROM guild_writeup 
                WHERE guild_id = $1 AND type = $2 
                ORDER by name ASC`, [guildId, counterUtils.counterWriteUpType], r => r.counter_name)
        } else {
            countersList = await database.db.any(
                `SELECT name, short_desc 
                FROM guild_writeup 
                WHERE guild_id = $1 AND type = $2 AND (name ILIKE $3 OR short_desc ILIKE $3 OR text ILIKE $3)  
                ORDER by name ASC`, [guildId, counterUtils.counterWriteUpType, '%' + filter + '%'], r => r.counter_name)
        }
        let listString;
        if (!countersList || countersList.length == 0)
            listString = "There are no counters defined for your guild."
        else
        {
            listString = "- " + countersList.sort(c => c.counter_name).map(c => "**" + c.name + "**: " + c.short_desc).join("\r\n- ")
        }

        return "Available Counters:\n" + listString;
    }
}