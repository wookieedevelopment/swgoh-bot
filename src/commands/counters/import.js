const { EmbedBuilder, AttachmentBuilder } = require("discord.js");
const database = require("../../database");
const playerUtils = require("../../utils/player");
const discordUtils = require("../../utils/discord");
const counterUtils = require("./counterUtils");
const got = require("got");
const parse = require("csv-parse/lib/sync");
const { SlashCommandSubcommandBuilder } = require('@discordjs/builders');

const newlineRegex = /[\r\n]/g;

module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("import")
        .setDescription("Import guild counters from CSV")
        .addAttachmentOption(o => 
            o.setName("file")
            .setDescription("CSV file to upload")
            .setRequired(true)),
    permissionLevel: playerUtils.UserRoles.GuildAdmin,
    interact: async function(interaction)
    {
        let { botUser, guildId, error } = await playerUtils.authorize(interaction, this.permissionLevel);     
        if (error)
        {
            await interaction.editReply({ content: error })
            return;
        }

        let file = interaction.options.getAttachment("file");

        let response = await this.importCounters(guildId, file);

        await interaction.editReply(response);
    },
    importCounters: async function(guildId, file)
    {
        let buffer = await this.getAttachmentBuffer(file);
        var bufferString = buffer.rawBody.toString("utf-8");
        const counters = parse(bufferString, { columns: true });

        let errors = new Array();
        let dataToSave = new Array();

        for (let cRow = 0; cRow < counters.length; cRow++)
        {
            let c = counters[cRow];
            if (!c.name || c.name.length == 0) { errors.push(`Invalid counter (Row ${cRow+2}): no name`); continue; }
            if (!c.short_desc || c.short_desc.length == 0) { errors.push(`Invalid counter (Row ${cRow+2}): no short_desc`); continue; }
            if (!c.text || c.text.length == 0) { errors.push(`Invalid counter (Row ${cRow+2}): no text`); continue; }

            if (c.name.match(newlineRegex)?.length > 0) { errors.push(`Invalid counter (Row ${cRow+2}): name cannot contain multiple lines of text`); continue; } 
            if (c.short_desc.match(newlineRegex)?.length > 0) { errors.push(`Invalid counter (Row ${cRow+2}): short_desc cannot contain multiple lines of text`); continue; } 

            if (c.name.length > 50) { errors.push(`Invalid counter (Row ${cRow+2}): name must be <= 50 characters`); continue; }
            if (c.short_desc.length > 200) { errors.push(`Invalid counter (Row ${cRow+2}): short_desc must be <= 200 characters`); continue; }
            if (c.text.length > 2000) { errors.push(`Invalid counter (Row ${cRow+2}): text must be <= 2000 characters`); continue; }

            let data = {
                name: c.name,
                short_desc: c.short_desc,
                text: c.text,
                guild_id: guildId,
                type: counterUtils.counterWriteUpType
            }

            dataToSave.push(data);
        }

        if (dataToSave.length > 0)
        {
            let guildWriteupScript = database.pgp.helpers.insert(dataToSave, database.columnSets.guildWriteupCS) + 
                ' ON CONFLICT(guild_id,name,type) DO UPDATE SET ' +
                database.columnSets.guildWriteupCS.assignColumns({from: 'EXCLUDED', skip: ['guild_id', 'name', 'type']});

            await database.db.none(guildWriteupScript);

        }

        let successEmbed = new EmbedBuilder()
            .setTitle(`${dataToSave.length} Counter${dataToSave.length == 1 ? "" : "s"} Saved`)
            .setColor(0x108010)
            .setDescription((dataToSave.length > 0) ? ("- " + dataToSave.map(d => d.name).join(`${discordUtils.LTR}\n- `)) : discordUtils.symbols.fail);

        let errorsEmbed = new EmbedBuilder()
            .setTitle(`${errors.length} Error${errors.length == 1 ? "" : "s"}`)
            .setColor(0x801010)
            .setDescription((errors.length > 0) ? errors.join("\n") : discordUtils.symbols.ok);


        let response = { embeds: [successEmbed, errorsEmbed] };

        return response;
    },
    getAttachmentBuffer: async function(attachment)
    {
        if (!attachment)   
        {
            throw new Error("You must upload a CSV file with three columns: name, short_desc, text");
        }

        if (attachment.size >= 1024*1024*8) // 8MB limit, maybe?
        {
            throw new Error("This file is too big.  Please limit to less than 8MB.  If you have a lot of counters data, you may need to split the file into more than one.  Just upload each separately.");
        }

        var ssUrl = attachment.url;
        var buffer = await got(ssUrl);
        return buffer;
    },
}