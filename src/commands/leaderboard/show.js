const { SlashCommandSubcommandBuilder, AttachmentBuilder } = require("discord.js");
const discordUtils = require("../../utils/discord");
const drawingUtils = require("../../utils/drawing");
const database = require("../../database");
const lbCommon = require("./common");
const _ = require("lodash");
const { CanvasTable, CTConfig } = require("canvas-table");

const MAX_GP_MINIMUM = 500000;
const MAX_GP_VALUE = 5000000000;

module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("show")
        .setDescription("Show a leaderboard")
        .addStringOption(o => 
            o.setName("board")
            .setDescription("Leaderboard to show")
            .setRequired(true)
            .addChoices(...lbCommon.LEADERBOARD_CHOICES_FOR_DISCORD))
        .addStringOption(o => 
            o.setName("guild")
            .setDescription("Guild for which to show guild leaderboard details.")
            .setRequired(false)
            .setAutocomplete(true))
        .addStringOption(o => 
            o.setName("allycode")
            .setDescription("Allycode for which to show player leaderboard details.")
            .setRequired(false))
        .addIntegerOption(o =>
            o.setName("min-gp")
            .setDescription("Minimum GP for guilds to be included in the list.")
            .setRequired(false)
            .setMinValue(0)
            .setMaxValue(MAX_GP_VALUE))
        .addIntegerOption(o =>
            o.setName("max-gp")
            .setDescription("Maximum GP for guilds to be included in the list.")
            .setRequired(false)
            .setMinValue(MAX_GP_MINIMUM)
            .setMaxValue(MAX_GP_VALUE))
        ,
    interact: async function(interaction)
    {
        const board = interaction.options.getString("board");
        const comlinkGuildId = interaction.options.getString("guild");
        const allycode = interaction.options.getString("allycode");
        const minGP = interaction.options.getInteger("min-gp");
        const maxGP = interaction.options.getInteger("max-gp");

        if (!lbCommon.isLeaderboardEnabled(board))
        {
            await interaction.editReply({ content: "This leaderboard is not yet enabled." });
            return;
        }

        const response = await createShowLeaderboardResponse(board, { comlinkGuildId: comlinkGuildId, allycode: allycode, minGP: minGP, maxGP: maxGP });
        await interaction.editReply(response);
    },
    autocomplete: async function(interaction)
    {
		const focusedValue = interaction.options.getFocused();
        const choices = await discordUtils.getGuildAutocompleteOptions(focusedValue);

        await interaction.respond(choices);
    }
}

async function createShowLeaderboardResponse(board, { comlinkGuildId, allycode, minGP, maxGP })
{
    if (minGP != null && (!_.isNumber(minGP) || minGP < 0)) return { embeds: [discordUtils.createErrorEmbed("Error", "min-gp needs to be a number greater than zero.")] };
    if (maxGP != null && (!_.isNumber(maxGP) || maxGP < MAX_GP_MINIMUM)) return { embeds: [discordUtils.createErrorEmbed("Error", `max-gp needs to be a number greater than ${MAX_GP_MINIMUM}.`)] };

    const { canvas, context } = drawingUtils.createBlankCanvas(645, 1837, "#000");
    let leaderboardData;

    if (comlinkGuildId || allycode)
    {
        if (lbCommon.isPlayerLeaderboard(board) && comlinkGuildId)
        {
            // show players filtered by guild
            leaderboardData = await database.db.any(
                `SELECT key, data, rank
                FROM leaderboard_data
                LEFT JOIN guild_players ON guild_players.allycode = leaderboard_data.key
                LEFT JOIN guild ON guild_players.guild_id = guild.guild_id
                WHERE leaderboard_id = $1
                AND guild.comlink_guild_id = $2
                ORDER BY rank ASC
                LIMIT 50`, [board, comlinkGuildId]);
        }
        else
        {
            leaderboardData = await database.db.any(
                `SELECT key, data, rank
                FROM leaderboard_data 
                WHERE leaderboard_id = $1
                AND (
                    (rank >= (select rank-25 from leaderboard_data where leaderboard_id = $1 and key = $2)
                    ${minGP != null ? `and (data->>'gp')::integer >= ${minGP}` : ""}
                    ${maxGP != null ? `and (data->>'gp')::integer <= ${maxGP}` : ""})
                    OR
                    (key = $2)
                )
                ORDER BY rank ASC
                LIMIT 50`, [board, comlinkGuildId || allycode]);    
        }
    }
    else
    {
        leaderboardData = await database.db.any(
            `SELECT key, data, rank 
            FROM leaderboard_data 
            WHERE leaderboard_id = $1 
            ${minGP != null ? `and (data->>'gp')::integer >= ${minGP}` : ""}
            ${maxGP != null ? `and (data->>'gp')::integer <= ${maxGP}` : ""}
            ORDER BY rank ASC
            LIMIT 50`, [board]);
    }

    await drawLeaderboardTable(context, board, leaderboardData);
    
    const buffer = canvas.toBuffer('image/png');
    
    let fileName = `lb-${board}-${Date.now()}.png`;
    const file = new AttachmentBuilder(buffer, { name: fileName });
    return { files: [file] };
}


const BORDER_COLOR = "#000";
const HEADER_BG_COLOR = "#003300";
const HEADER_FONT_COLOR = "#eeeeee";
const DEFAULT_FONT_COLOR = "#000";
const ROW_COLOR = "#bbbbbb";
const ALTERNATING_ROW_COLOR = "#eeeeee";

const LEADERBOARD_TABLE_OPTIONS = {
    borders: {
        column: false, //{ width: 1, color: "#aaa" },
        header: { width: 2, color: BORDER_COLOR }, // set to false to hide the header
        row: { width: 1, color: BORDER_COLOR },
        table: { width: 2, color: BORDER_COLOR }
    },
    background: "#000",
    fit: false,
    cell: {
        fontSize: 10,
        color: DEFAULT_FONT_COLOR,
        padding: { left: 2, right: 2, bottom: 5, top: 2 },
        lineHeight: 1.1
    },
    header: {
        background: HEADER_BG_COLOR,
        fontSize: 10,
        color: HEADER_FONT_COLOR,
        padding: { left: 2, right: 2, bottom: 5, top: 2 },
        lineHeight: 1.1
    },
    fader: {
        right: false, top: false, bottom: true, left: false, size: 20
    },
    padding: {
        right: 0, top: 0, bottom: 0, left: 0
    }
}




async function drawLeaderboardTable(context, board, leaderboardData)
{
    let data = [];
    for (let gd of leaderboardData)
    {
        let val = gd.data.value;
        if (_.isNumber(val)) {
            switch (board)
            {
                case lbCommon.LEADERBOARDS.GUILD_TOTAL_GP:
                    if (val >= 1000000) val = `${_.round(val/1000000, 2)}M`;
                    else val = `${_.round(val/1000, 2)}K`;
                    break;
                case lbCommon.LEADERBOARDS.GUILD_RAID_SCORES:
                    if (val >= 1000000) val = `${_.round(val/1000000, 2)}M`;
                    else val = `${_.round(val/1000, 2)}K`;
                    break;
                case lbCommon.LEADERBOARDS.PLAYER_RAID_SCORES:
                    if (val >= 1000000) val = `${_.round(val/1000000, 2)}M`;
                    else val = `${_.round(val/1000, 2)}K`;
                    break;
                case lbCommon.LEADERBOARDS.GUILD_TURNOVER:
                    // val is in seconds
                    let exactYears = val / (365*24*60*60);
                    let years = Math.floor(exactYears);
                    let days = Math.round((exactYears - years) * 365);

                    val = `${days}d`;
                    if (years > 0) val = `${years}y ${val}`;
                    break;
                default:
                    val = val.toLocaleString();
                    break;
            }
        }

        let rank = gd.rank.toString();
        let name = gd.data.name;

        if (lbCommon.isPlayerLeaderboard(board))
        {
            col3Value = discordUtils.addDashesToAllycode(gd.key);
        }
        else
        {
            let guildGPString;
            
            if (gd.data.gp >= 1000000) { guildGPString = `${_.round(gd.data.gp / 1000000)}M`}
            else { guildGPString = `${_.round(gd.data.gp / 1000)}K`}

            col3Value = guildGPString;
        }

        let rowColor = (data.length %2 === 0) ? ROW_COLOR : ALTERNATING_ROW_COLOR;
        let row = [
            { value: rank, background: rowColor},
            { value: name, background: rowColor},
            { value: col3Value, background: rowColor },
            { value: val, background: rowColor},
        ];


        data.push(row);
    };
    
    let valueColumnTitle;
    switch (board)
    {
        case lbCommon.LEADERBOARDS.GUILD_AVG_25PLUS_SPEED_MODS: valueColumnTitle = "Avg 25+"; break;
        case lbCommon.LEADERBOARDS.GUILD_AVG_MOD_QUALITY: valueColumnTitle = "Avg MQ"; break;
        case lbCommon.LEADERBOARDS.GUILD_AVG_SKILL_RATING: valueColumnTitle = "Avg Skill"; break;
        case lbCommon.LEADERBOARDS.GUILD_TB_SPECIAL: valueColumnTitle = "Total GET"; break;
        case lbCommon.LEADERBOARDS.GUILD_AVG_FLEET_RANK: valueColumnTitle = "Avg Fleet"; break;
        case lbCommon.LEADERBOARDS.GUILD_AVG_OMICRONS: valueColumnTitle = "Avg Omis"; break;
        case lbCommon.LEADERBOARDS.GUILD_AVG_LEVEL9_DATACRONS: valueColumnTitle = "Avg DC9"; break;
        case lbCommon.LEADERBOARDS.GUILD_TOTAL_DATACRON_REROLLS: valueColumnTitle = "Rerolls"; break;
        case lbCommon.LEADERBOARDS.GUILD_TOTAL_GP: valueColumnTitle = "Avg GP"; break;
        case lbCommon.LEADERBOARDS.GUILD_TURNOVER: valueColumnTitle = "Turnover"; break;
        case lbCommon.LEADERBOARDS.GUILD_RAID_SCORES: valueColumnTitle = "Raid"; break;
        case lbCommon.LEADERBOARDS.PLAYER_25PLUS_SPEED_MODS: valueColumnTitle = "# 25+"; break;
        case lbCommon.LEADERBOARDS.PLAYER_6PLUS_OFFENSE_MODS: valueColumnTitle = "# 6%+"; break;
        case lbCommon.LEADERBOARDS.PLAYER_RAID_SCORES: valueColumnTitle = "Raid"; break;
    }

    
    let LEADERBOARD_TABLE_COLUMNS;
    if (lbCommon.isPlayerLeaderboard(board))
    {
        LEADERBOARD_TABLE_COLUMNS = [
            { title: "Rank", options: { maxWidth: 30, minWidth: 30 } },
            { title: "Player Name", options: { maxWidth: 150, minWidth: 150 } },
            { title: "Ally Code", options: { maxWidth: 75, minWidth: 75 } },
            { title: valueColumnTitle, options: { maxWidth: 50, minWidth: 50 } }
        ];
    }
    else
    {
        LEADERBOARD_TABLE_COLUMNS = [
            { title: "Rank", options: { maxWidth: 30, minWidth: 30 } },
            { title: "Guild Name", options: { maxWidth: 175, minWidth: 175 } },
            { title: "GP", options: { maxWidth: 50, minWidth: 50 } },
            { title: valueColumnTitle, options: { maxWidth: 50, minWidth: 50 } }
        ];
    }


    const config = { 
        data: data,
        columns: LEADERBOARD_TABLE_COLUMNS,
        options: LEADERBOARD_TABLE_OPTIONS,
        event: null
    };

    const ct = new CanvasTable(context.canvas, config);
    await ct.generateTable();
    let buffer = await ct.renderToBuffer();

    await drawingUtils.loadAndDrawImage(context, buffer, 0, 0, context.canvas.width, context.canvas.height);

}