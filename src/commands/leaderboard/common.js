const database = require("../../database");

const LEADERBOARDS = {
    GUILD_TB_SPECIAL: "GUILD_TB_SPECIAL",
    GUILD_AVG_SKILL_RATING: "GUILD_AVG_SKILL_RATING",
    GUILD_AVG_25PLUS_SPEED_MODS: "GUILD_AVG_25PLUS_SPEED_MODS",
    GUILD_AVG_MOD_QUALITY: "GUILD_AVG_MOD_QUALITY",
    GUILD_AVG_FLEET_RANK: "GUILD_AVG_FLEET_RANK",
    GUILD_AVG_OMICRONS: "GUILD_AVG_OMICRONS",
    GUILD_AVG_LEVEL9_DATACRONS: "GUILD_AVG_LEVEL9_DATACRONS",
    GUILD_TOTAL_DATACRON_REROLLS: "GUILD_TOTAL_DATACRON_REROLLS",
    GUILD_TOTAL_GP: "GUILD_TOTAL_GP",
    GUILD_TURNOVER: "GUILD_TURNOVER",
    GUILD_RAID_SCORES: "GUILD_RAID_SCORES",
    PLAYER_25PLUS_SPEED_MODS: "PLAYER_25PLUS_SPEED_MODS",
    PLAYER_6PLUS_OFFENSE_MODS: "PLAYER_6PLUS_OFFENSE_MODS",
    PLAYER_RAID_SCORES: "PLAYER_RAID_SCORES"
}

const LEADERBOARD_CHOICES_FOR_DISCORD = [
    { name: "Guild Potential TB Special Income (Sum of all GET)", value: "GUILD_TB_SPECIAL" },
    { name: "Guild Turnover (Avg Length of Membership)", value: "GUILD_TURNOVER" },
    { name: "Guild Average 25+ Speed Mods per Member", value: "GUILD_AVG_25PLUS_SPEED_MODS" },
    { name: "Guild Average Mod Quality (Wookiee Style)", value: "GUILD_AVG_MOD_QUALITY" },
    { name: "Guild Average Skill Rating", value: "GUILD_AVG_SKILL_RATING" },
    { name: "Guild Average Fleet Rank", value: "GUILD_AVG_FLEET_RANK" },
    { name: "Guild Average Omicrons per Player", value: "GUILD_AVG_OMICRONS" },
    { name: "Guild Average Level 9 Datacrons per Player", value: "GUILD_AVG_LEVEL9_DATACRONS" },
    { name: "Guild Total Datacron Rerolls", value: "GUILD_TOTAL_DATACRON_REROLLS" },
    { name: "Guild Total Galactic Power", value: "GUILD_TOTAL_GP" },
    { name: "Guild Raid Scores (Featured Raid Only)", value: "GUILD_RAID_SCORES" },
    { name: "Player 25+ Speed Mods", value: "PLAYER_25PLUS_SPEED_MODS" },
    { name: "Player 6%+ Offense Secondary Mods", value: "PLAYER_6PLUS_OFFENSE_MODS" },
    { name: "Player Raid Scores (Featured Raid Only)", value: "PLAYER_RAID_SCORES" },
];

module.exports = {
    LEADERBOARDS: LEADERBOARDS,
    LEADERBOARD_CHOICES_FOR_DISCORD: LEADERBOARD_CHOICES_FOR_DISCORD,
    isLeaderboardEnabled: isLeaderboardEnabled,
    getLeaderboardDescription: getLeaderboardDescription,
    isPlayerLeaderboard: isPlayerLeaderboard
}

async function getLeaderboardDescription(lbId)
{
    const result = await database.db.oneOrNone("SELECT description FROM leaderboard WHERE leaderboard_id = $1", [lbId]);
    return result?.description;
}

async function isLeaderboardEnabled(lbId)
{
    const result = await database.db.oneOrNone("SELECT enabled FROM leaderboard WHERE leaderboard_id = $1", [lbId]);
    if (!result || !result.enabled) return false;
    return true;
}

function isPlayerLeaderboard(lbId)
{
    return lbId.startsWith("PLAYER");
}