const { SlashCommandSubcommandBuilder, EmbedBuilder, IntegrationApplication } = require("discord.js");
const discordUtils = require("../../utils/discord");
const playerUtils = require("../../utils/player");
const database = require("../../database");
const lbCommon = require("./common");
const _ = require("lodash");
const constants = require("../../utils/constants");

module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("player")
        .setDescription("Show all leaderboard rankings for a player (default: you)")
        .addStringOption(o => 
            o.setName("allycode")
            .setDescription("Ally Code for which to show leaderboard details.")
            .setRequired(false)
            .setAutocomplete(false)
        )
        .addIntegerOption(o =>
            o.setName("alt")
            .setDescription("Alt account (/user me)")
            .setRequired(false)
            .setAutocomplete(true)),
    interact: async function(interaction)
    {
        let allycode = interaction.options.getString("allycode");
        let altNum = interaction.options.getInteger("alt");

        if (!allycode)
        {
            let { botUser, guildId, error } = await playerUtils.authorize(interaction);

            if (error) {
                logger.error(formatError(error));
                await interaction.editReply({ embeds: [discordUtils.createErrorEmbed("Error", error)] });
                return;
            }

            let alt = await playerUtils.chooseBestAlt(botUser, guildId, altNum, false);

            if (!alt)
            {
                await interaction.editReply({ content: "You do not have an SWGOH account registered in WookieeBot. Please run `/register` to register." });
            }

            allycode = alt.allycode;
        }

        allycode = playerUtils.cleanAndVerifyStringAsAllyCode(allycode);

        const lbData = await database.db.any(
            `SELECT leaderboard_id, data, rank 
            FROM leaderboard_data
            LEFT JOIN leaderboard USING (leaderboard_id) 
            WHERE key = $1
            ORDER BY display_order ASC`, [allycode]);

        const response = await createPlayerLeaderboardResponse(allycode, lbData);

        await interaction.editReply(response);
    },
    autocomplete: async function(interaction) {
        const focusedValue = interaction.options.getFocused();
        await interaction.respond((await playerUtils.getAltAutocompleteOptions(focusedValue, interaction.user.id)));
    },
}

async function createPlayerLeaderboardResponse(allycode, lbData)
{
    if ((lbData?.length ?? 0) == 0) {
        return { content: `No leaderboard data yet.` };
    }

    const playerName = lbData[0].data.name;
    const allycodeString = discordUtils.addDashesToAllycode(allycode);

    const embed = new EmbedBuilder()
        .setTitle(`${playerName} (${allycodeString})`);

    const response = { embeds: [embed] };

    let leaderboardRankings = [];
    for (let l of lbData)
    {
        const lbText = await lbCommon.getLeaderboardDescription(l.leaderboard_id);
        const rank = l.rank;

        let value = l.data.value;

        if (_.isNumber(value)) value = value.toLocaleString();

        leaderboardRankings.push(`**${lbText}:** ${value}\n-# > Rank ${rank}`);
    }

    let joinedText = leaderboardRankings.join("\n\n");

    const description = 
`Player Name: ${playerName}
Ally Code: ${allycodeString}
Link: https://swgoh.gg/p/${allycode}/
### __Leaderboard Rankings__
${joinedText}
`;
    embed.setDescription(description);
    
    return response;
}