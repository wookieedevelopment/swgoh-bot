const { SlashCommandSubcommandBuilder, EmbedBuilder } = require("discord.js");
const discordUtils = require("../../utils/discord");
const guildUtils = require("../../utils/guild");
const database = require("../../database");
const lbCommon = require("./common");
const _ = require("lodash");
const constants = require("../../utils/constants");

module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("guild")
        .setDescription("Show all leaderboard rankings for a guild")
        .addStringOption(o => 
            o.setName("guild")
            .setDescription("Guild for which to show leaderboard details.")
            .setRequired(true)
            .setAutocomplete(true)
        ),
    interact: async function(interaction)
    {
        const comlinkGuildId = interaction.options.getString("guild");

        const guildData = await guildUtils.getGuildDataFromAPI({ comlinkGuildId: comlinkGuildId }, { useCache: true });
        const lbData = await database.db.any(
            `SELECT leaderboard_id, data, rank 
            FROM leaderboard_data
            LEFT JOIN leaderboard USING (leaderboard_id) 
            WHERE key = $1
            ORDER BY display_order ASC`, [comlinkGuildId]);

        const response = await createGuildLeaderboardResponse(guildData, lbData);

        await interaction.editReply(response);
    },
    autocomplete: async function(interaction)
    {
		const focusedValue = interaction.options.getFocused();
        const choices = await discordUtils.getGuildAutocompleteOptions(focusedValue);

        await interaction.respond(choices);
    }
}

async function createGuildLeaderboardResponse(guildData, lbData)
{
    const guildName = guildUtils.getGuildName(guildData);
    const guildGP = _.parseInt(guildData.profile.guildGalacticPower).toLocaleString();
    const guildMemberCount = guildData.profile.memberCount;
    const guildLeader = guildData.member.find(m => m.memberLevel === constants.GUILD_MEMBER_TYPES.LEADER);

    if (lbData.length == 0) {
        return { content: `No leaderboard data yet for **${guildName}**` };
    }

    const embed = new EmbedBuilder()
        .setTitle(`${guildName}`);

    const response = { embeds: [embed] };

    let leaderboardRankings = [];
    for (let l of lbData)
    {
        const lbText = await lbCommon.getLeaderboardDescription(l.leaderboard_id);
        const rank = l.rank;

        let value;

        // special handling for the guild total GP leaderboard
        if (l.leaderboard_id === lbCommon.LEADERBOARDS.GUILD_TOTAL_GP) value = guildGP;
        else if (l.leaderboard_id === lbCommon.LEADERBOARDS.GUILD_TURNOVER)
        {
            let exactYears = l.data.value / (365*24*60*60);
            let years = Math.floor(exactYears);
            let days = Math.round((exactYears - years) * 365);

            value = `${days} day${days === 1 ? "" : "s"}`;
            if (years > 0) value = `${years} year${years === 1 ? "" : "s"}, ${value}`;
        }
        else value = l.data.value;

        if (_.isNumber(value)) value = value.toLocaleString();

        leaderboardRankings.push(`**${lbText}:** ${value}\n-# > Rank ${rank}`);
    }

    let joinedText = leaderboardRankings.join("\n\n");

    const description = 
`Members: ${guildMemberCount}
GP: ${guildGP}
Link: https://swgoh.gg/g/${guildData.profile.id}/
Leader: [${guildLeader.name} (${discordUtils.addDashesToAllycode(guildLeader.allyCode)})](https://swgoh.gg/p/${guildLeader.allyCode})
### __Leaderboard Rankings__
${joinedText}
`;
    embed.setDescription(description);
    
    return response;
}