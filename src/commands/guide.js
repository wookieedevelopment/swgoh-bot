const guidesData = require("../data/guides_data.json");
const { EmbedBuilder } = require("discord.js");
const { SlashCommandBuilder } = require('@discordjs/builders');

let choices = Object.keys(guidesData).map(o => { return { name: o, value: o }; });

module.exports = {
    data: new SlashCommandBuilder()
        .setName("guide")
        .setDescription("Selected Guides for SWGOH")
        .addStringOption(o => 
            o.setName("guide")
            .setDescription("The name of the guide.")
            .setRequired(true)
            .addChoices(...choices)
            ),
    name: "guide",
    description: "Selected Guides for SWGOH",
    interact: async function(interaction)
    {
        let guide = interaction.options.getString("guide");

        let content = this.getContent(guide);
        await interaction.editReply(content);
    },
    list: function() {
        
        let list = Object.keys(guidesData);

        let embed = new EmbedBuilder()
            .setTitle("Available SWGOH Guides")
            .setColor(0xD2691E)
            .setDescription(list.sort().toString().split(',').join("\r\n"));

        return { embeds: [embed] };
    },
    execute: async function(client, message, args) {
        let guide = null;
        if (args && args[0]) guide = args[0].trim();

        let content = this.getContent(guide);

        if (content) 
        {
            await message.channel.send(content);
            return;
        }
    },
    getContent: function(guide)
    {
        if (!guide || !guidesData[guide]) return this.list();

        switch (guidesData[guide].type)
        {
            case "file":
                return { files: [guidesData[guide].data] };
            case "text":
                return guidesData[guide].data;
            default:
                return this.list();
        }
    }
}