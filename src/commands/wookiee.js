const data = require("../data/wookiee_data.json");
const { SlashCommandBuilder } = require('@discordjs/builders');

module.exports = {
    data: new SlashCommandBuilder()
        .setName("talk")
        .setDescription("Wookiee speak"),
    interact: async function(interaction)
    {
        await interaction.channel.send(this.getRandomMessage());
        await interaction.deleteReply();
    },
    talk: async function(message) {
        await message.channel.send(this.getRandomMessage());
    },
    getRandomMessage: function()
    {
        return data[Math.floor(Math.random()*data.length)];
    }
}