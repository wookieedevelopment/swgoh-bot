const guildRefresh = require("./guild/refresh")
const guildLinkToDiscord = require("./guild/linkToDiscord")
const guildStatus = require("./guild/status")
const guildCache = require("./guild/cache")
const guildTBData = require ("./guild/tbData")
const guildHelp = require ("./guild/help")
const guildAdmin = require("./guild/admin");
const guildReady = require("./guild/ready");
const guildUsers = require("./guild/users");
const { SlashCommandBuilder } = require('@discordjs/builders');
const tbData = require("./guild/tbData");
const guildSearch = require("./guild/search.js");
const guildRec = require("./guild/rec");
const guildMeta = require("./guild/meta");
const guildData = require("./guild/data");
const guildCriteria = require("./guild/criteria");
const guildShowLinks = require("./guild/show-links.js");

module.exports = {
    data: new SlashCommandBuilder()
        .setName("guild")
        .setDescription("Guild Management")
        .addSubcommandGroup(guildUsers.data)
        .addSubcommandGroup(guildRefresh.data)
        .addSubcommandGroup(guildReady.data)
        .addSubcommand(guildLinkToDiscord.data)
        .addSubcommand(guildShowLinks.data)
        .addSubcommand(guildStatus.data)
        .addSubcommand(guildHelp.data)
        .addSubcommandGroup(guildCache.data)
        .addSubcommandGroup(guildTBData.data)
        .addSubcommand(guildAdmin.data)
        .addSubcommand(guildSearch.data)
        .addSubcommandGroup(guildRec.data)
        .addSubcommandGroup(guildMeta.data)
        .addSubcommand(guildData.data)
        // .addSubcommandGroup(guildCriteria.data)
        ,
    interact: async function(interaction)
    {
        let group, subcommand;
        try { group = interaction.options.getSubcommandGroup(); } catch {}
        if (group)
        {
            switch (group)
            {
                case tbData.data.name:
                    await tbData.interact(interaction);
                    break;
                case guildUsers.data.name:
                    await guildUsers.interact(interaction);
                    break;
                case guildRefresh.data.name:
                    await guildRefresh.interact(interaction);
                    break;
                case guildCache.data.name:
                    await guildCache.interact(interaction);
                    break;
                case guildReady.data.name:
                    await guildReady.interact(interaction);
                    break;
                case guildRec.data.name:
                    await guildRec.interact(interaction);
                    break;
                case guildMeta.data.name:
                    await guildMeta.interact(interaction);
                    break;
                case guildCriteria.data.name:
                    await guildCriteria.interact(interaction);
                    break;
            }
            return;
        }

        try { subcommand = interaction.options.getSubcommand(); } catch {}

        switch (subcommand)
        {
            case guildLinkToDiscord.data.name:
                await guildLinkToDiscord.interact(interaction);
                break;
            case guildShowLinks.data.name:
                await guildShowLinks.interact(interaction);
                break;
            case guildStatus.data.name:
                await guildStatus.interact(interaction);
                break;
            case guildHelp.data.name:
                await guildHelp.interact(interaction);
                break;
            case guildAdmin.data.name:
                await guildAdmin.interact(interaction);
                break;
            case guildSearch.data.name:
                await guildSearch.interact(interaction);
                break;
            case guildData.data.name:
                await guildData.interact(interaction);
                break;
            default:
                await interaction.editReply("That is not a valid command. Huac ooac. ('Uh oh.').")
                break;         
        }
    },
    isEphemeral: function(subcommandName)
    {
        if (subcommandName == guildShowLinks.data.name) return true;
        return false;
    },
    handleButton: async function(interaction)
    {  
        if (interaction.customId.startsWith("guild:search:"))
            await guildSearch.handleButton(interaction)
    },
    onSelect: async function(interaction)
    {
        if (interaction.customId.startsWith("guild:search"))
            await guildSearch.onSelect(interaction);
    },
    autocomplete: async function(interaction)
    {
        let group, subcommand;
        try { group = interaction.options.getSubcommandGroup(); } catch {}

        if (group)
        {
            switch (group) {
                case guildRec.data.name:
                    await guildRec.autocomplete(interaction);
                    break;
                case guildReady.data.name:
                    await guildReady.autocomplete(interaction);
                    break;
                case guildMeta.data.name:
                    await guildMeta.autocomplete(interaction);
                    break;
            }
            return;
        }
        try { subcommand = interaction.options.getSubcommand(); } catch {}
        if (subcommand)
        {
            switch (subcommand) {
                case guildLinkToDiscord.data.name:
                    await guildLinkToDiscord.autocomplete(interaction);
                    break;
                case guildShowLinks.data.name:
                    await guildShowLinks.autocomplete(interaction);
                    break;
                case guildData.data.name:
                    await guildData.autocomplete(interaction);
                    break;
                
            }
            return;
        }
    }
}