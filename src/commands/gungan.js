const metadataCache = require("../utils/metadataCache");
const fs = require("fs");
const { SlashCommandBuilder } = require('@discordjs/builders');

cacheGunganSpeak();
fs.watchFile("./src/data/gungan_data.json",
  (curr, prev) => { cacheGunganSpeak(); });

const switchExtendedCharacters = (text) => { return text.replace(/[’]/g, "'"); }

function cacheGunganSpeak() {
    fs.readFile("./src/data/gungan_data.json", { encoding: "utf-8" }, (err, data) => {
        let gunganData = JSON.parse(data);

        gunganData.words.sort((a, b) => (b.key.split(" ").length - a.key.split(" ").length) || (b.length - a.length));

        metadataCache.set(metadataCache.CACHE_KEYS.gunganData, gunganData, 1000 * 60 * 60 * 24 * 30);
    });
}


module.exports = {
    data: new SlashCommandBuilder()
        .setName("gungan")
        .setDescription("Speak Gungan")
        .addStringOption(o =>
            o.setName("text")
            .setDescription("Text to translate")
            .setRequired(false)),
    interact: async function(interaction)
    {
        let text = interaction.options.getString("text");

        let responseText;
        if (!text)
        {
            responseText = this.getRandomPhrase();
        }
        else
        {
            responseText = this.translateToGungan(text);
        }
        await interaction.editReply({ content: responseText });
    },
    getRandomPhrase: function()
    {
        const GUNGAN_DATA = metadataCache.get(metadataCache.CACHE_KEYS.gunganData);
        return GUNGAN_DATA.phrases[Math.floor(Math.random()*GUNGAN_DATA.phrases.length)];
    },
    translateToGungan: function(text)
    {
        const GUNGAN_DATA = metadataCache.get(metadataCache.CACHE_KEYS.gunganData);
        const GUNGAN_WORDS = GUNGAN_DATA.words;
        let newText = switchExtendedCharacters(text);

        let separatorRegex = /[, :;./\-?!"@\n]+/
        for (let p of GUNGAN_WORDS)
        {
            let ix = newText.toLowerCase().indexOf(p.key.toLowerCase());

            while (ix !== -1)
            {
                if (
                    (ix == 0 || separatorRegex.test(newText[ix-1])) 
                    && ((ix + p.key.length) == (newText.length) || separatorRegex.test(newText[ix+p.key.length])))
                {
                    // do the replacement
                    let value = p.value;
                    if (isUpperCase(newText[ix]))
                    {
                        value = value[0].toUpperCase() + value.slice(1);
                    }

                    newText = newText.slice(0, ix) + value + newText.slice(ix + p.key.length);

                }

                ix = newText.toLowerCase().indexOf(p.key.toLowerCase(), ix + p.value.length);
            }
        }

        return newText;
    }
}

const isUpperCase = (string) => /^[A-Z]*$/.test(string)
