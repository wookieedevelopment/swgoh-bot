const got = require("got");
const recCommon = require("./guild/rec/common");
const swapiUtils = require ("../utils/swapi");
const playerUtils = require("../utils/player");
const discordUtils = require("../utils/discord");
const datacronUtils = require("../utils/datacron");
const { SlashCommandBuilder } = require('@discordjs/builders');
const constants = require("../utils/constants");
const database = require("../database");
const { EmbedBuilder, AttachmentBuilder } = require("discord.js");

const GEAR_RELIC_LEVELS = [
    { name: "G1", value: "G1" },
    { name: "G11", value: "G11" },
    { name: "G12", value: "G12" },
    { name: "G13 (R0)", value: "R0" },
    { name: "R3", value: "R3" },
    { name: "R5", value: "R5" },
    { name: "R7", value: "R7" },
    { name: "R8", value: "R8" }
];

module.exports = {
    data: new SlashCommandBuilder()
        .setName("rec")
        .setDescription("Get recommendations")
        .addStringOption(o => 
            o.setName("type")
            .setDescription("Type of recommendation")
            .setRequired(true)
            .addChoices(...recCommon.REC_TYPE_CHOICES))
        .addBooleanOption(o => 
            o.setName("show-locked")
            .setDescription("Show recommendations for locked units (default: false)")
            .setRequired(false))
        .addStringOption(o => 
            o.setName("omi-type")
            .setDescription("Show only a specific type of omicron")
            .setRequired(false)
            .addChoices(...recCommon.OMICRON_MODES_CHOICES))
        .addStringOption(o =>
            o.setName("template")
            .setDescription("Use a recommendations template (instead of your guild's recommendations)")
            .setRequired(false)
            .setAutocomplete(true))
        .addStringOption(o =>
            o.setName("min-gear")
            .setDescription("Minimum gear/relic required for a unit's omi to be considered (default: no minimum)")
            .setRequired(false)
            .addChoices(...GEAR_RELIC_LEVELS))
        .addIntegerOption(o => 
            o.setName("set")
            .setRequired(false)
            .setAutocomplete(true)
            .setDescription("Set of the datacron"))
        .addStringOption(o => 
            o.setName("allycode")
            .setDescription("Ally code of player to get recommendations for")
            .setRequired(false))
        .addIntegerOption(o =>
            o.setName("alt")
            .setDescription("Alt account (/user me)")
            .setRequired(false)
            .setAutocomplete(true)),
    interact: async function(interaction) {
        const type = interaction.options.getString("type");
        let allycode = interaction.options.getString("allycode");
        let showLocked = interaction.options.getBoolean("show-locked") ?? false;
        let template = interaction.options.getString("template");
        let minGear = interaction.options.getString("min-gear") ?? GEAR_RELIC_LEVELS[0].value;
        let alt = interaction.options.getInteger("alt");
        let set = interaction.options.getInteger("set");
        let omiType = interaction.options.getString("omi-type");

        let { botUser, guildId, error } = await playerUtils.authorize(interaction);

        if (!guildId && !template) {
            interaction.editReply({ content: "Your guild is not registered or is not linked to this channel. Run this where your guild is linked or choose a template." });
            return;
        }
        
        if (allycode) allycode = playerUtils.cleanAndVerifyStringAsAllyCode(allycode);
        
        allycode = playerUtils.chooseBestAllycode(botUser, guildId, allycode, alt);

        if (!allycode)
        {
            await interaction.editReply({ content: "Please provide an allycode: `/raid max allycode:123456789` or `/register`" });
            return;
        }
        
        let playerData = await swapiUtils.getPlayer(allycode, constants.CacheTypes.QUERY, true);
        if (!playerData) {
            interaction.editReply({ content: `No player found with allycode **${allycode}**.`});
            return;
        }

        let response;
        switch (type)
        {
            case recCommon.REC_TYPES.OMICRON.abbreviation:
                response = await this.getNextOmicronRecommendationResponse(playerData, guildId, template, showLocked, minGear, omiType);
                break;
            case recCommon.REC_TYPES.DATACRON.abbreviation:
                response = await this.getDatacronRecommendationResponse(playerData, guildId, set);
                break;
        }

        await interaction.editReply(response);
    },
    autocomplete: async function (interaction)
    {
		const focusedOption = interaction.options.getFocused(true);

        let choices;

        if (focusedOption.name === "template")
        {
            const type = interaction.options.getString("type");

            let params = [`%${focusedOption.value}%`];
            if (type) params.push(type);

            let matches = await database.db.any(`
            SELECT DISTINCT template_name,template_display_name
            FROM guild_recommendation_template
            WHERE ${type ? "type = $2 AND" : ""} template_display_name ILIKE $1
            LIMIT 25`,
            params);

            choices = matches.map(r => (
                    { 
                        name: discordUtils.fitForAutocompleteChoice(r.template_display_name), 
                        value: r.template_name
                    }));
        }
        else if (focusedOption.name === "alt")
        {
            const focusedValue = interaction.options.getFocused();
            choices = await playerUtils.getAltAutocompleteOptions(focusedValue, interaction.user.id);
        }
        else if (focusedOption.name === "set")
        {
            // set
            let activeDatacronSetList = await datacronUtils.getDatacronSetList(true);
            choices = activeDatacronSetList.map(s => { return { name: `${s.set_id} (${s.color_text})`, value: s.set_id }; });
        }

		await interaction.respond(choices);
    },
    getDatacronRecommendationResponse: async function (playerData, guildId, set)
    {
        let recsData = await recCommon.loadRecommendationsForGuild(guildId, recCommon.REC_TYPES.DATACRON.abbreviation);
        if (recsData.recs === null || Object.keys(recsData.recs).length === 0)
        {
            return { content: `You have no datacron recommendations for your guild.` }; 
        }

        if (set)
        {
            recsData.recs.sets = recsData.recs.sets.filter(s => s.set === set);
        }

        const buffer = await recCommon.drawRecommendationsToBuffer(recsData, recCommon.REC_TYPES.DATACRON.abbreviation, playerData);

        if (buffer instanceof Array)
        {
            let files = [], embeds = [];
            for (let b = 0; b < buffer.length; b++)
            {
                let fileName = `dc-checklist-${swapiUtils.getPlayerAllycode(playerData)}-${buffer[b].set}.png`;
                files.push(new AttachmentBuilder(buffer[b].buffer, { name: fileName }));   
         
                let embed = new EmbedBuilder()
                        .setColor(buffer[b].color ?? "Grey")
                        .setTitle(`Set ${buffer[b].set}`)
                        .setImage(`attachment://${fileName}`);
                embeds.push(embed);
            }

            return { embeds: embeds, files: files };
        }
        else
        {
            let fileName = `dc-checklist-${swapiUtils.getPlayerAllycode(playerData)}-${Date.now()}.png`;
            const file = new AttachmentBuilder(buffer, { name: fileName });
            return { files: [file] };    
        }
    },
    getNextOmicronRecommendationResponse: async function(playerData, guildId, template, showLocked, minGear, omiType) {
        let recs;
        if (!template)
        {
            recs = await database.db.any(
                `SELECT tier,value,essential
                FROM guild_recommendation
                WHERE guild_id = $1 AND type = $2${omiType == null ? "AND tier <= 2" : ""}
                ORDER BY tier ASC, essential DESC`,
                [guildId, recCommon.REC_TYPES.OMICRON.abbreviation]
            );
        }
        else
        {
            recs = await database.db.any(
                `SELECT tier,value,essential
                FROM guild_recommendation_template
                WHERE template_name = $1 AND type = $2${omiType == null ? "AND tier <= 2" : ""}
                ORDER by tier ASC, essential DESC`,
                [template, recCommon.REC_TYPES.OMICRON.abbreviation]
            )
        }

        let playerRoster = swapiUtils.getPlayerRoster(playerData);
        let playerOmis = swapiUtils.getPlayerOmicrons(playerData);
        let allOmicronAbilities = await swapiUtils.getOmicronAbilitiesList();

        let embed = new EmbedBuilder();

        let missingOmis = {};
        for (let om of recCommon.OMICRON_MODES_DISPLAY_ORDER) {
            missingOmis[om.id] = new Array();
        }


        let minGearLevel = 1, minRelicLevel = -2;
        if (minGear.startsWith("G"))
        {
            minGearLevel = parseInt(minGear.slice(1));
        }
        else if (minGear.startsWith("R"))
        {
            minGearLevel = 13;
            minRelicLevel = parseInt(minGear.slice(1));
        }

        for (let o of recs)
        {
            let ability = allOmicronAbilities.find(a => a.base_id === o.value);

            if (omiType && recCommon.OMICRON_MODES[ability.omicron_mode].category.id !== omiType) continue;

            let playerHas = playerOmis[ability.omicron_mode]?.find(po => po === o.value);

            if (playerHas) continue;

            let playerUnit = playerRoster.find(ru => swapiUtils.getUnitDefId(ru) === ability.character_base_id);
            if (!showLocked && !playerUnit) continue;
            if (playerUnit && (
                    swapiUtils.getUnitGearLevel(playerUnit) < minGearLevel || 
                    (swapiUtils.getUnitRelicLevel(playerUnit) - 2) < minRelicLevel
                )) continue;

            missingOmis[recCommon.OMICRON_MODES[ability.omicron_mode].category.id].push(ability);
        }

        let keys = Object.keys(missingOmis);
        for (let keyIx in keys)
        {
            let property = keys[keyIx];
            if (omiType && omiType !== property) continue;

            let field = { name: recCommon.OMICRON_MODES_DISPLAY_ORDER[keyIx].displayName, value: "```html\n", inline: false };

            if (missingOmis[property].length === 0)
            {
                field.value = "None recommended";
            }
            else
            {
                let lastTier, lastIndexOfLastTier = 0;
                for (let abilityIx = 0; abilityIx < missingOmis[property].length && abilityIx < (omiType ? 18 : 10); abilityIx++)
                {
                    let ability = missingOmis[property][abilityIx];
                    let rec = recs.find(r => r.value === ability.base_id);
                    if (rec.tier != lastTier)
                    {
                        lastTier = rec.tier;
                        lastIndexOfLastTier = abilityIx;
                        field.value += `${(lastTier != null) ? "\n" : ""}>> ${recCommon.TIERS[rec.tier]}-Tier <<\n`
                    }
                    let unit = playerRoster.find(u => swapiUtils.getUnitDefId(u) === ability.character_base_id);

                    let gearStr;
                    if (unit)
                    {
                        if (swapiUtils.getUnitGearLevel(unit) === 13) gearStr = `R${swapiUtils.getUnitRelicLevel(unit)-2}`;
                        else gearStr = `G${swapiUtils.getUnitGearLevel(unit)}`;
                    } else gearStr = "NA";

                    field.value += `${abilityIx+1-lastIndexOfLastTier}. ${rec.essential ? "+ " : ""}<${gearStr} ${ability.unitName}> (${recCommon.ABILITY_TYPES[ability.type].short}) ${ability.name}\n`
                }
                field.value += "```";
            }

            embed.addFields(field);
        }
        
        embed.setTitle(`${swapiUtils.getPlayerName(playerData)} (${swapiUtils.getPlayerAllycode(playerData)}): Next Omicrons`);
        if (!omiType) embed.setFooter({ text: "Showing only recommendations at Tier B and above."})

        // message += `${playerHas ? discordUtils.symbols.pass : discordUtils.symbols.fail} ${ability.unitName}: ${ability.name}\n`

        let response = {
            embeds: [embed]
        };

        return response;
    }
}