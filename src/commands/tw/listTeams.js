const { EmbedBuilder } = require("discord.js");
const discordUtils = require("../../utils/discord.js");
const twUtils = require("./utils.js");
const guildUtils = require("../../utils/guild");
const playerUtils = require("../../utils/player");
const swapiUtils = require("../../utils/swapi");
const database = require("../../database");
const { SlashCommandSubcommandBuilder } = require("@discordjs/builders");

module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("list")
        .setDescription("List teams you've created for the war")
        .addStringOption(o => 
            o.setName("filter")
            .setDescription("Filter text for team names")
            .setRequired(false))
        .addStringOption(o =>
            o.setName("one")
            .setDescription("Team to show")
            .setRequired(false)
            .setAutocomplete(true)),
    permissionLevel: playerUtils.UserRoles.User,
    interact: async function(interaction, isDefenseTeam)
    {
        let filter = interaction.options.getString("filter")?.trim().toLowerCase();
        let one = interaction.options.getString("one")?.trim().toLowerCase();

        let response = await this.listTeams(interaction, filter, one, isDefenseTeam);
        
        if (response.embeds) {
            await interaction.editReply(response);
            return;
        }

        let messagesArray = discordUtils.splitMessage(response, "\r\n");
        for (let m = 0; m < messagesArray.length; m++)
        {
            if (m == 0)
                await interaction.editReply(messagesArray[m]);
            else 
                await interaction.followUp(messagesArray[m])
        }
    },
    autocomplete: async function(interaction, isDefenseTeam)
    {
        let { botUser, guildId, error } = await playerUtils.authorize(interaction, this.permissionLevel);     
        if (error)
        {
            await interaction.respond([{ name: "You do not have permission to do this.", value: "wwwwwww" }]);
            return;
        }

        const focusedValue = interaction.options.getFocused();

        let teams = await database.db.any(
`SELECT name, description
FROM tw_team
WHERE guild_id = $1 
  AND is_defense_team = $2 
  AND (name ILIKE $3 OR description ILIKE $3)
LIMIT 25
`, [guildId, isDefenseTeam, `%${focusedValue}%`]
        );

		await interaction.respond(
			teams
                .filter(t => t.name.length < twUtils.MAX_TEAM_NAME_LENGTH)
                .map(choice => ({ name: discordUtils.fitForAutocompleteChoice(`${choice.name} :: ${choice.description}`), value: choice.name }))
		);
    },
    listTeams: async function(inputOrInteraction, filter, teamName, isDefense) {
        let { botUser, guildId, error } = await playerUtils.authorize(inputOrInteraction, this.permissionLevel)
        if (error) {
            return error;
        }
        
        if (teamName)
            return await this.renderOneWithDetails(guildId, teamName, isDefense);
        else if (filter)
            return await this.renderListWithDetails(guildId, filter, isDefense);
        else
            return await this.renderListWithoutDetails(guildId, isDefense);
    },
    renderOneWithDetails: async function(guildId, teamName, isDefense)
    {
        let teamEmbed = new EmbedBuilder().setTitle(`Team: ${teamName}`).setColor(0x00aa00);
        teamEmbed.addFields(await twUtils.getTeamDescriptionAsEmbedField(guildId, teamName, isDefense));

        return { embeds: [teamEmbed] };
    },
    renderListWithDetails: async function(guildId, filter, isDefense)
    {
        var embedToSend = new EmbedBuilder().setTitle("Your TW Teams").setColor(0x00aa00);

        let fields = await twUtils.getAllMatchingTeamsDescriptionAsEmbedField(guildId, filter, isDefense);

        embedToSend.addFields(fields);
        
        return { embeds: [embedToSend] };
    },
    renderListWithoutDetails: async function (guildId, isDefense)
    {
        const teamsData = await database.db.any("SELECT name,description FROM tw_team WHERE guild_id = $1 AND is_defense_team = $2 ORDER BY name ASC", [guildId, isDefense]);

        var output = `List of Territory War ${isDefense ? "defense" : "offense"} teams:`;

        for (let teamIx = 0; teamIx < teamsData.length; teamIx++)
        {
            let team = teamsData[teamIx];
            output += "\r\n" + (teamIx+1) + ". **" + team.name + (team.description ? "**: " + team.description.slice(0, twUtils.MAX_TEAM_DESC_DISPLAY_LENGTH) : "**");
        }

        return output;
    }
}
