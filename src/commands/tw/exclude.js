const { EmbedBuilder } = require("discord.js");
const discordUtils = require("../../utils/discord.js");
const twUtils = require("./utils.js");
const guildUtils = require("../../utils/guild");
const playerUtils = require("../../utils/player");
const database = require("../../database");
const { SlashCommandSubcommandBuilder } = require("@discordjs/builders");

module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("exclude")
        .setDescription("Exclude players from war")
        .addStringOption(o =>
            o.setName("player")
            .setDescription("Player to exclude")
            .setRequired(false)
            .setAutocomplete(true))
        .addStringOption(o =>
            o.setName("p2")
            .setDescription("Player to exclude")
            .setRequired(false)
            .setAutocomplete(true))
        .addStringOption(o =>
            o.setName("p3")
            .setDescription("Player to exclude")
            .setRequired(false)
            .setAutocomplete(true))
        .addStringOption(o =>
            o.setName("p4")
            .setDescription("Player to exclude")
            .setRequired(false)
            .setAutocomplete(true))
        .addStringOption(o =>
            o.setName("p5")
            .setDescription("Player to exclude")
            .setRequired(false)
            .setAutocomplete(true))
        .addStringOption(o =>
            o.setName("p6")
            .setDescription("Player to exclude")
            .setRequired(false)
            .setAutocomplete(true))
        .addStringOption(o =>
            o.setName("p7")
            .setDescription("Player to exclude")
            .setRequired(false)
            .setAutocomplete(true))
        .addStringOption(o =>
            o.setName("p8")
            .setDescription("Player to exclude")
            .setRequired(false)
            .setAutocomplete(true))
        .addStringOption(o =>
            o.setName("p9")
            .setDescription("Player to exclude")
            .setRequired(false)
            .setAutocomplete(true)),
    permissionLevel: playerUtils.UserRoles.GuildAdmin,
    interact: async function(interaction)
    {
        let allycodesToExclude = [];

        let allycode = interaction.options.getString("player");
        if (allycode) allycodesToExclude.push(allycode);

        for (let i = 2; i <= 9; i++)
        {
            let ac = interaction.options.getString(`p${i}`);

            if (ac) allycodesToExclude.push(ac);
        }

        let response = await this.exclude(interaction, allycodesToExclude);
        await interaction.editReply(response);
    },
    autocomplete: async function(interaction)
    {
        
        let { botUser, guildId, error } = await playerUtils.authorize(interaction, this.permissionLevel)
        if (error) {
            await interaction.respond([{ name: "You do not have permission to do this.", value: "wwwwwwww" }]);
            return;
        }

        const focusedValue = interaction.options.getFocused();

        let names = await database.db.any(
`select player_name, allycode
from guild_players
left join tw_exclusion using (allycode)
where guild_players.guild_id = $1 AND tw_exclusion.tw_exclusion_id is NULL AND (player_name ILIKE $2 OR allycode LIKE $2)
LIMIT 25
`, [guildId, `%${focusedValue}%`]
        )

		await interaction.respond(
			names.map(choice => ({ name: `${choice.player_name} (${choice.allycode})`, value: choice.allycode }))
		);
    },
    exclude: async function(inputOrInteraction, allycodesToExclude)
    {
        let { botUser, guildId, error } = await playerUtils.authorize(inputOrInteraction, this.permissionLevel)
        if (error) {
            return error;
        }

        if (allycodesToExclude.length == 0)
        {
            let embed = await twUtils.getExclusionsListEmbed(guildId);
            return { embeds: [embed] };
        }

        let response = { content: "" };
        let acErrors = [];
        let acExcludes = [];
        for (let i = 0; i < allycodesToExclude.length; i++)
        {
            let allycodeParsed;
            try 
            { 
                allycodeParsed = playerUtils.cleanAndVerifyStringAsAllyCode(allycodesToExclude[i]);
                
                if (!acExcludes.find(e => e.allycode === allycodeParsed)) 
                {
                    const name = await playerUtils.getPlayerNameByAllycode(allycodeParsed);
                    acExcludes.push({ allycode: allycodeParsed, name: name });
                }
            }
            catch (err) 
            { 
                acErrors.push(allycodesToExclude[i]);
            }
        }

        if (acErrors.length > 0)
        {
            response.content += `Invalid Ally Codes:\n${acErrors.map(a => `- ${a}`).join("\n")}\n\n`
        }

        if (acExcludes.length > 0)
        {
            let excludeInsertValues = acExcludes.map(a => { return { allycode: a.allycode, guild_id: guildId }});
            let excludeInsert = database.pgp.helpers.insert(excludeInsertValues, database.columnSets.twExclusionCS);
            excludeInsert += " ON CONFLICT DO NOTHING;";
            await database.db.none(excludeInsert);

            response.content += `The following players have been excluded from your war plan:\n${acExcludes.map(a => `- ${a.name} (${a.allycode})`).join("\n")}`
        }

        return response;
    }
}
