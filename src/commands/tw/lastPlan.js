const { EmbedBuilder } = require("discord.js");
const discordUtils = require("../../utils/discord.js");
const twUtils = require("./utils.js");
const guildUtils = require("../../utils/guild");
const playerUtils = require("../../utils/player");
const database = require("../../database");
const swapi = require("../../utils/swapi.js");
const { SlashCommandSubcommandBuilder } = require("@discordjs/builders");

module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("last")
        .setDescription("Show the last plan results"),
    permissionLevel: playerUtils.UserRoles.User,
    interact: async function(interaction)
    {
        let { botUser, guildId, error } = await playerUtils.authorize(interaction, this.permissionLevel)
        if (error) {
            await interaction.editReply(error);
            return;
        }


        let messagesToSend = await this.generateLastPlanMessages(guildId);

        for (var m in messagesToSend)
        {
            await interaction.followUp(messagesToSend[m]);
        }
    },
    generateLastPlanMessages: async function(guildId)
    {

        const existingPlan = await database.db.oneOrNone("SELECT result_json, teams_json, territories_json FROM tw_plan_result WHERE guild_id = $1", [guildId]);

        if (!existingPlan || !existingPlan.result_json)
        {
            return ["You do not have an existing plan saved.  Use **/tw plan run** to generate one."];
        }

        return twUtils.generatePlanResults(existingPlan.result_json, existingPlan.teams_json, existingPlan.territories_json);
        
    }
}