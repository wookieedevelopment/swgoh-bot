const { EmbedBuilder } = require("discord.js");
const database = require("../../database");
const playerUtils = require("../../utils/player");
const discordUtils = require("../../utils/discord")
const twUtils = require("./utils")

module.exports = {
    onButtonClick: async function(interaction)
    {
        let { botUser, guildId, error } = await playerUtils.authorize(interaction, playerUtils.UserRoles.User)
        if (error) {
            await interaction.editReply(error);
            return;
        }

        const existingPlan = await database.db.oneOrNone("SELECT result_json, teams_json FROM tw_plan_result WHERE guild_id = $1", [guildId]);

        let results = existingPlan.result_json;
        let teams = existingPlan.teams_json;
        let teamsAndCounts = {};
        
        for (var resultsIndex = 0; resultsIndex < results.length; resultsIndex++)
        {
            var result = results[resultsIndex];
            // ta zero is reserved for offense
            for (let ta = 1; ta < result.territoryAssignments.length; ta++)
            {
                if (result.territoryAssignments[ta].foundTeamsCount == 0) 
                {
                    continue;
                }

                result.territoryAssignments[ta].foundTeams.forEach(team =>
                    {
                        if (!teamsAndCounts[team.name])
                            teamsAndCounts[team.name] = 1;
                        else
                            teamsAndCounts[team.name]++;
                    })
            }
        }

        let teamsData = new Array();
        for (let teamName in teamsAndCounts)
        {
            let team = teams.find(t => t.name == teamName);
            teamsData.push( {
                name: teamName,
                count: teamsAndCounts[teamName],
                team: team
            });
        }

        teamsData.sort((a, b) => b.count - a.count);

        let message = "";
        for (let d = 0; d < teamsData.length; d++)
        {
            message += "```ini\r\n"
            message += twUtils.getTeamAssignmentMessage(d + 1, teamsData[d].team);
            message += "Quantity: " + teamsData[d].count + "```\r\n"
        }

        if (message.length < 2000)
        {
            await interaction.editReply(message);
            return;
        }
        
        let messagesArray = discordUtils.splitMessage(message, "```\r\n");
        for (let m = 0; m < messagesArray.length; m++)
        {
            if (m == 0)
                await interaction.editReply(messagesArray[m]);
            else 
                await interaction.followUp(messagesArray[m])
        }
    }
}
