const { EmbedBuilder, ModalBuilder, ActionRowBuilder, TextInputBuilder, TextInputStyle } = require("discord.js");
const discordUtils = require("../../utils/discord.js");
const twUtils = require("./utils.js");
const guildUtils = require("../../utils/guild");
const playerUtils = require("../../utils/player");
const database = require("../../database");
const { SlashCommandSubcommandBuilder } = require("@discordjs/builders");



module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("config")
        .setDescription("Configure the TW Planner.")
        .addIntegerOption(o => 
            o.setName("teams-per-territory")
            .setDescription("Max teams per territory to assign")
            .setRequired(false))
        .addIntegerOption(o => 
            o.setName("max-fleets-per-player")
            .setDescription("Max fleets a player can be assigned")
            .setRequired(false))
        .addIntegerOption(o => 
            o.setName("max-squads-per-player")
            .setDescription("Max squads a player can be assigned")
            .setRequired(false))
        .addBooleanOption(o =>
            o.setName("update-defense-message")
            .setDescription("Set a message to be sent with TW defense assignments")
            .setRequired(false))
                ,
    permissionLevel: playerUtils.UserRoles.GuildAdmin,
    interact: async function(interaction)
    {
        let { botUser, guildId, error } = await playerUtils.authorize(interaction, this.permissionLevel)
        if (error) {
            await interaction.editReply(error);
            return;
        }

        let dm = interaction.options.getBoolean("update-defense-message");

        if (!dm)
        {
            await interaction.deferReply();
        }
        else
        {

            const config = await database.db.oneOrNone("SELECT defense_message FROM tw_metadata WHERE guild_id = $1", [guildId]);

            const modal = new ModalBuilder() // We create a Modal
            .setCustomId('tw:config')
            .setTitle('Update TW Defense Message')
            .addComponents(
                new ActionRowBuilder()
                    .addComponents(
                        new TextInputBuilder()
                            .setCustomId('text')
                            .setLabel('Message')
                            .setStyle(TextInputStyle.Paragraph)
                            .setMaxLength(250)
                            .setPlaceholder('Enter message to send alongside TW defense assignments. Leave blank for none.')
                            .setRequired(false))
            );

            if (config.defense_message?.length > 0)
            {
                modal.components[0].components[0].setValue(config.defense_message);
            }

            await interaction.showModal(modal);
        }
        
        let tpt = interaction.options.getInteger("teams-per-territory");
        let mf = interaction.options.getInteger("max-fleets-per-player");
        let ms = interaction.options.getInteger("max-squads-per-player");


        let response = "";
        if (tpt != null)
        {
            response += await this.setTeamsPerTerritory(guildId, tpt) + "\r\n";
        }
        if (mf != null)
        {
            response += await this.setMaxFleets(guildId, mf) + "\r\n";
        }
        if (ms != null)
        {
            response += await this.setMaxSquads(guildId, ms) + "\r\n";
        }

        if (response.length > 0) response += "\r\n";
        

        if (!dm)
        {
            response += await this.showCurrentConfig(guildId);
    
            await interaction.editReply(response);
        }
    },
    handleModal: async function (interaction) {
        await interaction.deferReply();

        let { botUser, guildId, error } = await playerUtils.authorize(interaction, this.permissionLevel)
        if (error) {
            await interaction.editReply({ embeds: [discordUtils.createErrorEmbed("Error", error)] });
            return;
        }

        let text = interaction.fields.getTextInputValue('text');

        if (text) text = text.trim();

        let response = await this.setDefenseMessage(guildId, text);
        response += "\n\n";
        response += await this.showCurrentConfig(guildId);
        await interaction.editReply(response);
    },
    setMaxSquads: async function(guildId, value)
    {
        await database.db.none("UPDATE tw_metadata SET default_max_squads_per_player = $1 WHERE guild_id = $2", [value, guildId]);
        return discordUtils.symbols.pass + " Max squads per player set to " + value + ".";
    },
    setMaxFleets: async function(guildId, value)
    {
        await database.db.none("UPDATE tw_metadata SET default_max_fleets_per_player = $1 WHERE guild_id = $2", [value, guildId]);
        return discordUtils.symbols.pass + " Max fleets per player set to " + value + ".";
    },
    setTeamsPerTerritory: async function(guildId, value)
    {
        await database.db.none("UPDATE tw_metadata SET max_teams_per_territory = $1 WHERE guild_id = $2", [value, guildId]);
        return discordUtils.symbols.pass + " Max teams per territory set to " + value + ".";
    },
    setDefenseMessage: async function(guildId, value)
    {
        await database.db.none("UPDATE tw_metadata SET defense_message = $1 WHERE guild_id = $2", [value, guildId]);
        return `${discordUtils.symbols.pass} Updated`;
    },
    showCurrentConfig: async function(guildId)
    {
        const config = await database.db.oneOrNone("SELECT * FROM tw_metadata WHERE guild_id = $1", [guildId]);
        if (!config)
        {
            return "Your guild is not properly configured.  Ask the bot owner to fix this.";
        }
        
        let msg = 
`Current TW Planner Configuration Settings. Update with **/tw config**:
**Max squads per player:** ${config.default_max_squads_per_player}
**Max fleets per player:** ${config.default_max_fleets_per_player}
**Max teams per territory:** ${config.max_teams_per_territory}

**Defense Message:**
${config.defense_message?.length > 0 ? config.defense_message : "<None set>"}
`;

        return msg;
    }
}
