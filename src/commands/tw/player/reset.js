const { EmbedBuilder } = require("discord.js");
const guildUtils = require("../../../utils/guild");
const playerUtils = require("../../../utils/player");
const database = require("../../../database");
const twPlayersCommon = require("./common");
const { SlashCommandSubcommandBuilder } = require("@discordjs/builders");

module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("reset")
        .setDescription("Reset a player's priority and max defense squads to the default (defense, priority 50, no max).")
        .addStringOption(o =>
            o.setName("player")
            .setDescription("Player to reset defense priorities")
            .setRequired(true)
            .setAutocomplete(true))
        ,
    permissionLevel: playerUtils.UserRoles.GuildAdmin,
    interact: async function(interaction)
    {
        let { botUser, guildId, error } = await playerUtils.authorize(interaction, this.permissionLevel)
        if (error) {
            await interaction.editReply(error);
            return;
        }

        let allycode = interaction.options.getString("player");

        let response = await this.getOffResponse(guildId, allycode);

        await interaction.editReply(response);
    },
    autocomplete: async function(interaction)
    {
        twPlayersCommon.autocomplete(interaction, this.permissionLevel, true);
    },
    getOffResponse: async function(guildId, allycode)
    {
        if (!allycode) return { content: "Invalid player. Please try again." };

        if (allycode === twPlayersCommon.ALL_PLAYERS_ALLYCODE)
        {
            await resetAllPlayerPriorities(guildId);

            let response = { content: `All players reset to Defense with priority level ${twPlayersCommon.DEFAULT_PRIORITY} and max defense squads will be what you have configured in \`\\tw config\`.` };

            return response;
        }
        
        allycode = playerUtils.cleanAndVerifyStringAsAllyCode(allycode);
        
        var guildPlayers = await guildUtils.getGuildIdAndAllyCodesForGuildByGuildId(guildId);
        if (!guildPlayers || !guildPlayers.allycodes || guildPlayers.allycodes.length == 0)
        {
            return { embeds: [new EmbedBuilder().setTitle("Error").setColor("Red").setDescription("Failed to load guild data. Is your guild properly registered?")] };
        }
        
        let player = guildPlayers.allycodesAndNames.find(e => e.allycode == allycode);
        if (!player)
        {
            return { embeds: [new EmbedBuilder().setTitle("Error").setColor("Red").setDescription(`${allycode} is not in your guild.`)] };
        }

        await resetOnePlayerPriority(guildId, allycode);
    
        let response = { content: `${player.player_name} (${player.allycode}) reset to Defense with priority level ${twPlayersCommon.DEFAULT_PRIORITY} and max defense squads will be what you have configured in \`\\tw config\`.` };
        return response; 
    }
}

async function resetAllPlayerPriorities(guildId)
{
    await database.db.any(
        `UPDATE tw_player_priority
        SET priority = $1, max_defense_squads = $2
        WHERE type = $3 AND guild_id = $4`,
        [
            twPlayersCommon.DEFAULT_PRIORITY,
            twPlayersCommon.DEFAULT_MAX_DEFENSE_SQUADS,
            twPlayersCommon.TYPES.DEFENSE,
            guildId]
    );
}


async function resetOnePlayerPriority(guildId, allycode)
{
    await database.db.any(
        `UPDATE tw_player_priority
        SET priority = $1, max_defense_squads = $2
        WHERE type = $3 AND guild_id = $4 AND allycode = $5`,
        [
            twPlayersCommon.DEFAULT_PRIORITY,
            twPlayersCommon.DEFAULT_MAX_DEFENSE_SQUADS,
            twPlayersCommon.TYPES.DEFENSE,
            guildId,
            allycode]
    );
}