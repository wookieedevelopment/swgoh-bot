const { EmbedBuilder } = require("discord.js");
const guildUtils = require("../../../utils/guild");
const playerUtils = require("../../../utils/player");
const database = require("../../../database");
const twPlayersCommon = require("./common");
const { SlashCommandSubcommandBuilder } = require("@discordjs/builders");

module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("off")
        .setDescription("Toggle whether a player has NO defense teams.")
        .addStringOption(o =>
            o.setName("player")
            .setDescription("Player to toggle for NO defense.")
            .setRequired(true)
            .setAutocomplete(true))
        ,
    permissionLevel: playerUtils.UserRoles.GuildAdmin,
    interact: async function(interaction)
    {
        let { botUser, guildId, error } = await playerUtils.authorize(interaction, this.permissionLevel)
        if (error) {
            await interaction.editReply(error);
            return;
        }

        let allycode = interaction.options.getString("player");

        let response = await this.getOffResponse(guildId, allycode);

        await interaction.editReply(response);
    },
    autocomplete: async function(interaction)
    {
        twPlayersCommon.autocomplete(interaction, this.permissionLevel);
    },
    getOffResponse: async function(guildId, allycode)
    {
        if (!allycode) return { content: "Invalid player. Please try again." };
        
        allycode = playerUtils.cleanAndVerifyStringAsAllyCode(allycode);
        
        var guildPlayers = await guildUtils.getGuildIdAndAllyCodesForGuildByGuildId(guildId);
        if (!guildPlayers || !guildPlayers.allycodes || guildPlayers.allycodes.length == 0)
        {
            return { embeds: [new EmbedBuilder().setTitle("Error").setColor("Red").setDescription("Failed to load guild data. Is your guild properly registered?")] };
        }
        
        let player = guildPlayers.allycodesAndNames.find(e => e.allycode == allycode);
        if (!player)
        {
            return { embeds: [new EmbedBuilder().setTitle("Error").setColor("Red").setDescription(`${allycode} is not in your guild.`)] };
        }

        let existingPriority = await database.db.oneOrNone("SELECT type,priority,max_defense_squads FROM tw_player_priority WHERE guild_id = $1 AND allycode = $2", [guildId, allycode]);

        let newPriority = existingPriority?.priority ?? twPlayersCommon.DEFAULT_PRIORITY;
        let newType;
        let newMaxDefenseSquads = (existingPriority?.max_defense_squads == null || existingPriority.max_defense_squads === 0) ? twPlayersCommon.DEFAULT_MAX_DEFENSE_SQUADS : existingPriority.max_defense_squads;

        if (!existingPriority || existingPriority.type === twPlayersCommon.TYPES.DEFENSE) newType = twPlayersCommon.TYPES.OFFENSE;
        else newType = twPlayersCommon.TYPES.DEFENSE;
        
        await database.db.any(
            `INSERT INTO tw_player_priority (guild_id, allycode, type, priority, max_defense_squads)
            VALUES ($1, $2, $3, $4, $5)
            ON CONFLICT (guild_id, allycode) DO UPDATE SET type = $3, priority = $4, max_defense_squads = $5`,
            [guildId, allycode, newType, newPriority, newMaxDefenseSquads]
        );

        let responseText;
        if (newType === twPlayersCommon.TYPES.OFFENSE)
        {
            responseText = `${player.player_name} (${player.allycode}) will NOT have any defense assignments. Run this command again to switch them back to defense.`;
        } 
        else
        {
            responseText = `${player.player_name} (${player.allycode}) will have defense assignments at priority level ${newPriority}.`;
        }
        
        return { content: responseText }; 
    }
}
