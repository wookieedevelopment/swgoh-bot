
const database = require("../../../database");
const playerUtils = require("../../../utils/player");

const PLAYER_PRIORITY_TYPES = {
    DEFENSE: 'D',
    OFFENSE: 'O'
};

const ALL_PLAYERS_ALLYCODE = "ALL";

module.exports = {
    MIN_PRIORITY: 1,
    MAX_PRIORITY: 100,
    DEFAULT_PRIORITY: 50,
    DEFAULT_MAX_DEFENSE_SQUADS: -1,
    TYPES: PLAYER_PRIORITY_TYPES,
    ALL_PLAYERS_ALLYCODE: ALL_PLAYERS_ALLYCODE,
    autocomplete: async function(interaction, permissionLevel, includeAllPlayersOption)
    {
        let { botUser, guildId, error } = await playerUtils.authorize(interaction, permissionLevel)
        if (error) {
            await interaction.respond([{ name: "You do not have permission to do this.", value: "wwwwwwww" }]);
            return;
        }

        const focusedValue = interaction.options.getFocused();

        let names = await database.db.any(
`select player_name, allycode
from guild_players 
WHERE guild_id = $1 AND (player_name ILIKE $2 OR allycode LIKE $2)
LIMIT 25
`, [guildId, `%${focusedValue}%`]
        );

        if (includeAllPlayersOption)
        {
            names.unshift({ player_name: "All Players", allycode: ALL_PLAYERS_ALLYCODE });
            names = names.slice(0, 25);
        }

		await interaction.respond(
			names.map(choice => ({ name: `${choice.player_name ? choice.player_name : "Unknown Player"} (${choice.allycode})`, value: choice.allycode }))
		);
    },
}