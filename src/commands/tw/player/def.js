const { EmbedBuilder } = require("discord.js");
const guildUtils = require("../../../utils/guild");
const playerUtils = require("../../../utils/player");
const database = require("../../../database");
const twPlayersCommon = require("./common");
const { SlashCommandSubcommandBuilder } = require("@discordjs/builders");

module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("def")
        .setDescription("Set a player's defense priority")
        .addStringOption(o =>
            o.setName("player")
            .setDescription("Player to adjust defense priority")
            .setRequired(true)
            .setAutocomplete(true))
        .addIntegerOption(o =>
            o.setName("priority")
            .setDescription("1 = first assigned, 100 = last assigned")
            .setRequired(false)
            .setMinValue(1)
            .setMaxValue(100))
        .addIntegerOption(o =>
            o.setName("max-defense-squads")
            .setDescription("Override guild max-squads-per-player (0-100). 0 means offense only. -1 removes this setting.")
            .setRequired(false)
            .setMinValue(-1)
            .setMaxValue(100))
        ,
    permissionLevel: playerUtils.UserRoles.GuildAdmin,
    interact: async function(interaction)
    {
        let { botUser, guildId, error } = await playerUtils.authorize(interaction, this.permissionLevel)
        if (error) {
            await interaction.editReply(error);
            return;
        }

        let allycode = interaction.options.getString("player");
        let priority = interaction.options.getInteger("priority");
        let maxDefenseSquads = interaction.options.getInteger("max-defense-squads");


        let response = await this.getDefResponse(guildId, allycode, priority, maxDefenseSquads);

        await interaction.editReply(response);
    },
    autocomplete: async function(interaction)
    {
        twPlayersCommon.autocomplete(interaction, this.permissionLevel);
    },
    getDefResponse: async function(guildId, allycode, priority, maxDefenseSquads)
    {
        if (!allycode) return { content: "Invalid player. Please try again." };
        
        if (priority == null && maxDefenseSquads == null)
        {
            return { content: "You must provide at least one of `priority` or `max-defense-squads`. Please try again." };   
        }

        if (priority && (priority < 1 || priority > 100)) return { content: "Invalid priority. Must be 1-100. Please try again." };
        if (maxDefenseSquads != null && (maxDefenseSquads < -1 || maxDefenseSquads > 100)) return { content: "Invalid max-defense-squads. Must be -1 (to remove this setting) or 0-100. Zero max defense squads is the same as setting the player to offense only. Please try again." };

        allycode = playerUtils.cleanAndVerifyStringAsAllyCode(allycode);
        
        var guildPlayers = await guildUtils.getGuildIdAndAllyCodesForGuildByGuildId(guildId);
        if (!guildPlayers || !guildPlayers.allycodes || guildPlayers.allycodes.length == 0)
        {
            return { embeds: [new EmbedBuilder().setTitle("Error").setColor("Red").setDescription("Failed to load guild data. Is your guild properly registered?")] };
        }
        
        let player = guildPlayers.allycodesAndNames.find(e => e.allycode == allycode);
        if (!player)
        {
            return { embeds: [new EmbedBuilder().setTitle("Error").setColor("Red").setDescription(`${allycode} is not in your guild.`)] };
        }

        if (maxDefenseSquads == null)
        {
            await database.db.any(
                `INSERT INTO tw_player_priority (guild_id, allycode, type, priority)
                VALUES ($1, $2, 'D', $3)
                ON CONFLICT (guild_id, allycode) DO UPDATE SET type = 'D', priority = $3`,
                [guildId, allycode, priority]
            );    
        }
        else if (maxDefenseSquads === 0)
        {
            await database.db.any(
                `INSERT INTO tw_player_priority (guild_id, allycode, type, priority)
                VALUES ($1, $2, $3, $4)
                ON CONFLICT (guild_id, allycode) DO UPDATE SET type = $3`,
                [guildId, allycode, twPlayersCommon.TYPES.OFFENSE, twPlayersCommon.DEFAULT_PRIORITY]
            );
        }
        else if (priority == null)
        {
            await database.db.any(
                `INSERT INTO tw_player_priority (guild_id, allycode, type, max_defense_squads)
                VALUES ($1, $2, 'D', $3)
                ON CONFLICT (guild_id, allycode) DO UPDATE SET type = 'D', max_defense_squads = $3`,
                [guildId, allycode, maxDefenseSquads]
            );    
        }
        else
        {
            await database.db.any(
                `INSERT INTO tw_player_priority (guild_id, allycode, type, priority, max_defense_squads)
                VALUES ($1, $2, 'D', $3, $4)
                ON CONFLICT (guild_id, allycode) DO UPDATE SET type = 'D', priority = $3, max_defense_squads = $4`,
                [guildId, allycode, priority, maxDefenseSquads]
            );    
        }

        let response = { content: null };

        if (maxDefenseSquads === 0)
        {
            response.content = `${player.player_name} (${player.allycode}) will NOT have any defense assignments.`
        }
        else
        {
            response.content = `${player.player_name} (${player.allycode}) will now have `;
            if (priority != null) response.content += `priority ${priority} for defense assignments`;
            if (maxDefenseSquads != null) response.content += `${priority == null ? "" : " and " }${maxDefenseSquads == -1 ? "the default max defense squads, as specified in `/tw config`" : `a maximum of ${maxDefenseSquads} defense squads`}`;
            response.content += ".";    
        }
        
        return response;
    }
}
