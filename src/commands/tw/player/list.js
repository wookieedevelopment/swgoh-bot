const { EmbedBuilder } = require("discord.js");
const guildUtils = require("../../../utils/guild");
const playerUtils = require("../../../utils/player");
const discordUtils = require("../../../utils/discord");
const database = require("../../../database");
const twPlayersCommon = require("./common");
const { SlashCommandSubcommandBuilder } = require("@discordjs/builders");

module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("list")
        .setDescription("List defense priorities")
        ,
    permissionLevel: playerUtils.UserRoles.GuildAdmin,
    interact: async function(interaction)
    {
        let { botUser, guildId, error } = await playerUtils.authorize(interaction, this.permissionLevel)
        if (error) {
            await interaction.editReply(error);
            return;
        }

        let response = await this.getListResponse(guildId);

        await interaction.editReply(response);
    },
    getListResponse: async function(guildId)
    {
        var guildPlayers = await guildUtils.getGuildIdAndAllyCodesForGuildByGuildId(guildId);
        if (!guildPlayers || !guildPlayers.allycodes || guildPlayers.allycodes.length == 0)
        {
            return { embeds: [new EmbedBuilder().setTitle("Error").setColor("Red").setDescription("Failed to load guild data. Is your guild properly registered?")] };
        }

        let prioritiesData = await database.db.any(
            `SELECT g.allycode, g.player_name, type, priority, max_defense_squads
            FROM guild_players g
            LEFT JOIN tw_player_priority tpp ON g.allycode = tpp.allycode AND g.guild_id = tpp.guild_id
            WHERE g.guild_id = $1`,
            [guildId]
        );

        for (let p = 0; p < prioritiesData.length; p++)
        {
            if (prioritiesData[p].type == null) 
            {
                prioritiesData[p].type = twPlayersCommon.TYPES.DEFENSE;
                prioritiesData[p].priority = twPlayersCommon.DEFAULT_PRIORITY;
                prioritiesData[p].max_defense_squads = twPlayersCommon.DEFAULT_MAX_DEFENSE_SQUADS;
            }
        }

        prioritiesData.sort((a, b) => a.priority - b.priority);

        let embeds = [];


        // player defense priorities
        let defenseEmbed = new EmbedBuilder()
            .setColor("DarkBlue")
            .setTitle("Defense Priorities (1 = highest, 100 = lowest)");

        let defendersListText = prioritiesData
            .filter(p => p.type === twPlayersCommon.TYPES.DEFENSE)
            .map(p => `| ${discordUtils.fit(p.player_name, 20)} | ${discordUtils.addDashesToAllycode(p.allycode)} | ${discordUtils.fit(p.priority.toString(), 3)} | ${(p.max_defense_squads == null || p.max_defense_squads == -1) ? "∞" : p.max_defense_squads.toString()}`)
            .join("\n");

        let defendersTableText;
        if (defendersListText.length == 0)
        {
            defendersTableText = "No defenses will be assigned. This is likely not a good plan.";
        }
        else
        {
            defendersTableText = 
`\`\`\`
| Defense Player       |  Ally Code  | Pr. | MDS
|----------------------|-------------|-----|----
${defendersListText}
\`\`\``;
        }

        defenseEmbed.setDescription(defendersTableText);
        defenseEmbed.setFooter({ text: "Pr.: Priority\nMDS: Max Defense Squads. ∞ will default to the value in /tw config."})

        // players on offense
        let offenseEmbed = new EmbedBuilder()
            .setColor("DarkRed")
            .setTitle("Offense ONLY players");

        let offenseListText = prioritiesData.filter(p => p.type === twPlayersCommon.TYPES.OFFENSE)
            .map(p => `| ${discordUtils.fit(p.player_name, 23)} | ${discordUtils.addDashesToAllycode(p.allycode)}`)
            .join("\n");

        let offenseTableText;
        if (offenseListText.length == 0)
        {
            offenseTableText = "No players are offense-only."
        }
        else
        {
            offenseTableText = 
`\`\`\`
| Offense ONLY Player     |  Ally Code  
|-------------------------|-------------
${offenseListText}
\`\`\``;
        }
        offenseEmbed.setDescription(offenseTableText);
        embeds.push(defenseEmbed, offenseEmbed);

        let response = { embeds: embeds };
        return response; 
    }
}
