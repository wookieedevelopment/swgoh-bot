const { EmbedBuilder, MessageFlags } = require("discord.js");
const database = require("../../database");
const playerUtils = require("../../utils/player");
const discordUtils = require("../../utils/discord");
const twUtils = require("./utils");

module.exports = {
    onSelect: async function(interaction)
    {
        await interaction.deferReply({ flags: MessageFlags.Ephemeral });
        let { botUser, guildId, error } = await playerUtils.authorize(interaction, playerUtils.UserRoles.GuildAdmin)
        if (error) {
            await input.channel.send(error);
            return;
        }

        let territory = parseInt(interaction.values[0]);

        const existingPlan = await database.db.oneOrNone("SELECT result_json, teams_json, territories_json FROM tw_plan_result WHERE guild_id = $1", [guildId]);

        if (!existingPlan || !existingPlan.result_json)
        {
            await interaction.editReply("You do not have an existing plan saved.  Use **/tw plan run** to generate one.");
            return;
        }

        let territoryDetails = existingPlan.territories_json[territory];

        if (!territoryDetails)
        {
            await interaction.editReply("Invalid territory.");
            return;
        }
        
        if (territoryDetails.curTeams.length === 0)
        {
            await interaction.editReply(`No players have been assigned to Territory ${territory}.`);
            return;
        }

        // catalog unique teams in the zone
        let teamNames = [...new Set(territoryDetails.teamNamesToAssign.flatMap(x => x).map(t => t.teamName))];

        let embed = new EmbedBuilder()
            .setColor("Grey")
            .setTitle(territory == twUtils.RESERVED_TERRITORY ? `Reserved for Offense Assignments` : `Assignments for Territory ${territory}`);

        for (let t of teamNames)
        {
            // find players that have been assigned the team
            let matchingPlayers = existingPlan.result_json.filter(p => p.territoryAssignments[territory].foundTeams.find(ft => ft.name === t));

            if (matchingPlayers.length === 0) continue;

            matchingPlayers.sort((a, b) => a.player.name.localeCompare(b.player.name));
            
            let field = {
                name: `${t} (x${matchingPlayers.length})`,
                value: null
            };

            field.value = "- " + matchingPlayers.map(p => p.player.name).join(`${discordUtils.LTR}\n- `);

            embed.addFields(field);
        }

        await interaction.editReply({ embeds: [embed] });
    }
}