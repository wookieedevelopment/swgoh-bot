const { EmbedBuilder } = require("discord.js");
const { logger, formatError }  = require ("../../utils/log")
const playerUtils = require("../../utils/player");
const discordUtils = require("../../utils/discord.js");
const twUtils = require("./utils.js");
const database = require("../../database");
const { SlashCommandSubcommandBuilder } = require("@discordjs/builders");

module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("delete")
        .setDescription("Delete team")
        .addStringOption(o => 
            o.setName("team")
            .setDescription("Team name to delete")
            .setRequired(true)
            .setAutocomplete(true)),
    permissionLevel: playerUtils.UserRoles.GuildAdmin,
    interact: async function(interaction, isDefenseTeam)
    {
        let team = interaction.options.getString("team").toLowerCase();

        let response = await this.deleteTeam(interaction, team, isDefenseTeam);
        await interaction.editReply(response);
    },
    autocomplete: async function(interaction, isDefenseTeam)
    {
        let { botUser, guildId, error } = await playerUtils.authorize(interaction, this.permissionLevel);     
        if (error)
        {
            await interaction.respond([{ name: "You do not have permission to do this.", value: "wwwwwwww" }]);
            return;
        }

        const focusedValue = interaction.options.getFocused();

        let teams = await database.db.any(
`SELECT name, description
FROM tw_team
WHERE guild_id = $1 
  AND is_defense_team = $2 
  AND name ILIKE $3
LIMIT 25
`, [guildId, isDefenseTeam, `%${focusedValue}%`]
        );

		await interaction.respond(
			teams
                .filter(t => t.name.length < twUtils.MAX_TEAM_NAME_LENGTH)
                .map(choice => ({ name: discordUtils.fitForAutocompleteChoice(`${choice.name} :: ${choice.description}`), value: choice.name }))
		);
    },
    deleteTeam: async function(inputOrInteraction, teamNameToDelete, isDefenseTeam) {
        let { botUser, guildId, error } = await playerUtils.authorize(inputOrInteraction, this.permissionLevel)
        if (error) {
            return error;
        }

        var teamId = null;

        await database.db.oneOrNone("DELETE FROM tw_team WHERE name = $1 AND guild_id = $2 AND is_defense_team = $3 RETURNING tw_team_id", 
        [teamNameToDelete, guildId, isDefenseTeam], e => { teamId = e ? e.tw_team_id : null; }); 

        if (teamId == null) {
            return `No ${isDefenseTeam ? "defense" : "offense"} team found by the name '${teamNameToDelete}'`;
        } 
        try {
            // clean up -- TODO: rewrite with event-driven architecture
            await database.db.oneOrNone("DELETE FROM tw_manager_tracked_team WHERE tw_team_id = $1 AND tw_manager_id = (SELECT tw_manager_id FROM tw_manager WHERE guild_id = $2)", [teamId, guildId]);
        } catch (e)
        {
            logger.error(`Failed to carry through deletion of tw_team ${teamId} from tw_manager_tracked_team. Details:\r\n${formatError(e)}`);
        }

        var unitsDeleted = await database.db.result("DELETE FROM tw_team_unit WHERE tw_team_id = $1", [teamId], r => r.rowCount);
        return `Deleted ${isDefenseTeam ? "defense" : "offense"} team '${teamNameToDelete}' and all ${unitsDeleted} features of the team.`;
    }
}
