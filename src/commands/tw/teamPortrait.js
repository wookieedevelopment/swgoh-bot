const { EmbedBuilder } = require("discord.js");
const discordUtils = require("../../utils/discord.js");
const playerUtils = require("../../utils/player");
const database = require("../../database");
const swapiUtils = require("../../utils/swapi.js");
const unitsUtils = require("../../utils/units.js");
const twUtils = require("./utils.js");
const { SlashCommandSubcommandBuilder } = require("@discordjs/builders");
const { formatError } = require("../../utils/log.js");

module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("portrait")
        .setDescription("Force unit portrait for team")
        .addStringOption(o => 
            o.setName("team")
            .setDescription("Team name")
            .setAutocomplete(true)
            .setRequired(true))
        .addStringOption(o =>
            o.setName("portrait")
            .setDescription("Unit to use for portrait")
            .setAutocomplete(true)
            .setRequired(true)),
    permissionLevel: playerUtils.UserRoles.GuildAdmin,
    interact: async function(interaction, isDefenseTeam)
    {
        let team = interaction.options.getString("team").trim().toLowerCase();
        let portrait = interaction.options.getString("portrait");

        let unitsList = await swapiUtils.getUnitsList();
        let match = unitsList.find(u => swapiUtils.getUnitDefId(u) === portrait);
        if (!match)
        {
            await interaction.editReply(`Invalid unit "${portrait}". Please select from the list.`);
            return;
        }

        let response = await this.setTeamPortrait(interaction, team, portrait, isDefenseTeam);
        await interaction.editReply(response);
    },
    autocomplete: async function(interaction, isDefenseTeam)
    {
        let { botUser, guildId, error } = await playerUtils.authorize(interaction, this.permissionLevel);     
        if (error)
        {
            await interaction.respond([{ name: "You do not have permission to do this.", value: "wwwwwwww" }]);
            return;
        }

		const focusedOption = interaction.options.getFocused(true);
        const focusedValue = focusedOption.value;
        let choices;

        if (focusedOption.name === "team")
        {
            let teams = await database.db.any(
    `SELECT name, description
    FROM tw_team
    WHERE guild_id = $1 
    AND is_defense_team = $2 
    AND name ILIKE $3
    LIMIT 25
    `, [guildId, isDefenseTeam, `%${focusedValue}%`]
            );

            choices = teams
            .filter(t => t.name.length < twUtils.MAX_TEAM_NAME_LENGTH)
            .map(choice => ({ name: discordUtils.fitForAutocompleteChoice(`${choice.name} :: ${choice.description}`), value: choice.name }));

        }
        else if (focusedOption.name === "portrait")
        {
            let cleanName = focusedValue.replace(/\W/g, "").toLowerCase();
            choices = await unitsUtils.getUnitMatchesByNickname(cleanName) ?? new Array();
            
            let unitsList = await swapiUtils.getUnitsList();
            let otherChoices = unitsList
                .filter(u => 
                    !choices.find(c => swapiUtils.getUnitDefId(c) === swapiUtils.getUnitDefId(u)) 
                    && u.name.replace(/\W/g, "").toLowerCase().indexOf(cleanName) >= 0);

            otherChoices.sort((a, b) => a.name.localeCompare(b.name));
            choices = choices.concat(otherChoices);
            choices = choices.slice(0, 25).map(c => ({ name: c.name, value: swapiUtils.getUnitDefId(c)}));
        }

		await interaction.respond(choices);
    },
    setTeamPortrait: async function(interaction, teamName, portrait, isDefenseTeam) {
        let { botUser, guildId, error } = await playerUtils.authorize(interaction, this.permissionLevel)
        if (error) {
            return discordUtils.createErrorEmbed("Error", error);
        }

        const { rowCount } = await database.db.result("UPDATE tw_team SET portrait = $1 WHERE guild_id = $2 AND name = $3 AND is_defense_team = $4", [portrait, guildId, teamName, isDefenseTeam])

        if (rowCount >= 1)
            return { content: "Updated portrait for team " + teamName + "." }
        else
            return { content: "No team by the name " + teamName + ". Choose from the list, or add a team with `/tw team add`." };
    }
}
