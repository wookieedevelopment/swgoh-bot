const { EmbedBuilder } = require("discord.js");
const discordUtils = require("../../utils/discord.js");
const twUtils = require("./utils.js");
const guildUtils = require("../../utils/guild");
const playerUtils = require("../../utils/player");
const swapiUtils = require("../../utils/swapi")
const database = require("../../database");
const { logger, formatError } = require("../../utils/log");
const { SlashCommandSubcommandBuilder } = require("@discordjs/builders");

const defaultErrorMessage = 
`You have to specify a team name, team members, and features. For example:
 - /tw team add team:glk features:jedi master kenobi(g=13, z=6); ki-adi mundi (r>4, z=1); ahsoka tano (g=13, z=1)
 - /tw team add team:exec features:exec(s>6)
 - /tw team add team:fast_dr features:darthrev(speed>340)`;

module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("add")
        .setDescription("Create or update a TW team.")
        .addStringOption(o => 
            o.setName("team")
            .setDescription("Team name to create or update. Capitalization ignored.")
            .setRequired(true))
        .addStringOption(o =>
            o.setName("features")
            .setDescription("Features of the team. See /tw team help")
            .setRequired(false))
        .addStringOption(o => 
            o.setName("desc")
            .setDescription("Description, sent to players for defense teams.")
            .setRequired(false))
        .addBooleanOption(o =>
            o.setName("clear-dcs")
            .setDescription("Clear all datacron requirements for this team (default: false).")
            .setRequired(false)
        )
        .addStringOption(o =>
            o.setName("dc-filter")
            .setDescription("Additional DC filtering")
            .setRequired(false)
            .setChoices(
                { name: "Any L9 Match", value: twUtils.DATACRON_FILTERS.AnyL9Match },
                { name: "Any L6 Match", value: twUtils.DATACRON_FILTERS.AnyL6Match },
                { name: "Any L3 Match", value: twUtils.DATACRON_FILTERS.AnyL3Match }
            ))
        .addIntegerOption(o =>
            o.setName("dc-1")
            .setDescription("Datacron ability to match.")
            .setAutocomplete(true)
            .setRequired(false))
        .addIntegerOption(o =>
            o.setName("dc-2")
            .setDescription("Datacron ability to match.")
            .setAutocomplete(true)
            .setRequired(false))
        .addIntegerOption(o =>
            o.setName("dc-3")
            .setDescription("Datacron ability to match.")
            .setAutocomplete(true)
            .setRequired(false))
        .addIntegerOption(o =>
            o.setName("dc-4")
            .setDescription("Datacron ability to match.")
            .setAutocomplete(true)
            .setRequired(false))
        .addIntegerOption(o =>
            o.setName("dc-5")
            .setDescription("Datacron ability to match.")
            .setAutocomplete(true)
            .setRequired(false))
        .addIntegerOption(o => 
            o.setName("req-dc-1")
            .setDescription("Required datacron ability to match.")
            .setAutocomplete(true)
            .setRequired(false))
        .addIntegerOption(o => 
            o.setName("req-dc-2")
            .setDescription("Required datacron ability to match.")
            .setAutocomplete(true)
            .setRequired(false)),
    permissionLevel: playerUtils.UserRoles.GuildAdmin,
    interact: async function(interaction, isDefenseTeam)
    {
        let team = interaction.options.getString("team").trim().toLowerCase();
        let features = interaction.options.getString("features")?.trim().toLowerCase();
        let description = interaction.options.getString("desc")?.trim();
        let clearDCs = interaction.options.getBoolean("clear-dcs") ?? false;
        let dcFilter = interaction.options.getString("dc-filter");
        let dcs = [
            interaction.options.getInteger("dc-1") ?? twUtils.NO_DATACRON_SPECIFIED,
            interaction.options.getInteger("dc-2") ?? twUtils.NO_DATACRON_SPECIFIED,
            interaction.options.getInteger("dc-3") ?? twUtils.NO_DATACRON_SPECIFIED,
            interaction.options.getInteger("dc-4") ?? twUtils.NO_DATACRON_SPECIFIED,
            interaction.options.getInteger("dc-5") ?? twUtils.NO_DATACRON_SPECIFIED
        ];

        let requiredDcs = [
            interaction.options.getInteger("req-dc-1") ?? twUtils.NO_DATACRON_SPECIFIED,
            interaction.options.getInteger("req-dc-2") ?? twUtils.NO_DATACRON_SPECIFIED
        ]

        let response = await this.addTeam(interaction, team, features, description, isDefenseTeam, clearDCs, dcFilter, dcs, requiredDcs);

        await interaction.editReply(response);
    },
    autocomplete: async function(interaction)
    {
        let { botUser, guildId, error } = await playerUtils.authorize(interaction, this.permissionLevel);     
        if (error)
        {
            await interaction.respond([{ name: "You do not have permission to do this.", value: "wwwwwwww" }]);
            return;
        }

        const focusedValue = interaction.options.getFocused();

        let dcs = await database.db.any(
`SELECT DISTINCT datacron_id, short_description, (select count(*) from tw_team where datacron_id = ANY(datacrons))
FROM datacron
LEFT JOIN datacron_template using (affix_id, set_id) 
WHERE short_description ILIKE $1 AND expiration_dt > now()
ORDER BY count DESC, short_description
LIMIT 25
`, [`%${focusedValue}%`]);

        await interaction.respond(
            dcs.map(choice => ({ name: discordUtils.fitForAutocompleteChoice(choice.short_description), value: choice.datacron_id }))
        );
    },
    addTeam: async function(inputOrInteraction, name, squad, description, isDefenseTeam, clearDatacrons, datacronFilter, datacrons, datacronsRequired) {
        if (!name || name.length > twUtils.MAX_TEAM_NAME_LENGTH)
        {
            return { embeds: [
                discordUtils.createErrorEmbed("Error", "Team name must be between 1 and 100 characters.")
            ]};
        }

        let { botUser, guildId, error } = await playerUtils.authorize(inputOrInteraction, this.permissionLevel)
        if (error) {
            return error;
        }

        let team;
        
        try {
            if (squad) team = await twUtils.parseTeam(squad);
        }
        catch (e)
        {
            return { embeds: [
                discordUtils.createErrorEmbed("Error", `${e.message}\r\n\r\n${defaultErrorMessage}`)
            ]};
        }

        var existingTeamId = await database.db.oneOrNone("SELECT tw_team_id FROM tw_team WHERE guild_id = $1 AND name = $2 AND is_defense_team = $3", [guildId, name, isDefenseTeam], r => (r == null ? null : r.tw_team_id));

        if (existingTeamId)
        {
            if (team) team.id = existingTeamId;

            let params = [existingTeamId];
            let additionalQueryArgs = "";
            if (team)
            {
                params.push(team.isSquad(), team.hasGL);
                additionalQueryArgs += `, is_squad = $${params.length-1}, has_gl = $${params.length}`;
            }
            if (clearDatacrons || datacronFilter) { params.push(datacronFilter); additionalQueryArgs += `, datacron_filter = $${params.length}`; }
            if (clearDatacrons || datacrons.find(d => d != twUtils.NO_DATACRON_SPECIFIED)) { params.push(datacrons); additionalQueryArgs += `, datacrons = $${params.length}`; }
            if (clearDatacrons || datacronsRequired.find(d => d != twUtils.NO_DATACRON_SPECIFIED)) { params.push(datacronsRequired); additionalQueryArgs += `, datacrons_required = $${params.length}`; }
            if (description) { params.push(description); additionalQueryArgs += `, description = $${params.length}`; }

            if (additionalQueryArgs.length === 0) 
            {
                return { embeds: [
                    discordUtils.createErrorEmbed("Error", "No changes were specified.")
                ] };
            }

            // remove the ", " from the start of the additionalQueryArgs string
            let queryText = `UPDATE tw_team SET ${additionalQueryArgs.slice(2)} WHERE tw_team_id = $1`;

            if (squad) queryText = `DELETE FROM tw_team_unit WHERE tw_team_id = $1; ${queryText}`;
            await database.db.multi(queryText, params);
        }
        else if (!team)
        {
            // not an existing team, but no features were specified
            return { embeds: [
                discordUtils.createErrorEmbed("Error", `No features were specified for new team \`${name}\`.`)
            ] };
        }
        else
        {   
            try {
                await database.db.one(`INSERT INTO tw_team (name, guild_id, is_squad, is_defense_team, has_gl, datacron_filter, datacrons, datacrons_required, description) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9) RETURNING tw_team_id`,
                    [name, guildId, team.isSquad(), isDefenseTeam, team.hasGL, datacronFilter, datacrons, datacronsRequired, description], e => { team.id = e.tw_team_id });
            } catch (e)
            {
                logger.error(formatError(e));
                return { embeds: [
                    discordUtils.createErrorEmbed("Error", "Failed to create the team.  Please try again or contact the developer for help.")
                ] };
            }
        }

        if (team)
        {
            const teamUnitCS = new database.pgp.helpers.ColumnSet(
                ['tw_team_id', 
                'unit_def_id', 
                'feature', 
                'value',
                'comparison',
                'logic'], 
                {table: 'tw_team_unit'});
            
            var teamUnitInsertValues = new Array();

            for (var unitIndex = 0; unitIndex < team.units.length; unitIndex++)
            {
                var ucur = team.units[unitIndex];
                for (var featureIndex = 0; featureIndex < ucur.features.length; featureIndex++)
                {
                    teamUnitInsertValues.push(
                        {
                            tw_team_id: team.id,
                            unit_def_id: ucur.defId,
                            feature: ucur.features[featureIndex].key,
                            value: ucur.features[featureIndex].value,
                            comparison: ucur.features[featureIndex].comparison,
                            logic: ucur.features[featureIndex].logic
                        }
                    );
                }
            }
            
            const teamUnitInsert = database.pgp.helpers.insert(teamUnitInsertValues, teamUnitCS);
            
            await database.db.none(teamUnitInsert);
        }

        let teamEmbed = new EmbedBuilder().setTitle("Your Team").setColor(0x00aa00);
        teamEmbed.addFields(await twUtils.getTeamDescriptionAsEmbedField(guildId, name, isDefenseTeam));

        let msg;
        if (existingTeamId) msg = `Existing ${isDefenseTeam ? "defense" : "offense"} team ${name} updated.`;
        else msg = `Added ${isDefenseTeam ? "defense" : "offense"} team "${name}" with ${teamUnitInsertValues.length} features.`;

        let response = {
            content: msg,
            embeds: [teamEmbed]
        };
        
        return response;
    }
}
