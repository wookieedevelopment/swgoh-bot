const { SlashCommandSubcommandBuilder } = require("@discordjs/builders");
const { EmbedBuilder } = require("discord.js");
const discordUtils = require("../../utils/discord.js");
const twUtils = require("./utils.js");

module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("help")
        .setDescription("Show help for the TW defense planner."),
    name: "tw.help",
    aliases: ["war.help"],
    description: "Help for the WookieeBot TW planner.",
    interact: async function(interaction)
    {

        let overviewField = {
            name: "Overview", 
            value:
`The WookieeBot TW Planner helps you create a plan and automatically assigns teams to qualified players based on the plan. Your guild must be registered with WookieeBot to use.

Key features:
   1. Define possible teams, including advanced filtering capabilities.
   2. Create a priority order of assignments.
   3. Limit the number of squads/fleets per player or teams per territory.
   4. Define which teams go in which territories.
   5. Fast!  Recalculate an entire plan in seconds (after initial caching)
   6. Direct discord messaging of assignments (requires one-time registration)
   7. Set custom descriptions for each team.
   8. On-demand graphical display of the plan and assignments.
   9. Roles & permissions to limit who can make changes to the plan or metadata.
  10. Exclude specific players from the plan (e.g., if they did not join the war)
  `,
inline: false
            };

        let adminCommandsField = {
            name: "Admin Commands",
            value:
`**/tw config**: manage tw planner configuration
**/tw exclude**: exclude an allycode in your guild from the plan
**/tw include**: include an excluded allycode in the plan
**/tw team add**: add a team to your list of possible teams
**/tw team delete**: delete a team from your list of teams
**/tw team list**: list your teams
**/tw team describe**: set description for a team
**/tw testteam**: see who in your guild meets a team's criteria
**/tw plan show**: show your current tw plan
**/tw plan setteam**: assign/update a team in a territory
**/tw plan remteam**: remove a team from a territory
**/tw plan run**: run the plan
**/tw plan last**: show the last results of **/tw plan run**
**/tw plan send**: send DMs with assignments to guild members
`,
            inline: false
        };

        let allUserCommandsField = {
            name: "Guild Member Commands",
            value:
`**/tw team list**: list your teams
**/tw plan show**: show your current tw plan
**/tw plan last**: show the last results of **/tw plan run**
**/tw plan my**: show someone's assignments
`,
            inline: false
        };

        let embed = new EmbedBuilder()
            .setTitle("WookieeBot TW Defense Planner")
            .setColor(0xD2691E)
            .addFields(overviewField, adminCommandsField, allUserCommandsField);

        await interaction.editReply({ embeds: [embed] });
    }

}
