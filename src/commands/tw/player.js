const playerDef = require("./player/def");
const playerOff = require("./player/off");
const playerReset = require("./player/reset");
const playerList = require("./player/list");
const { SlashCommandSubcommandGroupBuilder } = require("@discordjs/builders");

module.exports = {
    data: new SlashCommandSubcommandGroupBuilder()
        .setName("player")
        .setDescription("TW Defense Player Priorities (Defense / Offense / Normal)")
        .addSubcommand(playerDef.data)
        .addSubcommand(playerOff.data)
        .addSubcommand(playerReset.data)
        .addSubcommand(playerList.data),
    interact: async function(interaction)
    {
      let subcommand = interaction.options.getSubcommand();

      switch (subcommand)
      {
        case playerDef.data.name:
            await playerDef.interact(interaction);
            break;
        case playerOff.data.name:
            await playerOff.interact(interaction);
            break;
        case playerReset.data.name:
            await playerReset.interact(interaction);
            break;
        case playerList.data.name:
            await playerList.interact(interaction);
            break;
      }
    },
    autocomplete: async function(interaction)
    {
        let subcommand = interaction.options.getSubcommand();
  
        switch (subcommand)
        {
            case playerDef.data.name:
                await playerDef.autocomplete(interaction);
                break;
            case playerOff.data.name:
                await playerOff.autocomplete(interaction);
                break;
            case playerReset.data.name:
                await playerReset.autocomplete(interaction);
                break;
        }
    }
}