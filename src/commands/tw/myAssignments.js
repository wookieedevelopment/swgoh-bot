const { EmbedBuilder } = require("discord.js");
const discordUtils = require("../../utils/discord.js");
const twUtils = require("./utils.js");
const guildUtils = require("../../utils/guild");
const playerUtils = require("../../utils/player");
const database = require("../../database");
const { SlashCommandSubcommandBuilder } = require("@discordjs/builders");
const swapiUtils = require("../../utils/swapi")



module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("my")
        .setDescription("Show one player's TW defense assignments (default: yours).")
        .addStringOption(o =>
            o.setName("player")
            .setDescription("Player whose assignments to show. Choose from the list or specify ally code.")
            .setRequired(false)
            .setAutocomplete(true))
        .addBooleanOption(o =>
            o.setName("required-assignments")
            .setDescription("Whether to include a message that the assignments are required (default: true).")
            .setRequired(false))
        ,
    permissionLevel: playerUtils.UserRoles.User,
    interact: async function(interaction)
    {
        let { botUser, guildId, error } = await playerUtils.authorize(interaction, this.permissionLevel)
        if (error) {
            await interaction.editReply(error);
            return;
        }

        let allycode = interaction.options.getString("player");
        let includeRequiredAssignments = interaction.options.getBoolean("required-assignments") ?? true;

        let response = await this.getMyAssignmentsResponse(botUser, guildId, allycode, includeRequiredAssignments);

        await interaction.editReply(response);
        
        // let messagesArray = discordUtils.splitMessage(message, "```\r\n");
        // for (let m = 0; m < messagesArray.length; m++)
        // {
        //     if (m == 0) await interaction.editReply(messagesArray[m]);
        //     else await interaction.followUp(messagesArray[m]);
        // }
    },
    autocomplete: async function(interaction)
    {
        let { botUser, guildId, error } = await playerUtils.authorize(interaction, this.permissionLevel)
        if (error) {
            await interaction.respond([{ name: "You do not have permission to do this.", value: "wwwwwwww" }]);
            return;
        }

        const focusedValue = interaction.options.getFocused();

        let names = await database.db.any(
`select player_name, allycode
from guild_players 
WHERE guild_id = $1 AND (player_name ILIKE $2 OR allycode LIKE $2)
LIMIT 25
`, [guildId, `%${focusedValue}%`]
        )

		await interaction.respond(
			names.map(choice => ({ name: `${choice.player_name ? choice.player_name : "Unknown Player"} (${choice.allycode})`, value: choice.allycode }))
		);
    },
    getMyAssignmentsResponse: async function(botUser, guildId, allycode, includeRequiredAssignments)
    {
        if (allycode) allycode = playerUtils.cleanAndVerifyStringAsAllyCode(allycode);
        
        var guildPlayers = await guildUtils.getGuildIdAndAllyCodesForGuildByGuildId(guildId);
        if (!guildPlayers || !guildPlayers.allycodes || guildPlayers.allycodes.length == 0)
        {
            return { embeds: [new EmbedBuilder().setTitle("Error").setColor(0x500000).setDescription("Failed to load guild data. Is your guild properly registered?")] };
        }

        if (!allycode)
        {
            allycode = guildPlayers.allycodes.find(p => botUser.allycodes.find(a => p == a))
        } 
        
        if (!allycode || guildPlayers.allycodes.indexOf(allycode) == -1)
        {
            return { embeds: [new EmbedBuilder().setTitle("Error").setColor(0x500000).setDescription(`${allycode} is not in your guild. Either specify a valid ally code in your guild to see a guild mate's assignments, or use **/tw plan my** to see your own assignments.`)] };
        }

        const existingPlan = await database.db.oneOrNone("SELECT result_json, teams_json FROM tw_plan_result WHERE guild_id = $1", [guildId]);
        
        if (!existingPlan || !existingPlan.result_json)
        {
            return { embeds: [new EmbedBuilder().setTitle("Error").setColor(0x909000).setDescription("You do not have any assignments.  Did an officer run the plan?")] };
        }

        var my = existingPlan.result_json.find(p => p.player.allycode == allycode );

        if (!my)
        {
            // check if player was excluded
            let excluded = await database.db.one("SELECT COUNT(*) FROM tw_exclusion WHERE guild_id = $1 AND allycode = $2", [guildId, allycode]);
            if (excluded.count > 0) return { embeds: [new EmbedBuilder().setTitle("Error").setColor(0x500000).setDescription(`${allycode} is excluded from the war and does not have any assignments.\n\nInclude with \`/tw include\` and re-run your plan to give them assignments.`)] };
            return { embeds: [new EmbedBuilder().setTitle("Error").setColor(0x500000).setDescription(`${allycode} does not have any assignments.`) ] };
        }
        var teams = existingPlan.teams_json;

        const defenseMessage = (await database.db.oneOrNone("SELECT defense_message FROM tw_metadata WHERE guild_id = $1", [guildId]))?.defense_message;

        return await twUtils.getIndividualAssignmentResponse(my, teams, defenseMessage, includeRequiredAssignments);
    }
}
