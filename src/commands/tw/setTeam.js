const { EmbedBuilder } = require("discord.js");
const discordUtils = require("../../utils/discord.js");
const twUtils = require("./utils.js");
const guildUtils = require("../../utils/guild");
const playerUtils = require("../../utils/player");
const database = require("../../database");
const twPlayerCommon = require("./player/common.js");
const { SlashCommandSubcommandBuilder } = require("@discordjs/builders");
const tools = require("../../utils/tools.js");

module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("set")
        .setDescription("Set a team on the defense layout.")
        .addIntegerOption(o =>
            o.setName("territory")
            .setDescription("Territory in which to set the team.")
            .setRequired(true)
            .addChoices(
                { name:"1", value: 1},
                { name:"2", value: 2},
                { name:"3", value: 3},
                { name:"4", value: 4},
                { name:"5", value: 5},
                { name:"6", value: 6},
                { name:"7", value: 7},
                { name:"8", value: 8},
                { name:"9", value: 9},
                { name:"10", value: 10}
            ))
        .addStringOption(o => 
            o.setName("team")
            .setDescription("Name of team to set. Capitalization is ignored.")
            .setRequired(true)
            .setAutocomplete(true))
        .addIntegerOption(o =>
            o.setName("priority")
            .setDescription("Priority (1-50), default 1")
            .setRequired(false)
            .setMinValue(1)
            .setMaxValue(50))
        .addIntegerOption(o => 
            o.setName("shift-by")
            .setDescription("Add to priority for all assignments at or above this assignment's priority.")
            .setRequired(false)
            .setMinValue(0)
            .setMaxValue(50))
        .addIntegerOption(o =>
            o.setName("limit")
            .setDescription("Limit # that can be assigned (1-50). Default unlimited.")
            .setRequired(false)
            .setMaxValue(50)
            .setMinValue(1))
        .addStringOption(o =>
            o.setName("for")
            .setDescription("Limit to a player")
            .setRequired(false)
            .setAutocomplete(true))
        .addIntegerOption(o => 
            o.setName("max-defense-priority")
            .setDescription("Max player defense priority allowed to set this team")
            .setRequired(false)
            .setMinValue(twPlayerCommon.MIN_PRIORITY)
            .setMaxValue(twPlayerCommon.MAX_PRIORITY)),
    permissionLevel: playerUtils.UserRoles.GuildAdmin,
    interact: async function(interaction)
    {
        let { botUser, guildId, error } = await playerUtils.authorize(interaction, this.permissionLevel)
        if (error) {
            await interaction.editReply(error);
            return;
        }

        let territory = interaction.options.getInteger("territory");
        let team = interaction.options.getString("team").trim().toLowerCase();
        let priority = interaction.options.getInteger("priority") ?? 1;
        let forAllyCode = interaction.options.getString("for");
        let limit = interaction.options.getInteger("limit");
        let shiftBy = interaction.options.getInteger("shift-by");
        let maxDefensePriority = interaction.options.getInteger("max-defense-priority");
        
        let response = await this.setTeam(guildId, team, territory, priority, limit, shiftBy, maxDefensePriority, forAllyCode);
        await interaction.editReply(response);
    },
    autocomplete: async function(interaction)
    {
        let { botUser, guildId, error } = await playerUtils.authorize(interaction, this.permissionLevel);     
        if (error)
        {
            await interaction.respond([{ name: "You do not have permission to do this.", value: "wwwwwwww" }]);
            return;
        }

		const focusedOption = interaction.options.getFocused(true);
        let choices;
        
        if (focusedOption.name === "team")
        {
            const territory = interaction.options.getInteger("territory");

            let isSquadFilter;
            if (territory == null)
            {
                isSquadFilter = [true, false];
            } else 
            {
                isSquadFilter = [!twUtils.isFleetTerritory(territory)];
            }

            let teams = await database.db.any(
    `SELECT name, description
    FROM tw_team
    WHERE guild_id = $1 
    AND is_defense_team = TRUE 
    AND name LIKE $2
    AND is_squad IN ($3:csv)
    LIMIT 25
    `, [guildId, `%${focusedOption.value}%`, isSquadFilter]
            );

            choices = teams
                        .filter(t => t.name.length < twUtils.MAX_TEAM_NAME_LENGTH)
                        .map(choice => ({ name: discordUtils.fitForAutocompleteChoice(`${choice.name} :: ${choice.description}`), value: choice.name }));
        } 
        else if (focusedOption.name === "for")
        {
            let names = await database.db.any(
                `select player_name, allycode
                from guild_players 
                WHERE guild_id = $1 AND (player_name ILIKE $2 OR allycode LIKE $2)
                LIMIT 25
                `, [guildId, `%${focusedOption.value}%`]
                        );
                        
            choices = names.map(choice => ({ name: `${choice.player_name ? choice.player_name : "Unknown Player"} (${choice.allycode})`, value: choice.allycode }));

        }
		await interaction.respond(choices);
    },
    setTeam: async function(guildId, teamName, territoryNum, priority, limit, shiftBy, maxDefensePriority, forAllyCode)
    {
        if (isNaN(priority) || priority < 1 || priority > twUtils.MAX_PRIORITY)
        {
            return `Priority must be between 1 and ${twUtils.MAX_PRIORITY}.`;
        }
        if (limit != null && (isNaN(limit) || limit < 1 || limit > twUtils.MAX_LIMIT))
        {
            return `Limit must be between 1 and ${twUtils.MAX_LIMIT}.`;
        }
        if (shiftBy != null && (isNaN(shiftBy) || shiftBy < 0 || shiftBy > twUtils.MAX_PRIORITY))
        {
            return `shift-by must be between 0 and ${twUtils.MAX_PRIORITY}.`;
        }
        if (maxDefensePriority != null && (isNaN(maxDefensePriority) || maxDefensePriority < twPlayerCommon.MIN_PRIORITY || maxDefensePriority > twPlayerCommon.MAX_PRIORITY))
        {
            return `max-defense-priority must be between ${twPlayerCommon.MIN_PRIORITY} and ${twPlayerCommon.MAX_PRIORITY}.`;
        }

        let forPlayerName;
        if (forAllyCode != null)
        {
            forAllyCode = tools.cleanAndVerifyStringAsAllyCode(forAllyCode);

            // confirm player is in guild
            forPlayerName = await guildUtils.isInGuild(forAllyCode, guildId);
            if (!forPlayerName) return `Ally Code ${forAllyCode} is not a member of your guild. If this is incorrect, refresh your guild's roster with \`/guild refresh roster\``;
        }

        var teamValid = await this.validateTeam(guildId, teamName, territoryNum);
        if (!teamValid)
        {
            return `**${teamName}** is not a valid team for that territory.  Check that you spelled it right and that you're not mis-matching squad and fleet territories.`;
        }

        const existingTeam = await twUtils.getExistingPlanAssignment(guildId, teamName, territoryNum, forAllyCode);

        var feedbackText = "";

        if (shiftBy)
        {
            await database.db.none(`UPDATE tw_plan SET priority = LEAST(priority + $1, $2) WHERE guild_id = $3 AND territory = $4 AND priority >= $5`,
            [shiftBy, twUtils.MAX_PRIORITY, guildId, territoryNum, priority]);
            feedbackText += `Shifted priorities for all assignments in territory ${territoryNum} with priority >= ${priority} by ${shiftBy}.\r\n`;
        }

        let maxDefensePriorityString = "", forAllyCodeString = "";
        if (maxDefensePriority) maxDefensePriorityString = `\nOnly players with defense priority <= ${maxDefensePriority} will be assigned. Adjust with \`/tw player def\``;
        if (forPlayerName) forAllyCodeString = `\nOnly ${forPlayerName} (${forAllyCode}) will be eligible for this assignment.`;

        if (!existingTeam)
        {
            await database.db.none("INSERT INTO tw_plan (guild_id, territory, team_name, priority, team_limit, max_defense_priority, for_allycode) VALUES ($1, $2, $3, $4, $5, $6, $7)",
            [guildId, territoryNum, teamName, priority, limit, maxDefensePriority, forAllyCode])

            feedbackText += `Set team **${teamName}** in territory ${territoryNum}${limit ? `, limit ${limit},` : ""} with priority ${priority}.${maxDefensePriorityString}${forAllyCodeString}`;
        } 
        else 
        {
            await twUtils.updatePlanAssignment(priority, limit, maxDefensePriority, null, guildId, teamName, territoryNum, forAllyCode);
            feedbackText += `Updated team **${teamName}** in territory ${territoryNum}${limit ? `, limit ${limit}` : ""}. Set priority to ${priority}.${maxDefensePriorityString}${forAllyCodeString}`;
        }

        return feedbackText;
    },
    validateTeam: async function(guildId, teamName, territoryNum)
    {
        const team = await database.db.oneOrNone("SELECT is_squad FROM tw_team WHERE guild_id = $1 AND name = $2 AND is_defense_team = TRUE", [guildId, teamName])
        if (team == null) return false;

        if (team.is_squad && twUtils.isFleetTerritory(territoryNum))
            return false;
        if (!team.is_squad && !twUtils.isFleetTerritory(territoryNum))
            return false;
        return true;
    }
}