const { EmbedBuilder, MessageFlags } = require("discord.js");
const database = require("../../database");
const playerUtils = require("../../utils/player");
const discordUtils = require("../../utils/discord")
const twUtils = require("./utils")

module.exports = {
    onButtonClick: async function(interaction)
    {
        await interaction.deferReply({ flags: MessageFlags.Ephemeral});

        let { botUser, guildId, error } = await playerUtils.authorize(interaction, playerUtils.UserRoles.User);
        if (error != null) {
            await interaction.followUp({ flags: MessageFlags.Ephemeral, content: error });
            return;
        }

        
        const existingPlan = await database.db.oneOrNone("SELECT result_json FROM tw_plan_result WHERE guild_id = $1", [guildId]);

        if (!existingPlan || !existingPlan.result_json)
        {
            await interaction.followUp({ flags: MessageFlags.Ephemeral, content: "You do not have an existing plan saved.  Use **/tw plan run** to generate one." });
            return;
        }

        let response = await twUtils.getPlanResultsJsonAndCSVMessage(existingPlan.result_json);

        response.flags = MessageFlags.Ephemeral;
        await interaction.editReply(response);
    }
}
