const { EmbedBuilder } = require("discord.js");
const discordUtils = require("../../utils/discord.js");
const playerUtils = require("../../utils/player");
const database = require("../../database");
const twUtils = require("./utils.js");
const { SlashCommandSubcommandBuilder } = require("@discordjs/builders");

module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("describe")
        .setDescription("Set a description for a team")
        .addStringOption(o => 
            o.setName("team")
            .setDescription("Team name")
            .setRequired(true)
            .setAutocomplete(true))
        .addStringOption(o =>
            o.setName("description")
            .setDescription("Description for the team")
            .setRequired(true)),
    permissionLevel: playerUtils.UserRoles.GuildAdmin,
    interact: async function(interaction, isDefenseTeam)
    {
        let team = interaction.options.getString("team").trim().toLowerCase();
        let description = interaction.options.getString("description");

        let response = await this.setTeamDescription(interaction, team, description, isDefenseTeam);
        await interaction.editReply(response);
    },
    autocomplete: async function(interaction, isDefenseTeam)
    {
        let { botUser, guildId, error } = await playerUtils.authorize(interaction, this.permissionLevel);     
        if (error)
        {
            await interaction.respond([{ name: "You do not have permission to do this.", value: "wwwwwwww" }]);
            return;
        }

        const focusedValue = interaction.options.getFocused();

        let teams = await database.db.any(
`SELECT name, description
FROM tw_team
WHERE guild_id = $1 
    AND is_defense_team = $2 
    AND name ILIKE $3
LIMIT 25
`, [guildId, isDefenseTeam, `%${focusedValue}%`]
        );

        await interaction.respond(
            teams
                .filter(t => t.name.length < twUtils.MAX_TEAM_NAME_LENGTH)
                .map(choice => ({ name: discordUtils.fitForAutocompleteChoice(`${choice.name} :: ${choice.description}`), value: choice.name }))
        );
    },
    setDefenseTeamDescription: async function(client, input, args)
    {
        if (!args || args.length == 0)
        {
            await input.reply("You have to specify an existing team and a description. For example:\r\n" + 
            "tw.sd cls CLS, Chewie, Han, C3PO, Chewpio\r\n");
            return;
        }

        let cmd = input.content.slice(discordUtils.prefix.length, input.content.indexOf(" "));
        let argStr = input.content.slice(discordUtils.prefix.length + cmd.length).trim();
        let firstSpaceIx = argStr.indexOf(" ");
        
        if (firstSpaceIx < 0)
        {
            await input.reply("You have to specify an existing team and a description. For example:\r\n" + 
            "tw.sd cls CLS, Chewie, Han, C3PO, Chewpio\r\n");
            return;
        }
        
        let teamName = argStr.slice(0, firstSpaceIx).toLowerCase();
        let description = argStr.slice(firstSpaceIx).trim().replace(/(?:\r\n|\r|\n)/g, "");

        let response = await this.setTeamDescription(input, teamName, description, true);
        await input.channel.send(response);
    },
    setOffenseTeamDescription: async function(client, input, args)
    {
        if (!args || args.length == 0)
        {
            await input.reply("You have to specify an existing team and a description. For example:\r\n" + 
            "tw.sd cls CLS, Chewie, Han, C3PO, Chewpio\r\n");
            return;
        }

        let cmd = input.content.slice(discordUtils.prefix.length, input.content.indexOf(" "));
        let argStr = input.content.slice(discordUtils.prefix.length + cmd.length).trim();
        let firstSpaceIx = argStr.indexOf(" ");
        
        if (firstSpaceIx < 0)
        {
            await input.reply("You have to specify an existing team and a description. For example:\r\n" + 
            "tw.sd cls CLS, Chewie, Han, C3PO, Chewpio\r\n");
            return;
        }
        
        let teamName = argStr.slice(0, firstSpaceIx).toLowerCase();
        let description = argStr.slice(firstSpaceIx).trim().replace(/(?:\r\n|\r|\n)/g, "");

        let response = await this.setTeamDescription(input, teamName, description, false);

        await input.channel.send(response);
    },
    setTeamDescription: async function(inputOrInteraction, teamName, description, isDefenseTeam) {
        let { botUser, guildId, error } = await playerUtils.authorize(inputOrInteraction, this.permissionLevel)
        if (error) {
            return error;
        }

        const { rowCount } = await database.db.result("UPDATE tw_team SET description = $1 WHERE guild_id = $2 AND name = $3 AND is_defense_team = $4", [description, guildId, teamName, isDefenseTeam])

        if (rowCount >= 1)
            return "Updated description for team " + teamName + ".";
        else
            return "No team by the name " + teamName + ". Use tw.addteam to add a defense team, or twm.addteam for an offense team.";
    }
}
