const { EmbedBuilder, MessageFlags } = require("discord.js");
const discordUtils = require("../../../utils/discord.js");
const twManagerUtils = require("./utils.js");
const database = require("../../../database");
const playerUtils = require("../../../utils/player");
const twManagerRepair = require("./repair")
const twManagerHistory = require("./history");

module.exports = {
    onButtonClick: async function(interaction)
    {
        // we need to do the defers this ASAP, so before any db calls
        if (interaction.customId == twManagerUtils.undoGLButtonId ||
            interaction.customId == twManagerUtils.undoFleetButtonId ||
            interaction.customId == twManagerUtils.undoSquadButtonId)
        {
            await interaction.deferUpdate();
        } 
        else 
        {
            await interaction.deferReply({ flags: MessageFlags.Ephemeral});
        }
        
        let existingManager = await twManagerUtils.getTWManagerByChannelId(interaction.channelId);
        if (!existingManager) return;

        let teamTypes;

        let buttonClickedTeamName = interaction.component?.label;

        if (interaction.customId == twManagerUtils.undoGLButtonId ||
            interaction.customId == twManagerUtils.undoFleetButtonId ||
            interaction.customId == twManagerUtils.undoSquadButtonId)
        {
            let hasGL = (interaction.customId == twManagerUtils.undoGLButtonId);
            let isSquad = (interaction.customId != twManagerUtils.undoFleetButtonId);

            if (hasGL) teamTypes = [twManagerUtils.teamTypes.GL];
            else if (isSquad) teamTypes = [twManagerUtils.teamTypes.SQUAD];
            else teamTypes = [twManagerUtils.teamTypes.FLEET];

            await database.db.any(
                `
UPDATE tw_manager_data SET used = FALSE, timestamp = NULL
WHERE tw_manager_data_id = 
(SELECT tw_manager_data_id 
	FROM tw_manager_data 
	WHERE discord_id = $1 and used = TRUE AND tw_manager_id = $2
 	AND tw_team_name IN (
		SELECT name
		 FROM tw_team
		 WHERE 
	 	tw_team.guild_id = $3
		AND tw_team.has_gl = $4
		AND tw_team.is_squad = $5
		AND tw_team.is_defense_team = false
	)
	ORDER BY timestamp DESC 
	limit 1)
`, [interaction.user.id, existingManager.tw_manager_id, existingManager.guild_id, hasGL, isSquad]
            );
        }
        else
        {
            let curDate = new Date();
            let r = await database.db.multiResult(
                `UPDATE tw_manager_data SET used = TRUE, timestamp = $2
                WHERE tw_manager_data_id = 
                (SELECT tw_manager_data_id 
                    FROM tw_manager_data 
                    WHERE discord_id = $1 and used = FALSE AND tw_manager_id = $4 
                    AND tw_team_name = (SELECT name FROM tw_team WHERE tw_team_id = $3) 
                    limit 1)
                RETURNING tw_team_name; 
                
                UPDATE tw_manager_data SET used = TRUE, timestamp = $2
                WHERE tw_manager_data_id IN (
                    SELECT distinct on(tw_team_name) tw_manager_data_id
                    FROM tw_manager_data 
                    WHERE discord_id = $1 and used = FALSE AND tw_manager_id = $4
                    AND tw_team_name in (SELECT name FROM tw_team WHERE tw_team_id in (
                        select distinct tw_team_id From tw_team
                        join tw_team_unit ttu using (tw_team_id)
                        inner join tw_manager_tracked_team using (tw_team_id)
                        where ttu.tw_team_id in 
                            (select tw_team_id from tw_team_unit where unit_def_id in 
                                (select unit_def_id from tw_team_unit where tw_team_id = $3))
                            and tw_team.tw_team_id <> $3
                            and tw_manager_id = $4)))
                AND EXISTS (
                    SELECT tw_manager_data_id
                    FROM tw_manager_data
                    WHERE discord_id = $1 and used = TRUE and tw_manager_id = $4 and timestamp = $2 and tw_team_name = (SELECT name FROM tw_team WHERE tw_team_id = $3)
                )
                RETURNING tw_team_name;
    
                SELECT name, has_gl, is_squad 
                FROM tw_team 
                WHERE tw_team_id = $3
                OR tw_team_id IN (
                    select distinct tw_team_id From tw_team
                    join tw_team_unit ttu using (tw_team_id)
                    inner join tw_manager_tracked_team using (tw_team_id)
                    where ttu.tw_team_id in 
                        (select tw_team_id from tw_team_unit where unit_def_id in 
                            (select unit_def_id from tw_team_unit where tw_team_id = $3))
                        and tw_team.tw_team_id <> $3
                        and tw_manager_id = $4
                );`, 
                [interaction.user.id, curDate, parseInt(interaction.customId), existingManager.tw_manager_id])

            let otherTeamNamesAffected = r[1]?.rows?.map(t => t.tw_team_name).join(", ");
            teamTypes = r[2]?.rows?.map(r => twManagerUtils.getTeamType(r));

            if (r[0].rowCount == 0)
            {
                await interaction.editReply({ 
                    content: `You do not have **${buttonClickedTeamName}** available. Here is what you have.`,
                    embeds: [await twManagerHistory.getHistoryEmbed(existingManager, interaction.user.id)] });
            } else {
                await interaction.editReply(`Marked **${buttonClickedTeamName}** as used${otherTeamNamesAffected.length == 0 ? "" : `, as well as **${otherTeamNamesAffected}**, which overlap units. Thank you!` }`);
            }
        }

        await twManagerRepair.refreshMessage(existingManager, interaction.channel, teamTypes);

    }
}
