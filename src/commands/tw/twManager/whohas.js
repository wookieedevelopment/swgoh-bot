const { EmbedBuilder } = require("discord.js");
const discordUtils = require("../../../utils/discord.js");
const twManagerUtils = require("./utils.js");
const twManagerRepair = require("./repair.js");
const database = require("../../../database");
const playerUtils = require("../../../utils/player");
const { SlashCommandBuilder, SlashCommandSubcommandBuilder } = require('@discordjs/builders');

const commandData = {
	name: 'whohas',
	description: 'Check who has and who has used a team on offense.',
    type: 1,
	defaultPermission: true,
	options: [
        {
            name: 'team',
            description: "The team to check.",
            type: 3,
            autocomplete: true,
            required: true
        }
	],
};

module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("whohas")
        .setDescription("Check who has and who has used a team on offense.")
        .addStringOption(o =>
            o.setName("team")
            .setDescription("The team to check.")
            .setRequired(true)
            .setAutocomplete(true))
        .addStringOption(o =>
            o.setName("filter")
            .setDescription("Additional filter to apply to the team notification.")
            .setRequired(false)),
    interact: async function(interaction)
    {
        let { existingManager, error } = await twManagerUtils.verifyManager(interaction, playerUtils.UserRoles.User);        
        if (error)
        {
            await interaction.editReply(error);
            return;
        }

        let teamName = interaction.options.getString("team").trim().toLowerCase();

        let filter = interaction.options.getString("filter");
        let embed;
        if (filter) embed = await this.getWhoHasEmbedWithFilter(existingManager, teamName, filter);
        else embed = await this.getWhoHasEmbed(existingManager, teamName);
        
        await interaction.editReply({ embeds: [embed] })
    },
    autocomplete: async function(interaction)
    {
        
        let { existingManager, error } = await twManagerUtils.verifyManager(interaction, playerUtils.UserRoles.User);        
        if (error)
        {
            await interaction.respond([{ name: "You do not have permission to do this.", value: "wwwwwwww" }]);
            return;
        }

        const focusedValue = interaction.options.getFocused();

        let teams = await twManagerUtils.getOffenseTeamSuggestions(existingManager, focusedValue);

		await interaction.respond(
			teams
                .filter(t => t.tw_team_name.length < twManagerUtils.MAX_TEAM_NAME_LENGTH)
                .map(choice => ({ name: choice.tw_team_name, value: choice.tw_team_name }))
		);
    },
    getWhoHasEmbedWithFilter: async function(existingManager, teamName, filter)
    {
        let embed = new EmbedBuilder().setTitle(`Team Availability for **${teamName}**`);
        let filterString, usersString, count;

        try {
            let { team, users } = await twManagerUtils.findUsersWhoHaveTeamWithFilter(existingManager, teamName, filter, true);
            if (filter) 
            {
                if (!team) 
                {
                    embed.setDescription("The filter specified was not valid.");
                    return embed;
                }
                filterString = await twManagerUtils.createFilterString(team);
            }
            if (!users || users.length == 0) 
            {
                embed.setDescription("No one has that team for offense.");
                return embed;
            }
            count = users.length;
            usersString = users.map(u => discordUtils.makeIdTaggable(u)).join("\r\n");
        } catch (e)
        {
            return e.message;
        }
        
        let description = `Filter: ${filterString}\r\nCount: ${count}\r\n\r\n${usersString}`;
        embed.setDescription(description);

        let footnote = "Known Issue: for users with more than one account, this filter may not properly indicate what they have available."

        embed.setFooter({ text: footnote });

        return embed;
    },
    getWhoHasEmbed: async function(existingManager, teamName)
    {
        let embed = new EmbedBuilder().setTitle(`TW Usage for **${teamName}**`);
                
        let data = await database.db.any(`
            SELECT discord_id, timestamp, used
            FROM tw_manager_data
            WHERE tw_manager_id = $1 AND tw_team_name = $2`, 
            [existingManager.tw_manager_id, teamName]);
        
        if (data.length == 0)
        {
            embed.setDescription("No one has that team for offense.");
            return embed;
        }
        

        let usedFields = [{ name: `Already used:`, value: "", inline: false }]
        let unusedFields = [{ name: `Not used:`, value: "", inline: false }]

        let curUsedField = usedFields[0];
        let curUnusedField = unusedFields[0];

        for (let t = 0; t < data.length; t++)
        {
            let user = discordUtils.makeIdTaggable(data[t].discord_id);
            if (data[t].used)
            {
                let str = `${user}: ${data[t].timestamp.toLocaleString("en-us", {
                    year: 'numeric',
                    month: 'short',
                    day: 'numeric',
                    hour: 'numeric',
                    minute: 'numeric',
                    timeZone: 'utc'
                  })}\r\n`;

                  if (curUsedField.value.length + str.length >= 1024)
                  {
                      curUsedField = { name: `Already used (continued):`, value: str, inline: false };
                      usedFields.push(curUsedField);
                  } else {
                      curUsedField.value += str;
                  }
            }
            else
            {
                let str = `${user}\r\n`;

                if (curUnusedField.value.length + str.length >= 1024)
                {
                    curUnusedField = { name: `Not used (continued):`, value: str, inline: false };
                    unusedFields.push(curUnusedField);
                } else {
                    curUnusedField.value += str;
                }
            }
        }

        if (usedFields[0].value.length == 0)
        {
            usedFields[0].value = "No one.";
        }
        if (unusedFields[0].value.length == 0)
        {
            unusedFields[0].value = "No one.";
        }

        let footnote = "If a user shows up more than once, that means they have multiple accounts. All times in UTC."

        embed.setFooter({ text: footnote });
        embed.addFields(usedFields);
        embed.addFields(unusedFields);

        return embed;
    }
}
