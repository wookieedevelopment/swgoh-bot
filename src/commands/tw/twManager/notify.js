const { EmbedBuilder, ModalBuilder, TextInputBuilder, TextInputStyle, ActionRowBuilder } = require("discord.js");
const discordUtils = require("../../../utils/discord.js");
const twManagerUtils = require("./utils.js");
const twUtils = require("../utils.js");
const twManagerRepair = require("./repair.js");
const database = require("../../../database");
const playerUtils = require("../../../utils/player");
const swapiUtils = require("../../../utils/swapi");
const { logger, formatError } = require("../../../utils/log");
const { SlashCommandBuilder, SlashCommandSubcommandBuilder } = require('@discordjs/builders');
const { Formatters } = require('discord.js');

module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("notify")
        .setDescription("Send a message to players with a team unused. A pop-up box will allow you to customize it.")
        .addChannelOption(o =>
            o.setName("channel")
                .setDescription("The channel in which to send the notification.")
                .setRequired(true))
        .addStringOption(o =>
            o.setName("team")
                .setDescription("The team to alert.")
                .setRequired(true)
                .setAutocomplete(true))
        .addStringOption(o =>
            o.setName("message")
                .setDescription("The message to send. For multi-line messages, leave this blank.")
                .setRequired(false))
        .addStringOption(o =>
            o.setName("filter")
                .setDescription("Additional filter to apply to the team notification.")
                .setRequired(false))
    ,
    name: "twm.notify",
    aliases: ["twmanager.notify"],
    description: "Notify people with a particular team.",
    interact: async function (interaction) {
        let { existingManager, error } = await twManagerUtils.verifyManager(interaction, playerUtils.UserRoles.GuildAdmin);
        if (error) {
            await interaction.editReply(error);
            return;
        }

        let channel = interaction.options.getChannel("channel");
        let teamName = interaction.options.getString("team").trim().toLowerCase();
        let message = interaction.options.getString("message");
        let filter = interaction.options.getString("filter");

        if (message) {
            await interaction.deferReply();
            let reply = await this.notify(existingManager, channel, teamName, message, filter);
            await interaction.editReply(reply);
            return;
        }

        const modal = new ModalBuilder() // We create a Modal
            .setCustomId('twmanager:notify')
            .setTitle('TW Notification')
            .addComponents(
                new ActionRowBuilder()
                    .addComponents(
                        new TextInputBuilder() // We create a Text Input Component
                            .setCustomId('message')
                            .setLabel('What message do you want to send?')
                            .setStyle(TextInputStyle.Paragraph)
                            .setMinLength(1)
                            .setMaxLength(1500)
                            .setPlaceholder('Enter message here')
                            .setRequired(true)),
                new ActionRowBuilder()
                    .addComponents(
                        new TextInputBuilder()
                            .setCustomId('channel')
                            .setLabel("[DO NOT CHANGE] Channel ID")
                            .setStyle(TextInputStyle.Short)
                            .setValue(channel.id)
                            .setRequired(true)),
                new ActionRowBuilder()
                    .addComponents(
                        new TextInputBuilder()
                            .setCustomId('team')
                            .setLabel("Team")
                            .setStyle(TextInputStyle.Short)
                            .setValue(teamName)
                            .setRequired(true)),
                new ActionRowBuilder()
                    .addComponents(
                        new TextInputBuilder()
                            .setCustomId('filter')
                            .setLabel('Additional team filter (optional)')
                            .setStyle(TextInputStyle.Paragraph)
                            .setMinLength(1)
                            .setMaxLength(500)
                            .setPlaceholder('Example: piett(speed>330)')
                            .setRequired(false))
            );

        if (message?.length > 0) modal.components[0].components[0].setValue(message);
        if (filter?.length > 0) modal.components[3].components[0].setValue(filter);

        await interaction.showModal(modal);
    },
    handleModal: async function (interaction) {
        await interaction.deferReply();
        const message = interaction.fields.getTextInputValue('message');
        const filter = interaction.fields.getTextInputValue('filter');
        const channelId = interaction.fields.getTextInputValue('channel');
        const teamName = interaction.fields.getTextInputValue('team');

        let { existingManager, error } = await twManagerUtils.verifyManager(interaction, playerUtils.UserRoles.GuildAdmin);
        if (error) {
            await interaction.editReply(error);
            return;
        }

        let channel = interaction.client.channels.cache.get(channelId);

        if (!channel) {
            await interaction.editReply("Invalid channel. Please try again.");
            return;
        }

        let reply = await this.notify(existingManager, channel, teamName, message, filter);
        await interaction.editReply(reply);
    },
    autocomplete: async function (interaction) {

        let { existingManager, error } = await twManagerUtils.verifyManager(interaction, playerUtils.UserRoles.GuildAdmin);
        if (error) {
            await interaction.respond([{ name: "You do not have permission to do this.", value: "wwwwwwww" }])
            return;
        }

        const focusedValue = interaction.options.getFocused();

        let teams = await twManagerUtils.getOffenseTeamSuggestions(existingManager, focusedValue);

        await interaction.respond(
            teams
                .filter(t => t.tw_team_name.length < twManagerUtils.MAX_TEAM_NAME_LENGTH)
                .map(choice => ({ name: choice.tw_team_name, value: choice.tw_team_name }))
        );
    },
    notify: async function (existingManager, channel, teamName, text, filter) {

        let filterString, usersString;

        try {
            let { team, users } = await twManagerUtils.findUsersWhoHaveTeamWithFilter(existingManager, teamName, filter, true);
            if (filter) {
                if (!team) return "The filter specified was not valid.";
                filterString = await twManagerUtils.createFilterString(team);
            }
            if (!users || users.length == 0) return "No one has that team for offense.";

            usersString = users.map(u => discordUtils.makeIdTaggable(u)).join(" ");

        } catch (e) {
            return e.message;
        }


        let msg =
            `**Notification for team: ${teamName}**
${filterString ? `Filter: ${filterString}\r\n` : ""}
Players: ${usersString}

${text}`

        if (msg.length > discordUtils.MAX_DISCORD_MESSAGE_LENGTH) {
            return `Notification, including the IDs of the players being notified, is too long. Reduce to ${discordUtils.MAX_DISCORD_MESSAGE_LENGTH - (msg.length - text.length)} characters.\r\nCurrent length: ${text.length} characters`;
        }

        try {
            await channel.send(msg);
            return "Notification sent!"
        }
        catch (e) {
            let embed = discordUtils.createErrorEmbed("Error", "Could not send. Check WookieeBot permissions in the channel specified.\r\n\r\nDetails:\r\n" + e.message);

            return { embeds: [embed] };
        }
    }
}
