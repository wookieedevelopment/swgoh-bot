const { EmbedBuilder } = require("discord.js");
const discordUtils = require("../../../utils/discord.js");
const swapiUtils = require("../../../utils/swapi.js");
const twManagerUtils = require("./utils.js");
const database = require("../../../database");
const playerUtils = require("../../../utils/player");
const guildUtils = require("../../../utils/guild");
const twManagerRepair = require("./repair")
const twUtils = require("../utils");
const { SlashCommandSubcommandBuilder } = require("@discordjs/builders");
const constants = require("../../../utils/constants.js");

module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("reset")
        .setDescription("Reset the WookieeBot TW Manager for a new war. Use after running defense plan."),
    permissionLevel: playerUtils.UserRoles.GuildAdmin,
    interact: async function(interaction)
    {
        let { botUser, guildId, error } = await playerUtils.authorize(interaction, this.permissionLevel)
        if (error) {
            await interaction.editReply({embeds: [discordUtils.createErrorEmbed("Error", error)]});
            return;
        }
        
        let responseMessage = await this.resetManager(interaction.client, guildId, interaction);
        
        await interaction.editReply(responseMessage);
    },
    resetManager: async function(client, guildId, interaction)
    {
        // make sure guild roster is updated
        let guild = await guildUtils.getGuildDataFromAPI({ wookieeBotGuildId: guildId }, { useCache: false });
        await guildUtils.refreshGuildPlayers(guild);

        if (!guildId) {
            return "Invalid guild for TW Manager.";
        }

        if (!await this.verifyGuildRegistration(guildId))
        {
            return "All members of your guild must be registered (**/user register** or **/guild users**) before doing this.  Check with **/guild status**";
        }

        let existingManager = await twManagerUtils.getTWManagerByGuildId(guildId);

        if (!existingManager) {
            return `You do not have a TW manager registered to your guild. Make sure your guild is registered in this channel/server, and install TW manager with install.`;
        }

        var managerChannel = client.channels.cache.get(existingManager.channel);
        
        if (!managerChannel) {
            return "You cannot do that to a channel to which WookieeBot does not have access.";
        }

        // reset tw data
        await database.db.none("DELETE FROM tw_manager_data WHERE tw_manager_id = $1", [existingManager.tw_manager_id]);

        // catalog teams available
        let offenseTeams = await this.catalogAvailableTeams(guildId, interaction);

        await this.saveAvailableTeamsToDb(existingManager, offenseTeams);

        await twManagerRepair.repairManager(client, guildId);

        return `TW Manager reset in ${discordUtils.makeChannelTaggable(managerChannel)}.`;
    },
    catalogAvailableTeams: async function(guildId, interaction)
    {
        await interaction.editReply(`Loading player and war data...`);
        
        // pull all assigned defenses
        let existingPlan = await database.db.oneOrNone("SELECT result_json, teams_json FROM tw_plan_result WHERE guild_id = $1", [guildId]);

        // pull all cached data for players in the war
        const caches = await database.db.any(
            `SELECT allycode, data_text, player_cache.timestamp, discord_id
            FROM guild_players
            LEFT JOIN player_cache USING (allycode)
            LEFT JOIN cache_data USING (cache_data_id)
            LEFT JOIN user_registration USING (allycode)
            WHERE guild_players.guild_id = $1
                AND (player_cache.type = $2 OR player_cache.type is NULL)
                AND allycode not in (SELECT allycode from tw_exclusion where guild_id = $1)`
            , [guildId, constants.CacheTypes.TW]);

        let earliestAllowedCacheDate = new Date();
        earliestAllowedCacheDate.setHours(earliestAllowedCacheDate.getHours() - 72);

        let playersThatNeedToBeCached = caches.filter(c => c.timestamp == null || c.timestamp < earliestAllowedCacheDate);
        if (playersThatNeedToBeCached.length > 0)
        {
            await interaction.editReply(`Data for ${playersThatNeedToBeCached.length} player${playersThatNeedToBeCached.length === 1 ? "" : "s"} is over 72 hours old. Retrieving new data.`);
            
            for (let cachedPlayerData of playersThatNeedToBeCached)
            {
                cachedPlayerData.data_text = await swapiUtils.getPlayer(cachedPlayerData.allycode, constants.CacheTypes.TW, true);
            }

            await interaction.editReply(`Saved data for ${playersThatNeedToBeCached.length} player${playersThatNeedToBeCached.length === 1 ? "" : "s"}.\nDetermining offense teams...`);
        } else {
            await interaction.editReply(`Determining offense teams...`);
        }

        // pull all tracked teams.  We'll manipulate this object to add players that have the team available for offense
        let trackedTeamsData = await database.db.any(
            `select tw_team.tw_team_id, name,description,unit_def_id,feature,value,comparison,logic,is_squad,datacrons,datacron_filter,datacrons_required
            FROM tw_manager_tracked_team
            LEFT JOIN tw_team USING (tw_team_id) 
            LEFT JOIN tw_team_unit USING (tw_team_id) 
            WHERE guild_id = $1 AND is_defense_team = false
            ORDER BY tw_team_id ASC`, [guildId]);
            
        let trackedTeams = twUtils.mapTeamsDataToObject(trackedTeamsData);

        // for the rewrite:
        // loop through all assigned defense teams in the plan, catalog all the individual characters assigned for each player
        // allycode -> assignments (defId array)

        // we'll need this in the search for datacron filters
        let unitsList = await swapiUtils.getUnitsList();

        // use "isTeamViable" to figure out who has what team
        for (let cacheIx = 0; cacheIx < caches.length; cacheIx++)
        {
            let playerCachedData = caches[cacheIx];
            let playerData = playerCachedData.data_text.allyCode ? playerCachedData.data_text : JSON.parse(playerCachedData.data_text);

            // loop through all offense teams to see which are viable for the player and do not overlap assigned defenses
            for (var teamIndex = 0; teamIndex < trackedTeams.length; teamIndex++)
            {
                if (!trackedTeams[teamIndex].players) trackedTeams[teamIndex].players = new Array();

                const { viable, reason, boundUnitDefIds } = await twUtils.isTeamViable(playerData, trackedTeams[teamIndex]);
                
                if (!viable) continue;

                // find existing defense assignments, if any
                let defenseDataForPlayer = existingPlan?.result_json?.find(r => r.player.allycode == playerCachedData.allycode);
                if (defenseDataForPlayer)
                {
                    // if player has defense assignments, then check if there's an overlap 
                    // between the defense assignments and the offense team being considered

                    let defenseAssignments = defenseDataForPlayer.territoryAssignments.slice(1); // don't include the reserved for offense territory
                    
                    let defenseTeamsDetails = existingPlan?.teams_json?.filter(
                        t => defenseAssignments.find(
                            da => da.foundTeams.findIndex(ft => ft.name === t.name) >= 0 
                        )
                    );

                    let overlap = boundUnitDefIds?.find(defId => 
                        defenseAssignments.find(da => da.foundTeams?.find(dt => dt.boundUnitDefIds?.find(budi => budi == defId))));

                    // they were assigned a defense team that contains a unit in the offense team
                    if (overlap) continue;
                }

                // the team we're looking at has a datacron filter associated
                if (trackedTeams[teamIndex].datacron_filter || 
                    (trackedTeams[teamIndex].datacrons && (trackedTeams[teamIndex].datacrons.find(id => id != twUtils.NO_DATACRON_SPECIFIED) != null)) || 
                    (trackedTeams[teamIndex].datacrons_required && (trackedTeams[teamIndex].datacrons_required.find(id => id != twUtils.NO_DATACRON_SPECIFIED) != null)))
                {
                    // find all datacrons that have not been assigned on defense for the player and that match ALL abilities specified in datacrons_required
                    let datacronsAvailable = playerData.datacron.filter(
                        d => 
                        {
                            // has it been assigned on defense?
                            if (defenseDataForPlayer?.playerMetadata.datacronsAssigned.indexOf(d.id) >= 0) return false;

                            if (!trackedTeams[teamIndex].datacrons_required) return true;

                            // no, so check if it has the abilities
                            let requiredDatacronIds = trackedTeams[teamIndex].datacrons_required.filter(r => r != constants.NO_DATACRON_SPECIFIED);

                            
                            for (let datacronId of requiredDatacronIds)
                            {
                                if (d.affix?.find(af => af.wookieeBotDatacronId === datacronId) == null) return false;
                            }

                            return true;
                        });
                    
                    // search through datacronsAvailable to see if any are a match for either the "datacrons" or the "datacron_filter"
                    let match = datacronsAvailable?.find(dc => 
                        {
                            let matchingDC = null;
                            
                            
                            // first check if there's a match for specific datacron "types" by wookieeBotDatacronId
                            if (trackedTeams[teamIndex].datacrons)
                            {
                                matchingDC = dc.affix?.find(af => trackedTeams[teamIndex].datacrons.indexOf(af.wookieeBotDatacronId) != -1);
                            }

                            // Then check if there's a match based on the broader filters
                            if (!matchingDC && trackedTeams[teamIndex].datacron_filter)
                            {
                                switch (trackedTeams[teamIndex].datacron_filter)
                                {
                                    case twUtils.DATACRON_FILTERS.AnyL9Match:
                                        matchingDC = trackedTeams[teamIndex].units.find(u => u.defId === dc.meta.character)
                                        break;
                                        
                                    case twUtils.DATACRON_FILTERS.AnyL6Match:
                                        matchingDC = trackedTeams[teamIndex].units.find(u => unitsList.find(ru => swapi.getUnitDefId(ru) === u.defId)?.categoryId.indexOf(dc.meta.faction) != -1);
                                        break;
                                        
                                    case twUtils.DATACRON_FILTERS.AnyL3Match:
                                        matchingDC = trackedTeams[teamIndex].units.find(u => unitsList.find(ru => swapi.getUnitDefId(ru) === u.defId)?.categoryId.indexOf(dc.meta.alignment) != -1);
                                        break;
                                }
                            }

                            return matchingDC != null;
                        });
                    
                    // there's no datacron available that matches the team, so exclude the player
                    if (!match) continue;
                }

                trackedTeams[teamIndex].players.push({ discordId: playerCachedData.discord_id, allycode: playerCachedData.allycode })
            }
        }

        return trackedTeams;
    },
    saveAvailableTeamsToDb: async function (existingManager, teams) {
        // records to be updated:
        var twmData = new Array();

        // declare ColumnSet once, and then reuse it:
        const twmDataCS = new database.pgp.helpers.ColumnSet(
            ['tw_manager_id', 
            'discord_id', 
            'tw_team_name'], 
            {table: 'tw_manager_data'});

        for (var t = 0; t < teams.length; t++)
        {
            if (teams[t].players == null || teams[t].players.length == 0) continue;
            let teamName = teams[t].name;
            let data = teams[t].players.map((p) => {
                return {
                    tw_manager_id: existingManager.tw_manager_id,
                    discord_id: p.discordId,
                    tw_team_name: teamName
                };
            })
            twmData = twmData.concat(data);
        }

        if (twmData.length == 0) return;
        
        const twmDataInsert = database.pgp.helpers.insert(twmData, twmDataCS);
        
        // clear out old TWM data
        await database.db.none("DELETE FROM tw_manager_data WHERE tw_manager_id = $1", [existingManager.tw_manager_id]);
        
        // executing the query:
        await database.db.none(twmDataInsert);
    },
    verifyGuildRegistration: async function(guildId)
    {
        let res = await database.db.any(
            `SELECT COUNT(*) FROM guild_players
            LEFT JOIN user_registration USING (allycode)
            WHERE guild_id = $1 AND discord_id IS NULL`, [guildId]);

        if (res[0].count > 0) return false;

        return true;
    }
}
