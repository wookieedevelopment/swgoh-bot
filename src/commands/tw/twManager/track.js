const { EmbedBuilder } = require("discord.js");
const discordUtils = require("../../../utils/discord.js");
const guildUtils = require("../../../utils/guild");
const playerUtils = require("../../../utils/player");
const database = require("../../../database");
const twUtils = require("../utils.js");
const twManagerUtils = require("./utils.js");
const twManagerRepair = require("./repair");
const { SlashCommandSubcommandBuilder, SlashCommandSubcommandGroupBuilder } = require("@discordjs/builders");

module.exports = {
    data: new SlashCommandSubcommandGroupBuilder()
        .setName("track")
        .setDescription("Manage tracked teams for your TW Manager.")
        .addSubcommand(s =>
            s.setName("start")
            .setDescription("Start tracking a team in your TW Manager. Run reset after to see it.")
            .addStringOption(o =>
                o.setName("team")
                .setDescription("The name of the team to stop tracking")
                .setRequired(true)
                .setAutocomplete(true)))
        .addSubcommand(s => 
            s.setName("stop")
            .setDescription("Stop tracking a team in your TW Manager.")
            .addStringOption(o =>
                o.setName("team")
                .setDescription("The name of the team to stop tracking")
                .setRequired(true)
                .setAutocomplete(true)))
        .addSubcommand(s => 
            s.setName("list")
            .setDescription("List tracked teams.")),
    permissionLevel: playerUtils.UserRoles.GuildAdmin,
    init: async function(interactionOrInput, teamName)
    {
        let retObj = {
            existingManager: null, 
            team: null, 
            guildId: null,
            errorResponse: null
        }

        let { botUser, guildId, error } = await playerUtils.authorize(interactionOrInput, this.permissionLevel)
        if (error) {
            retObj.errorResponse = error;
            return retObj;
        }

        retObj.guildId = guildId;

        const existingManager = await twManagerUtils.getTWManagerByGuildId(guildId);

        if (!existingManager)
        {
            retObj.errorResponse = "You do not have a TW manager installed or it is misconfigured. Install with **install** or reset with **reset**.";
            return retObj;
        }

        retObj.existingManager = existingManager;

        if (!teamName || teamName.length == 0)
        {
            retObj.errorResponse = { embeds: [await this.getTrackedTeamsListEmbed(existingManager.tw_manager_id)] };
            return retObj;
        }
        
        let team = await twUtils.findTeam(guildId, teamName, false);

        if (!team)
        {
            retObj.errorResponse = `You do not have an offense team defined by the name ${teamName}. Please double check with **twm.lt** or add one with **twm.at**.`;
            return retObj;
        }

        retObj.team = team;

        return retObj;
    },
    trackInteraction: async function(interaction)
    {
        let teamName = interaction.options.getString("team").trim().toLowerCase();
        let responseMessage = await this.track(interaction, teamName);
        await interaction.editReply(responseMessage);
    },
    trackExecute: async function(client, input, args)
    {
        let teamName = null;
        try { teamName = args[0]; } catch {}

        let responseMessage = await this.track(input, teamName);
        await input.reply(responseMessage);
    },
    track: async function(inputOrInteraction, teamName) {

        let { existingManager, team, guildId, errorResponse } = await this.init(inputOrInteraction, teamName);
        if (existingManager == null || team == null || guildId == null) {
            return errorResponse ? errorResponse : "Something went wrong. Please try again or contact the bot owner for help.";
        }

        try {
            await database.db.any(
                `INSERT INTO tw_manager_tracked_team (tw_manager_id, tw_team_id)
                VALUES ($1, $2)`, [existingManager.tw_manager_id, team.id]);
        } catch (e)
        {
            return `You are already tracking that team.`;
        }         
        
        let teamType = twManagerUtils.getTeamType(team);
        await twManagerRepair.repairManager(inputOrInteraction.client, guildId, teamType);
        
        return `You are now tracking ${team.name} for TW offense.`;
    },
    untrackInteraction: async function(interaction)
    {
        let teamName = interaction.options.getString("team").trim().toLowerCase();
        let responseMessage = await this.untrack(interaction, teamName);
        await interaction.editReply(responseMessage);
    },
    untrackExecute: async function(client, input, args)
    {
        let teamName = null;
        try { teamName = args[0]; } catch {}

        let responseMessage = await this.untrack(input, teamName);
        await input.reply(responseMessage);
    },
    untrack: async function(inputOrInteraction, teamName)
    {
        let { existingManager, team, guildId, errorResponse } = await this.init(inputOrInteraction, teamName);
        if (existingManager == null || team == null || guildId == null) {
            return errorResponse ? errorResponse : "Something went wrong. Please try again or contact the bot owner for help.";
        }

        let deletedCount = await database.db.result(
            `DELETE FROM tw_manager_tracked_team
            WHERE tw_manager_id = $1 AND tw_team_id = $2`, [existingManager.tw_manager_id, team.id], r => r.rowCount);
        
        if (!deletedCount)
        {
            return `You are not tracking that team.`;
        }         
           
        let teamType = twManagerUtils.getTeamType(team);
        await twManagerRepair.repairManager(inputOrInteraction.client, guildId, teamType);
        
        return `You are no longer tracking ${team.name} for TW offense.`;
    },
    listInteraction: async function(interaction)
    {
        let { botUser, guildId, error } = await playerUtils.authorize(interaction, this.permissionLevel)
        if (error) {
            await interaction.editReply(error);
            return;
        }

        const existingManager = await twManagerUtils.getTWManagerByGuildId(guildId);

        if (!existingManager)
        {
            await interaction.editReply("You do not have a TW manager installed or it is misconfigured. Install with **install** or reset with **reset**.");
            return;
        }

        await interaction.editReply({ embeds: [await this.getTrackedTeamsListEmbed(existingManager.tw_manager_id)] });
    },
    getTrackedTeamsListEmbed : async function(twManagerId) {
        let embed = new EmbedBuilder().setColor(0x229900).setTitle("Tracked TW Offense Teams");

        let teams = await database.db.any(
            `SELECT tw_manager_tracked_team_id, tw_team.tw_team_id, tw_team.name, has_gl, is_squad 
            FROM tw_manager_tracked_team
            LEFT JOIN tw_team USING (tw_team_id)
            WHERE tw_manager_tracked_team.tw_manager_id = $1
			ORDER by tw_team.name ASC`,
            [twManagerId]
        )
        
        const teamNameDisplayMaxLength = 32;
        var glsField = {
            name: "Tracked GL Teams",
            value:
                "```" +   
                "           GL Team Name         \r\n" + 
                "--------------------------------\r\n",
            inline: false
        }
        var squadsField = {
            name: "Tracked Squads",
            value:
                "```" +   
                "            Squad Name          \r\n" + 
                "--------------------------------\r\n",
            inline: false
        };
        var fleetsField = {
            name: "Tracked Fleets",
            value:
                "```" +   
                "            Fleet Name          \r\n" + 
                "--------------------------------\r\n",
            inline: false
        };
        for (var t = 0; t < teams.length; t++)
        {
            let str = teams[t].name.padEnd(teamNameDisplayMaxLength).substring(0, teamNameDisplayMaxLength) + "\r\n";
            if (teams[t].has_gl) glsField.value += str;
            else if (teams[t].is_squad) squadsField.value += str;
            else fleetsField.value += str;
        }
        fleetsField.value += "```";
        glsField.value += "```";
        squadsField.value += "```";

        embed.addFields(glsField, squadsField, fleetsField);

        return embed;
    },
    autocomplete: async function(interaction)
    {
        let { existingManager, error } = await twManagerUtils.verifyManager(interaction, playerUtils.UserRoles.GuildAdmin);        
        if (error)
        {
            await interaction.respond([{ name: "You do not have permission to do this.", value: "wwwwwwww" }])
            return;
        }

        const focusedValue = interaction.options.getFocused();
        let subcommand = interaction.options.getSubcommand();

        let teams;
        if (subcommand == "start")
        {
            // suggest teams that aren't already tracked
            teams = await database.db.any(
`
SELECT name
    FROM tw_team twt
    WHERE guild_id = $1 AND twt.name ILIKE $2 AND is_defense_team = FALSE
    AND NOT EXISTS (SELECT tw_team_id FROM tw_manager_tracked_team WHERE tw_manager_id = $4 AND tw_team_id = twt.tw_team_id)
UNION
SELECT name
    FROM tw_team twt
    WHERE guild_id = $1 AND twt.name ILIKE $3 AND is_defense_team = FALSE
    AND NOT EXISTS (SELECT tw_team_id FROM tw_manager_tracked_team WHERE tw_manager_id = $4 AND tw_team_id = twt.tw_team_id)
LIMIT 25
`, [existingManager.guild_id, `${focusedValue}%`, `_%${focusedValue}%`, existingManager.tw_manager_id]
            )
        } else if (subcommand == "stop")
        {
            // suggest teams that are already tracked
            teams = await database.db.any(
                `
SELECT name
    FROM tw_team twt
    WHERE guild_id = $1 AND twt.name ILIKE $2 AND is_defense_team = FALSE
    AND EXISTS (SELECT tw_team_id FROM tw_manager_tracked_team WHERE tw_manager_id = $4  AND tw_team_id = twt.tw_team_id)
UNION
SELECT name
    FROM tw_team twt
    WHERE guild_id = $1 AND twt.name ILIKE $3 AND is_defense_team = FALSE
    AND EXISTS (SELECT tw_team_id FROM tw_manager_tracked_team WHERE tw_manager_id = $4  AND tw_team_id = twt.tw_team_id)
LIMIT 25
                `, [existingManager.guild_id, `${focusedValue}%`, `_%${focusedValue}%`, existingManager.tw_manager_id])
        }

		await interaction.respond(
			teams
                .filter(t => t.name.length < twManagerUtils.MAX_TEAM_NAME_LENGTH)
                .map(choice => ({ name: choice.name, value: choice.name }))
		);
    }
}
