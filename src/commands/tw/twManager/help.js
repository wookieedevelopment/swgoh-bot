const { SlashCommandSubcommandBuilder } = require("@discordjs/builders");
const { EmbedBuilder } = require("discord.js");
const discordUtils = require("../../../utils/discord");

module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("help")
        .setDescription("Show help for the TW Manager."),
    interact: async function(interaction)
    {
        let overviewField = {
            name: "Overview", 
            value:
`The WookieeBot TW Manager provides a real-time view of your TW offense capabilities with *the simplest* user interface for your guild.

Key features:
   1. Track custom offense teams.
   2. Automatically filter out assigned defenses.
   3. Immediately identify who has used and who has teams remaining.
   4. Notify members with customer messages who have not used a team.
  `,
inline: false
            };

        let adminCommandsField = {
            name: "Admin Commands",
            value:
`**/twmanager install**: install the TW manager
**/twmanager team add**: add a team to your list of possible teams
**/twmanager team delete**: delete a team from your list of teams
**/twmanager team list**: list your teams
**/twmanager test**: see who in your guild meets a team's criteria
**/twmanager team describe**: set description for a team
**/twmanager exclude**: exclude an allycode in your guild
**/twmanager include**: include an excluded allycode
**/twmanager track start**: track a specified offense team
**/twmanager track stop**: stop tracking a team
**/twamanager reset**: reset the manager (when a new offense phase starts, clears data)
**/twmanager repair**: if it seems broken, try this
**/twmanager whohas**: show who has a team 
**/twmanager notify**: send a message to everyone that has not used a specific team`,
            inline: false
        };

        let allUserCommandsField = {
            name: "Guild Member Commands",
            value:
`**/twmanager team list**: list your teams
**/twmanager whohas**: show who has a team 
**/twmanager history**: show team usage history for the latest war
`,
            inline: false
        };

        let embed = new EmbedBuilder()
            .setTitle("WookieeBot TW Planner")
            .setColor(0xD2691E)
            .addFields(overviewField, adminCommandsField, allUserCommandsField);

        await interaction.editReply({ embeds: [embed] });
    }
}
