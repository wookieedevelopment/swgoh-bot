
const database = require("../../../database");
const discordUtils = require("../../../utils/discord.js");
const playerUtils = require ("../../../utils/player")
const swapiUtils = require ("../../../utils/swapi")
const twUtils = require ("../utils")
const { ActionRowBuilder, ButtonBuilder, EmbedBuilder, ButtonStyle } = require('discord.js');
const constants = require("../../../utils/constants");

const undoGLButtonId = "undo_gl";
const undoSquadButtonId = "undo_squad"
const undoFleetButtonId = "undo_fleet"
const teamTypes = {
    GL: 0,
    SQUAD: 1,
    FLEET: 2
}

module.exports = {
    undoGLButtonId: undoGLButtonId,
    undoSquadButtonId: undoSquadButtonId,
    undoFleetButtonId: undoFleetButtonId,
    teamTypes: teamTypes,
    MAX_TEAM_NAME_LENGTH: discordUtils.MAX_DISCORD_AUTOCOMPLETE_CHOICE_LENGTH,
    getTWManagerByChannelId: async function(channelId)
    {
        return await database.db.oneOrNone("SELECT * FROM tw_manager WHERE channel = $1", [channelId]);
    },
    getTWManagerByGuildId: async function(guildId)
    {
        return await database.db.oneOrNone("SELECT * FROM tw_manager WHERE guild_id = $1", [guildId]);        
    },
    verifyManager: async function(input, roles)
    {
        let { botUser, guildId, error } = await playerUtils.authorize(input, roles)
        if (error) {
            return { existingManager: null, error: error };
        }

        let existingManager = await this.getTWManagerByGuildId(guildId);
        if (!existingManager) {
            return { existingManager: null, error: "Your TW Manager is not set up correctly.  Install with twm.install or repair with twm.repair." }
        };

        return { existingManager: existingManager, error: null };
    },
    getTWStatusEmbedAndComponents: async function(twManager, teamType)
    {
        let tmd = await database.db.any(
            `SELECT discord_id, timestamp, tw_team_name, used, tw_manager_data_id 
            FROM tw_manager_data 
            WHERE tw_manager_id = $1
            group by (discord_id, tw_team_name, used, timestamp, tw_manager_data_id)`, 
            [twManager.tw_manager_id]);
        
        let teams = await database.db.any(
            `SELECT tw_manager_tracked_team_id, tw_team.tw_team_id, tw_team.name, tw_team.is_squad, tw_team.has_gl
            FROM tw_manager_tracked_team
            INNER JOIN tw_team USING (tw_team_id)
            WHERE tw_manager_tracked_team.tw_manager_id = $1
            ORDER BY tw_team.has_gl desc, tw_team.is_squad DESC, tw_team.name ASC`,
            [twManager.tw_manager_id]
        )

        const teamNameDisplayMaxLength = 24;
        var glsField = {
            name: "Tracked GL Teams",
            value:
                "```" +   
                "|     GL Team Name      | Left\r\n" + 
                "----------------------- | ----\r\n",
            inline: false
        }
        var squadsField = {
            name: "Other Tracked Squads",
            value:
                "```" +   
                "|      Squad Name       | Left\r\n" + 
                "----------------------- | ----\r\n",
            inline: false
        }
        var fleetsField = {
            name: "Tracked Fleets",
            value:
                "```" +   
                "|      Fleet Name       | Left\r\n" + 
                "----------------------- | ----\r\n",
            inline: false
        }

        let rows = new Array(this.teamTypes.length);
        rows[this.teamTypes.GL] = new Array();
        rows[this.teamTypes.FLEET] = new Array();
        rows[this.teamTypes.SQUAD] = new Array();

        let glCount = 0, squadCount = 0, fleetCount = 0;
        const MAX_TEAM_BUTTONS_PER_MESSAGE = 24;

        for (var t = 0; t < teams.length; t++)
        {   
            let count = tmd.filter(d => d.tw_team_name == teams[t].name && d.used == false).length;
            let team_type_index = null;
            if (teams[t].has_gl)
            {
                if (teamType && teamType != this.teamTypes.GL) continue;
                if (++glCount > MAX_TEAM_BUTTONS_PER_MESSAGE) continue;
                team_type_index = this.teamTypes.GL;
                glsField.value += 
                    teams[t].name.padEnd(teamNameDisplayMaxLength).substring(0, teamNameDisplayMaxLength) + "| " + count.toString() + "\r\n"
            }
            else if (teams[t].is_squad) {
                if (teamType && teamType != this.teamTypes.SQUAD) continue;
                if (++squadCount > MAX_TEAM_BUTTONS_PER_MESSAGE) continue;
                team_type_index = this.teamTypes.SQUAD;
                squadsField.value += 
                    teams[t].name.padEnd(teamNameDisplayMaxLength).substring(0, teamNameDisplayMaxLength) + "| " + count.toString() + "\r\n"
            } else {
                if (teamType && teamType != this.teamTypes.FLEET) continue;
                if (++fleetCount > MAX_TEAM_BUTTONS_PER_MESSAGE) continue;
                team_type_index = this.teamTypes.FLEET;
                fleetsField.value += 
                    teams[t].name.padEnd(teamNameDisplayMaxLength).substring(0, teamNameDisplayMaxLength) + "| " + count.toString() + "\r\n"
            }

            // don't add a button if no one has the team
            if (count == 0) continue;
            
            // need to do all the things below based on which "row_index" it is in the new 3d rows array above

            let button = new ButtonBuilder()
                            .setCustomId(teams[t].tw_team_id.toString())
                            .setLabel(teams[t].name)
                            .setStyle(teams[t].is_squad ? ButtonStyle.Success : ButtonStyle.Primary);

            // no more than 5 buttons per row
            if (rows[team_type_index].length == 0 || rows[team_type_index][rows[team_type_index].length - 1].components.length == 5)
            {
                rows[team_type_index].push(new ActionRowBuilder());
            }

            rows[team_type_index][rows[team_type_index].length - 1].addComponents(button);            
        }

        for (var rowIndex in rows)
        {
            if (rows[rowIndex].length == 0) continue;

            if (rows[rowIndex][rows[rowIndex].length-1].components.length == 5)
                rows[rowIndex].push(new ActionRowBuilder());
            
            let undoButtonId;
            if (rowIndex == 0) undoButtonId = this.undoGLButtonId;
            else if (rowIndex == 1) undoButtonId = this.undoSquadButtonId;
            else undoButtonId = this.undoFleetButtonId;

            // add the undo button
            rows[rowIndex][rows[rowIndex].length-1].addComponents(
                new ButtonBuilder()
                    .setCustomId(undoButtonId)
                    .setLabel("Undo")
                    .setStyle(ButtonStyle.Danger)
            );
        }

        glsField.value += "```"
        squadsField.value += "```";
        fleetsField.value += "```"

        var currentSquadsEmbed = new EmbedBuilder()
            .setTitle("TW Offense: Squads")
            .setColor(0xD2691E)
            .addFields(squadsField);

        var currentGLsEmbed = new EmbedBuilder()
            .setTitle("TW Offense: Galactic Legends")
            .setColor(0xc2A011)
            .addFields(glsField);

        var currentFleetsEmbed = new EmbedBuilder()
            .setTitle("TW Offense: Fleets")
            .setColor(0x0044a8)
            .addFields(fleetsField);

        return {
            glsMessage: { embeds: [currentGLsEmbed], components: rows[this.teamTypes.GL] },
            squadsMessage: { embeds: [currentSquadsEmbed], components: rows[this.teamTypes.SQUAD] },
            fleetsMessage: { embeds: [currentFleetsEmbed], components: rows[this.teamTypes.FLEET] }
        };
    },
    getOffenseTeamSuggestions: async function(existingManager, filter)
    {
        return await database.db.any(`
        SELECT DISTINCT (tw_team_name) FROM
            (SELECT tw_team_name, 1 as sort
            FROM tw_manager_data
            WHERE tw_manager_id = $1 AND tw_team_name ILIKE $2
            UNION
            SELECT tw_team_name, 2 as sort
            FROM tw_manager_data
            WHERE tw_manager_id = $1 AND tw_team_name ILIKE $3
            ORDER BY sort ASC
            ) as foo
            LIMIT 25
        `, [existingManager.tw_manager_id, `${filter}%`, `_%${filter}%`])
    },
    getTeamType: function(team)
    {
        if (team.has_gl || team.hasGL) return this.teamTypes.GL;
        if (team.is_squad || team.isSquad) return this.teamTypes.SQUAD;
        return this.teamTypes.FLEET;
    },
    findUsersWhoHaveTeamWithFilter: async function(existingManager, teamName, filter, onlyUnused)
    {

        if (!filter)
        {
            
            let data = await database.db.any(`
            SELECT DISTINCT discord_id
            FROM tw_manager_data
            WHERE tw_manager_id = $1 AND tw_team_name = $2 AND used = FALSE`, 
            [existingManager.tw_manager_id, teamName]);

            return { team: null, users: data.map(d => d.discord_id) };
        }
        
        let team = await twUtils.parseTeam(filter);

        let caches = await database.db.any(`
        SELECT * FROM (
            SELECT
                ROW_NUMBER() OVER (
                    PARTITION BY allycode 
                    ORDER BY player_cache.timestamp desc
                ) AS r,
                allycode, 
                discord_id,
                data_text
            FROM player_cache
            LEFT JOIN cache_data USING (cache_data_id)
            join guild_players using (allycode)
            left join user_registration using (allycode)
            WHERE player_cache.type = $1 AND guild_players.guild_id = $2 AND user_registration.discord_id IN (
                SELECT DISTINCT discord_id
                FROM tw_manager_data
                WHERE tw_manager_id = $3 AND tw_team_name = $4 ${onlyUnused ? `AND used = FALSE` : ``})
        ) x
        WHERE x.r <= 1`, [constants.CacheTypes.TW, existingManager.guild_id, existingManager.tw_manager_id, teamName]);

        if (caches.length == 0)
        {
            return { team: team, users: null };
        }

        let users = new Array();
        try {
            await Promise.all(
                caches.map(async (cache) =>
                {
                    let playerData = JSON.parse(cache.data_text);
                    
                    if ((await twUtils.isTeamViable(playerData, team)).viable) 
                        users.push(cache.discord_id);
                })
            );
        } catch (err)
        {
            logger.error(formatError(err))
            throw new Error("An error occurred checking the filter. Please try again.");    
        }

        users = [...new Set(users)];

        return { team: team, users: users };
    },
    createFilterString: async function(team)
    {
        if (!team || !team.units || team.units.length == 0) return null;

        let unitsList = await swapiUtils.getUnitsList();
        var unitStrings = team.units.map(unit => {
            let str = unit.name;
            if (unit.features.length > 0 && unit.features[0] != twUtils.features.nullFeature)
            {
                str += " (";
                for (var f = 0; f < unit.features.length; f++)
                {
                    let value = unit.features[f].value;

                    if (isNaN(value))
                    {
                        let unitMatch = unitsList.find(u => swapiUtils.getUnitDefId(u) === value);
                        if (unitMatch) value = unitMatch.name;
                    }
                    
                    str += ` ${twUtils.getLogicString(unit.features[f].logic)} `;
                    str += twUtils.getFeatureString(unit.features[f].key, unit.features[f].comparison, value);
                }
                str += ")";
            }
            return str;
        });

        filterString = unitStrings.join("; ");
        return filterString;
    }
}