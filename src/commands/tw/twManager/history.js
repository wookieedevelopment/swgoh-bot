const { EmbedBuilder } = require("discord.js");
const discordUtils = require("../../../utils/discord.js");
const twManagerUtils = require("./utils.js");
const twManagerRepair = require("./repair.js");
const database = require("../../../database");
const playerUtils = require("../../../utils/player");
const { SlashCommandBuilder, SlashCommandSubcommandBuilder } = require('@discordjs/builders');

module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("history")
        .setDescription("Check yours or a guild-mate's team usage history for the current war.")
        .addUserOption(o => 
            o.setName("user")
            .setDescription("A discord user in your guild to check.")
            .setRequired(false)),
    name: "twm.history",
    description: "List your history of team usage for the war.",
    interact: async function(interaction)
    {
        let { existingManager, error } = await twManagerUtils.verifyManager(interaction, playerUtils.UserRoles.User);      
        if (error)
        {
            await interaction.editReply(error);
            return;
        }

        let user = interaction.options.getUser("user");

        let id = (user != null ? user.id : interaction.user.id)

        let embed = await this.getHistoryEmbed(existingManager, id);
        await interaction.editReply({ embeds: [embed] })
    },
    execute: async function(client, input, args) {
        
        let { existingManager, error } = await twManagerUtils.verifyManager(input, playerUtils.UserRoles.User);
        if (error)
        {
            await input.reply(error);
            return;
        }

        let discordId = input.author.id;
        if (args && args[0])
        {
            discordId = discordUtils.cleanDiscordId(args[0]);
            
            if (!discordUtils.isDiscordId(discordId))
            {
                await input.reply("Use **/twmanager history** to see your own history, or tag another user to see theirs: **/twmanager history user:@discordUser**");
                return;
            }
        }

        let embed = await this.getHistoryEmbed(existingManager, discordId);

        await input.reply({embeds: [embed]});
    },
    getHistoryEmbed: async function(existingManager, discordId)
    {

        let data = await database.db.any(`
            SELECT tw_manager_data.tw_team_name, timestamp, used, has_gl, is_squad
            FROM tw_manager_data
            LEFT JOIN tw_team ON tw_team.guild_id = (SELECT guild_id FROM tw_manager WHERE tw_manager_id = $1) AND tw_manager_data.tw_team_name = tw_team.name AND tw_team.is_defense_team = FALSE
            WHERE tw_manager_id = $1 AND discord_id = $2
            ORDER BY has_gl DESC, is_squad DESC, tw_team_name ASC`, 
            [existingManager.tw_manager_id, discordId]);

        let embed = new EmbedBuilder().setTitle(`TW Offense History`)
        if (data.length == 0)
        {
            embed.setDescription(`No teams tracked for ${discordUtils.makeIdTaggable(discordId)}.`);
            return embed;
        }

        let usedFields = [{ name: `Used:`, value: "", inline: false }]
        let unusedFields = [{ name: `Available:`, value: "", inline: false }]

        let curUsedField = usedFields[0];
        let curUnusedField = unusedFields[0];

        for (let t = 0; t < data.length; t++)
        {
            let teamName = data[t].tw_team_name;
            if (data[t].used)
            {
                let str = `${teamName}: ${data[t].timestamp.toLocaleString("en-us", {
                    year: 'numeric',
                    month: 'short',
                    day: 'numeric',
                    hour: 'numeric',
                    minute: 'numeric',
                    timeZone: 'utc'
                  })}\r\n`;

                  if (curUsedField.value.length + str.length >= 1000)
                  {
                      if (usedFields.length == 3) continue;
                      curUsedField = { name: `Used (continued):`, value: str, inline: false };
                      usedFields.push(curUsedField);
                  } else {
                      curUsedField.value += str;
                  }
            }
            else
            {
                let str = `${teamName}\r\n`;

                if (curUnusedField.value.length + str.length >= 1000)
                {
                    if (unusedFields.length == 3) continue;
                    curUnusedField = { name: `Available (continued):`, value: str, inline: false };
                    unusedFields.push(curUnusedField);
                } else {
                    curUnusedField.value += str;
                }
            }
        }

        if (usedFields[0].value.length == 0) usedFields[0].value = "None"
        if (unusedFields[0].value.length == 0) unusedFields[0].value = "None"

        let footnote = "All times in UTC"

        embed.setFooter({ text: footnote });

        embed.addFields(usedFields);
        embed.addFields(unusedFields);
        
        return embed;
    }
}
