const { EmbedBuilder, Message } = require("discord.js");
const discordUtils = require("../../../utils/discord.js");
const twManagerUtils = require("./utils.js");
const twManagerRepair = require("./repair.js");
const database = require("../../../database");
const playerUtils = require("../../../utils/player");
const { SlashCommandSubcommandBuilder } = require("@discordjs/builders");

/*
Workflow:
find all allycodes in the guild that are registered for the tw
find the discord IDs of those allycodes
find all the "tracked" teams (up to 26?) for the TW
display in the manager all tracked teams and number available with reactions available
when a discord user clicks on a reaction, increment the "used" by one until a maximum of the number of allycodes associated with the discordID are used.  Then loop back to zero.
in the display, automatically adjust the number of that team available 

twm.whohas teamname -- checks which discord IDs have a team available still
twm.track teamname -- add a team to be tracked
twm.track -- list all tracked teams
twm.untrack teamname -- stop tracking a teamname
*/

module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("install")
        .setDescription("Install the WookieeBot TW Manager.")
        .addChannelOption(o => 
            o.setName("channel")
            .setDescription("The dedicated channel to use for the TW Manager. No other messages should be sent here.")
            .setRequired(true)),
    permissionLevel: playerUtils.UserRoles.GuildAdmin,
    interact: async function(interaction)
    {
        let channel = interaction.options.getChannel("channel");
        
        let { botUser, guildId, error } = await playerUtils.authorize(interaction, this.permissionLevel)
        if (error) {
            await interaction.editReply({embeds: [discordUtils.createErrorEmbed("Error", error)]});
            return;
        }

        let reply = await this.install(channel, guildId);
        await interaction.editReply(reply)
    },
    execute: async function(client, input, args) {
        let { botUser, guildId, error } = await playerUtils.authorize(input, this.permissionLevel)
        if (error) {
            await input.channel.send(error);
            return;
        }

        if (!args || args.length == 0)
        {
            await input.channel.send("Please provide a dedicated TW channel that I can read.");
            return;
        }

        let channelId = discordUtils.cleanDiscordId(args[0]);

        let managerChannel = client.channels.cache.get(channelId);
        
        if (!managerChannel) {
            await input.channel.send("You cannot do that to a channel that doesn't exist or is in another server.");
            return;
        }

        let reply = await this.install(managerChannel, guildId);
        await input.reply(reply);
    },
    install: async function(channel, guildId)
    {
        // check if channel is linked to another guild
        let guildLink = await database.db.oneOrNone("SELECT guild_id FROM guild_discord_link WHERE discord_link = $1", channel.id);
        if (guildLink && guildLink.guild_id != guildId)
        {
            let embed = discordUtils.createErrorEmbed("Error", `Channel is already registered to another guild.  Choose a channel that has not been registered to another guild.`);

            return { embeds: [embed] };
        }

        // check if the manager is already installed in the channel
        let existingManager = await twManagerUtils.getTWManagerByChannelId(channel.id);

        if (existingManager)
        {
            let embed = discordUtils.createErrorEmbed("Error", "TW Manager is already installed there. If you need to reset it, use **twmanager reset**.");

            return { embeds: [embed] };
        }

        try {
            await channel.send("Testing access. You can delete this message now.");
        } catch (error) {
            let embed = discordUtils.createErrorEmbed("Error", "Not a valid channel or I cannot read from it. Please try again.\r\n\r\nDetails:\r\n" + error.message);

            return { embeds: [embed] };
        }

        try {
            // register tw manager and also add guild discord link
            await database.db.none("INSERT INTO tw_manager (channel, guild_id) VALUES ($1, $2) ON CONFLICT (guild_id) DO UPDATE SET channel = $1; INSERT INTO guild_discord_link (guild_id, discord_link, link_type) VALUES ($2, $1, $3) ON CONFLICT (discord_link) DO NOTHING", [channel.id, guildId, 'channel']);
        } catch (e)
        {
            let embed = discordUtils.createErrorEmbed("Error", "You already have a TW Manager installed for your guild or in this channel.");

            return { embeds: [embed] };
        }

        
        existingManager = await twManagerUtils.getTWManagerByGuildId(guildId);

        await twManagerRepair.refreshMessage(existingManager, channel);

        return `TW manager installed in ${discordUtils.makeChannelTaggable(channel.id)}`;
    }
}
