const { EmbedBuilder } = require("discord.js");
const discordUtils = require("../../../utils/discord.js");
const twManagerUtils = require("./utils.js");
const database = require("../../../database");
const playerUtils = require("../../../utils/player");
const { SlashCommandSubcommandBuilder } = require("@discordjs/builders");

module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("repair")
        .setDescription("Repair your WookieeBot TW Manager if manager isn't working properly. Does not reset data."),
    permissionLevel: playerUtils.UserRoles.GuildAdmin,
    interact: async function(interaction) {
        let { botUser, guildId, error } = await playerUtils.authorize(interaction, this.permissionLevel)
        if (error) {
            await interaction.editReply({embeds: [discordUtils.createErrorEmbed("Error", error)]});
            return;
        }
        
        let responseMessage = await this.repairManager(interaction.client, guildId);
        
        await interaction.editReply(responseMessage);
    },
    execute: async function(client, input, args) {
        let { botUser, guildId, error } = await playerUtils.authorize(input, this.permissionLevel)
        if (error) {
            await input.channel.send(error);
            return;
        }

        let responseMessage = await this.repairManager(client, guildId);

        await input.channel.send(responseMessage);
    },
    repairManager: async function(client, guildId, teamType)
    {
        if (!guildId) {
            return "Invalid guild for TW Manager.";
        }

        let existingManager = await twManagerUtils.getTWManagerByGuildId(guildId);

        if (!existingManager) {
            return `You do not have a TW manager registered to your guild. Make sure your guild is registered in this channel/server, and install TW manager with install.`;
        }

        var managerChannel = client.channels.cache.get(existingManager.channel);
        
        if (!managerChannel) {
            return "You cannot do that to a channel that doesn't exist or is in another server.";
        }

        await this.refreshMessage(existingManager, managerChannel, teamType);

        return "Manager repaired in " + discordUtils.makeChannelTaggable(existingManager.channel);
    },
    refreshMessage: async function(existingManager, managerChannel, teamType)
    {
        let { glsMessage, squadsMessage, fleetsMessage } = await twManagerUtils.getTWStatusEmbedAndComponents(existingManager);
        let snowflake1 = existingManager.static_message_snowflake, 
            snowflake2 = existingManager.static_message_snowflake_2, 
            snowflake3 = existingManager.static_message_snowflake_3;

        let teamTypeArray = null;
        if (teamType != null && teamType instanceof Array) teamTypeArray = teamType;
        else if (teamType != null) teamTypeArray = [teamType];

        if (!teamTypeArray || teamTypeArray.indexOf(twManagerUtils.teamTypes.GL) >= 0)
            snowflake1 = await this.refreshMessageBySnowflake(managerChannel, existingManager.static_message_snowflake, glsMessage);
        if (!teamTypeArray || teamTypeArray.indexOf(twManagerUtils.teamTypes.SQUAD) >= 0)
            snowflake2 = await this.refreshMessageBySnowflake(managerChannel, existingManager.static_message_snowflake_2, squadsMessage);
        if (!teamTypeArray || teamTypeArray.indexOf(twManagerUtils.teamTypes.FLEET) >= 0)
            snowflake3 = await this.refreshMessageBySnowflake(managerChannel, existingManager.static_message_snowflake_3, fleetsMessage);

        if (snowflake1 != existingManager.static_message_snowflake || 
            snowflake2 != existingManager.static_message_snowflake_2 ||
            snowflake3 != existingManager.static_message_snowflake_3)
        {
            // update database with new snowflakes, if any
            await database.db.none(
                `UPDATE tw_manager 
                SET static_message_snowflake = $1, static_message_snowflake_2 = $2, static_message_snowflake_3 = $3 
                WHERE tw_manager_id = $4`, [snowflake1, snowflake2, snowflake3, existingManager.tw_manager_id]);
        }
    },
    refreshMessageBySnowflake: async function(managerChannel, snowflake, message)
    {
        if (snowflake)
        {
            try {
                (await managerChannel.messages.fetch({ message: snowflake })).edit(message);
            } catch
            {
                try {
                    (await managerChannel.messages.fetch({ message: snowflake })).delete();
                } catch {}

                // set up condition to make a new one
                snowflake = null;
            }
        }

        if (snowflake == null)
        {
            const statusMessage = await managerChannel.send(message); 
            return statusMessage.id;
        }

        return snowflake;
    }
}
