const discordUtils = require("../../utils/discord.js");
const twUtils = require("./utils.js");
const guildUtils = require("../../utils/guild");
const playerUtils = require("../../utils/player");
const database = require("../../database");
const showPlan = require("./showPlan");
const { SlashCommandSubcommandBuilder } = require("@discordjs/builders");
const { ActionRowBuilder, ButtonBuilder, EmbedBuilder } = require('discord.js');
const { logger, formatError } = require("../../utils/log.js");

module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("list")
        .setDescription("List saved TW defense plans."),
    permissionLevel: playerUtils.UserRoles.GuildAdmin,
    interact: async function(interaction)
    {
        let { botUser, guildId, error } = await playerUtils.authorize(interaction, this.permissionLevel)
        if (error) {
            await interaction.editReply(error);
            return;
        }

        let response = await this.getPlansList(guildId);
        await interaction.editReply(response);
    },
    getPlansList: async function(guildId)
    {
        let plans = await database.db.any(
`SELECT DISTINCT plan_name
FROM tw_plan_saved
WHERE guild_id = $1`, [guildId]
        )

        if (plans.length == 0) return "You do not have any saved TW defense plans. Create one and save with **/tw plan save**";

        return "Your saved TW defense plans:\r\n" + plans.map((p, ix) => `${(ix + 1).toString()}) ${p.plan_name}`).join("\r\n")
    }
}
