const { EmbedBuilder } = require("discord.js");
const discordUtils = require("../../utils/discord.js");
const twUtils = require("./utils.js");
const guildUtils = require("../../utils/guild");
const playerUtils = require("../../utils/player");
const database = require("../../database");
const swapi = require("../../utils/swapi.js");
const { logger, formatError } = require("../../utils/log");
const { SlashCommandSubcommandBuilder } = require("@discordjs/builders");
const cacheUtils = require("../../utils/cache.js");
const constants = require("../../utils/constants.js");
const twPlayerCommon = require("./player/common.js");

const numTerritories = 10;

module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("run")
        .setDescription("Create a defense plan for the war."),
    name: "tw.runplan",
    aliases: ["war.runplan", "tw.run", "war.run"],
    description: "Create a plan for the war.",
    permissionLevel: playerUtils.UserRoles.GuildAdmin,
    interact: async function(interaction)
    {
        let { botUser, guildId, error } = await playerUtils.authorize(interaction, this.permissionLevel)
        if (error) {
            await interaction.editReply(error);
            return;
        }

        await interaction.editReply("Running TW Planner. This may take a few minutes, and this message will update periodically.");

        let messagesToSend = await this.runPlan(guildId, async (m) => { await interaction.editReply(m); })

        for (let m in messagesToSend)
        {
            await interaction.followUp(messagesToSend[m]);
        }
    },
    execute: async function (client, input, args) {
        await input.channel.send("Please use the slash command **/tw plan run**.");
        return;
    },
    runPlan: async function(guildId, updateStatusMessage)
    {
        // make sure all members are loaded
        let guild = await guildUtils.getGuildDataFromAPI({ wookieeBotGuildId: guildId }, { useCache: false });
        await guildUtils.refreshGuildPlayers(guild);


        var guildPlayers = await guildUtils.getGuildIdAndAllyCodesForGuildByGuildId(guildId);
        if (!guildPlayers || !guildPlayers.allycodes || guildPlayers.allycodes.length === 0)
        {
            return ["Error: Failed to load guild data. Is your guild properly registered?"];
        }
        
        var playerAssignments = {};
        var teams = await twUtils.getTeamsList(guildId, true);

        let msg = this.getTeamsMissingDescriptionMessage(teams);
        if (msg)
        {
            let embed = discordUtils.createErrorEmbed("Teams Missing Description", msg);
            return [{embeds: [embed]}];
        }

        var guildWarMetadata = await this.getGuildWarMetadata(guildId);
        var territories = await this.getTerritories(guildWarMetadata);
        var exclusions = await this.getExclusions(guildId);
        let playerPriorityData = await this.getPlayerPriorityData(guildId);

        let invalidPlanItems = this.validatePlan(territories, teams);
        if (invalidPlanItems.length > 0)
        {
            let msg = this.getInvalidPlanItemsMessage(invalidPlanItems);
            return [{ embeds: [ discordUtils.createErrorEmbed("Invalid Plan", msg) ]}];
        }

        // results of a pass, so that we can do it more than once
        var results = [];
        var cacheTimes = {};
        var cacheTypes = {};
        let newlyCachedPlayers = 0;
        let timestamp = new Date();

        // remove players that are not in the war or that are offense-only
        var participatingAllyCodes = guildPlayers.allycodes.filter(a => exclusions.indexOf(a) == -1);

        // sort by defense priority
        participatingAllyCodes.sort((a, b) => 
        {
            let aPriority = playerPriorityData.find(p => p.allycode == a)?.priority ?? twPlayerCommon.DEFAULT_PRIORITY;
            let bPriority = playerPriorityData.find(p => p.allycode == b)?.priority ?? twPlayerCommon.DEFAULT_PRIORITY;
            return aPriority - bPriority;
        });

        let offenseOnlyPlayerCount = 0;

        var metadata = await this.populatePlayerMetadata(participatingAllyCodes, guildWarMetadata, playerPriorityData);

        await updateStatusMessage("Loaded all data. Calculating assignments.");
        for (let acIx in participatingAllyCodes) {
            let allycode = participatingAllyCodes[acIx];

            let player = { allycode: allycode };
            let playerMetadata = metadata[allycode];

            let playerData = null;

            // grab the latest cache
            const cache = await database.db.oneOrNone(
                `SELECT player_cache_id, data_text, player_cache.timestamp, type
                FROM player_cache 
                LEFT JOIN cache_data USING (cache_data_id)
                WHERE allycode = $1 AND guild_id = $2 AND type IN ($3:csv) AND player_cache.timestamp > (now() - INTERVAL '48 hours')
                ORDER BY player_cache.timestamp DESC LIMIT 1`, 
                [allycode, guildId, [constants.CacheTypes.TW, constants.CacheTypes.MANUAL, constants.CacheTypes.NIGHTLY]])

            if (cache)
            {
                playerData = JSON.parse(cache.data_text);
                cacheTimes[new Date(cache.timestamp.getFullYear(), cache.timestamp.getMonth(), cache.timestamp.getDate(), cache.timestamp.getHours())] = true;

                if (!cacheTypes[cache.type]) cacheTypes[cache.type] = 1;
                else cacheTypes[cache.type]++;

                if (cache.type != constants.CacheTypes.TW)
                {
                    await database.db.any(`
                    INSERT INTO player_cache(allycode, cache_data_id, timestamp, guild_id, comlink_guild_id, type)
                        SELECT allycode, 
                            cache_data_id, 
                            timestamp,
                            guild_id,
                            comlink_guild_id,
                            $1
                        FROM player_cache WHERE player_cache_id = $2
                    ON CONFLICT (allycode, type)
                    DO UPDATE SET cache_data_id = EXCLUDED.cache_data_id, timestamp = EXCLUDED.timestamp`,
                        [constants.CacheTypes.TW, cache.player_cache_id]);
                }
            }
            if (!playerData) 
            {
                try {
                    playerData = await swapi.getPlayer(player.allycode);

                    await cacheUtils.cachePlayerDataInDb(playerData, timestamp, constants.CacheTypes.TW);

                    newlyCachedPlayers++;
                }
                catch (error) {
                    return ["Error: something went wrong getting a player: " + error];
                }
            }

            // store for later, since we'll discard playerData when the iteration of the loop ends
            player.name = swapi.getPlayerName(playerData);

            // viableTeams is a list of viable teams found for the player
            // unitsAssigned holds the full list of DefIds that have already been assigned for the player
            playerAssignments[player.allycode] =
            {
                viableTeams: [],
                availableDatacrons: playerData.datacron,
                unitsAssigned: [],
                datacronsAssigned: []
            };

            // sort by lowest level to highest level so that selection chooses the cheapest one that matches the criteria
            playerAssignments[player.allycode].availableDatacrons.sort((a, b) => a.affix.length - b.affix.length);

            try {
                await this.catalogViableTeams(playerData, playerAssignments, teams);
            } catch (err)
            {
                let embed = discordUtils.createErrorEmbed("Error", `Failed to catalog viable teams for ${player.name}. Please try again. If this continues to fail, contact the developer.`);
                logger.error(formatError(err));
                return [{ embeds: [embed] }];
            }

            // assignments for a player by territory
            let territoryAssignments = new Array(territories.length);
            for (var i = 0; i < territoryAssignments.length; i++) {
                territoryAssignments[i] =
                {
                    foundTeamsCount: 0,
                    foundTeamsString: "",
                    foundTeamsDescriptionsString: "",
                    foundTeams: new Array()
                };
            }

            // reserve teams for offense
            await this.reserveOffenseAssignments(player, playerAssignments, playerMetadata, territories[twUtils.RESERVED_TERRITORY], territoryAssignments);

            // only determine assignments for defense territories for players that are not offense-only
            if (playerMetadata.offenseOnly)
            {
                offenseOnlyPlayerCount++;
            } 
            else 
            {
                // Loop through territories and figure out what to assign    
                await this.determineAssignments(player, playerAssignments, playerMetadata, territories, territoryAssignments, 0);
            }

            results.push({
                player: player,
                playerMetadata: playerMetadata,
                territoryAssignments: territoryAssignments
            });

            if (results.length % 10 == 0)
                await updateStatusMessage("Calculated " + results.length + "/" + participatingAllyCodes.length + " top priority assignments.");
        }

        await updateStatusMessage("Calculating additional assignments.");
        for (var p = 1; p < twUtils.MAX_PRIORITY; p++)
        {
            // additional passes at assignments of lower priorities
            await this.doSecondaryAssignments(results, playerAssignments, territories, playerPriorityData, p);
        }

        await this.saveResults(guildId, results, teams, territories);

        var message = `Done planning assignments for ${results.length} players.\n\n`;

        let cacheTimesKeys = Object.keys(cacheTimes);
        if (cacheTimesKeys.length > 0)
        {
            message += "Used caches from:\n- " + cacheTimesKeys.join("\n- ") + "\n";
            let cacheTypeKeys = Object.keys(cacheTypes);
            for (let ct in cacheTypeKeys)
            {
                let key = cacheTypeKeys[ct];
                message += `Used ${key.toLowerCase()} cached player data for ${cacheTypes[key]} players.\n` 
            }
        }
        if (newlyCachedPlayers > 0)
            message += `Cached ${newlyCachedPlayers} additional rosters.\n`;
        
        message += 
`Offense-only players (\`/tw player list\`): ${offenseOnlyPlayerCount}

Here are your results. If all looks good, run **/tw send**. Otherwise, make changes and re-run. Good luck!`;

        await updateStatusMessage(message);

        return twUtils.generatePlanResults(results, teams, territories);
    },
    getTeamsMissingDescriptionMessage: function(teams)
    {
        let teamsWithoutDescription = teams.filter(t => t.description == null || t.length == 0);
        if (teamsWithoutDescription.length > 0)
        {
            let msg = "The following teams are missing a description. Please set a description with **/tw team describe** before running the plan.\r\n";
            for (let teamIx = 0; teamIx < teamsWithoutDescription.length; teamIx++)
            {
                let team = teamsWithoutDescription[teamIx];
                msg += `${teamIx+1}. ${team.name}\r\n`
            }
            return msg;
        }
        return null;
    },
    validatePlan: function(territories, teams)
    {
        let invalidPlanItems = new Array();

        for (let t = 0; t < territories.length; t++)
        {
            for (let n = 0; n < territories[t].teamNamesToAssign.length; n++)
            {
                for (let tnta = 0; tnta < territories[t].teamNamesToAssign[n].length; tnta++)
                {
                    let teamName = territories[t].teamNamesToAssign[n][tnta].teamName;
                    if (!teams.find(t => t.name === teamName))
                    {
                        // invalid plan item found
                        invalidPlanItems.push(
                            {
                                teamName: teamName,
                                territoryName: territories[t].territoryName
                            }
                        )
                    }
                }
            }
        }

        return invalidPlanItems;
    },
    getInvalidPlanItemsMessage: function(invalidPlanItems)
    {
        let msg = "The following teams are in your plan but do not exist in your list of teams:\n" + 
                    invalidPlanItems.map(p => `* **${p.territoryName == twUtils.getTerritoryNameByNumber(twUtils.RESERVED_TERRITORY) ? twUtils.getTerritoryNameByNumber(twUtils.RESERVED_TERRITORY) : `Territory ${p.territoryName}`}:** ${p.teamName}` ).join("\n");

        return msg;
    },
    getExclusions: async function(guildId)
    {
        return await database.db.map("SELECT allycode FROM tw_exclusion WHERE guild_id = $1", [guildId], r => r.allycode);
    },
    getPlayerPriorityData: async function(guildId)
    {
        return await database.db.any("SELECT allycode, type, priority, max_defense_squads FROM tw_player_priority WHERE guild_id = $1", [guildId]);
    },
    saveResults : async function(guildId, results, teams, territories)
    {
        var resultsText = JSON.stringify(results);
        var teamsText = JSON.stringify(teams);
        var territoriesText = JSON.stringify(territories);

        await database.db.none(`
        INSERT INTO tw_plan_result (guild_id, result_json, teams_json, territories_json) VALUES ($1, $2, $3, $4) 
        ON CONFLICT (guild_id)
        DO UPDATE SET result_json = $2, teams_json = $3, territories_json = $4`,
        [guildId, resultsText, teamsText, territoriesText]);
    },
    getGuildWarMetadata: async function(guildId)
    {
        var metadata = {
            maxTeamsPerTerritory: null,
            defaultMaxSquadsPerPlayer: null,
            defaultMaxFleetsPerPlayer: null,
            guildId: guildId
        };
        var data = await database.db.any("select max_teams_per_territory,default_max_squads_per_player,default_max_fleets_per_player from tw_metadata WHERE guild_id = $1", [guildId]);
        if (data.length == 0)
        {
            data = await database.db.any("insert into tw_metadata (guild_id) values ($1) RETURNING *", [guildId]);
        }

        metadata.maxTeamsPerTerritory = data[0].max_teams_per_territory;
        metadata.defaultMaxFleetsPerPlayer = data[0].default_max_fleets_per_player;
        metadata.defaultMaxSquadsPerPlayer = data[0].default_max_squads_per_player;

        return metadata;
    },
    getTerritories: async function (guildWarMetadata) {
        var territories = new Array();

        // handle the reserved teams separately
        territories.push(
            {
                territoryName: twUtils.getTerritoryNameByNumber(twUtils.RESERVED_TERRITORY),
                isSquad: true,
                isFleet: true,
                maxTeams: 9999999,
                curTeams: new Array(),
                teamNamesToAssign: new Array()
            }
        )

        for (var p = 0; p < twUtils.MAX_PRIORITY; p++) territories[0].teamNamesToAssign.push(new Array());

        // Capture territory metadata
        for (var i = 1; i <= numTerritories; i++) {
            var territory = {};
            territory.territoryName = twUtils.getTerritoryNameByNumber(i);
            if (twUtils.isFleetTerritory(i)) { territory.isSquad = false; territory.isFleet = true; }
            else { territory.isSquad = true; territory.isFleet = false; }

            territory.maxTeams = guildWarMetadata.maxTeamsPerTerritory;
            territory.curTeams = new Array(); // will be assigned later during assignments phase

            territory.teamNamesToAssign = new Array();
            for (var p = 0; p < twUtils.MAX_PRIORITY; p++) territory.teamNamesToAssign.push(new Array());

            territories.push(territory);
        }        

        // populate teams
        const plan = await database.db.any("SELECT * FROM tw_plan WHERE guild_id = $1", [guildWarMetadata.guildId]);
        for (var p in plan)
        {
            var planItem = plan[p];
            var territoryIx = planItem.territory;

            territories[territoryIx].teamNamesToAssign[planItem.priority-1].push(
                { 
                    teamName: planItem.team_name, 
                    limit: planItem.team_limit,
                    maxDefensePriority: planItem.max_defense_priority,
                    minDefensePriority: planItem.min_defense_priority,
                    forAllycode: planItem.for_allycode
                });
        }

        return territories;
    },
    populatePlayerMetadata: async function (allycodes, guildWarMetadata, playerPriorityData) {
        var metadata = {};
        for (var acIx in allycodes) {
            let player = {};
            player.allycode = allycodes[acIx];
            player.maxFleets = guildWarMetadata.defaultMaxFleetsPerPlayer;
            player.numSquadsAssigned = 0;
            player.numFleetsAssigned = 0;

            let priorityData = playerPriorityData.find(p => p.allycode == player.allycode);
            player.offenseOnly = (priorityData?.type == twPlayerCommon.TYPES.OFFENSE);
            player.defensePriority = priorityData?.priority ?? twPlayerCommon.DEFAULT_PRIORITY;
            
            player.maxSquads = ((priorityData?.max_defense_squads ?? -1) >= 0) ? priorityData.max_defense_squads : guildWarMetadata.defaultMaxSquadsPerPlayer;

            metadata[player.allycode] = player;
        }
        return metadata;
    },
    isTerritoryFull: function (territory) {
        return territory.curTeams.length >= territory.maxTeams;
    },
    createAssignmentsString: function (territoryAssignment) {
        return (!territoryAssignment || territoryAssignment.foundTeamsCount == 0)
            ? ""
            : territoryAssignment.foundTeamsCount + " (" + territoryAssignment.foundTeamsString + ")";
    },
    catalogViableTeams: async function (playerData, playerAssignments, teams) {
        // loop through all teams.  First determine if the team is available to the player (units haven't already been assigned)
        // Then determine if the team is viable for the player (characters in roster, gear/zetas are sufficient)
        for (var teamIndex in teams) {
            var team = teams[teamIndex];

            const { viable, reason, boundUnitDefIds } = await twUtils.isTeamViable(playerData, team)
            if (viable) {
                // capture that the team is viable
                playerAssignments[swapi.getPlayerAllycode(playerData)].viableTeams.push({ team: team, boundUnitDefIds: boundUnitDefIds });
            }
        }
    },
    isTeamAvailableForPlayer: function (assignments, team) {
        // loop through each assigned unit.  If the team has that unit bound for that player,
        // then the team cannot be assigned to the player
        for (var assignmentIndex in assignments) {
            let unit = team.boundUnitDefIds.find(u => u == assignments[assignmentIndex]);
            if (!unit) continue;
            return false;
        }

        // player hasn't been assigned any units from the team, so the team is available
        return true;
    },
    findAvailableDatacronForTeam: async function (availableDatacrons, team) {
        let datacronMatch = null, datacronMatchIndex = -1;
        let affixesForDisplay = [];

        if (!availableDatacrons || availableDatacrons.length === 0) return { datacronIndex: datacronMatchIndex, datacronString: this.generateDatacronString(datacronMatch) };

        let viableAvailableDatacrons = availableDatacrons;

        let datacronsRequiredSpecifiedOnly = team.datacronsRequired?.filter(r => r != twUtils.NO_DATACRON_SPECIFIED);

        // first, limit the set of possible datacrons to search on to only those that match ALL abilities specified in datacronsRequired
        if (datacronsRequiredSpecifiedOnly && datacronsRequiredSpecifiedOnly.length > 0)
        {
            // look for a datacron with an affix where the wookieeBotDatacronId of the affix is found within team.datacronsRequired
            viableAvailableDatacrons = availableDatacrons.filter(dc => {
                for (let datacronId of datacronsRequiredSpecifiedOnly)
                {
                    let ix = dc.affix?.findIndex(af => af.wookieeBotDatacronId === datacronId);
                    if (ix === null || ix === -1) return false;
                    if (affixesForDisplay.indexOf(ix) === -1) affixesForDisplay.push(ix);
                }
                return true;
            });
        }

        if (viableAvailableDatacrons.length === 0) return { datacronIndex: datacronMatchIndex, datacronString: this.generateDatacronString(datacronMatch) };

        let optionalDatacronSpecified = false;
        for (var datacronId of team.datacrons)
        {
            if (datacronId === twUtils.NO_DATACRON_SPECIFIED) continue;

            optionalDatacronSpecified = true;
            for (let vad of viableAvailableDatacrons)
            {
                let ix = vad.affix?.findIndex(af => af.wookieeBotDatacronId === datacronId);
                if (ix === null || ix === -1) continue;

                if (affixesForDisplay.indexOf(ix) === -1) affixesForDisplay.push(ix);
                datacronMatch = vad;
                break;
            }

            // found a matching DC so stop looking
            if (datacronMatch) break;
        }

        if (!datacronMatch && team.datacronFilter)
        {
            let unitsList = await swapi.getUnitsList();
            switch (team.datacronFilter)
            {
                case twUtils.DATACRON_FILTERS.AnyL9Match:
                    datacronMatch = viableAvailableDatacrons.find(d => team.units.find(u => u.defId === d.meta.character))
                    affixesForDisplay.push(8);
                    break;
                    
                case twUtils.DATACRON_FILTERS.AnyL6Match:
                    datacronMatch = viableAvailableDatacrons.find(d => team.units.find(u => unitsList.find(ru => swapi.getUnitDefId(ru) === u.defId)?.categoryId.indexOf(d.meta.faction) != -1));
                    affixesForDisplay.push(5);
                    break;
                    
                case twUtils.DATACRON_FILTERS.AnyL3Match:
                    datacronMatch = viableAvailableDatacrons.find(d => team.units.find(u => unitsList.find(ru => swapi.getUnitDefId(ru) === u.defId)?.categoryId.indexOf(d.meta.alignment) != -1));
                    affixesForDisplay.push(2);
                    break;

            }
        }

        // possible matches still, so choose the first
        if (!datacronMatch 
            && viableAvailableDatacrons.length > 0
            && !optionalDatacronSpecified
            && !team.datacronFilter) 
        {
            // choose the lowest level matching datacron - they were sorted when the player objects were loaded
            datacronMatch = viableAvailableDatacrons[0];
        }

        if (datacronMatch) datacronMatchIndex = availableDatacrons.indexOf(datacronMatch);

        return { datacronIndex: datacronMatchIndex, datacronString: this.generateDatacronString(datacronMatch, affixesForDisplay) };
    },
    generateDatacronString: function(datacron, affixesForDisplay)
    {
        if (datacron == null || datacron.affix == null || datacron.affix.length < 3) return "None specified.";

        let str = ``;
        for (let afIx = datacron.affix.length - 1; afIx >= 0; afIx--) {
            if (datacron.affix[afIx].short_description && (!affixesForDisplay || affixesForDisplay.indexOf(afIx) >= 0))
            {
                if (str.length > 0) str += ` & `;
                str += `L${afIx+1}: ${datacron.affix[afIx].short_description}`;
            }
        }

        return str;
    },
    reserveOffenseAssignments: async function(player, playerAssignments, metadata, reservedTerritory, territoryAssignments)
    {
        // don't waste time if there are no viable teams left for the player
        if (playerAssignments[player.allycode].viableTeams.length == 0) return;

        
        let foundTeamsCount = 0;
        let foundTeams = new Array();
        let foundTeamsString = "";
        let foundTeamsDescriptionsString = "";

        // loop through all teams reserved for offense in priority order
        for (let priority = 0; priority < reservedTerritory.teamNamesToAssign.length; priority++)
        {
            let teamsAtPriority = reservedTerritory.teamNamesToAssign[priority];

            // all teams at that priority
            for (let teamNameIndex = 0; teamNameIndex < teamsAtPriority.length; teamNameIndex++)
            {
                let teamName = teamsAtPriority[teamNameIndex].teamName;
                let limit = teamsAtPriority[teamNameIndex].limit;
                let minDefensePriority = teamsAtPriority[teamNameIndex].minDefensePriority;
                const forAllycode = teamsAtPriority[teamNameIndex].forAllycode;

                if (forAllycode && player.allycode != forAllycode) continue; // player isn't a match for this assignment

                if (limit && reservedTerritory.curTeams.filter(t => t === teamName).length >= limit) continue; // this team is maxed out

                // don't assign a team if the player has a lower defense priority
                if (minDefensePriority && metadata.defensePriority < minDefensePriority) continue;

                let viableTeams = playerAssignments[player.allycode].viableTeams;
                
                // loop through viableTeams in reverse so that we can remove teams when they are found via splice
                // If we want the logic to go forward, then we need to change it so that we decrease the index after we splice
                for (var viableTeamIndex = viableTeams.length - 1; viableTeamIndex >= 0; viableTeamIndex--) 
                {
                    // Look for a match with name
                    if (viableTeams[viableTeamIndex].team.name === teamName) 
                    {
                        // validate that the team is still available for the player based on what they've already been assigned
                        if (!this.isTeamAvailableForPlayer(playerAssignments[player.allycode].unitsAssigned, viableTeams[viableTeamIndex])) {
                            continue;
                        }

                        let datacronAssignmentString = null, datacronToAssign = null;
                        // validate that the datacron is still available for the player based on what they've already been assigned
                        if ((viableTeams[viableTeamIndex].team.datacronsRequired?.find(dc => dc != twUtils.NO_DATACRON_SPECIFIED) || 
                             viableTeams[viableTeamIndex].team.datacrons?.find(dc => dc != twUtils.NO_DATACRON_SPECIFIED) || 
                             viableTeams[viableTeamIndex].team.datacronFilter != null))
                        {
                            let { datacronIndex, datacronString } = await this.findAvailableDatacronForTeam(playerAssignments[player.allycode].availableDatacrons, viableTeams[viableTeamIndex].team);
                            if (datacronIndex < 0) continue; // no matching datacron found

                            datacronAssignmentString = datacronString;
                            datacronToAssign = playerAssignments[player.allycode].availableDatacrons[datacronIndex];
                            playerAssignments[player.allycode].availableDatacrons.splice(datacronIndex, 1);
                        }
 
                        foundTeams.push({ name: teamName, datacronString: datacronAssignmentString, boundUnitDefIds: viableTeams[viableTeamIndex].boundUnitDefIds });

                        // Make it the assignment and remove from the player's viable teams
                        foundTeamsString += (foundTeamsString.length > 0 ? ", " : "") + teamName;

                        // update the comment text with the team description.  Add newlines to separate teams
                        if (foundTeamsDescriptionsString.length > 0)
                            foundTeamsDescriptionsString += (String.fromCharCode(10) + String.fromCharCode(10));

                        foundTeamsDescriptionsString += (teamName + ": " + viableTeams[viableTeamIndex].team.description);

                        reservedTerritory.curTeams.push(teamName);
                        foundTeamsCount++;

                        // Record that the units in the team have been assigned for the player
                        for (var unitDefId of viableTeams[viableTeamIndex].boundUnitDefIds) {
                            playerAssignments[player.allycode].unitsAssigned.push(unitDefId);
                        }

                        viableTeams.splice(viableTeamIndex, 1);

                        // we don't catalog the datacron assigned here, since "datacronsAssigned" is used by the offense manager 

                        break;
                    }
                }
            }
        }
        
        territoryAssignments[twUtils.RESERVED_TERRITORY] =
        {
            foundTeamsCount: foundTeamsCount,
            foundTeams: foundTeams,
            foundTeamsString: foundTeamsString,
            foundTeamsDescriptionsString: foundTeamsDescriptionsString
        };
    },
    determineAssignments: async function (player, playerAssignments, metadata, territories, territoryAssignments, passNum) {

        // Loop through territories and figure out what to assign
        // Start at t = 1 because t = 0 is the reserved territory, handled in a different function
        for (var t = 1; t < territories.length; t++) {
            // don't waste time if there are no viable teams left for the player
            if (playerAssignments[player.allycode].viableTeams.length == 0)
                break;

            territory = territories[t];

            // territory is full, so continue to the next
            if (this.isTerritoryFull(territory))
                continue;

            var foundTeamsCount = 0;
            var foundTeams = new Array();
            var foundTeamsString = "";
            var foundTeamsDescriptionsString = "";

            if (territoryAssignments[t]) {
                foundTeamsCount = territoryAssignments[t].foundTeamsCount;
                foundTeamsString = territoryAssignments[t].foundTeamsString;
                foundTeamsDescriptionsString = territoryAssignments[t].foundTeamsDescriptionsString;
                foundTeams = territoryAssignments[t].foundTeams;
            }


            let maxTeamsToAssign = territory.teamNamesToAssign[passNum] ? territory.teamNamesToAssign[passNum].length : 0;

            // loop through names to assign for the territory, see what has a match in the player's viable teams
            for (var teamNameIndex = 0;
                teamNameIndex < maxTeamsToAssign && !this.isTerritoryFull(territory);
                teamNameIndex++) {

                var teamName = territory.teamNamesToAssign[passNum][teamNameIndex].teamName;
                var limit = territory.teamNamesToAssign[passNum][teamNameIndex].limit;
                var maxDefensePriority = territory.teamNamesToAssign[passNum][teamNameIndex].maxDefensePriority;
                const forAllycode = territory.teamNamesToAssign[passNum][teamNameIndex].forAllycode;

                if (forAllycode && player.allycode != forAllycode) continue; // player isn't a match for this assignment


                if (limit && territory.curTeams.filter(t => t === teamName).length >= limit) continue;

                // don't assign a team if the player has a higher defense priority
                if (maxDefensePriority && metadata.defensePriority > maxDefensePriority) continue;

                var viableTeams = playerAssignments[player.allycode].viableTeams;

                // loop through viableTeams in reverse so that we can remove teams when they are found via splice
                // If we want the logic to go forward, then we need to change it so that we decrease the index after we splice
                for (var viableTeamIndex = viableTeams.length - 1; viableTeamIndex >= 0 && !this.isTerritoryFull(territory); viableTeamIndex--) {

                    // if the player has used up all squad assignments, then don't both with other viable squad teams.  We don't want to break, though, in case we're looking at fleet teams.
                    if ((metadata.maxSquads == metadata.numSquadsAssigned) && viableTeams[viableTeamIndex].team.isSquad)
                        continue;

                    // if the player has used up all fleet assignments, then don't both with other viable fleet teams.  We don't want to break, though, in case we're looking at squad teams.
                    if ((metadata.maxFleets == metadata.numFleetsAssigned) && viableTeams[viableTeamIndex].team.isFleet)
                        continue;

                    // Look for a match with name
                    if (viableTeams[viableTeamIndex].team.name == teamName) {
                        // found a match. 

                        // validate that the team is still available for the player based on what they've already been assigned
                        if (!this.isTeamAvailableForPlayer(playerAssignments[player.allycode].unitsAssigned, viableTeams[viableTeamIndex])) {
                            continue;
                        }

                        let datacronAssignmentString = null, datacronToAssign = null;
                        // validate that the datacron is still available for the player based on what they've already been assigned
                        if ((viableTeams[viableTeamIndex].team.datacronsRequired?.find(dc => dc != twUtils.NO_DATACRON_SPECIFIED) || 
                             viableTeams[viableTeamIndex].team.datacrons?.find(dc => dc != twUtils.NO_DATACRON_SPECIFIED) || 
                             viableTeams[viableTeamIndex].team.datacronFilter != null))
                        {
                            let { datacronIndex, datacronString } = await this.findAvailableDatacronForTeam(playerAssignments[player.allycode].availableDatacrons, viableTeams[viableTeamIndex].team);
                            if (datacronIndex < 0) continue; // no matching datacron found

                            datacronAssignmentString = datacronString;
                            datacronToAssign = playerAssignments[player.allycode].availableDatacrons[datacronIndex];
                            playerAssignments[player.allycode].availableDatacrons.splice(datacronIndex, 1);
                        }
 
                        foundTeams.push({ name: teamName, datacronString: datacronAssignmentString, boundUnitDefIds: viableTeams[viableTeamIndex].boundUnitDefIds });

                        // Make it the assignment and remove from the player's viable teams
                        foundTeamsString += (foundTeamsString.length > 0 ? ", " : "") + teamName;

                        // update the comment text with the team description.  Add newlines to separate teams
                        if (foundTeamsDescriptionsString.length > 0)
                            foundTeamsDescriptionsString += (String.fromCharCode(10) + String.fromCharCode(10));

                        foundTeamsDescriptionsString += (teamName + ": " + viableTeams[viableTeamIndex].team.description);

 
                        territory.curTeams.push(teamName);
                        foundTeamsCount++;

                        if (territory.isSquad) metadata.numSquadsAssigned++;
                        if (territory.isFleet) metadata.numFleetsAssigned++;

                        // Record that the units in the team have been assigned for the player
                        for (var unitDefId of viableTeams[viableTeamIndex].boundUnitDefIds) {
                            playerAssignments[player.allycode].unitsAssigned.push(unitDefId);
                        }

                        viableTeams.splice(viableTeamIndex, 1);

                        if (datacronToAssign)
                            playerAssignments[player.allycode].datacronsAssigned.push(datacronToAssign.id);

                        break;
                    }
                }

            }

            territoryAssignments[t] =
            {
                foundTeamsCount: foundTeamsCount,
                foundTeams: foundTeams,
                foundTeamsString: foundTeamsString,
                foundTeamsDescriptionsString: foundTeamsDescriptionsString
            };

        }

        metadata.datacronsAssigned = playerAssignments[player.allycode].datacronsAssigned;
    },

    doSecondaryAssignments: async function (previousResults, playerAssignments, territories, playerPriorityData, passNum) {
        for (var resultIndex in previousResults) {

            var result = previousResults[resultIndex];
            var player = result.player;
            var playerMetadata = result.playerMetadata;
            var territoryAssignments = result.territoryAssignments;

            // don't do any assignments for players that are offense-only or have zero max defense squads
            let priorityData = playerPriorityData.find(p => p.allycode === result.player.allycode);
            if (priorityData?.type == twPlayerCommon.TYPES.OFFENSE || priorityData?.max_defense_squads === 0) continue;

            // Loop through territories and figure out what to assign    
            await this.determineAssignments(player, playerAssignments, playerMetadata, territories, territoryAssignments, passNum);
        }
    }
}


