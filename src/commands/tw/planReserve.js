// reserve teams for offense
// include a priority, team name, and a limit.
// update /run to use this as a first pass to reserve teams for offense

const discordUtils = require("../../utils/discord.js");
const twUtils = require("./utils.js");
const twPlayerCommon = require("./player/common.js");
const playerUtils = require("../../utils/player");
const database = require("../../database");
const tools = require ("../../utils/tools.js");
const guildUtils = require("../../utils/guild.js");
const { SlashCommandSubcommandBuilder } = require("@discordjs/builders");

module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("reserve")
        .setDescription("Reserve a team for offense.")
        .addStringOption(o => 
            o.setName("team")
            .setDescription("Team name to reserve")
            .setRequired(true)
            .setAutocomplete(true))
        .addIntegerOption(o =>
            o.setName("order")
            .setDescription("Order to reserve team (1-50), default 1")
            .setRequired(false)
            .setMinValue(1)
            .setMaxValue(50))
        .addIntegerOption(o =>
            o.setName("limit")
            .setDescription("Limit quantity reserved. Default unlimited.")
            .setRequired(false)
            .setMaxValue(50)
            .setMinValue(1))
        .addStringOption(o =>
            o.setName("for")
            .setDescription("Limit to a player")
            .setRequired(false)
            .setAutocomplete(true))
        .addIntegerOption(o => 
            o.setName("min-defense-priority")
            .setDescription("Min player defense priority allowed to reserve this team")
            .setRequired(false)
            .setMinValue(twPlayerCommon.MIN_PRIORITY)
            .setMaxValue(twPlayerCommon.MAX_PRIORITY)),
    permissionLevel: playerUtils.UserRoles.GuildAdmin,
    interact: async function(interaction)
    {
        let { botUser, guildId, error } = await playerUtils.authorize(interaction, this.permissionLevel)
        if (error) {
            await interaction.editReply(error);
            return;
        }

        let team = interaction.options.getString("team").trim().toLowerCase();
        let priority = interaction.options.getInteger("order") ?? 1;
        let limit = interaction.options.getInteger("limit");
        let minDefensePriority = interaction.options.getInteger("min-defense-priority");
        let forAllyCode = interaction.options.getString("for");
        
        let response = await this.reserveTeam(guildId, team, priority, limit, minDefensePriority, forAllyCode);
        await interaction.editReply(response);
    },
    autocomplete: async function(interaction)
    {
        let { botUser, guildId, error } = await playerUtils.authorize(interaction, this.permissionLevel);     
        if (error)
        {
            await interaction.respond([{ name: "You do not have permission to do this.", value: "wwwwwwww" }]);
            return;
        }

		const focusedOption = interaction.options.getFocused(true);
        let choices;

        if (focusedOption.name === "team")
        {    
            let teams = await database.db.any(
                `SELECT name, description
                FROM tw_team
                WHERE guild_id = $1 
                AND is_defense_team = TRUE 
                AND name LIKE $2
                LIMIT 25
                `, [guildId, `%${focusedOption.value}%`]
            );

            choices = teams
                    .filter(t => t.name.length < twUtils.MAX_TEAM_NAME_LENGTH)
                    .map(choice => ({ name: discordUtils.fitForAutocompleteChoice(`${choice.name} :: ${choice.description}`), value: choice.name }));

        }
        else if (focusedOption.name === "for")
        {
            let names = await database.db.any(
                `select player_name, allycode
                from guild_players 
                WHERE guild_id = $1 AND (player_name ILIKE $2 OR allycode LIKE $2)
                LIMIT 25
                `, [guildId, `%${focusedOption.value}%`]
                        );
                        
            choices = names.map(choice => ({ name: `${choice.player_name ? choice.player_name : "Unknown Player"} (${choice.allycode})`, value: choice.allycode }));

        }

		await interaction.respond(choices);

    },
    reserveTeam: async function(guildId, teamName, priority, limit, minDefensePriority, forAllyCode)
    {
        if (isNaN(priority) || priority < 1 || priority > twUtils.MAX_PRIORITY)
        {
            return `Order must be between 1 and ${twUtils.MAX_PRIORITY}.`;
        }
        if (limit != null && (isNaN(limit) || limit < 1 || limit > twUtils.MAX_LIMIT))
        {
            return `Limit must be between 1 and ${twUtils.MAX_LIMIT}.`;
        }
        if (minDefensePriority != null && (isNaN(minDefensePriority) || minDefensePriority < twPlayerCommon.MIN_PRIORITY || minDefensePriority > twPlayerCommon.MAX_PRIORITY))
        {
            return `min-defense-priority must be between ${twPlayerCommon.MIN_PRIORITY} and ${twPlayerCommon.MAX_PRIORITY}.`;
        }

        let forPlayerName;
        if (forAllyCode != null)
        {
            forAllyCode = tools.cleanAndVerifyStringAsAllyCode(forAllyCode);

            // confirm player is in guild
            forPlayerName = await guildUtils.isInGuild(forAllyCode, guildId);
            if (!forPlayerName) return `Ally Code ${forAllyCode} is not a member of your guild. If this is incorrect, refresh your guild's roster with \`/guild refresh roster\``;
        }

        const existingTeam = await twUtils.getExistingPlanAssignment(guildId, teamName, twUtils.RESERVED_TERRITORY, forAllyCode);

        var feedbackText = "";
        let minDefensePriorityString = "", forAllyCodeString = "";
        if (minDefensePriority) minDefensePriorityString = `\nOnly players with defense priority >= ${minDefensePriority} will be assigned. Adjust with \`/tw player def\``;
        if (forPlayerName) forAllyCodeString = `\nOnly ${forPlayerName} (${forAllyCode}) will be eligible for this assignment.`;


        if (!existingTeam)
        {
            await database.db.none("INSERT INTO tw_plan (guild_id, territory, team_name, priority, team_limit, min_defense_priority, for_allycode) VALUES ($1, $2, $3, $4, $5, $6, $7)",
            [guildId, twUtils.RESERVED_TERRITORY, teamName, priority, limit, minDefensePriority, forAllyCode])

            feedbackText += `Reserved **${teamName}** for offense.\r\n${limit ? `Limit: ${limit}, ` : ""}Order: ${priority}${minDefensePriorityString}${forAllyCodeString}`;
        } else {
            await twUtils.updatePlanAssignment(priority, limit, null, minDefensePriority, guildId, teamName, twUtils.RESERVED_TERRITORY, forAllyCode);

            feedbackText += `Updated reserved team **${teamName}**.\r\n${limit ? `Limit: ${limit}, ` : ""}Order: ${priority}${minDefensePriorityString}${forAllyCodeString}`;
        }

        return feedbackText;
    }
}
