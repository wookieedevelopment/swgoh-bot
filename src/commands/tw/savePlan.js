const discordUtils = require("../../utils/discord.js");
const twUtils = require("./utils.js");
const guildUtils = require("../../utils/guild");
const playerUtils = require("../../utils/player");
const database = require("../../database");
const { SlashCommandSubcommandBuilder } = require("@discordjs/builders");
const { ActionRowBuilder, ButtonBuilder, EmbedBuilder, ButtonStyle, ComponentType } = require('discord.js');
const { logger, formatError } = require("../../utils/log.js");

const YES_BUTTON_NAME = "tw:plan:save:yes"
const NO_BUTTON_NAME = "tw:plan:save:no"


module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("save")
        .setDescription("Save a TW defense plan for another time.")
        .addStringOption(o =>
            o.setName("name")
            .setDescription("A name to help you identify the plan later.")
            .setRequired(true)),
    permissionLevel: playerUtils.UserRoles.GuildAdmin,
    interact: async function(interaction)
    {
        let { botUser, guildId, error } = await playerUtils.authorize(interaction, this.permissionLevel)
        if (error) {
            await interaction.editReply(error);
            return;
        }

        let planName = interaction.options.getString("name");

        let planExists = await this.planExists(guildId, planName);

        if (!planExists)
        {
            let response = await this.savePlan(guildId, planName);
            await interaction.editReply(response);
            return;
        }

        const filter = async i => {
            await i.deferUpdate();
            return i.user.id === interaction.user.id;
        };

        // ask about replacing existing plan
        let message = await interaction.editReply(
            { 
                content: `Plan **${planName}** already exists. Replace existing plan?`,
                components: [
                    new ActionRowBuilder().addComponents(
                        new ButtonBuilder()
                            .setCustomId(YES_BUTTON_NAME)
                            .setLabel("Yes")
                            .setStyle(ButtonStyle.Success),
                        new ButtonBuilder()
                            .setCustomId(NO_BUTTON_NAME)
                            .setLabel("No")
                            .setStyle(ButtonStyle.Danger)
                    )
                ]
            });

        message.awaitMessageComponent({ filter, componentType: ComponentType.Button, time: 300000 })
            .then(async i => {
                if (i.customId == NO_BUTTON_NAME)
                    await interaction.editReply({ content: "Save cancelled.", components: [] });
                else
                    await this.handleButton(interaction)
            })
            .catch(err => logger.error(formatError(err)));
    },
    handleButton: async function(interaction)
    {
        let { botUser, guildId, error } = await playerUtils.authorize(interaction, this.permissionLevel)
        if (error) {
            await interaction.followUp(error);
            return;
        }

        let planName = interaction.options.getString("name");
        
        await this.savePlan(guildId, planName);

        await interaction.editReply({ content: `Plan **${planName}** saved. Load it anytime in the future with **/tw plan load**.`, components: [] });
    },
    planExists: async function(guildId, planName)
    {
        let existingPlan = await database.db.any("SELECT COUNT(*) FROM tw_plan_saved WHERE guild_id = $1 AND plan_name = $2", [guildId, planName])
        return existingPlan[0].count > 0;
    },
    savePlan: async function(guildId, planName)
    {
        if (!planName || planName.length > twUtils.MAX_PLAN_NAME_LENGTH)
        {
            return "Plan name must be between 1 and 100 characters.";
        }
        
        await database.db.any(
`DELETE FROM tw_plan_saved WHERE guild_id = $1 AND plan_name = $2;
INSERT INTO tw_plan_saved (guild_id, territory, team_name, priority, plan_name, team_limit, min_defense_priority, max_defense_priority, for_allycode)
(SELECT guild_id, territory, team_name, priority, $2, team_limit, min_defense_priority, max_defense_priority, for_allycode
FROM tw_plan
WHERE guild_id = $1)
`, [guildId, planName]
        )

        return `Plan **${planName}** saved.`;
    }
}
