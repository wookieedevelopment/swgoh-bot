const twUtils = require("./utils.js");
const playerUtils = require("../../utils/player");
const swapiUtils = require ("../../utils/swapi");
const discordUtils = require("../../utils/discord");
const unitsUtils = require("../../utils/units.js");
const database = require("../../database");
const { SlashCommandSubcommandBuilder } = require("@discordjs/builders");
const { EmbedBuilder, AttachmentBuilder } = require("discord.js");
const { createCanvas, Image, loadImage } = require("canvas");
const { Buffer } = require ("buffer")

const MAP_COORDINATES = [
    [], // 0 (reserved for offense)
    [ // 1
        {x: 750, y: 100},
        {x: 700, y: 180},
        {x: 800, y: 180}
    ],
    [ // 2
        {x: 730, y: 310},
        {x: 810, y: 310},
        {x: 730, y: 390}
    ],
    [ // 3
        {x: 580, y: 50},
        {x: 580, y: 125},
        {x: 580, y: 200}
    ],
    [ // 4
        {x: 550, y: 320},
        {x: 470, y: 400},
        {x: 550, y: 400}
    ],
    [ // 5
        {x: 390, y: 20},
        {x: 375, y: 95},
        {x: 450, y: 95}
    ],
    [ //6
        {x: 360, y: 200},
        {x: 320, y: 280},
        {x: 400, y: 280}
    ],
    [ // 7
        {x: 250, y: 420},
        {x: 250, y: 500},
        {x: 250, y: 580}
    ],
    [ // 8
        {x: 250, y: 40},
        {x: 180, y: 115},
        {x: 255, y: 115},
    ],
    [ // 9
        {x: 130,  y: 210},
        {x: 90,   y: 290},
        {x: 170,  y: 290}
    ],
    [ // 10
        {x: 90,  y: 400},
        {x: 50,  y: 480},
        {x: 130,  y: 480},
    ]
];

const PORTRAIT_WIDTH = 70;
const PORTRAIT_HEIGHT = 70;

module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("report")
        .setDescription("Show a zone-by-zone report of the current defense plan."),
    permissionLevel: playerUtils.UserRoles.User,
    interact: async function(interaction)
    {
        let { botUser, guildId, error } = await playerUtils.authorize(interaction, this.permissionLevel)
        if (error) {
            await interaction.editReply(error);
            return;
        }

        let response = await this.generateReportResponse(guildId);

        await interaction.followUp(response);
    },
    generateReportResponse: async function(guildId)
    {
        const existingPlan = await database.db.oneOrNone("SELECT result_json, teams_json, territories_json FROM tw_plan_result WHERE guild_id = $1", [guildId]);

        if (!existingPlan || !existingPlan.result_json)
        {
            return { content: "You do not have an existing plan saved.  Use **/tw plan run** to generate one."};
        }

        const portraitOverrides = await database.db.any("SELECT name, portrait FROM tw_team WHERE is_defense_team = TRUE AND guild_id = $1 AND portrait IS NOT NULL", [guildId]);

        let embed = new EmbedBuilder()
            .setTitle("Zone-by-Zone Defense Plan Report")
            .setColor(0x336688);
        
        const width = 929;
        const height = 732;
        const canvas = createCanvas(width, height);
        const context = canvas.getContext('2d')
        
        const img = new Image();
        img.onload = () => context.drawImage(img, 0, 0)
        img.onerror = err => { throw err }
        img.src = './src/img/tw_map_defense.png';

        let territories = new Array(existingPlan.territories_json.length);
        for (var i = 0; i < territories.length; i++) 
        {
            territories[i] = { teams: [] };
        }

        existingPlan.result_json.forEach(r => {
            r.territoryAssignments.forEach((ta, ix) => {
                ta.foundTeams.forEach(t => {
                    let team = territories[ix].teams.find(team => team.name === t.name);
                    if (!team) {
                        team = { name: t.name, quantity: 0, data: null };
                        territories[ix].teams.push(team);
                    }

                    team.quantity++;

                    team.data = existingPlan.teams_json.find(team => team.name === t.name);
                })
            })
        })

        for (var tix = 1; tix < territories.length; tix++)
        {
            let territory = territories[tix];
            let name = "**Territory** " + discordUtils.translateNumberToDiscordString(twUtils.getTerritoryNameByNumber(tix))

            territory.teams.sort((a, b) => b.quantity - a.quantity);
            let message;

            if (territory.teams.length == 0)
            {
                message = "None"
            }
            else
            {
                message = "```ini\r\n"
                territory.teams.forEach(t => {
                    message += `${t.name}: ${t.quantity}\r\n`;                
                })
                message += "```"

            }

            embed.addFields({
                name: name,
                value: message
            });


            // label the map
            if (territory.teams.length === 0) continue;

            // gather portrait info
            let portraitsToShow = [];
            for (var pix = 0; pix < territory.teams.length; pix++)
            {
                let portraitDefId = await findPortraitForTeam(territory.teams[pix].data, portraitOverrides);

                let existingTeamLeader = portraitsToShow.find(p => p.defId === portraitDefId);
                if (existingTeamLeader) 
                {
                    existingTeamLeader.quantity += territory.teams[pix].quantity;
                    continue;
                }

                portraitsToShow.push(
                    {
                        defId: portraitDefId,
                        quantity: territory.teams[pix].quantity
                    }
                )
            }

            portraitsToShow.sort((a, b) => b.quantity - a.quantity); // sort descending by quantity

            for (var portraitIx = 0; portraitIx < MAP_COORDINATES[tix].length && portraitIx < portraitsToShow.length; portraitIx++)
            {
                let image = await unitsUtils.getPortraitForUnitByDefId(portraitsToShow[portraitIx].defId);
                let img = image ?? './src/img/no-portrait.png';

                context.fillStyle = "rgb(255, 255, 255)";
                context.fillRect(MAP_COORDINATES[tix][portraitIx].x-1, MAP_COORDINATES[tix][portraitIx].y-1, PORTRAIT_WIDTH+2, PORTRAIT_HEIGHT+2);

                const unitIconImg = await loadImage(img);
                context.drawImage(unitIconImg, 
                    MAP_COORDINATES[tix][portraitIx].x, 
                    MAP_COORDINATES[tix][portraitIx].y, 
                    PORTRAIT_WIDTH, PORTRAIT_HEIGHT);

                
                const twoDigitQuantity = portraitsToShow[portraitIx].quantity >= 10;
                context.fillStyle = "rgb(0, 0, 0)"
                context.fillRect(
                    twoDigitQuantity ? MAP_COORDINATES[tix][portraitIx].x + 30 : MAP_COORDINATES[tix][portraitIx].x + 50, 
                    MAP_COORDINATES[tix][portraitIx].y + 45, 
                    twoDigitQuantity ? 40 : 20, 
                    25);

                context.font = "20pt Arial"
                context.textAlign = "left"
                context.fillStyle = "#fff"
                context.fillText(
                    portraitsToShow[portraitIx].quantity.toString(), 
                    twoDigitQuantity ? MAP_COORDINATES[tix][portraitIx].x + 35 : MAP_COORDINATES[tix][portraitIx].x + 52, 
                    MAP_COORDINATES[tix][portraitIx].y + 68);
            } 
        }

        let fileName = "tw-report-" + Date.now() + ".png"
        const file = new AttachmentBuilder(canvas.toBuffer("image/png"), { name: fileName });
        embed.setImage("attachment://" + fileName)
        return { embeds: [embed], files: [file] };
    }
}

async function findPortraitForTeam(team, portraitOverrides)
{
    let override = portraitOverrides.find(p => p.name === team.name);
    if (override) return override.portrait;

    let unitsList = await swapiUtils.getUnitsList();
    if (team.units.length == 0) return null;
    for (var u = 0; u < team.units.length; u++)
    {
        let unit = unitsList.find(unit => swapiUtils.getUnitDefId(unit) === team.units[u].defId);
        if (!unit) continue; // wtf?

        if (team.hasGL)
        {
            if (unit.is_galactic_legend) return swapiUtils.getUnitDefId(unit);
            continue;
        }
    
        if (unit.is_capital_ship || unit.categories.indexOf("Leader") >= 0) return swapiUtils.getUnitDefId(unit);
    }

    return team.units[0].defId;
}