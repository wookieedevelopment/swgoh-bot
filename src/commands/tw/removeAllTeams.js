const { EmbedBuilder } = require("discord.js");
const discordUtils = require("../../utils/discord.js");
const twUtils = require("./utils.js");
const guildUtils = require("../../utils/guild");
const playerUtils = require("../../utils/player");
const database = require("../../database");
const { SlashCommandSubcommandBuilder } = require("@discordjs/builders");

module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("rem-all")
        .setDescription("Remove all defense teams from a territory.")
        .addIntegerOption(o =>
            o.setName("territory")
            .setDescription("The territory number from which to remove the team.")
            .setRequired(true)
            .addChoices(
                { name:"1", value: 1},
                { name:"2", value: 2},
                { name:"3", value: 3},
                { name:"4", value: 4},
                { name:"5", value: 5},
                { name:"6", value: 6},
                { name:"7", value: 7},
                { name:"8", value: 8},
                { name:"9", value: 9},
                { name:"10", value: 10}
            )),
    permissionLevel: playerUtils.UserRoles.GuildAdmin,
    interact: async function(interaction)
    {
        let { botUser, guildId, error } = await playerUtils.authorize(interaction, this.permissionLevel)
        if (error) {
            await interaction.editReply(error);
            return;
        }

        let territory = interaction.options.getInteger("territory");

        let response = await this.removeAllTeamsInTerritory(guildId, territory);

        await interaction.editReply(response);
    },
    removeAllTeamsInTerritory: async function(guildId, territoryNum)
    {
        // remove all assignments in territory
        let teamsRemovedCount = await database.db.result("DELETE FROM tw_plan WHERE guild_id = $1 AND territory = $2",
            [guildId, territoryNum], r => r.rowCount);
        
        return `Removed all teams (${teamsRemovedCount}) from Territory ${territoryNum}.`;
    }
}
