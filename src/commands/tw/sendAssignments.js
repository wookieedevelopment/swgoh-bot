const { EmbedBuilder } = require("discord.js");
const discordUtils = require("../../utils/discord.js");
const twUtils = require("./utils.js");
const playerUtils = require("../../utils/player");
const database = require("../../database");
const { createCanvas } = require("canvas");
const moment = require("moment");
const { SlashCommandSubcommandBuilder } = require("@discordjs/builders");

module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("send")
        .setDescription("Send TW assignments to the guild.")
        .addBooleanOption(o =>
            o.setName("required-assignments")
            .setDescription("Whether to include a message that the assignments are required (default: true).")
            .setRequired(false)),
    interact: async function(interaction)
    {
        let { botUser, guildId, error } = await playerUtils.authorize(interaction, playerUtils.UserRoles.GuildAdmin)
        if (error) {
            await interaction.editReply(error);
            return;
        }

        let includeRequiredAssignments = interaction.options.getBoolean("required-assignments") ?? true;
        
        let response = await this.sendAll(interaction.client, guildId, includeRequiredAssignments);
        await interaction.editReply(response);
    },
    execute: async function(client, input, args) {
        let { botUser, guildId, error } = await playerUtils.authorize(input, playerUtils.UserRoles.GuildAdmin)
        if (error) {
            await input.channel.send(error);
            return;
        }

        let response = await this.sendAll(client, guildId);
        await input.channel.send(response);

    },
    sendAll: async function(client, guildId, includeRequiredAssignments)
    {
        
        const existingPlan = await database.db.oneOrNone("SELECT result_json, teams_json FROM tw_plan_result WHERE guild_id = $1", [guildId]);
        const registeredUsers = await database.db.any("SELECT user_registration.allycode, user_registration.discord_id, guild_players.player_name FROM user_registration JOIN guild_players ON user_registration.allycode = guild_players.allycode WHERE guild_players.guild_id = $1", [guildId])
        const defenseMessage = (await database.db.oneOrNone("SELECT defense_message FROM tw_metadata WHERE guild_id = $1", [guildId]))?.defense_message;

        if (!existingPlan || !existingPlan.result_json)
        {
            return "It's total chaos out there - there is no plan!  Did you run one?";
        }

        const banner = this.createDateBannerAttachment();

        var failures = "";
        var messagingPromises = new Array();

        for (var r in existingPlan.result_json)
        {
            var result = existingPlan.result_json[r];

            messagingPromises.push(
                this.sendOneAssignmentMessage(client, banner, result, registeredUsers, existingPlan.teams_json, defenseMessage, includeRequiredAssignments)    
            );
        }
        let failureMessages = await Promise.all(messagingPromises);

        var failuresCount = 0;
        for (var msgIx in failureMessages)
        {
            var msg = failureMessages[msgIx];
            if (msg == null) continue;

            failures += msg + "\r\n"
            failuresCount++;
        }

        var messageToSend = "Sent assignments to " + (existingPlan.result_json.length - failuresCount) + " players."
        if (failuresCount > 0)
        {
            messageToSend += `\n\nThere ${failuresCount === 1 ? "was" : "were"} ${failuresCount} failure${failuresCount === 1 ? "" : "s"}:\n${failures}`;
        }
        
        let embedColor = failuresCount > 0 ? 0xaa0000 : 0x00aa00;
        embed = new EmbedBuilder().setTitle("TW Assignments").setColor(embedColor).setDescription(messageToSend);
        return { embeds: [embed] };
    },
    createDateBannerAttachment: function()
    {
        
        const width = 500;
        const height = 100;
        const canvas = createCanvas(width, height);
        const context = canvas.getContext('2d')
        const defaultHeaderFont = "bold 20pt Arial"
        const defaultFontColor = "#000"

        context.fillStyle = '#ddd'
        context.fillRect(0, 0, width, height)
        
        context.font = defaultHeaderFont;
        context.textAlign = "center"
        context.fillStyle = defaultFontColor;

        let dateString = moment().utc().format(`MMMM Do, YYYY`)
        context.fillText(`Defenses for ${dateString}`,width/2, height/2 + 12);

        const buffer = canvas.toBuffer('image/png')
        
        return {
            files: [{
                attachment: buffer,
                name: "tw-banner-" + Date.now() + ".png"
            }]
        };
    },
    sendOneAssignmentMessage: async function(client, banner, result, registeredUsers, teams, defenseMessage, includeRequiredAssignments)
    {
        var allycode = result.player.allycode;
        var registration = registeredUsers.find(u => u.allycode == allycode);
        if (registration == null)
        {
            return "No Discord ID: " + result.player.name + " (" + allycode + ")";
        }

        var discordId = registration.discord_id;

        var response = await twUtils.getIndividualAssignmentResponse(result, teams, defenseMessage, includeRequiredAssignments);

        let failureMessage = "Could not send: " + result.player.name + " (" + allycode + ") -- " + discordUtils.makeIdTaggable(discordId);
        let discordUser;

        try {
            discordUser = await client.users.fetch(discordId);
        }
        catch (error)
        {
            return failureMessage;
        }

        try {
            await discordUser.send(banner);
        } catch {
            return failureMessage;
        }

        // if (message.length <= 2000)
        // {
        //     try {
        //         await discordUser.send(message);
        //     } catch (error)
        //     {
        //         return failureMessage;
        //     }
        //     return null;
        // }

        try {
            await discordUser.send(response);
        } catch (error)
        {
            return failureMessage;
        }
        
        return null;
        
        // let messagesArray = discordUtils.splitMessage(message, "```\r\n");
        // for (let m = 0; m < messagesArray.length; m++)
        // {
        //     try {
        //         await discordUser.send(messagesArray[m]);
        //     } catch (error)
        //     {
        //         return failureMessage;
        //     }
        // }

        // return null;
    }
}
