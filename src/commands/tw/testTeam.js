const { EmbedBuilder } = require("discord.js");
const discordUtils = require("../../utils/discord.js");
const twUtils = require("./utils.js");
const swapiUtils = require("../../utils/swapi");
const playerUtils = require("../../utils/player");
const database = require("../../database");
const { logger, formatError } = require("../../utils/log");
const { SlashCommandSubcommandBuilder } = require("@discordjs/builders");

module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("test")
        .setDescription("Test who matches a team. Only tests players included in the war.")
        .addStringOption(o => 
            o.setName("team")
            .setDescription("Team name to test")
            .setRequired(true)
            .setAutocomplete(true))
        .addBooleanOption(o =>
            o.setName("show-fails")
            .setDescription("Show players that do not match")
            .setRequired(false))
        .addStringOption(o =>
            o.setName("player")
            .setDescription("One player to test")
            .setRequired(false)
            .setAutocomplete(true)),
    permissionLevel: playerUtils.UserRoles.User,
    interact: async function(interaction, isDefenseTeam)
    {
        let teamName = interaction.options.getString("team").trim().toLowerCase();
        let showFails = interaction.options.getBoolean("show-fails") ?? false;
        let allycode = interaction.options.getString("player");

        if (allycode != null)
        {
            await interaction.editReply(`Checking player to see if they have a viable **${teamName}**. ${isDefenseTeam ? "defense" : "offense"} team.`);
        }
        else
        {
            await interaction.editReply(`Checking your guild's roster to see who has a viable **${teamName}** ${isDefenseTeam ? "defense" : "offense"} team. Players excluded with **/tw exclude** will not be checked.`);
        }

        let response = await this.testTeam(interaction, teamName, isDefenseTeam, showFails, allycode);
        await interaction.followUp(response);
    },
    autocomplete: async function(interaction, isDefenseTeam)
    {
        let { botUser, guildId, error } = await playerUtils.authorize(interaction, this.permissionLevel);     
        if (error)
        {
            await interaction.respond([{ name: "You do not have permission to do this.", value: "wwwwwwwww" }]);
            return;
        }

		const focusedOption = interaction.options.getFocused(true);

        let choices;

        if (focusedOption.name === "team")
        {
            let teams = await database.db.any(
    `SELECT name, description
    FROM tw_team
    WHERE guild_id = $1 
      AND is_defense_team = $2 
      AND name ILIKE $3
    LIMIT 25
    `, [guildId, isDefenseTeam, `%${focusedOption.value}%`]);
    
            choices = teams
                    .filter(t => t.name.length < twUtils.MAX_TEAM_NAME_LENGTH)
                    .map(choice => ({ name: discordUtils.fitForAutocompleteChoice(`${choice.name} :: ${choice.description}`), value: choice.name }));
        }
        else if (focusedOption.name === "player")
        {
            let names = await database.db.any(
                `select player_name, allycode
                from guild_players 
                WHERE guild_id = $1 AND (player_name ILIKE $2 OR allycode LIKE $2)
                LIMIT 25
                `, [guildId, `%${focusedOption.value}%`]
                        );
                        
            choices = names.map(choice => ({ name: `${choice.player_name ? choice.player_name : "Unknown Player"} (${choice.allycode})`, value: choice.allycode }));
        }


		await interaction.respond(choices);

    },
    testTeam: async function(inputOrInteraction, teamName, isDefenseTeam, showFails, allycode) {
        let { botUser, guildId, error } = await playerUtils.authorize(inputOrInteraction, this.permissionLevel)
        if (error) {
            return error;
        }

        let team = await twUtils.findTeam(guildId, teamName, isDefenseTeam);

        if (!team) {
            return `No ${isDefenseTeam ? "defense" : "offense"} team found by the name **${teamName}**.  Please compare to your list of teams: **/tw${isDefenseTeam ? "" : "manager"} team list**`;
        }

        var viablePlayers = new Array();
        let nonviablePlayers = new Array();
        var cacheTime;
        
        var playersChecked = 0;

        let caches;
        
        if (allycode)
        {
            caches = await database.db.any(`
                SELECT * FROM (
                    SELECT
                        ROW_NUMBER() OVER (
                            PARTITION BY allycode 
                            ORDER BY player_cache.timestamp desc
                        ) AS r,
                        allycode, 
                        player_cache.timestamp,
                        data_text, 
                        type
                    FROM player_cache
                    LEFT JOIN cache_data USING (cache_data_id)
                    WHERE allycode = $1
                ) x
                WHERE x.r <= 1`, [allycode]);
        }
        else
        {
            caches = await database.db.any(`
                SELECT * FROM (
                    SELECT
                        ROW_NUMBER() OVER (
                            PARTITION BY allycode 
                            ORDER BY player_cache.timestamp desc
                        ) AS r,
                        allycode, 
                        player_cache.timestamp,
                        data_text, 
                        type
                    FROM player_cache
                    LEFT JOIN cache_data USING (cache_data_id)
                    join guild_players using (allycode)
                    left join tw_exclusion using (allycode)
                    WHERE guild_players.guild_id = $1 AND tw_exclusion.tw_exclusion_id is NULL
                ) x
                WHERE x.r <= 1`, [guildId]);
        }

        if (caches.length == 0)
        {
            return `Your guild's player data isn't cached.  Run **/guild cache all**.`;
        }

        try {
            await Promise.all(
                caches.map(async (cache) =>
                {
                    let playerData = JSON.parse(cache.data_text);
                    if (!cacheTime) cacheTime = cache.timestamp;
                    
                    let viable = await twUtils.isTeamViable(playerData, team);
                    if (viable.viable) viablePlayers.push(`- ${swapiUtils.getPlayerName(playerData)}${discordUtils.LTR} (${swapiUtils.getPlayerAllycode(playerData)})`);
                    else {
                        nonviablePlayers.push(`- ${swapiUtils.getPlayerName(playerData)}${discordUtils.LTR} (${swapiUtils.getPlayerAllycode(playerData)})\n  - ${viable.reason}`)
                    }
                    playersChecked++;    
                })
            );
        } catch (err)
        {
            logger.error(formatError(err))
            return "An error occurred checking if a team was viable. Please try again.";    
        }

        let embed = new EmbedBuilder()
            .setTitle(`${viablePlayers.length} player${viablePlayers.length === 1 ? "" : "s"} match${viablePlayers.length === 1 ? "es" : ""} ${isDefenseTeam ? "defense" : "offense"} team **${teamName}**`)
            .setColor(0x33e844)
            .setDescription(
                viablePlayers.length === 0
                ? `${allycode ? "Player does not match" : "No one in your guild matches"} the criteria for ${isDefenseTeam ? "defense" : "offense"} team **${teamName}**.`
                : viablePlayers.sort((a, b) => a.localeCompare(b, undefined, { sensitivity: 'base' })).join("\n")
            )
            .setFooter({ text: `Using cached data for ${playersChecked} player${playersChecked.length == 1 ? "" : "s"} from ${discordUtils.convertDateToString(cacheTime)}` });

        let response = { embeds: [embed] };

        if (showFails)
        {
            let failEmbed = new EmbedBuilder()
                .setTitle(`${nonviablePlayers.length} player${nonviablePlayers.length === 1 ? "" : "s"} do${nonviablePlayers.length === 1 ? "es" : ""} NOT match`)
                .setColor(0x801010)
                .setDescription(
                    nonviablePlayers.length === 0 
                    ? (allycode ? "Player matches." : "All players match.")
                    : nonviablePlayers.sort((a, b) => a.localeCompare(b, undefined, { sensitivity: 'base' })).join("\n")
                )
                .setFooter({ text: `Using cached data for ${playersChecked} player${playersChecked.length == 1 ? "" : "s"} from ${discordUtils.convertDateToString(cacheTime)}` });
            response.embeds.push(failEmbed);
        }
        return response;
    }
}
