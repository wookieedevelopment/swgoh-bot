const { EmbedBuilder } = require("discord.js");
const discordUtils = require("../../utils/discord.js");
const twUtils = require("./utils.js");
const guildUtils = require("../../utils/guild");
const playerUtils = require("../../utils/player");
const database = require("../../database");
const { SlashCommandSubcommandBuilder } = require("@discordjs/builders");

module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("unreserve-all")
        .setDescription("Unreserve *all* offense teams."),
    permissionLevel: playerUtils.UserRoles.GuildAdmin,
    interact: async function(interaction)
    {
        let { botUser, guildId, error } = await playerUtils.authorize(interaction, this.permissionLevel)
        if (error) {
            await interaction.editReply(error);
            return;
        }

        let response = await this.unreserveAllOffenseTeams(guildId);

        await interaction.editReply(response);
    },
    unreserveAllOffenseTeams: async function(guildId)
    {
        // remove all assignments in territory
        let teamsRemovedCount = await database.db.result("DELETE FROM tw_plan WHERE guild_id = $1 AND territory = $2",
            [guildId, twUtils.RESERVED_TERRITORY], r => r.rowCount);
        
        return `Remove all teams (${teamsRemovedCount}) from offense reservations.`;
    }
}
