const showPlan = require("./showPlan");
const runPlan = require("./runPlan")
const lastPlan = require("./lastPlan")
const { SlashCommandSubcommandGroupBuilder } = require("@discordjs/builders");
const myAssignments = require("./myAssignments");
const removeAllTeams = require("./removeAllTeams")
const removeTeam = require("./removeTeam")
const setTeam = require("./setTeam")
const savePlan = require("./savePlan");
const loadPlan = require("./loadPlan");
const deletePlan = require("./deletePlan");
const listPlans = require("./listPlans");
const planReport = require("./planReport");
const planSwap = require("./planSwap");
const planReserve = require("./planReserve");
const planUnreserve = require("./planUnreserve");
const planUnreserveAll = require("./planUnreserveAll");

module.exports = {
    data: new SlashCommandSubcommandGroupBuilder()
        .setName("plan")
        .setDescription("TW Defense Planner")
        .addSubcommand(showPlan.data)
        .addSubcommand(runPlan.data)
        .addSubcommand(lastPlan.data)
        .addSubcommand(myAssignments.data)
        .addSubcommand(removeAllTeams.data)
        .addSubcommand(removeTeam.data)
        .addSubcommand(setTeam.data)
        .addSubcommand(planSwap.data)
        .addSubcommand(savePlan.data)
        .addSubcommand(loadPlan.data)
        .addSubcommand(listPlans.data)
        .addSubcommand(deletePlan.data)
        .addSubcommand(planReport.data)
        .addSubcommand(planReserve.data)
        .addSubcommand(planUnreserve.data)
        .addSubcommand(planUnreserveAll.data),
    interact: async function(interaction)
    {
      let subcommand = interaction.options.getSubcommand();

      switch (subcommand)
      {
        case showPlan.data.name:
            await showPlan.interact(interaction);
            break;
        case runPlan.data.name:
            await runPlan.interact(interaction);
            break;
        case lastPlan.data.name:
            await lastPlan.interact(interaction);
            break;
        case myAssignments.data.name:
            await myAssignments.interact(interaction);
            break;
        case setTeam.data.name:
            await setTeam.interact(interaction);
            break;
        case removeTeam.data.name:
            await removeTeam.interact(interaction);
            break;
        case removeAllTeams.data.name:
            await removeAllTeams.interact(interaction);
            break;
        case planSwap.data.name:
            await planSwap.interact(interaction);
            break;
        case savePlan.data.name:
            await savePlan.interact(interaction);
            break;
        case loadPlan.data.name:
            await loadPlan.interact(interaction);
            break;
        case listPlans.data.name:
            await listPlans.interact(interaction);
            break;
        case deletePlan.data.name:
            await deletePlan.interact(interaction);
            break;
        case planReport.data.name:
            await planReport.interact(interaction);
            break;
        case planReserve.data.name:
            await planReserve.interact(interaction);
            break;
        case planUnreserve.data.name:
            await planUnreserve.interact(interaction);
            break;
        case planUnreserveAll.data.name:
            await planUnreserveAll.interact(interaction);
            break;
      }
    },
    autocomplete: async function(interaction)
    {
        let subcommand = interaction.options.getSubcommand();
  
        switch (subcommand)
        {
            case myAssignments.data.name:
                await myAssignments.autocomplete(interaction);
                break;
            case setTeam.data.name:
                await setTeam.autocomplete(interaction);
                break;
            case removeTeam.data.name:
                await removeTeam.autocomplete(interaction);
                break;
            case loadPlan.data.name:
                await loadPlan.autocomplete(interaction);
                break;
            case deletePlan.data.name:
                await deletePlan.autocomplete(interaction);
                break;
            case planReserve.data.name:
                await planReserve.autocomplete(interaction);
                break;
            case planUnreserve.data.name:
                await planUnreserve.autocomplete(interaction);
                break;
        }
    }
}