const { EmbedBuilder } = require("discord.js");
const discordUtils = require("../../utils/discord.js");
const twUtils = require("./utils.js");
const guildUtils = require("../../utils/guild");
const playerUtils = require("../../utils/player");
const database = require("../../database");
const { SlashCommandSubcommandBuilder } = require("@discordjs/builders");

module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("swap")
        .setDescription("Swap all assignments between two territories.")
        .addIntegerOption(o =>
            o.setName("territory_a")
            .setDescription("A territory to swap.")
            .setRequired(true)
            .addChoices(
                { name: "1 (Squad, Front-Top)", value: 1},
                { name: "2 (Squad, Front-Bottom)", value: 2},
                { name: "3 (Squad, Second-Top)", value: 3},
                { name: "4 (Squad, Second-Bottom)", value: 4},
                { name: "5 (Fleet, Front)", value: 5},
                { name: "6 (Squad, Third-Mid)", value: 6},
                { name: "7 (Squad, Third-Bottom)", value: 7},
                { name: "8 (Fleet, Back)", value: 8},
                { name: "9 (Squad, Back-Top)", value: 9},
                { name: "10 (Squad, Back-Bottom)", value: 10}
            ))
        .addIntegerOption(o =>
            o.setName("territory_b")
            .setDescription("A territory with which to swap.")
            .setRequired(true)
            .addChoices(
                { name: "1 (Squad, Front-Top)", value: 1},
                { name: "2 (Squad, Front-Bottom)", value: 2},
                { name: "3 (Squad, Second-Top)", value: 3},
                { name: "4 (Squad, Second-Bottom)", value: 4},
                { name: "5 (Fleet, Front)", value: 5},
                { name: "6 (Squad, Third-Mid)", value: 6},
                { name: "7 (Squad, Third-Bottom)", value: 7},
                { name: "8 (Fleet, Back)", value: 8},
                { name: "9 (Squad, Back-Top)", value: 9},
                { name: "10 (Squad, Back-Bottom)", value: 10}
            )),
    permissionLevel: playerUtils.UserRoles.GuildAdmin,
    interact: async function(interaction)
    {
        let { botUser, guildId, error } = await playerUtils.authorize(interaction, this.permissionLevel)
        if (error) {
            await interaction.editReply(error);
            return;
        }

        let territoryA = interaction.options.getInteger("territory_a");
        let territoryB = interaction.options.getInteger("territory_b");

        let response = await this.swap(guildId, territoryA, territoryB);

        await interaction.editReply(response);
    },
    swap: async function(guildId, territoryA, territoryB)
    {
        if (territoryA == territoryB) return "Those are the same territory. No changes made.";

        let validSwap = 
            (!twUtils.isFleetTerritory(territoryA) && !twUtils.isFleetTerritory(territoryB)) ||
            (twUtils.isFleetTerritory(territoryA) && twUtils.isFleetTerritory(territoryB));

        if (!validSwap) return "You cannot swap a fleet territory with a squad territory.";

        await database.db.any(`
            UPDATE tw_plan SET territory = 99 WHERE guild_id = $1 AND territory = $2;
            UPDATE tw_plan SET territory = $2 WHERE guild_id = $1 AND territory = $3;
            UPDATE tw_plan SET territory = $3 WHERE guild_id = $1 AND territory = 99;`,
            [guildId, territoryA, territoryB]);

        return `Swapped all assignments between territories ${territoryA} and ${territoryB}`;
    }
}
