const { EmbedBuilder } = require("discord.js");
const discordUtils = require("../../utils/discord.js");
const twUtils = require("./utils.js");
const guildUtils = require("../../utils/guild");
const playerUtils = require("../../utils/player");
const database = require("../../database");
const { SlashCommandSubcommandBuilder } = require("@discordjs/builders");

module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("include")
        .setDescription("Include a player in the war.")
        .addStringOption(o =>
            o.setName("player")
            .setDescription("Player to include.")
            .setRequired(false)
            .setAutocomplete(true))
        .addBooleanOption(o =>
            o.setName("all")
            .setDescription("true: include all previously excluded players.")
            .setRequired(false)),
    permissionLevel: playerUtils.UserRoles.GuildAdmin,
    name: "tw.include",
    description: "Include a player in the war.",
    interact: async function(interaction)
    {
        let allycode = interaction.options.getString("player");

        let all = interaction.options.getBoolean("all");

        let response = await this.include(interaction, all ? "all" : allycode);
        await interaction.editReply(response);
    },
    autocomplete: async function(interaction)
    {
        let { botUser, guildId, error } = await playerUtils.authorize(interaction, this.permissionLevel)
        if (error) {
            await interaction.respond([{ name: "You do not have permission to do this.", value: "wwwwwwww" }]);
            return;
        }

        const focusedValue = interaction.options.getFocused();

        let names = await database.db.any(
`select player_name, allycode
from tw_exclusion 
left join guild_players using (allycode)
WHERE tw_exclusion.guild_id = $1 AND (player_name ILIKE $2 OR allycode LIKE $2)
LIMIT 25
`, [guildId, `%${focusedValue}%`]
        )

		await interaction.respond(
			names.map(choice => ({ name: `${choice.player_name ? choice.player_name : "Unknown Player"} (${choice.allycode})`, value: choice.allycode }))
		);
    },
    execute: async function(client, input, args) {

        let response = await this.include(input, args ? args[0] : null);
        await input.channel.send(response);

    },
    include: async function(inputOrInteraction, allycode)
    {
        let { botUser, guildId, error } = await playerUtils.authorize(inputOrInteraction, this.permissionLevel)
        if (error) {
            return error;
        }

        if (!allycode || allycode.length == 0)
        {
            let embed = await twUtils.getExclusionsListEmbed(guildId);
            return { embeds: [embed] };
        }

        if (allycode.toLowerCase().trim() == "all")
        {
            await database.db.none("DELETE FROM tw_exclusion WHERE guild_id = $1", [guildId]);
            return "All previously excluded players are now included.";
        }

        let allycodeParsed = playerUtils.cleanAndVerifyStringAsAllyCode(allycode);
        
        try {
            var deletedCount = await database.db.result("DELETE FROM tw_exclusion WHERE allycode = $1 AND guild_id = $2", [allycodeParsed, guildId], r=>r.rowCount);

            const playerName = await playerUtils.getPlayerNameByAllycode(allycodeParsed);

            if (deletedCount > 0)
                return `**${playerName} (${allycodeParsed})** is now included in the war.`;
            else
                return `No effect. **${playerName} (${allycodeParsed})** was not excluded.`;
        } catch {
            return "Huac ooac. ('Uh oh.').  Something went wrong.  Try again?";
        }
    }
}
