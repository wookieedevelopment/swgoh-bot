const { EmbedBuilder } = require("discord.js");
const twUtils = require("./utils.js");
const twPlayerCommon = require("./player/common.js");
const playerUtils = require("../../utils/player");
const database = require("../../database");
const { createCanvas } = require("canvas");
const { SlashCommandSubcommandBuilder } = require("@discordjs/builders");

const tBoxBorderColor = 'rgb(21, 180, 255)'
const tBoxFillColor = 'rgba(0, 123, 183, 0.9)'

const drawingData = 
[
    {},
    { xPos: 1700, yPos: 50, width: 500, height: 1075 },
    { xPos: 1700, yPos: 1175, width: 500, height: 1075 },
    { xPos: 1150, yPos: 50, width: 500, height: 1075 },
    { xPos: 1150, yPos: 1175, width: 500, height: 1075 },
    { xPos: 600, yPos: 50, width: 500, height: 700 },
    { xPos: 600, yPos: 800, width: 500, height: 700 },
    { xPos: 600, yPos: 1550, width: 500, height: 700 },
    { xPos: 50, yPos: 50, width: 500, height: 700 },
    { xPos: 50, yPos: 800, width: 500, height: 700 },
    { xPos: 50, yPos: 1550, width: 500, height: 700 },
]

module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("show")
        .setDescription("Show help for the TW defense planner."),
    name: "tw.showplan",
    aliases: ["war.showplan", "tw.sp", "war.sp"],
    description: "Show the plan you've laid out for the war.",
    interact: async function(interaction)
    {
        let { botUser, guildId, error } = await playerUtils.authorize(interaction, playerUtils.UserRoles.User)
        if (error) {
            await interaction.editReply(error);
            return;
        }

        let response = await this.generateDiscordMapFileResponse(guildId);

        await interaction.editReply(response);
    },
    generateDiscordMapFileResponse: async function(guildId)
    {
        let territories = await this.loadTerritories(guildId);
        var map = this.createMapImage(territories);

        let offenseTeamsEmbed = this.getOffenseTeamsEmbed(territories[twUtils.RESERVED_TERRITORY])

        return {
            embeds: [offenseTeamsEmbed],
            files: [{
                attachment: map,
                name: "tw-map-" + Date.now() + ".png"
            }]
        };
    },
    loadTerritories : async function(guildId){
        
        var territories = new Array(11);

        const plan = await database.db.any("SELECT territory, team_name, priority, team_limit, max_defense_priority, min_defense_priority, for_allycode, player_name FROM tw_plan LEFT JOIN guild_players ON for_allycode = allycode WHERE tw_plan.guild_id = $1 order by territory asc, priority asc", [guildId]);
        for (var p in plan)
        {
            var planItem = plan[p];
            var team = { 
                name: planItem.team_name, 
                priority: planItem.priority, 
                limit: planItem.team_limit, 
                maxDefensePriority: planItem.max_defense_priority, 
                minDefensePriority: planItem.min_defense_priority,
                forAllycode: planItem.for_allycode,
                forPlayerName: planItem.player_name
            };

            var territoryIx = planItem.territory;
            if (!territories[territoryIx]) territories[territoryIx] = new Array();
            territories[territoryIx].push(team);
        }
        return territories;
    },
    getOffenseTeamsEmbed: function(reservedTerritory)
    {
        let embed = new EmbedBuilder()
            .setTitle("Teams Reserved for Offense")
            .setColor(0x112299);
        
        let description;
        if (!reservedTerritory || reservedTerritory.length === 0) {
            description = "No teams reserved for offense. Use **/tw plan reserve** and **/tw plan unreserve** to manage.";
        }
        else
        {
            const teamNameDisplayMaxLength = 32;
            const orderMaxLength = 3;
            const limitMaxLength = 3;
            description =
                "```\n" + 
                "Ord |            Team Name            | Lim | Pri\r\n" +
                "-------------------------------------------------\r\n" + 
                reservedTerritory
                    .map(t => t.priority.toString().padEnd(orderMaxLength) + " | " + (t.name + (t.forPlayerName ? ` (${t.forPlayerName})` : "")).padEnd(teamNameDisplayMaxLength).substring(0, teamNameDisplayMaxLength) + "| " + (t.limit?.toString() ?? "").padEnd(limitMaxLength) + " | " + (t.minDefensePriority?.toString() ?? twPlayerCommon.MIN_PRIORITY.toString()) + "+")
                    .join("\n") + 
                "```";
        }
        embed.setDescription(description);
            
        return embed;
    },
    createMapImage: function(territories) {

        const width = 2250;
        const height = 2300;
        const canvas = createCanvas(width, height);
        const context = canvas.getContext('2d')

        context.fillStyle = 'rgb(106,53,0)'

        context.fillRect(0, 0, width, height)

        for (var t = 1; t < drawingData.length; t++)
        {
            this.drawTerritory(context, drawingData[t].xPos, drawingData[t].yPos, drawingData[t].width, drawingData[t].height);

            
            var territoryName = twUtils.getTerritoryNameByNumber(t) + (twUtils.isFleetTerritory(t) ? " (Fleet)" : " (Squad)");
            context.fillStyle = "rgb(26, 51, 230)"
            context.fillRect(drawingData[t].xPos, drawingData[t].yPos, drawingData[t].width, 55);
            context.font = "30pt Arial"
            context.textAlign = "center"
            context.fillStyle = "#fff";
            context.fillText(territoryName, drawingData[t].xPos + (drawingData[t].width/2), drawingData[t].yPos + 40)

            let text = "";
            for (var team in territories[t])
            {
                let parentheticals = [];
                if (territories[t][team].limit) parentheticals.push(territories[t][team].limit.toString());
                if (territories[t][team].maxDefensePriority) parentheticals.push(`d <= ${territories[t][team].maxDefensePriority}`);
                if (territories[t][team].forPlayerName) parentheticals.push(territories[t][team].forPlayerName);

                let parenthetical = parentheticals.join(", ");
                if (parenthetical.length > 0) parenthetical = ` (${parenthetical})`;

                text += `${territories[t][team].priority}: ${territories[t][team].name}${parenthetical}\n`;
            }

            context.font = "30pt Arial"
            context.textAlign = "left"
            context.fillStyle = "#fff"
            context.fillText(text, 
                drawingData[t].xPos + 20, 
                drawingData[t].yPos + 100, 
                drawingData[t].width - 20);
        }
        const buffer = canvas.toBuffer('image/png')

        return buffer;
    },
    drawTerritory: function(context, xPos, yPos, width, height)
    {
        this.drawBorder(context, tBoxBorderColor, xPos, yPos, width, height);

        context.fillStyle = tBoxFillColor;
        context.fillRect(xPos, yPos, width, height);

    },
    drawBorder: function (context, color, xPos, yPos, width, height, thickness = 1)
    {
        context.fillStyle=color;
        context.fillRect(xPos - (thickness), yPos - (thickness), width + (thickness * 2), height + (thickness * 2));
    }
}
