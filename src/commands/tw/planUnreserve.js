// reserve teams for offense
// include a priority, team name, and a limit.
// update /run to use this as a first pass to reserve teams for offense

const discordUtils = require("../../utils/discord.js");
const twUtils = require("./utils.js");
const playerUtils = require("../../utils/player");
const database = require("../../database");
const { SlashCommandSubcommandBuilder } = require("@discordjs/builders");

module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("unreserve")
        .setDescription("Remove a team from offense reservations list.")
        .addIntegerOption(o => 
            o.setName("team")
            .setDescription("The team to unreserve, selected from the list.")
            .setRequired(true)
            .setAutocomplete(true)),
    permissionLevel: playerUtils.UserRoles.GuildAdmin,
    interact: async function(interaction)
    {
        let { botUser, guildId, error } = await playerUtils.authorize(interaction, this.permissionLevel)
        if (error) {
            await interaction.editReply(error);
            return;
        }

        let twPlanId = interaction.options.getInteger("team");
        
        let response = await this.unreserveTeam(guildId, twPlanId);
        await interaction.editReply(response);
    },
    autocomplete: async function(interaction)
    {
        let { botUser, guildId, error } = await playerUtils.authorize(interaction, this.permissionLevel);     
        if (error)
        {
            await interaction.respond([{ name: "You do not have permission to do this.", value: "wwwwwwww" }]);
            return;
        }

        const focusedValue = interaction.options.getFocused();

        let teams = await database.db.any(
            `SELECT tw_plan_id, team_name, for_allycode, player_name
            FROM tw_plan
            LEFT JOIN guild_players ON for_allycode = allycode
            WHERE tw_plan.guild_id = $1 
                AND tw_plan.team_name LIKE $2
                AND tw_plan.territory = $3
            LIMIT 25
            `, [guildId, `%${focusedValue}%`, twUtils.RESERVED_TERRITORY]);

        await interaction.respond(
            teams
                .filter(t => t.team_name.length < twUtils.MAX_TEAM_NAME_LENGTH)
                .map(choice => ({ name: (choice.player_name ? `(${choice.player_name}) ` : "") + choice.team_name, value: choice.tw_plan_id })));
    },
    unreserveTeam: async function(guildId, twPlanId)
    {
        
        let removeResponse = await database.db.result("DELETE FROM tw_plan WHERE guild_id = $1 AND tw_plan_id = $2 AND territory = $3 RETURNING team_name,for_allycode",
            [guildId, twPlanId, twUtils.RESERVED_TERRITORY]);

        if (removeResponse && removeResponse.rows.length > 0)
        {
            let forText = removeResponse.rows[0].for_allycode ? ` for **${await playerUtils.getPlayerNameByAllycode(removeResponse.rows[0].for_allycode)}**` : "";
            return `Team **${removeResponse.rows[0].team_name}** no longer reserved for offense${forText}.`;    
        }
        else   
            return `No team by that name has been reserved for offense.`;
    }
}
