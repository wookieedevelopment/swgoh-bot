const { EmbedBuilder } = require("discord.js");
const discordUtils = require("../../utils/discord.js");
const twUtils = require("./utils.js");
const guildUtils = require("../../utils/guild");
const playerUtils = require("../../utils/player");
const database = require("../../database");
const { SlashCommandSubcommandBuilder } = require("@discordjs/builders");

module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("rem")
        .setDescription("Remove a defense team from a territory.")
        .addIntegerOption(o =>
            o.setName("territory")
            .setDescription("The territory number from which to remove the team.")
            .setRequired(true)
            .addChoices(
                { name:"1", value: 1},
                { name:"2", value: 2},
                { name:"3", value: 3},
                { name:"4", value: 4},
                { name:"5", value: 5},
                { name:"6", value: 6},
                { name:"7", value: 7},
                { name:"8", value: 8},
                { name:"9", value: 9},
                { name:"10", value: 10}
            ))
        .addIntegerOption(o => 
            o.setName("team")
            .setDescription("The team to remove, selected from the list.")
            .setRequired(true)
            .setAutocomplete(true)),
    permissionLevel: playerUtils.UserRoles.GuildAdmin,
    interact: async function(interaction)
    {
        let { botUser, guildId, error } = await playerUtils.authorize(interaction, this.permissionLevel)
        if (error) {
            await interaction.editReply(error);
            return;
        }

        let territory = interaction.options.getInteger("territory");
        let twPlanId = interaction.options.getInteger("team");

        let response = await this.removeOneTeam(guildId, twPlanId, territory);

        await interaction.editReply(response);
    },
    autocomplete: async function(interaction)
    {
        let { botUser, guildId, error } = await playerUtils.authorize(interaction, this.permissionLevel);     
        if (error)
        {
            await interaction.respond([{ name: "You do not have permission to do this.", value: "wwwwwwww" }]);
            return;
        }

        const focusedValue = interaction.options.getFocused();
        const territory = interaction.options.getInteger("territory");

        let teams;

        if (territory == null)
        {
            teams = await database.db.any(
                `SELECT tw_plan_id, team_name, for_allycode, player_name
                FROM tw_plan
                LEFT JOIN guild_players ON for_allycode = allycode
                WHERE tw_plan.guild_id = $1 
                  AND tw_plan.team_name LIKE $2
                LIMIT 25
                `, [guildId, `%${focusedValue}%`]);
        } 
        else 
        {
            teams = await database.db.any(
                `SELECT tw_plan_id, team_name, for_allycode, player_name
                FROM tw_plan
                LEFT JOIN guild_players ON for_allycode = allycode
                WHERE tw_plan.guild_id = $1 
                  AND tw_plan.team_name LIKE $2
                  AND tw_plan.territory = $3
                LIMIT 25
                `, [guildId, `%${focusedValue}%`, territory]);
        }

		await interaction.respond(
			teams
                .filter(t => t.team_name.length < twUtils.MAX_TEAM_NAME_LENGTH)
                .map(choice => ({ name: (choice.player_name ? `(${choice.player_name}) ` : "") + choice.team_name, value: choice.tw_plan_id }))
		);
    },
    removeOneTeam: async function(guildId, twPlanId, territoryNum)
    {
        let removeResponse = await database.db.result("DELETE FROM tw_plan WHERE guild_id = $1 AND tw_plan_id = $2 AND territory = $3 RETURNING team_name,for_allycode",
            [guildId, twPlanId, territoryNum]);

        if (removeResponse && removeResponse.rows.length > 0)
        {
            let forText = removeResponse.rows[0].for_allycode ? ` for **${await playerUtils.getPlayerNameByAllycode(removeResponse.rows[0].for_allycode)}**` : "";
            return `Removed team **${removeResponse.rows[0].team_name}** from territory ${territoryNum}${forText}.`;    
        }
        else   
            return `That team was not assigned to territory ${territoryNum}.`;
    },
    removeAllTeamsInTerritory: async function(guildId, territoryNum)
    {
        // remove all assignments in territory
        let teamsRemovedCount = await database.db.result("DELETE FROM tw_plan WHERE guild_id = $1 AND territory = $2",
            [guildId, territoryNum], r => r.rowCount);
        
        return `Removed all teams (${teamsRemovedCount}) from Territory ${territoryNum}.`;
    }
}
