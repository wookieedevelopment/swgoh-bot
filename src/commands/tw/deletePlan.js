const discordUtils = require("../../utils/discord.js");
const twUtils = require("./utils.js");
const guildUtils = require("../../utils/guild");
const playerUtils = require("../../utils/player");
const database = require("../../database");
const { SlashCommandSubcommandBuilder } = require("@discordjs/builders");
const { ActionRowBuilder, ButtonBuilder, EmbedBuilder, ButtonStyle, ComponentType } = require('discord.js');
const { logger, formatError } = require("../../utils/log.js");

const YES_BUTTON_NAME = "tw:plan:delete:yes"
const NO_BUTTON_NAME = "tw:plan:delete:no"


module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("delete")
        .setDescription("Delete saved TW defense plan")
        .addStringOption(o =>
            o.setName("name")
            .setDescription("Name of plan to delete")
            .setRequired(true)
            .setAutocomplete(true)),
    permissionLevel: playerUtils.UserRoles.GuildAdmin,
    interact: async function(interaction)
    {
        let { botUser, guildId, error } = await playerUtils.authorize(interaction, this.permissionLevel)
        if (error) {
            await interaction.editReply(error);
            return;
        }

        let planName = interaction.options.getString("name");

        let planExists = await this.planExists(guildId, planName);

        if (!planExists)
        {
            await interaction.editReply(`There is no plan by the name **${planName}**.`);
            return;
        }

        const filter = async i => {
            await i.deferUpdate();
            return i.user.id === interaction.user.id;
        };

        // ask about replacing existing plan
        let message = await interaction.editReply(
            { 
                content: `Are you sure you want to delete plan **${planName}**? This cannot be undone.`,
                components: [
                    new ActionRowBuilder().addComponents(
                        new ButtonBuilder()
                            .setCustomId(YES_BUTTON_NAME)
                            .setLabel("Yes")
                            .setStyle(ButtonStyle.Success),
                        new ButtonBuilder()
                            .setCustomId(NO_BUTTON_NAME)
                            .setLabel("No")
                            .setStyle(ButtonStyle.Danger)
                    )
                ]
            });

        message.awaitMessageComponent({ filter, componentType: ComponentType.Button, time: 300000 })
            .then(async i => {
                if (i.customId == NO_BUTTON_NAME)
                    await interaction.editReply({ content: "Delete cancelled.", components: [] });
                else
                    await this.handleButton(interaction)
            })
            .catch(err => logger.error(formatError(err)));
    },
    autocomplete: async function(interaction)
    {
        
        let { botUser, guildId, error } = await playerUtils.authorize(interaction, this.permissionLevel)
        if (error) {
            await interaction.respond([{ name: "You do not have permission to do this.", value: "wwwwwwww" }]);
            return;
        }

        const focusedValue = interaction.options.getFocused();

        let names = await database.db.any(`
SELECT DISTINCT plan_name
FROM tw_plan_saved
WHERE guild_id = $1 AND plan_name ILIKE $2
LIMIT 25`,
[guildId, `%${focusedValue}%`])

		await interaction.respond(
			names
                .filter(n => n.plan_name.length < twUtils.MAX_PLAN_NAME_LENGTH)
                .map(choice => ({ name: choice.plan_name, value: choice.plan_name }))
		);
    },
    handleButton: async function(interaction)
    {
        let { botUser, guildId, error } = await playerUtils.authorize(interaction, this.permissionLevel)
        if (error) {
            await interaction.followUp(error);
            return;
        }

        let planName = interaction.options.getString("name");
        
        await this.deletePlan(guildId, planName);

        await interaction.editReply({ content: `Plan **${planName}** deleted.`, components: [] });
    },
    planExists: async function(guildId, planName)
    {
        let existingPlan = await database.db.any("SELECT COUNT(*) FROM tw_plan_saved WHERE guild_id = $1 AND plan_name = $2", [guildId, planName])
        return existingPlan[0].count > 0;
    },
    deletePlan: async function(guildId, planName)
    {
        if (!planName || planName > twUtils.MAX_PLAN_NAME_LENGTH)
        {
            return "Plan name must be between 1 and 100 characters.";
        }
        
        await database.db.any(
`DELETE FROM tw_plan_saved WHERE guild_id = $1 AND plan_name = $2`, [guildId, planName]
        )
    }
}
