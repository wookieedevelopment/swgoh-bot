const discordUtils = require("../../utils/discord.js");
const twUtils = require("./utils.js");
const guildUtils = require("../../utils/guild");
const playerUtils = require("../../utils/player");
const database = require("../../database");
const showPlan = require("./showPlan");
const { SlashCommandSubcommandBuilder } = require("@discordjs/builders");
const { ActionRowBuilder, ButtonBuilder, EmbedBuilder } = require('discord.js');
const { logger, formatError } = require("../../utils/log.js");

module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("load")
        .setDescription("Load a previously saved TW defense plan. Any unsaved changes will be lost.")
        .addStringOption(o =>
            o.setName("name")
            .setDescription("The name of the plan to load.")
            .setRequired(true)
            .setAutocomplete(true)),
    permissionLevel: playerUtils.UserRoles.GuildAdmin,
    interact: async function(interaction)
    {
        let { botUser, guildId, error } = await playerUtils.authorize(interaction, this.permissionLevel)
        if (error) {
            await interaction.editReply(error);
            return;
        }

        let planName = interaction.options.getString("name");

        let planExists = await this.planExists(guildId, planName);

        if (!planExists)
        {
            await interaction.editReply("You do not have a plan by that name.");
            return;
        }

        let response = await this.loadPlan(guildId, planName);
        await interaction.editReply(response);

        await interaction.followUp(await showPlan.generateDiscordMapFileResponse(guildId));
    },
    autocomplete: async function(interaction)
    {
        let { botUser, guildId, error } = await playerUtils.authorize(interaction, this.permissionLevel)
        if (error) {
            await interaction.respond([{ name: "You do not have permission to do this.", value: "wwwwwwww" }]);
            return;
        }

        const focusedValue = interaction.options.getFocused();

        const names = await database.db.any(
`select DISTINCT plan_name
from tw_plan_saved
WHERE guild_id = $1 AND plan_name ILIKE $2
LIMIT 25
`, [guildId, `%${focusedValue}%`]);

        await interaction.respond(
            names
            .filter(n => n.plan_name.length <= twUtils.MAX_PLAN_NAME_LENGTH)
            .map(choice => (
            { 
                name: choice.plan_name, 
                value: choice.plan_name 
            })));
    },
    planExists: async function(guildId, planName)
    {
        let existingPlan = await database.db.any("SELECT COUNT(*) FROM tw_plan_saved WHERE guild_id = $1 AND plan_name = $2", [guildId, planName])
        return existingPlan[0].count > 0;
    },
    loadPlan: async function(guildId, planName)
    {
        await database.db.any(
`DELETE FROM tw_plan WHERE guild_id = $1;
INSERT INTO tw_plan (guild_id, territory, team_name, priority, team_limit, min_defense_priority, max_defense_priority, for_allycode)
(SELECT guild_id, territory, team_name, priority, team_limit, min_defense_priority, max_defense_priority, for_allycode
FROM tw_plan_saved
WHERE guild_id = $1 AND plan_name = $2)
`, [guildId, planName]
        )

        return `Plan **${planName}** loaded. Showing plan...`;
    }
}
