
const { createCanvas, Image, loadImage } = require("canvas");
const database = require("../../database");
const { EmbedBuilder, ActionRowBuilder, ButtonBuilder, ButtonStyle, AttachmentBuilder, StringSelectMenuBuilder, StringSelectMenuOptionBuilder } = require("discord.js");
const playerUtils = require("../../utils/player")
const discordUtils = require("../../utils/discord");
const swapiUtils = require("../../utils/swapi")
const { logger, formatError } = require("../../utils/log");
const UNIT_DATA = require("../../data/unit_data.json");
const stats = require("../../data/stats.json")
const constants = require("../../utils/constants");
const tools = require("../../utils/tools");
const metadataCache = require("../../utils/metadataCache");
const unitsUtils = require("../../utils/units");
const _ = require("lodash");
const { parseAsync } = require("json2csv");

const MAX_TEAM_DESC_DISPLAY_LENGTH = 400;
const MAX_PRIORITY = 50;
const MAX_LIMIT = 50;
const RESERVED_TERRITORY = 0;
const NO_STAT_FOUND = null;
const OMICRON_FEATURE_KEY = "o";
const ZETA_FEATURE_KEY = "z";

const DATACRON_FILTERS = 
{
    AnyL9Match: "L9",
    AnyL6Match: "L6",
    AnyL3Match: "L3",
    None: "None"
};

const MATCH_TYPE = 
{
    Unit: 1,
    Ability: 2
};

const FEATURE_LOGIC = 
{
    OR: "OR",
    AND: "AND"
};

const FEATURE_LOGIC_SYMBOL_MAP = {
    ",": FEATURE_LOGIC.AND,
    "/": FEATURE_LOGIC.OR,
    "+": FEATURE_LOGIC.AND,
    "&": FEATURE_LOGIC.AND,
    "|": FEATURE_LOGIC.OR,
    "||": FEATURE_LOGIC.OR,
    "&&": FEATURE_LOGIC.AND
}



const TERRITORY_LABELS = [
    { label:"1", value: "1"},
    { label:"2", value: "2"},
    { label:"3", value: "3"},
    { label:"4", value: "4"},
    { label:"5", value: "5"},
    { label:"6", value: "6"},
    { label:"7", value: "7"},
    { label:"8", value: "8"},
    { label:"9", value: "9"},
    { label:"10", value: "10"}
];


const TERRITORY_LABELS_WITH_RESERVED = [{ label: "Reserved for Offense", value: RESERVED_TERRITORY.toString()}].concat(TERRITORY_LABELS);

const features = {
    options: [
        { 
            key: "z", 
            description: "Zetas", 
            abbreviation: "ZETA",
            formatString: function(c, v) { return `Zeta${isNaN(v) ? " =" : `s ${c}`} ${v}` },
            getUnitStat: function(unit)
            {
                return unit?.zeta_abilities;
            }
        },
        { 
            key: "zl", 
            description: "Zeta Lead", 
            abbreviation: "LZTA",
            formatString: function(c, v) { return `Zeta Lead: ${v > 0 ? `Yes` : `No`}` },
            getUnitStat: async function(unit)
            {
                let zetas = await swapiUtils.getUnitZetaAbilities(unit);
                let lz = zetas?.find(a => a.type === 3); // lead ability is type 3
                if (lz) return 1;
                return 0;
            }
        },
        { 
            key: OMICRON_FEATURE_KEY, 
            description: "Omicrons", 
            abbreviation: "OMI", 
            formatString: function(c, v) { return `Omicron${isNaN(v) ? " =" : `s ${c}`} ${v}` },
            getUnitStat: function(unit)
            {
                return unit?.omicron_abilities;
            }
        },
        { 
            key: "ol", 
            description: "Omicron Lead", 
            abbreviation: "LOMI", 
            formatString: function(c, v) { return `Omicron Lead: ${v > 0 ? `Yes` : `No`}` },
            getUnitStat: async function(unit)
            {
                let omis = await swapiUtils.getUnitOmicronAbilities(unit);
                let leadOmi = omis?.find(a => a.type === 3); // lead ability is type 3
                if (leadOmi) return 1;
                return 0;
            }
        },
        { key: "g", description: "Gear Level", abbreviation: "GEAR", formatString: function(c, v) { return `Gear ${c} ${v}` }, getUnitStat: function(unit) { return (unit == null || swapiUtils.getUnitCombatType(unit) != 1) ? NO_STAT_FOUND : swapiUtils.getUnitGearLevel(unit); } },
        { key: "r", description: "Relic Level", abbreviation: "REL", formatString: function(c, v) { return `Relic ${c} ${v}` }, getUnitStat: function(unit) { return (unit == null || swapiUtils.getUnitCombatType(unit) != 1) ? NO_STAT_FOUND : swapiUtils.getUnitRelicLevel(unit) - 2; } },
        { key: "s", description: "Stars", formatString: function(c, v) { return `Stars ${c} ${v}` }, getUnitStat: function(unit) { return unit == null ? null : swapiUtils.getUnitRarity(unit); } },
        { 
            key: "u", 
            description: "Has Ultimate (1=yes, 0=no)", 
            abbreviation: "ULT", 
            formatString: function(c, v) { return `Ultimate: ${v == 0 ? `No` : `Yes`}` }, 
            getUnitStat: function(unit, playerData)
            {
                if (unit == null) return 0;
                if (unit.purchasedAbilityId?.length >= 1) return 1;
                if (unit.data)
                {
                    if (unit.data.has_ultimate) return 1;
                    return 0;
                }
                return 0;
            }
        },
        { key: "speed", description: "Speed", abbreviation: "SPD", formatString: function(c, v) { return `Speed ${c} ${v}` }, getUnitStat: function(unit) { return getUnitFinalStatByName(unit, "Speed"); } },
        { key: "physical", description: "Physical Offence", abbreviation: "P.OF", formatString: function(c, v) { return `Phys. Off. ${c} ${v}` }, getUnitStat: function(unit) { return getUnitFinalStatByName(unit, "Physical Damage"); } },
        { key: "special", description: "Special Offence", abbreviation: "S.OF", formatString: function(c, v) { return `Spec. Off. ${c} ${v}` }, getUnitStat: function(unit) { return getUnitFinalStatByName(unit, "Special Damage"); } },
        { key: "critAvoidance", description: "Crit Avoidance", abbreviation: "CA", formatString: function(c, v) { return `CA ${c} ${v}%` }, getUnitStat: function(unit) { return getUnitFinalStatByName(unit, swapiUtils.STATS.PHYSICAL_CRITICAL_AVOIDANCE); } },
        { key: "critDamage", description: "Crit Damage", abbreviation: "CD", formatString: function(c, v) { return `CD ${c} ${v}%` }, getUnitStat: function(unit) { return getUnitFinalStatByName(unit, "Critical Damage"); } },
        { key: "critChance", description: "Crit Chance", abbreviation: "CC", formatString: function(c, v) { return `CC ${c} ${v}%` }, getUnitStat: function(unit) { return getUnitFinalStatByName(unit, "Physical Critical Chance"); } },
        { key: "tenacity", description: "Tenacity", abbreviation: "TEN", formatString: function(c, v) { return `Tenacity ${c} ${v}%` }, getUnitStat: function(unit) { return getUnitFinalStatByName(unit, "Tenacity"); } },
        { key: "potency", description: "Potency", abbreviation: "POT", formatString: function(c, v) { return `Potency ${c} ${v}%` }, getUnitStat: function(unit) { return getUnitFinalStatByName(unit, "Potency"); } },
        { key: "armor", description: "Armor", abbreviation: "ARM", formatString: function(c, v) { return `Armor ${c} ${v}%` }, getUnitStat: function(unit) { return getUnitFinalStatByName(unit, "Armor"); } },
        { key: "health", description: "Health", abbreviation: "HP", formatString: function(c, v) { return `Health ${c} ${v}` }, getUnitStat: function(unit) { return getUnitFinalStatByName(unit, "Health"); } },
        { key: "protection", description: "Protection", abbreviation: "PROT", formatString: function(c, v) { return `Prot. ${c} ${v}` }, getUnitStat: function(unit) { return getUnitFinalStatByName(unit, "Protection"); } },
        { key: "totalLife", description: "Total Life (Health + Protection)", abbreviation: "TL", formatString: function(c, v) { return `Total Life ${c} ${v}` }, getUnitStat: function(unit) { return getUnitFinalStatByName(unit, "Health") + getUnitFinalStatByName(unit, "Protection") } },
        { key: "EHP", description: "EHP (Health + Protection) / (1-Armor)", abbreviation: "EHP", formatString: function(c, v) { return `EHP ${c} ${v}` }, getUnitStat: function(unit) { return playerUtils.calculateEHP(getUnitFinalStatByName(unit, "Health"), getUnitFinalStatByName(unit, "Protection"), getUnitFinalStatByName(unit, "Armor")); } },
        { key: "accuracy", description: "Accuracy", abbreviation: "ACC", formatString: function(c, v) { return `Acc. ${c} ${v}` }, getUnitStat: function(unit) { return getUnitFinalStatByName(unit, swapiUtils.STATS.PHYSICAL_ACCURACY); } },
        { key: "resistance", description: "Resistance", abbreviation: "RES", formatString: function(c, v) { return `Resistance ${c} ${v}` }, getUnitStat: function(unit) { return getUnitFinalStatByName(unit, "Resistance"); } },
        { key: "gp", description: "GP", abbreviation: "GP", formatString: function(c, v) { return `GP ${c} ${v}` }, getUnitStat: function(unit) { return unit == null ? NO_STAT_FOUND : swapiUtils.getUnitGP(unit); } },
        { 
            key: "locked", 
            description: "Locked", 
            abbreviation: "LKD", 
            formatString: function(c, v) { return `${v === 0 ? "🔓 Unlocked" : "🔒 Locked"}` }, 
            getUnitStat: function(unit) { return unit == null ? 1 : 0; }
        }
    ],
    nullFeature: { key: "n", comparison: "=", value: 0 },
    allKeys: function () { return this.options.map(o => o.key) }
}

module.exports = {
    MAX_PRIORITY: MAX_PRIORITY,
    MAX_LIMIT: MAX_LIMIT,
    MAX_TEAM_DESC_DISPLAY_LENGTH: MAX_TEAM_DESC_DISPLAY_LENGTH,
    MAX_TEAM_NAME_LENGTH: discordUtils.MAX_DISCORD_AUTOCOMPLETE_CHOICE_LENGTH,
    MAX_PLAN_NAME_LENGTH: discordUtils.MAX_DISCORD_AUTOCOMPLETE_CHOICE_LENGTH,
    DATACRON_FILTERS: DATACRON_FILTERS,
    NO_DATACRON_SPECIFIED: constants.NO_DATACRON_SPECIFIED,
    RESERVED_TERRITORY: RESERVED_TERRITORY,
    TERRITORY_LABELS: TERRITORY_LABELS,
    TERRITORY_LABELS_WITH_RESERVED: TERRITORY_LABELS_WITH_RESERVED,
    features: features,
    NO_STAT_FOUND: NO_STAT_FOUND,
    isNullFeatureKey: isNullFeatureKey,
    getLockedFeature: function(unit)
    {
        if (!unit.features) return null;
        let f = unit.features.find(f => f.key.toLowerCase() == "locked");
        return f;
    }, 
    getLogicString: getLogicString,
    getFeatureString: getFeatureString,
    getExistingPlanAssignment: getExistingPlanAssignment,
    updatePlanAssignment: updatePlanAssignment,
    isFleetTerritory : function(tIndex) { return tIndex == 8 || tIndex == 5; },
    calculateTotalPoints: function (numSquads, numFleets) {
        return numSquads * 30 + numFleets * 34;
    },
    getTerritoryNameByNumber: function(territoryNum)
    {
        if (territoryNum === this.RESERVED_TERRITORY) return "Reserved";
        return territoryNum.toString();
    },
    getPlanResultsJsonAndCSVMessage: async function(results)
    {
        const twData = new Array();

        results.forEach(r => r.totalPoints = this.calculateTotalPoints(r.playerMetadata.numSquadsAssigned, r.playerMetadata.numFleetsAssigned));

        // sort by points descending, then name ascending
        results = results.sort((a, b) => 
            { 
                let diff = b.totalPoints - a.totalPoints;  
                if (diff != 0) return diff;
                return a.player.name.normalize().localeCompare(b.player.name.normalize())
            })

        for (var resultsIndex = 0; resultsIndex < results.length; resultsIndex++)
        {
            var result = results[resultsIndex];

            const playerTWObject = createPlayerTWAssignmentsObject(
                result.player.allycode, 
                result.player.name, 
                result.playerMetadata.numSquadsAssigned,
                result.playerMetadata.numFleetsAssigned);

                twData.push(playerTWObject);
            
            for (var ta = 0; ta < result.territoryAssignments.length; ta++)
            {
                // nothing to do if no teams
                if (result.territoryAssignments[ta].foundTeamsCount === 0) continue;

                // don't grab the offense teams
                if (ta === this.RESERVED_TERRITORY) continue;

                // #teams (team1, ...)
                var assignmentString = result.territoryAssignments[ta].foundTeamsCount + " (" + result.territoryAssignments[ta].foundTeamsString + ")";

                playerTWObject[`T${ta}`] = assignmentString;
            }
        }

        
        const jsonText = JSON.stringify(twData);
        const twDataBuffer = Buffer.from(jsonText);
        const jsonTwDataAttachment = new AttachmentBuilder(twDataBuffer, { name: `tw-defense-data-${new Date().toJSON()}.json`});

        const csvParsingOptions = { defaultValue: "" }
        const csvText = await parseAsync(twData, csvParsingOptions);
        const csvBuffer = Buffer.from(csvText);
        const csvAttachment = new AttachmentBuilder(csvBuffer, { name: `tw-defense-data-${new Date().getTime()}.csv`});
        

        const message = {
            files: [
                csvAttachment,
                jsonTwDataAttachment
            ]
        };

        return message;
    },
    generatePlanResults: function(results, teams, territories)
    {

        const width = 6000;
        const height = 3400;
        const canvas = createCanvas(width, height);
        const context = canvas.getContext('2d')
        const assignmentBgColor = "rgb(71, 184, 85)";
        const defaultFont = "24pt Arial";
        const defaultHeaderFont = "bold 30pt Arial"
        const defaultFontColor = "#000"

        context.fillStyle = '#ddd'
        context.fillRect(0, 0, width, height)


        var headerHeight = 100;
        var heightPerPlayer = (height - headerHeight)/results.length;
        var firstColWidth = 550; 
        var secondColWidth = 250;
        var firstAssignmentColumnX = firstColWidth + secondColWidth;
        var widthPerAssignmentColumn = (width - firstAssignmentColumnX)/10;
        var cellBuffer = 10;

        let defenseTeamsAndCounts = {};
        let offenseTeamsAndCounts = {};
        let playersWithoutSquads = "";
        let playersWithoutFleets = "";
        let territoriesNotMaxed = "";
        let glsPlaced = 0;

        // draw alternative row colors and lines
        for (var resultsIndex = 0; resultsIndex < results.length; resultsIndex++)
        {
            // fill row background
            context.fillStyle = (resultsIndex %2 == 0 ? "rgb(210, 226, 240)" : "rgb(167,199,224)");
            context.fillRect(0, headerHeight + resultsIndex*heightPerPlayer, width, headerHeight + resultsIndex*(heightPerPlayer+1));

            // draw line
            context.fillStyle = "#000";
            context.beginPath();
            context.moveTo(0, headerHeight + resultsIndex*heightPerPlayer);
            context.lineTo(width, headerHeight + resultsIndex*heightPerPlayer);
            context.stroke();
        }

        // draw vertical lines
        var territoryCount = 0;
        context.fillStyle = "#000";
        context.beginPath();
        context.moveTo(firstColWidth, 0);
        context.lineTo(firstColWidth, height);
        context.stroke();
        // for (var territoryIx in territories)
        // {
        //     context.beginPath();
        //     context.moveTo(firstAssignmentColumnX + territoryCount*widthPerAssignmentColumn, 0);
        //     context.lineTo(firstAssignmentColumnX + territoryCount*widthPerAssignmentColumn, height);
        //     context.stroke();
        //     territoryCount++;
        // }

        // write headers
        context.font = defaultHeaderFont;
        context.textAlign = "left"
        context.fillStyle = defaultFontColor;
        context.fillText("Player (Ally Code)", cellBuffer, headerHeight-cellBuffer);
        context.textAlign = "center";
        context.fillText("Points", firstColWidth + secondColWidth/2, headerHeight-cellBuffer);

        // write territory names and assignments count
        territoryCount = 0;
        for (var territoryIx = 1; territoryIx < territories.length; territoryIx++)
        {
            context.beginPath();
            context.moveTo(firstAssignmentColumnX + territoryCount*widthPerAssignmentColumn, 0);
            context.lineTo(firstAssignmentColumnX + territoryCount*widthPerAssignmentColumn, height);
            context.stroke();

            var territory = territories[territoryIx];
            
            context.textAlign = "center"
            context.fillText("Territory " + this.getTerritoryNameByNumber(territoryIx) + " (" + territory.curTeams.length + " teams)", firstAssignmentColumnX + (territoryCount+0.5)*(widthPerAssignmentColumn), headerHeight-cellBuffer, widthPerAssignmentColumn)
            territoryCount++;

            if (territories[territoryIx].curTeams.length != territories[territoryIx].maxTeams)
            {
                territoriesNotMaxed += `${this.getTerritoryNameByNumber(territoryIx)}: ${territories[territoryIx].curTeams.length}/${territories[territoryIx].maxTeams}\r\n`
            }
        }

        results.forEach(r => r.totalPoints = this.calculateTotalPoints(r.playerMetadata.numSquadsAssigned, r.playerMetadata.numFleetsAssigned));

        // sort by points descending, then name ascending
        results = results.sort((a, b) => 
            { 
                let diff = b.totalPoints - a.totalPoints;  
                if (diff != 0) return diff;
                return a.player.name.normalize().localeCompare(b.player.name.normalize())
            })

        for (var resultsIndex = 0; resultsIndex < results.length; resultsIndex++)
        {
            var result = results[resultsIndex];
            
            if (result.playerMetadata.numSquadsAssigned == 0)
            {
                playersWithoutSquads += `${result.player.name} (${result.player.allycode})\r\n`;
            }
            if (result.playerMetadata.numFleetsAssigned == 0)
            {
                playersWithoutFleets += `${result.player.name} (${result.player.allycode})\r\n`
            }
            var rowTop = headerHeight + heightPerPlayer*resultsIndex;
            var rowTextYPos = rowTop + heightPerPlayer*0.5;
            
            context.font = defaultFont;
            context.textAlign = "left"
            context.fillStyle = defaultFontColor;
            context.fillText(result.player.name + " (" + result.player.allycode + ")", cellBuffer, rowTextYPos + cellBuffer, firstColWidth)
            context.textAlign = "center"
            context.fillText(result.totalPoints, firstColWidth + secondColWidth/2, rowTextYPos + cellBuffer)

            const playerTWObject = createPlayerTWAssignmentsObject(
                result.player.allycode, 
                result.player.name, 
                result.playerMetadata.numSquadsAssigned,
                result.playerMetadata.numFleetsAssigned);
            
            for (var ta = 0; ta < result.territoryAssignments.length; ta++)
            {
                // nothing to do if no teams
                if (result.territoryAssignments[ta].foundTeamsCount === 0) continue;

                // catalog number of each team for offense reservations and defense for display as part of summary results
                result.territoryAssignments[ta].foundTeams.forEach(foundTeam =>
                    {
                        let teamName = foundTeam.name;
                        if (foundTeam.boundUnitDefIds?.find(d => UNIT_DATA.GalacticLegends[d])) glsPlaced++;

                        let teamsAndCounts = (ta === this.RESERVED_TERRITORY) ? offenseTeamsAndCounts : defenseTeamsAndCounts;

                        if (!teamsAndCounts[teamName])
                            teamsAndCounts[teamName] = 1;
                        else
                            teamsAndCounts[teamName]++;
                    });

                // no drawing the offense teams
                if (ta === this.RESERVED_TERRITORY) continue;

                // background for assignment cell
                context.fillStyle = assignmentBgColor;
                context.fillRect(firstAssignmentColumnX + (ta-1)*widthPerAssignmentColumn+1, rowTop+1, widthPerAssignmentColumn-2, heightPerPlayer-2);

                // #teams (team1, ...)
                var assignmentString = result.territoryAssignments[ta].foundTeamsCount + " (" + result.territoryAssignments[ta].foundTeamsString + ")";

                // write the text
                context.font = defaultFont;
                context.textAlign = "left"
                context.fillStyle = defaultFontColor;
                context.textAlign = "center"
                context.fillText(assignmentString, firstAssignmentColumnX + (ta-0.5)*(widthPerAssignmentColumn), rowTextYPos + cellBuffer)
            }
        }

        const buffer = canvas.toBuffer('image/png');

        let messagesToSend = [];
        
        messagesToSend.push({
            files: [
                {
                    attachment: buffer,
                    name: "tw-assignments-" + Date.now() + ".png"
                }
            ]
        });


        let defenseTeamsAndCountsStrings = this.getTeamsAndCountsStrings(defenseTeamsAndCounts);
        let offenseTeamsAndCountsStrings = this.getTeamsAndCountsStrings(offenseTeamsAndCounts);

        let summaryFields = new Array();

        // add Offense Reservations summary
        for (let i = 0; i < offenseTeamsAndCountsStrings.length; i+= 15)
        {
            summaryFields.push( {
                name: "Reserved Offense Teams Summary",
                value: 
                    "```" + 
                    "|           Team Name           | Count\r\n" +
                    "---------------------------------------\r\n" + offenseTeamsAndCountsStrings.slice(i, i+15).map(t => t.string).join("\r\n") + "```",
                inline: false });
        }

        for (let i = 0; i < defenseTeamsAndCountsStrings.length; i+= 15)
        {
            summaryFields.push( {
                name: "Defense Teams Summary",
                value: 
                    "```" + 
                    "|           Team Name           | Count\r\n" +
                    "---------------------------------------\r\n" + defenseTeamsAndCountsStrings.slice(i, i+15).map(t => t.string).join("\r\n") + "```",
                inline: false });
        }

        summaryFields.push({ name: "GLs Placed", value: glsPlaced.toString(), inline: false });

        let embed = new EmbedBuilder().setTitle("TW Plan Summary")
            .addFields(summaryFields)
            .setColor(0x336688);

        if (playersWithoutSquads.length > 0)
            embed.addFields([ { name: "Players without Defense Squads Assigned", value: playersWithoutSquads.slice(0, 1024) } ]);
        if (playersWithoutFleets.length > 0)
            embed.addFields([ { name: "Players without Defense Fleets Assigned", value: playersWithoutFleets.slice(0, 1024) } ]);
        if (territoriesNotMaxed.length > 0)
            embed.addFields([ { name: "Territories not Full", value: territoriesNotMaxed } ]);
        
        let buttonRow = new ActionRowBuilder();
        buttonRow.addComponents(
            new ButtonBuilder()
                .setCustomId("tw_def_assignment_details")
                .setLabel("See Defense Details")
                .setStyle(ButtonStyle.Primary),
            new ButtonBuilder()
                .setCustomId("tw_def_report")
                .setLabel("Show Visual Report")
                .setStyle(ButtonStyle.Success),
            new ButtonBuilder()
                .setCustomId("tw_def_json_csv")
                .setLabel("Download JSON/CSV")
                .setStyle(ButtonStyle.Secondary)
        );     

        let detailsRow = new ActionRowBuilder()
            .addComponents(
                new StringSelectMenuBuilder()
                    .setCustomId("tw:plan:zone")
                    .setPlaceholder("Show Assignments for Territory...")
                    .addOptions(TERRITORY_LABELS_WITH_RESERVED)
            );

        messagesToSend.push({ embeds: [embed], components: [buttonRow, detailsRow] });

        return messagesToSend;
    },
    getTeamsAndCountsStrings: function(teamsAndCounts)
    {
        const teamNameDisplayMaxLength = 32;
        let teamsAndCountsStrings = new Array();
        for (var teamName in teamsAndCounts)
        {

            teamsAndCountsStrings.push( 
                {
                    string: teamName.padEnd(teamNameDisplayMaxLength).substring(0, teamNameDisplayMaxLength) + "| " + teamsAndCounts[teamName].toString(),
                    count: teamsAndCounts[teamName]
                }
            );
        }

        teamsAndCountsStrings.sort((a, b) => b.count - a.count);

        return teamsAndCountsStrings;
    },
    getIndividualAssignmentResponse: async function(assignmentResult, teams, defenseMessage, includeRequiredAssignments)
    {
        let embeds = [];
        let totalPoints = this.calculateTotalPoints(assignmentResult.playerMetadata.numSquadsAssigned, assignmentResult.playerMetadata.numFleetsAssigned);
        
        let summaryMessage = 
`${includeRequiredAssignments ? `Place *all* teams assigned below.` : ""}
${assignmentResult.playerMetadata.numSquadsAssigned} squad${assignmentResult.playerMetadata.numSquadsAssigned == 1 ? "" : "s"} and ${assignmentResult.playerMetadata.numFleetsAssigned} fleet${assignmentResult.playerMetadata.numFleetsAssigned == 1 ? "" : "s"} (${totalPoints} total points)`;

        let embed = new EmbedBuilder()
            .setTitle(`TW Defense Assignments for ${assignmentResult.player.name} (${assignmentResult.player.allycode})`)
            .setDescription(summaryMessage)
            .setColor("Green");

        let fields = [];
        // let files = [];
        for (var ta = 1; ta < assignmentResult.territoryAssignments.length; ta++)
        {
            var assignment = assignmentResult.territoryAssignments[ta];

            // commented code below prototype for displaying unit images
            /*
            let teamsForTerritory = teams.filter(t => assignment.foundTeams.find(a => a.name === t.name))
            let imageBuffer = await this.getTeamImageBuffer(teamsForTerritory);
            const fileName = `territory_${ta}_teams.png`;
            const file = new AttachmentBuilder(imageBuffer, { name: fileName })
            files.push(file);

            let assignmentMessage = "```ini\r\n"
            for (var teamIx = 0; teamIx < assignment.foundTeams.length; teamIx++)
            {
                var team = teams.find(t => t.name == assignment.foundTeams[teamIx].name);
                assignmentMessage += this.getTeamAssignmentMessage(teamIx+1, team, assignment.foundTeams[teamIx].datacronString);
            }
            assignmentMessage += "```";

            let embed = new EmbedBuilder()
                .setTitle("**Territory** " + discordUtils.translateNumberToDiscordString(this.getTerritoryNameByNumber(ta)) + " - Place " + assignment.foundTeams.length + " team" + (assignment.foundTeams.length == 1 ? "" : "s"))
                .setDescription(assignmentMessage)
                .setColor("Green")
                .setImage("attachment://" + fileName);

            embeds.push(embed);
            */
            let field = {
                name: "",
                value: "",
                inline: false
            }

            field.name = "**Territory** " + discordUtils.translateNumberToDiscordString(this.getTerritoryNameByNumber(ta)) + " - Place " + assignment.foundTeams.length + " team" + (assignment.foundTeams.length == 1 ? "" : "s");
            
            field.value = "```ini\r\n"
            if (assignment.foundTeams.length == 0) field.value += "N/A";
            for (var teamIx = 0; teamIx < assignment.foundTeams.length; teamIx++)
            {
                var team = teams.find(t => t.name == assignment.foundTeams[teamIx].name);
                field.value += this.getTeamAssignmentMessage(teamIx+1, team, assignment.foundTeams[teamIx].datacronString);
            }
            field.value += "```";

            fields.push(field);
        }

        
        // part of unit image prototype
        // let message = `:warning: Important :warning: Your defense points should be ${totalPoints} after setting defense.`;

        if (includeRequiredAssignments)
        {
            let lastField = {
                name: ":warning: Important :warning:",
                value: `Your defense points should be ${totalPoints} after setting defense.`,
                inline: false
            }
    
            fields.push(lastField);    
        }

        embed.addFields(fields);

        embeds.push(embed);

        if (defenseMessage && defenseMessage.trim().length > 0)
        {
            let defenseMessageEmbed = new EmbedBuilder()
                .setTitle("Message from your Officers")
                .setColor("Purple")
                .setDescription(defenseMessage);

            embeds.push(defenseMessageEmbed);
        }
        return { embeds: embeds };

        // part of unit image prototype
        //return { embeds: embeds, content: message, files: files };
    },
    getTeamImageBuffer: async function(teams) {
        // units in team.units
        // unit.defId
        // unit.features (exclude any with locked)

        
        const PORTRAIT_WIDTH = 30;
        const PORTRAIT_HEIGHT = 30;
        const width = (PORTRAIT_WIDTH+3)*5;
        const height = (PORTRAIT_HEIGHT+5)*teams.length;
        const canvas = createCanvas(width, height);
        const context = canvas.getContext('2d')

        // context.fillStyle = "rgb(30, 30, 30)"
        // context.fillRect(0, 0, width, height);

        for (var teamIx = 0; teamIx < teams.length; teamIx++)
        {
            
            // write the team Index +1
            // context.font = "15pt Monospace"
            // context.textAlign = "left"
            // context.fillStyle = "#fff"
            // context.fillText((teamIx + 1).toString(), 5, teamIx*35 + 24);


            let team = teams[teamIx];

            let activeUnits = team.units.filter(u => {
                let lf = this.getLockedFeature(u);
                if (!lf) return true;
                return lf.logic === FEATURE_LOGIC.OR;
            });
            let portraitsToGet = activeUnits.map(u => u.defId);

            let portraitData = await database.db.any("SELECT unit_def_id, img FROM unit_portrait WHERE unit_def_id IN ($1:csv)", [portraitsToGet]);
            let yPos = teamIx*35;

            let teamLeaderUnitIndex = await this.guessTeamLeaderIndex(activeUnits);
            if (teamLeaderUnitIndex != 0)
            {
                let teamLeaderUnit = activeUnits[teamLeaderUnitIndex];
                activeUnits.splice(teamLeaderUnitIndex, 1);
                activeUnits.unshift(teamLeaderUnit);
            }

            for (let unitIx = 0; unitIx < activeUnits.length; unitIx++)
            {
                let xPos = (unitIx) * 33;

                let imgData = portraitData.find(p => p.unit_def_id === activeUnits[unitIx].defId).img;
                const unitIconImg = await loadImage(imgData);
                context.drawImage(unitIconImg, 
                    xPos, 
                    yPos, 
                    PORTRAIT_WIDTH, PORTRAIT_HEIGHT);
            }
        }
        
        return canvas.toBuffer("image/png");
    },
    guessTeamLeaderIndex: async function(teamUnits)
    {
        
        let unitsList = await swapiUtils.getUnitsList();
        if (teamUnits.length == 0) return null;

        // check for a GL and assume it's the leader
        let glOrCsIx = teamUnits.findIndex(u => u.is_galactic_legend || u.is_capital_ship);
        if (glOrCsIx >= 0) return glOrCsIx;

        // otherwise, loop through and look for the first unit with a leader ability
        for (var u = 0; u < teamUnits.length; u++)
        {
            
            let unit = unitsList.find(unit => swapiUtils.getUnitDefId(unit) === teamUnits[u].defId);
            if (!unit) continue; // wtf?

            if (unit.categories.indexOf("Leader") >= 0) return u;
        }
        
        return 0;
    },
    getTeamAssignmentMessage: function(number, team, datacronString)
    {
        return `${number}. [${team.name}] ${team.description.slice(0, this.MAX_TEAM_DESC_DISPLAY_LENGTH)}\r\n`
            + (datacronString ? `${``.padStart(number.toString().length + 2)}Datacron: ${datacronString}\r\n\r\n` : ``);
    },
    getExclusionsListEmbed: async function(guildId)
    {
        let currentExclusions = await database.db.any("SELECT tw_exclusion.allycode, guild_players.player_name FROM tw_exclusion LEFT JOIN guild_players USING (allycode) WHERE tw_exclusion.guild_id = $1 ORDER BY guild_players.player_name ASC", [guildId]);
        let message;
        if (currentExclusions.length == 0)
        {
            message = "No players are currently excluded from the war plan.";
        }
        else
        {
            message = `${currentExclusions.length} player${currentExclusions.length == 1 ? "" : "s"} excluded from this war:\r\n- ` + 
                currentExclusions.map(x => (x.player_name ?? `${discordUtils.symbols.warning} Unknown Player`) + " (" + x.allycode + ")").join("\r\n- ");
        }
        message += "\r\n\r\nTo exclude a player from the war (e.g., they did not join), specify an ally code.  For example: **/tw exclude player:123456789**";
        message += "\r\n\r\nTo include a player in the war who has been excluded, specify an ally code.  For example: **/tw include player:123456789**";
        message += "\r\n\r\nTo reset your exclusions, use **/tw include all:true**.";
        let embed = new EmbedBuilder().setTitle("TW - Excluded Players").setDescription(message).setColor(0x349910)
        return embed;
    },
    parseTeam: async function(stringToParse, allowMixedUnits = false)
    {

        var team = {
            combatType: 0,
            isSquad: function () { return this.combatType == 1; },
            units: [],
            hasGL: false
        }
        let normalizedStringToParse = tools.removeAccents(stringToParse)
        
        var teamRx = /([\w\-&()'\s]+)\s*\(([<>=\w,&\|+/\s]*)\)/g
        var featureRx = /([&|/|,|\||+]){0,2}\s*([\w]+)\s*(<|>|=|<=|>=)\s*([\w]+)\s*/g
        
        let match = teamRx.exec(normalizedStringToParse);


        if (match == null)
        {
            throw new Error(`Input was not in the correct format: ${stringToParse}`);
        }

        const unitsList = await swapiUtils.getUnitsList();
        const abilitiesList = await swapiUtils.getAbilitiesList();
        
        do {
            var unit = { name: null, defId: null, features: [] };
            unit.name = match[1].trim();

            let unitMatches = this.findUnitMatches(unit.name, unitsList);
            let errorMessage = this.createMatchesErrorString(MATCH_TYPE.Unit, unitMatches, unit.name);

            if (errorMessage) throw new Error(errorMessage);

            unit.name = unitMatches[0].name;
            unit.defId = unitMatches[0].base_id;

            if (!team.combatType)
            {
                team.combatType = unitMatches[0].combat_type;
            } 
            else if (!allowMixedUnits && team.combatType != unitMatches[0].combat_type)
            {
                let combatTypeName = (team.combatType == 1) ? "character" : "ship";
                throw new Error("You cannot mix character and fleet units in the same team. " + unit.name + " is not a " + combatTypeName + ".");
            }

            var features = match[2].trim();
            let featuresMatch = featureRx.exec(features);
            
            if (featuresMatch != null)
            {
                do {
                    // feature logic by default is AND (if nothing provided)
                    let featureLogic = featuresMatch[1]?.trim(); 
                    if (featureLogic) featureLogic = FEATURE_LOGIC_SYMBOL_MAP[featureLogic];
                    else featureLogic = FEATURE_LOGIC.AND;
                    
                    var featureKey = featuresMatch[2].trim();
                    var featureComparison = featuresMatch[3].trim();
                    var featureValue = featuresMatch[4].trim();

                    if (this.features.options.find(o => o.key.toLowerCase() == featureKey) == null) {
                        throw new Error("Unit feature not found: " + featureKey);
                    }

                    if (isNaN(featureValue))
                    {
                        if (featureKey === OMICRON_FEATURE_KEY || featureKey === ZETA_FEATURE_KEY)
                        {
                            // compare to an ability name
                            let valueAbilityMatches = this.findAbilityMatches(featureValue, unit.defId, abilitiesList);
                            let err = this.createMatchesErrorString(MATCH_TYPE.Ability, valueAbilityMatches, featureValue);
                            if (err) throw new Error(err);

                            featureValue = valueAbilityMatches[0].base_id;
                        }
                        else
                        {
                            // see if it's a compare to another unit
                            let valueUnitMatches = this.findUnitMatches(featureValue, unitsList);
                            let err = this.createMatchesErrorString(MATCH_TYPE.Unit, valueUnitMatches, featureValue);
                            if (err) throw new Error(err);

                            featureValue = valueUnitMatches[0].base_id;
                        }
                    }
                    
                    var feature = { key: featureKey, value: featureValue, comparison: featureComparison, logic: featureLogic };
                    unit.features.push(feature);

                } while ((featuresMatch = featureRx.exec(features)) != null)
            } else {
                unit.features.push(this.features.nullFeature)
            }
            
            let lf = this.getLockedFeature(unit);
            if (UNIT_DATA.GalacticLegends[unit.defId] && (!lf || lf.logic === FEATURE_LOGIC.OR)) team.hasGL = true;

            team.units.push(unit);

        } while ((match = teamRx.exec(normalizedStringToParse)) != null)

        return team;
    },
    findUnitMatches: function(name, unitsList)
    {
        let cutUnitName = tools.removeAccents(name).replace(/\W/g, "").toLowerCase();

        const unitNicknames = metadataCache.get(metadataCache.CACHE_KEYS.unitNicknames);
        let nicknameMatch = unitNicknames.lowerUnitNicknames[cutUnitName];
        let unitMatches;

        if (nicknameMatch) {
            unitMatches = unitsList.filter((u) => swapiUtils.getUnitDefId(u) === nicknameMatch);

            if (unitMatches.length === 1) return unitMatches;

            // typo in the unit_nicknames.json file
            logger.error(`There may be a typo in the unit_nicknames.json. Match found for ${cutUnitName} but no unit ${nicknameMatch} exists in API.`)
        }

        let exactMatch = null;

        unitMatches = unitsList.filter((u) => 
            {
                if (exactMatch) return false;
                let cutNameKey = tools.removeAccents(u.name).replace(/\W/g, "").toLowerCase();
                
                if (cutNameKey == cutUnitName)
                {
                    exactMatch = u;
                    return true;
                }
                return cutNameKey.indexOf(cutUnitName) >= 0;
            }
        );

        if (exactMatch)
        {
            return [exactMatch];
//            unitMatches = unitMatches.filter((u) => { return u.name.replace(/\W/g, "").toLowerCase() == cutUnitName });
        }

        return unitMatches;
    },
    findAbilityMatches: function(name, characterBaseId, abilitiesList)
    {
        let cutAbilityName = tools.removeAccents(name).replace(/\W/g, "").toLowerCase();

        let exactMatch = null;

        let abilityMatches = abilitiesList.filter((a) =>
            {
                if (characterBaseId && a.character_base_id != characterBaseId) return false;
                if (exactMatch) return false;
                let cutNameKey = tools.removeAccents(a.name).replace(/\W/g, "").toLowerCase();

                if (cutNameKey == cutAbilityName)
                {
                    exactMatch = a;
                    return true;
                }
                return cutNameKey.indexOf(cutAbilityName) >= 0;
            }
        );

        if (exactMatch)
        {
            return [exactMatch];
        }

        return abilityMatches;
    },
    createMatchesErrorString: function(matchType, matches, origName)
    {
        if (!matches || matches.length == 0)
        {
            return `No ${matchType === MATCH_TYPE.Unit ? "unit" : "ability"} found matching "${origName}".`;
        }

        if (matches.length > 1) {
            var errorString = `${matches.length} possible matches found for "${origName}"`;
            if (matches.length > 5) errorString += ". First five:\r\n"
            else errorString += ":\r\n"
            for (var u = 0; u < 5 && u < matches.length; u++)
            {
                errorString += matches[u].name + (u != 4 && u != matches.length-1 ? ", " : "");
            }
            return errorString;
        }

        return null;
    },
    findTeam: async function(guildId, teamName, isDefenseTeam)
    {
        const teamData = await database.db.any(
            `SELECT tw_team.tw_team_id, name,description,unit_def_id,feature,value,comparison,logic,is_squad,has_gl,datacron_filter,datacrons, datacrons_required, portrait
            FROM tw_team 
            JOIN tw_team_unit on tw_team.tw_team_id = tw_team_unit.tw_team_id 
            WHERE guild_id = $1 AND tw_team.name = $2 AND is_defense_team = $3
            ORDER BY name ASC`, [guildId, teamName, isDefenseTeam]);

        let team = this.mapTeamsDataToObject(teamData);
        return team ? team[0] : null;
    },
    getTeamsList: async function (guildId, isDefenseTeam) {
        const teamsData = await database.db.any(
            `select tw_team.tw_team_id, name,description,unit_def_id,feature,value,comparison,logic,is_squad,has_gl,datacrons,datacrons_required,datacron_filter, portrait
            from tw_team join tw_team_unit on tw_team.tw_team_id = tw_team_unit.tw_team_id 
            WHERE guild_id = $1 AND is_defense_team = $2 
            ORDER BY tw_team_id ASC, tw_team_unit_id ASC`,
             [guildId, isDefenseTeam]);
        return this.mapTeamsDataToObject(teamsData);
    },
    mapTeamsDataToObject: function(teamsData)
    {
        if (!teamsData) return null;
        
        var teams = new Array();
        var team, unit;
        for (var teamIx in teamsData) {
            var row = teamsData[teamIx];

            team = teams.find(t => t.id == row.tw_team_id);

            if (team == null) {
                team = { 
                    id: row.tw_team_id, 
                    name: row.name, 
                    description: row.description, 
                    isSquad: row.is_squad, 
                    isFleet: !row.is_squad, 
                    hasGL: row.has_gl, 
                    datacronFilter: row.datacron_filter,
                    datacrons: row.datacrons,
                    datacronsRequired: row.datacrons_required,
                    portrait: row.portrait,
                    units: new Array() };
                teams.push(team);
            }

            unit = team.units.find(u => u.defId == row.unit_def_id);
            if (!unit)
            {
                unit = { defId: row.unit_def_id, features: new Array() };
                team.units.push(unit)
            }

            if (this.features.options.find(f => f.key.toLowerCase() == row.feature.toLowerCase()))
            {
                unit.features.push({ key: row.feature, value: row.value, comparison: row.comparison, logic: row.logic })
            }
        }

        return teams;
    },
    isTeamViable: async function (playerData, team) {
        var roster = swapiUtils.getPlayerRoster(playerData);

        if (!roster || roster.length == 0) return createIsViableResponse(false, "Player has no units.", null);

        let dcs = swapiUtils.getPlayerDatacrons(playerData);

        if (dcs != null && dcs.length > 0)
        {
            let datacronMatch = true;

            let playerDcOptions = dcs;

            // first, limit the set of possible datacrons to search on to only those that match ALL abilities specified in datacronsRequired
            if (team.datacronsRequired)
            {
                playerDcOptions = swapiUtils.getPlayerDatacronWithAllWookieeBotDatacronIds(playerData, team.datacronsRequired.filter(r => r != this.NO_DATACRON_SPECIFIED));

                // make sure we have at least one
                if (playerDcOptions.length === 0) return createIsViableResponse(false, "No matching datacrons.", null); 
            }

            if (team.datacrons)
            {
                // second, look in the  list of possible matches (filtered by the datacronsRequired) to find the first that matches one of the
                // optional datacron abilities.
                for (var datacronId of team.datacrons)
                {
                    if (datacronId === this.NO_DATACRON_SPECIFIED) continue;

                    datacronMatch = false;

                    let crons = playerDcOptions.filter(c => c.affix && c.affix.find(af => af.wookieeBotDatacronId == datacronId));

                    if (crons?.length > 0) {
                        datacronMatch = true;
                        playerDcOptions = crons; // update so we can further limit the search when we apply a filter (if specified)
                        break;
                    }
                }
            }

            // none of the possible datacrons matched any of the optional abilities
            if (!datacronMatch) return createIsViableResponse(false, "No available matching datacron.", null);

            // at this point, if there's a filter, we search the playerDcOptions list to see whether we can find one that has a match at the
            // specified filter level. 
            if (team.datacronFilter)
            {
                let match = true;
                switch (team.datacronFilter)
                {
                    case DATACRON_FILTERS.AnyL9Match:
                        match = playerDcOptions.find(d => d.meta && team.units.find(u => u.defId === d.meta.character))
                        break;
                        
                    case DATACRON_FILTERS.AnyL6Match:
                        match = playerDcOptions.find(d => d.meta && team.units.find(u => roster.find(ru => swapiUtils.getUnitDefId(ru) === u.defId)?.categoryId.indexOf(d.meta.faction) != -1));
                        break;
                        
                    case DATACRON_FILTERS.AnyL3Match:
                        match = playerDcOptions.find(d => d.meta && team.units.find(u => roster.find(ru => swapiUtils.getUnitDefId(ru) === u.defId)?.categoryId.indexOf(d.meta.alignment) != -1));
                        break;

                }

                if (!match) return createIsViableResponse(false, "No available matching datacron.", null);
            }
        } 
        else if (
            (team.datacronsRequired && team.datacronsRequired.filter(r => r != this.NO_DATACRON_SPECIFIED).length > 0) || 
            (team.datacrons && team.datacrons.filter(d => d != this.NO_DATACRON_SPECIFIED).length > 0) || team.datacronFilter)
        {
            // team requires a datacron, but the player has no datacrons
            return createIsViableResponse(false, "Player has no datacrons.", null);
        }
        
        let boundUnitDefIds = new Array();

        for (var teamUnitIndex in team.units) {
            let teamUnit = team.units[teamUnitIndex];

            if (teamUnit.defId == "") {
                continue;
            }

            let rosterUnit = roster.find(ru => swapiUtils.getUnitDefId(ru) === teamUnit.defId);

            if (teamUnit.features.length == 0)
            {
                if (!rosterUnit) {
                    const unitName = (await swapiUtils.getUnitsList()).find(u => swapiUtils.getUnitDefId(u) === teamUnit.defId).name;
                    return createIsViableResponse(false, `Does not have ${unitName}.`, null);
                } 

                if (!boundUnitDefIds.find(u => u == teamUnit.defId)) boundUnitDefIds.push(teamUnit.defId);
                continue;
            }

            let orResult = null, orFailuresTexts = new Array();
            let lockedFeatureMatch = false;

            for (var featureIx = 0; featureIx < teamUnit.features.length; featureIx++)
            {
                let failText = null;
                let feature = this.features.options.find(f => f.key.toLowerCase() == teamUnit.features[featureIx].key.toLowerCase());
                if (!feature) continue;

                let value = teamUnit.features[featureIx].value;
                let logic = teamUnit.features[featureIx].logic;

                // we already have a match for an OR feature, so we can continue
                if (logic === FEATURE_LOGIC.OR && orResult === true) continue;

                // check if it's a unit or ability
                // ability values stay as the base_id, unit def_ids get turned into the actual unit in the roster
                if (isNaN(value) && !rosterUnit?.skill?.find(a => a.id === value))
                {
                    let unitMatch = roster.find(ru => swapiUtils.getUnitDefId(ru) === value);
                    if (!unitMatch) failText = `Does not have ${value}.`;
                    value = unitMatch;
                }

                if (!await compare(feature, playerData, rosterUnit, teamUnit.features[featureIx].comparison, value))
                {
                    let unitName;
                    if (!rosterUnit) unitName = (await swapiUtils.getUnitsList()).find(u => swapiUtils.getUnitDefId(u) === teamUnit.defId).name;
                    else unitName = rosterUnit.name;
    
                    failText = `Failed: ${unitName} (${getFeatureString(teamUnit.features[featureIx].key, teamUnit.features[featureIx].comparison, teamUnit.features[featureIx].value)})`;
                } 
                else if (logic === FEATURE_LOGIC.OR)
                {
                    orResult = true;
                }
                
                if (failText)
                {
                    // if it's an AND, then we can fail immediately
                    if (logic == null || logic === FEATURE_LOGIC.AND) return createIsViableResponse(false, failText, null);
                    
                    // if it's an OR, then we can keep going to see if we have a positive OR match
                    if (logic === FEATURE_LOGIC.OR) orResult ||= false;
                    orFailuresTexts.push(failText);
                }
                else if (feature.key == "locked")
                {
                    // it matched the locked feature, which we'll need to add as binding if the unit matches
                    lockedFeatureMatch = true;
                }
            }

            if (orResult === false) {
                let failText;
                if (orFailuresTexts.length == 1) failText = orFailuresTexts[0];
                else failText = "Does not match any of the 'or' features.";
                return createIsViableResponse(false, failText, null);
            }

            if (!lockedFeatureMatch && !boundUnitDefIds.find(u => u == teamUnit.defId)) boundUnitDefIds.push(teamUnit.defId);
        }

        return createIsViableResponse(true, null, boundUnitDefIds);
    },
    // match based on filter with %filter% in the database query
    getAllMatchingTeamsDescriptionAsEmbedField: async function(guildId, filter, isDefense)
    {
        let teamsData = await database.db.any(`
        SELECT name, description, datacron_filter, 
                (SELECT short_description AS dc1 FROM datacron WHERE datacron_id = tt.datacrons[1]),
                (SELECT short_description AS dc2 FROM datacron WHERE datacron_id = tt.datacrons[2]),
                (SELECT short_description AS dc3 FROM datacron WHERE datacron_id = tt.datacrons[3]),
                (SELECT short_description AS dc4 FROM datacron WHERE datacron_id = tt.datacrons[4]),
                (SELECT short_description AS dc5 FROM datacron WHERE datacron_id = tt.datacrons[5]),
                (SELECT short_description AS dcr1 FROM datacron WHERE datacron_id = tt.datacrons_required[1]),
                (SELECT short_description AS dcr2 FROM datacron WHERE datacron_id = tt.datacrons_required[2]),
                tu.def_id_array, tu.feature_array, tu.value_array, tu.comparison_array, tu.logic_array
        FROM   tw_team tt
        JOIN 
        (SELECT tw_team_id, array_agg(unit_def_id) as def_id_array, array_agg(feature)::varchar[] as feature_array, array_agg(value) as value_array, array_agg(comparison) as comparison_array, array_agg(logic) as logic_array
            FROM   tw_team_unit
            GROUP BY tw_team_id
        ) tu USING (tw_team_id)
        WHERE tt.guild_id = $1 AND tt.name like $2 AND tt.is_defense_team = $3
        LIMIT 10`
        , [guildId, `%${filter}%`, isDefense]);

        return await getTeamsDataDescriptionsAsEmbedFields(teamsData, filter);
    },
    // match based on exact team name
    getTeamDescriptionAsEmbedField: async function(guildId, teamName, isDefense)
    {
        let teamsData = await database.db.any(`
        SELECT name, description, datacron_filter, 
                (SELECT short_description AS dc1 FROM datacron WHERE datacron_id = tt.datacrons[1]),
                (SELECT short_description AS dc2 FROM datacron WHERE datacron_id = tt.datacrons[2]),
                (SELECT short_description AS dc3 FROM datacron WHERE datacron_id = tt.datacrons[3]),
                (SELECT short_description AS dc4 FROM datacron WHERE datacron_id = tt.datacrons[4]),
                (SELECT short_description AS dc5 FROM datacron WHERE datacron_id = tt.datacrons[5]),
                (SELECT short_description AS dcr1 FROM datacron WHERE datacron_id = tt.datacrons_required[1]),
                (SELECT short_description AS dcr2 FROM datacron WHERE datacron_id = tt.datacrons_required[2]),
                tu.def_id_array, tu.feature_array, tu.value_array, tu.comparison_array, tu.logic_array
        FROM   tw_team tt
        JOIN 
        (SELECT tw_team_id, array_agg(unit_def_id) as def_id_array, array_agg(feature)::varchar[] as feature_array, array_agg(value) as value_array, array_agg(comparison) as comparison_array, array_agg(logic) as logic_array
            FROM   tw_team_unit
            GROUP BY tw_team_id
        ) tu USING (tw_team_id)
        WHERE tt.guild_id = $1 AND tt.name = $2 AND tt.is_defense_team = $3
        LIMIT 10`
        , [guildId, teamName, isDefense]);

        return await getTeamsDataDescriptionsAsEmbedFields(teamsData, teamName);
    }
}

function createPlayerTWAssignmentsObject(allycode, name, numSquadsAssigned, numFleetsAssigned)
{
    return {
        allycode: allycode,
        name: name,
        totalSquads: numSquadsAssigned,
        totalFleets: numFleetsAssigned
    }
}

function isNullFeatureKey(key)
{
    return key == features.nullFeature.key;
}

function getLogicString(logic)
{
    switch (logic)
    {
        case FEATURE_LOGIC.AND: return "&";
        case FEATURE_LOGIC.OR: return "|";
        default: return "";
    }
}

function getFeatureString(feature, comparison, value)
{
    var feature = features.options.find(f => f.key.toLowerCase() == feature.toLowerCase());
    if (feature === null) return "";
    
    return feature.formatString(comparison, value);
}

async function getExistingPlanAssignment(guildId, teamName, territoryNum, forAllyCode)
{
    let params = [guildId, teamName, territoryNum];
    
    if (forAllyCode)
    {
        params.push(forAllyCode);
        forClause = "AND for_allycode = $4";
    }
    else
    {
        forClause = "AND for_allycode IS NULL"
    }

    const existingTeam = await database.db.oneOrNone(`SELECT * FROM tw_plan WHERE guild_id = $1 AND team_name = $2 AND territory = $3 ${forClause}`, params);

    return existingTeam;
}

async function updatePlanAssignment(priority, limit, maxDefensePriority, minDefensePriority, guildId, teamName, territoryNum, forAllyCode)
{
    let params = [priority, limit, maxDefensePriority, minDefensePriority, guildId, teamName, territoryNum];

    if (forAllyCode)
    {
        params.push(forAllyCode);
        forClause = "AND for_allycode = $8";
    }
    else
    {
        forClause = "AND for_allycode IS NULL";
    }

    await database.db.none(`UPDATE tw_plan SET priority = $1, team_limit = $2, max_defense_priority = $3, min_defense_priority = $4 WHERE guild_id = $5 AND team_name = $6 AND territory = $7 ${forClause}`,
        params)
}

async function getTeamsDataDescriptionsAsEmbedFields(teamsData, filter)
{
    if (!teamsData || teamsData.length === 0)
    {
        return [{ 
            name: "No Match Found",
            value: `No teams found matching or similar to **${filter}**.  Please double check your team list.`,
            inline: false
        }];
    }

    let fields = new Array();

    for (var teamIx in teamsData)
    {
        var team = teamsData[teamIx];

        fields.push({
            name: ">>>>  " + team.name + "  <<<<",
            value: await getTeamDescription(team, teamsData.length === 1 ? true : false),
            inline: false
        });

        if (teamsData.length <= 3)
        {
            fields.push({
                name: "Features Text",
                value: await getTeamAddString(team),
                inline: false
            });
        }
    }

    return fields;
}

async function getTeamAddString(team)
{
    let output = `/tw team add team:${team.name} features:`;
    let unitsList = await swapiUtils.getUnitsList();
    let abilitiesList = await swapiUtils.getAbilitiesList();

    let unitsInTeam = new Set(team.def_id_array);

    unitsInTeam.forEach((unitDefId) => {
        let unit = unitsList.find(u => swapiUtils.getUnitDefId(u) === unitDefId);
        let unitShortenedName = unitsUtils.getUnitNicknameByDefId(unitDefId) ?? tools.removeAccents(unit.name).replace(/\W/g, "").toLowerCase();
        output += `${unitShortenedName}(`;
        let featureString = "";
        for (let j = 0; j < team.def_id_array.length; j++)
        {
            if (team.def_id_array[j] != unitDefId) continue;
            if (isNullFeatureKey(team.feature_array[j])) continue;

            let value = team.value_array[j];
            let valueText;
            if (isNaN(value))
            {
                if (value.indexOf("_") != -1) // likely an ability
                {
                    let ability = abilitiesList.find(a => a.base_id === value);
                    if (ability) valueText = tools.removeAccents(ability.name).replace(/\W/g, "").toLowerCase();
                }

                if (!valueText)
                {
                    let unitMatch = unitsList.find(u => swapiUtils.getUnitDefId(u) === value);
                    if (unitMatch) {
                        valueText = unitsUtils.getUnitNicknameByDefId(value) ?? tools.removeAccents(unitMatch.name).replace(/\W/g, "").toLowerCase();
                    }
                }
            }

            if (!valueText) valueText = value;
            
            let logicString = (team.logic_array[j] === FEATURE_LOGIC.OR) ? "|" : "&"; // null means AND by default
            featureString += `${logicString}${team.feature_array[j]}${team.comparison_array[j]}${valueText}`;
        }
        output += `${featureString});`;
    });

    return output;
}

async function getTeamDescription(team, expandedTeam)
{
    let output = "**Description**: " + (team.description ? team.description.slice(0, MAX_TEAM_DESC_DISPLAY_LENGTH) : "None.  Set with **/tw team describe**.") + "\r\n"
            + "**Units**:\r\n";
    
    var units = new Array();
    for (var i = 0; i < team.def_id_array.length; i++)
    {
        let unit = units.find(u => u.baseId == team.def_id_array[i]);
        if (!unit) 
        {
            unit = { baseId: team.def_id_array[i], features: new Array() };
            units.push(unit);
        }
        if (isNullFeatureKey(team.feature_array[i]))
            unit.features.push(features.nullFeature);
        else 
            unit.features.push({ key: team.feature_array[i], value: team.value_array[i], comparison: team.comparison_array[i], logic: team.logic_array[i] });
    }

    var unitsList = await swapiUtils.getUnitsList();
    const abilitiesList = await swapiUtils.getAbilitiesList();

    if (expandedTeam) output += "```gauss\n";
    let unitStrings = units.map(unit => {
        let unitDef = unitsList.find(u => swapiUtils.getUnitDefId(u) === unit.baseId);
        let str = unitDef.name;
        if (unit.features.length > 0 && unit.features[0] != features.nullFeature)
        {
            unit.features.sort((a, b) => (''+a.logic).localeCompare(b.logic));

            if (expandedTeam)
            {
                str += "\n";
                for (var f = 0; f < unit.features.length; f++)
                {
                    let value = unit.features[f].value;
                    let unitMatch = unitsList.find(u => swapiUtils.getUnitDefId(u) === value);
                    if (unitMatch) value = unitMatch.name;
                    else
                    {
                        let abilityMatch = abilitiesList.find(a => a.base_id === value);
                        if (abilityMatch) value = abilityMatch.name;
                    }

                    str += `    ${unit.features[f].logic ?? FEATURE_LOGIC.AND} `;
                    str += getFeatureString(unit.features[f].key, unit.features[f].comparison, value)
                    str += "\n";
                }
            }
            else
            {
                str += " (";
                for (var f = 0; f < unit.features.length; f++)
                {
                    let value = unit.features[f].value;
                    let unitMatch = unitsList.find(u => swapiUtils.getUnitDefId(u) === value);
                    if (unitMatch) value = unitMatch.name;
                    else
                    {
                        let abilityMatch = abilitiesList.find(a => a.base_id === value);
                        if (abilityMatch) value = abilityMatch.name;
                    }

                    str += ` ${getLogicString(unit.features[f].logic)} `;
                    str += getFeatureString(unit.features[f].key, unit.features[f].comparison, value)
                }
                str += ")";
            }
        }
        return str;
    });
    
    for (let s = 0; s < unitStrings.length; s++)
    {
        output += (s+1) + ") " + unitStrings[s] + "\n";
    }

    if (expandedTeam) output += "```";

    let datacronsSpecified = (team.datacron_filter != null || team.dc1 != null || team.dc2 != null || team.dc3 != null || team.dc4 != null || team.dc5 != null || team.dcr1 != null || team.dcr2 != null );
    output += "**Datacrons:**" + (datacronsSpecified ? "\r\n" : " None specified.");
    switch (team.datacron_filter)
    {
        case DATACRON_FILTERS.AnyL9Match:
            output += "Any Matching L9\r\n";
            break;
        case DATACRON_FILTERS.AnyL6Match:
            output += "Any Matching L6\r\n";
            break;
        case DATACRON_FILTERS.AnyL3Match:
            output += "Any Matching L3\r\n"
            break;
    }

    let datacronsString = "";
    if (team.dc1) datacronsString += `${team.dc1}`;
    if (team.dc2) datacronsString += (datacronsString.length > 0 ? " -OR-\r\n" : "" ) + `${team.dc2}\r\n`;
    if (team.dc3) datacronsString += (datacronsString.length > 0 ? " -OR-\r\n" : "" ) + `${team.dc3}\r\n`;
    if (team.dc4) datacronsString += (datacronsString.length > 0 ? " -OR-\r\n" : "" ) + `${team.dc4}\r\n`;
    if (team.dc5) datacronsString += (datacronsString.length > 0 ? " -OR-\r\n" : "" ) + `${team.dc5}\r\n`;
    if (team.dcr1) datacronsString += (datacronsString.length > 0 ? " -AND-\r\n" : "" ) + `${team.dcr1}\r\n`;
    if (team.dcr2) datacronsString += (datacronsString.length > 0 ? " -AND-\r\n" : "" ) + `${team.dcr2}\r\n`;

    output += datacronsString;

    return output;
}

const swgohGGStatManipulations = {
    "0": (stat) => { return stat; },
    "1": (stat) => { return stat; },
    "2": (stat) => { return stat; },
    "3": (stat) => { return stat; },
    "4": (stat) => { return stat; },
    "5": (stat) => { return stat; },
    "6": (stat) => { return stat; },
    "7": (stat) => { return stat; },
    "8": (stat) => { return stat; },
    "9": (stat) => { return stat; },
    "10": (stat) => { return stat; },
    "11": (stat) => { return stat; },
    "12": (stat) => { return stat; },
    "13": (stat) => { return stat; },
    "14": (stat) => { return stat; },
    "15": (stat) => { return stat; },
    "16": (stat) => { return stat*100; },
    "17": (stat) => { return stat*100; },
    "18": (stat) => { return stat*100; },
    "19": (stat) => { return stat; },
    "20": (stat) => { return stat; },
    "21": (stat) => { return stat; },
    "22": (stat) => { return stat; },
    "23": (stat) => { return stat; },
    "24": (stat) => { return stat; },
    "25": (stat) => { return stat; },
    "26": (stat) => { return stat; },
    "27": (stat) => { return stat*100; },
    "28": (stat) => { return stat; },
    "29": (stat) => { return stat; },
    "30": (stat) => { return stat; },
    "31": (stat) => { return stat; },
    "32": (stat) => { return stat; },
    "33": (stat) => { return stat; },
    "34": (stat) => { return stat; },
    "35": (stat) => { return stat; },
    "36": (stat) => { return stat; },
    "37": (stat) => { return stat; },
    "38": (stat) => { return stat; },
    "39": (stat) => { return stat; },
    "40": (stat) => { return stat; },
    "41": (stat) => { return stat; },
    "42": (stat) => { return stat; },
    "43": (stat) => { return stat; },
    "44": (stat) => { return stat; },
    "45": (stat) => { return stat; },
    "46": (stat) => { return stat; },
    "47": (stat) => { return stat; },
    "48": (stat) => { return stat; },
    "49": (stat) => { return stat; },
    "50": (stat) => { return stat; },
    "51": (stat) => { return stat; },
    "52": (stat) => { return stat; },
    "53": (stat) => { return stat; },
    "54": (stat) => { return stat; },
    "55": (stat) => { return stat; },
    "56": (stat) => { return stat; },
    "57": (stat) => { return stat; },
    "58": (stat) => { return stat; },
    "59": (stat) => { return stat; },
    "61": (stat) => { return stat; }
}

const swgohHelpStatManipulations = {
    "0": (stat) => { return stat; },
    "1": (stat) => { return stat; },
    "2": (stat) => { return stat; },
    "3": (stat) => { return stat; },
    "4": (stat) => { return stat; },
    "5": (stat) => { return stat; },
    "6": (stat) => { return stat; },
    "7": (stat) => { return stat; },
    "8": (stat) => { return stat*100; },
    "9": (stat) => { return stat*100; },
    "10": (stat) => { return stat; },
    "11": (stat) => { return stat; },
    "12": (stat) => { return stat; },
    "13": (stat) => { return stat; },
    "14": (stat) => { return stat*100; },
    "15": (stat) => { return stat; },
    "16": (stat) => { return stat*100; },
    "17": (stat) => { return stat*100; },
    "18": (stat) => { return stat*100; },
    "19": (stat) => { return stat; },
    "20": (stat) => { return stat; },
    "21": (stat) => { return stat; },
    "22": (stat) => { return stat; },
    "23": (stat) => { return stat; },
    "24": (stat) => { return stat*100; },
    "25": (stat) => { return stat; },
    "26": (stat) => { return stat; },
    "27": (stat) => { return stat*100; },
    "28": (stat) => { return stat; },
    "29": (stat) => { return stat; },
    "30": (stat) => { return stat; },
    "31": (stat) => { return stat; },
    "32": (stat) => { return stat; },
    "33": (stat) => { return stat*100; },
    "34": (stat) => { return stat; },
    "35": (stat) => { return stat*100; },
    "36": (stat) => { return stat; },
    "37": (stat) => { return stat*100; },
    "38": (stat) => { return stat; },
    "39": (stat) => { return stat; },
    "40": (stat) => { return stat; },
    "41": (stat) => { return stat; },
    "42": (stat) => { return stat; },
    "43": (stat) => { return stat; },
    "44": (stat) => { return stat; },
    "45": (stat) => { return stat; },
    "46": (stat) => { return stat; },
    "47": (stat) => { return stat; },
    "48": (stat) => { return stat; },
    "49": (stat) => { return stat; },
    "50": (stat) => { return stat; },
    "51": (stat) => { return stat; },
    "52": (stat) => { return stat*100; },
    "53": (stat) => { return stat; },
    "54": (stat) => { return stat; },
    "55": (stat) => { return stat; },
    "56": (stat) => { return stat; },
    "57": (stat) => { return stat; },
    "58": (stat) => { return stat; },
    "59": (stat) => { return stat; },
    "61": (stat) => { return stat; }
}

function getUnitFinalStatByName(unit, stat)
{
    if (!unit) return NO_STAT_FOUND;

    let statKey;
    if (Number.isInteger(stat))
    {
        statKey = stat;
    } 
    else
    {
        statKey = Object.keys(stats).find(key => stats[key] === stat);
    }

    if (unit.data)
    {
        // swgoh.gg data
        return swgohGGStatManipulations[statKey](unit.data.stats[statKey]);
    }

    if (!unit.stats || !unit.stats.final) return NO_STAT_FOUND;
    return swgohHelpStatManipulations[statKey](unit.stats.final[statKey]);
}

async function getFeatureValueOrUnitStat(feature, playerData, a)
{
    if (isNaN(a) && !_.isString(a))
    {
        let aVal = NO_STAT_FOUND;

        if (feature.getUnitStat.constructor && feature.getUnitStat.constructor.name == 'AsyncFunction')
        {
            try { aVal = await feature.getUnitStat(a, playerData); } catch {}
        }
        else
        {
            try { aVal = feature.getUnitStat(a, playerData); } catch {}
        }

        return aVal;
    }

    return a;
}

async function compare(feature, playerData, a, c, b)
{
    let aVal = await getFeatureValueOrUnitStat(feature, playerData, a);
    let bVal = await getFeatureValueOrUnitStat(feature, playerData, b);

    if (aVal == NO_STAT_FOUND || bVal == NO_STAT_FOUND) return false;

    if (_.isArray(aVal))
    {
        if (isNaN(bVal)) return aVal.includes(bVal);
        else aVal = aVal.length;
    }

    switch (c)
    {
        case '=': return aVal == bVal;
        case '<': return aVal < bVal;
        case '>': return aVal > bVal;
        case '<=': return aVal <= bVal;
        case '>=': return aVal >= bVal;
        default: return false;
    }
}

function createIsViableResponse(viable, reason, boundUnitDefIds)
{
    return { viable: viable, reason: reason, boundUnitDefIds: boundUnitDefIds };
}