const addTeam = require("./addTeam");
const deleteTeam = require("./deleteTeam");
const listTeams = require("./listTeams");
const setDescription = require("./setDescription");
const teamPortrait = require("./teamPortrait");
const testTeam = require("./testTeam");
const twUtils = require("./utils");
const { SlashCommandSubcommandBuilder, SlashCommandSubcommandGroupBuilder } = require("@discordjs/builders");

const helpMessage = 
`Managing TW Teams:
**list**: List teams that you've created. Optional filter for details on specific teams.
**delete**: Delete a team.
**describe**: Set a short description for a team.
**add**: Add or update a team.  Details below.
**test**: Test who in the war matches a team.
**portrait**: Override the portrait shown in \`/tw team report\`

To add or update a team, specify the name (case insensitive) and features. Features are the in-game (US English) names of the units followed by a list of attributes. For example:
 - jedi master kenobi(g=13, z=6); ki-adi mundi (r>4, z=1); ahsoka tano (g=13, z=1)
   (JMK at G13 and 6 zetas with KAM at R5+ and 1 zeta and Ahsoka at G13 and 1 Zeta)
 - exec(s>=5)
   (Executor at 5+ stars)
 - darthrev(speed>340)
   (DR with speed > 340)
 - wrecker(speed>hunter)
   (Wrecker faster than Hunter)

Unit names can be nicknames or substrings of the full name. In case of conflicts, use the exact name (e.g., "Ahsoka Tano" matches Snips, not "Commander Ahsoka Tano").

${"Valid features (case does not matter):\r\n- " + twUtils.features.options.map(f => `${f.key}: ${f.description}`).join("\r\n- ")}
`

module.exports = {
    data: new SlashCommandSubcommandGroupBuilder()
        .setName("team")
        .setDescription("Manage TW teams.")
        .addSubcommand(addTeam.data)
        .addSubcommand(deleteTeam.data)
        .addSubcommand(listTeams.data)
        .addSubcommand(setDescription.data)
        .addSubcommand(teamPortrait.data)
        .addSubcommand(s =>
            s.setName("help")
            .setDescription("Help managing TW teams.")),
    interact: async function(interaction, isDefenseTeam)
    {
      
      let subcommand = interaction.options.getSubcommand();

      switch (subcommand)
      {
          case addTeam.data.name:
              await addTeam.interact(interaction, isDefenseTeam);
              break;
          case listTeams.data.name:
              await listTeams.interact(interaction, isDefenseTeam);
              break;
          case setDescription.data.name:
              await setDescription.interact(interaction, isDefenseTeam);
              break;
          case teamPortrait.data.name:
              await teamPortrait.interact(interaction, isDefenseTeam);
              break;
          case deleteTeam.data.name:
              await deleteTeam.interact(interaction, isDefenseTeam);
              break;
          case testTeam.data.name:
              await testTeam.interact(interaction, isDefenseTeam);
              break;
          case "help":
              await this.help(interaction);
              break;
      }
    },
    autocomplete: async function(interaction, isDefenseTeam)
    {
      let subcommand = interaction.options.getSubcommand();

      switch (subcommand)
      {
          case testTeam.data.name:
              await testTeam.autocomplete(interaction, isDefenseTeam);
              break;
          case addTeam.data.name:
              await addTeam.autocomplete(interaction);
              break;
          case deleteTeam.data.name:
            await deleteTeam.autocomplete(interaction, isDefenseTeam);
            break;
          case listTeams.data.name:
            await listTeams.autocomplete(interaction, isDefenseTeam);
            break;
          case teamPortrait.data.name:
            await teamPortrait.autocomplete(interaction, isDefenseTeam);
            break;
          case setDescription.data.name:
            await setDescription.autocomplete(interaction, isDefenseTeam);
            break;
      }
    },
    help: async function(interaction)
    {
        await interaction.editReply(helpMessage);
    }
}