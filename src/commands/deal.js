const { SlashCommandBuilder } = require("discord.js");
const fs = require("fs");
const discordUtils = require("../utils/discord");
const metadataCache = require("../utils/metadataCache");
const _ = require("lodash");

cacheMaterialsData();
fs.watchFile("./src/data/materials.json",
    (curr, prev) => { cacheMaterialsData(); });
    
function cacheMaterialsData() {
    fs.readFile("./src/data/materials.json", { encoding: "utf-8" }, (err, data) => {
        let materialsData = JSON.parse(data);

        metadataCache.set(metadataCache.CACHE_KEYS.materialsData, materialsData, 1000 * 60 * 60 * 24 * 30);
    });
}

const scb = new SlashCommandBuilder()
                .setName("deal")
                .setDescription("Check the crystal value of a deal");

let materialsData = require("../data/materials.json");
for (let c of materialsData)
{
    scb.addIntegerOption(o => 
        o.setName(c.id)
        .setDescription(c.plural_desc)
        .setRequired(false)
    )
}


module.exports = {
    data: scb,
    interact: async function(interaction)
    {
        const MATERIALS_DATA = metadataCache.get(metadataCache.CACHE_KEYS.materialsData);
        let materialsQty = _.cloneDeep(MATERIALS_DATA);

        let text = "";
        for (let m of materialsQty)
        {
            let qty = interaction.options.getInteger(m.id) ?? 0;
            m.quantity = qty;
        }

        let dealParts = materialsQty.filter(m => m.quantity > 0);

        if (dealParts.length === 0)
        {
            text = "No deal specified. Conversion costs shown below.\n\n";

            materialsQty.sort((a, b) => a.cost_crystals - b.cost_crystals);
            text += materialsQty.map(m => `- ${m.emoji ?? ""} ${m.plural_desc}: ${m.cost_crystals} ${m.cost_crystals === 1 ? "Crystal" : "Crystals"} each`).join("\n");
        
            await interaction.editReply({ content: text });
            return;
        }

        dealParts.sort((a, b) => a.cost_crystals - b.cost_crystals);

        text = dealParts.map(m => `- ${m.emoji ?? ""} ${m.quantity === 1 ? m.singular_desc : m.plural_desc}: ${m.quantity}\n-# > ${m.cost_crystals} ${m.cost_crystals === 1 ? "Crystal" : "Crystals"} each`).join("\n");

        let value = dealParts.reduce((p, c, i, arr) => { return p + Math.ceil(c.cost_crystals * c.quantity); }, 0);

        text = `**${discordUtils.symbols.crystal} Total Crystal Value: ${value.toLocaleString()}**\n\nDeal breakdown:\n${text}`;
        await interaction.editReply({ content: text });
    }
}