const unitMatch = require("./unit/match");
const unitNames = require("./unit/names");
const { SlashCommandBuilder } = require('@discordjs/builders');

module.exports = {
    data: new SlashCommandBuilder()
        .setName("unit")
        .setDescription("Unit Information")
        .addSubcommand(unitMatch.data)
        .addSubcommand(unitNames.data)
        ,
    interact: async function(interaction)
    {

        let subcommand = interaction.options.getSubcommand();
        switch (subcommand)
        {
            case unitMatch.data.name:
                await unitMatch.interact(interaction);
                break;
            // case "settimezoneoffset":
            //     await userTimeZone.setOffset(interaction);
            //     break;
            case unitNames.data.name:
                await unitNames.interact(interaction);
                break;            
        }
    },
    autocomplete: async function(interaction)
    {
        let subcommand;
        try { subcommand = interaction.options.getSubcommand(); } catch {}
        if (subcommand)
        {
            switch (subcommand) {
                case unitNames.data.name:
                    await unitNames.autocomplete(interaction);
                    break;
            }
            return;
        }
    },
}