const { EmbedBuilder, AttachmentBuilder } = require("discord.js");
const database = require("../database");
const guildUtils = require("../utils/guild.js");
const discordUtils = require("../utils/discord.js");
const playerUtils = require("../utils/player.js");
const { ChartJSNodeCanvas } = require('chartjs-node-canvas');
const swapiUtils = require("../utils/swapi.js");
const { SlashCommandBuilder, SlashCommandSubcommandBuilder } = require('@discordjs/builders');
const { logger, formatError } = require("../utils/log");
const datacronUtils = require('../utils/datacron.js');
const { parseAsync } = require('json2csv');
const raidCommon = require("../commands/raid/raidCommon.js");

function getGradient(ctx, chartArea) {
    let gradient = ctx.createLinearGradient(
      0,
      chartArea.bottom,
      0,
      chartArea.top
    );
    gradient.addColorStop(0.9, "rgba(0, 50, 250, .2)");
    gradient.addColorStop(0, "transparent");
    return gradient;
  }

function getGradientBackgroundColor(context) {
    const chart = context.chart;
    const { ctx, chartArea } = chart;
    // This case happens on initial chart load
    if (!chartArea) return;
    return getGradient(ctx, chartArea);
}
const legend = {labels: {fontColor: 'white'}}
const xAxesFont = [{ticks: {fontColor: 'white'}}]

const UNIT_DATA = require("../data/unit_data.json")

const ONE_DAY_MILLIS = 1000*60*60*24;
const DEFAULT_REPORT_TIMEFRAME_DAYS = 90;

const monthShortNames = 
[
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec"
];

const MOD_QUALITY_TYPES = {
    WOOKIEE: { name: "Wookiee Style", formulaDesc: "((# of 15+ speed) + (# of 20+ speed) + (# of 25+ speed) + (# of 6-dot * 0.5)) / (Squad GP / 100,000)"},
    DSR: { name: "DSR Style", formulaDesc: "(# of +15 speeds) / (Squad GP / 100,000)" },
    HOT: { name: "HotBot Style", formulaDesc: "((# of 15-19 Speed * 0.8) + (# of 20-24 Speed) + (# of 25+ Speed * 1.6)) / (squad GP / 100,000)" }
}

const reportTypes = {
    "reek" : getReportReek,
    "gp" : getChartGpOverTimeForAllyCode,
    "gac" : getChartGacOverTimeForAllyCode,
    "capships" : getChartCapitalShipsForGuild,
    "gls" : getChartGLsForGuild,
    "mq" : getChartModQualityOverTimeForAllyCode,
    "mq-guild": getChartModQualityOverTimeForGuild,
    "payouts" : getChartGuildPayouts,
    "gg" : getChartGuildGrowth,
    "tb" : getChartTBPerformance,
    "tbphase" : getReportTBPhasePerformance,
    "omicrons" : getGuildOmicronReport,
    "datacrons": getGuildDatacronReport,
    "raid": getChartRaidOverTimeForAllyCode,
    "raid-guild": getChartGuildRaidOverTime,
    "krayt": getChartKraytOverTimeForAllyCode,
    "krayt-guild": getChartGuildKraytOverTime
};

const allGACOmicronTypeCode = 9999;
const allOmicronTypeCode = 9998;

module.exports = {
    data: new SlashCommandBuilder()
        .setName("report")
        .setDescription("Generate reports")
        .addSubcommand(subcommand => 
            subcommand
                .setName("help")
                .setDescription("Show help about reports"))
        .addSubcommand(subcommand => 
            subcommand
                .setName("raid")
                .setDescription("Chart of player raid scores over time")
                .addStringOption(o => 
                    o.setName("allycode")
                    .setDescription("An ally code (default: you)")
                    .setRequired(false))
                .addStringOption(o =>
                    o.setName("raid")
                    .setDescription("The raid to check (Default: featured raid)")
                    .setRequired(false)
                    .addChoices(
                        { name: raidCommon.NABOO.description, value: raidCommon.NABOO.KEY },
                        { name: raidCommon.ENDOR.description, value: raidCommon.ENDOR.KEY },
                        { name: raidCommon.KRAYT.description, value: raidCommon.KRAYT.KEY }
                    ))
                .addIntegerOption(o =>
                    o.setName("alt")
                    .setDescription("Alt account (/user me)")
                    .setRequired(false)
                    .setAutocomplete(true)))
        .addSubcommand(subcommand => 
            subcommand
                .setName("raid-guild")
                .setDescription("Chart of guild raid scores over time")
                .addStringOption(o => 
                    o.setName("allycode")
                    .setDescription("An ally code in the guild (default: your guild)")
                    .setRequired(false))
                .addStringOption(o =>
                    o.setName("raid")
                    .setDescription("The raid to check (Default: featured raid)")
                    .setRequired(false)
                    .addChoices(
                        { name: raidCommon.NABOO.description, value: raidCommon.NABOO.KEY },
                        { name: raidCommon.ENDOR.description, value: raidCommon.ENDOR.KEY },
                        { name: raidCommon.KRAYT.description, value: raidCommon.KRAYT.KEY }
                    ))
                .addIntegerOption(o =>
                    o.setName("alt")
                    .setDescription("Alt account (/user me)")
                    .setRequired(false)
                    .setAutocomplete(true)))
        .addSubcommand(subcommand => 
            subcommand
                .setName("krayt")
                .setDescription("Chart of player Krayt scores over time")
                .addStringOption(o => 
                    o.setName("allycode")
                    .setDescription("An ally code (default: you)")
                    .setRequired(false))
                .addIntegerOption(o =>
                    o.setName("alt")
                    .setDescription("Alt account (/user me)")
                    .setRequired(false)
                    .setAutocomplete(true)))
        .addSubcommand(subcommand => 
            subcommand
                .setName("krayt-guild")
                .setDescription("Chart of guild Krayt scores over time")
                .addStringOption(o => 
                    o.setName("allycode")
                    .setDescription("An ally code in the guild (default: your guild)")
                    .setRequired(false))
                .addIntegerOption(o =>
                    o.setName("alt")
                    .setDescription("Alt account (/user me)")
                    .setRequired(false)
                    .setAutocomplete(true)))
        .addSubcommand(subcommand =>
            subcommand
                .setName("reek")
                .setDescription("Check who has not beaten the Reek in a guild")
                .addStringOption(o => 
                    o.setName("allycode")
                    .setDescription("An ally code in the guild (default: your guild).")
                    .setRequired(false))
                .addIntegerOption(o =>
                    o.setName("alt")
                    .setDescription("Alt account (/user me)")
                    .setRequired(false)
                    .setAutocomplete(true)))
        .addSubcommand(subcommand =>
            subcommand
                .setName("payouts")
                .setDescription("Chart of when guild members have payouts, and how many")
                .addIntegerOption(o => 
                    o.setName("allycode")
                    .setDescription("An ally code in the guild (default: your guild).")
                    .setRequired(false))
                .addIntegerOption(o =>
                    o.setName("alt")
                    .setDescription("Alt account (/user me)")
                    .setRequired(false)
                    .setAutocomplete(true)))
        .addSubcommand(subcommand =>
            subcommand
                .setName("gls")
                .setDescription("Chart of galactic legends for a guild")
                .addStringOption(o => 
                    o.setName("allycode")
                    .setDescription("An ally code in the guild (default: your guild).")
                    .setRequired(false))
                .addIntegerOption(o =>
                    o.setName("alt")
                    .setDescription("Alt account (/user me)")
                    .setRequired(false)
                    .setAutocomplete(true)))
        .addSubcommand(subcommand =>
            subcommand
                .setName("capships")
                .setDescription("Chart of capital ships for a guild")
                .addStringOption(o => 
                    o.setName("allycode")
                    .setDescription("An ally code in the guild (default: your guild).")
                    .setRequired(false))
                .addIntegerOption(o =>
                    o.setName("alt")
                    .setDescription("Alt account (/user me)")
                    .setRequired(false)
                    .setAutocomplete(true)))
        .addSubcommand(subcommand =>
            subcommand
                .setName("gp")
                .setDescription("Graph of GP growth for a player (default: 90 days)")
                .addStringOption(o => 
                    o.setName("allycode")
                    .setDescription("Player's ally code (default: you).")
                    .setRequired(false))
                .addIntegerOption(o => 
                    o.setName("timeframe")
                    .setDescription("Timeframe to pull data (default: 90 days).")
                    .setRequired(false)
                    .addChoices(
                        { name: "1 Year", value: 365 },
                        { name: "6 Months", value: 180 }
                    ))
                .addStringOption(o =>
                    o.setName("type")
                    .setDescription("Subtype of GP report (default: all types)")
                    .setRequired(false)
                    .addChoices(
                        { name: "total", value: "total" },
                        { name: "fleet", value: "fleet" },
                        { name: "squad", value: "squad" }
                    ))
                .addIntegerOption(o =>
                    o.setName("alt")
                    .setDescription("Alt account (/user me)")
                    .setRequired(false)
                    .setAutocomplete(true)))
        .addSubcommand(subcommand =>
            subcommand
                .setName("mq")
                .setDescription("Graph of mod quality growth for a player (180 days)")
                .addStringOption(o => 
                    o.setName("allycode")
                    .setDescription("Player's ally code (default: you).")
                    .setRequired(false))
                .addStringOption(o =>
                    o.setName("type")
                    .setDescription("Subtype of mod quality report (default: all types)")
                    .setRequired(false)
                    .addChoices(
                        { name: MOD_QUALITY_TYPES.WOOKIEE.name, value: MOD_QUALITY_TYPES.WOOKIEE.name },
                        { name: MOD_QUALITY_TYPES.DSR.name, value: MOD_QUALITY_TYPES.DSR.name },
                        { name: MOD_QUALITY_TYPES.HOT.name, value: MOD_QUALITY_TYPES.HOT.name }
                    ))
                .addIntegerOption(o =>
                    o.setName("alt")
                    .setDescription("Alt account (/user me)")
                    .setRequired(false)
                    .setAutocomplete(true)))
        .addSubcommand(subcommand =>
            subcommand
                .setName("mq-guild")
                .setDescription("Report on mod quality over time for guild.")
                .addStringOption(o => 
                    o.setName("allycode")
                    .setDescription("Guild member's ally code (default: you).")
                    .setRequired(false))
                .addStringOption(o =>
                    o.setName("type")
                    .setDescription("Subtype of mod quality report (default: Wookiee Style)")
                    .setRequired(false)
                    .addChoices(
                        { name: MOD_QUALITY_TYPES.WOOKIEE.name, value: MOD_QUALITY_TYPES.WOOKIEE.name },
                        { name: MOD_QUALITY_TYPES.DSR.name, value: MOD_QUALITY_TYPES.DSR.name },
                        { name: MOD_QUALITY_TYPES.HOT.name, value: MOD_QUALITY_TYPES.HOT.name }
                    ))
                .addIntegerOption(o =>
                    o.setName("alt")
                    .setDescription("Alt account (/user me)")
                    .setRequired(false)
                    .setAutocomplete(true)))
        .addSubcommand(subcommand =>
            subcommand
                .setName("gac")
                .setDescription("Chart of GAC skill rating for a player (90 days)")
                .addStringOption(o => 
                    o.setName("allycode")
                    .setDescription("An ally code for a player (default: you).")
                    .setRequired(false))
                .addIntegerOption(o =>
                    o.setName("alt")
                    .setDescription("Alt account (/user me)")
                    .setRequired(false)
                    .setAutocomplete(true)))
        .addSubcommand(subcommand =>
            subcommand
                .setName("tb")
                .setDescription("Chart of TB Wave performance for a player. Requires upload with guild.uploadtbdata.")
                .addStringOption(o => 
                    o.setName("allycode")
                    .setDescription("An ally code for a player (default: you).")
                    .setRequired(false))
                .addIntegerOption(o =>
                    o.setName("alt")
                    .setDescription("Alt account (/user me)")
                    .setRequired(false)
                    .setAutocomplete(true)))
        .addSubcommand(subcommand =>
            subcommand
                .setName("guild-growth")
                .setDescription("Report on guild growth (last 30 days)")
                .addStringOption(o => 
                    o.setName("allycode")
                    .setDescription("An ally code for a player to report on (default: you).")
                    .setRequired(false))
                .addIntegerOption(o =>
                    o.setName("alt")
                    .setDescription("Alt account (/user me)")
                    .setRequired(false)
                    .setAutocomplete(true)))
        .addSubcommand(subcommand =>
            subcommand
                .setName("omicrons")
                .setDescription("Report on guild omicrons by type")
                .addIntegerOption(o =>
                    o.setName("type")
                    .setDescription("Type of omicron to report on.")
                    .setRequired(true)
                    .addChoices(
                        { name: "TW", value: swapiUtils.OMICRON_MODE.TERRITORY_WAR_OMICRON },
                        { name: "TB", value: swapiUtils.OMICRON_MODE.TERRITORY_BATTLE_BOTH_OMICRON },
                        { name: "GAC", value: allGACOmicronTypeCode },
                        { name: "Conquest", value: swapiUtils.OMICRON_MODE.CONQUEST_OMICRON },
                        { name: "All", value: allOmicronTypeCode },
                    ))
                .addStringOption(o => 
                    o.setName("allycode")
                    .setDescription("An ally code for a player to report on (default: you).")
                    .setRequired(false))
                .addIntegerOption(o =>
                    o.setName("alt")
                    .setDescription("Alt account (/user me)")
                    .setRequired(false)
                    .setAutocomplete(true)))
        .addSubcommand(subcommand =>
            subcommand
                .setName("datacrons")
                .setDescription("Report on guild datacrons")
                .addStringOption(o => 
                    o.setName("allycode")
                    .setDescription("An ally code for a player to report on (default: you).")
                    .setRequired(false))
                .addIntegerOption(o => 
                    o.setName("set")
                    .setRequired(false)
                    .setAutocomplete(true)
                    .setDescription("Set of the datacron"))
                .addIntegerOption(o =>
                    o.setName("alt")
                    .setDescription("Alt account (/user me)")
                    .setRequired(false)
                    .setAutocomplete(true)))
        .addSubcommand(subcommand =>
            subcommand
                .setName("tbphase")
                .setDescription("Report on wave performance for a guild by phase. Requires upload with guild.uploadtbdata.")
                .addStringOption(o =>
                    o.setName("tb-type")
                    .setDescription("Type of TB")
                    .setRequired(true)
                    .addChoices(
                        { name: "LS Hoth TB", value: "lshtb" },
                        { name: "LS Geo TB", value: "lsgtb" },
                        { name: "DS Hoth TB", value: "dshtb" },
                        { name: "DS Geo TB", value: "dsgtb" }
                    ))
                .addIntegerOption(o =>
                    o.setName("phase")
                    .setDescription("Phase of TB")
                    .setRequired(true)
                    .addChoices(
                        { name: "1",  value: 1 },
                        { name: "2",  value: 2 },
                        { name: "3",  value: 3 },
                        { name: "4",  value: 4 },
                        { name: "5",  value: 5 },
                        { name: "6",  value: 6 }
                    ))
                .addStringOption(o => 
                    o.setName("allycode")
                    .setDescription("An ally code in the guild (default: your guild).")
                    .setRequired(false))
                .addIntegerOption(o =>
                    o.setName("alt")
                    .setDescription("Alt account (/user me)")
                    .setRequired(false)
                    .setAutocomplete(true)))
        ,
    list: async function(input) {
        
        let list = Object.keys(reportTypes);

        let embed = new EmbedBuilder()
            .setTitle("Available Reports")
            .setColor(0xD2691E)
            .setDescription(list.sort().toString().split(',').join("\r\n"));

        await input.channel.send({ embeds: [embed] }).catch();
    },
    getHelpEmbed: function(input) {
        let embed = new EmbedBuilder()
            .setTitle("WookieeBot Reports")
            .setColor(0xD2691E)
            .setDescription(
`gac: graph gac skill rating for a player (90 days)
gls: graph GLs in the guild
capships: graph Capital Ships in the guild
gp: graph gp growth for a player (90 days)
mq: graph player mod quality growth (90 days, Wookiee/DSR/HotBot types)
mq-guild: report on guild mod quality growth (90 days, Wookiee/DSR/HotBot types)
reek: list guild members that have not beaten the reek 
tb: combat waves over time (after HU data upload) 
tbphase: waves by tb/phase for guild (lastest tb, after HU data upload) 
payouts: chart of when guild members have payouts, and how many 
omicrons: show how many of a type of omicron each guild member has 
datacrons: show a count of datacrons for the guild 
raid: show player raid score over time
raid-guild: show guild raid score over time

Examples:
/report gac 
/report tbphase tb-type:LS Geo TB phase:4 
/report reek allycode:985736123
/report gp type:squad allycode:985736123
`
            );

        return embed;
    },
    interact: async function(interaction)
    {
        let reportType = interaction.options.getSubcommand();

        if (reportType == "help")
        {
            let embed = this.getHelpEmbed();
            await interaction.editReply({ embeds: [embed]});
            return;
        }

        // avoid using the shorthand in interactions
        if (reportType == "guild-growth") reportType = "gg";

        let allycode = interaction.options.getString("allycode");

        // allycode selection:
        // 1. argument supplied
        // 2. alt supplied
        // 3. match to the guild that's linked
        // 4. first alt [0]

        let alt = interaction.options.getInteger("alt");

        let { botUser, guildId, error } = await playerUtils.authorize(interaction);
        
        if (allycode) allycode = playerUtils.cleanAndVerifyStringAsAllyCode(allycode);
        allycode = playerUtils.chooseBestAllycode(botUser, guildId, allycode, alt);

        if (!allycode)
        {
            await interaction.editReply({ content: "Register with `/register` or provide a valid allycode or alt." });
            return;
        }

        if (!reportTypes[reportType]) {
            await interaction.editReply("Error: Report does not exist. WookieeBot Developers must have broken something. What were they thinking?")
            logger.error("Report requested that doesn't exist: " + reportType);
            return;
        }

        let reportObj, type, timeframe;

        try {

            switch (reportType) {
                case "gp":
                    type = interaction.options.getString("type");
                    timeframe = interaction.options.getInteger("timeframe") ?? DEFAULT_REPORT_TIMEFRAME_DAYS;
                    reportObj = await reportTypes[reportType](allycode, type, timeframe);
                    break;
                case "mq":
                case "mq-guild":
                    type = interaction.options.getString("type");
                    reportObj = await reportTypes[reportType](allycode, type);
                    break;
                case "tbphase":
                    let tbtype = interaction.options.getString("tb-type");
                    let phase = interaction.options.getInteger("phase");
                    type = tbtype + phase.toString();
                    reportObj = await reportTypes[reportType](allycode, type)
                    break;
                case "omicrons":
                    type = interaction.options.getInteger("type");
                    reportObj = await reportTypes[reportType](allycode, type);
                    break;
                case "datacrons":
                    set = interaction.options.getInteger("set");
                    reportObj = await reportTypes[reportType](allycode, set);
                    break;
                case "raid":
                case "raid-guild":
                    raid = interaction.options.getString("raid") ?? raidCommon.DEFAULT_RAID.KEY;
                    reportObj = await reportTypes[reportType](allycode, raid);
                    break;
                default:
                    reportObj = await reportTypes[reportType](allycode);
                    break;
            }
        } 
        catch (error)
        {
            reportObj = discordUtils.createErrorEmbed("Error", error.message ? error.message : error);            
            logger.error(formatError(error));
        }

        if (reportObj.imageBuffer)
        {
            const attachment = new AttachmentBuilder(reportObj.imageBuffer, { name: reportObj.fileName });
            await interaction.editReply({ files: [attachment] });
        } else if (reportObj instanceof EmbedBuilder) {
            await interaction.editReply({ embeds: [reportObj] });
        } else {
            await interaction.editReply(reportObj);
        }
    },
    autocomplete: async function(interaction) {
        
		const focusedOption = interaction.options.getFocused(true);
        let choices;

        if (focusedOption.name === "alt")
        {
            choices = await playerUtils.getAltAutocompleteOptions(focusedOption.value, interaction.user.id);    
        }
        else if (focusedOption.name === "set")
        {
            let activeDatacronSetList = await datacronUtils.getDatacronSetList(true);
            choices = activeDatacronSetList.map(s => { return { name: `${s.set_id} (${s.color_text})`, value: s.set_id }; });
        }

        await interaction.respond(choices);
    },
    sendError: async function(input, title, errorMessage) {
        errorEmbed = discordUtils.createErrorEmbed(title, errorMessage);
        try {
            await input.channel.send({ embeds: [errorEmbed] });
        } catch (e) {
            logger.error(formatError(e));
        }
    },
}


function createImageReportResultObj(imageBuffer, fileName)
{
    return {
        imageBuffer: imageBuffer,
        fileName: fileName
    };
} 

const tbCombatData = require("../data/tb_data.json");
const swapi = require("../utils/swapi.js");
const comlink = require("../utils/comlink");
const { data } = require("./raid/guild.js");

async function getReportTBPhasePerformance(allycode, type = null)
{
    var embed = new EmbedBuilder();

    var guildInfo = await guildUtils.getGuildIdAndAllyCodesForGuildByAllycode(allycode);
    if (!guildInfo || !guildInfo.guildId || !guildInfo.allycodes || guildInfo.allycodes.length == 0)
    {
        embed.setTitle("Error");
        embed.setColor(0xAA0000);
        embed.setDescription("Guild is not registered.");
        return embed;
    }
    let typeRegex = /^([a-zA-Z]+)(\d)$/g
    let matches = typeRegex.exec(type);

    if (!matches)
    {
        embed.setTitle("Error");
        embed.setColor(0xAA0000);
        embed.setDescription(
`That is not a valid query.  Format should be [TB Type][Phase #].  Valid TB Types are LSHTB, DSHTB, LSGTB, DSGTB. For example:
^report tbphase.lsgtb3  -- Last LS Geo TB, Phase 3
^report tbphase.dshtb4  -- Last DS Hoth TB, Phase 4`)
        return embed;
    }

    let tbType = matches[1].toUpperCase();
    let phase = parseInt(matches[2]);
    
    const tbData = await database.db.any(`
        select date, player_tb_data.allycode, combat_waves, player_name, manual_entry, phase_data_json
        from player_tb_data
        left join player_data_static using (allycode)
        where guild_id = $1 and phase_data_json is not null
            and date = (select date from player_tb_data where guild_id=$1 and tb_type=$2 order by date desc limit 1)
        order by manual_entry desc`,
        [guildInfo.guildId, tbType])

    let desc = "";

    if (tbData.length == 0)
    {
        embed.setTitle("No Data");
        embed.setColor(0xaa0000);
        embed.setDescription(`No TB data for the last ${tbType} Phase ${phase}.`);
        return embed;
    }

    tbData.forEach((v, ix, a) => {
        if (Array.isArray(v.phase_data_json.combatWaves[phase-1]))
            a[ix].totalPhaseWaves = v.phase_data_json.combatWaves[phase-1].reduce((a, b) => a + b, 0);
        else 
            a[ix].totalPhaseWaves = v.phase_data_json.combatWaves[phase-1];
    })

    tbData.sort((a, b) => b.totalPhaseWaves - a.totalPhaseWaves)

    const phaseData = tbCombatData[tbType][phase-1];
    let dateString = tbData[0].date.toLocaleDateString("en-US", { weekday: 'short', year: 'numeric', month: 'short', day: 'numeric' });
    embed.setTitle(tbType + " Phase " + phase + " Combat Performance for Guild, " + dateString)
    embed.setColor(0x24BC66);

    let hasFleet = phaseData.find(c => c.type == "fleet");

    for (var d = 0; d < tbData.length; d++)
    {
        var dataRow = tbData[d];
        let waves = dataRow.phase_data_json.combatWaves[phase-1];
        let totalPhaseWaves = dataRow.totalPhaseWaves; 
        let fleetWaves = 0;
        let groundWaves = 0;
        
        for (var cix = 0; cix < phaseData.length; cix++)
        {
            if (phaseData[cix].type == "fleet") fleetWaves += waves[cix];
            else groundWaves += waves[cix];
        }

        desc += `${d+1}. ${dataRow.player_name} (${dataRow.allycode}): ${totalPhaseWaves} waves (${hasFleet ? `${fleetWaves} fleet, ` : ""} ${groundWaves} ground)\r\n`
    }

    embed.setDescription(desc);

    return embed;
}


const AVG_PREFIX_TEXT = "Guild Avg. ";
const TB_REPORT_LINE_COLOR_OPTIONS = {
    "LSGTB": 'rgba(54, 162, 235, 1)', 
    "Guild Avg. LSGTB": 'rgba(0, 100, 175, 1)',
    "DSGTB": 'rgba(130, 30, 30, 1)',
    "Guild Avg. DSGTB": 'rgba(80, 0, 0, 1)',
    "LSHTB": 'rgba(40, 90, 19, 1)',
    "Guild Avg. LSHTB": 'rgba(0, 40, 0, 1)',
    "DSHTB": 'rgba(150, 50, 235, 1)',
    "Guild Avg. DSHTB": 'rgba(80, 0, 165, 1)',
}

async function getChartTBPerformance(allycode)
{
    // [
    //     'rgba(54, 162, 235, 1)',
    //     'rgba(0, 100, 175, 1)',
    //     'rgba(150, 50, 235, 1)',
    //     'rgba(80, 0, 165, 1)',
    //     'rgba(150, 35, 55, 1)',
    //     'rgba(80, 0, 15, 1)',
    //     'rgba(40, 90, 19, 1)',
    //     'rgba(0, 40, 0, 1)',
    //     'rgba(170, 150, 35, 1)',
    //     'rgba(100, 80, 0, 1)',
    // ]

    const guildTbData = await database.db.any(
        `select date, allycode, tb_type, combat_waves, player_name, manual_entry
        from player_tb_data
        left join player_data_static using (allycode)
		where player_name is not null and player_tb_data.guild_id = (select guild_id from guild_players where allycode = $1)
        group by date, allycode, manual_entry, tb_type, combat_waves, player_name
        order by date asc, manual_entry desc`, [allycode]
    )
    // const tbData = await database.db.any(
    //     `
    //     select distinct on (date) date, tb_type, combat_waves, player_name, manual_entry
    //     from player_tb_data
    //     left join player_data_static using (allycode)
    //     where allycode = $1
    //     group by date, manual_entry, tb_type, combat_waves, player_name
    //     order by date asc, manual_entry desc
    //     `, [allycode]
    // )

    const tbData = guildTbData.filter((v, ix, arr) => v.allycode == allycode && (v.manual_entry || !arr.find(d => d.date == v.date && d.manual_entry)));


    var labels = new Array();
    let guildAvg = new Array();

    if (tbData.length == 0)
        throw new Error(`No data for allycode ${allycode}.`);

    const playerName = tbData[0].player_name; 
    var data = {};

    for (var i = 0; i < tbData.length; i++) {
        //var date = tbData[i].date;
        var label = tbData[i].date;
        if (labels.indexOf(label) == -1) labels.push(label);
        
        if (!data[tbData[i].tb_type]) {
            data[tbData[i].tb_type] = new Array();
            data[AVG_PREFIX_TEXT + tbData[i].tb_type] = new Array();
        }

        data[tbData[i].tb_type].push({ x: label, y: tbData[i].combat_waves, id: `${i}${tbData[i].tb_type}`});

        let curTbGuildData = guildTbData.filter((v, ix, arr) => v.date.getTime() == label.getTime() && (v.manual_entry || !arr.find(d => d.allycode == v.allycode && d.date.getTime() == v.date.getTime() && d.manual_entry)));
        let sum = 0;
        for (let x in curTbGuildData)
        {
            sum += curTbGuildData[x].combat_waves;
        }
        let avg = Math.round(sum / curTbGuildData.length);
        data[AVG_PREFIX_TEXT + tbData[i].tb_type].push(
            {
                x: label,
                y: avg,
                id: `Avg${i}${tbData[i].tb_type}`
            }
        )
        
    }
    
    var max_entries_in_data = 0;
    const MAX_ENTRIES_TO_SHOW = 15;
    for (let type in data)
    {
        if (data[type].length > MAX_ENTRIES_TO_SHOW)
        {
            data[type] = data[type].slice(data[type].length-MAX_ENTRIES_TO_SHOW);
        }

        if (data[type].length > max_entries_in_data) max_entries_in_data = data[type].length;
    }

    // only include labels for which there's matching data to display. Probably better to do this by
    // building it up at the start, but it's not so much worse this way.
    // TODO: Efficiency
    labels = labels.filter(l => {
        for (var type in data)
        {
            let match = data[type].find(entry => entry.x === l);
            if (match) return true;
        }
        return false;
    })

    const width = 1500;
    const height = 600;
    const chartCallback = (ChartJS) => {
        ChartJS.defaults.global.responsive = true;
        ChartJS.defaults.global.maintainAspectRatio = false;

        ChartJS.plugins.register({
            beforeDraw: function(chartInstance) {
              var ctx = chartInstance.chart.ctx;
              ctx.fillStyle = "white";
              ctx.fillRect(0, 0, chartInstance.chart.width, chartInstance.chart.height);
            },
            afterDraw: function (chartInstance) {
                var ctx = chartInstance.chart.ctx;
                ctx.textBaseline = "middle";
                ctx.fillStyle = 'black'
                ctx.font = "15px Arial";
                ctx.textAlign = "center";

                var dsNum = 0;
                for (var t in data) {
                    for (var d in data[t])
                    {
                        var labelText = data[t][d].y;
                        var x = chartInstance.chart.controller.data.datasets[dsNum]._meta[0].data[d]._view.x;
                        var y = Math.min(chartInstance.chart.controller.data.datasets[dsNum]._meta[0].data[d]._view.y + 15, height - 120);
                        ctx.fillText(labelText, x, y);
                    }
                    dsNum++;
                }

            }
          });
    };

    var chartTitle = `TB Combat Waves for ${playerName} (${allycode})`;

    var configuration = {
        type: 'line',
        data: {
            datasets: new Array(),
            labels: labels
        },
        options: {
            title: {
                display: true,
                align: 'center',
                position: 'bottom',
                text: chartTitle
            },
            scales: {
                xAxes: [{
                    type: 'time',
                    time: {
                        displayFormats: {
                            month: "DD MMM YYYY",
                            quarter: "DD MMM YYYY",
                            day: "DD MMM YYYY",
                        }
                    },
                    ticks: {
                        autoSkip: true,
                        maxTicksLimit: max_entries_in_data
                    }
                }]
            }
        }
    };

    for (var type in data)
    {
        configuration.data.datasets.push({
            borderColor: TB_REPORT_LINE_COLOR_OPTIONS[type],
            backgroundColor: 'transparent',
            borderDash: type.startsWith(AVG_PREFIX_TEXT) ? [10, 10] : [],
            tension: 0,
            label: type,
            data: data[type]
        });
    }
    
    const chartJSNodeCanvas = new ChartJSNodeCanvas({width, height, chartCallback: chartCallback});

    const imageBuffer = await chartJSNodeCanvas.renderToBuffer(configuration);    

    var fileName = "chart-tbcw-" + allycode + "-" + Date.now() + ".png";

    return createImageReportResultObj(imageBuffer, fileName);
}

async function getChartGuildGrowth(allycode)
{
    var now = new Date();

    const ggData = await database.db.any(
        `select maxs.allycode, maxs.player_name, maxs.max_mod_quality2, maxs.max_gp_squad, maxs.max_gp_fleet, maxs.max_mods_speed25, maxs.max_skill_rating,
        mins.min_mod_quality2, mins.min_gp_squad, mins.min_gp_fleet, mins.min_mods_speed25, mins.min_skill_rating
  from 
    (select 
      player_data.allycode,
      player_data_static.player_name,
      max(player_data.mod_quality2) max_mod_quality2,
      max(player_data.gp_squad) max_gp_squad,
      max(player_data.gp_fleet) max_gp_fleet,
      max(player_data.mods_speed25) max_mods_speed25,
      max(player_data.skill_rating) max_skill_rating
  from player_data
  join player_data_static on player_data.allycode=player_data_static.allycode
  where player_data.timestamp between $1 and $2
  and player_data.allycode in (select allycode from guild_players where guild_id=(select guild_id from guild_players where allycode=$3))
  group by player_data.allycode,player_data_static.player_name) maxs
      join ( 
  select 
      distinct on (player_data.allycode) player_data.allycode,
      player_data_static.player_name,
      player_data.timestamp,
      mod_quality2 min_mod_quality2,
      gp_squad min_gp_squad,
      gp_fleet min_gp_fleet,
      mods_speed25 min_mods_speed25,
      skill_rating min_skill_rating
  from player_data
  join player_data_static on player_data.allycode=player_data_static.allycode
  join (select min(timestamp),allycode from player_data pdx group by allycode) pd2 on pd2.allycode = player_data.allycode
  where player_data.timestamp between $1 and $2
  and player_data.allycode in (select allycode from guild_players where guild_id=(select guild_id from guild_players where allycode=$3))
  order by player_data.allycode, player_data.timestamp asc) mins on maxs.allycode = mins.allycode
        `,
        [new Date(now-ONE_DAY_MILLIS*30).toDateString(), now.toDateString(), allycode]
    );

    var gains = {
        modQuality: [],
        gpSquad: [],
        gpFleet: [],
        modsSpeed25: [],
        skillRating: []
    }

    for (var d in ggData)
    {
        var modQualityGain = (ggData[d].max_mod_quality2 - ggData[d].min_mod_quality2).toFixed(2);
        var gpSquadGain = ggData[d].max_gp_squad - ggData[d].min_gp_squad;
        var gpFleetGain = ggData[d].max_gp_fleet - ggData[d].min_gp_fleet;
        var modsSpeed25Gain = ggData[d].max_mods_speed25 - ggData[d].min_mods_speed25;
        let skillRatingGain = ggData[d].max_skill_rating - ggData[d].min_skill_rating;

        if (modQualityGain > 0)
            gains.modQuality.push({ gain: modQualityGain, allycode: ggData[d].allycode, playerName: ggData[d].player_name });

        if (gpSquadGain > 0)
            gains.gpSquad.push({ gain: gpSquadGain, allycode: ggData[d].allycode, playerName: ggData[d].player_name });

        if (gpFleetGain > 0)
            gains.gpFleet.push({ gain: gpFleetGain, allycode: ggData[d].allycode, playerName: ggData[d].player_name });

        if (modsSpeed25Gain > 0)
            gains.modsSpeed25.push({ gain: modsSpeed25Gain, allycode: ggData[d].allycode, playerName: ggData[d].player_name });

        gains.skillRating.push({ gain: skillRatingGain, allycode: ggData[d].allycode, playerName: ggData[d].player_name });
    }

    gains.modQuality.sort((a,b) => b.gain - a.gain);
    gains.gpSquad.sort((a,b) => b.gain - a.gain);
    gains.gpFleet.sort((a,b) => b.gain - a.gain);
    gains.modsSpeed25.sort((a,b) => b.gain - a.gain);
    gains.skillRating.sort((a,b) => b.gain - a.gain);

    userModQuality = gains.modQuality.find(g => g.allycode == allycode);
    userGpSquad = gains.gpSquad.find(g => g.allycode == allycode);
    userGpFleet = gains.gpFleet.find(g => g.allycode == allycode);
    userModsSpeed25 = gains.modsSpeed25.find(g => g.allycode == allycode);
    userSkillRating = gains.skillRating.find(g => g.allycode == allycode);

    let desc = "";

    desc += "Mod Quality Improvement:\r\n";
    
    if (gains.modQuality.length > 0)
    {
        for (var p = 0; p < 3 && p < gains.modQuality.length; p++) {
            var symbol = (p == 0 ? discordUtils.symbols.firstPlace : (p == 1 ? discordUtils.symbols.secondPlace : discordUtils.symbols.thirdPlace));
            desc += symbol + " " + gains.modQuality[p].playerName + ": " + gains.modQuality[p].gain + "\r\n"
        }
    } else {
        desc += "No one improved their mod quality :cry:\r\n";
    }
    if (userModQuality)
        desc += "Your improvement: " + userModQuality.gain + "\r\n";

    desc += "\r\nNew mods with 25+ speed secondary:\r\n";
    
    if (gains.modsSpeed25.length > 0)
    {
        for (var p = 0; p < 3 && p < gains.modsSpeed25.length; p++) {
            var symbol = (p == 0 ? discordUtils.symbols.firstPlace : (p == 1 ? discordUtils.symbols.secondPlace : discordUtils.symbols.thirdPlace));
            desc += symbol + " " + gains.modsSpeed25[p].playerName + ": " + gains.modsSpeed25[p].gain + "\r\n"
        }
    } else {
        desc += "No one got any new mods with 25+ speed secondary :cry:\r\n";
    }
    if (userModsSpeed25)
        desc += "Your new 25+ speed mods: " + userModsSpeed25.gain + "\r\n";

    desc += "\r\nSquad GP Growth:\r\n";
    
    if (gains.gpSquad.length > 0)
    {
        for (var p = 0; p < 3 && p < gains.gpSquad.length; p++) {
            var symbol = (p == 0 ? discordUtils.symbols.firstPlace : (p == 1 ? discordUtils.symbols.secondPlace : discordUtils.symbols.thirdPlace));
            desc += symbol + " " + gains.gpSquad[p].playerName + ": " + gains.gpSquad[p].gain.toLocaleString("en-US") + "\r\n"
        }
    } else {
        desc += "No one gained any squad GP :cry:\r\n";
    }
    if (userGpSquad)
        desc += "Your growth: " + userGpSquad.gain.toLocaleString("en-US") + "\r\n";

    desc += "\r\nFleet GP Growth:\r\n";
    
    if (gains.gpFleet.length > 0)
    {
        for (var p = 0; p < 3 && p < gains.gpFleet.length; p++) {
            var symbol = (p == 0 ? discordUtils.symbols.firstPlace : (p == 1 ? discordUtils.symbols.secondPlace : discordUtils.symbols.thirdPlace));
            desc += symbol + " " + gains.gpFleet[p].playerName + ": " + gains.gpFleet[p].gain.toLocaleString("en-US") + "\r\n"
        }
    } else {
        desc += "No one gained any Fleet GP :cry:\r\n";
    }
    if (userGpFleet)
        desc += "Your growth: " + userGpFleet.gain.toLocaleString("en-US") + "\r\n";
    
    desc += "\r\GAC Skill Rating Growth:\r\n";
    if (gains.skillRating.length > 0 && gains.skillRating[0].gain > 0)
    {
        for (var p = 0; p < 3 && p < gains.skillRating.length; p++) {
            var symbol = (p == 0 ? discordUtils.symbols.firstPlace : (p == 1 ? discordUtils.symbols.secondPlace : discordUtils.symbols.thirdPlace));
            desc += symbol + " " + gains.skillRating[p].playerName + ": " + gains.skillRating[p].gain.toLocaleString("en-US") + "\r\n"
        }
    } else {
        desc += "No one gained any GAC Skill Rating :cry:\r\n";
    }
    if (userSkillRating && userSkillRating.gain != 0)
        desc += "Your growth: " + userSkillRating.gain.toLocaleString("en-US") + "\r\n";

    var embed = new EmbedBuilder()
        .setTitle("Guild Growth Achievements - Last 30 Days")
        .setColor(0xD2691E)
        .setDescription(desc);

    return embed;
}

function getNameForOmicronType(type)
{
    switch (type)
    {
        case swapiUtils.OMICRON_MODE.TERRITORY_WAR_OMICRON:
            return "TW";
        case swapiUtils.OMICRON_MODE.TERRITORY_BATTLE_BOTH_OMICRON:
            return "TB";
        case allGACOmicronTypeCode:
            return "GAC";
        case swapiUtils.OMICRON_MODE.CONQUEST_OMICRON:
            return "Conquest";
        case allOmicronTypeCode:
            return "All";
        default: 
            return "Error";
    }
}

async function getGuildOmicronReport(allycode, type)
{
    // get all players in guild
    // loop through, create dictionary of omicron unit -> count
    // show table, rows are players, columns are units, values are count
    let desc = "";
    var guildInfo = await guildUtils.getGuildIdAndAllyCodesForGuildByAllycode(allycode);
    if (!guildInfo || !guildInfo.guildId || !guildInfo.allycodes || guildInfo.allycodes.length == 0)
    {
        // guild isn't registered
        // get a list of guild members from API
        guildInfo = await guildUtils.getGuildDataFromAPI({ allycode: allycode }, { useCache: true, includeRecentGuildActivityInfo: true });
    }

    let playerData = await getPlayerDataForGuild(guildInfo);

    let playerOmicrons = [];
    let guildUnitOmicronCounts = [];
    
    let allAbilities = await swapiUtils.getAbilitiesList();
    let allUnits = await swapiUtils.getUnitsList();

    for (var p in playerData)
    {
        let player = playerData[p];
        let playerOmicronData = {
            allycode: swapiUtils.getPlayerAllycode(player),
            name: "",
            omicronUnitsAndCounts: {} 
        }
        playerOmicrons.push(playerOmicronData);
    
        playerOmicronData.name = swapiUtils.getPlayerName(player);
        let playerOmisByType;
        // GAC is handled differently
        if (type === allOmicronTypeCode)
        {
            playerOmisByType = player.overview.omicrons.flat().filter(o => o != null);
        }
        else if (type === allGACOmicronTypeCode)
        {
            playerOmisByType = [];
            if (player.overview.omicrons[swapiUtils.OMICRON_MODE.TERRITORY_TOURNAMENT_OMICRON]) playerOmisByType = playerOmisByType.concat(player.overview.omicrons[swapiUtils.OMICRON_MODE.TERRITORY_TOURNAMENT_OMICRON]);
            if (player.overview.omicrons[swapiUtils.OMICRON_MODE.TERRITORY_TOURNAMENT_3_OMICRON]) playerOmisByType = playerOmisByType.concat(player.overview.omicrons[swapiUtils.OMICRON_MODE.TERRITORY_TOURNAMENT_3_OMICRON]);
            if (player.overview.omicrons[swapiUtils.OMICRON_MODE.TERRITORY_TOURNAMENT_5_OMICRON]) playerOmisByType = playerOmisByType.concat(player.overview.omicrons[swapiUtils.OMICRON_MODE.TERRITORY_TOURNAMENT_5_OMICRON]);
        }
        else {
            playerOmisByType = player.overview.omicrons[type];
        }
        for (var o in playerOmisByType)
        {
            let omiId = playerOmisByType[o];
            let ability = allAbilities.find(a => a.base_id === omiId);
            let unitId = ability.character_base_id;
            let unitName = allUnits.find(u => swapiUtils.getUnitDefId(u) === unitId)?.name;

            let curCount = playerOmicronData.omicronUnitsAndCounts[unitId];
            if (!curCount) playerOmicronData.omicronUnitsAndCounts[unitId] = 1;
            else playerOmicronData.omicronUnitsAndCounts[unitId]++;

            let guildUnitCount = guildUnitOmicronCounts.find(u => u === unitId);
            if (!guildUnitCount) guildUnitOmicronCounts.push({ unitId: unitId, unitName: unitName, count: 1})
            else guildUnitCount.count++;
        }
    }

    guildUnitOmicronCounts.sort((a, b) => a.count - b.count);

    let csvData = [];

    const playerNameWidth = 25;
    desc =  "Name".padEnd(playerNameWidth) + " | #\r\n";
    desc += "".padEnd(25, "-") + "-|---\r\n";

    let playersNamesAndCounts = []; 
    for (var p in playerOmicrons)
    {
        let playerOmicronData = playerOmicrons[p];

        let totalOmisOfType = Object.keys(playerOmicronData.omicronUnitsAndCounts)
            .reduce((p, c, i) => {
                return p + playerOmicronData.omicronUnitsAndCounts[c];
        }, 0);

        playersNamesAndCounts.push({ name: playerOmicronData.name, count: totalOmisOfType});

        // populate data for CSV
        let csvDataObj = {
            "Name": playerOmicronData.name,
            "Ally Code": playerOmicronData.allycode,
            "Total": totalOmisOfType
        };

        for(var go in guildUnitOmicronCounts)
        {
            let unitName = guildUnitOmicronCounts[go].unitName;
            csvDataObj[unitName] = playerOmicronData.omicronUnitsAndCounts[guildUnitOmicronCounts[go].unitId];
        }
        csvData.push(csvDataObj);
        
    }

    // summary should be sorted by count, descending:
    // name: number of [type] omis
    // name: number of [type] omis

    playersNamesAndCounts.sort((a, b) => { return (a.count === b.count) ? (a.name.localeCompare(b.name)) : (b.count - a.count) });
    desc += playersNamesAndCounts.map(obj => `${obj.name.padEnd(playerNameWidth).substring(0, playerNameWidth)} | ${obj.count}`).join("\r\n");

    const csvParsingOptions = { defaultValue: 0 }
    csvData.sort((a, b) => { return (a.Total === b.Total) ? (a.Name.localeCompare(b.Name)) : (b.Total - a.Total) });
    let csvText = await parseAsync(csvData, csvParsingOptions);
    let buffer = Buffer.from(csvText);
    
    let embed = new EmbedBuilder()
        .setTitle(`Guild ${getNameForOmicronType(type)} Omicrons Summary`)
        .setColor(0xD2691E)
        .setDescription("```" + desc + "```\r\n" + `Full ${getNameForOmicronType(type)} omicron data is in the .CSV file attached to this message. You may need to scroll up to see it. Only characters for which your guild has one or more omicrons of the selected type are shown.`)
        .setFooter({ text: "Data is cached for 4 hours. If you do not see an omicron that was just applied, try again later." });

    let csvAttachment = new AttachmentBuilder(buffer, { name: `guild_${getNameForOmicronType(type)}_omicrons_${new Date().getTime()}.csv`});
    return { embeds: [embed], files: [csvAttachment] };
}

async function getGuildDatacronReport(allycode, set)
{
    // get all players in guild
    // loop through, create dictionary of omicron unit -> count
    // show table, rows are players, columns are units, values are count
    let desc = "";
    var guildInfo = await guildUtils.getGuildIdAndAllyCodesForGuildByAllycode(allycode);
    if (!guildInfo || !guildInfo.guildId || !guildInfo.allycodes || guildInfo.allycodes.length == 0)
    {
        // guild isn't registered
        // get a list of guild members from API
        guildInfo = await guildUtils.getGuildDataFromAPI({ allycode: allycode }, { useCache: true, includeRecentGuildActivityInfo: true });
    }

    let playerData = await getPlayerDataForGuild(guildInfo);

    let playerDatacrons = [];
    
    for (var player of playerData)
    {
        let playerDatacronData = {
            allycode: swapiUtils.getPlayerAllycode(player),
            name: swapiUtils.getPlayerName(player),
            counts: {
                byLevel: new Array(10),
                rerolls: 0
            }
        }
        playerDatacronData.counts.byLevel.fill(0);
        playerDatacrons.push(playerDatacronData);

        let playerDCs = swapi.getPlayerDatacrons(player);
    
        if (set) playerDCs = playerDCs.filter(d => d.setId === set);
        
        playerDCs.map(dc => {
            playerDatacronData.counts.byLevel[dc.affix.length]++;
            playerDatacronData.counts.rerolls += dc.rerollCount;
        })
    }

    playerDatacrons.sort((a, b) => {
        return (b.counts.byLevel[9] - a.counts.byLevel[9]) || 
                (b.counts.byLevel[6] - a.counts.byLevel[6]) ||
                (b.counts.byLevel[3] - a.counts.byLevel[3]) ||
                (b.counts.rerolls - a.counts.rerolls) ||
                a.name.localeCompare(b.name);
    });

    const csvDatacronData = playerDatacrons.map(d => {
        let obj = {
            name: d.name,
            allycode: d.allycode,
        };

        for (let level in d.counts.byLevel)
        {
            obj["L" + level] = d.counts.byLevel[level];
        }

        obj.rerolls = d.counts.rerolls;

        return obj;
    });

    
    const csvParsingOptions = { defaultValue: 0 }
    csvDatacronData.sort((a, b) => { return a.name.localeCompare(b.name); });
    let csvText = await parseAsync(csvDatacronData, csvParsingOptions);
    let buffer = Buffer.from(csvText);

    let csvAttachment = new AttachmentBuilder(buffer, { name: `guild_datacrons_${new Date().getTime()}.csv`});

    const playerNameWidth = 25;
    desc =  "Name".padEnd(playerNameWidth) + " | L9 | L6 | L3 | Rerolls\r\n";
    desc += "".padEnd(25, "-") +             "-|----|----|----|--------\r\n";

    desc += playerDatacrons.map(obj => `${discordUtils.fit(obj.name, playerNameWidth)}${discordUtils.LTR} | ${discordUtils.fit(obj.counts.byLevel[9], 2)} | ${discordUtils.fit(obj.counts.byLevel[6]+obj.counts.byLevel[7]+obj.counts.byLevel[8], 2)} | ${discordUtils.fit(obj.counts.byLevel[3]+obj.counts.byLevel[4]+obj.counts.byLevel[5], 2)} | ${discordUtils.fit(obj.counts.rerolls, 3)}`).join("\r\n");
    
    let embed = new EmbedBuilder()
        .setTitle(`Guild Datacrons Summary Report`)
        .setColor(0xD2691E)
        .setDescription("```" + desc + "```\r\n")
        .setFooter({ text: "Data is cached for 4 hours. If you do not see a datacron that was just gained, try again later." });

    return { embeds: [embed], files: [csvAttachment] };
}

async function getReportReek(allycode)
{
    
    let desc = "", data;
    var guildInfo = await guildUtils.getGuildIdAndAllyCodesForGuildByAllycode(allycode);
    if (!guildInfo || !guildInfo.guildId || !guildInfo.allycodes || guildInfo.allycodes.length == 0)
    {
        // guild isn't registered
        // get a list of guild members from API
        let guildData = await guildUtils.getGuildDataFromAPI({ allycode: allycode }, { includeRecentGuildActivityInfo: true, useCache: true, cacheTimeHours: 24 });

        guildName = guildUtils.getGuildName(guildData);
        
        playerData = await getPlayerDataForGuild(guildData);

        desc += "Found " + playerData.length + " players in **" + guildName + "**.\n"

        let reekLosers = playerData.filter(p => swapiUtils.getPlayerUnlockedPortraits(p)?.indexOf("PLAYERPORTRAIT_REEK") >= 0);

        if (reekLosers.length > 0)
            desc += reekLosers
                .map(l => l.name + " (" + l.allyCode + ")")
                .sort((a, b) => a.localeCompare(b, undefined, { sensitivity: 'base' }))
                .join("\n");
        else
            desc += "All guild members have beaten the Reek."
    }
    else
    {
        const reekData = await database.db.any('SELECT allycode, player_name, reek_win FROM player_data_static WHERE reek_win = FALSE AND allycode LIKE ANY ($1) ORDER BY player_name ASC', [guildInfo.allycodes]);


        for (var d in reekData)
        {
            var dataRow = reekData[d];
            desc += dataRow.player_name + " (" + dataRow.allycode + ")\r\n";
        }

        if (desc == "")
            desc = "Congratulations!  All guild members have beaten the Reek.";
    }
    var embed = new EmbedBuilder()
        .setTitle("Guild members that have not beaten the Reek mission")
        .setColor(0xD2691E)
        .setDescription(desc);

    
    return embed;
}

async function getPlayerDataForGuild(guildData)
{
    let allycodes = guildData.allycodes;
    if (!allycodes) allycodes = guildUtils.getGuildAllycodesForAPIGuild(guildData)

    return (await swapiUtils.getPlayers({ allycodes: allycodes, useCache: true })).players;
}

async function getCapitalShipData(guildData)
{    
    let result = await getPlayerDataForGuild(guildData);
    let csData = new Array(UNIT_DATA.CapitalShips.BarChartConfig.Labels.length).fill(0);

    result.forEach(p => {
        let playerCS = swapiUtils.getPlayerCapitalShips(p);

        playerCS.forEach(cs => {
            csData[UNIT_DATA.CapitalShips[cs].order]++
        });
    })

    return csData;
}

async function getGLData(guildData)
{    
    let result = await getPlayerDataForGuild(guildData);
    let glData = new Array(UNIT_DATA.GalacticLegends.BarChartConfig.Labels.length).fill(0);

    result.forEach(p => {
        let playerGLs = swapiUtils.getPlayerGalacticLegends(p);

        playerGLs.forEach(gl => {
            glData[UNIT_DATA.GalacticLegends[gl].order]++
        });
    })

    return glData;
}

async function getChartGLsForGuild(allycode)    
{
    const glData = await database.db.any(
        `SELECT   
         COUNT(*) FILTER (WHERE gl_rey = TRUE)::int AS rey_count, 
         COUNT(*) FILTER (WHERE gl_slkr = TRUE)::int AS slkr_count,
         COUNT(*) FILTER (WHERE gl_jml = TRUE)::int AS jml_count,
         COUNT(*) FILTER (WHERE gl_see = TRUE)::int AS see_count,
         COUNT(*) FILTER (WHERE gl_jmk = TRUE)::int AS jmk_count,
         COUNT(*) FILTER (WHERE gl_lv = TRUE)::int AS lv_count,
         COUNT(*) FILTER (WHERE gl_jabba = TRUE)::int AS jabba_count,
         COUNT(*) FILTER (WHERE gl_leia = TRUE)::int AS leia_count,
         COUNT(*) FILTER (WHERE gl_ahsoka = TRUE)::int AS ahsoka_count,
         COUNT(*)::int AS players_count,
		 MIN(timestamp) AS timestamp
         FROM player_data_static WHERE allycode LIKE ANY 
         (SELECT allycode FROM guild_players WHERE guild_id = (SELECT guild_id FROM guild_players WHERE allycode = $1))`, allycode
    );


    let data;
    let guildName;
    let date;

    if (glData[0].players_count == 0)
    {
        try {
            // guild not in guild data, so do this the hard way :)
            
            // get a list of guild members
            let guildData = await guildUtils.getGuildDataFromAPI({ allycode: allycode }, { useCache: true, includeRecentGuildActivityInfo: true });

            guildName = guildUtils.getGuildName(guildData);
            
            data = await getGLData(guildData);
        }
        catch (err)
        {
            logger.error(formatError(err));
            return discordUtils.createErrorEmbed("Error", "Failed to retrieve guild data for ally code " + allycode + ". " + err.message);    
        }
    }
    else
    {
        data = [glData[0].rey_count, glData[0].slkr_count, glData[0].jml_count, glData[0].see_count, glData[0].jmk_count, glData[0].lv_count, glData[0].jabba_count, glData[0].leia_count, glData[0].ahsoka_count];
        date = glData[0].timestamp; 

        const guildData = await database.db.one("SELECT guild_name FROM guild WHERE guild_id = (SELECT guild_id FROM guild_players WHERE allycode = $1)", [allycode]);

        guildName = guildData.guild_name;
    }
    
    if (!date) date = new Date();

    const width = 750;
    const height = 500;
    const chartCallback = (ChartJS) => {
        ChartJS.defaults.global.responsive = true;
        ChartJS.defaults.global.maintainAspectRatio = false;

        ChartJS.plugins.register({
            beforeDraw: function(chartInstance) {
              var ctx = chartInstance.chart.ctx;
              ctx.fillStyle = "black";
              ctx.fillRect(0, 0, chartInstance.chart.width, chartInstance.chart.height);
            },
            afterDraw: function (chartInstance) {
                var ctx = chartInstance.chart.ctx;
                ctx.textBaseline = "middle";
                ctx.fillStyle = 'white'
                ctx.font = "15px Sans";
                ctx.textAlign = "center";

                for (var li = 0; li < data.length; li++)
                {
                    var labelText = data[li];
                    var x = chartInstance.chart.controller.data.datasets[0]._meta[0].data[li]._view.x;
                    var y = Math.min(chartInstance.chart.controller.data.datasets[0]._meta[0].data[li]._view.y + 15, height - 95);
                    ctx.fillText(labelText, x, y);
                }
            }
          });
    };

    var totalGls = data.reduce((x, y) => x + y);
    var chartTitle = [`Total GLs for ${guildName}: ${totalGls.toLocaleString('en')}`, date.toUTCString()];

    const configuration = {
        type: 'bar',
        data: {
            labels: UNIT_DATA.GalacticLegends.BarChartConfig.Labels,
            datasets: [{
                label: 'Count of Galactic Legends',
                data: data,
                backgroundColor: UNIT_DATA.GalacticLegends.BarChartConfig.ReportBackgroundColors,
                borderColor: UNIT_DATA.GalacticLegends.BarChartConfig.ReportBorderColors,
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        fontColor: 'white',
                        beginAtZero: true,
                    }
                }],
                xAxes: xAxesFont,
            },
            title: {
                display: true,
                align: 'center',
                text: chartTitle,
                fontColor: 'white',
                position: 'bottom'
            },
            legend: legend
        }
    };

    const chartJSNodeCanvas = new ChartJSNodeCanvas({width, height, chartCallback: chartCallback});

    const imageBuffer = await chartJSNodeCanvas.renderToBuffer(configuration);    

    var fileName = "chart-gls-" + allycode + "-" + date.getTime() + ".png";

    return createImageReportResultObj(imageBuffer, fileName);
}

async function getChartCapitalShipsForGuild(allycode)
{
    const csData = await database.db.any(
        `SELECT capital_ships, timestamp
        FROM player_data_static WHERE allycode LIKE ANY
        (SELECT allycode FROM guild_players WHERE guild_id = (SELECT guild_id FROM guild_players WHERE allycode = $1))`, [allycode]);

    let data;
    let guildName;
    let date;

    if (csData.length == 0)
    {
        try {
            // guild not in guild data, so do this the long way :)
            
            // get a list of guild members
            let guildData = await guildUtils.getGuildDataFromAPI({ allycode: allycode }, { useCache: true, includeRecentGuildActivityInfo: true });

            guildName = guildUtils.getGuildName(guildData);
            
            data = await getCapitalShipData(guildData);
        }
        catch (err)
        {
            logger.error(formatError(err));
            return discordUtils.createErrorEmbed("Error", "Failed to retrieve guild data for ally code " + allycode + ". " + err.message);    
        }
    }
    else
    {
        let unitsList = await swapiUtils.getUnitsList();
        let capShips = unitsList.filter(u => u.is_capital_ship).map(c => swapiUtils.getUnitDefId(c));
        data = new Array();

        capShips.sort((a, b) => UNIT_DATA.CapitalShips[a].order - UNIT_DATA.CapitalShips[b].order);

        for (let c of capShips)
        {
            data.push(csData.filter(r => r.capital_ships?.find(cs => cs === c)).length);
        }

        date = csData[0].timestamp; 

        const guildData = await database.db.one("SELECT guild_name FROM guild WHERE guild_id = (SELECT guild_id FROM guild_players WHERE allycode = $1)", [allycode]);

        guildName = guildData.guild_name;
    }
    
    if (!date) date = new Date();

    const width = 1000;
    const height = 500;
    const chartCallback = (ChartJS) => {
        ChartJS.defaults.global.responsive = true;
        ChartJS.defaults.global.maintainAspectRatio = false;

        ChartJS.plugins.register({
            beforeDraw: function(chartInstance) {
              var ctx = chartInstance.chart.ctx;
              ctx.fillStyle = "black";
              ctx.fillRect(0, 0, chartInstance.chart.width, chartInstance.chart.height);
            },
            afterDraw: function (chartInstance) {
                var ctx = chartInstance.chart.ctx;
                ctx.textBaseline = "middle";
                ctx.fillStyle = 'white'
                ctx.font = "15px Sans";
                ctx.textAlign = "center";

                for (var li = 0; li < data.length; li++)
                {
                    var labelText = data[li];
                    var x = chartInstance.chart.controller.data.datasets[0]._meta[0].data[li]._view.x;
                    var y = Math.min(chartInstance.chart.controller.data.datasets[0]._meta[0].data[li]._view.y + 15, height - 95);
                    ctx.fillText(labelText, x, y);
                }
            }
          });
    };

    var chartTitle = [`Capital Ships for ${guildName}`, date.toUTCString()];

    const configuration = {
        type: 'bar',
        data: {
            labels: UNIT_DATA.CapitalShips.BarChartConfig.Labels,
            datasets: [{
                label: 'Count of Capital Ships',
                data: data,
                backgroundColor: UNIT_DATA.CapitalShips.BarChartConfig.ReportBackgroundColors,
                borderColor: UNIT_DATA.CapitalShips.BarChartConfig.ReportBorderColors,
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        fontColor: 'white',
                        beginAtZero: true,
                    }
                }],
                xAxes: xAxesFont,
            },
            title: {
                display: true,
                align: 'center',
                text: chartTitle,
                fontColor: 'white',
                position: 'bottom'
            },
            legend: legend
        }
    };

    const chartJSNodeCanvas = new ChartJSNodeCanvas({width, height, chartCallback: chartCallback});

    const imageBuffer = await chartJSNodeCanvas.renderToBuffer(configuration);    

    var fileName = "chart-cs-" + allycode + "-" + date.getTime() + ".png";

    return createImageReportResultObj(imageBuffer, fileName);
}

async function getChartRaidOverTimeForAllyCode(allycode, raid)
{
    const limit = 40;
    
    let player = await comlink.getPlayerArenaProfileByAllycode(allycode);
    let playerId = player.playerId;
    var playerData = await playerUtils.getPlayerRaidScoresByPlayerId(playerId, {raidId: raidCommon[raid].raidId, limit: limit});
    if (playerData.length === 0)
    {
        throw new Error(`No ${raidCommon[raid].description} data for allycode ${allycode} yet.`);
    }

    //grab raid_score_estimate and timestamp ordered by timestamp
    var estimatesData = await database.db.any(
        `SELECT raid_score_estimate, timestamp 
        FROM player_data 
        WHERE allycode = $1 AND timestamp >= $2
        ORDER BY timestamp`, [allycode, new Date(playerData[playerData.length - 1].end_time_seconds * 1000)]);
    var labels = new Array();
    var data = new Array();
    //create new array for estimates
    var estimates = new Array();

    let step = 1; //Math.min(Math.ceil(limit / 30), Math.ceil(playerData.length / 30));

    // grab their most recent recorded name
    var playerName = player.name;
    
    // Loop through playerData to populate labels and data
    for (var i = 0; i < playerData.length; i += step) {
        var date = new Date(playerData[i].end_time_seconds * 1000);
        var label = monthShortNames[date.getUTCMonth()] + " " + date.getUTCDate();
        labels.push(label);
        data.push(playerData[i].score);

        // select the first estimate that is at least the date of the actual raid
        let timeToMatch = new Date(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate()).getTime();
        let estimate;
        for (let ed of estimatesData)
        {
            let estimateTime = new Date(ed.timestamp.getUTCFullYear(), ed.timestamp.getUTCMonth(), ed.timestamp.getUTCDate()).getTime();
            if (estimateTime >= timeToMatch) {
                estimate = ed.raid_score_estimate;
                break;
            }
        }
        
        if (estimate && estimate[raid]) estimates.push(estimate[raid])
        else estimates.push(null);
    }

    // earliest on the left
    labels.reverse();
    data.reverse();
    estimates.reverse();
    const width = 900;
    const height = 400;
    const chartCallback = (ChartJS) => {
        ChartJS.defaults.global.responsive = true;
        ChartJS.defaults.global.maintainAspectRatio = false;

        ChartJS.plugins.register({
            beforeDraw: function(chartInstance) {
              var ctx = chartInstance.chart.ctx;
              ctx.fillStyle = "black";
              ctx.fillRect(0, 0, chartInstance.chart.width, chartInstance.chart.height);
            }
          });
    };
    
    var chartTitle = raidCommon[raid].description + " Scores over time for " + playerName + " (" + allycode + ")";

    const configuration = {
        type: 'line',
        data: {
            datasets: [
                {
                    borderColor: raidCommon[raid].color, // Change the color as needed
                    borderWidth: 1,
                    borderDash: [6, 1], // Define the dashed line pattern [dash length, gap length]
                    cubicInterpolationMode: 'monotone',
                    pointRadius: 1,
                    label: "Estimates", // Change the label as needed
                    data: estimates // Define your data for the new dataset
                },
                {
                    fill: true,
                    borderColor: 'rgba(255,255,255, .5)',
                    borderWidth: 1,
                    backgroundColor: getGradientBackgroundColor,
                    cubicInterpolationMode: 'monotone',
                    label: "Actuals",
                    data: data
                }
            ],
            labels: labels
        },
        options: {
            title: {
                display: true,
                align: 'center',
                position: 'top',
                fontColor: 'white',
                text: chartTitle
            },
            scales: {
                yAxes: [{
                    ticks: {
                        fontColor: 'white',
                        userCallback: function(value, index, values) {
                            if (value >= 1000000) return (value / 1000000).toLocaleString() + "M";
                            else return (value / 1000).toLocaleString() + "K";
                        }
                    }
                }],
                xAxes: xAxesFont,
            },
            legend: legend
        }
    };
    
    const chartJSNodeCanvas = new ChartJSNodeCanvas({width, height, chartCallback: chartCallback});

    const imageBuffer = await chartJSNodeCanvas.renderToBuffer(configuration);    

    var fileName = "chart-raid-" + allycode + "-" + Date.now() + ".png";

    return createImageReportResultObj(imageBuffer, fileName);
}

async function getChartKraytOverTimeForAllyCode(allycode)
{
    const limit = 40;
    
    let player = await comlink.getPlayerArenaProfileByAllycode(allycode);
    let playerId = player.playerId;
    var playerData = await playerUtils.getPlayerRaidScoresByPlayerId(playerId, {raidId: raidCommon.KRAYT.raidId, limit: limit});
    //grab raid_score_estimate and timestamp ordered by timestamp
    var kraytestimate = await database.db.any(
        `SELECT raid_score_estimate, timestamp 
        FROM player_data 
        WHERE allycode = $1 AND timestamp >= $2
        ORDER BY timestamp`, [allycode, new Date(playerData[playerData.length - 1].end_time_seconds * 1000)]);
    var labels = new Array();
    var data = new Array();
    //create new array for estimates
    var estimates = new Array();
    if (playerData.length == 0 || !kraytestimate)
        throw new Error("No data for allycode " + allycode);

    let step = 1; //Math.min(Math.ceil(limit / 30), Math.ceil(playerData.length / 30));

    // grab their most recent recorded name
    var playerName = player.name;
    
    // Loop through playerData to populate labels and data
    for (var i = 0; i < playerData.length; i += step) {
        var date = new Date(playerData[i].end_time_seconds * 1000);
        var label = monthShortNames[date.getUTCMonth()] + " " + date.getUTCDate();
        labels.push(label);
        data.push(playerData[i].score);

        let estimate = kraytestimate.find(k => 
            new Date(k.timestamp.getUTCFullYear(), k.timestamp.getUTCMonth(), k.timestamp.getUTCDate()).getTime() ===
            new Date(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate()).getTime())?.raid_score_estimate?.KRAYT ?? null;
        estimates.push(estimate);
    }

    // earliest on the left
    labels.reverse();
    data.reverse();
    estimates.reverse();
    const width = 900;
    const height = 400;
    const chartCallback = (ChartJS) => {
        ChartJS.defaults.global.responsive = true;
        ChartJS.defaults.global.maintainAspectRatio = false;

        ChartJS.plugins.register({
            beforeDraw: function(chartInstance) {
              var ctx = chartInstance.chart.ctx;
              ctx.fillStyle = "black";
              ctx.fillRect(0, 0, chartInstance.chart.width, chartInstance.chart.height);
            }
          });
    };
    
    var chartTitle = "Krayt Scores over time for " + playerName + " (" + allycode + ")";

    const configuration = {
        type: 'line',
        data: {
            datasets: [
                {
                    borderColor: 'rgba(230,190,70, 1)', // Change the color as needed
                    borderWidth: 1,
                    borderDash: [6, 1], // Define the dashed line pattern [dash length, gap length]
                    cubicInterpolationMode: 'monotone',
                    pointRadius: 0,
                    label: "Estimates", // Change the label as needed
                    data: estimates // Define your data for the new dataset
                },
                {
                    fill: true,
                    borderColor: 'rgba(255,255,255, .5)',
                    borderWidth: 1,
                    backgroundColor: getGradientBackgroundColor,
                    cubicInterpolationMode: 'monotone',
                    label: "Actuals",
                    data: data
                }
            ],
            labels: labels
        },
        options: {
            title: {
                display: true,
                align: 'center',
                position: 'top',
                fontColor: 'white',
                text: chartTitle
            },
            scales: {
                yAxes: [{
                    ticks: {
                        fontColor: 'white',
                        userCallback: function(value, index, values) {
                            if (value >= 1000000) return (value / 1000000).toLocaleString() + "M";
                            else return (value / 1000).toLocaleString() + "K";
                        }
                    }
                }],
                xAxes: xAxesFont,
            },
            legend: legend
        }
    };
    
    const chartJSNodeCanvas = new ChartJSNodeCanvas({width, height, chartCallback: chartCallback});

    const imageBuffer = await chartJSNodeCanvas.renderToBuffer(configuration);    

    var fileName = "chart-krayt-" + allycode + "-" + Date.now() + ".png";

    return createImageReportResultObj(imageBuffer, fileName);
}

async function getChartGuildRaidOverTime(allycode, raid)
{
    const limit = 50, raidData = raidCommon[raid];
    
    let guildMetaData = await guildUtils.getGuildMetadataByAllyCode(allycode);

    if (guildMetaData.guild_id == null)
        throw new Error(`Account is not in a guild or the guild is not tracked by WookieeBot.`);
    
    var guildData = await guildUtils.getGuildRaidScores(guildMetaData.comlink_guild_id, { raidId: raidData.raidId, limit: limit });

    if (guildData.length === 0)
    {
        throw new Error(`No ${raidCommon[raid].description} data for guild yet.`);
    }

    // grab most recent recorded guild name
    let guildName = guildMetaData.guild_name;

    var labels = new Array();
    var data = new Array();

    if (guildData.length == 0)
        throw new Error(`No ${raidData.description} data for guild ${guildName} (${guildMetaData.comlink_guild_id}).`);

    let step = 1; //Math.min(Math.ceil(limit / 30), Math.ceil(guildData.length / 30));

    for (var i = 0; i < guildData.length; i += step) {
        var date = new Date(guildData[i].end_time_seconds*1000);
        var label = monthShortNames[date.getUTCMonth()] + " " + date.getUTCDate();
        labels.push(label);
        data.push(parseInt(guildData[i].score));
    }

    // earliest on the left
    labels.reverse();
    data.reverse();

    const width = 900;
    const height = 400;
    const chartCallback = (ChartJS) => {
        ChartJS.defaults.global.responsive = true;
        ChartJS.defaults.global.maintainAspectRatio = false;

        ChartJS.plugins.register({
            beforeDraw: function(chartInstance) {
              var ctx = chartInstance.chart.ctx;
              ctx.fillStyle = "black";
              ctx.fillRect(0, 0, chartInstance.chart.width, chartInstance.chart.height);
            }
          });
    };
    const configuration = {
        type: 'line',
        data: {
            datasets: [{
                fill: true,
                borderColor: 'rgba(255,255,255, 1)',
                borderWidth: 1,
                backgroundColor: getGradientBackgroundColor,
                cubicInterpolationMode: 'monotone',
                label: `${raidData.description} Scores over time for ${guildName}`,
                data: data
                },
            ],
            labels: labels
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        fontColor: 'white',
                        userCallback: function(value, index, values) {
                            return (value / 1000000).toLocaleString() + "M";   // this is all we need
                        }
                    }
                }],
                xAxes: xAxesFont,
            },
            legend: legend
        }
    };
    
    const chartJSNodeCanvas = new ChartJSNodeCanvas({width, height, chartCallback: chartCallback});

    const imageBuffer = await chartJSNodeCanvas.renderToBuffer(configuration);    

    var fileName = "chart-raidguild-" + guildMetaData.comlink_guild_id + "-" + Date.now() + ".png";
    
    const imageAttachment = new AttachmentBuilder(imageBuffer, { name: fileName });
    const csvText = await getPlayerRaidScoresCSV(guildMetaData, raidData);

    let buffer = Buffer.from(csvText);
    
    let csvAttachment = new AttachmentBuilder(buffer, { name: `guild_${raidData.raidId}_scores_${new Date().getTime()}.csv`});
    return { files: [imageAttachment, csvAttachment] };
}

function secondsToISODateString(s) 
{
    return new Date(s*1000).toISOString().split('T')[0]; 
}

async function getPlayerRaidScoresCSV(guildMetaData, raidData)
{
    
    let individualRaidScores = await database.db.any(
        `SELECT end_time_seconds, score, player_id, allycode, player_name
        FROM player_raid_score
        JOIN guild_players USING (player_id) 
        WHERE player_id IN (SELECT player_id FROM guild_players WHERE guild_id = $1) AND raid_id = $2 
        ORDER BY player_name, allycode, end_time_seconds DESC`, 
        [guildMetaData.guild_id, raidData.raidId]);
 
    // transform individualRaidScores into CSV-ready data
    // columns are:
    //      player_name
    //      allycode
    //      <each end time as a date, starting with most recent>

    let endTimes = [];
    for (let i = 0; i < individualRaidScores.length; i++)
    {
        if (endTimes.indexOf(individualRaidScores[i].end_time_seconds) === -1) endTimes.push(secondsToISODateString(individualRaidScores[i].end_time_seconds));
    }

    let csvData = [];
    let obj;
    for (let i = 0; i < individualRaidScores.length; i++)
    {
        let iData = individualRaidScores[i];
        if (!obj || obj.allycode !== iData.allycode)
        {
            obj = {
                player_name: iData.player_name,
                allycode: iData.allycode
            };

            for (let e of endTimes) obj[e] = -1;

            csvData.push(obj);
        }

        obj[secondsToISODateString(iData.end_time_seconds)] = iData.score;
    }

    
    const csvParsingOptions = { defaultValue: 0 }
    return await parseAsync(csvData, csvParsingOptions);
}

async function getChartGuildKraytOverTime(allycode)
{
    const limit = 50;
    
    let guildMetaData = await guildUtils.getGuildMetadataByAllyCode(allycode);

    if (guildMetaData.guild_id == null)
        throw new Error(`Account is not in a guild or the guild is not tracked by WookieeBot.`);

    var guildData = await guildUtils.getGuildRaidScores(guildMetaData.comlink_guild_id, { raidId: raidCommon.KRAYT.raidId, limit: limit });

    // grab most recent recorded guild name
    let guildName = guildMetaData.guild_name;

    var labels = new Array();
    var data = new Array();

    if (guildData.length == 0)
        throw new Error(`No Krayt data for guild ${guildName} (${guildMetaData.comlink_guild_id}).`);

    let step = 1; //Math.min(Math.ceil(limit / 30), Math.ceil(guildData.length / 30));

    for (var i = 0; i < guildData.length; i += step) {
        var date = new Date(guildData[i].end_time_seconds*1000);
        var label = monthShortNames[date.getUTCMonth()] + " " + date.getUTCDate();
        labels.push(label);
        data.push(parseInt(guildData[i].score));
    }

    // earliest on the left
    labels.reverse();
    data.reverse();

    const width = 900;
    const height = 400;
    const chartCallback = (ChartJS) => {
        ChartJS.defaults.global.responsive = true;
        ChartJS.defaults.global.maintainAspectRatio = false;

        ChartJS.plugins.register({
            beforeDraw: function(chartInstance) {
              var ctx = chartInstance.chart.ctx;
              ctx.fillStyle = "black";
              ctx.fillRect(0, 0, chartInstance.chart.width, chartInstance.chart.height);
            }
          });
    };
    const configuration = {
        type: 'line',
        data: {
            datasets: [{
                fill: true,
                borderColor: 'rgba(255,255,255, 1)',
                borderWidth: 1,
                backgroundColor: getGradientBackgroundColor,
                cubicInterpolationMode: 'monotone',
                label: "Krayt Scores over time for " + guildName,
                data: data
                },
            ],
            labels: labels
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        fontColor: 'white',
                        userCallback: function(value, index, values) {
                            return (value / 1000000).toLocaleString() + "M";   // this is all we need
                        }
                    }
                }],
                xAxes: xAxesFont,
            },
            legend: legend
        }
    };
    
    const chartJSNodeCanvas = new ChartJSNodeCanvas({width, height, chartCallback: chartCallback});

    const imageBuffer = await chartJSNodeCanvas.renderToBuffer(configuration);    

    var fileName = "chart-kraytguild-" + guildMetaData.comlink_guild_id + "-" + Date.now() + ".png";

    return createImageReportResultObj(imageBuffer, fileName);
}

async function getChartGacOverTimeForAllyCode(allycode)
{
    const limit = 90;
    var playerData = await playerUtils.getAllPlayerDataByAllyCode(allycode, limit);
    
    var labels = new Array();
    var data = new Array();

    if (playerData.length == 0)
        throw new Error("No data for allycode " + allycode);

    playerData.sort((a,b) => b.timestamp - a.timestamp);
    let step = Math.min(Math.ceil(limit / 30), Math.ceil(playerData.length / 30));
    let player = await comlink.getPlayerArenaProfileByAllycode(allycode);
    // grab their most recent recorded name
    var playerName = player.name;
    
    var gacAveragePerDay = "Not enough data.";
    
    if (playerData.length > 1) {
        var daysOfData = Math.round((playerData[playerData.length-1].timestamp.getTime() - playerData[0].timestamp.getTime())/ONE_DAY_MILLIS);
        var gacChange = playerData[playerData.length-1].skill_rating - playerData[0].skill_rating;
        gacAveragePerDay = Math.round(gacChange / daysOfData);   
    }

    for (var i = 0; i < playerData.length; i += step) {
        var ts = playerData[i].timestamp;
        var label = monthShortNames[ts.getUTCMonth()] + " " + ts.getUTCDate();
        labels.push(label);
        data.push(playerData[i].skill_rating);
    }

    // earliest on the left
    labels.reverse();
    data.reverse();

    const width = 600;
    const height = 300;
    const chartCallback = (ChartJS) => {
        ChartJS.defaults.global.responsive = true;
        ChartJS.defaults.global.maintainAspectRatio = false;

        ChartJS.plugins.register({
            beforeDraw: function(chartInstance) {
              var ctx = chartInstance.chart.ctx;
              ctx.fillStyle = "black";
              ctx.fillRect(0, 0, chartInstance.chart.width, chartInstance.chart.height);
            }
          });
    };

    var chartTitle = "Average GAC Skill Rating increase per day: " + gacAveragePerDay.toLocaleString('en');

    const configuration = {
        type: 'line',
        data: {
            datasets: [{
                fill: true,
                borderColor: 'rgba(255,255,255, 1)',
                borderWidth: 1,
                backgroundColor: getGradientBackgroundColor,
                cubicInterpolationMode: 'monotone',
                label: "GAC Skill Rating over time for " + playerName + " (" + allycode + ")",
                data: data
            }],
            labels: labels
        },
        options: {
            title: {
                display: true,
                align: 'center',
                position: 'bottom',
                fontColor: 'white',
                text: chartTitle
            },
            scales: {
                yAxes: [{
                    ticks: {
                        fontColor: 'white',
                    }
                }],
                xAxes: xAxesFont,
            },
            legend: legend
        }
    };
    
    const chartJSNodeCanvas = new ChartJSNodeCanvas({width, height, chartCallback: chartCallback});

    const imageBuffer = await chartJSNodeCanvas.renderToBuffer(configuration);    

    var fileName = "chart-gac-" + allycode + "-" + Date.now() + ".png";

    return createImageReportResultObj(imageBuffer, fileName);
}

async function getChartGpOverTimeForAllyCode(allycode, type = null, timeframe = DEFAULT_REPORT_TIMEFRAME_DAYS)
{   
    const currentDate = new Date();
    
    let timeframeText;
    switch (timeframe)
    {
        case DEFAULT_REPORT_TIMEFRAME_DAYS: timeframeText = "90 Days"; break;
        case 180: timeframeText = "6 Months"; break;
        case 365: timeframeText = "1 Year"; break;
    }

    // Now you can use the 'limit' variable in your database query
    var playerData = await database.db.any(`SELECT * FROM player_data WHERE allycode = $1 ORDER BY timestamp DESC LIMIT ${timeframe}`, [allycode]);

    var labels = new Array();
    var gpData = new Array();
    var gpSquadData = new Array();
    var gpFleetData = new Array();
    if (playerData.length == 0)
        throw new Error("No data for allycode " + allycode);

    playerData.sort((a,b) => b.timestamp - a.timestamp);
    let step = Math.min(Math.ceil(timeframe / 30), Math.ceil(playerData.length / 30));

    let player = await comlink.getPlayerArenaProfileByAllycode(allycode);
    // grab their most recent recorded name
    var playerName = player.name;

    var gpAveragePerDay = "Not enough data.";
    var gpSquadAveragePerDay = "Not enough data.";
    var gpFleetAveragePerDay = "Not enough data.";
    
    if (playerData.length > 1) {
        var daysOfData = Math.round((playerData[playerData.length-1].timestamp.getTime() - playerData[0].timestamp.getTime())/ONE_DAY_MILLIS);
        var gpChange = playerData[playerData.length-1].gp - playerData[0].gp;
        gpAveragePerDay = Math.round(gpChange / daysOfData);

        var gpSquadChange = playerData[playerData.length-1].gp_squad - playerData[0].gp_squad;
        gpSquadAveragePerDay = Math.round(gpSquadChange / daysOfData);   

        var gpFleetChange = playerData[playerData.length-1].gp_fleet - playerData[0].gp_fleet;
        gpFleetAveragePerDay = Math.round(gpFleetChange / daysOfData);   
    }

    for (var i = 0; i < playerData.length; i += step) {
        var ts = playerData[i].timestamp;
        var label = monthShortNames[ts.getUTCMonth()] + " " + ts.getUTCDate();
        labels.push(label);
        gpData.push(playerData[i].gp);
        gpSquadData.push(playerData[i].gp_squad);
        gpFleetData.push(playerData[i].gp_fleet);
    }
    
    // Calculate the end year
    const endYear = playerData[0].timestamp.getFullYear();
    const beginYear = playerData[playerData.length-1].timestamp.getFullYear();

    // earliest on the left
    labels.reverse();
    gpData.reverse();
    gpSquadData.reverse();
    gpFleetData.reverse();


    // Create a plugin to display beginYear and endYear labels
    const displayBeginEndYearPlugin = {
        beforeDraw: function (chartInstance) {
            const ctx = chartInstance.chart.ctx;
            const xAxis = chartInstance.scales['x-axis-0'];
            const yAxis = chartInstance.scales['y-axis-0'];
            const beginYearLabel = beginYear;
            const endYearLabel = endYear;

            // Set font styles for the labels
            ctx.font = '14px Arial';
            ctx.fillStyle = 'white';
            ctx.textAlign = 'left';

            // Draw beginYear label below the far left x-axis label
            const beginYearX = xAxis.getPixelForTick(0)-30;
            const beginYearY = yAxis.bottom + 50; // Adjust the vertical position as needed
            ctx.fillText(beginYearLabel, beginYearX, beginYearY);

            // Draw endYear label below the far right x-axis label
            const endYearX = xAxis.getPixelForTick(xAxis.ticks.length - 1);
            const endYearY = yAxis.bottom + 50; // Adjust the vertical position as needed
            ctx.fillText(endYearLabel, endYearX - ctx.measureText(endYearLabel).width, endYearY);
        }
    };


    const width = 600;
    const height = 300;
    const chartCallback = (ChartJS) => {
        ChartJS.defaults.global.responsive = true;
        ChartJS.defaults.global.maintainAspectRatio = false;

        ChartJS.plugins.register({
            beforeDraw: function(chartInstance) {
              var ctx = chartInstance.chart.ctx;
              ctx.fillStyle = "black";
              ctx.fillRect(0, 0, chartInstance.chart.width, chartInstance.chart.height);
            }
          });
        // Add the custom plugin to the chart plugins
        ChartJS.plugins.register(displayBeginEndYearPlugin);
    };
    
    var datasets = new Array();
    if (!type || type.startsWith("t"))
    {
        datasets.push({
            backgroundColor: 'rgba(0, 0, 0, 0)',
            borderColor: 'rgba(54, 162, 235, 0.5)',
            cubicInterpolationMode: 'monotone',
            label: "Total GP",
            data: gpData
        });
    }
    if (!type || type.startsWith("s"))
    {
        datasets.push({
            backgroundColor: 'rgba(0, 0, 0, 0)',
            borderColor: 'rgba(14, 180, 5, 0.5)',
            cubicInterpolationMode: 'monotone',
            label: "Squad GP",
            data: gpSquadData
        });
    }
    if (!type || type.startsWith("f")) 
    {
        datasets.push({
            backgroundColor: 'rgba(0, 0, 0, 0)',
            borderColor: 'rgba(180, 25, 50, 0.5)',
            cubicInterpolationMode: 'monotone',
            label: "Fleet GP",
            data: gpFleetData
        });
    }

    var line1 = "GP over time for " + playerName + " (" + allycode + ")" + " (" + timeframeText + ")";
    if (type && type.startsWith("s")) line1 = "Squad " + line1; 
    if (type && type.startsWith("f")) line1 = "Fleet " + line1; 

    var titleText = [
        line1,
        "Average GP increase per day: " + gpAveragePerDay.toLocaleString('en'),
        "Average Squad GP increase per day: " + gpSquadAveragePerDay.toLocaleString('en'),
        "Average Fleet GP increase per day: " + gpFleetAveragePerDay.toLocaleString('en')
    ];

    const configuration = {
        type: 'line',
        data: {
            datasets: datasets,
            labels: labels
        },
        options: {
            title: {
                display: true,
                align: 'center',
                position: 'bottom',
                text: titleText,
                fontColor: 'white'
            },scales: {
                yAxes: [{
                    ticks: {
                        fontColor: 'white',
                        callback: function (value, index, values) {
                            // Format the Y-axis labels as "X.XXM"
                            let newvalue;
                            if(value >= 1000000){
                                newvalue = (value / 1000000).toFixed(1)+ 'M'
                            }
                            else 
                            {
                                newvalue = (value / 1000)+ 'K'
                            }
                            return  newvalue ;
                        }
                    }
                }],
                xAxes: xAxesFont,
            },
            legend: legend
        }
    };

    const chartJSNodeCanvas = new ChartJSNodeCanvas({width, height, chartCallback: chartCallback});

    const imageBuffer = await chartJSNodeCanvas.renderToBuffer(configuration);    

    var fileName = "chart-gp-" + allycode + "-" + Date.now() + ".png";

    return createImageReportResultObj(imageBuffer, fileName);
}

const TO_MQ_REPORT_DATE = (date) => { let s = date.toISOString(); return s.slice(0, s.indexOf("T")); }

async function getChartModQualityOverTimeForGuild(allycode, type)
{
    if (!type) type = MOD_QUALITY_TYPES.WOOKIEE.name;
    let guildModData = await database.db.multiResult(`
    SELECT guild_name FROM guild WHERE guild_id = (SELECT guild_id FROM guild_players WHERE allycode = $1);

    SELECT t_limited.*
    FROM (
            SELECT DISTINCT allycode
            FROM player_Data
            where allycode in (
                select allycode 
                from guild_players 
                where guild_id in (
                    select guild_id 
                    from guild_players 
                    where allycode = $1
                )
            )
        ) t_groups
        JOIN LATERAL (
            SELECT allycode,player_name,timestamp,mod_quality,mods_six_dot,mods_speed10,mods_speed15,mods_speed20,mods_speed25,mod_quality2,mod_quality_hotbot
            FROM player_data t_all
            WHERE t_all.allycode = t_groups.allycode AND timestamp > NOW() - interval '180 days'
            ORDER BY t_all.allycode,t_all.timestamp desc
        ) t_limited ON true;
    `, [allycode]);

    if (guildModData[0].rows.length == 0)
    {
        return { content: "Player is not in a guild." };
    }

    let csvData = [];

    let modData = guildModData[1].rows;
    let dates = [];

    // catalog unique dates
    for (let row of modData)
    {
        let reportDate = TO_MQ_REPORT_DATE(row.timestamp);
        if (dates.indexOf(reportDate) === -1) dates.push(reportDate);
    }

    dates.sort();

    let obj;
    for (let i = 0; i < modData.length; i++)
    {
        let iData = modData[i];
        if (!obj || obj["Ally Code"] !== iData.allycode)
        {
            obj = {
                "Name": iData.player_name,
                "Ally Code": iData.allycode
            };

            for (let e of dates) obj[e] = null;

            csvData.push(obj);
        }

        let iDataDateString = TO_MQ_REPORT_DATE(iData.timestamp);
        switch (type) {
            case MOD_QUALITY_TYPES.DSR.name:
                obj[iDataDateString] = parseFloat(iData.mod_quality).toFixed(2);
                break;
            case MOD_QUALITY_TYPES.HOT.name:
                obj[iDataDateString] = parseFloat(iData.mod_quality_hotbot).toFixed(2);
                break;
            default:
                obj[iDataDateString] = parseFloat(iData.mod_quality2).toFixed(2);
                break;
        }
        
    }

    let growthPerDay = [];
    for (let c of csvData)
    {
        let earliestDate, earliestMQ, latestDate, latestMQ;

        for (let d of dates)
        {
            if (c[d] != null) 
            { 
                earliestDate = d;
                earliestMQ = c[d];
                break;
            }
        }

        for (let d = dates.length-1; d >= 0; d--)
        {
            if (c[dates[d]] != null)
            {
                latestDate = dates[d];
                latestMQ = c[dates[d]];
                break;
            }
        }

        let gpdString = ((latestMQ - earliestMQ)/(new Date(new Date(latestDate) - new Date(earliestDate)).getTime()/(1000*60*60*24))).toFixed(5);
        let obj = {
            name: c.Name,
            allycode: c["Ally Code"],
            gpdString: isNaN(gpdString) ? "No Data" : gpdString,
            gpd: isNaN(gpdString) ? -99999 : parseFloat(gpdString)
        }

        growthPerDay.push(obj);
    }

    growthPerDay.sort((a, b) => b.gpd - a.gpd);

    let growthChart = 
`Average MQ Growth / Day (180 days)
\`\`\`
Player                  | Growth
----------------------------------
${growthPerDay.map(g => `${discordUtils.fit(g.name, 23)}${discordUtils.LTR} | ${g.gpdString}`).join("\n")}
\`\`\``

    const csvParsingOptions = { defaultValue: 0 }
    csvData.sort((a, b) => { a.Name.localeCompare(b.Name) ?? a["Ally Code"].localeCompare(b["Ally Code"]) });
    let csvText = await parseAsync(csvData, csvParsingOptions);
    let buffer = Buffer.from(csvText);

    let formulaDesc;
    switch (type)
    {
        case MOD_QUALITY_TYPES.DSR.name: formulaDesc = MOD_QUALITY_TYPES.DSR.formulaDesc; break;
        case MOD_QUALITY_TYPES.HOT.name: formulaDesc = MOD_QUALITY_TYPES.HOT.formulaDesc; break;
        default: formulaDesc = MOD_QUALITY_TYPES.WOOKIEE.formulaDesc; break;
    }
    
    let embed = new EmbedBuilder()
        .setTitle(`**${guildModData[0].rows[0].guild_name}** Mod Quality Growth: ${type}`)
        .setColor(0xD2691E)
        .setDescription(
`${growthChart}

Full mod quality data is in the .CSV file attached to this message. You may need to scroll up to download it.

${discordUtils.symbols.note} Data are a snapshot in time on each day. Discrepancies could be due to players remodding when the snapshot was taken. Inspect a player's overall mod quality trend with \`/report mq\`.

${type}: ${formulaDesc}`);

    let csvAttachment = new AttachmentBuilder(buffer, { name: `guild_mq_${type}_${new Date().getTime()}.csv`});
    return { embeds: [embed], files: [csvAttachment] };

}

async function getChartModQualityOverTimeForAllyCode(allycode, type)
{
    if (!type) type = MOD_QUALITY_TYPES.WOOKIEE.name;
    const limit = 90;
    var playerData = await playerUtils.getAllPlayerDataByAllyCode(allycode, limit);


    if (playerData.length == 0)
        throw new Error("No data for allycode " + allycode);

    playerData.sort((a,b) => b.timestamp - a.timestamp);
    let step = Math.min(Math.ceil(limit / 30), Math.ceil(playerData.length / 30));
    
    var labels = new Array();
    var data = new Array();

    let player = await comlink.getPlayerArenaProfileByAllycode(allycode);
    // grab their most recent recorded name
    var playerName = player.name;

    for (var i = 0; i < playerData.length; i += step) {

        let val = 0;
        if (type === MOD_QUALITY_TYPES.DSR.name)
            val = playerData[i].mod_quality;
        else if (type === MOD_QUALITY_TYPES.HOT.name)
            val = playerData[i].mod_quality_hotbot;
        else
            val = playerData[i].mod_quality2;

        if (val != null)
        {
            var ts = playerData[i].timestamp;
            var label = monthShortNames[ts.getUTCMonth()] + " " + ts.getUTCDate();
            labels.push(label);
            data.push(val);
        }
    }

    labels.reverse();
    data.reverse();

    const width = 600;
    const height = 300;
    const chartCallback = (ChartJS) => {
        ChartJS.defaults.global.responsive = true;
        ChartJS.defaults.global.maintainAspectRatio = false;

        ChartJS.plugins.register({
            beforeDraw: function(chartInstance) {
              var ctx = chartInstance.chart.ctx;
              ctx.fillStyle = "black";
              ctx.fillRect(0, 0, chartInstance.chart.width, chartInstance.chart.height);
            }
          });
    };

    var chartTitle = [
        "Mod Quality over time for " + playerName + " (" + allycode + "), "
    ];

    if (type == MOD_QUALITY_TYPES.DSR.name) {
        chartTitle.push(`calculated as ${MOD_QUALITY_TYPES.DSR.formulaDesc}`);
        chartTitle[0] += "DSR Style:"
    } else if (type == MOD_QUALITY_TYPES.HOT.name) {
        chartTitle.push(`calculated as ${MOD_QUALITY_TYPES.HOT.formulaDesc}`)
        chartTitle[0] += "HotBot Style:"
    } else {
       chartTitle.push(`calculated as ${MOD_QUALITY_TYPES.WOOKIEE.formulaDesc}`);
       chartTitle[0] += "Wookiee Style:"
    }

    const configuration = {
        type: 'line',
        data: {
            datasets: [{
                fill: true,
                borderColor: 'rgba(255,255,255, 1)',
                borderWidth: 1,
                backgroundColor: getGradientBackgroundColor,
                cubicInterpolationMode: 'monotone',
                label: "Mod Quality",
                data: data
            }],
            labels: labels
        },
        options: {
            title: {
                display: true,
                align: 'center',
                position: 'bottom',
                fontColor: 'white',
                text: chartTitle
            },scales: {
                yAxes: [{
                    ticks: {
                        fontColor: 'white',
                    }
                }],
                xAxes: xAxesFont,
            },
            legend: legend
        }
    };

    const chartJSNodeCanvas = new ChartJSNodeCanvas({width, height, chartCallback: chartCallback});

    const imageBuffer = await chartJSNodeCanvas.renderToBuffer(configuration);    

    var fileName = "chart-mq-" + allycode + "-" + Date.now() + ".png";

    return createImageReportResultObj(imageBuffer, fileName);
}

async function getChartGuildPayouts(allycode, type = "")
{
    var guildName = null;
    var payoutsData = null;
    var guildData = await database.db.any("select guild_name from guild where guild_id = (select guild_id from guild_players where allycode = $1)", allycode);

    if (guildData && guildData[0]) {
        guildName = guildData[0].guild_name;
        var payoutsData = await database.db.any(
            `select distinct payout_utc_offset_minutes, count(payout_utc_offset_minutes)
            FROM player_data_static WHERE allycode LIKE ANY 
            (SELECT allycode FROM guild_players WHERE guild_id = (SELECT guild_id FROM guild_players WHERE allycode = $1))
            group by payout_utc_offset_minutes
            order by payout_utc_offset_minutes asc`, allycode
        );
    }

    if (payoutsData == null || payoutsData.length == 0)
    {
        
        // guild not registered in db, so load from API
        let guildData;
        try {
            guildData = await guildUtils.getGuildDataFromAPI({ allycode: allycode }, { useCache: true, includeRecentGuildActivityInfo: true });
        }
        catch (err)
        {
            logger.error(formatError(err));
            return discordUtils.createErrorEmbed("Error", "Failed to retrieve guild data for ally code " + allycode + ". " + err.message);
        }
        
        guildName = guildUtils.getGuildName(guildData);
        let playerPayouts = await getPlayerDataForGuild(guildData);

        playerPayouts.sort((a, b) => swapiUtils.getPlayerPayoutUTCOffsetMinutes(a) - swapiUtils.getPlayerPayoutUTCOffsetMinutes(b));
        payoutsData = new Array();
        var lastPayoutData = null;
        for (var p = 0; p < playerPayouts.length; p++)
        {
            var poData = swapiUtils.getPlayerPayoutUTCOffsetMinutes(playerPayouts[p]);
            if (lastPayoutData == null || lastPayoutData.payout_utc_offset_minutes != poData)
            {
                lastPayoutData = { payout_utc_offset_minutes: poData, count: 1 }
                payoutsData.push(lastPayoutData);
            } else {
                lastPayoutData.count++;
            }
        }
    }

    var data = new Array();
    var labels = new Array();
    var backgroundColors = new Array();
    var borderColors = new Array();

    const colorLimits = {
        rMin: 0,
        rMax: 10,
        gMin: 80,
        gMax: 230,
        bMin: 150,
        bMax: 200
    }
    for (var i = 0; i < payoutsData.length; i++)
    {
        data.push(payoutsData[i].count);

        var offsetHours = payoutsData[i].payout_utc_offset_minutes / 60;
        var label = "UTC" + (offsetHours >= 0 ? "+" : "-") + Math.abs(offsetHours);
        labels.push(label);

        var baseColorR = Math.floor((colorLimits.rMax - colorLimits.rMin) / payoutsData.length) * i + colorLimits.rMin;
        var baseColorG = Math.floor((colorLimits.gMax - colorLimits.gMin) / payoutsData.length) * i + colorLimits.gMin;
        var baseColorB = Math.floor((colorLimits.bMax - colorLimits.bMin) / payoutsData.length) * i + colorLimits.bMin;
        backgroundColors.push("rgba(" + baseColorR + "," + baseColorG + "," + baseColorB + ", 0.7)");
        borderColors.push("rgba(" + baseColorR + "," + baseColorG + "," + baseColorB + ", 1)");
        
    }

    const width = 40*data.length;
    const height = 500;
    const chartCallback = (ChartJS) => {
        ChartJS.defaults.global.responsive = true;
        ChartJS.defaults.global.maintainAspectRatio = false;

        ChartJS.plugins.register({
            beforeDraw: function(chartInstance) {
              var ctx = chartInstance.chart.ctx;
              ctx.fillStyle = "black";
              ctx.fillRect(0, 0, chartInstance.chart.width, chartInstance.chart.height);
            },
            afterDraw: function (chartInstance) {
                var ctx = chartInstance.chart.ctx;
                ctx.textBaseline = "middle";
                ctx.fillStyle = 'white'
                ctx.font = "15px Sans";
                ctx.textAlign = "center";

                for (var li = 0; li < data.length; li++)
                {
                    var labelText = data[li];
                    var x = chartInstance.chart.controller.data.datasets[0]._meta[0].data[li]._view.x;
                    var y = Math.min(chartInstance.chart.controller.data.datasets[0]._meta[0].data[li]._view.y + 15, height - 40);
                    ctx.fillText(labelText, x, y);
                }
            }
          });
    };

    const configuration = {
        type: 'bar',
        data: {
            labels: labels,
            datasets: [{

                label: 'Count',
                data: data,
                backgroundColor: backgroundColors,
                borderColor: borderColors,
                borderWidth: 1,
                fontColor: 'white',
            }]
        },
        options: {
            title: {
                display: true,
                align: 'center',
                position: 'bottom',
                fontColor: 'white',
                text: "Payouts for " + guildName + ", grouped by time (UTC)"
            },
            scales: {
                yAxes: [{
                    ticks: {
                        fontColor: 'white',
                        beginAtZero: true,
                    }
                }],
                xAxes: xAxesFont,
            },
            legend: legend
        }
    };

    const chartJSNodeCanvas = new ChartJSNodeCanvas({width, height, chartCallback: chartCallback});

    const imageBuffer = await chartJSNodeCanvas.renderToBuffer(configuration);    

    var fileName = "chart-guild-payouts-" + allycode + "-" + Date.now() + ".png";

    return createImageReportResultObj(imageBuffer, fileName);
}