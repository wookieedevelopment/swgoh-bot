
const { SlashCommandSubcommandBuilder } = require("@discordjs/builders");
const { EmbedBuilder, ActionRowBuilder, StringSelectMenuBuilder } = require("discord.js");
const comlink = require("../../utils/comlink.js");
const guildUtils = require("../../utils/guild.js");
const cheatingGuild = require("./guild");

const MAX_GUILDS_TO_FIND = 25;
module.exports = {
    data: new SlashCommandSubcommandBuilder()
        .setName("search")
        .setDescription("Search for guilds by name.")
        .addStringOption(o =>
            o.setName("text")
            .setDescription("Text to search for in guild names")
            .setRequired(true)
            .setMaxLength(30)
            .setMinLength(1)),
    interact: async function(interaction)
    {
        let searchString = interaction.options.getString("text");

        let guilds = await comlink.searchGuilds(searchString, 0, MAX_GUILDS_TO_FIND);

        let embed = new EmbedBuilder()
            .setTitle(`Guilds matching "${searchString}"`)
            .setColor(0x555500);
        
        if (!guilds || guilds.guild.length === 0)
        {
            embed.setDescription("No guilds found.")
                .setColor(0x550000);
            await interaction.followUp({ embeds: [embed] });
            return;
        }

        guilds.guild.sort((a, b) => {
            if (a.memberCount != b.memberCount) return b.memberCount - a.memberCount;
            return parseInt(b.guildGalacticPower) - parseInt(a.guildGalacticPower);
        });
        
        let matchingGuilds = guilds.guild.map(g => { return {
            label: g.name,
            value: g.id,
            description: `${g.memberCount} member${g.memberCount === 1 ? "" : "s"}. GP: ${parseInt(g.guildGalacticPower).toLocaleString("en-US")}.`
        }});

        if (matchingGuilds.length === MAX_GUILDS_TO_FIND)
            embed.setDescription(`Found at least ${MAX_GUILDS_TO_FIND} guilds. Displaying only the first ${MAX_GUILDS_TO_FIND}. To see others, please provide a more precise name.`);
        else
            embed.setDescription(`Found ${matchingGuilds.length} guilds matching the search text.`);
        
        let componentsRow = new ActionRowBuilder()
            .addComponents(
                new StringSelectMenuBuilder()
                    .setCustomId(`cheating.search.guild`)
                    .setPlaceholder("Run a cheat check on a guild")
                    .addOptions(matchingGuilds)
            );

        let response = {
            embeds: [embed],
            components: [componentsRow]
        }

        await interaction.followUp(response);
    },
    onSelect: async function(interaction)
    {
        //let originalMessage = interaction.message.content;
        await interaction.deferReply();

        let guildId = interaction.values[0];

        let guild = await guildUtils.getGuildDataFromAPI({ comlinkGuildId: guildId }, { useCache: true, cacheTimeHours: 4, includeRecentGuildActivityInfo: true });

        let statusInteraction = await interaction.followUp(`Checking ${guildUtils.getGuildRoster(guild).length} players in guild **${guildUtils.getGuildName(guild)}**. Please wait...`);

        let response = await cheatingGuild.checkGuild(
            guild,
            async (statusMessage) => { await statusInteraction.edit(statusMessage); });

        await statusInteraction.edit(response);
    }
}