const jp = require('jsonpath');
const { EmbedBuilder } = require("discord.js");
const { logger, formatError } = require('../../utils/log');
const swapiUtils = require("../../utils/swapi")
const database = require ("../../database")
const discordUtils = require ("../../utils/discord")
const playerUtils = require ("../../utils/player")
const constants = require ("../../utils/constants")
const cheating_data = require("../../data/cheating_data.json");

module.exports = {
    criticalThreshold: 60,
    warningThreshold: 20,
    likelihoodSymbol: function(likelihood)
    {
        if (likelihood >= this.criticalThreshold) {
            return ":rotating_light:";
        } else if (likelihood >= this.warningThreshold) {
            return ":thinking:";
        } else
        {
            return ":white_check_mark:";
        }
    },
    checkOnePlayer: async function(allycode, detailed) {
    
        var player;
        await database.db.one("INSERT INTO cheating_queries(allycode, timestamp) VALUES($1, $2) RETURNING *",
                        [allycode, new Date()]);

        let now = new Date();

        let cachedPlayerData = await database.db.any(`
            SELECT data_text 
            FROM player_cache
            LEFT JOIN cache_data USING (cache_data_id)
            WHERE type = $1 AND allycode = $2 AND player_cache.timestamp > (now() - INTERVAL '1 DAY') 
            LIMIT 1`,
            [constants.CacheTypes.QUERY, allycode])
    
        if (cachedPlayerData && cachedPlayerData.length > 0)
        {
            player = JSON.parse(cachedPlayerData[0].data_text);
        }
        else {

            try {
                player = await swapiUtils.getPlayer(allycode);
            } 
            catch (error)
            {
                return discordUtils.createErrorEmbed("Huac ooac. ('Uh oh.').", error.toString());
            }
        }

        var playerName = swapiUtils.getPlayerName(player);
    
        var embedToSend = new EmbedBuilder()
            .setTitle(`Cheat check results for ${playerName}${discordUtils.LTR} (${allycode}):`)
            .setColor(0xD2691E);
        
        for (var checkCharDisplayName in cheating_data)
        {
            var checkChar = cheating_data[checkCharDisplayName].character_name;
            var charResultsString = "";
            var fv = { name: null, value: null, inline: false };
    
            let unit = swapiUtils.getPlayerRoster(player).find(u => u.name === checkChar.replace("'", "\\'"));
    
            fv.name = "__**" + checkCharDisplayName + "**__";
    
            if (!unit)
            {
                charResultsString = "Not activated.";
                fv.name = ":zero: " + fv.name;
            } 
            else if (swapiUtils.getUnitRarity(unit) < cheating_data[checkCharDisplayName].min_stars_to_check)
            {
                charResultsString = "Only checking units at " + cheating_data[checkCharDisplayName].min_stars_to_check + "★";
                fv.name = ":no_mouth: " + fv.name + " (" + swapiUtils.getUnitRarity(unit) + "★)";
            }
            else
            {
                let results = await this.checkRequirements(player, cheating_data[checkCharDisplayName], await swapiUtils.getUnitsList());
    
                fv.name = (this.likelihoodSymbol(results.likelihood) + " " + fv.name + " (" + swapiUtils.getUnitRarity(unit) + "★)").trim();
    
                var likelihoodString = results.likelihood + "% chance that " + playerName + " cheated.";
                
                charResultsString += likelihoodString;
                if (detailed || results.likelihood >= this.warningThreshold)
                {
                    charResultsString += "```";
                    for (var charIndex = 0; charIndex < results.characters.length; charIndex++)
                    {
                        var character = results.characters[charIndex];
                        charResultsString += character.characterName + ", " + 
                                            character.stars + "★, " + 
                                            "G" + character.gear + ", " + 
                                            character.zetas + " zetas\r\n";
                    }
                    charResultsString += "```";
                }
    
            }
    
            fv.value = charResultsString;
            embedToSend.addFields([ { name: fv.name, value: fv.value, inline: fv.inline } ]);
        }
        
        embedToSend.addFields([ { name: `GAC History`, value: `https://swgoh.gg/p/${allycode}/gac-history/` }])
    
        return embedToSend;
    },
    checkRequirements: async function(player, characterObj, unitsList)
    {
        var numCharacters = characterObj.requirements.num_characters;

        var category = characterObj.requirements.category;
        var category_units = [];

        if (category)
        {
            category_units = unitsList.filter((u) => 
                u.categories.includes(category) &&
                !characterObj.requirements.characters.find((c) => c.name == u.name) &&
                swapiUtils.getUnitCombatType(u) == 1
                );
        }

        var resultsSet = [];
        
        for (var charIndex in characterObj.requirements.characters)
        {
            var characterRequirement = characterObj.requirements.characters[charIndex];

            var resultObj = await this.checkCharacter(player, characterRequirement);

            if (resultObj != null && resultObj.character.stars >= 7) 
                resultsSet.push(resultObj);
        }

        for (var fuix = 0; fuix < category_units.length; fuix++)
        {
            var characterRequirement = characterObj.requirements.faction_characters_default;

            var resultObj = await this.checkCharacter(player, characterRequirement, category_units[fuix].name);

            if (resultObj != null && resultObj.character.stars >= 7) 
                resultsSet.push(resultObj);
        }

        // sort in numeric order
        resultsSet = resultsSet.sort((a, b) => b.impact - a.impact);

        
        var results = {
            characters: [],
            likelihood: 100
        };
        
        // only look at the first numCharacters impacts on likelihood, since those are the characters that are most
        // likely to have been used in the event
        for (var ix = 0; ix < numCharacters && ix < resultsSet.length; ix++)
        {
            results.likelihood -= resultsSet[ix].impact;
            results.characters.push(resultsSet[ix].character);
        }

        // can't be less than a 0% chance or above 100%
        results.likelihood = Math.min(Math.max(0, results.likelihood), 100);

        //console.log(results);

        return results;
    },
    checkCharacter: async function(player, characterRequirement, characterName)
    {
        if (!characterName)
            characterName = characterRequirement.name;

        var resultObj = {
            impact: 0,
            character: {
                characterName: characterName,
                stars: 0,
                gear: 0,
                zetas: 0
            }
        };

        let unit = swapiUtils.getPlayerRoster(player).find(u => u.name === characterName.replace("'", "\\'"));

        if (unit) 
        {

            if (swapiUtils.getUnitRarity(unit) < 7)
                return null;

            resultObj.character.stars = swapiUtils.getUnitRarity(unit);
            resultObj.character.gear = swapiUtils.getUnitGearLevel(unit);
            
            // find out how many zetas the player put on the character
            resultObj.character.zetas = await swapiUtils.getUnitZetaCount(unit);
            
            resultObj.impact = characterRequirement.gear_scale[resultObj.character.gear] 
                + characterRequirement.zeta_scale[resultObj.character.zetas];

        }

        return resultObj;
    },
    
    createErrorEmbed: function(title, errorMessage) {
        errorEmbed = new EmbedBuilder().setTitle(title).setColor(0xaa0000).setDescription(errorMessage);
        return errorEmbed;
    }
};