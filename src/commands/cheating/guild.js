const { EmbedBuilder, ButtonBuilder, ButtonStyle } = require("discord.js");
const discordUtils = require("../../utils/discord.js");
const database = require("../../database.js");
const cheatingUtils = require("./utils.js");
const guildUtils = require("../../utils/guild.js");
const swapi = require("../../utils/swapi.js");
const cheating_data = require("../../data/cheating_data.json");
const { logger, formatError } = require("../../utils/log.js");
const { ActionRowBuilder, StringSelectMenuBuilder } = require("discord.js");

module.exports = {
    name: "cheating.guild",
    description: "Check all players in a guild for potential cheating.",
    interact: async function(interaction, allycode)
    {
        let guild, guildAllyCodes;
        try {
            guild = await guildUtils.getGuildDataFromAPI({ allycode: allycode }, { useCache: true, cacheTimeHours: 8, includeRecentGuildActivityInfo: true });
        } 
        catch (err)
        {
            let embed = discordUtils.createErrorEmbed("Error", "Failed to retrieve guild data for " + allycode);
            await interaction.editReply({ embeds: [embed]});
            logger.error(formatError(err))
            return;
        }

        let guildName = guildUtils.getGuildName(guild);

        await interaction.editReply("Found guild **" + guildName + "**.  Checking " + guildUtils.getGuildRoster(guild).length + " players, please wait...");

        let replyObject = await this.checkGuild(guild, async (statusMessage) => { await interaction.editReply(statusMessage); });

        await interaction.followUp(replyObject);
    },
    execute: async function(input, args)
    {
        await input.reply("Please use the slash command.  **/cheating guild**");
        return;
    },
    checkGuild: async function(guild, updateStatusCallback)
    {
        let replyObject = 
        { 
            embeds: []
        };

        var foundCheater = false;
        var embedDescription = "Here are the players that have a high likelihood of cheating at events:\n";
        embedDescription += "```";   

        let { players, errors } = await swapi.getPlayers({
            allycodes: guildUtils.getGuildAllycodesForAPIGuild(guild), 
            useCache: true, 
            cacheTimeHours: 24, 
            updateStatusCallback: async (allycodes, players, errors) => { 
                await updateStatusCallback(`Retrieved ${players.length}/${allycodes.length} rosters for **${guildUtils.getGuildName(guild)}**. Errors: ${errors.length}`); 
            }
        });

        if (errors && errors.length > 0)
        {
            let errorEmbed = discordUtils.createErrorEmbed("Error", "Failed to retrieve player data for " + guildUtils.getGuildName(guild) + "\n\nErrors:");
            replyObject.embeds.push(errorEmbed);
            for (var i = 0; i < errors.length; i++)
            {
                errorEmbed.description += "\n" + errors[i];
            }
        }
        
        let problemPlayers = new Array();

        for (var playerIndex = 0; playerIndex < players.length; playerIndex++)
        {
            var player = players[playerIndex];

            for (var checkCharDisplayName in cheating_data)
            {
                var checkChar = cheating_data[checkCharDisplayName].character_name;
                
                // confirm they have the character activated
                let rareUnit = swapi.getPlayerRoster(player).find(u => u.name === checkChar.replace("'", "\\'"));
                if (!rareUnit || swapi.getUnitRarity(rareUnit) != 7) continue;

                let results = await cheatingUtils.checkRequirements(player, cheating_data[checkCharDisplayName], await swapi.getUnitsList());

                // something suspicious with this player.  Add to the list and move to the next player
                // otherwise, check the next character
                if (results.likelihood >= cheatingUtils.criticalThreshold)
                {
                    foundCheater = true;
                    let text = `${swapi.getPlayerName(player)}${discordUtils.LTR} (${swapi.getPlayerAllycode(player)})`;
                    embedDescription += `${text}\n`

                    problemPlayers.push({ label: text, value: swapi.getPlayerAllycode(player).toString() });
                    break;
                }
            }
        }

        let date = new Date();

        let cheatingQueriesInsertValues = players.map(p => { return { allycode: swapi.getPlayerAllycode(p), timestamp: date } });
        const cheatingQueriesInsert = database.pgp.helpers.insert(cheatingQueriesInsertValues, database.columnSets.cheatingQueriesCS);
        await database.db.none(cheatingQueriesInsert);


        if (foundCheater)
            embedDescription += "```";
        else {
            embedDescription = "No players in **" + guildUtils.getGuildName(guild) + "** are likely to have cheated at checked events based on current roster.";
        }



        replyObject.embeds.push(new EmbedBuilder()
            .setTitle("Event cheat check results for guild **" + guildUtils.getGuildName(guild) + "**:")
            .setDescription(embedDescription)
            .setColor(0xD2691E));


        let raidCheckEmbed = getRaidCheckEmbed(guild, players);
        replyObject.embeds.push(raidCheckEmbed);

        if (guildUtils.getGuildRoster(guild).length === 0) return replyObject;

        const buttonRow = new ActionRowBuilder()
            .addComponents(
                new ButtonBuilder()
                    .setCustomId(guildUtils.generateGuildMembersSortCustomId(`guild:search:mem`, swapi.getGuildId(guild), guildUtils.GUILD_MEMBERS_SORT_OPTIONS.Rank, false))
                    .setLabel("Show All Guild Members")
                    .setStyle(ButtonStyle.Primary)
            )
            replyObject.components = [buttonRow];
        
        if (problemPlayers.length > 0)
        {
            const selectRow = new ActionRowBuilder()
                    .addComponents(
                        new StringSelectMenuBuilder()
                            .setCustomId(`cheating.guild`)
                            .setPlaceholder("View details for a player:")
                            .addOptions(problemPlayers)
                    );

            replyObject.components.unshift(selectRow);    
        }
        
        return replyObject;
    },
    onSelect: async function(interaction)
    {
        //let originalMessage = interaction.message.content;
        await interaction.deferReply();

        let allycode = interaction.values[0];
        let embed = await cheatingUtils.checkOnePlayer(allycode, false);

        await interaction.editReply({ embeds: [embed] });
    },
    handleButton: async function(interaction)
    {
        if (interaction.customId.startsWith("cheating.guild.show2")) await interaction.deferUpdate();
        else await interaction.deferReply();

        let { prefix, guildId, by, asc } = guildUtils.parseGuildMembersSortCustomId(interaction.customId);

        let response = await guildUtils.createGuildMembersResponse(guildId, "cheating.guild.show2", by, asc);

        let overviewRow = new ActionRowBuilder()
            .addComponents(
                new ButtonBuilder()
                    .setLabel("Overview")
                    .setStyle(ButtonStyle.Secondary)
                    .setCustomId(`guild:search:view:${guildId}`)
            );

        response.components.push(overviewRow);
            
        await interaction.editReply(response);
    }
//         let customId = interaction.customId;

//         let regex = /cheating.guild.show:(.+)/

//         let match = regex.exec(customId);

//         let guildId = match[1];

//         let guild = await guildUtils.getGuildDataFromAPI({ comlinkGuildId: guildId }, { useCache: true, cacheTimeHours: 24, includeRecentGuildActivityInfo: true });

//         let embed = new EmbedBuilder()
//             .setTitle(`Guild Membership for **${guildUtils.getGuildName(guild)}**`)
//             .setColor("Blurple");

//         let roster = guildUtils.getGuildRoster(guild).sort((a, b) => 
//         {
//             return swapi.getPlayerGuildMemberLevel(b) - swapi.getPlayerGuildMemberLevel(a) ||
//                 swapi.getPlayerName(a).localeCompare(swapi.getPlayerName(b));
//         });
//         let memberText = roster.map(m => {
//             let name = swapi.getPlayerName(m);
//             let ac = swapi.getPlayerAllycode(m);
//             let role;

//             switch (swapi.getPlayerGuildMemberLevel(m))
//             {
//                 case 4: role = "Leader"; break;
//                 case 3: role = "Officer"; break;
//                 default: role = "Member"; break;
//             }

//             return `${discordUtils.fit(name, 21)}${discordUtils.LTR} | ${ac} | ${role}`
//         }).join("\n");


//         let swgohggUrl = `https://swgoh.gg/g/${guildId}`;            
//         let desc =
// `\`\`\`
// Name                  | Ally Code | Role
// -------------------------------------------
// ${memberText}
// \`\`\`
// ${swgohggUrl}
// Note: If the above link does not work, the guild has not been registered on swgoh.gg.
// `;

//         embed.setDescription(desc);
//         embed.setURL(swgohggUrl);

//         await interaction.followUp({ embeds: [embed] });
//     }
}

const RAIDS = [
    { id: "rancor", text: "Rancor", runCheck: null },
    { id: "aat", text: "AAT", runCheck: null },
    { id: "sith_raid", text: "Sith", runCheck: sithRaidCheck },
    { id: "rancor_challenge", text: "Rancor (Challenge)", runCheck: rancorChallengeRaidCheck }
];

function getRaidCheckEmbed(guild, players)
{
    let embed = new EmbedBuilder()
        .setTitle(`Raid cheat check results for guild **${guildUtils.getGuildName(guild)}**:`)
        .setColor(0x000284);

    let fields = [];

    if (!guild.recentRaidResult || guild.recentRaidResult.length === 0)
    {
        embed.setDescription("No recent raids.")
        return embed;
    }

    for (let raid of RAIDS)
    {
        if (!raid.runCheck) continue;
        let raidData = guild.recentRaidResult.find(r => r.raidId.toLowerCase().localeCompare(raid.id) === 0);
        let joinDuration = parseInt(guild.profile.raidLaunchConfig.find(r => r.raidId.toLowerCase().localeCompare(raid.id) === 0)?.joinPeriodDuration ?? 0);

        let field = {
            name: `${raid.text}`,
            value: "```"
        };

        if (!raidData)
        {
            field.value += "\nNot run.\n```";
            fields.push(field);
            continue;
        }

        let tierText = getTierTextForRaid(raidData);
        let durationHours = (parseInt(raidData.duration) - joinDuration) / 3600; // hours
        let ceilDurationHours = Math.ceil(durationHours);
        let pluralHours = ceilDurationHours != 1;

        field.name += `${tierText ? ` - ${tierText} (${ceilDurationHours.toLocaleString('en')} hr${pluralHours ? "s" : ""})` : ""}`;

        let cheatingPlayers = raid.runCheck(raidData, durationHours, players);

        if (cheatingPlayers.length > 0)
        {
            for (let cheater of cheatingPlayers)
            {
                field.value += `\n${cheater.name}${discordUtils.LTR} (${cheater.allycode}) - ${cheater.score.toLocaleString('en')} pts\n${cheater.reason}\n`
            }
        } else {
            field.value += "\nNo obvious cheaters.\n"
        }

        field.value += "```";
        fields.push(field);
    }

    embed.addFields(fields);

    return embed;
}

function rancorRaidCheck(raidResult, durationHours, players)
{

}

function aatRaidCheck(raidResult, durationHours, players)
{

}

const SITH_TIER_THRESHOLDS = {
    "DIFF05": { rarity: 5, gear: 10, scorePerDay: 1000000 },
    "DIFF06": { rarity: 6, gear: 11, scorePerDay: 1000000 },
    "HEROIC85": { rarity: 7, gear: 13, score: 2000000 }
}

function sithRaidCheck(raidResult, durationHours, players)
{
    let thresholds = SITH_TIER_THRESHOLDS[raidResult.identifier.campaignMissionId];
    let potentialCheaters = [];
    
    if (!thresholds) return potentialCheaters;

    let raidDurationDays = Math.ceil(durationHours / 24); // days

    for (let rr of raidResult.raidMember)
    {
        if (rr.memberProgress == 0) continue;

        let p = players.find(p => p.playerId === rr.playerId);

        if (!p) continue; // alternatively, retrieve the player

        let playerEligibleUnits = swapi.getPlayerRoster(p)?.filter(u => swapi.getUnitRarity(u) >= thresholds.rarity);
        if (!playerEligibleUnits || playerEligibleUnits.length == 0) continue;

        let playerReasonableUnits = playerEligibleUnits.filter(u => swapi.getUnitGearLevel(u) >= thresholds.gear);

        if (playerReasonableUnits.length >= 5) continue;

        let score = parseInt(rr.memberProgress);

        if (thresholds.score && score > thresholds.score)
        {
            let plural = playerReasonableUnits.length != 1;
            let obj = createPotentialCheaterObject(p, score, `${playerReasonableUnits.length} G${thresholds.gear}+ ${thresholds.rarity}* unit${plural ? "s" : ""}`);
            potentialCheaters.push(obj);
        }
        else if (thresholds.scorePerDay && score/raidDurationDays > thresholds.scorePerDay)
        {
            let plural = playerReasonableUnits.length != 1;
            let obj = createPotentialCheaterObject(p, score, `Avg. ${Math.round(score/raidDurationDays).toLocaleString('en')} points per day with ${playerReasonableUnits.length} G${thresholds.gear}+ ${thresholds.rarity}* unit${plural ? "s" : ""}`);
            potentialCheaters.push(obj);
        }
    }

    return potentialCheaters;
}

const CHALLENGE_RANCOR_TEAMS_AND_SCORES = [
    {
        units: ["JEDIMASTERKENOBI", "COMMANDERAHSOKA", "C3POLEGENDARY"],
        score: 42000000,
        desc: "JMK CAT C3PO for possible P1 solo"
    }
]

function rancorChallengeRaidCheck(raidResult, durationHours, players)
{
    let potentialCheaters = [];
    for (let rr of raidResult.raidMember)
    {
        if (rr.memberProgress == 0) continue;
        let score = parseInt(rr.memberProgress);

        let p = players.find(p => p.playerId === rr.playerId);

        if (!p) continue; // alternatively, retrieve the player

        if (score > 100000000) {
            let obj = createPotentialCheaterObject(p, score, `Score is unreasonably high given current best teams.`);
            potentialCheaters.push(obj);
            continue;
        }

        let playerEligibleUnits = swapi.getPlayerRoster(p)?.filter(u => swapi.getUnitGearLevel(u) >= 13 && swapi.getUnitRelicLevel(u) - 2 >= 5);
        if (!playerEligibleUnits || playerEligibleUnits.length == 0) continue;

        if (playerEligibleUnits.length < 5)
        { 
            if (score <= 500000) continue;

            let plural = playerEligibleUnits.length != 1;
            let obj = createPotentialCheaterObject(p, score, `${playerEligibleUnits.length} R5+ unit${plural ? "s" : ""}`);
            potentialCheaters.push(obj);
            continue;
        }

        // estimate how many viable teams they have
        let estimatedTeams = Math.floor(playerEligibleUnits.length / 5);
        let reasonablePoints = 0;
        let messageAddendum = "";

        for (let presetTeam of CHALLENGE_RANCOR_TEAMS_AND_SCORES)
        {
            let hasTeam = playerEligibleUnits.filter(u => presetTeam.units.indexOf(swapi.getUnitDefId(u)) >= 0).length === presetTeam.units.length;
            if (hasTeam)
            {
                estimatedTeams--;
                reasonablePoints += presetTeam.score;
                messageAddendum += `\nHas ${presetTeam.desc}.`
            }
            else
            {
                messageAddendum += `\nDoes not have ${presetTeam.desc}.`
            }
        }

        reasonablePoints += estimatedTeams * 10000000;

        if (score > reasonablePoints)
        {
            let obj = createPotentialCheaterObject(p, score, 
                `Only has ${playerEligibleUnits.length} eligible units.${messageAddendum}\nPoints scored seems too high.`)
            potentialCheaters.push(obj);
            continue;
        }
    }

    return potentialCheaters;
}

function createPotentialCheaterObject(player, score, reason)
{
    return {
        name: swapi.getPlayerName(player),
        allycode: swapi.getPlayerAllycode(player),
        score: score,
        reason: reason
    };
}

function getTierTextForRaid(raidData)
{
    if (!raidData) return null;
    if (raidData.identifier.campaignMissionId.startsWith("HEROIC")) return "Heroic";
    if (raidData.identifier.campaignMissionId.startsWith("DIFF")) 
    {
        let tier = raidData.identifier.campaignMissionId.slice(5);
        return `Tier ${tier}`;
    }

    return raidData.identifier.campaignMissionId;
}