const database = require("../database");
const discordUtils = require("../utils/discord.js")
const twDeleteTeam = require("./tw/deleteTeam.js");
const twAddTeam = require("./tw/addTeam.js");
const twListTeams = require("./tw/listTeams.js");
const twSetDescription = require("./tw/setDescription");
const twExclude = require("./tw/exclude")
const twInclude = require("./tw/include")
const twTestTeam = require("./tw/testTeam")
const twManagerInstall = require("./tw/twManager/install");
const twManagerReset = require("./tw/twManager/reset")
const twManagerRepair = require("./tw/twManager/repair")
const twManagerTrack = require("./tw/twManager/track")
const twManagerWhoHas = require("./tw/twManager/whohas")
const twManagerNotify = require("./tw/twManager/notify")
const twManagerHelp = require("./tw/twManager/help")
const twManagerHistory = require("./tw/twManager/history")
const twTeam = require("./tw/team")
const { SlashCommandBuilder } = require('@discordjs/builders');

module.exports = {
    data: new SlashCommandBuilder()
        .setName("twmanager")
        .setDescription("TW Manager")
        .addSubcommand(twManagerNotify.data)
        .addSubcommand(twManagerWhoHas.data)
        .addSubcommand(twManagerHistory.data)
        .addSubcommand(twManagerHelp.data)
        .addSubcommand(twManagerInstall.data)
        .addSubcommand(twManagerRepair.data)
        .addSubcommand(twManagerReset.data)
        .addSubcommandGroup(twManagerTrack.data)
        .addSubcommandGroup(twTeam.data)
        .addSubcommand(twExclude.data)
        .addSubcommand(twInclude.data)
        .addSubcommand(twTestTeam.data)
        ,
    name: "twm",
    aliases: ["twm"],
    description: "TW Manager",
    shouldDefer: function(interaction)
    {
        try {
            if (interaction.options.getSubcommand() == "notify") return false;
        } catch {}

        return true;
    },
    interact: async function(interaction)
    {
        let group, subcommand;
        try { group = interaction.options.getSubcommandGroup(); } catch {}
        try { subcommand = interaction.options.getSubcommand(); } catch {}
        
        if (group == twManagerTrack.data.name)
        {
            switch (subcommand)
            {
                case "start":
                    await twManagerTrack.trackInteraction(interaction);
                    break;
                case "stop":
                    await twManagerTrack.untrackInteraction(interaction);
                    break;
                case "list":
                    await twManagerTrack.listInteraction(interaction);
                    break;
            }
            return;
        }

        if (group == twTeam.data.name)
        {
            await twTeam.interact(interaction, false);
            return;
        }
        
        switch (subcommand)
        {
            case twManagerNotify.data.name:
                await twManagerNotify.interact(interaction);
                break;
            case twManagerWhoHas.data.name:
                await twManagerWhoHas.interact(interaction);
                break;
            case twManagerHistory.data.name:
                await twManagerHistory.interact(interaction);
                break;
            case twManagerInstall.data.name:
                await twManagerInstall.interact(interaction);
                break;
            case twManagerHelp.data.name:
                await twManagerHelp.interact(interaction);
                break;
            case twManagerRepair.data.name:
                await twManagerRepair.interact(interaction);
                break;
            case twManagerReset.data.name:
                await twManagerReset.interact(interaction);
                break;
            case twInclude.data.name:
                await twInclude.interact(interaction);
                break;
            case twExclude.data.name:
                await twExclude.interact(interaction);
                break;
            case twTestTeam.data.name:
                await twTestTeam.interact(interaction, false);
                break;
            default:
                await interaction.editReply("That is not a valid command. Huac ooac. ('Uh oh.').")
                break;
        }
    },
    handleModal: async function(interaction)
    {
        let subcommand = interaction.customId.slice("twmanager:".length);

        switch (subcommand)
        {
            case twManagerNotify.data.name:
                await twManagerNotify.handleModal(interaction);
                break;
        }
    },
    autocomplete: async function(interaction)
    {
        let group, subcommand;
        try { group = interaction.options.getSubcommandGroup(); } catch {}

        if (group)
        {
            switch (group)
            {
                case twManagerTrack.data.name:
                    await twManagerTrack.autocomplete(interaction);
                    break;
                case twTeam.data.name:
                    await twTeam.autocomplete(interaction, false);
                    break;
            }
            return;
        }

        try { subcommand = interaction.options.getSubcommand(); } catch {}

        switch (subcommand)
        {
            case twManagerWhoHas.data.name:
                await twManagerWhoHas.autocomplete(interaction);
                break;
            case twManagerNotify.data.name:
                await twManagerNotify.autocomplete(interaction);
                break;
            case twInclude.data.name:
                await twInclude.autocomplete(interaction);
                break;
            case twExclude.data.name:
                await twExclude.autocomplete(interaction);
                break;
            case twTestTeam.data.name:
                await twTestTeam.autocomplete(interaction, false);
                break;
        }
    }
}