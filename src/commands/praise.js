const got = require("got");
const discordUtils = require("../utils/discord");
const compliments = require("../data/compliments.json");
const { SlashCommandBuilder } = require('@discordjs/builders');

module.exports = {
    data: new SlashCommandBuilder()
        .setName("praise")
        .setDescription("Give praise to someone.")
        .addUserOption(o => 
            o.setName("user")
            .setDescription("A discord user to praise.")
            .setRequired(true)),
    interact: async function(interaction) {
        
        let compliment = compliments[Math.floor(Math.random()*compliments.length)];
        let user = interaction.options.getUser("user");
        await interaction.editReply(discordUtils.makeIdTaggable(user.id) + ": " + compliment);
        // await interaction.channel.send(discordUtils.makeIdTaggable(user.id) + ": " + compliment);
        // await interaction.editReply("Sent! (Only you can see this.)");
    }
}