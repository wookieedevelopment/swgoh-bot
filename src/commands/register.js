const discordUtils = require("../utils/discord");
const playerUtils = require("../utils/player");
const database = require("../database");
const guildUtils = require("../utils/guild");
const { SlashCommandBuilder } = require("@discordjs/builders");
const { logger, formatError } = require("../utils/log.js");
const swapi = require("../utils/swapi");
const { ActionRowBuilder, ButtonBuilder, ButtonStyle, EmbedBuilder, AttachmentBuilder } = require('discord.js');
const { createCanvas, loadImage } = require("canvas");
const guildRefreshRoster = require("./guild/refresh/refreshRoster.js");

// Function to randomly select an unlocked portrait different from the selected portrait
function getRandomUnlockedPortraitId(playerData) {
  const currentPortraitId = swapi.getPlayerSelectedPortrait(playerData);
  const unlockedPortraits = swapi.getPlayerUnlockedPortraits(playerData);
  const filteredPortraits = unlockedPortraits.filter(
    (portraitId) => portraitId !== currentPortraitId
  );

  const randomPortraitIx = Math.floor(Math.random() * filteredPortraits.length);
  return filteredPortraits[randomPortraitIx];
}

// Function to draw the image from URL onto canvas
async function drawPortraitImageToCanvas(imageBuffer) {
  const canvas = createCanvas(60, 60);
  const ctx = canvas.getContext('2d');

  let imageToDraw = await loadImage(imageBuffer);

  // Draw the image on the canvas at position (x: 0, y: 0) with width: 60 and height: 60
  ctx.drawImage(imageToDraw, 0, 0, 60, 60);

  return canvas;
}

const BUTTONS = {
  VERIFY: new ButtonBuilder()
    .setCustomId('verify')
    .setLabel('Verify')
    .setStyle(ButtonStyle.Success),
  CANCEL: new ButtonBuilder()
    .setCustomId('cancel')
    .setLabel('Cancel')
    .setStyle(ButtonStyle.Secondary)
    .setEmoji('❌')
    .setDisabled(false),
  UNREGISTER: function(allycode) {
    return new ButtonBuilder()
    .setCustomId('unregister')
    .setLabel(`Unregister ${discordUtils.addDashesToAllycode(allycode)}`)
    .setStyle(ButtonStyle.Danger)
  }

}

function createRegistrationEmbed(description, imageFileName)
{
  let embed = new EmbedBuilder()
    .setTitle(`WookieeBot Registration`)
    .setDescription(description)
    .setColor(0x0099FF);
  if (imageFileName) embed.setImage("attachment://" + imageFileName);

  return embed;
}

const EMBEDS = {
  ALREADY_REGISTERED: function (allycode)
  {
    return createRegistrationEmbed(`Ally code ${discordUtils.addDashesToAllycode(allycode)} is already registered to you. Would you like to unregister?`);
  },
  TIMEOUT: createRegistrationEmbed('No action received. Cancelled.'),
  CANCEL: createRegistrationEmbed('Cancelled.'),
  VERIFY_WAIT: createRegistrationEmbed('Verifying...'),
  VERIFY_FAILURE: createRegistrationEmbed('Portrait does not match. Please double-check allycode and try again.'),
  ERROR: function(message)
  {
    return createRegistrationEmbed(`${message ?? "An error occurred. Please try again or request help on the WookieeTools server."}`);
  },
  VERIFY: function (playerData, isAdmin = false) {
    let message = 
`${discordUtils.symbols.pass} **${swapi.getPlayerName(playerData)} (${swapi.getPlayerAllycode(playerData)})** has been registered to you.
${swapi.getPlayerGuildId(playerData) ? `${discordUtils.symbols.pass} You are a${isAdmin ? "n admin" : " member"} of ${playerData.guildName}.` : `${swapi.getPlayerName(playerData)} is not a member of a guild.`}`;

    return createRegistrationEmbed(message);
  },
  UNREGISTERED: function (allycode) {
    return createRegistrationEmbed(`Unregistered ally code ${discordUtils.addDashesToAllycode(allycode)}.`);
  },
  PROMPT: function(playerData, imageFileName) {
    return createRegistrationEmbed(
`Verify ownership of ${swapi.getPlayerName(playerData)} (${swapi.getPlayerAllycodeWithDashes(playerData)}):
1. Set your in-game portrait to the image below
2. Click "Verify"`, imageFileName);
  }
}

// Exporting the slash command and its interaction handling function
module.exports = {
  data: new SlashCommandBuilder()
    .setName("register")
    .setDescription("Register an allycode (including alts) to your Discord account.")
    .addStringOption(o =>
      o.setName("allycode")
        .setDescription("The ally code to register.")
        .setRequired(true)),
  interact: async function (interaction) {
    // Extracting information from the interaction

    let allycode = interaction.options.getString("allycode");

    let discordId = interaction.user.id;
    let userId = interaction.user.id;
    const existingUser = await playerUtils.getUser(userId);

    // Validating and formatting the provided ally code
    try { allycode = playerUtils.cleanAndVerifyStringAsAllyCode(allycode.toString()); }
    // Handling error in ally code validation
    catch (err) { await interaction.editReply({ embeds: [discordUtils.createErrorEmbed("Rwww. ('No.').", err.message)] }); return; }

    // Check if the allycode is already registered to the discord user 
    if (existingUser?.allycodes.includes(allycode)) {
      await handleUnregister(interaction, allycode, existingUser);
      return;
    }

    // Fetch player data from SWGOH API
    const playerData = await swapi.getPlayer(allycode);

    if (swapi.getPlayerUnlockedPortraits(playerData).length < 2)
    {
      // player cannot be registered with no portraits
      await interaction.editReply({ embeds: [discordUtils.createErrorEmbed("Account Cannot Be Registered", "Account cannot be registered because it needs at least 2 portraits. Please double-check allycode. If the allycode is correct, a guild admin can register you with `/guild users register`.")] });
      return;
    }

    // Fetch all portrait data
    const portraitData = await swapi.getPlayerPortriatsList();

    // loop in case there is an image that does not exist in the database
    let randomPortraitId, randomPortraitImg;
    do {
      // Get the randomly selected unlocked portrait
      randomPortraitId = getRandomUnlockedPortraitId(playerData);
    
      // Find icon URLs for the random portrait
      randomPortraitImg = portraitData.find(p => p.player_portrait_id === randomPortraitId);
    } while (!randomPortraitImg);

    // Create the Discord message and components
    const canvasrandom = await drawPortraitImageToCanvas(randomPortraitImg.img);
    const portraitFileName = `${randomPortraitId}.png`;
    const filerandom = new AttachmentBuilder(canvasrandom.toBuffer("image/png"), { name: portraitFileName });
    const row = new ActionRowBuilder().addComponents(BUTTONS.CANCEL, BUTTONS.VERIFY);
    const embed = EMBEDS.PROMPT(playerData, portraitFileName);

    //passing the files and embed to be parsed in discord command
    // Send the message and wait for user response
    const response = await interaction.editReply({ embeds: [embed], files: [filerandom], components: [row] });

    // Handle user response (Verify/Cancel)
    const collectorFilter = i => { i.deferUpdate(); return i.user.id === interaction.user.id; }
    try {
      const confirmation = await response.awaitMessageComponent({ filter: collectorFilter, time: 300_000 });
      
      if (confirmation.customId === 'cancel') 
      {
        await interaction.editReply({ embeds: [EMBEDS.CANCEL], components: [], files: [] });
      }
      else if (confirmation.customId === 'verify') 
      {
        await doVerifyOwnership(interaction, allycode, randomPortraitId);
      }
      else
      {
        throw new Error(`No matching customId during verification step 1. CustomId: ${confirmation.customId}`);
      }
    }
    catch (e) {
      if (e.message.endsWith("time")) await interaction.editReply({ embeds: [EMBEDS.TIMEOUT], components: [], files: [] });
      else {
        logger.error('Error during unregister: ' + formatError(e));
        await interaction.editReply({ embeds: [EMBEDS.ERROR()], components: [], files: []});
      }
    }
  }
}

async function doVerifyOwnership(interaction, allycode, portraitIdToVerify)
{
  await interaction.editReply({ embeds: [EMBEDS.VERIFY_WAIT], components: [], files: [] });

  // retrieve player profile again to check whether they got the portrait changed properly
  let playerData = await swapi.getPlayer(allycode);
  let secondPortraitId = swapi.getPlayerSelectedPortrait(playerData);

  if (secondPortraitId !== portraitIdToVerify) {
    await interaction.editReply({ embeds: [EMBEDS.VERIFY_FAILURE], components: [], files: [] });
    return;
  }
  // Find the number of occurrences of the discord_id in the table
  const countQuery = 'SELECT COUNT(*) FROM user_registration WHERE discord_id = $1';
  const countResult = await database.db.one(countQuery, [interaction.user.id]);
  const alt = countResult.count;
  // at this point, the we can be confident that the discord user has access to the player account
  await database.db.any(
    `INSERT INTO user_registration(allycode, discord_id, alt)
    VALUES ($1, $2, $3)
    ON CONFLICT (allycode) DO UPDATE SET discord_id = $2, alt = $3;`,
    [allycode, interaction.user.id, alt]
  );
  let isAdmin = false;
  if (playerData.guildId)
  {
    // Fetching guild data from the SWGOH API to match member level
    let guild = await guildUtils.getGuildDataFromAPI({ allycode: allycode }, { useCache: false });

    // populate guild players
    await guildUtils.refreshGuildPlayers(guild);

    let wookieeBotGuildId = await guildUtils.getWookieeBotGuildIdForAPIGuild(guild);

    let matchingMember = guildUtils.getGuildRoster(guild).find(m => swapi.getPlayerAllycode(m) === allycode);

    switch (swapi.getPlayerGuildMemberLevel(matchingMember))
    {
      case 4: // leader
      case 3: // officer
        // if they were already registered, but not an admin, then make them an admin
        await database.db.any(
          `INSERT INTO user_guild_role (discord_id, guild_id, guild_admin, guild_bot_admin) 
          VALUES ($1, $2, $3, $4)
          ON CONFLICT (discord_id, guild_id) DO UPDATE SET guild_admin = $3, guild_bot_admin = $4`,
          [interaction.user.id, wookieeBotGuildId, true, true]
          );

        isAdmin = true;
        break;
      case 2: // member
        // if they were already registered, don't change any admin levels
        await database.db.any(
          `INSERT INTO user_guild_role (discord_id, guild_id, guild_admin, guild_bot_admin) 
          VALUES ($1, $2, $3, $4)
          ON CONFLICT (discord_id, guild_id) DO NOTHING`,
          [interaction.user.id, wookieeBotGuildId, false, false]
          );
        break;
      default: // not a member of a guild
        break;
    }
  }

  await interaction.editReply({ embeds: [EMBEDS.VERIFY(playerData, isAdmin)], components: [], files: [] })
}

async function handleUnregister(interaction, allycode, existingUser)
{
  //Create and send the Discord message with the "Unregister" button
  const row = new ActionRowBuilder()
    .addComponents(BUTTONS.UNREGISTER(allycode), BUTTONS.CANCEL);

  let content = { embeds: [EMBEDS.ALREADY_REGISTERED(allycode)], components: [row] };

  // Handle button interaction
  const collectorFilter = i => i.user.id === interaction.user.id;

  try {
    const response = await interaction.editReply(content);
    //No response after 120 seconds
    const confirmation = await response.awaitMessageComponent({ filter: collectorFilter, time: 120_000 });
    
    if (confirmation.customId === 'cancel') {
      await interaction.editReply({ embeds: [EMBEDS.CANCEL], components: [] });
      return;
    }
    
    if (confirmation.customId === 'unregister') {
      // Remove allycode from user_registration. If last allycode tied to discord_id delete discord_id in user_guild_role 

      let query = "UPDATE user_registration SET alt = alt-1 WHERE discord_id = $2 AND alt > (SELECT alt FROM user_registration WHERE allycode = $1); DELETE FROM user_registration WHERE allycode = $1";
      let params = [allycode, interaction.user.id];
      // todo: check whether this is their last allycode in the guild. If so, delete all guild roles for that guild

      let userGuild = existingUser.guilds.find(g => g.allycodes.find(a => a === allycode));
      if (userGuild?.allycodes.length === 1)
      {
        // this will be their last allycode in the guild. Remove all their guild roles for that guild
        query += "; DELETE FROM user_guild_role WHERE discord_id = $2 AND guild_id = $3;"
        params.push(userGuild.guildId);
      }

      await database.db.any(query, params);

      // Create response indicating successful unregister
      await interaction.editReply({ embeds: [EMBEDS.UNREGISTERED(allycode)], components: [] });
    }
    else
    {
      throw new Error(`No matching customId during verification step 1. CustomId: ${confirmation.customId}`);
    }
  }
  catch (e) {
    if (e.message.endsWith("time")) await interaction.editReply({ embeds: [EMBEDS.TIMEOUT], components: [], files: [] });
    else {
      logger.error('Error during unregister: ' + formatError(e));
      await interaction.editReply({ embeds: [EMBEDS.ERROR()], components: [], files: []});
    }
  }

}