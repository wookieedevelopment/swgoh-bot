const guildUtils = require("../../utils/guild.js");
const discordUtils = require("../../utils/discord.js");
const playerUtils = require("../../utils/player.js");
const database = require("../../database");
const guildRefresh = require("./refreshAllGuildData.js");
const guildRefreshRoster = require("./refresh/refreshRoster.js");
const { logger, formatError } = require("../../utils/log.js");
const constants = require("../../utils/constants.js");

module.exports = {
    name: "guild.register",
    description: "Register a guild for data tracking",
    execute: async function(client, input, args) {
        
        // for now, bot admin only
        if (!discordUtils.isBotOwner(input.author.id)) return;
        
        if (!args)
        {
            await input.channel.send("Need to provide an ally code.");
            return;
        }
    
        let allycode;
        try { allycode = playerUtils.cleanAndVerifyStringAsAllyCode(args[0]); }
        catch (err) { await input.channel.send({ embeds: [discordUtils.createErrorEmbed("Rwww. ('No.').", err.message)] }); return; }

        let guild;
        try {
            guild = await guildUtils.getGuildDataByAllycode(allycode);
        }
        catch (err)
        {
            logger.error(formatError(err));
            await input.channel.send("Failed to retrieve guild data for ally code " + allycode + ". " + err.message);
            return;
        }

        let guildName = guildUtils.getGuildName(guild);
        let guildAllycodes = guildUtils.getGuildAllycodesForAPIGuild(guild);

        await input.channel.send(`Found guild **${guildName}** with ${guildAllycodes.length} members.`);
        
        let existingGuild;
        if (guild.data)
        {
            existingGuild = await database.db.one('SELECT COUNT(*) FROM guild WHERE swgoh_gg_guild_id = $1', [guild.data.guild_id], r => r.count > 0);
        } else {
            existingGuild = await database.db.one('SELECT COUNT(*) FROM guild WHERE swgoh_help_guild_id = $1', [guild.id], r => r.count > 0);
        }
    
        if (!existingGuild)
        {
            await this.createGuildEntryAndPopulateData(input, guild, allycode);
            
            await input.reply("Guild **" + guildName + "** registered.");
        }
        else {
            await input.reply("Guild **" + guildName + "** is already registered.");
        }

        let guildId = await guildUtils.getWookieeBotGuildIdForAPIGuild(guild);

        // initialize TW planner settings
        const twMetadata = await database.db.oneOrNone("SELECT guild_id FROM tw_metadata WHERE guild_id = $1", guildId);
        if (!twMetadata)
        {
            try {
                await this.setupTwMetadata(guildId);
            } catch (e)
            {
                await input.channel.send("Error initializing TW settings: " + (e.message ? e.message : e));
                return;
            }
            await input.channel.send("Initialized TW settings.  Check with **tw.config**.");
        }
    },
    setupTwMetadata: async function(guildId) 
    {
        await database.db.any("INSERT INTO tw_metadata (max_teams_per_territory, default_max_squads_per_player, default_max_fleets_per_player, guild_id) VALUES ($1, $2, $3, $4)",
        [25, 5, 1, guildId])
    },
    createGuildEntryAndPopulateData : async function(input, guild, allycode)
    {
        // records to be updated:
        var guildInsertData = new Array();
        
        // declare ColumnSet once, and then reuse it:
        const guildCS = new database.pgp.helpers.ColumnSet(['guild_name', 'swgoh_help_guild_id', 'swgoh_gg_guild_id', 'allycode', 'description', 'track_data'], {table: 'guild'});
    
        let swgoh_help_guild_id = null, swgoh_gg_guild_id = null;
        if (guild.data) swgoh_gg_guild_id = guild.data.guild_id;
        else swgoh_help_guild_id = guild.id;

        guildInsertData.push({
            guild_name: guildUtils.getGuildName(guild),
            allycode: allycode,
            description: guildUtils.getGuildDescription(guild),
            swgoh_help_guild_id: swgoh_help_guild_id,
            swgoh_gg_guild_id: swgoh_gg_guild_id,
            track_data: true
        });
        
        const guildInsert = database.pgp.helpers.insert(guildInsertData, guildCS);
        
        // executing the query:
        await database.db.none(guildInsert);
    
        // populate guild players
        await guildRefreshRoster.populateGuildPlayers(guild);

        // grab an initial set of player data
        await guildRefresh.populatePlayerData(guild, constants.CacheTypes.MANUAL);

    }
    
}