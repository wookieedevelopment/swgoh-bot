const { loggers } = require("winston");
const database = require("../../database");
const discordUtils = require("../../utils/discord.js")
const playerUtils = require("../../utils/player.js")
const logger = require('winston');
//const modules = require('./index');

module.exports.registerUser = async function(args, callerInfo) {
    return await registerUser(args, callerInfo);
}

module.exports.setGuildAdmin = async function (args, callerInfo) {
    return await setGuildAdmin(args, callerInfo);
}

async function verifyBotOwer(userId)
{
    if (!discordUtils.isBotOwner(userId))
        throw modules.wookiee.talk();
    
    return await playerUtils.getUser(userId);
}

async function verifyGuildBotAdmin(userId, guildId)
{
    var me = await playerUtils.getUser(userId);
    
    var guildRoles = me.guilds.find(g => g.guildId == guildId)
    if (!guildRoles || (!guildRoles.guildBotAdmin))
        throw "Need to be the bot administrator for the guild. Check with your guild leader.\r\nIf you are an administrator, the user you are trying to affect may not be recognized by WookieeBot as part of your guild.  Please try again later.";

    return me;
}

async function verifyGuildAdmin(userId, guildId)
{
    var me = await playerUtils.getUser(userId);

    var guildRoles = me.guilds.find(g => g.guildId == guildId)
    if (!guildRoles || (!guildRoles.guildAdmin && !guildRoles.guildBotAdmin))
        throw "Need an administrative role for your guild to do this. Check with your guild leader.\r\nIf you are an administrator, the user you are trying to affect may not be recognized by WookieeBot as part of your guild.  Please try again later.";

    return me;
}

async function registerUser(args, callerInfo) 
{
    if (!args)
    {
        throw "Need to provide an ally code and a discord tag."
    }
    
    var splitArgs = args[0].toLowerCase().trim().split(/ +/);

    if (splitArgs.length != 2)
    {
        throw "Need to provide an ally code and a discord tag."
    }

    var allycode = playerUtils.cleanAndVerifyStringAsAllyCode(splitArgs[0]);
    var userIdToRegister = discordUtils.cleanDiscordId(splitArgs[1]);


    // get guild id for the allycode to register
    var guildPlayerInfo = await database.db.oneOrNone("SELECT guild_id FROM guild_players WHERE allycode = $1", [allycode]);

    // player not part of guild
    if (!guildPlayerInfo)
        throw "Ally code **" + allycode + "** is not yet confirmed as part of a guild. Please double check the ally code, or if it is correct, try again later.";

    const me = await verifyGuildAdmin(callerInfo.userId, guildPlayerInfo.guild_id);

    await registerUserAndSetDefaultRolesForGuild(userIdToRegister, allycode, guildPlayerInfo.guild_id);
    
    return "Success! Ally code **" + allycode + "** has been registered to " + discordUtils.makeIdTaggable(userIdToRegister);
}

async function setGuildAdmin(args, callerInfo)
{
    if (!args)
    {
        throw "Need to provide an ally code in the guild and a discord tag."
    }
    
    var splitArgs = args[0].toLowerCase().trim().split(/ +/);

    if (splitArgs.length != 2)
    {
        throw "Need to provide an ally code in the guild and a discord tag."
    }

    var allycodeToBeAdmin = playerUtils.cleanAndVerifyStringAsAllyCode(splitArgs[0]);
    var userIdToBeAdmin = discordUtils.cleanDiscordId(splitArgs[1]);

    // get guild id for the allycode to register
    var guildPlayerInfo = await database.db.oneOrNone("SELECT guild_id FROM guild_players WHERE allycode = $1", [allycodeToBeAdmin]);
    
    // player not part of guild
    if (!guildPlayerInfo)
        throw "Ally code **" + allycodeToBeAdmin + "** is not yet confirmed as part of a guild. Please double check the ally code, or if it is correct, try again later.";

    var me = await verifyGuildBotAdmin(callerInfo.userId, guildPlayerInfo.guild_id);
    
    var registration = playerUtils.getUserRegistration(userIdToBeAdmin, allycodeToBeAdmin);

    if (!registration)
    {
        await registerUserAndSetDefaultRolesForGuild(userIdToBeAdmin, allycodeToBeAdmin, guildPlayerInfo.guild_id);
    }

    await database.db.none("UPDATE user_guild_role SET guild_admin = TRUE WHERE discord_id = $1 AND guild_id = $2", [userIdToBeAdmin, guildPlayerInfo.guild_id]);

    return discordUtils.makeIdTaggable(userIdToBeAdmin) + " is now an admin for your guild.";
}

async function registerUserAndSetDefaultRolesForGuild(userId, allycode, guildId)
{
    const existingUser = await utils.getUser(userId);

    if (existingUser.guilds.length == 0)
    {
        // give them the default roles
        await database.db.none("INSERT INTO user_guild_role VALUES ($1, $2, $3, $4)", [userId, guildId, false, false]);
    } 

    if (existingUser.allycodes.find(a => a == allycode))
    {
        throw "User has already been registered to that ally code.";
    }
    else 
    {
        await database.db.none("INSERT INTO user_registration VALUES ($1, $2)", [allycode, userId]);
    }
}