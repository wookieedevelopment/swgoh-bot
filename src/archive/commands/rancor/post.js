const { MessageEmbed } = require("discord.js");
const discordUtils = require("../../../utils/discord.js");
const rancorUtils = require("./utils.js");
const database = require("../../../database");
const { logger, formatError } = require("../../../utils/log");

const postMemes = [
    "https://media.discordapp.net/attachments/786348824485560370/793953638367887420/Screenshot_20201230-162810_Chrome.jpg",
    "https://media.discordapp.net/attachments/376947363102851072/794293669179686922/post-meme-1.png",
    "https://cdn.discordapp.com/attachments/625813576510210058/656575301723226142/image0.gif",
    "https://cdn.discordapp.com/attachments/786348824485560370/795521871985901588/image0.gif",
    "https://tenor.com/2Wez.gif"
];

module.exports = {
    name: "rancor.post",
    aliases: ["shitpit.post"],
    description: "Send DMs to post Rancor/ShitPit held damage",
    execute: async function(client, input, args, sfw) {
        if (!args || args.length == 0) {
            await input.channel.send("You need to provide the channel for the " + rancorUtils.getRancorOrShitPitWord(sfw) + " monitor.");
            return;
        }

        let channelId = discordUtils.cleanDiscordId(args[0]);
        
        var monitorChannel = client.channels.cache.get(channelId);
        
        if (!monitorChannel) {
            await input.channel.send("You cannot do that to a channel that doesn't exist or is in another server.");
            return;
        }

        // check if the monitor is already installed in the channel
        var rancorMonitor = await rancorUtils.getRancorMonitor(channelId);

        if (!rancorMonitor) {
            await input.channel.send("You do not have a " + rancorUtils.getRancorOrShitPitWord(sfw) + " monitor set up in " + args[0] + ". Install " + rancorUtils.getRancorOrShitPitWord(sfw) + " monitor with " + rancorUtils.getRancorOrShitPitWord(sfw).toLowerCase() + ".install.");
            return;
        }
        
        let rmd = await database.db.any("SELECT player, damage_percent FROM rancor_monitor_data WHERE rancor_monitor_id = $1 AND phase = $2 AND posted = FALSE", 
            [rancorMonitor.rancor_monitor_id, rancorMonitor.current_phase]);

        for (let i = 0; i < rmd.length; i++)
        {
            if (discordUtils.isDiscordId(rmd[i].player))
            {
                try {
                    await input.author.send("Post your " + rmd[i].damage_percent + "% damage for Phase " + rancorMonitor.current_phase + "!");
                } catch (e) { logger.error(formatError(e)); }
            }
        }

        await database.db.none("UPDATE rancor_monitor_data SET posted = TRUE WHERE rancor_monitor_id = $1 AND phase = $2", [rancorMonitor.rancor_monitor_id, rancorMonitor.current_phase]);

        await rancorUtils.updateRancorMonitorMessage(rancorMonitor, monitorChannel);

        let postMeme = postMemes[Math.floor(Math.random()*postMemes.length)];

        await input.channel.send(postMeme);
        await input.channel.send("Direct messages have been sent to everyone holding damage.");
    }
}
