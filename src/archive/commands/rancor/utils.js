
const database = require("../../../database");
const discordUtils = require("../../../utils/discord.js");
const { MessageEmbed } = require("discord.js");

module.exports = {
    getRancorMonitor: async function(channelId)
    {
        return await database.db.oneOrNone("SELECT * FROM rancor_monitor WHERE channel = $1", [channelId]);
    },
    getRancorOrShitPitWord: function(sfw) { return sfw ? "Rancor" : "ShitPit"; },
    getRancorStatusEmbed: async function(rancorMonitor)
    {
        let rmd = await database.db.any(
            `SELECT player, posted, sum(damage_percent) as damage_percent FROM rancor_monitor_data 
            WHERE rancor_monitor_id = $1 AND phase = $2
            group by (player, posted)`, 
            [rancorMonitor.rancor_monitor_id, rancorMonitor.current_phase]);
        
        let postedDamageContributions = "", heldDamageContributions = "";
        let totalDamageHeld = 0, totalDamagePosted = 0;
        for (let i = 0; i < rmd.length; i++)
        {
            damagePercent = parseFloat(rmd[i].damage_percent)
            if (!rmd[i].posted) {
                totalDamageHeld += damagePercent;
                heldDamageContributions += discordUtils.makeIdTaggable(rmd[i].player) + ": " + rmd[i].damage_percent + "%\r\n";
            }
            else {
                totalDamagePosted += damagePercent;
                postedDamageContributions += discordUtils.makeIdTaggable(rmd[i].player) + ": " + rmd[i].damage_percent + "%\r\n";
            }
        }
        
        var totalDamage = totalDamageHeld + totalDamagePosted;
        var totalPrefix = "";
        if (totalDamage >= rancorMonitor.threshold)
        {
            totalPrefix = discordUtils.symbols.ok + " ";
        } else {
            totalPrefix = discordUtils.symbols.fail + " ";
        }

        var totalField = {
            name: "Total Damage (Held + Posted)",
            value:  totalPrefix + (totalDamageHeld + totalDamagePosted).toFixed(2) + "% (target = " + rancorMonitor.threshold + "%)",
            inline: false
        }

        var heldField = {
            name: "Total Damage Held: " + totalDamageHeld.toFixed(2) + "%",
            value: "----------------------------------\r\n" + heldDamageContributions,
            inline: true
        }

        var postedField = {
            name: "Total Damage Posted: " + totalDamagePosted.toFixed(2) + "%",
            value: "----------------------------------\r\n" + postedDamageContributions,
            inline: true
        }

        var currentRancorStatusEmbed = new MessageEmbed()
            .setTitle("Rancor Status, Phase " + rancorMonitor.current_phase)
            .setColor(0xD2691E)
            .addFields(
                totalField,
                heldField,
                postedField
            );

        return currentRancorStatusEmbed;
    },
    updateRancorMonitorMessage: async function(rancorMonitor, channel)
    {
        let embed = await this.getRancorStatusEmbed(rancorMonitor);

        (await channel.messages.fetch(rancorMonitor.static_message_snowflake)).edit(embed);
        //.bot.editMessage({ channelID: rancorMonitor.channel, messageID: rancorMonitor.static_message_snowflake, embed: embed });
    },
    parseDamage : function(damage, min, max)
    {
        if (!min) min = 0;
        if (!max) max = 100;

        var num = parseFloat(damage);
        if (isNaN(num) || num < min || num > max) throw new Error(damage + " isn't valid damage. Try a number between " + min + " and " + max + ".");

        num = num.toFixed(2);

        return num;
    }
}