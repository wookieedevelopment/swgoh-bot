const { MessageEmbed } = require("discord.js");
const discordUtils = require("../../utils/discord.js");
const rancorUtils = require("./utils.js");
const database = require("../../database");

const victoryMemes = [
    "https://tenor.com/view/star-wars-wookiee-gif-7412245",
    "https://tenor.com/view/star-wars-darth-vader-dance-dancing-happy-gif-4289259",
    "https://media.giphy.com/media/3o7abB06u9bNzA8lu8/giphy.gif"
];

const topContributorMemes = [
    "https://media3.giphy.com/media/6ADzKDwzPzLt6/giphy.gif",
    "https://media.giphy.com/media/l2JJyxPh3hWhQh05O/giphy.gif"
];

const roseImage = "https://cdn.discordapp.com/attachments/276169374178607105/617112614098436152/kellymarietran_5_3_2018_12_49_41_476.jpg";

module.exports = {
    name: "rancor.next",
    aliases: ["shitpit.next"],
    description: "Advance a WookieeBot Rancor/ShitPit monitor to the next phase.",
    execute: async function(client, input, args, sfw) {
        
        if (!args || args.length == 0) {
            await input.channel.send("You need to provide the channel for the " + rancorUtils.getRancorOrShitPitWord(sfw) + " monitor.");
            return;
        }

        let channelId = discordUtils.cleanDiscordId(args[0]);
        
        var monitorChannel = client.channels.cache.get(channelId);
        
        if (!monitorChannel) {
            await input.channel.send("You cannot do that to a channel that doesn't exist or is in another server.");
            return;
        }

        // check if the monitor is already installed in the channel
        var existingMonitor = await rancorUtils.getRancorMonitor(channelId);

        if (!existingMonitor) {
            await input.channel.send("You do not have a " + rancorUtils.getRancorOrShitPitWord(sfw) + " monitor set up in " + args[0] + ". Install " + rancorUtils.getRancorOrShitPitWord(sfw) + " monitor with " + rancorUtils.getRancorOrShitPitWord(sfw).toLowerCase() + ".install.");
            return;
        }

        if (existingMonitor.current_phase == 4)
        {
            await input.channel.send(victoryMemes[Math.floor(Math.random()*victoryMemes.length)]);

            let victoryMessage = "The " + rancorUtils.getRancorOrShitPitWord(sfw) + " is finished! :relieved:\r\nWhen you're ready, reset your monitor with " + rancorUtils.getRancorOrShitPitWord(sfw).toLowerCase() + ".reset";

            let topPercentContributor = await database.db.oneOrNone(
                `select player, sum(damage_percent) total from rancor_monitor_data
                    where rancor_monitor_id = $1
                    group by player
                    order by total desc
                    limit 1`, [existingMonitor.rancor_monitor_id]);
    
            if (topPercentContributor) {
                victoryMessage += "\r\n\r\nA warm, furry congratulations to " + discordUtils.makeIdTaggable(topPercentContributor.player) + " for contributing the highest total % of damage: " + topPercentContributor.total + "%!";
                victoryMessage += "\r\n\r\n" + discordUtils.makeIdTaggable(topPercentContributor.player) + " right now:";
            }

            await input.channel.send(victoryMessage);

            if (topPercentContributor)
                await input.channel.send(topContributorMemes[Math.floor(Math.random()*topContributorMemes.length)]);
                
            return;
        }

        await database.db.none("UPDATE rancor_monitor SET current_phase = $1, threshold = 100 WHERE rancor_monitor_id = $2", [existingMonitor.current_phase + 1, existingMonitor.rancor_monitor_id]);
        
        existingMonitor.current_phase++;

        await rancorUtils.updateRancorMonitorMessage(existingMonitor, monitorChannel);

        let response = "On to Phase " + existingMonitor.current_phase + "!";
        if (existingMonitor.current_phase == 2)
        {
            response += " Remember to feed the rancor.";
            await input.channel.send(roseImage);
        }
        
        await input.channel.send(response);
    }
}
