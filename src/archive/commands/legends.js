var gl_data = require("../../data/legends_data.json");
var jp = require('jsonpath');

const ApiSwgohHelp = require('api-swgoh-help');
const swapi = new ApiSwgohHelp({
    "username":process.env.API_USERNAME,
    "password":process.env.API_PASSWORD,
});

module.exports.parsePlayer = async function(args) {
    return await parsePlayer(args);
}

async function parsePlayer(args) {

    if (!args.length) {
        return message.channel.send(`You didn't provide an ally code!`);
    }
    var playerId = args[0].replace(/-/g, "");
    var detailed = false;

    if (args.length > 1)
    {
        for (var argIndex = 1; argIndex < args.length; argIndex++)
        {
            if (args[argIndex] == "d" || args[argIndex] == "details")
            {
                detailed = true;
            }
        }
    }

    let payload = {
        allycode:playerId,
        language: "eng_us"
    };

    var data = await swapi.fetchData(
        {
            collection : "unitsList",
            language: "eng_us",
            match :
                {
                    "rarity":7,
                    "obtainable": true,
                    "obtainableTime": 0,
                    "combatType": 1 // characters only
                },
            project :
                {
                    "nameKey": 1,
                    "baseId": 1,
                    "descKey": 1,
                    "categoryIdList": 1
                }
        }
    );

    var unitsList = data.result;

    let fetchPlayer = await swapi.fetchPlayer(payload).catch(error => {
        return(error);
    });

    var player = fetchPlayer.result;
    var playerName = player[0].name;

    var embedToSend = createEmbed();

    for (var checkChar in cheating_data)
    {
        var charResultsString = "";
        var fv = { name: null, value: null, inline: false };

        var rareCharQuery = jp.query(player, "$..roster[?(@.nameKey=='" + checkChar + "')]");


        fv.name = "__**" + checkChar + "**__";

        if (rareCharQuery.length == 0)
        {
            charResultsString = "Not activated.";
            fv.name = ":zero: " + fv.name;
        }
        else if (rareCharQuery[0].rarity != 7)
        {
            charResultsString = "Only checking units at 7★";
            fv.name = ":no_mouth: " + fv.name + " (" + rareCharQuery[0].rarity + "★)";
        }
        else
        {

            let results = await checkRequirements(player[0], cheating_data[checkChar], unitsList);

            fv.name = (likelihoodSymbol(results.likelihood) + " " + fv.name + " (" + rareCharQuery[0].rarity + "★)").trim();

            var likelihoodString = results.likelihood + "% chance that " + playerName + " cheated.";

            charResultsString += likelihoodString;
            if (detailed || results.likelihood >= warningThreshold)
            {
                charResultsString += "```";
                for (var charIndex = 0; charIndex < results.characters.length; charIndex++)
                {
                    var character = results.characters[charIndex];
                    charResultsString += character.characterName + ", " +
                        character.stars + "★, " +
                        "G" + character.gear + ", " +
                        character.zetas + " zetas\r\n";
                }
                charResultsString += "```";
            }

        }

        fv.value = charResultsString;

        embedToSend.fields.push(fv);
    }

    return embedToSend;
}