const database = require("../../database");
const discordUtils = require("../../utils/discord.js")
const { logger } = require("../../utils/log");
const rancorHelp = require("./rancor/help.js");
const rancorInstall = require("./rancor/install.js");
const rancorUninstall = require("./rancor/uninstall.js");
const rancorReset = require("./rancor/reset.js");
const rancorNext = require("./rancor/next.js");
const rancorThreshold = require("./rancor/threshold.js");
const rancorPost = require("./rancor/post.js");
const rancorReport = require("./rancor/report.js");
const rancorUtils = require("./rancor/utils.js");
const { SlashCommandBuilder } = require('@discordjs/builders');

module.exports = {
    data: new SlashCommandBuilder()
        .setName("rancor")
        .setDescription("Damage tracking and notifications for the Rancor (Challenge Tier) raid."),
    name: "rancor",
    aliases: ["shitpit"],
    description: "Damage tracking and notifications for the Rancor (Challenge Tier) raid.",
    execute: async function(client, input, args)
    {
        let cmd = discordUtils.splitCommandInput(input);

        let sfw = true;
        if (cmd[0].toLowerCase() == "shitpit") sfw = false;

        if (cmd.length == 1) {
            await rancorHelp.execute(client, input, args, sfw);
            return;
        }

        switch (cmd[1].toLowerCase())
        {
            case "help":
                await rancorHelp.execute(client, input, args, sfw);
                break;
            case "install":
                await rancorInstall.execute(client, input, args, sfw);
                break;
            case "uninstall":
                await rancorUninstall.execute(client, input, args, sfw);
                break;
            case "reset":
                await rancorReset.execute(client, input, args, sfw);
                break;
            case "next":
                await rancorNext.execute(client, input, args, sfw);
                break;
            case "post":
                await rancorPost.execute(client, input, args, sfw);
                break;
            case "report":
                await rancorReport.execute(client, input, args, sfw);
                break;
            case "treshold":
            case "t":
                await rancorThreshold.execute(client, input, args, sfw);
                break;
            default: break;
        }
    },
    activity: async function(client, input, rancorMonitor)
    {
        if (!client || !input) return;
        if (client.user.id == input.author.id) return; // don't delete the message if this bot is the author

        input.delete().catch(() => { /* someone already deleted it */ });
        
        if (input.author.bot)
            return;

        let player = input.author.id;
        let playerName = input.author.username;

        var damage;

        let message = input.content.trim();
    
        let lastSpace = message.lastIndexOf(" ");
        if (lastSpace > 0)
        {
            player = message.slice(0, lastSpace);
            if (discordUtils.isDiscordTag(player)) player = discordUtils.cleanDiscordId(player);
            playerName = null; // they're putting this in on behalf of someone else
            message = message.slice(lastSpace + 1);
        }

        try {
            damage = rancorUtils.parseDamage(message);
        }
        catch (e)
        {
            await input.author.send(e.message);
            return;
        }

        // check if the player already has damage held in the phase and replace it if they do
        await database.db.none("DELETE FROM rancor_monitor_data WHERE phase = $1 AND player = $2 AND posted = FALSE", [rancorMonitor.current_phase, player])

        if (damage > 0)
        {
            await database.db.none("INSERT INTO rancor_monitor_data(rancor_monitor_id, phase, player, player_name, damage_percent, timestamp) VALUES ($1, $2, $3, $4, $5, $6)", 
                [rancorMonitor.rancor_monitor_id, rancorMonitor.current_phase, player, playerName, damage, new Date()]);
        }

        await rancorUtils.updateRancorMonitorMessage(rancorMonitor, input.channel);
    }
}