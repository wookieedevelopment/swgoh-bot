const database = require("../../database");
const utils = require("../utils/general.js")
const ApiSwgohHelp = require('api-swgoh-help');
const modules = require('../commands/index');

const swapi = new ApiSwgohHelp({
    "username":process.env.API_USERNAME,
    "password":process.env.API_PASSWORD,
});

module.exports.startTrackingGoalSetForGuild = async function(args, callerInfo) {
    return await startTrackingGoalSetForGuild(args, callerInfo);
}

module.exports.stopTrackingGoalSetForGuild = async function(args, callerInfo) {
    return await stopTrackingGoalSetForGuild(args, callerInfo);
}

module.exports.updateGuildStatusForGoals = async function(args, callerInfo) {
    return await updateGuildStatusForGoals(args, callerInfo);
}

async function getAllGoalSets()
{
    const goalsets = await database.db.any('SELECT DISTINCT goal_set_name FROM goal_set_goal');
    return goalsets.map((value) => { return value.goal_set_name; });
}

// track a goal set for a guild
async function startTrackingGoalSetForGuild(args)
{
    const validGoalSets = await getAllGoalSets();

    // args expected: allycode goal-set-name
    if (!args)
    {
        throw "Need to provide an allycode and a goal name.  Valid goal names are:\r\n" + validGoalSets.join("\r\n");
    }

    var splitArgs = args[0].toUpperCase().trim().split(/ +/);

    if (splitArgs.length != 2)
    {
        throw "Need to provide an allycode and a goal name."
    }
    
    var allycode = utils.cleanAndVerifyStringAsAllyCode(splitArgs[0]);

    var goalNameToSet = splitArgs[1].trim();

    if (validGoalSets.indexOf(goalNameToSet) == -1)
        throw "Need to provide a valid goal name.  Valid goal names are:\r\n" + validGoalSets.join("\r\n");

    var guild = await utils.getGuildData(allycode);

    var guildGoalSetInsertData = new Array();
    guildGoalSetInsertData.push({
        guild_id: guild.id,
        goal_set_name: goalNameToSet
    })
    const guildGoalSetCS = new database.pgp.helpers.ColumnSet(['guild_id', 'goal_set_name'], {table: 'guild_goal_set'});
    
    const guildGoalSetInsert = database.pgp.helpers.insert(guildGoalSetInsertData, guildGoalSetCS);
    
    try 
    {
        await database.db.none(guildGoalSetInsert);
    } 
    catch (e)
    {
        if (e.message.includes("duplicate key value violates unique constraint"))
            throw "Goal '" + goalNameToSet + "' is already set for guild " + guild.name + ".";
        else
            throw "Something went wrong.  Please try again later.";
    }

    return "Goal '" + goalNameToSet + "' has been set for guild " + guild.name + ".";
}

// stop tracking a goal set for a guild
async function stopTrackingGoalSetForGuild(args)
{
    const validGoalSets = await getAllGoalSets();

    console.log(validGoalSets);
    // args expected: allycode goal-set-naem
    if (!args)
    {
        throw "Need to provide an allycode and a goal name.  Valid goal names are:\r\n" + validGoalSets.join("\r\n");
    }

    var splitArgs = args[0].toUpperCase().trim().split(/ +/);

    if (splitArgs.length != 2)
    {
        throw "Need to provide an allycode and a goal name."
    }
    
    var allycode = utils.cleanAndVerifyStringAsAllyCode(splitArgs[0]);

    var goalNameToStopTracking = splitArgs[1].trim();

    if (validGoalSets.indexOf(goalNameToStopTracking) == -1)
        throw "Need to provide a valid goal name.  Valid goal names are:\r\n" + validGoalSets.join("\r\n");

    var guild = await utils.getGuildData(allycode);

    // delete old static data
    await database.db.none("DELETE FROM guild_goal_set WHERE guild_id = ($1) AND goal_set_name = ($2)", [guild.id, goalNameToStopTracking]);
    
    return "Goal '" + goalNameToStopTracking + "' has been removed for guild " + guild.name + ".";
}

// loop through all players in guild and update their status toward all tracked goals
async function updateGuildStatusForGoals(args, callerInfo)
{
    if (!utils.isBotOwner(callerInfo.userId))
        throw modules.wookiee.talk();

    if (!args || !args[0])
    {
        throw "Need to provide an ally code."
    }

    var allycode = utils.cleanAndVerifyStringAsAllyCode(args[0]);

    var guildInfo = await utils.getGuildIdAndAllyCodesForGuildByAllycode(allycode);

    // loop through guidl_goal_set and identify all goal_set_names
    var goalSetsResults = await database.db.any(
        "SELECT guild_id,guild_goal_set.goal_set_name,goal_type,goal_data,goal_target,goal_set_goal_id " +
        "FROM guild_goal_set join goal_set_goal on guild_goal_set.goal_set_name = goal_set_goal.goal_set_name " +
        "WHERE guild_id = $1",
        [guildInfo.guildId]
    );

    // fetch all rosters for allycodes in guild
    let { result, error, warning } = await swapi.fetchPlayer({ allycodes: guildInfo.allycodes, language: "ENG_US" });

    var timestamp = new Date();
    
    var goalPlayerStatusInsertData = new Array();
    const goalPlayerStatusCS = new database.pgp.helpers.ColumnSet(['allycode', 'goal_set_goal_id', 'timestamp', 'status'], {table: 'goal_player_status'});

    var errorMessages = "";

    // loop through all players and rosters and match against the goal_set_goals
    for (var playerIndex = 0; playerIndex < result.length; playerIndex++)
    {
        var player = result[playerIndex];

        for (var g = 0; g < goalSetsResults.length; g++)
        {
            var goal = goalSetsResults[g];
            var goalType = goal.goal_type;
            var goalData = goal.goal_data;
            var goalId = goal.goal_set_goal_id;

            var status;

            if (goalType == "RELIC")
            {
                var unit = player.roster.find(u => u.defId == goalData);
                if (!unit)
                {
                    status = -1;
                } else if (!unit.relic)
                {
                    errorMessages += "Relic data is null for " + goalData + ". Is this really a character?\r\n";
                    continue;
                } else {
                    status = unit.relic.currentTier - 2; // API shows relic with +2 from what's in game
                }
            } else if (goalType == "GEAR")
            {
                var unit = player.roster.find(u => u.defId == goalData);
                if (!unit)
                {
                    status = 0;
                } else {
                    status = unit.gear;
                }

            } else if (goalType == "RARITY")
            {
                var unit = player.roster.find(u => u.defId == goalData);
                if (!unit)
                {
                    status = 0;
                } else {
                    status = unit.rarity;
                }
            }
            
            goalPlayerStatusInsertData.push({
                allycode: player.allyCode,
                goal_set_goal_id: goalId,
                timestamp: timestamp,
                status: status
            });
        }
    }

    const goalPlayerStatusInsert = database.pgp.helpers.insert(goalPlayerStatusInsertData, goalPlayerStatusCS);
    
    await database.db.none(goalPlayerStatusInsert);

    return "Guild status for all goals refreshed for " + guildInfo.allycodes.length + " members." + 
        (errorMessages.length > 0 ? ("\r\n\r\nErrors: " + errorMessages) : "");
}