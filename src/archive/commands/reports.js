const database = require("../../database");
//TODO: Need to rehookup utils
const { ChartJSNodeCanvas } = require('chartjs-node-canvas');
const ApiSwgohHelp = require('api-swgoh-help');

const swapi = new ApiSwgohHelp({
    "username":process.env.API_USERNAME,
    "password":process.env.API_PASSWORD,
});


module.exports.getReport = async function(args, callerInfo) {
    return await getReport(args, callerInfo);
}

const monthShortNames = 
[
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec"
];

const reportTypes = {
    "reek" : getReportReek,
    "pg" : getReportPlayerGoals,
    "gg" : getReportGuildGoals,
    "gp" : getChartGpOverTimeForAllyCode,
    "gac" : getChartGacOverTimeForAllyCode,
    "gls" : getChartGLsForGuild,
    "mq" : getChartModQualityOverTimeForAllyCode,
    "payouts" : getChartGuildPayouts
};


function getReportTypesString()
{
    return Object.keys(reportTypes).sort().toString().split(",").join(", ");
}

async function getReport(args, callerInfo)
{
    if (!args)
    {
        var embedToSend = createEmbed();
        embedToSend.description = "Need to provide a report type and an ally code.  Valid report types are: " + getReportTypesString();
        embedToSend.fields.push(
            { name: "Report options: ",
              value : 
                "gac: graph of gac lifetime score for a player (30 days)\r\n" +
                "gls: graph of GLs in the guild\r\n" +
                "gp: graph of gp growth for a player (30 days)\r\n" +
                "gp.s: graph of squad gp growth for a player (30 days)\r\n" +
                "gp.f: graph of fleet gp growth for a player (30 days)\r\n" +
                "mq: graph of mod quality growth for a player (Wookiee style, 30 days)\r\n" +
                "mq.dsr: graph of mod quality growth for a player (DSR style, 30 days)\r\n" +           
                "reek: list of guild members that have not beaten the reek\r\n\r\n" + 
                "payouts: chart of when guild members have payouts, and how many\r\n\r\n" + 
                "gg: summary of the status of all guild goals for the guild\r\n" +
                "gg.<goal_name>: list of who has and has not met a specific goal\r\n" +
                "pg: details of a player's progression toward goals, in priority order\r\n" +
                "goal names: ^setgoal\r\n\r\n" +

                "Examples:\r\n" +
                "^report reek 985736123\r\n" +
                "^report gp.s 985736123\r\n" +
                "^report gg.RANCOR_P1_GAS"
                ,
              inline: false});

        return embedToSend;
    }

    var splitArgs = args[0].toLowerCase().trim().split(/ +/);

    var allycode;

    if (splitArgs.length == 1)
    {
        // check if the caller is registered
        let user = await utils.getUser(callerInfo.userId);

        if (user && user.allycodes.length > 0) 
        {
            allycode = user.allycodes[0];
        }
        else
        {
            throw "Need to be registered (use registeruser) or provide a report type and an ally code."
        }
    } 
    else if (splitArgs.length != 2)
    {
        throw "Need to provide a report type and an ally code."
    }
    else
    {
        allycode = utils.cleanAndVerifyStringAsAllyCode(splitArgs[1]);
    }
        
    var reportType = splitArgs[0].trim();

    var subreport = null;

    if (reportType.indexOf(".") > 0)
    {
        var splitReportType = reportType.split(/\./);
        if (splitReportType.length != 2)
        {
            throw "Invalid subreport format. Format is [report type].[subreport]. For example: report gg.kam allycode"
        }
        reportType = splitReportType[0];
        subreport = splitReportType[1];
    }
    if (reportTypes[reportType]) {
        if (subreport)
            return await reportTypes[reportType](allycode, subreport);

        return await reportTypes[reportType](allycode);
    }

    throw "Valid report types are: " + getReportTypesString();
}


function createEmbed()
{
    var embed = {
        "title": "",
        "description": "",
        "color": 0xD2691E,
        "author": {
          "name": "WookieeBot"
        },
        "fields": []
    };

    return embed;
}

function createImageReportResultObj(message, imageBuffer, fileName)
{
    return {
        message: message,
        imageBuffer: imageBuffer,
        fileName: fileName
    };
} 

async function getReportReek(allycode)
{
    
    var guildInfo = await utils.getGuildIdAndAllyCodesForGuildByAllycode(allycode);
    const reekData = await database.db.any('SELECT allycode, player_name, reek_win FROM player_data_static WHERE reek_win = FALSE AND allycode LIKE ANY ($1) ORDER BY player_name ASC', [guildInfo.allycodes]);

    var embedToSend = createEmbed();
    embedToSend.title = "Guild members that have not beaten the Reek mission";

    for (var d in reekData)
    {
        var dataRow = reekData[d];
        embedToSend.description += dataRow.player_name + " (" + dataRow.allycode + ")\r\n";
    }
    
    return embedToSend;
}

async function getReportPlayerGoals(allycode, subReport)
{
    // loop through all guild goals, in priority order
    // display status of each of those goals
    
    // embed
    var embedToSend = createEmbed();

    // get all the goals for the guild and all the latest statuses for players in the guild
    var goalStatusResults = await database.db.any(
    `select distinct on (priority, gsg.goal_set_name, gsg.goal_set_goal_id) allycode, gps.goal_set_goal_id, goal_type, goal_data, goal_target, gsg.goal_set_name, priority, status, timestamp
    FROM goal_player_status gps
    JOIN goal_set_goal gsg ON gps.goal_set_goal_id = gsg.goal_set_goal_id 
	JOIN guild_goal_set ggs ON ggs.goal_set_name = gsg.goal_set_name
    WHERE allycode = $1
    ORDER BY priority, gsg.goal_set_name, gsg.goal_set_goal_id, timestamp desc`, allycode);

    if (goalStatusResults.length == 0)
    {
        throw "No goal status information for " + allycode + ".";
    }

    embedToSend.title = "Status of Guild Goals for " + allycode + "\r\nLast checked: " + new Date(goalStatusResults[0].timestamp);

    var goalSets = new Array();
    var curGoalSet = null;

    for (var gsIndex in goalStatusResults)
    {
        var goalStatus = goalStatusResults[gsIndex];
        var goalSetName = goalStatus.goal_set_name;

        if (!curGoalSet || curGoalSet.goalSetName != goalSetName)
        {
            curGoalSet = { 
                goalSetName: goalStatus.goal_set_name, 
                priority: goalStatus.priority, 
                goals: new Array(), 
                met: true 
            };

            goalSets.push(curGoalSet);
        }
        
        if (goalStatus.goal_type == "RELIC" || goalStatus.goal_type == "GEAR" || goalStatus.goal_type == "RARITY")
        {
            var currentValue = parseInt(goalStatus.status);
            var target = parseInt(goalStatus.goal_target);
            var goal = {
                data: goalStatus.goal_data,
                goalType: goalStatus.goal_type, 
                goalTarget: target,
                currentValue: currentValue,
                met: true
            };

            if (currentValue < target)
            {
                goal.met = false;
                curGoalSet.met = false;
            }

            curGoalSet.goals.push(goal);

        } 
    }

    // field is goal
    for (var gsIx in goalSets)
    {
        var goalSet = goalSets[gsIx];

        var fieldName = (goalSet.met ? utils.discordSymbols.pass : "") + " " + goalSet.goalSetName;

        var fv = { name: fieldName, value: "", inline: false }; 

        if (!goalSet.met)
        {
            for (var goalIx in goalSet.goals)
            {
                var goal = goalSet.goals[goalIx];
                var dataName = goal.data;
                var translatedDataName = await utils.translateDefIdToNameKey(dataName);
                var discordSymbol = (goal.met ? utils.discordSymbols.pass : utils.discordSymbols.fail);
                fv.value += discordSymbol + " **" + translatedDataName + "**" + " at " + goal.goalType + " " + goal.goalTarget + 
                        " (Current: " + goal.goalType + " " + Math.max(goal.currentValue, 0) + ")\r\n";
            }
        } else { fv.value = "Goal met!"; }
            
        embedToSend.fields.push(fv);
    }

    return embedToSend;
}

async function getReportGuildGoals(allycode, goal)
{
    if (!goal)
        return await getReportAllGuildGoals(allycode);
    else
        return await getReportOneGuildGoal(allycode, goal);
    
}

async function getReportAllGuildGoals(allycode)
{
    // embed
    var embedToSend = createEmbed();

    var guildInfo = await utils.getGuildIdAndAllyCodesForGuildByAllycode(allycode);
    var totalGuildMembers = guildInfo.allycodes.length;

    // get all the goals for the guild and all the latest statuses for players in the guild
    var goalStatusResults = await database.db.any(
    `select distinct on (allycode, goal_set_goal_id) allycode, gps.goal_set_goal_id, goal_type, goal_data, goal_target, goal_set_name, status, timestamp
    FROM goal_player_status gps
    JOIN goal_set_goal gsg ON gps.goal_set_goal_id = gsg.goal_set_goal_id 
    WHERE allycode LIKE ANY 
    (SELECT allycode FROM guild_players WHERE guild_id = (SELECT guild_id FROM guild_players WHERE allycode = $1))
    ORDER BY allycode, goal_set_goal_id, timestamp desc`, allycode);

    var goals = {};
    for (var gsIndex in goalStatusResults)
    {
        var goalStatus = goalStatusResults[gsIndex];
        var goalSetName = goalStatus.goal_set_name;
        
        if (!goals[goalSetName])
            goals[goalSetName] = {};
        if (!goals[goalSetName][goalStatus.goal_set_goal_id])
            goals[goalSetName][goalStatus.goal_set_goal_id] = { goalData: goalStatus.goal_data, goalType: goalStatus.goal_type, goalTarget: goalStatus.goal_target, count: 0 };
        
        if (goalStatus.goal_type == "RELIC" || goalStatus.goal_type == "GEAR" || goalStatus.goal_type == "RARITY")
        {
            var currentValue = parseInt(goalStatus.status);
            var target = parseInt(goalStatus.goal_target);
            if (currentValue < target)
                continue;
        } 

        goals[goalSetName][goalStatus.goal_set_goal_id].count++;
    }

    // field is goal
    var goalSetNames = Object.keys(goals);
    for (var gsn in goalSetNames)
    {
        var goalInfo = goals[goalSetNames[gsn]];

        var fv = { name: goalSetNames[gsn], value: "", inline: false }; 
        var goalSetGoalIds = Object.keys(goalInfo);

        for (var gsgIndex in goalSetGoalIds)
        {
            var gsgId = goalSetGoalIds[gsgIndex];
            var data = goalInfo[gsgId];
            var translatedDataName = await utils.translateDefIdToNameKey(data.goalData);
            fv.value += "**" + translatedDataName + "**" + " at " + data.goalType + " " + data.goalTarget + ": " + data.count + "/" + totalGuildMembers + "\r\n";
        }
        
        embedToSend.fields.push(fv);
    }

    return embedToSend;
}


async function getReportOneGuildGoal(allycode, goal)
{
    // embed
    var embedToSend = createEmbed();

    var guildInfo = await utils.getGuildIdAndAllyCodesForGuildByAllycode(allycode);
    var totalGuildMembers = guildInfo.allycodes.length;

    goal = goal.toUpperCase();

    // get all the goal statuses for the guild and all the goal in question
    var goalStatusResults = await database.db.any(
        `select distinct on (gps.allycode, goal_set_goal_id) gps.allycode, player_name, gps.goal_set_goal_id, goal_type, goal_data, goal_target, goal_set_name, status, timestamp
        from goal_player_status gps
        join goal_set_goal gsg on gps.goal_set_goal_id = gsg.goal_set_goal_id 
        join guild_players gp on gps.allycode = gp.allycode
        where gps.allycode LIKE ANY 
        (SELECT allycode FROM guild_players WHERE guild_id = (SELECT guild_id FROM guild_players WHERE allycode = $1))
        AND goal_set_name = $2
        ORDER BY gps.allycode, goal_set_goal_id, timestamp desc`, [allycode, goal]);

    var goalDetails = {};
    var details = { };
    for (var gsIndex in goalStatusResults)
    {
        var goalStatus = goalStatusResults[gsIndex];
        if (!goalDetails[goalStatus.goal_set_goal_id])
        {
            goalDetails[goalStatus.goal_set_goal_id] = {
                goalType: goalStatus.goal_type,
                goalData: goalStatus.goal_data,
                goalTarget: goalStatus.goal_target
            };
        }
        // initialize
        if (details[goalStatus.allycode] == null)
            details[goalStatus.allycode] = { name: goalStatus.player_name, metGoal: true };
        
        // check if they already failed
        if (details[goalStatus.allycode].metGoal == false)
            continue;

        if (goalStatus.goal_type == "RELIC" || goalStatus.goal_type == "GEAR" || goalStatus.goal_type == "RARITY")
        {
            var currentValue = parseInt(goalStatus.status);
            var target = parseInt(goalStatus.goal_target);
            if (currentValue < target)
            {
                details[goalStatus.allycode].metGoal = false;
                continue;
            }
        }
    }

    var fvSummary = {name: "Goal Summary: " + goal, value: "", inline: false };

    var goalDetailKeys = Object.keys(goalDetails);
    for (var keyIndex in goalDetailKeys)
    {
        var goalDetail = goalDetails[goalDetailKeys[keyIndex]];

        dataName = goalDetail.goalData;
        
        if (goalDetail.goalType == "RELIC" || goalDetail.goalType == "GEAR" || goalDetail.goalType == "RARITY")
            dataName = await utils.translateDefIdToNameKey(goalDetail.goalData);

        fvSummary.value += utils.discordSymbols.smallBlueDiamond + " **" + dataName + "**" + " at " + goalDetail.goalType + " " + goalDetail.goalTarget + "\r\n";
    }

    var fvPass = { name: utils.discordSymbols.pass + " Players Meeting Goal", value: "", inline: false }; 
    var fvPass2 = { name: utils.discordSymbols.pass + " Players Meeting Goal (continued)", value: "", inline: false }; 
    var fvFail = { name: utils.discordSymbols.fail + " Players Not Meeting Goal", value: "", inline: false }; 
    var fvFail2 = { name: utils.discordSymbols.fail + " Players Not Meeting Goal (continued)", value: "", inline: false }; 
    var fvNoData = { name: utils.discordSymbols.question + " No Data Yet", value: "", inline: false }

    var passCount = 0, failCount = 0, noDataCount = 0;

    for (var aci in guildInfo.allycodes)
    {
        var allycode = guildInfo.allycodes[aci];
        var detail = details[allycode];

        if (!detail)
        {
            fvNoData.value += allycode + "\r\n";
            noDataCount++;
        }
        else if (detail.metGoal)
        {
            var valToAdd = detail.name + " (" + allycode + ")\r\n";
            if (valToAdd.length + fvPass.value.length < utils.maxEmbedFieldValueLength)
                fvPass.value += valToAdd;
            else
                fvPass2.value += valToAdd;

            passCount++;
        } 
        else
        {
            var valToAdd = detail.name + " (" + allycode + ")\r\n";
            if (valToAdd.length + fvFail.value.length < utils.maxEmbedFieldValueLength)
                fvFail.value += valToAdd;
            else
                fvFail2.value += valToAdd;
                
            failCount++;
        }
    }

    fvPass.name += " (" + passCount + ")";
    fvPass2.name += " (" + passCount + ")";
    fvFail.name += " (" + failCount + ")";
    fvFail2.name += " (" + failCount + ")";
    fvNoData.name += " (" + noDataCount + ")";

    embedToSend.fields.push(fvSummary);
    embedToSend.fields.push(fvPass);
    if (fvPass2.value.length > 0)
        embedToSend.fields.push(fvPass2);
        
    embedToSend.fields.push(fvFail);
    if (fvFail2.value.length > 0)
        embedToSend.fields.push(fvFail2);

    if (fvNoData.value != "")
        embedToSend.fields.push(fvNoData);
    
    return embedToSend;
}

async function getChartGLsForGuild(allycode)    
{
    const glData = await database.db.any(
        `SELECT   
         COUNT(*) FILTER (WHERE gl_rey = TRUE) AS rey_count, 
         COUNT(*) FILTER (WHERE gl_slkr = TRUE) AS slkr_count,
         COUNT(*) FILTER (WHERE gl_jml = TRUE) AS jml_count,
         COUNT(*) FILTER (WHERE gl_see = TRUE) AS see_count,
         COUNT(*) AS players_count
         FROM player_data_static WHERE allycode LIKE ANY 
         (SELECT allycode FROM guild_players WHERE guild_id = (SELECT guild_id FROM guild_players WHERE allycode = $1))`, allycode
    );

    var data = [glData[0].rey_count, glData[0].slkr_count, glData[0].jml_count, glData[0].see_count];

    const width = 500;
    const height = 500;
    const chartCallback = (ChartJS) => {
        ChartJS.defaults.global.responsive = true;
        ChartJS.defaults.global.maintainAspectRatio = false;

        ChartJS.plugins.register({
            beforeDraw: function(chartInstance) {
              var ctx = chartInstance.chart.ctx;
              ctx.fillStyle = "white";
              ctx.fillRect(0, 0, chartInstance.chart.width, chartInstance.chart.height);
            },
            afterDraw: function (chartInstance) {
                var ctx = chartInstance.chart.ctx;
                ctx.textBaseline = "middle";
                ctx.fillStyle = 'black'
                ctx.font = "15px Roboto";
                ctx.textAlign = "center";

                for (var li = 0; li < data.length; li++)
                {
                    var labelText = data[li];
                    var x = chartInstance.chart.controller.data.datasets[0]._meta[0].data[li]._view.x;
                    var y = Math.min(chartInstance.chart.controller.data.datasets[0]._meta[0].data[li]._view.y + 15, height - 40);
                    ctx.fillText(labelText, x, y);
                }
            }
          });
    };

    const configuration = {
        type: 'bar',
        data: {
            labels: ['Rey', 'SL Kylo Ren', 'JM Luke', 'Sith Emperor'],
            datasets: [{
                label: 'Count of Galactic Legends',
                data: data,
                backgroundColor: [
                    'rgba(54, 162, 235, 0.7)',
                    'rgba(255, 99, 132, 0.7)',
                    'rgba(255, 206, 86, 0.7)',
                    'rgba(75, 192, 192, 0.7)'
                ],
                borderColor: [
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 99, 132, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true,
                    }
                }]
            }
        }
    };

    const chartJSNodeCanvas = new ChartJSNodeCanvas({width, height, chartCallback});

    const imageBuffer = await chartJSNodeCanvas.renderToBuffer(configuration);    

    var totalGls = (parseInt(glData[0].rey_count) + parseInt(glData[0].slkr_count) + parseInt(glData[0].jml_count) + parseInt(glData[0].see_count));
    var fileName = "chart-gls-" + allycode + "-" + Date.now() + ".png";
    var message = "Total GLs for guild: " + totalGls.toLocaleString('en');

    return createImageReportResultObj(message, imageBuffer, fileName);
}

async function getChartGacOverTimeForAllyCode(allycode)
{
    var playerData = await utils.getAllPlayerDataByAllyCode(allycode);

    var labels = new Array();
    var data = new Array();

    if (playerData.length == 0)
        throw "No data for allycode " + allycode;

    playerData.sort((a,b) => a.timestamp - b.timestamp);

    // grab their most recent recorded name
    var playerName = playerData[playerData.length - 1].player_name;
    
    var gacAveragePerDay = "Not enough data.";
    
    if (playerData.length > 1) {
        var daysOfData = Math.round((playerData[playerData.length-1].timestamp.getTime() - playerData[0].timestamp.getTime())/utils.ONE_DAY_MILLIS);
        var gacChange = playerData[playerData.length-1].gac_lifetime_score - playerData[0].gac_lifetime_score;
        gacAveragePerDay = Math.round(gacChange / daysOfData);   
    }

    for (var i = 0; i < playerData.length; i++) {
        var ts = playerData[i].timestamp;
        var label = monthShortNames[ts.getUTCMonth()] + " " + ts.getUTCDate();
        labels.push(label);
        data.push(playerData[i].gac_lifetime_score);
    }

    const width = 600;
    const height = 300;
    const chartCallback = (ChartJS) => {
        ChartJS.defaults.global.responsive = true;
        ChartJS.defaults.global.maintainAspectRatio = false;

        ChartJS.plugins.register({
            beforeDraw: function(chartInstance) {
              var ctx = chartInstance.chart.ctx;
              ctx.fillStyle = "white";
              ctx.fillRect(0, 0, chartInstance.chart.width, chartInstance.chart.height);
            }
          });
    };

    const configuration = {
        type: 'line',
        data: {
            datasets: [{
                backgroundColor: 'rgba(54, 162, 235, 0.2)',
                cubicInterpolationMode: 'monotone',
                label: "GAC Lifetime Score over time for " + playerName + " (" + allycode + ")",
                data: data
            }],
            labels: labels
        }
    };
    
    const chartJSNodeCanvas = new ChartJSNodeCanvas({width, height, chartCallback});

    const imageBuffer = await chartJSNodeCanvas.renderToBuffer(configuration);    

    var fileName = "chart-gac-" + allycode + "-" + Date.now() + ".png";
    var message = "Average GAC Lifetime increase per day: " + gacAveragePerDay.toLocaleString('en');

    return createImageReportResultObj(message, imageBuffer, fileName);
}

async function getChartGpOverTimeForAllyCode(allycode, type = null)
{
    var playerData = await utils.getAllPlayerDataByAllyCode(allycode);

    var labels = new Array();
    var gpData = new Array();
    var gpSquadData = new Array();
    var gpFleetData = new Array();

    if (playerData.length == 0)
        throw "No data for allycode " + allycode;

    playerData.sort((a,b) => a.timestamp - b.timestamp);

    // grab their most recent recorded name
    var playerName = playerData[playerData.length - 1].player_name;

    var gpAveragePerDay = "Not enough data.";
    var gpSquadAveragePerDay = "Not enough data.";
    var gpFleetAveragePerDay = "Not enough data.";
    
    if (playerData.length > 1) {
        var daysOfData = Math.round((playerData[playerData.length-1].timestamp.getTime() - playerData[0].timestamp.getTime())/utils.ONE_DAY_MILLIS);
        var gpChange = playerData[playerData.length-1].gp - playerData[0].gp;
        gpAveragePerDay = Math.round(gpChange / daysOfData);

        var gpSquadChange = playerData[playerData.length-1].gp_squad - playerData[0].gp_squad;
        gpSquadAveragePerDay = Math.round(gpSquadChange / daysOfData);   

        var gpFleetChange = playerData[playerData.length-1].gp_fleet - playerData[0].gp_fleet;
        gpFleetAveragePerDay = Math.round(gpFleetChange / daysOfData);   
    }

    for (var i = 0; i < playerData.length; i++) {
        var ts = playerData[i].timestamp;
        var label = monthShortNames[ts.getUTCMonth()] + " " + ts.getUTCDate();
        labels.push(label);
        gpData.push(playerData[i].gp);
        gpSquadData.push(playerData[i].gp_squad);
        gpFleetData.push(playerData[i].gp_fleet);
    }

    const width = 600;
    const height = 300;
    const chartCallback = (ChartJS) => {
        ChartJS.defaults.global.responsive = true;
        ChartJS.defaults.global.maintainAspectRatio = false;

        ChartJS.plugins.register({
            beforeDraw: function(chartInstance) {
              var ctx = chartInstance.chart.ctx;
              ctx.fillStyle = "white";
              ctx.fillRect(0, 0, chartInstance.chart.width, chartInstance.chart.height);
            }
          });
    };

    var datasets = new Array();
    if (!type || type == "t")
    {
        datasets.push({
            backgroundColor: 'rgba(0, 0, 0, 0)',
            borderColor: 'rgba(54, 162, 235, 0.5)',
            cubicInterpolationMode: 'monotone',
            label: "Total GP",
            data: gpData
        });
    }
    if (!type || type == "s")
    {
        datasets.push({
            backgroundColor: 'rgba(0, 0, 0, 0)',
            borderColor: 'rgba(14, 180, 5, 0.5)',
            cubicInterpolationMode: 'monotone',
            label: "Squad GP",
            data: gpSquadData
        });
    }
    if (!type || type == "f") 
    {
        datasets.push({
            backgroundColor: 'rgba(0, 0, 0, 0)',
            borderColor: 'rgba(180, 25, 50, 0.5)',
            cubicInterpolationMode: 'monotone',
            label: "Fleet GP",
            data: gpFleetData
        });
    }

    var titleText = "GP over time for " + playerName + " (" + allycode + ")";
    if (type == "s") titleText = "Squad " + titleText; 
    if (type == "f") titleText = "Fleet " + titleText; 

    const configuration = {
        type: 'line',
        data: {
            datasets: datasets,
            labels: labels
        },
        options: {
            title: {
                display: true,
                align: 'center',
                text: titleText
            }
        }
    };

    const chartJSNodeCanvas = new ChartJSNodeCanvas({width, height, chartCallback});

    const imageBuffer = await chartJSNodeCanvas.renderToBuffer(configuration);    

    var fileName = "chart-gp-" + allycode + "-" + Date.now() + ".png";
    var message = 
        "Average GP increase per day: " + gpAveragePerDay.toLocaleString('en') + "\r\n" + 
        "Average Squad GP increase per day: " + gpSquadAveragePerDay.toLocaleString('en') + "\r\n" + 
        "Average Fleet GP increase per day: " + gpFleetAveragePerDay.toLocaleString('en');

    return createImageReportResultObj(message, imageBuffer, fileName);
}

async function getChartModQualityOverTimeForAllyCode(allycode, type = "")
{
    type = type ? type.toLowerCase() : "";
    var playerData = await utils.getAllPlayerDataByAllyCode(allycode);

    var labels = new Array();
    var data = new Array();

    if (playerData.length == 0)
        throw "No data for allycode " + allycode;

    playerData.sort((a,b) => a.timestamp - b.timestamp);

    // grab their most recent recorded name
    var playerName = playerData[playerData.length - 1].player_name;

    for (var i = 0; i < playerData.length; i++) {
        var ts = playerData[i].timestamp;
        var label = monthShortNames[ts.getUTCMonth()] + " " + ts.getUTCDate();
        labels.push(label);
        if (type == "dsr")
            data.push(playerData[i].mod_quality);
        else
            data.push(playerData[i].mod_quality2)
    }

    const width = 600;
    const height = 300;
    const chartCallback = (ChartJS) => {
        ChartJS.defaults.global.responsive = true;
        ChartJS.defaults.global.maintainAspectRatio = false;

        ChartJS.plugins.register({
            beforeDraw: function(chartInstance) {
              var ctx = chartInstance.chart.ctx;
              ctx.fillStyle = "white";
              ctx.fillRect(0, 0, chartInstance.chart.width, chartInstance.chart.height);
            }
          });
    };

    var chartTitle = "Mod Quality over time for " + playerName + " (" + allycode + "),\r\n";
    if (type == "dsr")
        chartTitle += "calculated as (Number of +15 speeds) / (Squad GP / 100,000)"
    else
        chartTitle += "calculated as (Num. 15+20+25 speeds + 0.5*Num. 6-dot) / (Squad GP / 100,000)"

    const configuration = {
        type: 'line',
        data: {
            datasets: [{
                backgroundColor: 'rgba(54, 162, 235, 0.2)',
                cubicInterpolationMode: 'monotone',
                label: "Mod Quality",
                data: data
            }],
            labels: labels
        },
        options: {
            title: {
                display: true,
                align: 'center',
                text: chartTitle
            }
        }
    };

    const chartJSNodeCanvas = new ChartJSNodeCanvas({width, height, chartCallback});

    const imageBuffer = await chartJSNodeCanvas.renderToBuffer(configuration);    

    var fileName = "chart-mq-" + allycode + "-" + Date.now() + ".png";

    return createImageReportResultObj(null, imageBuffer, fileName);
}

async function getChartGuildPayouts(allycode, type = "")
{
    var guildName = null;
    var payoutsData = null;
    var guildData = await database.db.any("select guild_name from guild where guild_id = (select guild_id from guild_players where allycode = $1)", allycode);

    if (guildData && guildData[0]) {
        guildName = guildData[0].guild_name;
        var payoutsData = await database.db.any(
            `select distinct payout_utc_offset_minutes, count(payout_utc_offset_minutes)
            FROM player_data_static WHERE allycode LIKE ANY 
            (SELECT allycode FROM guild_players WHERE guild_id = (SELECT guild_id FROM guild_players WHERE allycode = $1))
            group by payout_utc_offset_minutes
            order by payout_utc_offset_minutes asc`, allycode
        );
    }

    if (payoutsData == null || payoutsData.length == 0)
    {
        // guild not registered in db, so load from API
        var guildData = await utils.getGuildData(allycode);
        
        var memberAllyCodes = new Array();
        for (var m = 0; m < guildData.roster.length; m++)
        {
            guildName = guildData.name;
            var memberAllyCode = guildData.roster[m].allyCode;
            memberAllyCodes.push(memberAllyCode);
        }

        var playerPayouts = await swapi.fetchPlayer(
            { 
                allycodes: memberAllyCodes,
                project : 
                {
                    "poUTCOffsetMinutes": 1
                }
            }
        );

        playerPayouts.result.sort((a, b) => a.poUTCOffsetMinutes - b.poUTCOffsetMinutes);
        payoutsData = new Array();
        var lastPayoutData = null;
        for (var p = 0; p < playerPayouts.result.length; p++)
        {
            var poData = playerPayouts.result[p].poUTCOffsetMinutes;
            if (lastPayoutData == null || lastPayoutData.payout_utc_offset_minutes != poData)
            {
                lastPayoutData = { payout_utc_offset_minutes: poData, count: 1 }
                payoutsData.push(lastPayoutData);
            } else {
                lastPayoutData.count++;
            }
        }
    }

    var data = new Array();
    var labels = new Array();
    var backgroundColors = new Array();
    var borderColors = new Array();

    const colorLimits = {
        rMin: 20,
        rMax: 100,
        gMin: 0,
        gMax: 230,
        bMin: 100,
        bMax: 200
    }
    for (var i = 0; i < payoutsData.length; i++)
    {
        data.push(payoutsData[i].count);

        var offsetHours = payoutsData[i].payout_utc_offset_minutes / 60;
        var label = "UTC " + (offsetHours >= 0 ? "+" : "-") + Math.abs(offsetHours);
        labels.push(label);

        var baseColorR = Math.floor((colorLimits.rMax - colorLimits.rMin) / payoutsData.length) * i + colorLimits.rMin;
        var baseColorG = Math.floor((colorLimits.gMax - colorLimits.gMin) / payoutsData.length) * i + colorLimits.gMin;
        var baseColorB = Math.floor((colorLimits.bMax - colorLimits.bMin) / payoutsData.length) * i + colorLimits.bMin;
        backgroundColors.push("rgba(" + baseColorR + "," + baseColorG + "," + baseColorB + ", 0.7)");
        borderColors.push("rgba(" + baseColorR + "," + baseColorG + "," + baseColorB + ", 1)");
        
    }

    const width = 40*data.length;
    const height = 500;
    const chartCallback = (ChartJS) => {
        ChartJS.defaults.global.responsive = true;
        ChartJS.defaults.global.maintainAspectRatio = false;

        ChartJS.plugins.register({
            beforeDraw: function(chartInstance) {
              var ctx = chartInstance.chart.ctx;
              ctx.fillStyle = "white";
              ctx.fillRect(0, 0, chartInstance.chart.width, chartInstance.chart.height);
            },
            afterDraw: function (chartInstance) {
                var ctx = chartInstance.chart.ctx;
                ctx.textBaseline = "middle";
                ctx.fillStyle = 'black'
                ctx.font = "15px Roboto";
                ctx.textAlign = "center";

                for (var li = 0; li < data.length; li++)
                {
                    var labelText = data[li];
                    var x = chartInstance.chart.controller.data.datasets[0]._meta[0].data[li]._view.x;
                    var y = Math.min(chartInstance.chart.controller.data.datasets[0]._meta[0].data[li]._view.y + 15, height - 40);
                    ctx.fillText(labelText, x, y);
                }
            }
          });
    };

    const configuration = {
        type: 'bar',
        data: {
            labels: labels,
            datasets: [{

                label: 'Count',
                data: data,
                backgroundColor: backgroundColors,
                borderColor: borderColors,
                borderWidth: 1
            }]
        },
        options: {
            title: {
                display: true,
                align: 'center',
                text: "Payout Diversity for " + guildName
            },
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true,
                    }
                }]
            }
        }
    };

    const chartJSNodeCanvas = new ChartJSNodeCanvas({width, height, chartCallback});

    const imageBuffer = await chartJSNodeCanvas.renderToBuffer(configuration);    

    var message = "Payouts for guild **" + guildName + "**, grouped by time (UTC)";
    var fileName = "chart-guild-payouts-" + allycode + "-" + Date.now() + ".png";

    return createImageReportResultObj(message, imageBuffer, fileName);
}