const fs = require('fs');
const path = require('path');
const { exec } = require('child_process');


const jobsFolderPath = path.join(__dirname, 'jobs');
const filesToExclude = [
  'registerGuild.js',
  'populateComlinkGuildIDs.js', 
  'getInquisitorsDataAsDelimitedFile.js', 
  'clearOldCachedData.js', 
  'brain-test.js', 
  'brain-populateTrainingData.js', 
  'boss-test.js',
  'refreshPriorityGuildData.js',
  'addRaidTeamData.js',
  'addTbCombatData.js',
  'addLastActivityTimeToSRResults.js',
  'populateDatacronEntriesInDatabase.js',
  'registerLeaderboardGuilds.js'
]; // List the files you want to exclude

function runScriptsSequentially(files) {
  const runNextScript = (index) => {
    if (index >= files.length) {
      console.log('All scripts have been executed.');
      return;
    }

    const filePath = path.join(jobsFolderPath, files[index]);
    console.log('Running:', filePath);

    const child = exec(`node "${filePath}"`, (error) => {
      if (error) {
        console.error('Error running', filePath, error);
      }

      runNextScript(index + 1);
    });

    child.stdout.on('data', (data) => {
      console.log(data);
    });

    child.stderr.on('data', (data) => {
      console.error(data);
    });
  };

  runNextScript(0);
}

fs.readdir(jobsFolderPath, (err, files) => {
  if (err) {
    console.error('Error reading folder:', err);
    return;
  }

  const jsFiles = files.filter(file => file.endsWith('.js') && !filesToExclude.includes(file));

  runScriptsSequentially(jsFiles);
});