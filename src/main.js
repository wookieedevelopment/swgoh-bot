const fs = require('fs');
require('dotenv').config()
const { Client, Events, GatewayIntentBits, Collection } = require('discord.js');
const { registerFont } = require('canvas');
const { CLIENT } = require("./utils/discordClient")

registerFont("./src/font/NotoSansSC-Regular.otf", { family: "Noto Sans SC" });
registerFont("./src/font/NotoSans-Regular.ttf", { family: "Noto Sans" });
registerFont("./src/font/NotoSans-ExtraBold.ttf", { family: "Noto Sans Extra Bold" });
registerFont("./src/font/NotoSansArmenian-Regular.ttf", { family: "Noto Sans Armenian" });
registerFont("./src/font/NotoSansThai-Regular.ttf", { family: "Noto Sans Thai" });
registerFont("./src/font/NotoSansArabic-Regular.ttf", { family: "Noto Sans Arabic" });
registerFont("./src/font/NotoSansKR-Regular.otf", { family: "Noto Sans Korean" });

// const client = new Client({ intents: [GatewayIntentBits.Guilds, GatewayIntentBits.GuildMessages] });

CLIENT.commands = new Collection();
const commandFiles = fs.readdirSync(`${__dirname}/commands`).filter(file => file.endsWith('.js'));

for (const file of commandFiles) {
	const command = require(`${__dirname}/commands/${file}`);
	// Set a new item in the Collection
	// With the key as the command name and the value as the exported module
	CLIENT.commands.set(command.data.name, command);
}

const eventFiles = fs.readdirSync(__dirname + '/events').filter(file => file.endsWith('.js'));

for (const file of eventFiles) {
	const event = require(`${__dirname}/events/${file}`);
	if (event.once) {
		CLIENT.once(event.name, async (...args) => { await event.execute(...args) });
	} else {
		CLIENT.on(event.name, async (...args) => { await event.execute(...args) });
	}
}

//console.log(`Server count: ${client.guilds.cache.size}.`)
CLIENT.login(process.env.BOT_TOKEN);