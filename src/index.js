const { ShardingManager } = require('discord.js');
require('dotenv').config();
const { logger, formatError } = require('./utils/log');
const statCalc = require("./utils/stats/stats");

const manager = new ShardingManager('./src/main.js', { 
    // for ShardingManager options see:
    // https://discord.js.org/#/docs/main/stable/class/ShardingManager
    totalShards: "auto",
    token: (process.env.BOT_TOKEN)
});

manager.on('shardCreate', shard => 
{
	console.log(`Launched shard ${shard.id}`);
	
	// statCalc.initCalc().then(() => logger.info("Initialized Stat Calc"));
	// queueUtils.start().then(() => logger.info("Started queue."));
});

manager.spawn({
	timeout: 60000
})
	.then(shards => {
		const promises = [
			manager.fetchClientValues('guilds.cache.size'),
			manager.fetchClientValues('user.tag')
		];

		Promise.all(promises)
			.then(results => {
				logger.info(
`Ready! Logged in as ${results[1][0]}
Server count: ${results[0].reduce((acc, guildCount) => acc + guildCount, 0)}.`
				)
			})
			.catch(console.error);

		statCalc.listenForUpdates();

		// statCalc.initCalc().then(() => statCalc.listenForUpdates());
		// shards.forEach(shard => {
		// 	shard.on('message', message => {
		// 		console.log(`Shard[${shard.id}] : ${message._eval} : ${message._result}`);
		// 	});
		// });
	})
	.catch(console.error);