
// configure database connection
const { logger } = require('./utils/log');

const initOptions = {
    // initialization options;
};

const dbConfig = {
    "host": process.env.DB_HOST,
    "port": process.env.DB_PORT,
    "database": process.env.DB_NAME,
    "user": process.env.DB_USERNAME,
    "password": process.env.DB_PASSWORD
    // "ssl": process.env.DB_SSL
}

const pgp = require('pg-promise')(initOptions);

const db = pgp(dbConfig);

db.query("SELECT NOW() as now").then(data => { logger.info("Testing database connection. Now is: " + data[0].now); });

exports.db = db;
exports.pgp = pgp;

exports.columnSets = {
    cheatingQueriesCS: new pgp.helpers.ColumnSet(
        ['timestamp', 
        'allycode'
        ], 
        {table: 'cheating_queries'}),
    userGuildRoleCS: new pgp.helpers.ColumnSet(
        ['discord_id', 
        'guild_id',
        'guild_admin',
        'guild_bot_admin'
        ], 
        {table: 'user_guild_role'}),
    guildCS: new pgp.helpers.ColumnSet(
        ['comlink_guild_id', 
        'guild_name',
        'track_data',
        'description',
        'priority'
        ], 
        {table: 'guild'}),
    guildPlayersCS: new pgp.helpers.ColumnSet(
        ['guild_id', 
        'player_name', 
        'allycode', 
        'member_type', 
        'player_id',
        'guild_join_time',
        'last_checked_dt'], 
        {table: 'guild_players'}),
    datacronCS: new pgp.helpers.ColumnSet(
        ['ability_id',
        'affix_id',
        'set_id',
        'tags',
        'target_rule',
        'alignment',
        'faction',
        'character',
        'icon_asset_name'],
        { table: 'datacron' }),
    datacronTemplateCS: new pgp.helpers.ColumnSet(
        ['template_id',
        'affix_id',
        'set_id',
        'expiration_dt'],
        { table: 'datacron_template' }
    ),
    guildRaidScoreCS: new pgp.helpers.ColumnSet(
        ['comlink_guild_id',
        'raid_id',
        'end_time_seconds',
        'outcome',
        'progress',
        'campaign_mission_id',
        'campaign_node_difficulty'],
        { table: 'guild_raid_score' }
    ),
    playerRaidScoreCS: new pgp.helpers.ColumnSet(
        ['comlink_guild_id',
        'raid_id',
        'end_time_seconds',
        'player_id',
        'score'],
        { table: 'player_raid_score' }
    ),
    warResultCS: new pgp.helpers.ColumnSet(
        ['comlink_guild_id',
        'guild_name',
        'end_time_seconds',
        'score',
        'opponent_score',
        'territory_war_id'],
        { table: 'war_result' }
    ),
    raidTrainingDataCS: new pgp.helpers.ColumnSet(
        ['gp_squad',
        'skill_rating',
        'mod_quality',
        'relics_old_republic',
        'relics_hutt_cartel',
        'relics_mandalorian',
        'relics_tusken',
        'relics_jawa',
        'last_score',
        'raid_end_time_seconds',
        'player_id'],
        { table: 'raid_training_data' }
    ),
    playerDataCS: new pgp.helpers.ColumnSet(
        ['allycode', 
        'player_name', 
        'gp', 
        'gp_fleet',
        'gp_squad',
        'arena_rank', 
        'fleet_rank',
        'gl_count', 
        'mod_quality',
        'mod_quality2',
        'mod_quality_hotbot',
        'mods_six_dot',
        'mods_speed25',
        'mods_speed20',
        'mods_speed15',
        'mods_speed10',
        'mods_offense6',
        'gear',
        'relic',
        'skill_rating',
        'raid_score_estimate',
        'last_activity_time',
        'zetas',
        'omicrons',
        'tb_special_income',
        'datacron_summary',
        'timestamp'], 
        {table: 'player_data'}),
    playerDataStaticCS: new pgp.helpers.ColumnSet(
        ['allycode', 
        'player_name', 
        'gl_rey',
        'gl_slkr',
        'gl_jml',
        'gl_see',
        'gl_jmk',
        'gl_lv',
        'gl_jabba',
        'gl_leia',
        'gl_ahsoka',
        'capital_ships',
        'timestamp',
        'payout_utc_offset_minutes',
        'reek_win'
        ], 
        {table: 'player_data_static'}),
    tbOpsCS: new pgp.helpers.ColumnSet(
        ['comlink_guild_id', 
        'plan_name', 
        'timestamp',
        'phase',
        'operation_req_id',
        'preload',
        'allycode',
        'total_needed',
        'total_available'
        ], 
        {table: 'tb_ops'}),
    playerRosterCS: new pgp.helpers.ColumnSet(
        [
            'allycode',
            'unit_def_id',
            'relic_level',
            'gear_level',
            'rarity',
            'timestamp'
        ],
        {table: 'player_roster'}
    ),
    guildWriteupCS: new pgp.helpers.ColumnSet(
        [
            'guild_id',
            'name',
            'text',
            'short_desc',
            'type'
        ],
        {table: 'guild_writeup'}
    ),
    twExclusionCS: new pgp.helpers.ColumnSet(
        [
            'allycode',
            'guild_id'
        ],
        { table: 'tw_exclusion' }
    ),
    leaderboardDataCS: new pgp.helpers.ColumnSet(
        [
            'leaderboard_id',
            'key',
            'data',
            'rank'
        ],
        { table: 'leaderboard_data' }
    )
}