const { logger, formatError } = require("../../utils/log");
require('dotenv').config({path:__dirname+'/./../../../.env'});
const tbCommon = require("../../commands/tb/common");
const raidCommon = require("../../commands/raid/raidCommon");
var database = require("../../database");
const _ = require("lodash");
const NodeCache = require('node-cache');

const guildDataCache = new NodeCache({ useClones: false });
const playerDataCache = new NodeCache({ useClones: false });

const ALL_GUILDS_KEY = "ALL_GUILDS";
const ALL_PLAYERS_KEY = "ALL_PLAYERS";

const LEADERBOARD_FUNCTION_MAP = {
    GUILD_TB_SPECIAL: regenerateGuildTBSpecialsLeaderboard,
    GUILD_AVG_SKILL_RATING: regenerateGuildAverageSkillRatingLeaderboard,
    GUILD_AVG_25PLUS_SPEED_MODS: regenerateGuildAverage25PlusSpeedMods,
    GUILD_AVG_MOD_QUALITY: regenerateGuildAverageModQuality,
    GUILD_AVG_FLEET_RANK: regenerateGuildAverageFleetRank,
    GUILD_AVG_OMICRONS: regenerateGuildAverageOmicrons,
    GUILD_AVG_LEVEL9_DATACRONS: regenerateGuildAverageLevel9Datacrons,
    GUILD_TOTAL_DATACRON_REROLLS: regenerateGuildTotalDatacronRerolls,
    GUILD_TOTAL_GP: regenerateGuildTotalGalacticPower,
    GUILD_TURNOVER: regenerateGuildTurnover,
    GUILD_RAID_SCORES: regenerateGuildRaidScores,
    GUILD_TW_WIN_RATE_LAST90: regenerateGuildTWWinRate,
    PLAYER_25PLUS_SPEED_MODS: regeneratePlayer25PlusSpeedMods,
    PLAYER_6PLUS_OFFENSE_MODS: regeneratePlayer6PlusOffenseMods,
    PLAYER_RAID_SCORES: regeneratePlayerRaidScores,
}

const MIN_GUILD_MEMBERS = 5;

module.exports = {
    populateLeaderboards: populateLeaderboards
}

async function populateLeaderboards()
{
    const leaderboards = await database.db.any('SELECT leaderboard_id FROM leaderboard WHERE enabled = TRUE');

    for (const lbId of leaderboards)
    {
        await generateLeaderboard(lbId.leaderboard_id);
    }
}

// new Promise(async (resolve, reject) =>
// {
//     const leaderboards = await database.db.any('SELECT leaderboard_id FROM leaderboard WHERE enabled = TRUE');
    
//     for (const lbId of leaderboards)
//     {
//         await generateLeaderboard(lbId.leaderboard_id);
//     }

//     database.pgp.end();

//     resolve();
// }).finally(() => { process.exit(); });

async function loadRecentPlayerData(allycode)
{
    let data = playerDataCache.get(allycode);
    if (data === undefined)
    {
        data = await database.db.oneOrNone(
            `
SELECT *
FROM 
(
	SELECT 
		tb_special_income,
		skill_rating,
		mods_speed25,
		mods_offense6,
		mod_quality2,
		gp,
		omicrons,
		fleet_rank,
		datacron_summary
	FROM player_data
	WHERE allycode = $1
	ORDER BY timestamp DESC
	LIMIT 1
) t_data
JOIN 
(
	SELECT 
		player_raid_score.score as raid_score
	FROM player_raid_score 
	LEFT JOIN guild_players USING (player_id)
	WHERE guild_players.allycode = $1
    AND raid_id = $2
	ORDER BY player_raid_score.score DESC 
	limit 1
) t_raid_scores on TRUE;
            `, [allycode, raidCommon.DEFAULT_RAID.raidId]
        );

        playerDataCache.set(allycode, data);
    }
    return data;
}

async function loadRecentPlayerDataForGuild(comlink_guild_id)
{
    let data = guildDataCache.get(comlink_guild_id);
    
    if (data === undefined) 
    {
        writeToLog(`Loading player data for ${comlink_guild_id}`);
        data = await database.db.any(
        `
SELECT t_limited.*, t_raid_scores.*
FROM (
SELECT DISTINCT allycode
FROM player_Data
where allycode in (
    select allycode 
    from guild_players
    left join guild on (guild_players.guild_id = guild.guild_id) 
    where comlink_guild_id = $1
)
) t_groups
JOIN LATERAL (
SELECT 
    t_all.allycode,
    tb_special_income,
    skill_rating,
    mods_speed25,
    mods_offense6,
    mod_quality2,
    gp,
    omicrons,
    fleet_rank,
    datacron_summary,
    guild_join_time
FROM player_data t_all
LEFT JOIN guild_players USING (allycode)
LEFT JOIN player_raid_score ON player_raid_score.player_id = guild_players.player_id
WHERE t_all.allycode = t_groups.allycode
ORDER BY t_all.allycode,t_all.timestamp desc
limit 1
) t_limited ON true
LEFT JOIN LATERAL (
    SELECT 
        player_raid_score.score as raid_score
    FROM player_raid_score 
    LEFT JOIN guild_players USING (player_id)
    WHERE guild_players.allycode = t_groups.allycode
    AND raid_id = $2
    ORDER BY player_raid_score.score DESC 
    limit 1
) t_raid_scores ON true;
        `, [comlink_guild_id, raidCommon.DEFAULT_RAID.raidId]);

        // pre-cache player data, too
        for (let p of data)
        {
            playerDataCache.set(p.allycode,p);
        }

        guildDataCache.set(comlink_guild_id, data);
    }

    return data;
}


async function generateLeaderboard(leaderboardId)
{
    logger.info(`[Leaderboard: ${leaderboardId}]: Start Generating`);

    const leaderboardEntries = await LEADERBOARD_FUNCTION_MAP[leaderboardId](leaderboardId);

    await updateLeaderboardInDatabase(leaderboardId, leaderboardEntries);

    logger.info(`[Leaderboard: ${leaderboardId}]: End Generating`);
}

async function updateLeaderboardInDatabase(leaderboardId, leaderboardEntries)
{
    let guildInsert;

    if (leaderboardEntries?.length > 0)
    {
        guildInsert = database.pgp.helpers.insert(leaderboardEntries, database.columnSets.leaderboardDataCS) +
        ' ON CONFLICT(leaderboard_id, key) DO UPDATE SET ' +
        database.columnSets.leaderboardDataCS.assignColumns({from: 'EXCLUDED', skip: ['leaderboard_id', 'key']});
    }
    else
    {
        guildInsert = "";
    }

    const clearRanks = `UPDATE leaderboard_data SET rank = NULL where leaderboard_id = $1; `;
    const deleteUnranked = `; DELETE FROM leaderboard_data WHERE rank is NULL and leaderboard_id = $1;`

    
    for (let attempt = 1; attempt <= 5; attempt++)
    {
        try {
            await database.db.any(clearRanks + guildInsert + deleteUnranked, [leaderboardId]);
            return;
        }
        catch (e) {
            logger.error(`Failed to save data for leaderboard ${leaderboardId} ${formatError(e)}`);
        }

        await tools.sleep(20 * (attempt ** 2));
    }
}

async function getPlayerList()
{
    let data = playerDataCache.get(ALL_PLAYERS_KEY);
    if (data === undefined)
    {
        data = await database.db.any("SELECT allycode,player_name FROM player_data_static");
        playerDataCache.set(ALL_PLAYERS_KEY, data);
    }

    return data;
}

const NOW_SECONDS = Math.round(new Date().getTime()/1000);
const NINETY_DAYS_IN_SECONDS = 90*24*60*60;

async function getGuildsList()
{
    let data = guildDataCache.get(ALL_GUILDS_KEY);
    
    if (data === undefined) 
    {        
        data = await database.db.any(
            `
            select * from
(SELECT comlink_guild_id, guild_name 
FROM guild) t_guilds
LEFT JOIN LATERAL
(
SELECT 
	SUM(player_raid_score.score)::int as raid_score
FROM guild_raid_score
JOIN player_raid_score USING (comlink_guild_id, end_time_seconds)
WHERE guild_raid_score.comlink_guild_id = t_guilds.comlink_guild_id AND guild_raid_score.raid_id = $1
GROUP BY guild_raid_score.end_time_seconds
ORDER BY raid_score DESC
LIMIT 1
) t_raid_scores on true
 LEFT JOIN LATERAL
 (
    SELECT COUNT(war_result_id)::int as wars_won
    FROM war_result 
    WHERE war_result.comlink_guild_id = t_guilds.comlink_guild_id
    AND score > opponent_score
    AND end_time_seconds >= $2
 ) t_war_wins on true
LEFT JOIN LATERAL
(
    SELECT COUNT(war_result_id)::int as wars_total
    FROM war_result 
    WHERE war_result.comlink_guild_id = t_guilds.comlink_guild_id
    AND end_time_seconds >= $2
) t_wars_total on true;
            `, [raidCommon.DEFAULT_RAID.raidId, NOW_SECONDS - NINETY_DAYS_IN_SECONDS]);


        for (let g of data)
        {
            try {
                await calculateGuildTotalsFromPlayerData(g);
            }
            catch (e)
            {
                writeToLog(`Failed calculateGuildTotalsFromPlayerData for guild ${g.comlink_guild_id}:\n${formatError(e)}`);
            }
        }
        guildDataCache.set(ALL_GUILDS_KEY, data);
    }
    return data;
}

async function calculateGuildTotalsFromPlayerData(guildObject)
{
    const data = await loadRecentPlayerDataForGuild(guildObject.comlink_guild_id);
    let guildGP = 0;
    let members = data.length;
    
    guildObject.membersCount = members;
    let tbRewards = {
        getTotal: 0,
        get1Total: 0,
        get2Total: 0,
        get3Total: 0
    };
    guildObject.tbRewards = tbRewards;

    if (members === 0) return;
        
    let totalFleetRank = 0, playersEligibleForFleet = 0;     
    let totalOmicrons = 0, totalL9Datacrons = 0, totalRerolls = 0;  
    let lengthOfMembershipSeconds = 0, membersWithGuildJoinTime = 0; 
    let totalModQuality = 0, totalModsSpeed25 = 0, totalSkillRating = 0;

    const rewardKeysToCount = [
        tbCommon.TB_REWARDS.GET1.key, 
        tbCommon.TB_REWARDS.GET2.key, 
        tbCommon.TB_REWARDS.GET3.key];
    
    for (let playerData of data)
    {
        totalSkillRating += (playerData.skill_rating ?? 0);
        totalOmicrons += (playerData.omicrons ?? 0);
        totalL9Datacrons += (playerData.datacron_summary?.level9 ?? 0);
        totalRerolls += (playerData.datacron_summary?.rerolls ?? 0);
        guildGP += (playerData.gp ?? 0);
        totalModsSpeed25 += (playerData.mods_speed25 ?? 0);

        try {
            let mq = parseFloat(playerData.mod_quality2);
            if (_.isFinite(mq)) 
            {
                totalModQuality += mq;
            }
        } catch {}

        if (playerData.guild_join_time) 
        {
            membersWithGuildJoinTime++;
            lengthOfMembershipSeconds += (NOW_SECONDS - playerData.guild_join_time);
        }

        if (playerData.fleet_rank) {
            totalFleetRank += playerData.fleet_rank;
            playersEligibleForFleet++;
        }

        for (let rk of rewardKeysToCount)
        {
            if (!playerData.tb_special_income) continue;
            const val = playerData.tb_special_income[rk];
            if (val == null || !_.isInteger(val)) continue;

            switch (rk)
            {
                case tbCommon.TB_REWARDS.GET1.key: tbRewards.get1Total += val;
                case tbCommon.TB_REWARDS.GET2.key: tbRewards.get2Total += val;
                case tbCommon.TB_REWARDS.GET3.key: tbRewards.get3Total += val;
            }

            tbRewards.getTotal += val;
        }                
    }

    guildObject.averageFleetRank = _.round(totalFleetRank/playersEligibleForFleet, 2);
    guildObject.averageSkillRating = _.round(totalSkillRating/members, 2);
    guildObject.totalGP = guildGP;
    guildObject.averageOmicrons = _.round(totalOmicrons/members, 2);
    guildObject.averageDC9 = _.round(totalL9Datacrons/members);
    guildObject.totalRerolls = totalRerolls;
    guildObject.averageLengthOfMembershipSeconds = _.round(lengthOfMembershipSeconds/members);
    guildObject.averageMQ  = _.round(totalModQuality/members, 2);
    guildObject.averageModsSpeed25  = _.round(totalModsSpeed25/members);
}

async function regenerateGuildAverageModQuality(leaderboardId)
{
    const guildData = await getGuildsList();

    let leaderboardEntries = [];

    for (const guild of guildData)
    {
        logGuildStart(leaderboardEntries.length + 1, guild.comlink_guild_id);
        if (guild.members < MIN_GUILD_MEMBERS) continue;

        let entry = createLeaderboardEntry(leaderboardId, guild.comlink_guild_id, { value: guild.averageMQ ?? 0, gp: guild.totalGP, name: guild.guild_name }, null);
        leaderboardEntries.push(entry);
    }

    leaderboardEntries.sort((a, b) => b.data.value - a.data.value);

    // assign ranks
    leaderboardEntries.forEach( (v, ix) => { v.rank = (ix + 1) });

    return leaderboardEntries;
}

async function regenerateGuildAverage25PlusSpeedMods(leaderboardId)
{
    const guildData = await getGuildsList();

    let leaderboardEntries = [];

    for (const guild of guildData)
    {
        logGuildStart(leaderboardEntries.length + 1, guild.comlink_guild_id);
        if (guild.membersCount < MIN_GUILD_MEMBERS) continue;

        let entry = createLeaderboardEntry(leaderboardId, guild.comlink_guild_id, { value: guild.averageModsSpeed25 ?? 0, gp: guild.totalGP, name: guild.guild_name }, null);
        leaderboardEntries.push(entry);
    }

    leaderboardEntries.sort((a, b) => (b.data.value ?? 0) - (a.data.value ?? 0));

    // assign ranks
    leaderboardEntries.forEach( (v, ix) => { v.rank = (ix + 1) });

    return leaderboardEntries;
}


async function regenerateGuildAverageSkillRatingLeaderboard(leaderboardId)
{
    const guildData = await getGuildsList();

    let leaderboardEntries = [];

    for (const guild of guildData)
    {
        logGuildStart(leaderboardEntries.length + 1, guild.comlink_guild_id);
        if (guild.membersCount < MIN_GUILD_MEMBERS) continue; 

        let entry = createLeaderboardEntry(leaderboardId, guild.comlink_guild_id, { value: guild.averageSkillRating ?? 0, gp: guild.totalGP, name: guild.guild_name }, null);
        leaderboardEntries.push(entry);
    }

    leaderboardEntries.sort((a, b) => b.data.value - a.data.value);

    // assign ranks
    leaderboardEntries.forEach( (v, ix) => { v.rank = (ix + 1) });

    return leaderboardEntries;
}

async function regenerateGuildTBSpecialsLeaderboard(leaderboardId)
{
    const guildData = await getGuildsList();

    let leaderboardEntries = [];

    for (const guild of guildData)
    {
        logGuildStart(leaderboardEntries.length + 1, guild.comlink_guild_id);
        if (guild.membersCount < MIN_GUILD_MEMBERS) continue; 

        let entry = createLeaderboardEntry(leaderboardId, guild.comlink_guild_id, 
            { 
                value: guild.tbRewards.getTotal,
                value2: guild.tbRewards.get1Total,
                value3: guild.tbRewards.get2Total,
                value4: guild.tbRewards.get3Total, 
                gp: guild.totalGP,
                name: guild.guild_name
            }, null);
        leaderboardEntries.push(entry);
    }

    leaderboardEntries.sort((a, b) => b.data.value - a.data.value);

    // assign ranks
    leaderboardEntries.forEach( (v, ix) => { v.rank = (ix + 1) });

    return leaderboardEntries;
}

async function regenerateGuildAverageFleetRank(leaderboardId)
{
    const guildData = await getGuildsList();

    let leaderboardEntries = [];

    for (const guild of guildData)
    {
        logGuildStart(leaderboardEntries.length + 1, guild.comlink_guild_id);

        if (guild.membersCount < MIN_GUILD_MEMBERS) continue; 

        let entry = createLeaderboardEntry(leaderboardId, guild.comlink_guild_id, { value: guild.averageFleetRank ?? 0, gp: guild.totalGP, name: guild.guild_name }, null);
        leaderboardEntries.push(entry);
    }

    // smallest first
    leaderboardEntries.sort((a, b) => a.data.value - b.data.value);

    // assign ranks
    leaderboardEntries.forEach( (v, ix) => { v.rank = (ix + 1) });

    return leaderboardEntries;
}

async function regenerateGuildAverageOmicrons(leaderboardId)
{
    const guildData = await getGuildsList();

    let leaderboardEntries = [];

    for (const guild of guildData)
    {
        logGuildStart(leaderboardEntries.length + 1, guild.comlink_guild_id);
        if (guild.membersCount < MIN_GUILD_MEMBERS) continue; 

        let entry = createLeaderboardEntry(leaderboardId, guild.comlink_guild_id, { value: guild.averageOmicrons ?? 0, gp: guild.totalGP, name: guild.guild_name }, null);
        leaderboardEntries.push(entry);
    }

    // most first
    leaderboardEntries.sort((a, b) => b.data.value - a.data.value);

    // assign ranks
    leaderboardEntries.forEach( (v, ix) => { v.rank = (ix + 1) });

    return leaderboardEntries;
}

async function regenerateGuildAverageLevel9Datacrons(leaderboardId)
{
    const guildData = await getGuildsList();

    let leaderboardEntries = [];

    for (const guild of guildData)
    {
        logGuildStart(leaderboardEntries.length + 1, guild.comlink_guild_id);
        if (guild.membersCount < MIN_GUILD_MEMBERS) continue; 

        let entry = createLeaderboardEntry(leaderboardId, guild.comlink_guild_id, { value: guild.averageDC9 ?? 0, gp: guild.totalGP, name: guild.guild_name }, null);
        leaderboardEntries.push(entry);
    }

    // most first
    leaderboardEntries.sort((a, b) => b.data.value - a.data.value);

    // assign ranks
    leaderboardEntries.forEach( (v, ix) => { v.rank = (ix + 1) });

    return leaderboardEntries;
}

async function regenerateGuildTotalDatacronRerolls(leaderboardId)
{
    const guildData = await getGuildsList();

    let leaderboardEntries = [];

    for (const guild of guildData)
    {
        logGuildStart(leaderboardEntries.length + 1, guild.comlink_guild_id);
        if (guild.membersCount < MIN_GUILD_MEMBERS) continue; 

        let entry = createLeaderboardEntry(leaderboardId, guild.comlink_guild_id, { value: guild.totalRerolls ?? 0, gp: guild.totalGP, name: guild.guild_name }, null);
        leaderboardEntries.push(entry);
    }

    // most first
    leaderboardEntries.sort((a, b) => b.data.value - a.data.value);

    // assign ranks
    leaderboardEntries.forEach( (v, ix) => { v.rank = (ix + 1) });

    return leaderboardEntries;
}

async function regenerateGuildTotalGalacticPower(leaderboardId)
{
    const guildData = await getGuildsList();

    let leaderboardEntries = [];

    for (const guild of guildData)
    {
        logGuildStart(leaderboardEntries.length + 1, guild.comlink_guild_id);
        if (guild.membersCount < 1) continue; 

        let avgGP = _.round(guild.totalGP/guild.membersCount);
        let entry = createLeaderboardEntry(leaderboardId, guild.comlink_guild_id, { value: avgGP, gp: guild.totalGP, name: guild.guild_name }, null);
        leaderboardEntries.push(entry);
    }

    // most first
    leaderboardEntries.sort((a, b) => b.data.gp - a.data.gp);

    // assign ranks
    leaderboardEntries.forEach( (v, ix) => { v.rank = (ix + 1) });

    return leaderboardEntries;
}

async function regenerateGuildTurnover(leaderboardId)
{
    const guildData = await getGuildsList();
    const nowSeconds = Math.round(Date.now()/1000);

    let leaderboardEntries = [];

    for (const guild of guildData)
    {
        logGuildStart(leaderboardEntries.length + 1, guild.comlink_guild_id);

        // 10 members minimum
        if (guild.membersCount < 10) continue;

        let entry = createLeaderboardEntry(leaderboardId, guild.comlink_guild_id, { value: guild.averageLengthOfMembershipSeconds, gp: guild.totalGP, name: guild.guild_name }, null);
        leaderboardEntries.push(entry);    
    }

    // most first
    leaderboardEntries.sort((a, b) => b.data.value - a.data.value);

    // assign ranks
    leaderboardEntries.forEach( (v, ix) => { v.rank = (ix + 1) });

    return leaderboardEntries;
}

async function regenerateGuildRaidScores(leaderboardId)
{
    const guildData = await getGuildsList();

    let leaderboardEntries = [];

    for (const guild of guildData)
    {
        logGuildStart(leaderboardEntries.length + 1, guild.comlink_guild_id);

        if (!guild.raid_score || guild.membersCount < 10) continue;
        
        let entry = createLeaderboardEntry(leaderboardId, guild.comlink_guild_id, { value: guild.raid_score, gp: guild.totalGP, name: guild.guild_name }, null);
        leaderboardEntries.push(entry);    
    }

    // most first
    leaderboardEntries.sort((a, b) => b.data.value - a.data.value);

    // assign ranks
    leaderboardEntries.forEach( (v, ix) => { v.rank = (ix + 1) });

    return leaderboardEntries;
}

async function regenerateGuildTWWinRate(leaderboardId)
{
    const guildData = await getGuildsList();

    let leaderboardEntries = [];

    for (const guild of guildData)
    {
        logGuildStart(leaderboardEntries.length + 1, guild.comlink_guild_id);

        if (!guild.wars_total || guild.membersCount < MIN_GUILD_MEMBERS) continue;

        let winRate = (guild.wars_won ?? 0) / guild.wars_total;
        let entry = createLeaderboardEntry(leaderboardId, guild.comlink_guild_id, { value: winRate, warsWon: guild.wars_won ?? 0, warsTotal: guild.wars_total, gp: guild.totalGP, name: guild.guild_name }, null);
        leaderboardEntries.push(entry);    
    }

    // most first
    leaderboardEntries.sort((a, b) => b.data.value - a.data.value);

    // assign ranks
    leaderboardEntries.forEach( (v, ix) => { v.rank = (ix + 1) });

    return leaderboardEntries;
}

async function regeneratePlayerRaidScores(leaderboardId)
{
    const playerList = await getPlayerList();

    let leaderboardEntries = [];

    for (const player of playerList)
    {
        logGuildStart(leaderboardEntries.length + 1, `${player.player_name} (${player.allycode})`);
        const data = await loadRecentPlayerData(player.allycode);

        if (!data || !data.raid_score) continue;

        let entry = createLeaderboardEntry(leaderboardId, player.allycode, { value: data.raid_score, name: player.player_name });
        leaderboardEntries.push(entry);
    }

    // most first
    leaderboardEntries.sort((a, b) => b.data.value - a.data.value);

    // assign ranks
    leaderboardEntries.forEach( (v, ix) => { v.rank = (ix + 1) });

    return leaderboardEntries;
}

async function regeneratePlayer25PlusSpeedMods(leaderboardId)
{
    const playerList = await getPlayerList();

    let leaderboardEntries = [];

    for (const player of playerList)
    {
        logGuildStart(leaderboardEntries.length + 1, `${player.player_name} (${player.allycode})`);
        const data = await loadRecentPlayerData(player.allycode);

        if (!data) continue;

        let entry = createLeaderboardEntry(leaderboardId, player.allycode, { value: data.mods_speed25, name: player.player_name });
        leaderboardEntries.push(entry);
    }

    // most first
    leaderboardEntries.sort((a, b) => b.data.value - a.data.value);

    // assign ranks
    leaderboardEntries.forEach( (v, ix) => { v.rank = (ix + 1) });

    return leaderboardEntries;
}

async function regeneratePlayer6PlusOffenseMods(leaderboardId)
{
    const playerList = await getPlayerList();

    let leaderboardEntries = [];

    for (const player of playerList)
    {
        logGuildStart(leaderboardEntries.length + 1, `${player.player_name} (${player.allycode})`);
        const data = await loadRecentPlayerData(player.allycode);

        if (!data || data.mods_offense6 == null) continue;

        let entry = createLeaderboardEntry(leaderboardId, player.allycode, { value: data.mods_offense6, name: player.player_name });
        leaderboardEntries.push(entry);
    }

    // most first
    leaderboardEntries.sort((a, b) => b.data.value - a.data.value);

    // assign ranks
    leaderboardEntries.forEach( (v, ix) => { v.rank = (ix + 1) });

    return leaderboardEntries;
}

function createLeaderboardEntry(leaderboardId, key, data, rank)
{
    return {
        leaderboard_id: leaderboardId,
        key: key,
        data: data,
        rank: rank
    }
}

function writeToLog(text) { logger.info(text); }

function logGuildStart(number, comlinkGuildId)
{
    writeToLog(`[${number}] ${comlinkGuildId}`);
}