
const { logger, formatError } = require("../../utils/log");
require('dotenv').config({path:__dirname+'/./../../../.env'});
require('dotenv').config({path:__dirname+'/refresh.env'})
var database = require("../../database");
var guildRefresh = require("../../commands/guild/refreshAllGuildData");
const constants = require("../../utils/constants");
const async = require("async");

const REFRESH_DELAY = process.env.PRIORITY_GUILD_REFRESH_DELAY_MILLIS ? parseInt(process.env.PRIORITY_GUILD_REFRESH_DELAY_MILLIS) : 30000;

async.forever(
    function (next)
    {
        database.db.any('SELECT guild_name,comlink_guild_id FROM guild WHERE track_data = TRUE and priority > 0 ORDER BY priority DESC,guild_id')
            .then( async (guilds) => {
                for (let g = 0; g < guilds.length; g++)
                {
                    let guild = guilds[g];
                    console.log(`[${g+1}/${guilds.length}] Refreshing data for ${guild.guild_name}`);
                    
                    try {
                        await guildRefresh.refreshGuildData(guild.comlink_guild_id, constants.CacheTypes.NIGHTLY);
                        console.log("Refreshed guild data for " + guild.guild_name);
                    } catch (e)
                    {
                        console.log("Error refreshing guild data for " + guild.guild_name + ". Details:\r\n" + formatError(e));
                    }
                }
            })
            .catch((err) =>
            {
                console.log(err);
            })
            .finally(() => { setTimeout(next, REFRESH_DELAY);  });
    },
    function (err)
    {
        console.log(err);
    }
)