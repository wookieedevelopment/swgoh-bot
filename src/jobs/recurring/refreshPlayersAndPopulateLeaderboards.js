const populateLeaderboards = require("./populateLeaderboards");
const refreshPlayerData = require("./refreshPlayerData");

const async = require("async");

const REFRESH_DELAY = process.env.PRIORITY_GUILD_REFRESH_DELAY_MILLIS ? parseInt(process.env.PRIORITY_GUILD_REFRESH_DELAY_MILLIS) : 30000;

async.forever(
    function (next)
    {
        refreshPlayerData.refreshPlayerData()
            .then(populateLeaderboards.populateLeaderboards)
            .catch((err) =>
            {
                console.log(err);
            })
            .finally(() => { setTimeout(next, REFRESH_DELAY);  });
    },
    function (err)
    {
        console.log(err);
    }
)