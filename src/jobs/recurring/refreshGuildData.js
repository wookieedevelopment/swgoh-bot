
const { logger, formatError } = require("../../utils/log");
require('dotenv').config({path:__dirname+'/./../../../.env'});
require('dotenv').config({path:__dirname+'/refresh.env'})
var database = require("../../database");
var guildRefresh = require("../../commands/guild/refreshAllGuildData");
var playerUtils = require("../../utils/player");
const constants = require("../../utils/constants");

const DELAY_BETWEEN_REFRESHES = 60*1000; // 1 minute
function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

database.db.any('SELECT guild_name,comlink_guild_id FROM guild WHERE track_data = TRUE AND priority = 0 ORDER BY guild_id').then( async (registeredGuilds) => {
    for (let g = 0; g < registeredGuilds.length; g++)
    {
        var guild = registeredGuilds[g];
        logger.info(`[${g+1}/${registeredGuilds.length}] Refreshing data for ${guild.guild_name}`);
        
        try {
            await guildRefresh.refreshGuildData(guild.comlink_guild_id, constants.CacheTypes.NIGHTLY);
            logger.info("Refreshed guild data for " + guild.guild_name);
        } catch (e)
        {
            logger.error("Error refreshing guild data for " + guild.guild_name + ". Details:\r\n" + formatError(e));
        }
        //await sleep(DELAY_BETWEEN_REFRESHES);
   }

   database.db.$pool.end();
});

