const got = require("got");
const swapi = require("../../utils/swapi");
const database = require("../../database");


new Promise(async (resolve, reject) =>
{

    let unitsList = await swapi.getUnitsList();

    for (var ix in unitsList)
    {
        let unitEntry = unitsList[ix];

        // if (unitEntry.base_id != "ZEBS3")
        // {
            let result = await database.db.any("SELECT COUNT(*) FROM unit_portrait WHERE unit_def_id = $1", [unitEntry.base_id]);
            if (result[0].count > 0) continue;
        // }
        let imgUrl = unitEntry.image;

        let img = await got(imgUrl).buffer();

        console.log(`Adding portrait for ${unitEntry.base_id}`);

        await database.db.any("INSERT INTO unit_portrait VALUES ($1, $2) ON CONFLICT (unit_def_id) DO UPDATE SET img = $2", [unitEntry.base_id, img]);
    }

    let abilitiesList = await swapi.getAbilitiesList();

    for (var ix in abilitiesList)
    {
        let ability = abilitiesList[ix];

        let result = await database.db.any("SELECT COUNT(*) FROM unit_ability_icon WHERE def_id = $1", [ability.base_id]);
        if (result[0].count > 0) continue;

        let imgUrl = ability.image;
        let img = await got(imgUrl).buffer();
        console.log(`Adding ability icon for ${ability.base_id}`);
        await database.db.any("INSERT INTO unit_ability_icon (def_id, img) VALUES ($1, $2)", [ability.base_id, img]);
    }

    database.db.$pool.end();
})