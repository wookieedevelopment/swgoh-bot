
require('dotenv').config({path:__dirname+'/./../../../.env'});
const { logger, formatError } = require("../../utils/log");
const database = require("../../database");
const metadataCache = require("../../utils/metadataCache");
const comlink = require ("../../utils/comlink");
const got = require ("got");
const swapiUtils = require ("../../utils/swapi");

new Promise(async (resolve, reject) =>
{
    logger.info("Updating Units List");
    let unitsList = await updateUnitsList();
    logger.info("Updating Abilities List");
    await updateAbilitiesList(unitsList);

    database.db.$pool.end();
});

async function updateUnitsList() {
    let data = await got("https://swgoh.gg/api/units/");
    let list = JSON.parse(data.body).data;

    let comlinkUnitMetadata = await comlink.getUnitsList();
    list.forEach(u => {
        try {
            u.categoryId = comlinkUnitMetadata.find(cu => swapiUtils.getUnitDefId(cu) === swapiUtils.getUnitDefId(u)).categoryId;
        } catch 
        {
            logger.info(`No match in comlink unit data for unit ${swapiUtils.getUnitDefId(u)} when populating categories.`);
        }
    } );

    await database.db.any(`
INSERT INTO metadata_cache(key, value)
VALUES ($1, $2)
ON CONFLICT(key) DO UPDATE SET value = $2`, [metadataCache.CACHE_KEYS.unitsList, JSON.stringify(list)]);

    return list;
}

async function updateAbilitiesList(unitsList) {
    
    let data = await got("https://swgoh.gg/api/abilities/")
    list = JSON.parse(data.body);

    list.forEach(ability => {
        let unit;
        if (ability.base_id === "outrider_grantedability_suprosa_supercomputer") unit = unitsList.find(u => u.base_id === "OUTRIDER");
        else unit = unitsList.find(u => u.base_id === ability.character_base_id || u.base_id === ability.ship_base_id);

        if (!unit) {
            console.log(`No matching unit found for ${ability.base_id}: ${ability.character_base_id} | ${ability.ship_base_id}`);
            return;
        }

        ability.unitName = unit.name;
    });
    
    await database.db.any(`
INSERT INTO metadata_cache(key, value)
VALUES ($1, $2)
ON CONFLICT(key) DO UPDATE SET value = $2`, [metadataCache.CACHE_KEYS.abilitiesList, JSON.stringify(list)]);
}