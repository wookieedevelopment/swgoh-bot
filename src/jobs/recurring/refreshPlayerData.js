
require('dotenv').config({path:__dirname+'/refresh.env'});
const constants = require("../../utils/constants");
const { logger, formatError } = require("../../utils/log");
const database = require("../../database");
const swapiUtils = require("../../utils/swapi");
const raidCommon = require("../../commands/raid/raidCommon");
const recommendationsUtils = require("../../utils/recommendations");
const statCalc = require("../../utils/stats/stats");
const tbCommon = require("../../commands/tb/common");

const RAIDS_TO_ESTIMATE = [/*raidCommon.KRAYT,*/ raidCommon.DEFAULT_RAID];
const chunkSize = process.env.CHUNK_SIZE ? parseInt(process.env.CHUNK_SIZE) : 20;

module.exports = {
    refreshPlayerData: refreshPlayerData
}

async function refreshPlayerData()
{
    logger.info("[Start] Refreshing Player Data");
    await statCalc.initCalc();

    let allycodes = await database.db.any(`
        SELECT DISTINCT allycode, priority
        FROM guild_players 
        LEFT JOIN guild ON (guild_players.guild_id = guild.guild_id) 
        ORDER BY priority DESC`)

        allycodes = allycodes.map(a => a.allycode);
        let promises = new Array(2);
        for (let i = 0; i < allycodes.length; i += chunkSize) {
            const chunkOfAllycodes = allycodes.slice(i, i + chunkSize);
            if (chunkOfAllycodes.length < chunkSize) {
                await insertPlayerDataForAllyCodes(chunkOfAllycodes, constants.CacheTypes.NIGHTLY);
            } else {
                logger.info(`Starting ${i}-${i + chunkSize}/${allycodes.length}`);
                const split1 = chunkOfAllycodes.slice(0, Math.floor(chunkOfAllycodes.length / 2));
                const split2 = chunkOfAllycodes.slice(Math.floor(chunkOfAllycodes.length / 2));

                promises[0] = insertPlayerDataForAllyCodes(split1, constants.CacheTypes.NIGHTLY);
                promises[1] = insertPlayerDataForAllyCodes(split2, constants.CacheTypes.NIGHTLY);

                let results = await Promise.allSettled(promises);

                if (results[0].reason) logger.info(`Failed on allycodes: ${split1.join(", ")}`);
                if (results[1].reason) logger.info(`Failed on allycodes: ${split1.join(", ")}`);

                logger.info(`Settled.`)
            }
        }
        
        logger.info(`[End] Completed Nightly Player Data Refresh Queuing. Count: ${allycodes.length}.`);
}

// logger.info("[Start] Refreshing Player Data");
//
// statCalc.initCalc().then(() => database.db.any(`
// SELECT DISTINCT allycode, priority
// FROM guild_players 
// LEFT JOIN guild ON (guild_players.guild_id = guild.guild_id) 
// ORDER BY priority DESC`).then(
//     async (allycodes) => {
//         allycodes = allycodes.map(a => a.allycode);
//         let promises = new Array(2);
//         for (let i = 0; i < allycodes.length; i += chunkSize) {
//             const chunkOfAllycodes = allycodes.slice(i, i + chunkSize);
//             if (chunkOfAllycodes.length < chunkSize) {
//                 await insertPlayerDataForAllyCodes(chunkOfAllycodes, constants.CacheTypes.NIGHTLY);
//             } else {
//                 logger.info(`Starting ${i}-${i + chunkSize}/${allycodes.length}`);
//                 const split1 = chunkOfAllycodes.slice(0, Math.floor(chunkOfAllycodes.length / 2));
//                 const split2 = chunkOfAllycodes.slice(Math.floor(chunkOfAllycodes.length / 2));

//                 promises[0] = insertPlayerDataForAllyCodes(split1, constants.CacheTypes.NIGHTLY);
//                 promises[1] = insertPlayerDataForAllyCodes(split2, constants.CacheTypes.NIGHTLY);

//                 let results = await Promise.allSettled(promises);

//                 if (results[0].reason) logger.info(`Failed on allycodes: ${split1.join(", ")}`);
//                 if (results[1].reason) logger.info(`Failed on allycodes: ${split1.join(", ")}`);

//                 logger.info(`Settled.`)
//             }
//         }
        
//         logger.info(`[End] Completed Nightly Player Data Refresh Queuing. Count: ${allycodes.length}.`);
//         database.db.$pool.end();
//     }
// )).finally(() => process.exit());

async function insertPlayerDataForAllyCodes(allyCodes, cacheType)
{
    var updateTime = new Date();

    let { players, errors } = await swapiUtils.getPlayers({ allycodes: allyCodes, useCache: true, cacheTimeHours: 12, cacheUpdatedPlayerData: false, cacheType: cacheType });

    // log errors, but don't stop
    if (errors.length > 0)
    {
        logger.error(`Failed to retrieve player data for some players as part of nightly refresh.
${errors.map(e => `[${e.allycode}] ${e.message}`).join("\r\n")}`);
    }

    var playerDataInsertValues = new Array();
    var playerDataStaticInsertValues = new Array();


    for (var playerIndex = 0; playerIndex < players.length; playerIndex++)
    {
        var player = players[playerIndex];

        if (!player) continue;

        let insertData;
        try {
            insertData = await getPlayerDataInsertValues(player, updateTime);
        } catch (err)
        {
            logger.error(`Error refreshing data for player ${swapiUtils.getPlayerAllycode(player)}\r\n` + formatError(err));
        }

        if (!insertData) continue;

        if (insertData.playerDataInsertValue) playerDataInsertValues.push(insertData.playerDataInsertValue);
        if (insertData.playerDataStaticInsertValue) playerDataStaticInsertValues.push(insertData.playerDataStaticInsertValue);
    }

    // no data was retrieved, so don't try to insert
    if (playerDataInsertValues.length === 0 || playerDataStaticInsertValues.length === 0) return;

    const playerDataInsert = database.pgp.helpers.insert(playerDataInsertValues, database.columnSets.playerDataCS);
    const playerDataStaticInsert = database.pgp.helpers.insert(playerDataStaticInsertValues, database.columnSets.playerDataStaticCS);

    const playerDataStaticOnConflict = ' ON CONFLICT(allycode) DO UPDATE SET ' +
        database.columnSets.playerDataStaticCS.assignColumns({from: 'EXCLUDED', skip: ['allycode']});

    // refresh static data & insert new dynamic data
    await database.db.none(playerDataStaticInsert + playerDataStaticOnConflict + "; " + playerDataInsert);
}

async function getPlayerDataInsertValues(player, updateTime)
{
    let retObj = { playerDataInsertValue: null, playerDataStaticInsertValue: null, ultUnits: null };
    if (!player) return retObj;

    var poUTCOffsetMinutes = swapiUtils.getPlayerPayoutUTCOffsetMinutes(player);
    var gp = parseInt(swapiUtils.getGP(player));
    var gpSquad = parseInt(swapiUtils.getGPSquad(player));
    var gpFleet = parseInt(swapiUtils.getGPFleet(player));
    var arenaRank = swapiUtils.getSquadArenaRank(player);
    var fleetRank = swapiUtils.getFleetArenaRank(player);
    var name = swapiUtils.getPlayerName(player);
    var allyCode = swapiUtils.getPlayerAllycode(player);
    let skillRating = swapiUtils.getPlayerSkillRating(player);
    let lastActivityTime = new Date(parseInt(player.lastActivityTime));
    
    let portraits = swapiUtils.getPlayerUnlockedPortraits(player);
    var reekWin = portraits ? portraits.indexOf("PLAYERPORTRAIT_REEK") >= 0 : null;

    let gear = swapiUtils.getPlayerGearSummary(player);
    let relic = swapiUtils.getPlayerRelicSummary(player);

    let playerGLs = swapiUtils.getPlayerGalacticLegends(player);
    let playerCapitalShips = swapiUtils.getPlayerCapitalShips(player);

    let modCounts = swapiUtils.getPlayerModSummary(player);

    let zetaCount = swapiUtils.getPlayerZetaCount(player);
    let omicronCount = swapiUtils.getPlayerOmicronCount(player);

    // same as DSR bot
    let modQuality = modCounts.speed15 / (gpSquad / 100000.0);

    // double counts +20s, triple counts +25s, and incorporates 6dot
    let modQuality2 = (modCounts.speed15 + modCounts.speed20 + modCounts.speed25 + 0.5*modCounts.sixDot) / (gpSquad / 100000.0);

    // hotbot formula ((# of 15-19 Speed * 0.8) + (# of 20-24 Speed) + (# of 25+ Speed * 1.6)) / (squadGP / 100,000)
    let modQualityHotbot = ((modCounts.speed15-modCounts.speed20) * 0.8 + (modCounts.speed20-modCounts.speed25) + modCounts.speed25*1.6) / (gpSquad / 100000.0);

    let raidScoreEstimate = {};
    for (let r = 0; r < RAIDS_TO_ESTIMATE.length; r++)
    {
        raidScoreEstimate[RAIDS_TO_ESTIMATE[r].KEY] = await getPlayerRaidEstimate(RAIDS_TO_ESTIMATE[r], player);
    }

    let datacronSummaryData = 
    {
        level3: 0,
        level6: 0,
        level9: 0,
        rerolls: 0
    };

    let datacrons = swapiUtils.getPlayerDatacrons(player);
    for (let dc of datacrons)
    {
        datacronSummaryData.level3 += ((dc.affix.length >= 3) ? 1 : 0);
        datacronSummaryData.level6 += ((dc.affix.length >= 6) ? 1 : 0);
        datacronSummaryData.level9 += ((dc.affix.length >= 9) ? 1 : 0);
        datacronSummaryData.rerolls += dc.rerollCount;
    }


    const { specialMissionEligibility, income } = await tbCommon.estimateSpecialMissionIncomeForPlayer(player);

    retObj.playerDataInsertValue = {
        allycode: allyCode,
        player_name: name,
        gp: gp,
        gp_fleet: gpFleet,
        gp_squad: gpSquad,
        arena_rank: arenaRank,
        fleet_rank: fleetRank,
        gl_count: playerGLs.length,
        mod_quality: modQuality,
        mod_quality2: modQuality2,
        mod_quality_hotbot: modQualityHotbot,
        mods_six_dot: modCounts.sixDot,
        mods_speed25: modCounts.speed25,
        mods_speed20: modCounts.speed20,
        mods_speed15: modCounts.speed15,
        mods_speed10: modCounts.speed10,
        mods_offense6: modCounts.offense6,
        gear: gear,
        relic: relic,
        skill_rating: skillRating,
        zetas: zetaCount,
        omicrons: omicronCount,
        timestamp: updateTime,
        raid_score_estimate: raidScoreEstimate,
        tb_special_income: income,
        datacron_summary: datacronSummaryData,
        last_activity_time: lastActivityTime
    };

    retObj.playerDataStaticInsertValue = {
        allycode: allyCode,
        player_name: name,
        payout_utc_offset_minutes: poUTCOffsetMinutes,
        reek_win: reekWin,
        gl_rey: playerGLs.indexOf("GLREY") >= 0,
        gl_slkr: playerGLs.indexOf("SUPREMELEADERKYLOREN") >= 0,
        gl_jml: playerGLs.indexOf("GRANDMASTERLUKE") >= 0,
        gl_see: playerGLs.indexOf("SITHPALPATINE") >= 0,
        gl_jmk: playerGLs.indexOf("JEDIMASTERKENOBI") >= 0,
        gl_lv: playerGLs.indexOf("LORDVADER") >= 0,
        gl_jabba: playerGLs.indexOf("JABBATHEHUTT") >= 0,
        gl_leia: playerGLs.indexOf("GLLEIA") >= 0,
        gl_ahsoka: playerGLs.indexOf("GLAHSOKATANO") >= 0,
        capital_ships: playerCapitalShips,
        timestamp: updateTime
    };

    return retObj;
}

async function getPlayerRaidEstimate(raidData, playerData)
{

    let rec = await recommendationsUtils.generateRaidRecommendations(playerData, {
        DIFFICULTY_MINIMUMS: raidData.DIFFICULTY_MINIMUMS,
        MAX_ATTEMPTS: raidData.MAX_ATTEMPTS,
        MAX_TOTAL_SCORE: raidData.MAX_TOTAL_SCORE,
        MIN_RECOMMENDED_GEAR: raidData.MIN_RECOMMENDED_GEAR,
        REC_TYPE: raidData.KEY,
        ROUND_FN: raidData.ROUND_FN,
        FILLER_TEAM: raidData.FILLER_TEAM,
        effort: recommendationsUtils.EFFORT.MEDIUM,
        extraRelicScalingFactor: raidData.EXTRA_RELIC_SCALING_FACTOR
    });

    return rec.points;
}