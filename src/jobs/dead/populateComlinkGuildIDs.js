const got = require("got");
const swapi = require("../../utils/swapi");
const guildUtils = require("../../utils/guild")
const database = require("../../database")

new Promise(async (resolve, reject) =>
{
    let guildsToUpdate = await database.db.any("SELECT guild_id, guild_name,allycode FROM guild");

    for (let guild of guildsToUpdate)
    {
        
        let apiGuild = await guildUtils.getGuildDataFromAPI({ allycode: guild.allycode }, { useCache: false });
        let comlinkGuildId = swapi.getGuildId(apiGuild);

        if (guild.guild_name != swapi.getGuildName(apiGuild))
        {
            console.log("Guild may not match: " + guild.guild_name + " vs. " + swapi.getGuildName(apiGuild));
            continue;
        }
        console.log("Updating comlink Guild ID for guild " + guild.guild_name);
        await database.db.any("UPDATE guild SET comlink_guild_id = $1 WHERE guild_id = $2", [comlinkGuildId, guild.guild_id])
    }

    database.db.$pool.end();
})