const brain = require ("brain.js");
const database = require ("../../database");
const swapiUtils = require ("../../utils/swapi");
const guildUtils = require ("../../utils/guild");
const playerUtils = require("../../utils/player");
const kraytCommon = require("../../commands/krayt/kraytCommon");
const constants = require("../../utils/constants");
const fs = require('fs/promises');

/*
Input: 
Squad GP / 20M
GAC rank / 20M
Mod Quality / 100
Total OR relic levels / 1000
Total Tusken relic levels / 1000
Total Jawa relic levels / 1000
Total HC relic levels / 1000
Total Mandalorian relic levels / 1000

Output:
Expected Score / 20M
 */

const gpFactor = 20000000;
const gacFactor = 20000000;
const mqFactor = 100;
const relicFactor = 1000;
const scoreFactor = 20000000;

// const allycode = '552475294'; // fatal
const allycode = '985736123'; // me

const retrain = true;
const retrainFromExisting = false;

new Promise(async (resolve, reject) =>
{
    const net = new brain.NeuralNetwork({hiddenLayers :[2]});

    if (retrain)
    {
        if (retrainFromExisting)
        {
            let jsonText = await fs.readFile("./src/data/trained-net.json");
            net.fromJSON(JSON.parse(jsonText));
        }

        let trainingData = await database.db.any(
            `SELECT 
                gp_squad,
                skill_rating,
                mod_quality,
                relics_old_republic,
                relics_hutt_cartel,
                relics_mandalorian,
                relics_tusken,
                relics_jawa,
                last_score
            FROM raid_training_data`);

        let brainTraining = [];

        for (let trainingDataRow of trainingData)
        {
            let input = new Array(8);

            input[0] = trainingDataRow.gp_squad / gpFactor;
            input[1] = trainingDataRow.skill_rating / gacFactor;
            input[2] = trainingDataRow.mod_quality / mqFactor;
            input[3] = trainingDataRow.relics_old_republic / relicFactor;
            input[4] = trainingDataRow.relics_hutt_cartel / relicFactor;
            input[5] = trainingDataRow.relics_mandalorian / relicFactor;
            input[6] = trainingDataRow.relics_tusken / relicFactor;
            input[7] = trainingDataRow.relics_jawa / relicFactor;
            
            let output = [trainingDataRow.last_score / scoreFactor];

            brainTraining.push({ input: input, output: output })
        }

        net.train(brainTraining, 
            {
                log: true,
                iterations: 1000000,
                errorThresh: 0.001
            });

        const jsonNN = net.toJSON();
        
        await fs.writeFile("./src/data/trained-net.json", JSON.stringify(jsonNN));
    } else {
        let jsonText = await fs.readFile("./src/data/trained-net.json");
        net.fromJSON(JSON.parse(jsonText));
    }
    let guild = await guildUtils.getGuildDataFromAPI({allycode: allycode }, { useCache: false });
    let kraytRaidData = kraytCommon.findKraytRaid(guild);

    let player = await swapiUtils.getPlayer(allycode);

    let testingData = [
        swapiUtils.getGPSquad(player) / gpFactor,
        swapiUtils.getPlayerSkillRating(player) / gacFactor,
        playerUtils.calculateModScore(player, constants.MOD_SCORE_TYPES.WOOKIEEBOT) / mqFactor,
        getTotalRelicsForFaction(player, "affiliation_oldrepublic") / relicFactor,
        getTotalRelicsForFaction(player, "affiliation_huttcartel") / relicFactor,
        getTotalRelicsForFaction(player, "affiliation_mandalorian") / relicFactor,
        getTotalRelicsForFaction(player, "species_tusken") / relicFactor,
        getTotalRelicsForFaction(player, "species_jawa") / relicFactor
    ];

    let lastScore = kraytRaidData?.raidMember.find(pr => pr.playerId === player.playerId)?.memberProgress;

    console.log("Last Actual: " + lastScore)
    console.log("Estimated: " + net.run(testingData)[0]*scoreFactor);

    let players = (await swapiUtils.getPlayers({ allycodes: guildUtils.getGuildAllycodesForAPIGuild(guild), useCache: true})).players; 
    let totalEstimate = 0;
    
    let totalActual = kraytRaidData?.raidMember.reduce((p, v) => p + parseInt(v.memberProgress), 0) ?? 0;
    
    for (let p of players)
    {
        let data = [
            swapiUtils.getGPSquad(p) / gpFactor,
            swapiUtils.getPlayerSkillRating(p) / gacFactor,
            playerUtils.calculateModScore(p, constants.MOD_SCORE_TYPES.WOOKIEEBOT) / mqFactor,
            getTotalRelicsForFaction(p, "affiliation_oldrepublic") / relicFactor,
            getTotalRelicsForFaction(p, "affiliation_huttcartel") / relicFactor,
            getTotalRelicsForFaction(p, "affiliation_mandalorian") / relicFactor,
            getTotalRelicsForFaction(p, "species_tusken") / relicFactor,
            getTotalRelicsForFaction(p, "species_jawa") / relicFactor
        ];

        totalEstimate += net.run(data)[0]*scoreFactor;
    }

    console.log("Guild estimate: " + totalEstimate.toLocaleString("en"));
    console.log("Guild actual: " + totalActual.toLocaleString("en"))
});


function getTotalRelicsForFaction(player, faction)
{
    let roster = swapiUtils.getPlayerRoster(player);

    let factionUnits = roster.filter(ru => ru.categoryId.indexOf(faction) != -1);

    let total = 0;
    for (let fu of factionUnits)
    {
        if (swapiUtils.getUnitRelicLevel(fu)) total += Math.max(0, swapiUtils.getUnitRelicLevel(fu) - 2);
    }

    return total;
}