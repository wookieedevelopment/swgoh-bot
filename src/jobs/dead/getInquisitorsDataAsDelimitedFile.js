const swapi = require("../../utils/swapi");
const database = require("../../database")
const { parseAsync } = require('json2csv');
const fs = require('fs');
const parse = require("csv-parse/lib/sync");
const playerUtils = require("../../utils/player");

const INQ_DEF_IDS = [
    "GRANDINQUISITOR",
    "SEVENTHSISTER",
    "NINTHSISTER",
    "EIGHTHBROTHER",
    "SECONDSISTER",
    "FIFTHBROTHER",
    "THIRDSISTER"
];

const path = "./Reva Mission survey, Dec 2022 (Responses) - Form Responses 1.csv"

const revaMissionDataCS = new database.pgp.helpers.ColumnSet(
    ['allycode',
    'reported_timestamp'], 
    {table: 'reva_mission_data'});


new Promise(async (resolve, reject) =>
{
    // read file, if there is
    if (path)
    {
        let file = fs.readFileSync(path);

        const formData = parse(file, { columns: true });
        
        let revaMissionDataInsertValues = 
            formData
                .filter(d => {
                    try { 
                        playerUtils.cleanAndVerifyStringAsAllyCode(d["What is your ally code? (we are testing to see if this lets us pull modding data directly)"])
                        return true;
                    } catch { return false; }
                })
                .map(d => {
                    return {
                    allycode: playerUtils.cleanAndVerifyStringAsAllyCode(d["What is your ally code? (we are testing to see if this lets us pull modding data directly)"]),
                    reported_timestamp: d["Timestamp"]
                }});

        
        const revaMissionDataInsert = database.pgp.helpers.insert(revaMissionDataInsertValues, revaMissionDataCS);
        await database.db.none(revaMissionDataInsert + " ON CONFLICT DO NOTHING");
    }
                    
    let playersToCheck = await database.db.any("SELECT allycode, reported_timestamp, reva_mission_data_id FROM reva_mission_data WHERE exported = FALSE");

    let playerData = await swapi.getPlayers({
        allycodes: playersToCheck.map(p => p.allycode), 
        useCache: false
    });

    if (playerData.errors.length > 0)
    {
        playerData.errors.forEach(e => console.log(`Allycode: ${e.allycode}, Error: ${e.message}`))
    }

    let now = new Date();
    let csvData = [];

    

    for (var player of playerData.players)
    {
        let playerToCheckData = playersToCheck.find(p => p.allycode === swapi.getPlayerAllycode(player));
        await database.db.any("UPDATE reva_mission_data SET player_data = $1, timestamp = $2, exported = TRUE WHERE reva_mission_data_id = $3",
        [player, now, playerToCheckData.reva_mission_data_id]);

        
        // populate data for CSV
        let csvDataObj = {
            "Ally Code": swapi.getPlayerAllycode(player),
            "Reported Timestamp": playerToCheckData.reported_timestamp,
            "Data Timestamp": now
        };

        let playerRoster = swapi.getPlayerRoster(player);
        let badData = false;
        for (var inqId of INQ_DEF_IDS)
        {
            let inq = playerRoster.find(u => swapi.getUnitDefId(u) === inqId);
            
            if (inqId === "GRANDINQUISITOR" && inq === null) 
            {
                // invalid data, mission can't be done without GI
                badData = true; 
                break;
            }
            let speed = swapi.getUnitStat(inq, swapi.STATS.SPEED);
            let cc = swapi.getUnitStat(inq, swapi.STATS.ATTACK_CRITICAL_PERCENT_ADDITIVE);
            let cd = swapi.getUnitStat(inq, swapi.STATS.CRITICAL_DAMAGE);
            let physOff = swapi.getUnitStat(inq, swapi.STATS.ATTACK_DAMAGE);
            let hp = swapi.getUnitStat(inq, swapi.STATS.MAX_HEALTH);
            let prot = swapi.getUnitStat(inq, swapi.STATS.MAX_SHIELD);
            let tenacity = swapi.getUnitStat(inq, swapi.STATS.RESISTANCE);

            csvDataObj[inqId + "_speed"] = speed;
            csvDataObj[inqId + "_physical_crit_chance"] = parseFloat((cc*100).toFixed(2));
            csvDataObj[inqId + "_crit_damage"] = cd*100;
            csvDataObj[inqId + "_physical_offense"] = physOff;
            csvDataObj[inqId + "_max_hp"] = hp;
            csvDataObj[inqId + "_max_prot"] = prot;
            csvDataObj[inqId + "_tenacity"] = parseFloat((tenacity*100).toFixed(2));
        }

        if (badData) continue;

        csvData.push(csvDataObj);
    }

    if (csvData.length === 0) {
        console.log("No new data to export.");
        return;
    }
    
    const csvParsingOptions = { defaultValue: 0 }
    let csvText = await parseAsync(csvData, csvParsingOptions);

    fs.writeFileSync("./inq_data_" + now.getTime() + ".csv", csvText, { encoding: "utf-8" });
    
})