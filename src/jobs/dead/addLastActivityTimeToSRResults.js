const swapi = require("../../utils/swapi");
const database = require("../../database")
const { parseAsync } = require('json2csv');
const fs = require('fs');
const parse = require("csv-parse/lib/sync");
const playerUtils = require("../../utils/player");
const comlink = require("../../utils/comlink");


const origFilePath = "./src/jobs/gac-squish-11-05-2023-to-11-08-2023.csv";
const updatedFilePath = "./src/jobs/gac-squish-11-05-2023-to-11-08-2023-dt.csv";


new Promise(async (resolve, reject) =>
{
    
    let file = fs.readFileSync(origFilePath);
    const origData = parse(file, { columns: true });
    let csvData = [];

    for (let r = 0; r < origData.length; r++)
    {
        let od = origData[r];
        let playerData = await comlink.getPlayer(od.allycode);
        let lastActivityTime = playerData.lastActivityTime;

        od.last_active = new Date(parseInt(lastActivityTime));
        csvData.push(od);
        if (r%10 === 0) console.log(`${r}/${origData.length}`);
    }

    const csvParsingOptions = { defaultValue: 0 }
    let csvText = await parseAsync(csvData, csvParsingOptions);

    fs.writeFileSync(updatedFilePath, csvText, { encoding: "utf-8" });
    
})