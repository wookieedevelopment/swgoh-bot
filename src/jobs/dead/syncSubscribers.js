const got = require("got");
const swapi = require("../../utils/swapi");
const database = require("../../database");

const DEV_IDS = ["276185061970149377", "628359474377129985"];

const SUBSCRIBER_IDS = [
    "312062488566956042", // zynix.
    "517942221345914880",   // .chefsalat
    "311955045946228740",   // grimm519
    "812064541666967563",   // .kacabal
    "377883183313584129",   // mahler444
    "432312897532723202",   // mando818
    "225932017735303168",   // garalktail
    "424330660602183730",   // reoris
    "318823791600074774",   // russ
    "313757048452022292", // sasisan
    "696975690305634355",   // jkskippy
    "662265451879071747", // spy.hunter
    "189473051367178240", //yohnus
    "982855577472487475", // riiick
    "512049678850719787", // angus_i_am
];


new Promise(async (resolve, reject) =>
{
    await database.db.any(`
UPDATE guild
SET priority = 0
WHERE guild_id NOT IN (SELECT guild_id FROM guild_players WHERE guild_id is not null AND allycode IN (SELECT allycode FROM user_registration WHERE discord_id IN ($2:csv)));

UPDATE guild 
SET priority = 1
WHERE guild_id IN (SELECT guild_id FROM guild_players WHERE allycode IN (SELECT allycode FROM user_registration WHERE discord_id IN ($2:csv)));

UPDATE guild
SET priority = 1000
WHERE guild_id IN (SELECT guild_id FROM guild_players WHERE allycode IN (SELECT allycode FROM user_registration WHERE discord_id IN ($1:csv)));
    `, [DEV_IDS, SUBSCRIBER_IDS]);

    let priorityGuilds = await database.db.any(`select guild_name from guild where priority > 0`);

    let str = priorityGuilds.map(g => g.guild_name).join("\n");
    console.log(str);
    database.db.$pool.end();
})