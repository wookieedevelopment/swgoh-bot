const dotenv = require("dotenv");
dotenv.config({ path: ".env"});
const fs = require('fs');

const initOptions = {
    // initialization options;
};

const dbConfigProd = {
    "host": process.env.DB_HOST,
    "port": process.env.DB_PORT,
    "database": "wookieebot",
    "user": process.env.DB_USERNAME,
    "password": process.env.DB_PASSWORD
    // "ssl": process.env.DB_SSL
}

const dbConfigDev = {
    "host": process.env.DB_HOST,
    "port": process.env.DB_PORT,
    "database": "wookieebot_dev",
    "user": process.env.DB_USERNAME,
    "password": process.env.DB_PASSWORD
    // "ssl": process.env.DB_SSL
}

const pgp = require('pg-promise')(initOptions);

const dbProd = pgp(dbConfigProd);
const dbDev = pgp(dbConfigDev);


let ENDOR_IMPS = [
    "ADMIRALPIETT",
    "COLONELSTARCK",
    "IDENVERSIOEMPIRE",
    "MOFFGIDEONS1",
    "RANGETROOPER",
    "SCOUTTROOPER_V3",
    "SHORETROOPER"
];

let ENDOR_REBWOKS = [
    "ADMIRALACKBAR",
    "C3POLEGENDARY",
    "CAPTAINDROGAN",
    "HANSOLO",
    "HERASYNDULLAS3",
    "CHIEFCHIRPA",
    "EWOKELDER",
    "LOGRAY",
    "PAPLOO",
    "TEEBO"
];

let ENDOR_ITEM_OPTIONALS = ENDOR_IMPS.concat(ENDOR_REBWOKS);

// priority, lowest to highest
let ENDOR_LEADERS = [
    "HERASYNDULLAS3",
    "MOFFGIDEONS1",
    "IDENVERSIOEMPIRE",
    "ADMIRALACKBAR",
    "CHIEFCHIRPA",
    "ADMIRALPIETT",
    "TEEBO"
];

function getQualityForCombo(combo, scoutOmi = false)
{
    let qualityBoost = 0;
    if (combo.find(u => u === "SCOUTTROOPER_V3")) qualityBoost += 4;
    if (combo.find(u => u === "MOFFGIDEONS1")) qualityBoost += 2;
    if (combo.find(u => u === "HERASYNDULLAS3")) qualityBoost += 2;
    if (combo.find(u => u === "ADMIRALPIETT")) qualityBoost += 2;
    if (combo.find(u => u === "IDENVERSIOEMPIRE")) qualityBoost += 1;
    if (combo.find(u => u === "ADMIRALACKBAR")) qualityBoost += 4;
    if (combo.find(u => u === "CHIEFCHIRPA")) qualityBoost += 4;
    if (combo.find(u => u === "EWOKELDER")) qualityBoost += 3;
    if (!combo.find(u => ENDOR_LEADERS.find(u))) qualityBoost -= 4;
    if (scoutOmi) qualityBoost += 4;

    let quality = (combo.length === 2) ? new Array(7).fill(75 + qualityBoost) : new Array(7).fill(78 + qualityBoost);

    return quality;
}

const scoutOmi = ["uniqueskill_SCOUTTROOPER_V301"];

// const oldDupeRaidTeamIds = [73, 67, 99, 34, 128, 82, 78, 137, 107, 72, 109, 129, 130, 108, 102, 74, 131, 76, 117, 75];

new Promise(async (resolve, reject) =>
{
    let combos = new Array();

    for (let imp of ENDOR_IMPS)
    {

        for (let rebwok of ENDOR_REBWOKS)
        {
            let duo = [imp, rebwok]

            combos.push(duo);

            for (let opt of ENDOR_ITEM_OPTIONALS)
            {
                if (duo.indexOf(opt) != -1) continue;

                if (combos.find(c => c.length === 3 && c.indexOf(imp) != -1 && c.indexOf(rebwok) != -1 && c.indexOf(opt) != -1)) continue;

                let trio = [imp, rebwok, opt];
                combos.push(trio);
            }
        }
    }

    let dupes = [];
    let insertions = [];
    for (let c of combos)
    {
        let leaderUnitId = null;
        let unitsRequired;

        let ix1 = ENDOR_LEADERS.indexOf(c[0]);
        let ix2 = ENDOR_LEADERS.indexOf(c[1]);
        let ix3 = (c.length === 2) ? -1 : ENDOR_LEADERS.indexOf(c[2]);

        if (ix1 === -1 && ix2 === -1 && ix3 === -1)
        {
            // no leader unit, so just choose the imp
            leaderUnitId = c[0];
            unitsRequired = c.slice(1);
        }
        else if (ix1 > ix2 && ix1 > ix3)
        {
            // imp leader
            leaderUnitId = c[0];
            unitsRequired = c.slice(1);
        }
        else if (ix2 > ix1 && ix2 > ix3)
        {
            // rebwok leader
            leaderUnitId = c[1];
            unitsRequired = [c[0]];
            if (c.length === 3)
            {
                unitsRequired.push(c[2]);
            }
        }
        else if (ix3 > ix1 && ix3 > ix2)
        {
            // 3rd unit leader
            leaderUnitId = c[2];
            unitsRequired = [c[0], c[1]];
        }

        let match = await dbProd.oneOrNone(
            `SELECT * 
            FROM raid_team 
            WHERE leader_unit_id = $1 
                AND $2 <@ units_required::text[] 
                AND ult_required = $3 
                AND raid_id = $4 
                AND team_size = $5 
                AND omicrons_required IS NULL`,
            [leaderUnitId, unitsRequired, false, "ENDOR", c.length, null]);

        if (match) {
            console.log("team exists: " + c.join(", "));
            dupes.push(match.raid_team_id);
        }

        insertions.push({
            name: leaderUnitId + ", " + unitsRequired.join(", "),
            units_required: unitsRequired,
            raid_id: "ENDOR",
            team_size: c.length,
            omicrons_required: null,
            quality_by_tier: getQualityForCombo(c),
            leader_unit_id: leaderUnitId,
            note: null,
            auto_factor: 0.6,
            ult_required: false
        });

        if (c.find(u => u === "SCOUTTROOPER_V3"))
        {
            let match = await dbProd.oneOrNone(
                `SELECT * 
                FROM raid_team 
                WHERE leader_unit_id = $1 
                    AND $2 <@ units_required::text[] 
                    AND ult_required = $3 
                    AND raid_id = $4 
                    AND team_size = $5 
                    AND ( ($6 @> omicrons_required::text[]) AND (omicrons_required::text[] <@ $6) )`,
                [leaderUnitId, unitsRequired, false, "ENDOR", c.length, scoutOmi]);

            if (match) {
                console.log("team exists: omi " + c.join(", "));
                dupes.push(match.raid_team_id);
            }

            insertions.push({
                name: leaderUnitId + ", " + unitsRequired.join(", "),
                units_required: unitsRequired,
                raid_id: "ENDOR",
                team_size: c.length,
                omicrons_required: scoutOmi,
                quality_by_tier: getQualityForCombo(c, true),
                leader_unit_id: leaderUnitId,
                note: "generated ITEM",
                auto_factor: 0.6,
                ult_required: false
            });
        }
    }

    
    const colset = new pgp.helpers.ColumnSet(
        ['name', 
        'units_required',
        'raid_id',
        'team_size',
        'omicrons_required',
        'quality_by_tier',
        'leader_unit_id',
        'note',
        'auto_factor',
        'ult_required'
        ], 
        {table: 'raid_team'});

    let script = pgp.helpers.insert(insertions, colset);

    await dbProd.none(script);

    await dbProd.none(
        `DELETE FROM raid_team WHERE raid_team_id IN ($1:csv)`, [dupes]);

    dbDev.$pool.end();
    dbProd.$pool.end();
});


let ENDOR_ALLOWED_UNITS = [
    "ADMIRALACKBAR",
    "C3POLEGENDARY",
    "CAPTAINREX",
    "CAPTAINDROGAN",
    "CHEWBACCALEGENDARY",
    "HANSOLO",
    "HERASYNDULLAS3",
    "JEDIKNIGHTLUKE",
    "ADMINISTRATORLANDO",
    "GLLEIA",
    "WEDGEANTILLES",
    "CHIEFCHIRPA",
    "EWOKELDER",
    "EWOKSCOUT",
    "LOGRAY",
    "PAPLOO",
    "PRINCESSKNEESAA",
    "TEEBO",
    "WICKET",
    "ADMIRALPIETT",
    "COLONELSTARCK",
    "DEATHTROOPER",
    "VEERS",
    "IDENVERSIOEMPIRE",
    "MAGMATROOPER",
    "MOFFGIDEONS1",
    "RANGETROOPER",
    "SCOUTTROOPER_V3",
    "SHORETROOPER",
    "STORMTROOPER"
]