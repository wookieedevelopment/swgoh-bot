const database = require("../../database");
const swapi = require("../../utils/swapi");
const playerUtils = require("../../utils/player");
const guildUtils = require("../../utils/guild");
const constants = require("../../utils/constants");

async function updateDatabase() {
    let allAllycodes = await database.db.any("SELECT allycode FROM user_registration");
    let discordIdCounts = {};

    for (const ally of allAllycodes) {
        try {
            let allycode = ally.allycode;

            let guild;
            try {
                guild = await guildUtils.getGuildDataFromAPI({ allycode: allycode }, { useCache: false });
            } catch (err) {
                console.log(formatError(err));
                return;
            }

            let matchingMember = guildUtils.getGuildRoster(guild).find(m => swapi.getPlayerAllycode(m) === allycode);
            let name = matchingMember.playerName;

            var discordIDdata = await database.db.one("SELECT discord_id FROM user_registration WHERE allycode = $1", [allycode]);
            let discord_id = discordIDdata.discord_id;

            // Skip if discord_id is null or 'temp'
            if (discord_id === null || discord_id === 'temp') {
                console.log(`Skipping processing for allycode ${allycode} because discord_id is null or 'temp'.`);
                continue;
            }

            // Increment alt for each new instance of discord_id
            if (discordIdCounts[discord_id] === undefined) {
                discordIdCounts[discord_id] = 0;
            } else {
                discordIdCounts[discord_id]++;
            }

            const ALT = discordIdCounts[discord_id];

            await database.db.any(
                `INSERT INTO user_registration (allycode, discord_id, alt)
                VALUES ($1, $2, $3, $4)
                ON CONFLICT (allycode) DO UPDATE SET discord_id = COALESCE($2, user_registration.discord_id), alt = $3;
                `,
                [allycode, discord_id, ALT]
            );
        } catch (error) {
            console.error(`Error processing allycode ${ally.allycode}: ${error.message}`);
        }
    }
}

updateDatabase();
