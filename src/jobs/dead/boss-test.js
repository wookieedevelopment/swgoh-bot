
require('dotenv').config({path:__dirname+'/./../../../.env'});

async function readme() {
    const PgBoss = require('pg-boss');
    const boss = new PgBoss(`postgres://${process.env.DB_USERNAME}:${process.env.DB_PASSWORD}@${process.env.DB_HOST}/${process.env.DB_QUEUE_NAME}`);
  
    boss.on('error', error => console.error(error));
  
    await boss.start();
  
    const queue = 'some-queue';
  
    let jobId = await boss.send(queue, { param1: 'foo' });
  
    console.log(`created job in queue ${queue}: ${jobId}`);
  
    await boss.work(queue, someAsyncJobHandler);
  }
  
  async function someAsyncJobHandler(job) {
    console.log(`job ${job.id} received with data:`);
    console.log(JSON.stringify(job.data));
  
    await doSomethingAsyncWithThis(job.data);
  }

  
new Promise(async (resolve, reject) =>
{
    await readme();
});