const brain = require ("brain.js");
const database = require ("../../database");
const swapiUtils = require ("../../utils/swapi");
const guildUtils = require ("../../utils/guild");
const playerUtils = require("../../utils/player");
const kraytCommon = require("../../commands/krayt/kraytCommon");
const constants = require("../../utils/constants");

let otherGuildsToTrainWith = [
    'jrl9Q-_CRDGdMyNjTQH1rQ',
    'fJXYTxpsS9iZvGj2M1OUGw',
    'YejwMXrlTSaPuZp4OUQaLA',
    '0HQfPaTtSPSNOiSCNII89Q',
    'nNv53ssBQhaKue5zstelFQ',
    'DsTZ7vt6Tuy_rjehJDtE-g',
    'xxWfSE_uR9uRYUXL9erLag',
    'GbsabjvbQESUd8r_qPqdYg',
    'OVe2zpEJRXSZ4TZ0bFl0uw',
    'gxRpkB93QOmBBUTwtycxzw',
    'jccY3ocDRWGxmyIF-ozBZg',
    '4y94IJgFQN6Ruz0FcxU9ow',
    'l8butxLSSlSUVB_Y9TZh3w',
    '8RhppbbqR_ShmjY5S6ZtQg',
    'dG4QZJcaRSSus5w8z0LRlw'
]

database.db.any('SELECT guild_name,comlink_guild_id FROM guild').then( async (registeredGuilds) => {

    for (var comlinkId of otherGuildsToTrainWith)
    {
         console.log("Getting Krayt Training data for " + comlinkId);
         await populateTrainingDataForGuild(comlinkId);
    }

    for (var g in registeredGuilds)
    {
        var guild = registeredGuilds[g];
        console.log("Getting Krayt Training data for " + guild.guild_name);
        try {
            await populateTrainingDataForGuild(guild.comlink_guild_id);
        } catch (e) {
            console.log(`!! Failed Guild: ${guild.comlink_guild_id} - ${guild.guild_name}`)
        }
   }

});


async function populateTrainingDataForGuild(comlink_guild_id)
{    
    let guild = await guildUtils.getGuildDataFromAPI({comlinkGuildId: comlink_guild_id }, { useCache: false });
    let kraytRaidData = kraytCommon.findKraytRaid(guild);
    if (!kraytRaidData || kraytRaidData.outcome != 1) return;

    let players = (await swapiUtils.getPlayers({ allycodes: guildUtils.getGuildAllycodesForAPIGuild(guild), useCache: true})).players; 

    let trainingData = [];

    for (let player of players)
    {
        
        let lastScore = kraytRaidData?.raidMember.find(pr => pr.playerId === player.playerId)?.memberProgress;
        if (!lastScore || lastScore === '0') continue;
        lastScore = parseInt(lastScore);
        
        trainingData.push({ 
            'gp_squad': swapiUtils.getGPSquad(player),
            'skill_rating': swapiUtils.getPlayerSkillRating(player),
            'mod_quality': playerUtils.calculateModScore(player, constants.MOD_SCORE_TYPES.WOOKIEEBOT),
            'relics_old_republic': getTotalRelicsForFaction(player, "affiliation_oldrepublic"),
            'relics_hutt_cartel': getTotalRelicsForFaction(player, "affiliation_huttcartel"),
            'relics_mandalorian': getTotalRelicsForFaction(player, "affiliation_mandalorian"),
            'relics_tusken': getTotalRelicsForFaction(player, "species_tusken"),
            'relics_jawa': getTotalRelicsForFaction(player, "species_jawa"),
            'last_score': lastScore,
            'raid_end_time_seconds': parseInt(kraytRaidData.endTime),
            'player_id': player.playerId
         })
    }
    
    const trainingDataInsert = database.pgp.helpers.insert(trainingData, database.columnSets.raidTrainingDataCS);
        
    const trainingDataOnConflict = ' ON CONFLICT(player_id, raid_end_time_seconds) DO NOTHING';

    await database.db.none(trainingDataInsert + trainingDataOnConflict);
}


function getTotalRelicsForFaction(player, faction)
{
    let roster = swapiUtils.getPlayerRoster(player);

    let factionUnits = roster.filter(ru => ru.categoryId.indexOf(faction) != -1);

    let total = 0;
    for (let fu of factionUnits)
    {
        if (swapiUtils.getUnitRelicLevel(fu)) total += Math.max(0, swapiUtils.getUnitRelicLevel(fu) - 2);
    }

    return total;
}