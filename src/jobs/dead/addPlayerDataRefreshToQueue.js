const { QueueClient } = require("@azure/storage-queue");
require('dotenv').config({path:__dirname+'/./../../../.env'});
const { logger, formatError } = require("../../utils/log");
const database = require("../../database");

const AZURE_STORAGE_CONNECTION_STRING = process.env.AZURE_STORAGE_CONNECTION_STRING;


async function main() {
    logger.info("Queueing Nightly Player Data Refresh");

    const queueClient = new QueueClient(AZURE_STORAGE_CONNECTION_STRING, "players-to-refresh");

    let allycodes = await database.db.any(`
    SELECT DISTINCT allycode, priority
    FROM guild_players 
    JOIN guild ON (guild_players.guild_id = guild.guild_id) 
    ORDER BY priority DESC`);

    for (let a of allycodes)
    {
        try {
            await queueClient.sendMessage(a.allycode, 
            { 
                messageTimeToLive: 82800,
                
            }); // queued request shouldn't live for longer than a day
        } catch (e) {
            logger.error(`Failed to queue ${a.allycode}. ${formatError(e)}`);
        }
    }

    logger.info(`Completed Nightly Player Data Refresh Queuing. Count: ${allycodes.length}`)
}

main().then(() => logger.info("Done")).catch((ex) => logger.error(ex.message));