const guildUtils = require("../../utils/guild.js");
const swapiUtils = require("../../utils/swapi.js")
const database = require("../../database.js");
const guildRefresh = require("../../commands/guild/refreshAllGuildData.js");
const guildRefreshRoster = require("../../commands/guild/refresh/refreshRoster.js");
const { logger, formatError } = require("../../utils/log.js");
const constants = require("../../utils/constants.js");
const comlink = require("../../utils/comlink.js");

const allycode = "317747263";
const discordId = "628359474377129985";


new Promise(async (resolve, reject) =>
{
    let guild;
    try {
        guild = await guildUtils.getGuildDataFromAPI({ allycode: allycode }, { useCache: false });
    }
    catch (err)
    {
        console.log(formatError(err));
        return;
    }

    let guildName = guildUtils.getGuildName(guild);
    let guildAllycodes = guildUtils.getGuildAllycodesForAPIGuild(guild);

    console.log(`Found guild **${guildName}** with ${guildAllycodes.length} members.`);

    let trackingGuild = await database.db.oneOrNone('SELECT track_data FROM guild WHERE comlink_guild_id = $1', [swapiUtils.getGuildId(guild)], r => r?.track_data);

    let matchingMember = guildUtils.getGuildRoster(guild).find(m => swapiUtils.getPlayerAllycode(m) === allycode);

    let leaderDiscordId, officerDiscordId, userDiscordId;
    let memberTypeText = null;
    switch (swapiUtils.getPlayerGuildMemberLevel(matchingMember))
    {
        case 4: 
            leaderDiscordId = discordId;
            memberTypeText = "leader";
            console.log("Found leader matching allycode.");
            break;
        case 3: 
            officerDiscordId = discordId;
            memberTypeText = "officer";
            console.log("Found officer matching allycode.");
            break;
        case 2:
            userDiscordId = discordId;
            memberTypeText = "member";
            console.log("Found member matching allycode.");
            break;
        default:
            console.log("Allycode provided does not match an officer or the guild leader.");
            return;
    }

    if (trackingGuild)
    {
        console.log("Guild **" + guildName + "** already created and tracked.");
    } else {
        if (trackingGuild === undefined) {
            await createGuildEntry(guild);
        }

        if (leaderDiscordId || officerDiscordId)
        {
            await startTrackingGuild(guild);
            await setupTwMetadata(guild);
        }                
    }

    let guildId = await guildUtils.getWookieeBotGuildIdForAPIGuild(guild);

    let existingRegistration = await database.db.any("SELECT * FROM user_registration WHERE allycode = $1", [allycode]);
    if (existingRegistration?.length > 0)
    {
        console.log(`Ally code ${allycode} already registered. Changing registration.`);
        await database.db.any("UPDATE user_registration SET discord_id = $1 WHERE allycode = $2", [discordId, allycode]);
    }
    else
    {
        console.log(`Registered ${allycode} to DiscordId: <@${discordId}>.`);
        await database.db.any(`
        INSERT INTO user_registration(allycode, discord_id)
            VALUES ($1, $2)
        ON CONFLICT DO NOTHING;`, [allycode, discordId]);
    }

    await database.db.any(`
    INSERT INTO user_guild_role(
        discord_id, guild_id, guild_admin, guild_bot_admin)
        VALUES ($1, $2, true, true)
    ON CONFLICT(discord_id, guild_id)
    DO UPDATE SET guild_admin = $3, guild_bot_admin = $3;`,
        [discordId, guildId, (leaderDiscordId || officerDiscordId) ? true : false])

    console.log(`Set up ${memberTypeText} with ${(leaderDiscordId || officerDiscordId) ? "admin" : "generic member"} privileges.`);

    database.db.$pool.end();
});

async function setupTwMetadata(guild) 
{
    
    let guildId = await guildUtils.getWookieeBotGuildIdForAPIGuild(guild);
    // initialize TW planner settings
    const twMetadata = await database.db.oneOrNone("SELECT guild_id FROM tw_metadata WHERE guild_id = $1", guildId);
    if (!twMetadata)
    {
        try {
            await database.db.any("INSERT INTO tw_metadata (max_teams_per_territory, default_max_squads_per_player, default_max_fleets_per_player, guild_id) VALUES ($1, $2, $3, $4)",
            [50, 15, 3, guildId]);
        } catch (e)
        {
            console.log("Error initializing TW settings: " + (e.message ? e.message : e));
            return;
        }
        console.log("Initialized TW settings.  Check with **/tw config**.");
    }
    
}

async function startTrackingGuild(guild)
{
    let guildName = guildUtils.getGuildName(guild);
    console.log("Starting to track guild **" + guildName + "**.");

    let comlink_guild_id = swapiUtils.getGuildId(guild);
    await database.db.any("UPDATE guild SET track_data = TRUE WHERE comlink_guild_id = $1", [comlink_guild_id]);

    // populate guild players
    await guildUtils.refreshGuildPlayers(guild);
    
    console.log("Turned guild tracking on & populated guild players. Getting player data...");

    // grab an initial set of player data
    await guildRefresh.populatePlayerData(guild, constants.CacheTypes.MANUAL);
    
    console.log("Populated player data.");
}

async function createGuildEntry(guild)
{
    // records to be updated:
    var guildInsertData = new Array();

    // declare ColumnSet once, and then reuse it:
    const guildCS = new database.pgp.helpers.ColumnSet(['guild_name', 'comlink_guild_id', 'description', 'track_data'], {table: 'guild'});

    let comlink_guild_id = swapiUtils.getGuildId(guild);

    guildInsertData.push({
        guild_name: guildUtils.getGuildName(guild),
        description: guildUtils.getGuildDescription(guild),
        comlink_guild_id: comlink_guild_id,
        track_data: false
    });

    const guildInsert = database.pgp.helpers.insert(guildInsertData, guildCS);

    // executing the query:
    await database.db.none(guildInsert);

    // populate guild players
    await guildUtils.refreshGuildPlayers(guild);
    
    console.log("Created guild entry & populated guild players.");
}