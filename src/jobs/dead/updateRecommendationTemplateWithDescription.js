const got = require("got");
const swapi = require("../../utils/swapi");
const database = require("../../database");
const recCommon = require ("../../commands/guild/rec/common");

new Promise(async (resolve, reject) =>
{
    let unitsList = await swapi.getUnitsList();
    let omicronsList = await swapi.getOmicronAbilitiesList();

    let missing = await database.db.any("SELECT * FROM guild_recommendation_template WHERE description IS NULL");

    for (let m of missing)
    {
        let id = m.guild_recommendation_template_id;
        
        let omiMatch = omicronsList.find(o => o.base_id === m.value);
        if (!omiMatch) {
            console.error("Invalid omicron: " + m.value);
            continue;
        }

        let omiSpot = recCommon.ABILITY_TYPES[omiMatch.type].long;
        let desc = `${omiMatch.unitName} (${omiSpot}): ${omiMatch.name}`;

        await database.db.any("UPDATE guild_recommendation_template SET description = $1 WHERE guild_recommendation_template_id = $2",
            [desc, id]);
    }

    database.db.$pool.end();
})