const database = require("../../database");
const statCalc = require("../../utils/stats/stats");
const constants = require("../../utils/constants");
const swapiUtils = require("../../utils/swapi");
const playerUtils = require("../../utils/player");
const recommendationsUtils = require("../../utils/recommendations");
const dotenv = require("dotenv");
const parse = require("csv-parse/lib/sync");
dotenv.config({ path: ".env"});
const fs = require('fs');
const raidCommon = require("../../commands/raid/raidCommon");

const path = "./src/jobs/on-demand/raid_team_data.csv"

let NABOO_ALLOWED_UNITS = [
    "BOSSNASS",
    "CAPTAINTARPALS",
    "BOOMADIER",
    "GUNGANPHALANX",
    "JARJARBINKS",
    "QUEENAMIDALA",
    "PADAWANOBIWAN",
    "MASTERQUIGON",
    "R2D2_LEGENDARY",
    "MACEWINDU",
    "JEDIKNIGHTCONSULAR",
    "JEDIKNIGHTGUARDIAN",
    "KELLERANBEQ",
    "EETHKOTH",
    "GRANDMASTERYODA",
    "PLOKOON",
    "KIADIMUNDI",
    "AAYLASECURA",
    "SHAAKTI",
    "LUMINARAUNDULI",
    "KITFISTO",
    "QUIGONJINN",
    "NUTEGUNRAY",
    "B1BATTLEDROIDV2",
    "STAP",
    "DROIDEKA",
    "B2SUPERBATTLEDROID",
    "MAGNAGUARD",
    "MAUL",
    "DARTHSIDIOUS",
];

let ENDOR_ALLOWED_UNITS = [
    "ADMIRALACKBAR",
    "C3POLEGENDARY",
    "CAPTAINREX",
    "CAPTAINDROGAN",
    "CHEWBACCALEGENDARY",
    "HANSOLO",
    "HERASYNDULLAS3",
    "JEDIKNIGHTLUKE",
    "ADMINISTRATORLANDO",
    "GLLEIA",
    "WEDGEANTILLES",
    "CHIEFCHIRPA",
    "EWOKELDER",
    "EWOKSCOUT",
    "LOGRAY",
    "PAPLOO",
    "PRINCESSKNEESAA",
    "TEEBO",
    "WICKET",
    "ADMIRALPIETT",
    "COLONELSTARCK",
    "DEATHTROOPER",
    "VEERS",
    "IDENVERSIOEMPIRE",
    "MAGMATROOPER",
    "MOFFGIDEONS1",
    "RANGETROOPER",
    "SCOUTTROOPER_V3",
    "SHORETROOPER",
    "STORMTROOPER"
];

const initOptions = {
    // initialization options;
};

const dbConfigProd = {
    "host": process.env.DB_HOST,
    "port": process.env.DB_PORT,
    "database": "wookieebot",
    "user": process.env.DB_USERNAME,
    "password": process.env.DB_PASSWORD
    // "ssl": process.env.DB_SSL
}

const dbConfigDev = {
    "host": process.env.DB_HOST,
    "port": process.env.DB_PORT,
    "database": "wookieebot_dev",
    "user": process.env.DB_USERNAME,
    "password": process.env.DB_PASSWORD
    // "ssl": process.env.DB_SSL
}

const pgp = require('pg-promise')(initOptions);

const dbProd = pgp(dbConfigProd);
const dbDev = pgp(dbConfigDev);

const raidId = "NABOO";
// const tierCount = raidId === "NABOO" ? 8 : 7;
const leaderUnitId = "KELLERANBEQ";
const ultRequired = (leaderUnitId === "GLLEIA" && false);
const unitsRequired = ["SHAAKTI", "KIADIMUNDI", "MACEWINDU", "AAYLASECURA"];
const omicronsRequired = null;
// const omicronsRequired = ["uniqueskill_BOOMADIER01"];
const zetasRequired = null;
const autoFactor = 1; 
const qualityByTier = null;
// const qualityByTier = new Array(7).fill(Math.floor(1314/2700*100));
// const qualityByTier = new Array(tierCount).fill(100); // Math.floor((2100/2700 - 0.05)*100));
// const qualityByTier = [
//     60,  // <g12
//     63,  // g12
//     66,  // r1
//     69,  // r3
//     72,  // r5
//     75,  // r7
//     78   // r8
// ];
const note = null;

const defaultExactScore = null;
const defaultNote = null;
// `Mods: https://cdn.discordapp.com/attachments/1257144612368158880/1260460151190851594/toon.png?ex=668f667e&is=668e14fe&hm=7e12946bdd540d08e6c3384df37d900a80623b40e3d45f4c4ef159f65117e235&`;
// `All units fast.\nBeq: Health sets, Protection primaries.\nShaak: Speed set, health primaries\nMace: Health/Defense sets, health primaries\nKAM: Offense+health sets, offense primaries\nAayla: Offense+health sets, health primaries`;
// `Speed on Nass, Offense on Tarpals & Boomadier, Health on Phalanx`; 
// `Can mostly auto, but use Shaak basic to dispel damage immunity from Droideka.`;
// const defaultNote = `Auto with targeting: Droideka > STAP > Others`


let allyCode = null;
// allyCode = "931718874";
// allyCode = '134232169'; // quig
// allyCode = '376941286'; // valentine
// allyCode = '886776687'; // taliana
// allyCode = '841626362'; // rancor caulk
// allyCode = '277578176'; // matexx
// allyCode = '339292726';
// allyCode = '158929731'; // jedi master adastra
allyCode = '985736123'; // me
// allyCode = '366589251'; // velarier
// allyCode = '482676681'; // calvin awesome, I think
// allyCode = '974441699'; // gammamatrix
// allyCode = '457416549'; // ktow kanobee
// allyCode = '227838835'; // scorch
// allyCode = '399234324'; // clockan
// allyCode = '835294833'; // quixote
// allyCode = '841626362'; // pariah
// allyCode = '658799425'; // zeroCool
// allyCode = '158929731'; // adastra
// allyCode = '411536365'; // nomadzreaper - most R9s
// allyCode = '119983224'; // svephen uk
// allyCode = '968474237'; // solo bass


let discordId = null;
discordId = '266029916649160714';

const defaultModQuality = 10;

const dataPoint = {
    score: 1980000,
    effort: recommendationsUtils.EFFORT.MEDIUM.value,
    tier: 6,
    allycode: allyCode,
    modQuality: null,
    relicsAboveMinimum: 0,
    gearBelowMinium: 0,
    gearAboveMinium: 0
};


// add data point with score, auto, tier
// when loading teams, take all scores & tie

const tierData = [
    {   // 0
        exactScore: defaultExactScore,
        note: defaultNote
    },
    {   // 1
        exactScore: defaultExactScore,
        note: defaultNote
    },
    {   // 2
        exactScore: defaultExactScore,
        note: defaultNote
    },
    {   // 3
        exactScore: defaultExactScore,
        note: defaultNote
    },
    {   // 4
        exactScore: defaultExactScore,
        note: defaultNote
    },
    {   // 5
        exactScore: defaultExactScore,
        note: defaultNote
    },
    {   // 6
        exactScore: defaultExactScore,
        note: defaultNote
    },
    {   // 7
        exactScore: defaultExactScore,
        note: defaultNote
    },
];

new Promise(async (resolve, reject) =>
{
    if (path)
    {
        let file = fs.readFileSync(path);
        
        const raidData = parse(file, { columns: true, encoding: "ASCII" });

        for (let r of raidData)
        {
            if (!r.leader) continue; 
            let raidId = "NABOO";
            let leaderUnitId = r.leader;
            let unitsRequired = [];
            let omicronsRequired = null;
            let zetasRequired = null;
            for (let n = 1; n <= 4; n++)
            {
                if (r[`unit${n}`]?.length > 0) unitsRequired.push(r[`unit${n}`]);
            }

            if (r[`omicron`]?.length > 0) omicronsRequired = [r[`omicron`]];
            if (r[`zeta`]?.length > 0) zetasRequired = [r[`zeta`]];

            let dataPoint = {
                score: parseInt(r.score),
                effort: parseInt(r.effort),
                tier: parseInt(r.tier),
                allycode: r.allycode,
                modQuality: null,
                relicsAboveMinimum: 0,
                gearBelowMinium: 0,
                gearAboveMinium: 0
            }

            let tierData = [];

            for (let t = 0; t <= 7; t++)
            {
                tierData.push({
                    exactScore: null,
                    note: null,
                    extrapolate: true
                });

                if (r[`tier_score_${t}`]?.length > 0) tierData[t].exactScore = parseInt(r[`tier_score_${t}`]);
                if (r[`tier_note_${t}`]?.length > 0) tierData[t].note = r[`tier_note_${t}`];
                if (r[`extrapolate_tier_${t}`]?.length > 0) tierData[t].extrapolate = JSON.parse(r[`extrapolate_tier_${t}`]);
            }

            await addData(
                raidId,
                leaderUnitId,
                unitsRequired,
                r.allycode,
                null, // discord Id
                dataPoint,
                false,  // ultRequired
                omicronsRequired,
                zetasRequired,
                null, // qualityByTier
                null, // note
                null, // autoFactor
                tierData
            );
        }
    }
    else
    {
        await addData(raidId, leaderUnitId, unitsRequired, allyCode, discordId, dataPoint, ultRequired, omicronsRequired, null, qualityByTier, note, autoFactor, tierData);
    }
    
    closeDbConnections();
});

async function addData(raidId, leaderUnitId, unitsRequired, allyCode, discordId, dataPoint, ultRequired, omicronsRequired, zetasRequired, qualityByTier, note, autoFactor, tierData)
{
    if (!NABOO_ALLOWED_UNITS.find(u => u === leaderUnitId))
    {
        console.log(`Invalid leader unit: ${leaderUnitId}`);
        return;
    }

    for (let u = 0; u < unitsRequired.length; u++)
    {
        if (!NABOO_ALLOWED_UNITS.find(unit => unit === unitsRequired[u]))
        {
            console.log(`Invalid unit in unitsRequired: ${unitsRequired[u]}`);
            return;
        }
    }

    if (!allyCode && discordId?.length > 0)
    {
        const ac = await database.db.oneOrNone("SELECT allycode FROM user_registration WHERE discord_id = $1 LIMIT 1", [discordId]);
        if (ac?.allycode) { allyCode = ac.allycode; dataPoint.allycode = ac.allycode; }
        else {
            console.log("No player registered with that discord ID");
            return;
        }
    }
    if (allyCode)
    {
        
		await statCalc.initCalc();
        let reportingPlayer = await swapiUtils.getPlayer(allyCode);

        console.log(`Found player: ${swapiUtils.getPlayerName(reportingPlayer)} (${swapiUtils.getPlayerAllycode(reportingPlayer)})`);
        console.log(`Team: ${leaderUnitId}, ${unitsRequired?.join(", ")}`);

        if (reportingPlayer) 
        {
            dataPoint.modQuality = playerUtils.calculateModScore(reportingPlayer, constants.MOD_SCORE_TYPES.WOOKIEEBOT);

            let minimumsForTier = raidCommon[raidId].DIFFICULTY_MINIMUMS[dataPoint.tier];

            let allTeamUnits = unitsRequired.concat(leaderUnitId);
            let unitsInTeam = swapiUtils.getPlayerRoster(reportingPlayer).filter(u => allTeamUnits.find(atu => atu === swapiUtils.getUnitDefId(u)));


            if (unitsInTeam.length != allTeamUnits.length)
            {
                console.log("Error: Player doesn't have all units.");
                return;
            }

            for (let u of unitsInTeam)
            {
                let unitRelic = swapiUtils.getUnitRelicLevel(u) - 2;
                if (minimumsForTier.minRelicLevel)
                {
                    if (unitRelic < minimumsForTier.minRelicLevel) 
                    {
                        console.log(`Error: Unit ${u.name} is less than minimum relic tier.\nCurrent relic: ${unitRelic}\nMin relic: ${minimumsForTier.minRelicLevel}`);                    
                        return;
                    }
                    dataPoint.relicsAboveMinimum += (unitRelic - minimumsForTier.minRelicLevel);
                } 
                else if (minimumsForTier.minGearLevel)
                {
                    // gear check
                    let unitGearLevel = swapiUtils.getUnitGearLevel(u);
                    
                    if (unitGearLevel < minimumsForTier.minGearLevel) 
                    {
                        console.log(`Error: Unit ${u.name} is less than minimum gear.\nCurrent gear: ${unitGearLevel}\nMin gear: ${minimumsForTier.minGearLevel}`);                    
                        return;
                    }

                    if (unitGearLevel < raidCommon[raidId].MIN_RECOMMENDED_GEAR)
                    {
                        dataPoint.gearBelowMinium += (raidCommon[raidId].MIN_RECOMMENDED_GEAR - unitGearLevel);
                    } else {
                        dataPoint.gearAboveMinium += (unitGearLevel - raidCommon[raidId].MIN_RECOMMENDED_GEAR);
        
                        // this tier doesn't require relics. If the player has relics, though, then boost the quality more
                        if (unitRelic > 0) dataPoint.relicsAboveMinimum += unitRelic;
                    }
                }
            }
        }
    }

    if (dataPoint.modQuality == null) dataPoint.modQuality = defaultModQuality;
    
    const teamName = leaderUnitId + (ultRequired ? " (ult)" : "") + ", " + unitsRequired.join(", ");
    const teamSize = 1 + unitsRequired.length;

    let match = await dbDev.oneOrNone(
        `SELECT * 
        FROM raid_team 
        WHERE leader_unit_id = $1 
            AND $2 <@ units_required::text[] 
            AND ult_required = $3 
            AND raid_id = $4 
            AND team_size = $5 
            ${omicronsRequired != null ? "AND ( ($6 @> omicrons_required::text[]) AND (omicrons_required::text[] <@ $6) )" : "AND omicrons_required IS NULL"}
            ${zetasRequired != null ? "AND ( ($7 @> zetas_required::text[]) AND (zetas_required::text[] <@ $7) )" : "AND zetas_required IS NULL"}`,
        [leaderUnitId, unitsRequired, ultRequired, raidId, teamSize, omicronsRequired, zetasRequired]);

    if (match)
    {
        if (!match.data_points) match.data_points = [];
        match.data_points.push(dataPoint);

        await dbDev.any(
        `UPDATE raid_team
         SET data_points = $1::jsonb[]
         WHERE raid_team_id = $2
        `, [match.data_points, match.raid_team_id]);
        console.log("Added datapoint to dev.");
    }
    else
    {
        await dbDev.any(`INSERT INTO raid_team (name, units_required, raid_id, team_size, omicrons_required, quality_by_tier, leader_unit_id, note, auto_factor, ult_required, tier_data, data_points, zetas_required)
            VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11::json[], $12::json[], $13)`,
            [teamName, unitsRequired, raidId, teamSize, omicronsRequired, qualityByTier, leaderUnitId, note, autoFactor, ultRequired, tierData, [dataPoint], zetasRequired]);
        console.log("Inserted data into dev.");
    }
        
    let prodMatch = await dbProd.oneOrNone(
    `SELECT * 
    FROM raid_team 
    WHERE leader_unit_id = $1 
        AND $2 <@ units_required::text[] 
        AND ult_required = $3 
        AND raid_id = $4 
        AND team_size = $5 
        ${omicronsRequired != null ? "AND ( ($6 @> omicrons_required::text[]) AND (omicrons_required::text[] <@ $6) )" : "AND omicrons_required IS NULL"}
        ${zetasRequired != null ? "AND ( ($7 @> zetas_required::text[]) AND (zetas_required::text[] <@ $7) )" : "AND zetas_required IS NULL"}`,
    [leaderUnitId, unitsRequired, ultRequired, raidId, teamSize, omicronsRequired, zetasRequired]);


    if (!prodMatch) {
        await dbProd.any(`INSERT INTO raid_team (name, units_required, raid_id, team_size, omicrons_required, quality_by_tier, leader_unit_id, note, auto_factor, ult_required, tier_data, data_points, zetas_required)
            VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11::json[], $12::json[], $13)`,
            [teamName, unitsRequired, raidId, teamSize, omicronsRequired, qualityByTier, leaderUnitId, note, autoFactor, ultRequired, tierData, [dataPoint], zetasRequired]);
        console.log("Inserted data into prod.");
    } else {    
        // update prod data here separately in case they're different for whatever reason
        if (!prodMatch.data_points) prodMatch.data_points = [];
        prodMatch.data_points.push(dataPoint);

        await dbProd.any(
        `UPDATE raid_team
        SET data_points = $1::jsonb[]
        WHERE raid_team_id = $2
        `, [prodMatch.data_points, prodMatch.raid_team_id]);
        console.log("Added datapoint to prod.");
    }



}

function closeDbConnections()
{
    dbDev.$pool.end();
    dbProd.$pool.end();
    database.db.$pool.end();
}
