require('dotenv').config({path:__dirname+'/./../../../.env'});
let comlink = require("../../utils/comlink")
let database = require ("../../database")
let swapi = require("../../utils/swapi")

new Promise(async (resolve, reject) => {
    let dcData = await comlink.getDatacronData();
    let units = await swapi.getUnitsList();
    let categoryData = await comlink.getCategories();

    let affixes = dcData.datacronAffixTemplateSet;
    let templates = dcData.datacronTemplate;
    let sets = dcData.datacronSet;

    let dcInsertData = new Array();
    let dcTemplateInsertData = new Array();

    for (var t = 0; t < templates.length; t++)
    {
        
        let setId = templates[t].setId;
        if (setId != 20) continue;
        
        let template_id = templates[t].id;

        let expirationTimeMs = sets.find(s => s.id === setId).expirationTimeMs;
        let expirationDate = new Date(parseInt(expirationTimeMs));
        
        for (var tier = 0; tier < templates[t].tier.length; tier++)
        {
            let curTier = templates[t].tier[tier];

            for (var affixTemplateSetId in curTier.affixTemplateSetId)
            {
                let affixId = curTier.affixTemplateSetId[affixTemplateSetId];

                if (!dcTemplateInsertData.find(t => t.template_id === template_id && t.affix_id === affixId))
                {
                    dcTemplateInsertData.push(
                        {
                            template_id: template_id,
                            set_id: setId,
                            affix_id: affixId,
                            expiration_dt: expirationDate
                        }
                    );
                }
                
                let affixSet = affixes.find(aff => aff.id === affixId);
                
                if (!affixSet) continue;

                for (var ability in affixSet.affix)
                {
                    let abilityObject = affixSet.affix[ability];
                    if (abilityObject.abilityId == null || abilityObject.abilityId.length == 0) continue;

                    let iconName = abilityObject.scopeIcon.replace("tex.", "");
                    let abilityId = abilityObject.abilityId;
                    let tags = abilityObject.tag;
                    let targetRule = abilityObject.targetRule;
                    let alignment = null, faction = null, character = null;

                    // alignment
                    if (tier >= 2)
                    {
                        if (tags.indexOf("darkside") >= 0) alignment = "alignment_dark";
                        else if (tags.indexOf("lightside") >= 0) alignment = "alignment_light";
                    }

                    // faction
                    if (tier >= 5)
                    {
                        if (tags.indexOf("smuggler") >= 0) faction = "profession_smuggler";
                        else if (tags.indexOf("wookiee") >= 0) faction = "species_wookiee";
                        else if (tags.indexOf("ewok") >= 0) faction = "species_ewok";
                        else if (tags.indexOf("jedi") >= 0) faction = "profession_jedi";
                        else if (tags.indexOf("huttcartel") >= 0) faction = "affiliation_huttcartel";
                        else if (tags.indexOf("empire") >= 0) faction = "affiliation_empire";
                        else if (tags.indexOf("rebels") >= 0) faction = "affiliation_rebels";
                        else if (tags.indexOf("rebel") >= 0) faction = "affiliation_rebels";
                        else if (tags.indexOf("resistance") >= 0) faction = "affiliation_resistance";
                        else if (tags.indexOf("phoenix") >= 0) faction = "affiliation_phoenix";
                        else if (tags.indexOf("firstorder") >= 0) faction = "affiliation_firstorder";
                        else if (tags.indexOf("bountyhunter") >= 0) faction = "profession_bountyhunter";
                        else if (tags.indexOf("inquisitorius") >= 0) faction = "profession_inquisitorius";
                        else if (tags.indexOf("scoundrel") >= 0) faction = "profession_scoundrel";
                        else if (tags.indexOf("separatist") >= 0) faction = "affiliation_separatist";
                        else if (tags.indexOf("droid") >= 0) faction = "species_droid";
                        else if (tags.indexOf("jawa") >= 0) faction = "species_jawa";
                        else if (tags.indexOf("unalignedforceuser") >= 0) faction = "unaligned_force_user";
                        else if (tags.indexOf("unaligned_force_user") >= 0) faction = "unaligned_force_user";
                        else if (tags.indexOf("tusken") >= 0) faction = "species_tusken";
                        else if (tags.indexOf("mandalorian") >= 0) faction = "affiliation_mandalorian";
                        else if (tags.indexOf("nightsister") >= 0) faction = "affiliation_nightsisters";
                        else if (tags.indexOf("sithempire") >= 0) faction = "affiliation_sithempire";
                        else if (tags.indexOf("sith") >= 0) faction = "profession_sith";
                        else if (tags.indexOf("galacticrepublic") >= 0) faction = "affiliation_republic";
                        else if (tags.indexOf("oldrepublic") >= 0) faction = "affiliation_oldrepublic";
                        else if (tags.indexOf("clone") >= 0) faction = "profession_clonetrooper";
                        else if (tags.indexOf("imperialtrooper") >= 0) faction = "affiliation_imperialtrooper";
                        else if (tags.indexOf("rebelfighter") >= 0) faction = "affiliation_rebelfighter";
                        else if (tags.indexOf("gungan") >= 0) faction = "species_gungan";
                        else if (tags.indexOf("padawanobiwan") >= 0) faction = "selftag_padawanobiwan";
                        else if (tags.indexOf("greatmothers") >= 0) faction = "selftag_greatmothers";
                        else if (tags.indexOf("nighttrooper") >= 0) faction = "selftag_nighttrooper";
                        else if (tags.indexOf("baylanskoll") >= 0) faction = "selftag_baylanskoll";
                        else if (tags.indexOf("hunters3") >= 0) faction = "selftag_hunters3";
                        else if (tags.indexOf("support") >= 0) faction = "role_support";
                        else if (tags.indexOf("tank") >= 0) faction = "role_tank";
                        else if (tags.indexOf("healer") >= 0) faction = "role_healer";
                        else if (tags.indexOf("attacker") >= 0) faction = "role_attacker";
                        else if (tags.indexOf("rogueone") >= 0) faction = "affiliation_rogue_one";
                        else if (tags.indexOf("imperialremnant") >= 0) faction = "affiliation_imperialremnant";
                        else if (tags.indexOf("mercenary") >= 0) faction = "profession_mercenary";
                        else {
                            console.log("No tag match for `" + tags + "`")
                            return;
                        }
                    }

                    // character
                    if (tier == 8)
                    {
                        let partialId = targetRule.replace("target_datacron_", "");
                        try {
                            if (partialId === "ig86") partialId = "IG86SENTINELDROID";
                            if (partialId === "t3-m4") partialId = "T3_M4";
                            if (partialId === "l3-37") partialId = "L3_37";

                            let unitMatch = units.find(u => swapi.getUnitDefId(u).localeCompare(partialId, undefined, {sensitivity: 'accent' }) === 0);
                            if (!unitMatch) {
                                unitMatch = units.find(u => swapi.getUnitDefId(u).localeCompare(partialId.replace("_", ""), undefined, {sensitivity: 'accent' }) === 0);
                            }
                            if (!unitMatch) {
                                unitMatch = units.find(u => swapi.getUnitDefId(u).replace("_", "").localeCompare(partialId, undefined, {sensitivity: 'accent' }) === 0);
                            }
                            character = swapi.getUnitDefId(unitMatch);
                        } catch (e)
                        {
                            if (targetRule.indexOf("padawanobiwan") != -1 ) { character = "PADAWANOBIWAN"; }
                            else if (targetRule.indexOf("nighttrooper") != -1 ) { character = "NIGHTTROOPER"; }
                            else if (targetRule.indexOf("hunters3") != -1 ) { character = "HUNTERS3"; }
                            else {
                                console.log("No unit found matching target rule: " + targetRule);
                                return;
                            }
                        }
                    }

                    dcInsertData.push(
                        {
                            ability_id: abilityId,
                            set_id: setId,
                            affix_id: affixId,
                            target_rule: targetRule,
                            tags: tags,
                            alignment: alignment,
                            faction: faction,
                            character: character,
                            icon_asset_name: iconName
                        }
                    )
            
                }
            }
            
        }
    }
    
    const datacronInsert = database.pgp.helpers.insert(dcInsertData, database.columnSets.datacronCS);
    
    const datacronOnConflict = ' ON CONFLICT(set_id, affix_id, ability_id) DO NOTHING';

    await database.db.none(datacronInsert + datacronOnConflict);
    
    const datacronTemplateInsert = database.pgp.helpers.insert(dcTemplateInsertData, database.columnSets.datacronTemplateCS);
    
    const datacronTemplateOnConflict = ' ON CONFLICT(affix_id, template_id) DO UPDATE SET expiration_dt = EXCLUDED.expiration_dt';

    await database.db.none(datacronTemplateInsert + datacronTemplateOnConflict);

    database.db.$pool.end();
})