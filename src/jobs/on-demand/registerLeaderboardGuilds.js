
require('dotenv').config({path:__dirname+'/./../../../.env'});
const { logger, formatError } = require("../../utils/log");
const database = require("../../database");
const metadataCache = require("../../utils/metadataCache");
const comlink = require ("../../utils/comlink");
const got = require ("got");
const swapiUtils = require ("../../utils/swapi");

new Promise(async (resolve, reject) =>
{
    logger.info("Getting GP leaderboard");
    let allLeaderboards = await comlink.getAllGuildLeaderboards();

    let guildValues = [];

    for (let l of allLeaderboards.leaderboard)
    {
        guildValues = guildValues.concat(l.guild.filter(g => !guildValues.find(gv => gv.comlink_guild_id === g.id)).map(g => 
        {
            return {
                comlink_guild_id: g.id,
                track_data: true,
                description: g.externalMessageKey,
                guild_name: g.name,
                logo: null,
                priority: 0,
                fixed_name: null
            };
        }));
    }

    const guildInsert = database.pgp.helpers.insert(guildValues, database.columnSets.guildCS);
    await database.db.none(guildInsert + " ON CONFLICT (comlink_guild_id) DO UPDATE SET track_data = true");
    
    database.db.$pool.end();
});