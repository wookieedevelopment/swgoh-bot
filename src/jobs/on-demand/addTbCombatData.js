const dotenv = require("dotenv");
dotenv.config({ path: ".env"});
const fs = require('fs');

const initOptions = {
    // initialization options;
};

const dbConfigProd = {
    "host": process.env.DB_HOST,
    "port": process.env.DB_PORT,
    "database": "wookieebot",
    "user": process.env.DB_USERNAME,
    "password": process.env.DB_PASSWORD
    // "ssl": process.env.DB_SSL
}

const dbConfigDev = {
    "host": process.env.DB_HOST,
    "port": process.env.DB_PORT,
    "database": "wookieebot_dev",
    "user": process.env.DB_USERNAME,
    "password": process.env.DB_PASSWORD
    // "ssl": process.env.DB_SSL
}

const pgp = require('pg-promise')(initOptions);

const dbProd = pgp(dbConfigProd);
const dbDev = pgp(dbConfigDev);

const unitsRequired = ["VADER"];
const unitOptions = null;
const factionOptions = null;
const planet = "Death Star";
const mission = 4;
const teamName = unitsRequired[0] + "_" + planet + "_" + mission;
const notes = null;

const quality = 100;
const teamSize = 5;
const omicronsRequired = null;

new Promise(async (resolve, reject) =>
{
    await dbProd.any(`INSERT INTO tb_combat_team (team_name, units_required, unit_options, faction_options, planet, mission, notes, quality, team_size, mission_type, is_special_mission, omicrons_required)
    VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, 
        (SELECT combat_type FROM tb_mission WHERE planet = $5 AND mission = $6),
        (SELECT is_special FROM tb_mission WHERE planet = $5 AND mission = $6),
        $10)`,
    [teamName, unitsRequired, unitOptions, factionOptions, planet, mission, notes, quality, teamSize, omicronsRequired]);
    console.log("Inserted data into dev.");
    dbProd.$pool.end();

    await dbDev.any(`INSERT INTO tb_combat_team (team_name, units_required, unit_options, faction_options, planet, mission, notes, quality, team_size, mission_type, is_special_mission, omicrons_required)
    VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, 
        (SELECT combat_type FROM tb_mission WHERE planet = $5 AND mission = $6),
        (SELECT is_special FROM tb_mission WHERE planet = $5 AND mission = $6),
        $10)`,
    [teamName, unitsRequired, unitOptions, factionOptions, planet, mission, notes, quality, teamSize, omicronsRequired]);
    console.log("Inserted data into prod.");

    dbDev.$pool.end();
});
