
require('dotenv').config({path:__dirname+'/./../../../.env'});
const { logger, formatError } = require("../../utils/log");
const database = require("../../database");
const metadataCache = require("../../utils/metadataCache");
const comlink = require ("../../utils/comlink");
const got = require ("got");
const swapiUtils = require ("../../utils/swapi");

const aeUrlBase = "http://localhost:3000";

const DELAY_BETWEEN_QUERIES = 200; // 200ms
function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

const excludePrefixes = [
    "ability_char_", 
    "ability_event_", 
    "ability_rancor_", 
    "ability_reek_", 
    "ability_ship_", 
    "battle_character_",
    "battle_character_event_",
    "battle_event_",
    "battleui_",
    "challengeui_",
    "eventsparent_",
    "territoryui_",
    "territorymap_",
    "storeui_",
    "packopening_datacard_",
    "filtersort_",
    "modenhancement_",
    "leaderboardui_",
    "vfx_",
    "charactermods_",
    "cin_event_",
    "datacrons_",
    "guildraidmission_",
    "guildactivities_",
    "guildraid_",
    "inboxui_",
    "inventoryui_",
    "pack_gear_mk12plus_",
    "scv_",
    "territorybattleui_",
    "territorytournamentui_",
    "ui_",
    "unitdetailsui_",
    "unitguide_"
];

const excludePostfixes = [
    "_pre"
]

new Promise(async (resolve, reject) =>
{
    let metadata = await comlink.getMetadata();
    let assetVersion = metadata.assetVersion;

    let lastVersion = await database.db.one(
            `SELECT distinct version::text FROM asset_image
            order by version::text desc
            limit 1`);

    let assetList = JSON.parse((await got(aeUrlBase + `/Asset/listDiff?version=${assetVersion}&diffVersion=${lastVersion.version}`)).body);
    //let assetList = JSON.parse((await got(aeUrlBase + `/Asset/list?version=${assetVersion}`)).body);

    assetList = assetList.filter(a => !excludePrefixes.find(p => a.startsWith(p)) && !excludePostfixes.find(p => a.endsWith(p)));

    for (let asset of assetList)
    {
        let url = `${aeUrlBase}/Asset/single/?version=${assetVersion}&assetName=${asset}`;
        let img;
        try {
            img = await got(url).buffer();
        } catch (e)
        {
            console.log(`Failed to retrieve asset: ${asset}`);
            continue;
        }
        await database.db.any(`
            INSERT INTO asset_image (version, asset_name, img) 
            VALUES ($1, $2, $3) 
            ON CONFLICT (asset_name) DO UPDATE SET version = $1, img = $3`, 
            [
                assetVersion,
                asset,
                img
            ]);

        console.log(`Added asset ${asset}`);
        //await sleep(DELAY_BETWEEN_QUERIES);
    }

    database.db.$pool.end();
});