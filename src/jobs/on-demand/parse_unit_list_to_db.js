require('dotenv').config({ path: __dirname + '/./../../../.env' });
const swapi = require("../../utils/swapi");
const database = require("../../database");

new Promise(async (resolve, reject) => {
  try {
    // Fetch unit data from SWGOH API
    const unitsList = await swapi.getUnitsList();

    for (const unit of unitsList) {
      const { 
        base_id: unit_def_id, 
        name, 
        is_galactic_legend: gl, 
        is_capital_ship: cs, 
        combat_type, 
        crew_base_ids 
      } = unit;

      // Check if unit_def_id already exists in database
      const result = await database.db.oneOrNone("SELECT COUNT(*) FROM unit WHERE unit_def_id = $1", [unit_def_id]);
      if (result.count > 0) {
        // Update existing unit
        await database.db.none(`
          UPDATE unit
          SET name = $2, gl = $3, cs = $4, combat_type = $5, crew_base_ids = $6
          WHERE unit_def_id = $1
        `, [unit_def_id, name, gl, cs, combat_type, JSON.stringify(crew_base_ids)]);
      } else {
        // Insert new unit
        await database.db.none(`
          INSERT INTO unit (unit_def_id, name, gl, cs, combat_type, crew_base_ids)
          VALUES ($1, $2, $3, $4, $5, $6)
          ON CONFLICT (unit_def_id) DO UPDATE
          SET name = excluded.name, gl = excluded.gl, cs = excluded.cs, combat_type = excluded.combat_type, crew_base_ids = excluded.crew_base_ids
        `, [unit_def_id, name, gl, cs, combat_type, JSON.stringify(crew_base_ids)]);
      }

      console.log(`Upserted unit: ${name}`);
    }

    console.log('All units upserted successfully.');
    resolve();

  } catch (error) {
    console.error('Error upserting units:', error);
    reject(error);

  } finally {
    // Close database connection pool
    await database.db.$pool.end();
  }
});
