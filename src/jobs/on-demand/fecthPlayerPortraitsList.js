
require('dotenv').config({path:__dirname+'/./../../../.env'});
const fs = require('fs');
const comlink = require("../../utils/comlink");
const database = require("../../database");
const got = require("got");

// //Function to 
// function saveDataToJsonFile(data, filePath) {
//   const jsonData = JSON.stringify(data, null, 2);

//   fs.writeFile(filePath, jsonData, err => {
//     if (err) {
//       console.error('Error saving data to JSON file:', err);
//     } else {
//       console.log('Data has been successfully saved to JSON file.');
//     }
//   });
// }
// //Runs the function from swapi to grab the gamedata info for playerPortrait
// let portraitList = (swapi.getPlayerPortriatsList())
// //filepath of the gamedata JSOn holding the player portraits info 
// const filePath = './src/data/gamedata.json';
// //Run the function and await the result from the promise => then save it to the gamedata.json
// portraitList.then(function (result) {
//   userData = (result.playerPortrait.playerPortrait)
//   saveDataToJsonFile(userData, filePath);
// })



new Promise(async (resolve, reject) => {

  let portraitsList = await comlink.getPlayerPortriats();

  for (var ix in portraitsList) {
    let portrait = portraitsList[ix];
    let url = `https://game-assets.swgoh.gg/textures/${portrait.icon}.png`;
    let result = await database.db.any("SELECT COUNT(*) FROM player_portrait WHERE player_portrait_id = $1", [portrait.id]);
    if (result[0].count > 0) continue;

    let img = await got(url).buffer();
    console.log(`Adding player portrait image for ${portrait.id}`);
    await database.db.any("INSERT INTO player_portrait (player_portrait_id, img) VALUES ($1, $2) ON CONFLICT (player_portrait_id) DO UPDATE SET img = $2", [portrait.id, img]);
  }

  database.db.$pool.end();
});