require('dotenv').config({path:__dirname+'/./../../../.env'});
const { logger, formatError } = require("../../utils/log");
const database = require("../../database");
const guildUtils = require ("../../utils/guild");
const swapiUtils = require ("../../utils/swapi");
const comlink = require("../../utils/comlink.js");


new Promise(async (resolve, reject) =>
{    

    let playersFixed;
    let playersNotInGuild = [];
    let pass = 0;

    do
    {
        console.log(`Starting pass ${pass}`);
        playersFixed = [];
        let playersMissingGuild = await database.db.any(
            `SELECT player_id,allycode 
            FROM guild_players 
            JOIN user_registration using (allycode)
            WHERE guild_id IS NULL AND (last_checked_dt IS NULL OR last_checked_dt < now() - interval '1 day')`);

        // only look at players that are not already known to not be in a guild
        playersMissingGuild = playersMissingGuild.filter(p => playersNotInGuild.indexOf(p.player_id) == -1);

        console.log(`Found ${playersMissingGuild.length} players missing guild_id in guild_players`);

        for (let pIx = 0; pIx < playersMissingGuild.length; pIx++)
        {
            let p = playersMissingGuild[pIx];
            console.log(`Checking player ${pIx+1}/${playersMissingGuild.length}`);

            let id = p.player_id;

            if (id && playersFixed.indexOf(id) != -1) continue;

            let player;
            try
            {
                if (!id)
                {
                    // allycode cannot be null
                    player = await comlink.getPlayer(p.allycode);

                    console.log(`Adding missing player_id for allycode ${p.allycode}`);
                    await database.db.any("UPDATE guild_players SET player_id = $1 WHERE allycode = $2", [player.playerId, p.allycode]);

                    if (playersFixed.indexOf(player.playerId) != -1) continue;
                }
                else
                {
                    player = await comlink.getPlayerByPlayerId(id);
                }
            } 
            catch (e)
            {
                console.error(`Error with Ally Code ${p.allycode}: ${e.message}`);
                continue;
            }

            await database.db.any("UPDATE guild_players SET last_checked_dt = $1 WHERE allycode = $2", [new Date(), p.allycode]);

            let guildId = swapiUtils.getPlayerGuildId(player);
            if (!guildId) 
            {
                playersNotInGuild.push(player.playerId);
                console.info(`Player **${swapiUtils.getPlayerName(player)} (${swapiUtils.getPlayerAllycode(player)})** is not a member of a guild.`);
                continue;
            }

            let guild = await guildUtils.getGuildDataFromAPI({ comlinkGuildId: guildId }, { useCache: false });

            console.log(`Refreshing guild ${guildUtils.getGuildName(guild)} (${swapiUtils.getGuildId(guild)})`);
            await guildUtils.refreshGuildPlayers(guild);

            let roster = guildUtils.getGuildRoster(guild);
            let playerIds = roster.map(r => r.playerId);
            playersFixed = playersFixed.concat(playerIds);
            
            console.log(`Updated ${playerIds.length} players.`)
        }

        pass++;
    } while (playersFixed.length > 0)

    database.db.$pool.end();
})
.catch((reason) => {
    let x = 5;
});
