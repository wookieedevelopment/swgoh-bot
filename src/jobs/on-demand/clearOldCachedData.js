const DELETE_CACHES_SCRIPT = 
    `DELETE from cache_data WHERE NOT EXISTS (select player_cache_id from player_cache where player_cache.cache_data_id = cache_data.cache_data_id);
    DELETE from player_cache WHERE NOT EXISTS (select cache_data_id FROM cache_data where cache_data.cache_data_id = player_cache.cache_data_id);
    DELETE FROM guild_info_cache where timestamp < NOW() - interval '1 day';
    vacuum cache_data; vacuum guild_info_cache`;

const DELETE_OLD_TB_COMBAT_REQUEST_DATA_SCRIPT =
    `DELETE FROM tb_combat_request_data WHERE timestamp < NOW() - interval '7 days';`;



