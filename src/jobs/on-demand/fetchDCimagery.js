require('dotenv').config({path:__dirname+'/./../../../.env'});
const fs = require('fs');
const swapi = require("../../utils/swapi");
const database = require("../../database");
const got = require("got");


new Promise(async (resolve, reject) => {

    let DCList = await swapi.getDatacronsList();
    let id, icon;
    for (const DC of DCList.datacronAffixTemplateSet) {
        
        for (const affix of DC.affix) {
            if (!affix.scopeIcon.startsWith("tex")) continue;
            icon = affix.scopeIcon;
            id = DC.id;
            let url = `https://game-assets.swgoh.gg/textures/${icon}.png`;
            let result = await database.db.any("SELECT COUNT(*) FROM datacron_template WHERE affix_id = $1 AND dc_img IS NULL", [id]);
            if (result[0].count > 0) {
                let img = await got(url).buffer();
                console.log(`Adding DC imagery for ${id}`);
                await database.db.any("UPDATE datacron_template SET dc_img = $2 WHERE affix_id = $1", [id, img]);
            }
        }  
    }
  
    database.db.$pool.end();
  });