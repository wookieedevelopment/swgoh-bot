
const fs = require('fs');
const parse = require("csv-parse/lib/sync");
const { parseAsync } = require('json2csv');
const dotenv = require("dotenv");
dotenv.config({ path: ".env"});

const initOptions = {
    // initialization options;
};

const dbConfigProd = {
    "host": process.env.DB_HOST,
    "port": process.env.DB_PORT,
    "database": "wookieebot",
    "user": process.env.DB_USERNAME,
    "password": process.env.DB_PASSWORD
    // "ssl": process.env.DB_SSL
}

const pgp = require('pg-promise')(initOptions);

const dbProd = pgp(dbConfigProd);

const query = 
`select pa.allycode,pb.last_activity_time,pa.timestamp as timestamp_pre_squish, pa.skill_rating as skill_rating_pre_squish, pb.timestamp as timestamp_post_squish, pb.skill_rating as skill_rating_post_squish, (pb.skill_rating - pa.skill_rating) as squish
from (
WITH T AS (
    SELECT *, ROW_NUMBER() OVER(PARTITION BY allycode ORDER BY timestamp DESC) AS rn
    FROM player_data
	where timestamp >= '12/3/2023' and timestamp < '12/4/2023'
)
SELECT allycode,timestamp,skill_rating
FROM T 
WHERE rn = 1
) pa
join (
WITH T AS (
    SELECT *, ROW_NUMBER() OVER(PARTITION BY allycode ORDER BY timestamp DESC) AS rn
    FROM player_data
	where timestamp >= '12/6/2023' and timestamp < '12/7/2023'
)
SELECT allycode,timestamp,skill_rating,last_activity_time
FROM T 
WHERE rn = 1
) pb on pa.allycode = pb.allycode
where pa.skill_rating is not null and pb.skill_rating is not null
order by squish asc;
`;

const path = "./GAC Squish - DEC 2023.csv"

new Promise(async (resolve, reject) =>
{
    let results = await dbProd.any(query);

    results = results.filter(r => r.last_activity_time > new Date(2023, 11, 5));

    dbProd.$pool.end();

    const csvParsingOptions = { defaultValue: 0 }
    let csvText = await parseAsync(results, csvParsingOptions);

    fs.writeFileSync(path, csvText, { encoding: "utf-8" });
})
