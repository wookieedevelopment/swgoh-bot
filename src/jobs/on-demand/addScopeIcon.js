require('dotenv').config({path:__dirname+'/./../../../.env'});
let comlink = require("../../utils/comlink");
let database = require ("../../database");

new Promise(async (resolve, reject) => {
    let dcData = await comlink.getDatacronData();

    let affixes = dcData.datacronAffixTemplateSet;
    let templates = dcData.datacronTemplate;

    for (let t of templates)
    {
        let setId = t.setId;
        for (var tier = 0; tier < t.tier.length; tier++)
        {
            let curTier = t.tier[tier];
            for (var affixTemplateSetId in curTier.affixTemplateSetId)
            {
                let affixId = curTier.affixTemplateSetId[affixTemplateSetId];
            
                let affixSet = affixes.find(aff => aff.id === affixId);
                if (!affixSet) continue;
                for (var ability in affixSet.affix)
                {
                    let abilityObject = affixSet.affix[ability];
                    if (abilityObject.abilityId == null || abilityObject.abilityId.length == 0) continue;

                    let iconName = abilityObject.scopeIcon.slice(abilityObject.scopeIcon.indexOf(".") + 1);
                    let abilityId = abilityObject.abilityId;
                    
                    await database.db.any("UPDATE datacron SET icon_asset_name = $1 WHERE set_id = $2 AND ability_id = $3 AND affix_id = $4",
                        [iconName, setId, abilityId, affixId]);
                }    
            }
        }
    }

    database.db.$pool.end();
})