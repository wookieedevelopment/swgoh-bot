const { REST } = require('@discordjs/rest');
const { Routes } = require('discord-api-types/v10');
require('dotenv').config();

const commands = [];

// pick and choose commands
commands.push(require(`${__dirname}/commands/cheating.js`).data.toJSON());
commands.push(require(`${__dirname}/commands/counters.js`).data.toJSON());
commands.push(require(`${__dirname}/commands/guide.js`).data.toJSON());
commands.push(require(`${__dirname}/commands/guild.js`).data.toJSON());
commands.push(require(`${__dirname}/commands/help.js`).data.toJSON());
commands.push(require(`${__dirname}/commands/invite.js`).data.toJSON());
commands.push(require(`${__dirname}/commands/joke.js`).data.toJSON());
commands.push(require(`${__dirname}/commands/memes.js`).data.toJSON());
commands.push(require(`${__dirname}/commands/praise.js`).data.toJSON());
commands.push(require(`${__dirname}/commands/report.js`).data.toJSON());
commands.push(require(`${__dirname}/commands/trivia.js`).data.toJSON());
commands.push(require(`${__dirname}/commands/tw.js`).data.toJSON());
commands.push(require(`${__dirname}/commands/scout.js`).data.toJSON());
commands.push(require(`${__dirname}/commands/twManager.js`).data.toJSON());
commands.push(require(`${__dirname}/commands/user.js`).data.toJSON());
commands.push(require(`${__dirname}/commands/wookiee.js`).data.toJSON());
commands.push(require(`${__dirname}/commands/gungan.js`).data.toJSON());
commands.push(require(`${__dirname}/commands/gac.js`).data.toJSON());
commands.push(require(`${__dirname}/commands/tb.js`).data.toJSON());
// commands.push(require(`${__dirname}/commands/krayt.js`).data.toJSON());
commands.push(require(`${__dirname}/commands/raid.js`).data.toJSON());
commands.push(require(`${__dirname}/commands/register.js`).data.toJSON());
commands.push(require(`${__dirname}/commands/rec.js`).data.toJSON());
commands.push(require(`${__dirname}/commands/stats.js`).data.toJSON());
commands.push(require(`${__dirname}/commands/ops.js`).data.toJSON());
commands.push(require(`${__dirname}/commands/checkPermissions.js`).data.toJSON());
commands.push(require(`${__dirname}/commands/roast.js`).data.toJSON());
commands.push(require(`${__dirname}/commands/gungan-context-menu`).data.toJSON());
commands.push(require(`${__dirname}/commands/leaderboard`).data.toJSON());
commands.push(require(`${__dirname}/commands/unit`).data.toJSON());
commands.push(require(`${__dirname}/commands/deal`).data.toJSON());
// commands.push(require(`${__dirname}/commands/player`).data.toJSON());

// do all
// const commandFiles = fs.readdirSync(__dirname + '/commands').filter(file => file.endsWith('.js'));
// for (const file of commandFiles) {
// 	const command = require(`${__dirname}/commands/${file}`);
// 	if (!command.data) continue;
// 	commands.push(command.data.toJSON());
// }

// register for testing
const rest = new REST({ version: '10' }).setToken(process.env.BOT_TOKEN);

// register for prod
// const rest = new REST({ version: '10' }).setToken(process.env.PROD_BOT_TOKEN);


(async () => {
	try {
		// delete slash commands
		// let x = await rest.get(Routes.applicationCommands(process.env.PROD_CLIENT_ID))
		// for (var i = 0; i < x.length; i++)
		// {
		// 	let commandId = x[i].id;
		// 	try {
		// 		await rest.delete(
		// 			Routes.applicationCommand(process.env.CLIENT_ID, commandId)
		// 		)
		// 	} catch (e) {
		// 		console.log(e)
		// 	}
		// }

		// register slash commands for testing
		await rest.put(
			Routes.applicationGuildCommands(process.env.CLIENT_ID, process.env.GUILD_ID),
			{ body: commands },
		);
		
		// await rest.put(
		// 	Routes.applicationCommands(process.env.CLIENT_ID),
		// 	{ body: commands },
		// );

		// register for production
		// await rest.put(
		// 	Routes.applicationCommands(process.env.PROD_CLIENT_ID),
		// 	{ body: commands },
		// );


		console.log('Successfully registered application commands.');
	} catch (error) {
		console.error(error);
	}
})().finally(() => process.exit());