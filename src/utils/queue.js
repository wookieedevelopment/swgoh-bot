require('dotenv').config({path:__dirname+'/./../../.env'});
const { logger, formatError } = require('./log');
const guildUtils = require("./guild");
const readyUtils = require("./ready");
const discordUtils = require("./discord");
const { CLIENT } = require("./discordClient");

const PgBoss = require('pg-boss');
const { EmbedBuilder, AttachmentFlags, AttachmentBuilder } = require('discord.js');
const boss = new PgBoss(`postgres://${process.env.DB_USERNAME}:${process.env.DB_PASSWORD}@${process.env.DB_HOST}/${process.env.DB_QUEUE_NAME}`);

boss.on('error', error => logger.error(formatError(error)));


const QUEUES = {
    READY: "READY",
    OPS: "OPS",
    OPS_OLD: "OPS_OLD",
    RAID_BULK: "RAID_BULK",
    DMS: "DMS",
    OTHER: "OTHER"
};

const otherFunctionsMap = {};
let functionsToCallAtStart = [];
  
module.exports = {
    BOSS: boss,
    QUEUES: QUEUES,
    addStartupFunction: function(func) { functionsToCallAtStart.push(func); },
    addOtherFunction: function(key, func) { otherFunctionsMap[key] = func; },
    queueReadyJob: async function(shard, 
        {
        type,
        name, 
        mission,
        team,
        tier,
        allycode, 
        messageId, 
        channelId,
        userId
        })
    {
        await boss.send(this.getQueueName(QUEUES.READY, shard), 
            {
                type: type,
                name: name, 
                mission: mission,
                team: team,
                tier: tier,
                allycode: allycode, 
                messageId: messageId, 
                channelId: channelId,
                userId: userId 
            },
            {
                expireInHours: 2
            });
    },
    queueTbOpsJob: async function(shard,
        {
        fn,
        comlinkGuildId,
        allycode, 
        messageId, 
        channelId,
        userId, 
        planetsToUse,
        dsOps,
        lsOps,
        mxOps,
        allycodesToExclude,
        rareOnly,
        priority,
        customIdAppend
        })
    {
        await boss.send(this.getQueueName(QUEUES.OPS_OLD, shard), 
            {
                fn: fn,
                allycode: allycode,
                comlinkGuildId: comlinkGuildId,
                channelId: channelId,
                messageId: messageId,
                userId: userId,
                planetsToUse: planetsToUse,
                dsOps: dsOps,
                lsOps: lsOps,
                mxOps: mxOps,
                allycodesToExclude: allycodesToExclude,
                rareOnly: rareOnly,
                priority: priority,
                customIdAppend: customIdAppend
            },
            {
                expireInHours: 2
            });
    },
    queueOpsJob: async function(shard,
        {
        fn,
        comlinkGuildId,
        allycode, 
        messageId, 
        channelId,
        userId, 
        planetsToUse,
        phase,
        allycodesToExclude,
        rareThreshold,
        rareByDefault,
        playersPerImage,
        priority,
        customIdAppend
        })
    {
        await boss.send(this.getQueueName(QUEUES.OPS, shard), 
            {
                fn: fn,
                allycode: allycode,
                comlinkGuildId: comlinkGuildId,
                channelId: channelId,
                messageId: messageId,
                userId: userId,
                planetsToUse: planetsToUse,
                phase: phase,
                allycodesToExclude: allycodesToExclude,
                rareThreshold: rareThreshold,
                rareByDefault: rareByDefault,
                playersPerImage: playersPerImage,
                priority: priority,
                customIdAppend: customIdAppend
            },
            {
                expireInHours: 2
            });
    },
    queueRaidBulkJob: async function(shard, {
        jobType,
        type,
        raid,
        allycode,
        comlinkGuildId,
        sortBy,
        standardDeviationMultiple,
        effort, 
        sort,
        tier,
        exclude,
        include,
        messageId, 
        channelId,
        userId
    }) {
        await boss.send(this.getQueueName(QUEUES.RAID_BULK, shard), 
            {
                jobType: jobType,
                type: type,
                raid: raid,
                allycode: allycode,
                comlinkGuildId: comlinkGuildId,
                sortBy: sortBy,
                standardDeviationMultiple: standardDeviationMultiple,
                effort: effort,
                sort: sort,
                tier: tier,
                exclude: exclude,
                include: include,
                channelId: channelId,
                messageId: messageId,
                userId: userId
            },
            {
                expireInHours: 2
            });
    },
    queueOtherJob: async function(shard, {
        functionKey,
        data,
        messageId, 
        channelId,
        userId
    }) {
        await boss.send(this.getQueueName(QUEUES.OTHER, shard), 
            {
                functionKey: functionKey,
                data: data,
                channelId: channelId,
                messageId: messageId,
                userId: userId
            },
            {
                expireInHours: 2
            });
    },
    createDMData: function(user, message)
    {
        return { userId: user, message: message };
    },
    queueDMJob: async function(shard, {
        comlinkGuildId,
        userId,
        messagesToSend,
        channelId,
        messageId
    }) {
        await boss.send(this.getQueueName(QUEUES.DMS, shard), 
            {
                comlinkGuildId: comlinkGuildId,
                userId: userId,
                messagesToSend: messagesToSend,
                channelId: channelId,
                messageId: messageId
            },
            {
                expireInHours: 2
            });
    },
    getQueueSizes: async function()
    {
        let queueSizes = {};
        for (let q of Object.keys(QUEUES))
        {
            let queuePrefix = QUEUES[q];
            queueSizes[queuePrefix] = 0;
            for (let shard_id = 0; shard_id < CLIENT.shard.count; shard_id++)
            {
                let qs = await boss.getQueueSize(this.getQueueName(queuePrefix, { ids: [shard_id] }));
                queueSizes[queuePrefix] += qs;
            }
        }

        return queueSizes;
    },
    getQueueSizeMessage: async function(queue, shard)
    {
        let s = await boss.getQueueSize(this.getQueueName(queue, shard));
        let requestsAheadMessage;
        if (s === 0) requestsAheadMessage = "You are next in line.";
        else if (s === 1) requestsAheadMessage = "There is 1 request ahead of you.";
        else requestsAheadMessage = `There are ${s+1} requests ahead of you.`;

        return requestsAheadMessage;
    },
    start: async function(shard) {
        await boss.start();
        
        await boss.work(this.getQueueName(QUEUES.READY, shard), { newJobCheckIntervalSeconds: 15 }, readinessJobHandler);
        await boss.work(this.getQueueName(QUEUES.DMS, shard), { newJobCheckInterval: 100 }, sendDMJobHandler);
        await boss.work(this.getQueueName(QUEUES.OTHER, shard), { newJobCheckIntervalSeconds: 8 }, otherJobHandler);


        for (let f of functionsToCallAtStart)
        {
            f(boss, this.getQueueName);
        }
    },
    getQueueName: function(queue, shard)
    {
        return queue + shard.ids[0];
    }
}

const DELAY_BETWEEN_DMS_MS = 50;
function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

async function otherJobHandler(job)
{
    const channel = CLIENT.channels.cache.get(job.data.channelId);
    if (!channel) return false;

    const editMessage = (await channel.messages.fetch({ message: job.data.messageId }));
    await editMessage.edit({ content: "Processing your request now...", embeds: [], components: [], files: []});

    let response = await otherFunctionsMap[job.data.functionKey](job.data);

    response.content = `<@${job.data.userId}>\n${response.content ?? ""}`;

    await editMessage.reply(response);
}

async function sendDMJobHandler(job)
{
    let count = job.data.messagesToSend.length;

    const channel = CLIENT.channels.cache.get(job.data.channelId);
    if (!channel) return false;

    const editMessage = (await channel.messages.fetch({ message: job.data.messageId }));
    await editMessage.edit({ content: `Sending ${count} messages now...`});

    let fails = [];
    for (let m = 0; m < job.data.messagesToSend.length; m++)
    {
        if (m%5 == 0)
        {
            await editMessage.edit({ content: `Sending message ${m+1}/${job.data.messagesToSend.length}. Failures: ${fails.length}`});
        }

        let data = job.data.messagesToSend[m];
        let discordUser;
        try {
            discordUser = await CLIENT.users.fetch(data.userId);
        }
        catch (error)
        {
            fails.push(`User not found: ${discordUtils.makeIdTaggable(data.userId)}`);
            continue;
        }
        
        try 
        {
            if (data.message.files)
            {
                data.message.files = data.message.files.map(f => new AttachmentBuilder(Buffer.from(f.attachment.data), { name: f.name }));
            }
            await discordUser.send(data.message);
        } 
        catch (error)
        {
            fails.push(`Message to ${discordUtils.makeIdTaggable(data.userId)} failed.`);
            logger.error(`Failed to send assignments message to ${data.userId}. Details:\n${formatError(error)}`);
            continue;
        }

        await sleep(DELAY_BETWEEN_DMS_MS);
    }

    let failureText = fails.length > 0 ? "- " + fails.join("\n- ") : null;

    let finalResponse = { content: `Messages sent. Failures: ${fails.length}`};
    if (failureText)
    {
        let failureEmbed = new EmbedBuilder()
            .setColor("DarkRed")
            .setTitle("Failed Message Details")
            .setDescription(failureText)
            .setFooter({ text: "If you have questions about the failures, please ask on the WookieeTools server."});

        finalResponse.embeds = [failureEmbed];
    }
    await editMessage.edit(finalResponse);

}

async function readinessJobHandler(job)
{
    // await CLIENT.shard.broadcastEval(async (client, { job }) => {
        
        const channel = CLIENT.channels.cache.get(job.data.channelId);
        if (!channel) return false;

        const editMessage = (await channel.messages.fetch({ message: job.data.messageId }));
        await editMessage.edit({ content: "Processing your request now..."});

        let guildPlayerData = await guildUtils.loadGuildPlayerData({ allycode: job.data.allycode, editMessageId: job.data.messageId, editMessageChannelId: job.data.channelId });

        let minimums, checkData;
        if (job.data.type === "RAID") {
            checkData = readyUtils.RAID_CHECK_FUNCTION_MAPPING[job.data.team];
            minimums = checkData.raid.DIFFICULTY_MINIMUMS[job.data.tier];
            if (!minimums) minimums = checkData.raid.DIFFICULTY_MINIMUMS[checkData.raid.MAX_DIFFICULTY];
    
        }
        else if (job.data.type === "GL") {
            checkData = readyUtils.GL_CHECK_FUNCTION_MAPPING[job.data.name];
        }
        else if (job.data.type === "JOURNEY") {
            checkData = readyUtils.JOURNEY_CHECK_FUNCTION_MAPPING[job.data.name];
        }
        else if (job.data.type === "TB") {
            checkData = readyUtils.TB_CHECK_FUNCTION_MAPPING[job.data.mission];
        }

        let response = await readyUtils.getReadinessResponse(checkData, guildPlayerData, minimums);
        response.content = `<@${job.data.userId}>`;

        await editMessage.reply(response);
        
	// }, { context: { job: job } })

}