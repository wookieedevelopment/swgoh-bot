const winston = require('winston');
require('dotenv').config()

const console = new winston.transports.Console();
 
const logger = winston.createLogger({
  level: 'info',
  format: winston.format.combine(winston.format.json(), winston.format.timestamp(), winston.format.prettyPrint()),
  transports: [
    console
  ],
});

const myFormat = winston.format.printf(({ level, message, label, timestamp }) => {
    return `${timestamp} ${label ? `[${label} ]` : ``}${level}: ${message}`;
  });
//
// If we're not in production then log to the `console` with the format:
// `${info.level}: ${info.message} JSON.stringify({ ...rest }) `
//
if (process.env.NODE_ENV !== 'production') {
    logger.remove(console)
    logger.add(new winston.transports.Console({
        format: winston.format.combine(winston.format.timestamp(), myFormat)
    }));
}

function formatError(err)
{
    if (err instanceof Error)
    {
        return `${err.message ? `Message: ${err.message}\r\n` : ``}${err.description ? `Description: ${err.description}\r\n` : ``}${err.stack ? `Stack: ${err.stack}\r\n` : ``}`
    }

    if (typeof err == "string") return err;
}

module.exports = { logger, formatError }