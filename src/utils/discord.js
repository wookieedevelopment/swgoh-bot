const { EmbedBuilder, PermissionsBitField  } = require("discord.js");
const database = require("../database");

const MAX_DISCORD_MESSAGE_LENGTH = 2000;
const MAX_DISCORD_AUTOCOMPLETE_CHOICE_LENGTH = 100;
const MAX_DISCORD_EMBED_DESCRIPTION_LENGTH = 4096;
const MAX_DISCORD_DROPDOWN_ITEMS = 25;
const LTR = '\u202a';
const DISCORD_NUMBERS_MAP = 
[
    ":zero:",
    ":one:",
    ":two:",
    ":three:",
    ":four:",
    ":five:",
    ":six:",
    ":seven:",
    ":eight:",
    ":nine:",
    ":keycap_ten:"
];

const WOOKIEEBOT_DISCORD_LINK = 'https://discord.com/invite/KcHzz2v';

const requiredPermissions = [
    PermissionsBitField.Flags.ReadMessageHistory,
    PermissionsBitField.Flags.SendMessages,
    // PermissionsBitField.Flags.SendMessagesInThreads,
    // PermissionsBitField.Flags.CreatePublicThreads,
    // PermissionsBitField.Flags.CreatePrivateThreads,
    PermissionsBitField.Flags.EmbedLinks,
    PermissionsBitField.Flags.AttachFiles,
    // PermissionsBitField.Flags.MentionEveryone,
    // PermissionsBitField.Flags.AddReactions,
    PermissionsBitField.Flags.UseExternalEmojis,
    // PermissionsBitField.Flags.UseExternalStickers,
    // PermissionsBitField.Flags.UseApplicationCommands,
    PermissionsBitField.Flags.ViewChannel
];

const requiredPermissionsStrings = [
    "Read Message History",
    "Send Messages",
    // "Send Messages in Threads",
    // "Create Public Threads",
    // "Create Private Threads",
    "Embed Links",
    "Attach Files",
    // "Mention Everyone",
    // "Add Reactions",
    "Use External Emojis",
    // "Use External Stickers",
    // "Use Application Commands",
    "View Channel"
];

module.exports = {
    prefix: "^",
    symbols : {
        ok: ":white_check_mark:",
        pass: ":white_check_mark:",
        bad: ":warning:",
        warning: ":warning:",
        fail: ":x:",
        smallBlueDiamond: ":small_blue_diamond:",
        question: ":grey_question:",
        firstPlace: ":first_place:",
        secondPlace: ":second_place:",
        thirdPlace: ":third_place:",
        note: ":notepad_spiral:",
        crystal: "<:crystal:1346951415729225938>"
    },
    WOOKIEEBOT_DISCORD_LINK: WOOKIEEBOT_DISCORD_LINK,
    MAX_DISCORD_MESSAGE_LENGTH: MAX_DISCORD_MESSAGE_LENGTH,
    MAX_DISCORD_EMBED_DESCRIPTION_LENGTH: MAX_DISCORD_EMBED_DESCRIPTION_LENGTH,
    MAX_DISCORD_AUTOCOMPLETE_CHOICE_LENGTH: MAX_DISCORD_AUTOCOMPLETE_CHOICE_LENGTH,
    MAX_DISCORD_DROPDOWN_ITEMS: MAX_DISCORD_DROPDOWN_ITEMS,
    LTR: LTR,
    cleanDiscordId: function (id) {
        if (!id) return null;
        return id.replace(/[^0-9]/g, "");
    },
    isBotOwner: function(userId) {
        let cleanId = this.cleanDiscordId(userId);
        return cleanId == "276185061970149377" || cleanId == "276146474268753920";
    },
    isDiscordTag: function(tag) {
        return /^<(#|@!)[0-9]+>$/g.test(tag);
    },
    isDiscordId: function (id) {
        return /^[0-9]+$/g.test(id);
    },
    makeChannelTaggable: function(id){
        if (!id || id[0] == '<' || !this.isDiscordId(id)) return id;
        return "<#" + id + ">";
    },
    makeIdTaggable: function(id) {
        if (!id || id[0] == '<' || !this.isDiscordId(id)) return id;
        return "<@!" + id + ">";
    },
    splitCommandInput: function(input) {
        if (!input || !input.content) return null;
        let message = input.content;
        if (!message.startsWith(this.prefix)) {
            message = this.prefix+message.split(this.prefix)[1];
        }

        var cmd;

        let spaceIx = message.indexOf(" ");
        if (spaceIx > 0)
        {
            cmd = message.slice(this.prefix.length, spaceIx);
        } else {
            cmd = message.slice(this.prefix.length);
        }
        
        return cmd.split(".");
    },
    splitMessage: function(message, splitDelimiter)
    {
        var split = [];
        if (message.length <= 2000)
        {
            split.push(message);
            return split;
        }

        // need to split output
        for (let x = 0; x < message.length; )
        {
            let sliceOfOutput = message.slice(x, x+1999);
            if (x + 1999 < message.length)
            {
                let lastNewLine = sliceOfOutput.lastIndexOf(splitDelimiter);
                if (lastNewLine > 0)
                    sliceOfOutput = sliceOfOutput.slice(0, lastNewLine+splitDelimiter.length);
            }
            split.push(sliceOfOutput);
            
            x += sliceOfOutput.length;
        }

        return split;
    },
    splitAndSendMessage: async function(message, splitDelimiter, sendMessageCallback)
    {
        if (message.length <= 2000)
        {
            return await sendMessageCallback(message);
        }

        // need to split output
        for (let x = 0; x < message.length; )
        {
            let sliceOfOutput = message.slice(x, x+1999);
            if (x + 1999 < message.length)
            {
                let lastNewLine = sliceOfOutput.lastIndexOf(splitDelimiter);
                if (lastNewLine > 0)
                    sliceOfOutput = sliceOfOutput.slice(0, lastNewLine+splitDelimiter.length);
            }
            let error = await sendMessageCallback(sliceOfOutput);

            if (error) return error;

            x += sliceOfOutput.length;
        }

        return null;
    },
    createErrorEmbed: function(title, errorMessage) {
        return new EmbedBuilder()
            .setTitle(title)
            .setColor(0xaa0000)
            .setDescription(errorMessage)
            .addFields({ name: "For support with WookieeBot, join us on Discord:", value: this.WOOKIEEBOT_DISCORD_LINK });
    },
    translateNumberToDiscordString: function(n)
    {
        if (n < 0 || n >= DISCORD_NUMBERS_MAP.length)
        {
            return n.toString();
        }

        return DISCORD_NUMBERS_MAP[n];
    },
    fitForAutocompleteChoice: function(str)
    {
        if (!str) return str;
        return str.slice(0, 100);
    },
    fit(str, width)
    {
        return str?.toString().padEnd(width).substring(0, width)
    },
    addDashesToAllycode(allycode)
    {
        return `${allycode.slice(0, 3)}-${allycode.slice(3,6)}-${allycode.slice(6)}`;
    },
    getMissingBotPermissions: function(interaction, channel)
    {
        if (!interaction.guild) return null;
        const botPermissions = interaction.guild.members.me.permissionsIn(channel);
        let properPermissions = botPermissions.has(requiredPermissions);
        
        if (!properPermissions)
        {
            let missingPermissions = new Array();

            for (let p in requiredPermissions)
            {
                if (!botPermissions.has(requiredPermissions[p])) missingPermissions.push(requiredPermissionsStrings[p]);
            }

            return missingPermissions;
        }

        return null;
    },
    convertMissingBotPermissionsArrayToResponse: function(missingPermissions)
    {
        let response = { content: this.symbols.fail + " WookieeBot is missing the following permissions:\n* " };

        response.content += (missingPermissions.join("\n* "));

        return response;
    },
    convertDateToString: function(date)
    {
        return date.toLocaleString("en-us", {
            year: 'numeric',
            month: 'short',
            day: 'numeric',
            hour: 'numeric',
            minute: 'numeric',
            timeZone: 'utc'
        }) + " UTC";
    },
    getGuildAutocompleteOptions: async function(str) 
    {
        const names = await database.db.any(
            `
            SELECT * FROM 
            (
                (SELECT guild_name, comlink_guild_id, 1 o
                FROM guild
                WHERE guild_name ILIKE $1
                ORDER BY guild_name ASC)
                UNION
                (SELECT guild_name, comlink_guild_id, 2 o
                FROM guild
                WHERE guild_name ILIKE $2)
            ) q
            ORDER BY o ASC, guild_name ASC
            LIMIT 25
            `, [`${str.trim()}%`, `_%${str.trim()}%`]
        );

        if (names.length == 0)
        {
            choices = [{ name: "No matching guilds.", value: "x" }]
        }
        else
        {
            choices = names.map(choice => ({ name: `${choice.guild_name}`, value: choice.comlink_guild_id }));
        }

        return choices;
    }
}