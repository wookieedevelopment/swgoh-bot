
const { Client, Events, GatewayIntentBits, Collection, Partials } = require('discord.js');
const client = new Client(
    { 
        partials: [Partials.Channel, Partials.Message], 
        intents: [
            GatewayIntentBits.Guilds, 
            GatewayIntentBits.GuildMessages, 
            GatewayIntentBits.DirectMessages
        ] 
    });

module.exports = {
    CLIENT: client
}