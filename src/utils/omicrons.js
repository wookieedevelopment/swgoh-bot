const omicronData = require("../data/omicron_data.json");


module.exports = {
    findOmicronUnit: function(unitDefId)
    {
        return omicronData.find(o => o.unitDefId == unitDefId);
    },
    findOmicronData: function(unitDefId, skillId)
    {
        let unit = this.findOmicronUnit(unitDefId);
        if (!unit) return null;

        return unit.omicronSkills.find(s => s.skillId == skillId);
    },
    isOmicronSkill: function(unitDefId, skillToCheck)
    {
        if (skillToCheck.is_omicron != null) return skillToCheck.is_omicron;
        
        let omiData = this.findOmicronData(unitDefId, skillToCheck.id);
        if (!omiData) return false;

        return true;
    },
    hasOmicronOnSkill: function(unitDefId, skillToCheck)
    {
        let omiData = this.findOmicronData(unitDefId, skillToCheck.id);
        if (omiData == null) return false;
        
        if (skillToCheck.tier >= omiData.omicronTier) return true;
        return false;
    }
}