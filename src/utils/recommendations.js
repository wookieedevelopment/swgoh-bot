const metadataCache = require("./metadataCache");
const database = require("../database")
const swapiUtils = require("./swapi");
const player = require("./player");
const constants = require("./constants");
const raidCommon = require("../commands/raid/raidCommon");

const DEFAULT_ROUND_FN = (x) => x;
const FILTER_BLOCK_SIZE = 31;
let DEFAULT_FILTER_BLOCK_VALUE = 0;
for (let i = 0; i < FILTER_BLOCK_SIZE; i++) DEFAULT_FILTER_BLOCK_VALUE |= (1 << i);

const REC_TYPE = {
    KRAYT: raidCommon.KRAYT.KEY,
    ENDOR: raidCommon.ENDOR.KEY,
    NABOO: raidCommon.NABOO.KEY
}

const EFFORT = {
    LOW: { name: "Low (no remod, auto)", shortName: "Low", value: 1 },
    MEDIUM: { name: "Medium (balanced)", shortName: "Medium", value: 2 },
    HIGH: { name: "High (retry, remod)", shortName: "High", value: 3 }
}

const RETREATING_UNITS = [
    // "C3POLEGENDARY"
];

const ALLOWED_FILTERS_BY_RAID = {
    KRAYT: ["affiliation_mandalorian", "affiliation_huttcartel", "species_jawa", "species_tusken", "affiliation_oldrepublic"],
    ENDOR: [
        "ADMIRALACKBAR",
        "C3POLEGENDARY",
        "CAPTAINREX",
        "CAPTAINDROGAN",
        "CHEWBACCALEGENDARY",
        "HANSOLO",
        "HERASYNDULLAS3",
        "JEDIKNIGHTLUKE",
        "ADMINISTRATORLANDO",
        "GLLEIA",
        "WEDGEANTILLES",
        "CHIEFCHIRPA",
        "EWOKELDER",
        "EWOKSCOUT",
        "LOGRAY",
        "PAPLOO",
        "PRINCESSKNEESAA",
        "TEEBO",
        "WICKET",
        "ADMIRALPIETT",
        "COLONELSTARCK",
        "DEATHTROOPER",
        "VEERS",
        "IDENVERSIOEMPIRE",
        "MAGMATROOPER",
        "MOFFGIDEONS1",
        "RANGETROOPER",
        "SCOUTTROOPER_V3",
        "SHORETROOPER",
        "STORMTROOPER"
    ],
    NABOO: [
        "BOSSNASS",
        "CAPTAINTARPALS",
        "BOOMADIER",
        "GUNGANPHALANX",
        "JARJARBINKS",
        "QUEENAMIDALA",
        "PADAWANOBIWAN",
        "MASTERQUIGON",
        "R2D2_LEGENDARY",
        "MACEWINDU",
        "JEDIKNIGHTCONSULAR",
        "JEDIKNIGHTGUARDIAN",
        "KELLERANBEQ",
        "EETHKOTH",
        "GRANDMASTERYODA",
        "PLOKOON",
        "KIADIMUNDI",
        "AAYLASECURA",
        "SHAAKTI",
        "LUMINARAUNDULI",
        "KITFISTO",
        "QUIGONJINN",
        "NUTEGUNRAY",
        "B1BATTLEDROIDV2",
        "STAP",
        "DROIDEKA",
        "B2SUPERBATTLEDROID",
        "MAGNAGUARD",
        "MAUL",
        "DARTHSIDIOUS",
    ]
};

const ALLOWED_UNITS_BY_RAID = {
    KRAYT: null,
    ENDOR: null,
    NABOO: null
};

new Promise(async (resolve, reject) =>
{
    let allUnits = await swapiUtils.getUnitsList();
    ALLOWED_UNITS_BY_RAID.NABOO = allUnits.filter(u => ALLOWED_FILTERS_BY_RAID.NABOO.find(fu => fu === swapiUtils.getUnitDefId(u)));
    ALLOWED_UNITS_BY_RAID.KRAYT = allUnits.filter(u => swapiUtils.getUnitCombatType(u) === constants.COMBAT_TYPES.SQUAD && ALLOWED_FILTERS_BY_RAID.KRAYT.some(c => u.categoryId.includes(c)));
    ALLOWED_UNITS_BY_RAID.ENDOR = allUnits.filter(u => ALLOWED_FILTERS_BY_RAID.ENDOR.find(fu => fu === swapiUtils.getUnitDefId(u)));
});

module.exports = {
    REC_TYPE: REC_TYPE,
    EFFORT: EFFORT,
    ALLOWED_UNITS_BY_RAID: ALLOWED_UNITS_BY_RAID,
    calculateModScalingFactor: calculateModScalingFactor,
    mapValueToEffort: mapValueToEffort,
    getRaidTeams: getRaidTeams,
    getExpectedScore: getExpectedScore,
    createMockUnit: createMockUnit,
    findViableTeam: findViableTeam,
    generateRaidRecommendations: generateRaidRecommendations
}

function mapValueToEffort(v)
{
    if (v === null) return v;

    switch(v) {
        case EFFORT.LOW.value: return EFFORT.LOW;
        case EFFORT.MEDIUM.value: return EFFORT.MEDIUM;
        case EFFORT.HIGH.value: return EFFORT.HIGH;
        default: return null;
    }
}

const NO_SCORE_DEFINED = -1;

async function getRaidTeams(type)
{
    let raidTeamData = metadataCache.get(`${metadataCache.CACHE_KEYS.raidTeams}${type}`);
    if (raidTeamData === undefined)
    { 
        let data = await database.db.any(`
            SELECT raid_team_id, name, leader_unit_id, units_required, quality_by_tier, team_size, omicrons_required, zetas_required, note, auto_factor, ult_required, tier_data, data_points
            FROM raid_team
            WHERE raid_id = $1
            ORDER BY name`, [type]);

        let teams = data.map(d => {
            return {
                raidTeamId: d.raid_team_id,
                teamName: d.name,
                unitsRequired: d.units_required,
                leaderUnitId: d.leader_unit_id,
                allUnits: [d.leader_unit_id].concat(d.units_required),
                teamSize: d.team_size,
                qualityByTier: d.quality_by_tier,
                tierData: d.tier_data,
                dataPoints: d.data_points,
                omicronsRequired: d.omicrons_required,
                zetasRequired: d.zetas_required,
                note: d.note,
                autoFactor: (d.auto_factor != null) ? parseFloat(d.auto_factor) : null,
                ultRequired: d.ult_required
            }
        });

        let quickFilters = {};

        for (let x = 0; x < ALLOWED_UNITS_BY_RAID[type].length; x++)
        {
            let unitDefId = swapiUtils.getUnitDefId(ALLOWED_UNITS_BY_RAID[type][x]);
            let filter = new Array(Math.ceil(teams.length/FILTER_BLOCK_SIZE)).fill(0);
            for (let i = 0; i < teams.length; i++)
            {
                filter[Math.floor(i/FILTER_BLOCK_SIZE)] |= (teams[i].allUnits.find(u => u === unitDefId) ? 0 : 1) << (i%FILTER_BLOCK_SIZE);
            }
            quickFilters[unitDefId] = filter;
        }

        for (let t = 0; t < teams.length; t++)
        {
            let team = teams[t];

            // calculate scores per team and mod effects different from MQ_NO_EFFECT

            // instantiate scoreByTier
            let scoreByTier = new Array(raidCommon[type].MAX_DIFFICULTY + 1);
            for (let tier = 0; tier < scoreByTier.length; tier++)
            {
                scoreByTier[tier] = {};
                let score = NO_SCORE_DEFINED
                let exact = false, minRelicLevel = null, dataPointsCount = 0;
                if (team.tierData) 
                {
                    if(team.tierData[tier].exactScore)
                    {
                        exact = true;
                        score = team.tierData[tier].exactScore;
                        dataPointsCount = 1;
                    }
                    if (team.tierData[tier].minRelicLevel != null)
                    {
                        minRelicLevel = team.tierData[tier].minRelicLevel;
                    }
                }

                scoreByTier[tier][EFFORT.LOW.value] = { score: score, dataPointsCount: dataPointsCount, exact: exact, extrapolatedFromTier: tier, minRelicLevel: minRelicLevel };
                scoreByTier[tier][EFFORT.MEDIUM.value] = { score: score, dataPointsCount: dataPointsCount, exact: exact, extrapolatedFromTier: tier, minRelicLevel: minRelicLevel };
                scoreByTier[tier][EFFORT.HIGH.value] = { score: score, dataPointsCount: dataPointsCount, exact: exact, extrapolatedFromTier: tier, minRelicLevel: minRelicLevel };

            }

            team.scoreByTier = scoreByTier;

            if (team.dataPoints?.length > 0)
            {
                // add the known datapoints 
                for (let d = 0; d < team.dataPoints?.length ?? 0; d++)
                {
                    let dp = team.dataPoints[d];
                    let sbt = scoreByTier[dp.tier][dp.effort];
                    if (sbt.exact) continue; // don't overwrite exact scores, even if there's data

                    let unscaledScore = calculateUnscaledScore(
                        dp.score, 
                        dp.modQuality, 
                        dp.gearBelowMinium, 
                        dp.gearAboveMinium, 
                        dp.relicsAboveMinimum, 
                        dp.tier,
                        raidCommon[type]);

                    if (sbt.score === NO_SCORE_DEFINED) sbt.score = unscaledScore;
                    else sbt.score = ((sbt.score * sbt.dataPointsCount) + unscaledScore) / (sbt.dataPointsCount + 1);
                    
                    sbt.dataPointsCount++;
                }

                // extrapolate all gaps
                for (let tier = 0; tier < scoreByTier.length; tier++)
                {
                    let sbt = scoreByTier[tier];

                    for (let e = EFFORT.LOW.value; e <= EFFORT.HIGH.value; e++)
                    {
                        let sbe = sbt[e];
                        
                        if (sbe.score === NO_SCORE_DEFINED)
                        {
                            // if the team shouldn't be extrapolated at the tier, then don't
                            if (team.tierData[tier].extrapolate == false) { sbe.score = 0; continue; }

                            // find closest score
                            let scoreToExtrapolateFrom = findClosestScoreOnTeam(team, tier, e);
                            
                            // keep sbe.dataPointsCount at 0 to indicate that the score is extrapolated
                            sbe.extrapolatedFromTier = scoreToExtrapolateFrom.tier;
                            sbe.score = ((scoreToExtrapolateFrom.score ?? 0) / raidCommon[type].DIFFICULTY_MINIMUMS[(scoreToExtrapolateFrom.tier ?? 0)].maxScore) * raidCommon[type].DIFFICULTY_MINIMUMS[tier].maxScore;
                            sbe.score *= (1 + 0.1*(e - scoreToExtrapolateFrom.effortValue)); 

                            // round to an integer, but can't be more than max for tier
                            sbe.score = Math.min(Math.round(sbe.score), raidCommon[type].DIFFICULTY_MINIMUMS[tier].maxScore);
                        }
                    }
                }
            }
            else
            {
                // no datapoints, so populate based on quality & autofactor
                for (let tier = 0; tier < scoreByTier.length; tier++)
                {
                    let sbt = scoreByTier[tier];

                    let baseEstimate = Math.ceil(raidCommon[type].DIFFICULTY_MINIMUMS[tier].maxScore * team.qualityByTier[tier] / 100.0);

                    autoFactor = team.autoFactor ?? 1;
                    sbt[EFFORT.LOW.value].score = baseEstimate * autoFactor;
                    sbt[EFFORT.MEDIUM.value].score = baseEstimate * ((1 - autoFactor) * 0.7 + autoFactor);
                    sbt[EFFORT.HIGH.value].score = baseEstimate;
                    
                    sbt[EFFORT.LOW.value].dataPointsCount = 1;
                    sbt[EFFORT.MEDIUM.value].dataPointsCount = 1;
                    sbt[EFFORT.HIGH.value].dataPointsCount = 1;
                }
            }

            // establish quickFilters per team
            team.quickFilter = new Array(Math.ceil(teams.length/FILTER_BLOCK_SIZE));
            
            let newFilters = [];
            for (let u = 0; u < team.allUnits.length; u++)
            {
                newFilters.push(quickFilters[team.allUnits[u]]);
            }

            for (let block = 0; block < team.quickFilter.length; block++)
            {
                let val = newFilters[0][block];
                for (let f = 1; f < newFilters.length; f++)
                {
                    val &= newFilters[f][block];
                }

                team.quickFilter[block] = val;
            }
        }

        raidTeamData = {
            teams: teams,
            filters: quickFilters
        }

        metadataCache.set(`${metadataCache.CACHE_KEYS.raidTeams}${type}`, raidTeamData, 60*60*24); // 24 hours
    }

    return raidTeamData;
}

function findClosestScoreOnTeam(team, tierMissing, effortValue)
{
    let retObj = {
        score: null,
        tier: null,
        effortValue: null
    };

    // check for the same effort at other tiers
    for (let t = 1; t < team.scoreByTier.length; t++)
    {
        let tierToCheck = (t + tierMissing);
        if (tierToCheck >= team.scoreByTier.length) tierToCheck = (team.scoreByTier.length - t - 1);

        // tier 0/1 don't translate well in any raid to higher tiers
        if (tierMissing > 2 && (tierToCheck <= 2 || team.scoreByTier[tierToCheck][effortValue].extrapolatedFromTier <= 1)) continue;

        if (team.scoreByTier[tierToCheck][effortValue].score != NO_SCORE_DEFINED)
        {
            retObj.score = team.scoreByTier[tierToCheck][effortValue].score;
            retObj.tier = tierToCheck;
            retObj.effortValue = effortValue;
            return retObj; 
        }
    }

    // check for closest effort at same and other tiers
    for (let t = 0; t < team.scoreByTier.length; t++)
    {
        let tierToCheck = (t + tierMissing);
        if (tierToCheck >= team.scoreByTier.length) tierToCheck = (team.scoreByTier.length - t - 1);

        // tier 0/1 don't translate well in any raid to higher tiers
        if (tierMissing >= 2 && (tierToCheck <= 1 || team.scoreByTier[tierToCheck][effortValue].extrapolatedFromTier <= 1)) continue;

        for (let e = EFFORT.LOW.value; e <= EFFORT.HIGH.value; e++)
        {
            if (team.scoreByTier[tierToCheck][e].score != NO_SCORE_DEFINED)
            {
                retObj.score = team.scoreByTier[tierToCheck][e].score;
                retObj.tier = tierToCheck;
                retObj.effortValue = e;
                return retObj;                
            }
        }
    }

    return retObj;
}

function filterByIndex(filter, i)
{
    return filter[Math.floor(i/FILTER_BLOCK_SIZE)] & (1 << (i%FILTER_BLOCK_SIZE));
}


function findViableTeam(playerData, teamDefinition, tier, DIFFICULTY_MINIMUMS, teamsToSimulate)
{
    let roster = swapiUtils.getPlayerRoster(playerData);
    let viableTeam = [];    
        
    // check if there's a minimum relic level for the team at the tier
    let minRelicLevel = null;
    if (teamDefinition.scoreByTier) minRelicLevel = teamDefinition.scoreByTier[tier][EFFORT.LOW.value].minRelicLevel;
            
    let simTeam = teamsToSimulate?.find(t => t.raidTeamId === teamDefinition.raidTeamId);
    if (simTeam)
    {
        if (simTeam.tier < tier) return null;
        if (minRelicLevel == null) minRelicLevel = DIFFICULTY_MINIMUMS[simTeam.tier].minRelicLevel;
        
        // mock up the units to form the viableTeam
        for (let u = 0; u < teamDefinition.allUnits.length; u++)
        {
            let mock = createMockUnit(
                teamDefinition.allUnits[u],
                DIFFICULTY_MINIMUMS[simTeam.tier].minRarity,
                DIFFICULTY_MINIMUMS[simTeam.tier].minGearLevel,
                minRelicLevel
            )

            viableTeam.push(mock);
        }

        return viableTeam;
    }

    if (teamDefinition.omicronsRequired?.find(
        o => swapiUtils.getPlayerOmicrons(playerData, swapiUtils.OMICRON_MODE.GUILD_RAID_OMICRON)?.find(po => po === o) == null)) return null;


    for (let u of roster)
    {
        if (teamDefinition.allUnits.find(du => du === swapiUtils.getUnitDefId(u)))
        {
            // if the unit has a zeta ability in the zetasRequired list, but the unit doesn't have the zeta
            if (teamDefinition.zetasRequired?.find(
                z => u.skill.find(s => s.id === z) && !u.zeta_abilities.find(za => za === z)
            )) return null;

            // if the player doesn't have a unit at least at min relic level, then exclude
            if (teamDefinition.scoreByTier[tier][EFFORT.LOW.value].minRelicLevel != null && swapiUtils.getUnitRelicLevel(u) - 2 < teamDefinition.scoreByTier[tier][EFFORT.LOW.value].minRelicLevel) return null;

            if (!u.raidTierOverride)
            {
                // unit found, check tier
                if (DIFFICULTY_MINIMUMS[tier].minRelicLevel)
                {
                    if (swapiUtils.getUnitRelicLevel(u) - 2 < DIFFICULTY_MINIMUMS[tier].minRelicLevel) return null;
                }
                else if (swapiUtils.getUnitRarity(u) < DIFFICULTY_MINIMUMS[tier].minRarity) return null;
                else if (swapiUtils.getUnitGearLevel(u) < DIFFICULTY_MINIMUMS[tier].minGearLevel) return null;
                
                if (teamDefinition.ultRequired && (u.is_galactic_legend === true) && (u.purchasedAbilityId?.length === 0)) return null;
            }
            else if (u.raidTierOverride < tier) return null;

            viableTeam.push(u);
        }
    }

    // not all units were found
    if (teamDefinition.allUnits.length != viableTeam.length) return null;

    return viableTeam;
}

async function generateRaidRecommendations(playerData, { DIFFICULTY_MINIMUMS, REC_TYPE, MAX_TOTAL_SCORE, MAX_ATTEMPTS, MIN_RECOMMENDED_GEAR, ROUND_FN = DEFAULT_ROUND_FN, effort = EFFORT.MEDIUM, reuseEscapers = false, extraRelicScalingFactor = 0.4, teamsToSimulate = [], FILLER_TEAM = { qualityByTier: [50, 30, 20, 10, 10, 10, 10] }})
{
    // planetsToUse has three planets to search on. Load team data from the database
    let raidTeamData = await getRaidTeams(REC_TYPE);
    let raidTeams = raidTeamData.teams;

    let matchingTeams = [];
    let modScalingFactor = calculateModScalingFactor(playerData);
    if (effort === EFFORT.LOW) modScalingFactor = Math.min(modScalingFactor, 1);
    
    // catalog all matching teams that the player has
    for (let teamIx = 0; teamIx < raidTeams.length; teamIx++)
    {
        let team = raidTeams[teamIx];

        for (var tier = (team.qualityByTier?.length ?? team.scoreByTier.length) - 1; tier >= 0; tier--)
        {
            let viableTeam = findViableTeam(playerData, team, tier, DIFFICULTY_MINIMUMS, teamsToSimulate);

            if (!viableTeam) continue;

            // found highest tier at which the player is eligible to run the team

            let bestScore = { tier: 0, expectedScore: 0 };

            // find the tier with the highest expected score
            for (; tier >= 0; tier--)
            {
                let expectedScore = getExpectedScore(team, tier, DIFFICULTY_MINIMUMS, MIN_RECOMMENDED_GEAR, viableTeam, extraRelicScalingFactor, modScalingFactor, ROUND_FN, effort);


                if (expectedScore === 0 || expectedScore <= bestScore.expectedScore) continue;

                bestScore.tier = tier;
                bestScore.expectedScore = expectedScore;
            }

            // if the score is at least the filler team score for tier 0, then we add it to consideration.
            if (bestScore.expectedScore > 0 && bestScore.expectedScore >= DIFFICULTY_MINIMUMS[0].maxScore * (FILLER_TEAM.qualityByTier[0]/100))
            {
                let match = {
                    teamIndex: teamIx,
                    teamDefinition: team,
                    playerViableTeam: viableTeam,
                    tier: bestScore.tier,
                    expectedScore: bestScore.expectedScore
                };

                matchingTeams.push(match);
            }
        
            break;
        }
    }

    // sort descending by expected score, then by team size (smallest first), then by omis required (most first), then by zetas required (most first)
    matchingTeams.sort((a, b) => 
                (b.expectedScore - a.expectedScore) || 
                (a.teamDefinition.allUnits.length - b.teamDefinition.allUnits.length) || 
                ((b.teamDefinition.omicronsRequired?.length ?? 0) - (a.teamDefinition.omicronsRequired?.length ?? 0)) || 
                ((b.teamDefinition.zetasRequired?.length ?? 0) - (a.teamDefinition.zetasRequired?.length ?? 0)));

    // select the top teams
    let bestTeamCombination = await findBestTeamCombination(matchingTeams, raidTeamData, { MAX_TOTAL_SCORE, MAX_ATTEMPTS, effort, reuseEscapers });

    ///let possiblyBadNonFiller = bestTeamCombination.combination.filter(c => c.expectedScore < (DIFFICULTY_MINIMUMS[0].maxScore * 0.75));
    if ((bestTeamCombination.combination?.length ?? 0) < MAX_ATTEMPTS /*|| possiblyBadNonFiller.length > 0*/)
    {
        // didn't find enough teams to max out their attempts or some of the found teams may be worse than generic filler. 
        // Find all "extra" units and estimate what a player could score with those units
        let unitsUsed = bestTeamCombination
                        .combination?./*.filter(c => !possiblyBadNonFiller.find(bnf => bnf === c))*/
                        reduce((p,v)  => p.concat(v.teamDefinition.allUnits), []);

        // find all units that could be used in a filler team (i.e., those that are not part of unitsUsed)
        let unitsNotUsed = ALLOWED_UNITS_BY_RAID[REC_TYPE];
        if (unitsUsed) unitsNotUsed = unitsNotUsed.filter(u => !unitsUsed.find(used => used === swapiUtils.getUnitDefId(u)));

        // find those possible filler units in the player's roster
        let playerFillerUnits = swapiUtils.getPlayerRoster(playerData).filter(ru => {
            if (swapiUtils.getUnitRarity(ru) < DIFFICULTY_MINIMUMS[0].minRarity) return false;
            if (swapiUtils.getUnitGearLevel(ru) < DIFFICULTY_MINIMUMS[0].minGearLevel) return false;
            
            if (unitsNotUsed.find(u => swapiUtils.getUnitDefId(ru) === swapiUtils.getUnitDefId(u))) return true;
            return false;
        });

        // sort them by gear & relic
        playerFillerUnits.sort((a, b) => {
            return (swapiUtils.getUnitGearLevel(b) - swapiUtils.getUnitGearLevel(a)) || (swapiUtils.getUnitRelicLevel(b) - swapiUtils.getUnitRelicLevel(a));
        })

        let fillerTeams = [];
        let fillerScore = 0;
        for (let uIx = 0; uIx < playerFillerUnits.length && fillerTeams.length < (MAX_ATTEMPTS - (bestTeamCombination.combination?.length ?? 0)); uIx += 5)
//        for (let uIx = 0; uIx < playerFillerUnits.length && fillerTeams.length < Math.max(possiblyBadNonFiller.length, (MAX_ATTEMPTS - (bestTeamCombination.combination?.length ?? 0))); uIx += 5)
        {
            let playerUnitsForTeam = playerFillerUnits.slice(uIx, uIx+5);
            let expectedFillerScore = getExpectedScore(FILLER_TEAM, 0, DIFFICULTY_MINIMUMS, MIN_RECOMMENDED_GEAR, playerUnitsForTeam, extraRelicScalingFactor, modScalingFactor, ROUND_FN, effort);
            
            fillerTeams.push(expectedFillerScore);

            fillerScore += expectedFillerScore;

            // let firstMatch = possiblyBadNonFiller.find(nf => nf.expectedScore < expectedFillerScore);

            // // there's a possibly bad non-filler team that scores less than the generic filler
            // if (firstMatch)
            // {
            //     fillerTeams.push({ units: playerUnitsForTeam, score: expectedFillerScore });

            //     fillerScore += expectedFillerScore;

            //     let overlappingIx;
            //     while (-1 !== (overlappingIx = bestTeamCombination.combination.findIndex(c => c.playerViableTeam.find(ru => playerUnitsForTeam.find(pu => pu === ru))))) bestTeamCombination.combination.splice(overlappingIx, 1);

            //     if (bestTeamCombination.length + fillerTeams.length > MAX_ATTEMPTS)
            //     {
            //         // filler team didn't overlap, so remove the lowest scoring
            //         bestTeamCombination.combination.splice(bestTeamCombination.combination.length - 1, 1);
            //     }
            // }
        }

        bestTeamCombination.points += fillerScore;
        bestTeamCombination.fillerScore = fillerScore;
        bestTeamCombination.fillerTeams = fillerTeams;
    }

    return bestTeamCombination;
}

function getExpectedScore(team, tier, DIFFICULTY_MINIMUMS, MIN_RECOMMENDED_GEAR, playerUnitsForTeam, extraRelicScalingFactor, modScalingFactor, ROUND_FN, effort)
{
    let qualityDecrease = 0;
    let qualityBoost = 0;
    
    let minimumsForTier = DIFFICULTY_MINIMUMS[tier];

    if (team.tierData && team.tierData[tier].exactScore) return team.tierData[tier].exactScore;

    for (let u of playerUnitsForTeam)
    {
        let minRelicLevel = (team.tierData ? team.tierData[tier].minRelicLevel : null) ?? minimumsForTier.minRelicLevel;
        if (minRelicLevel != null)
        {
            // relic check
            let relicDiff = (u.raidTierOverride != null ? DIFFICULTY_MINIMUMS[u.raidTierOverride].minRelicLevel : (swapiUtils.getUnitRelicLevel(u) - 2)) - minRelicLevel;
            qualityBoost += relicDiff * extraRelicScalingFactor;
        } else {
            // gear check
            let unitGearLevel = (u.raidTierOverride != null) ? DIFFICULTY_MINIMUMS[u.raidTierOverride].minGearLevel : swapiUtils.getUnitGearLevel(u);
            if (unitGearLevel < MIN_RECOMMENDED_GEAR)
            {
                qualityDecrease += (MIN_RECOMMENDED_GEAR - unitGearLevel)*7;
            } else {
                qualityBoost += (unitGearLevel - MIN_RECOMMENDED_GEAR) * 1;

                // this tier doesn't require relics. If the player has relics, though, then boost the quality more
                let relic = swapiUtils.getUnitRelicLevel(u) - 2;

                if (relic > 0) qualityBoost += relic*2;
            }
        }
    }
    
    let maxScore = minimumsForTier.maxScore;
    let estimate;

    if (team.dataPoints?.length > 0)
    {
        // new method (NABOO+)
        let baseScore = team.scoreByTier[tier][effort.value].score;
        let qualityChange = qualityDecrease + qualityBoost;
        estimate = Math.ceil(baseScore*(1 + qualityChange/100.0));
    }
    else
    {
        // legacy method (KRAYT, ENDOR)
        estimate = Math.ceil(maxScore * Math.min(100, Math.max(team.qualityByTier[tier] - qualityDecrease, 0) + qualityBoost) / 100.0);

        // low effort, don't make any remod assumptions, but still scale down for poor mods
        if (team.autoFactor) {
            if (effort === EFFORT.LOW) estimate *= team.autoFactor;
            else if (effort === EFFORT.MEDIUM) estimate *= ((1 - team.autoFactor) * 0.7 + team.autoFactor);
        }
    }

    estimate *= modScalingFactor;

    estimate = ROUND_FN(Math.ceil(Math.min(DIFFICULTY_MINIMUMS[tier].maxScore, estimate)));

    return estimate;
}

const MQ_NO_EFFECT = 10;
function calculateModScalingFactor(playerData)
{
    let mq = player.calculateModScore(playerData, constants.MOD_SCORE_TYPES.WOOKIEEBOT);

    // use something relatively arbitrary - 10 - as a neutral

    let proportion = (mq - MQ_NO_EFFECT) / MQ_NO_EFFECT;

    return 1 + proportion/15.0;
    // someone with a 20 will have a score boost of 1.066666 times
}

function calculateUnscaledScore(score, mq, gearBelowMinium, gearAboveMinium, relicsAboveMinimum, tier, raidData)
{
    // remove mod quality scaling
    let proportion = (mq - MQ_NO_EFFECT) / MQ_NO_EFFECT;

    let factor = 1 + proportion/15.0;

    let unscaledScore = Math.round(score / factor);

    // remove gear/relic
    if (gearBelowMinium)
    {
        let factor = 7;
        unscaledScore *= (1 + factor*gearBelowMinium/100.0);
    }

    if (gearAboveMinium)
    {
        let factor = 1;
        unscaledScore *= (1 - factor*gearAboveMinium/100.0);
    }

    if (relicsAboveMinimum)
    {
        let factor = (raidData.DIFFICULTY_MINIMUMS[tier].minRelicLevel == undefined) ? 2 : raidData.EXTRA_RELIC_SCALING_FACTOR;
        unscaledScore *= (1 - factor*relicsAboveMinimum/100.0);
    }

    return Math.min(unscaledScore, raidData.DIFFICULTY_MINIMUMS[tier].maxScore);
}

async function getBestCombo(startingIndex, combination, filter, matchingTeams, best, raidTeamData, { MAX_TOTAL_SCORE, MAX_ATTEMPTS, effort, reuseEscapers = false })
{
    let curExpected = getExpectedPoints(combination);
    if (combination.length === MAX_ATTEMPTS) {
        
        if (curExpected > best.points)
        {
            best.combination = [...combination];
            best.points = curExpected;
            matchingTeams.splice(startingIndex);
        }

        return;
    }
    
    let possibleMore = 0;
    let count = 0;
    for (let x = startingIndex; x < matchingTeams.length && (count < MAX_ATTEMPTS - combination.length); x++)
    {
        if (filterByIndex(filter, matchingTeams[x].teamIndex))
        {
            possibleMore += matchingTeams[x].expectedScore;
            count++;
        }
    }

    if (curExpected + possibleMore < best.points) return;

    // matchingTeams is sorted by expected score, so start at the beginning
    // get team, for each unit overlay the filter
    // now we have a list of team indexes for the teams array
    for (let index = startingIndex; index < matchingTeams.length; index++)
    {

        let team = matchingTeams[index];
        let teamIndexInRaidTeams = team.teamIndex;
        
        if (filterByIndex(filter, teamIndexInRaidTeams) == 0) continue;

        let newFilter = new Array(Math.ceil(raidTeamData.teams.length/FILTER_BLOCK_SIZE));

        for (let block = 0; block < newFilter.length; block++)
        {
            newFilter[block] = team.teamDefinition.quickFilter[block] & filter[block];
        }

        combination.push(team);

        await getBestCombo(index + 1, combination, newFilter, matchingTeams, best, raidTeamData, { MAX_TOTAL_SCORE, MAX_ATTEMPTS, effort, reuseEscapers});

        if (best.points === MAX_TOTAL_SCORE) return; // found a winner
        combination.pop();
    }

    if (curExpected > best.points)
    {
        best.combination = [...combination];
        best.points = curExpected;
    }

    return;
}

function createMockUnit(defId, rarity, gear, relic)
{
    
    let mock = {
        relic: { currentTier: relic + 2 },
        rarity: rarity,
        gear: gear,
        defId: defId
    };

    return mock;
}

async function findBestTeamCombination(matchingTeams, raidTeamData, { MAX_TOTAL_SCORE, MAX_ATTEMPTS, effort, reuseEscapers = false })
{
    let best = { 
        combination: null, 
        points: 0,
    };

    let filter = new Array(Math.ceil(raidTeamData.teams.length/FILTER_BLOCK_SIZE)).fill(DEFAULT_FILTER_BLOCK_VALUE);
    await getBestCombo(0, [], filter, matchingTeams, best, raidTeamData, { MAX_TOTAL_SCORE, MAX_ATTEMPTS, effort, reuseEscapers })

    return best;
}

function getExpectedPoints(combination)
{
    return combination.reduce((p, c) => p + (c?.expectedScore ?? 0), 0);
}