const got = require('got');
const metadataCache = require('./metadataCache');
const ComlinkStub = require ("@swgoh-utils/comlink");

const comlinkClient = new ComlinkStub({
  url: process.env.COMLINK_URL,
  statsUrl: process.env.STATS_URL
});

const LEADERBOARD_TYPES = {
  TOTAL_RAID_POINTS: 0,
  SPECIFIC_RAID_POINTS: 2,
  GALACTIC_POWER: 3,
  TERRITORY_BATTLE: 4,
  TERRITORY_WAR: 5
};

const LEADERBOARD_DEF_IDS = {
  SITH_RAID: "sith_raid",
  RANCOR: "rancor",
  AAT: "aat",
  // TB_REBEL_ASSAULT: "t01D",
  TB_IMPERIAL_RETALIATION: "t02D",
  TB_SEPARATIST_MIGHT: "t03D",
  TB_REPUBLIC_OFFENSIVE: "t04D",
  TB_RISE_OF_THE_EMPIRE: "t05D",
  TW: "TERRITORY_WAR_LEADERBOARD"
}

module.exports = {
    profileStatsNames:
    {
        GP: "STAT_GALACTIC_POWER_ACQUIRED_NAME",
        GPCharacters: "STAT_CHARACTER_GALACTIC_POWER_ACQUIRED_NAME",
        GPShips: "STAT_SHIP_GALACTIC_POWER_ACQUIRED_NAME"
    },
    LEADERBOARD_TYPES: LEADERBOARD_TYPES,
    LEADERBOARD_DEF_IDS: LEADERBOARD_DEF_IDS,
    getGuildLeaderboard: async function(type, defId, monthOffset = 0, count = 200 )
    {
        let payload = { 
            "payload": 
            { 
                "leaderboardId": [
                    {
                        "leaderboardType": type, 
                        "defId": defId, 
                        "monthOffset": monthOffset
                    } 
                ],
                "count": count
            }
        };
        
        let lb = await got.post(
          process.env.COMLINK_URL + "/getGuildLeaderboard", 
          { json: payload }).json();

        return lb;
    },
    getAllGuildLeaderboards: async function(monthOffset = 0, count = 200)
    {
      let payload = { 
          "payload": 
          { 
              "leaderboardId": [
                  {
                      "leaderboardType": LEADERBOARD_TYPES.GALACTIC_POWER, 
                      "monthOffset": monthOffset
                  },
                  {
                      "leaderboardType": LEADERBOARD_TYPES.SPECIFIC_RAID_POINTS, 
                      "defId": LEADERBOARD_DEF_IDS.AAT, 
                      "monthOffset": monthOffset
                  },
                  {
                      "leaderboardType": LEADERBOARD_TYPES.SPECIFIC_RAID_POINTS, 
                      "defId": LEADERBOARD_DEF_IDS.RANCOR, 
                      "monthOffset": monthOffset
                  },
                  {
                      "leaderboardType": LEADERBOARD_TYPES.SPECIFIC_RAID_POINTS, 
                      "defId": LEADERBOARD_DEF_IDS.SITH_RAID, 
                      "monthOffset": monthOffset
                  },
                  {
                      "leaderboardType": LEADERBOARD_TYPES.TERRITORY_BATTLE, 
                      "defId": LEADERBOARD_DEF_IDS.TB_IMPERIAL_RETALIATION, 
                      "monthOffset": monthOffset
                  },
                  {
                      "leaderboardType": LEADERBOARD_TYPES.TERRITORY_BATTLE, 
                      "defId": LEADERBOARD_DEF_IDS.TB_REPUBLIC_OFFENSIVE, 
                      "monthOffset": monthOffset
                  },
                  {
                      "leaderboardType": LEADERBOARD_TYPES.TERRITORY_BATTLE, 
                      "defId": LEADERBOARD_DEF_IDS.TB_SEPARATIST_MIGHT, 
                      "monthOffset": monthOffset
                  },
                  {
                      "leaderboardType": LEADERBOARD_TYPES.TERRITORY_BATTLE, 
                      "defId": LEADERBOARD_DEF_IDS.TB_RISE_OF_THE_EMPIRE, 
                      "monthOffset": monthOffset
                  },
                  {
                      "leaderboardType": LEADERBOARD_TYPES.TERRITORY_WAR, 
                      "defId": LEADERBOARD_DEF_IDS.TW, 
                      "monthOffset": monthOffset
                  }
              ],
              "count": count
          }
      };
      
      let lb = await got.post(
        process.env.COMLINK_URL + "/getGuildLeaderboard", 
        { json: payload }).json();

      return lb;
    },
    getGuild: async function(guildId, includeRecentGuildActivityInfo = false)
    {
        if (!guildId) return;
        let guild = await comlinkClient.getGuild(guildId, includeRecentGuildActivityInfo);
        return guild;
    },
    searchGuilds: async function(str, startIndex = 0, count = 25)
    {
        if (!str) return;
        str = str.replace(/(\/|\[|\])/g, "" );
        if (str.length === 0) return;
        let guilds = await comlinkClient.getGuildsByName(str, startIndex, count);
        return guilds;
    },
    getPlayerArenaProfileByPlayerId: async function(playerId)
    {
        if (!playerId) return null;

        let player = await comlinkClient.getPlayerArenaProfile(null, playerId, true);

        return player;
    },
    getPlayerArenaProfileByAllycode: async function(allycode)
    {
        if (!allycode) return null;
        
        let player = await comlinkClient.getPlayerArenaProfile(allycode, null, true);

        return player;
    },
    getPlayer: async function(allycode)
    {
        if (!allycode) return null;

        let player = await comlinkClient.getPlayer(allycode.toString());

        if (player.code)
        {
          // there was an error
          throw new Error(player.message);
        }
        
        // if (player.rosterUnit.length > 0)
        // {
        //   player.rosterUnit = await comlinkClient.getUnitStats(player.rosterUnit, ["gameStyle", "calcGP"]);
        // }
        
        return player;
    },
    getPlayerByPlayerId: async function(playerId)
    {
        if (!playerId) return null;
        let player = await comlinkClient.getPlayer(null, playerId);

        if (player.code)
        {
          // there was an error
          throw new Error(player.message);
        }
        
        // if (player.rosterUnit.length > 0)
        // {
        //   player.rosterUnit = await comlinkClient.getUnitStats(player.rosterUnit, ["gameStyle", "calcGP"]);
        // }
        
        return player;
    },
    getMetadata: async function()
    {
      return await comlinkClient.getMetaData();
    },
    getLatestGamedataVersion: async function()
    {
        let version = metadataCache.get(metadataCache.CACHE_KEYS.gamedataVersion);
        if (version == undefined)
        {
            let metadata = await this.getMetadata();
            version = metadata.latestGamedataVersion;

            // let dataversion = await got(baseStatsUrl + "/dataVersion.json").json();
            // version = dataversion.game;

            metadataCache.set(metadataCache.CACHE_KEYS.gamedataVersion, version, 86400);
        }
        
        return version;
    },
    getLatestLocalizationBundleVersion: async function()
    {
      let md = await this.getMetadata();
      return md.latestLocalizationBundleVersion;
    },
    getCategories: async function()
    {
      let categoryData = metadataCache.get(metadataCache.CACHE_KEYS.categoryData);
      if (categoryData == undefined)
      {
        let version = await this.getLatestGamedataVersion();
        let response = await comlinkClient.getGameData(version, false, 1);
        categoryData = response.category;
        metadataCache.set(metadataCache.CACHE_KEYS.categoryData, categoryData, 86400)
      }

        return categoryData;

    },
    getLocalization: async function()
    {
      let localizationData = metadataCache.get(metadataCache.CACHE_KEYS.localization);
      if (localizationData == undefined)
      {
        let version = await this.getLatestLocalizationBundleVersion();

        let response = await comlinkClient.getLocalizationBundle(version, true);

        localizationData = response["Loc_ENG_US.txt"]
          .split("\n")
          .filter(s => s.indexOf("|") >= 0)
          .map((s) => 
             {
               let pipeIndex = s.indexOf("|");
               return { key: s.slice(0, pipeIndex), value: s.slice(pipeIndex+1) }
              })

        metadataCache.set(metadataCache.CACHE_KEYS.localization, localizationData, 86400)
      }

        return localizationData;
    },
    getUnitsList: async function()
    {
      let unitData = metadataCache.get(metadataCache.CACHE_KEYS.comlinkUnitsList);
      if (unitData == undefined)
      {
        let version = await this.getLatestGamedataVersion();
        let response = await comlinkClient.getGameData(version, false, 3)
        unitData = response.units;
        metadataCache.set(metadataCache.CACHE_KEYS.comlinkUnitsList, unitData, 86400);
      }

      return unitData;
    },
    getModSetData: async function()
    {
      let msData = metadataCache.get(metadataCache.CACHE_KEYS.modSets);
      if (msData == undefined)
      {
        let version = await this.getLatestGamedataVersion();
        let response = await comlinkClient.getGameData(version, false, 2)

        msData = {
          statModSet: response.statModSet,
          statMod: response.statMod
        };

        let modDefinitionIdMap = {};
        for (let smIx = 0; smIx < response.statMod.length; smIx++)
        {
          let sm = response.statMod[smIx];
          modDefinitionIdMap[sm.id] = {
            setId: sm.setId,
            slot: sm.slot,
            rarity: sm.rarity
          };
        } 

        msData.modDefinitionIdMap = modDefinitionIdMap;
        
        metadataCache.set(metadataCache.CACHE_KEYS.modSets, msData, 86400);
      }
      return msData;
    },
    getDatacronData: async function()
    {
      let dcData = metadataCache.get(metadataCache.CACHE_KEYS.datacrons);
      if (dcData == undefined)
      {
        let version = await this.getLatestGamedataVersion();
        let response = await comlinkClient.getGameData(version, false, 4)

        dcData = {
          datacronSet: response.datacronSet,
          datacronAffixTemplateSet: response.datacronAffixTemplateSet,
          datacronTemplate: response.datacronTemplate
        };
        metadataCache.set(metadataCache.CACHE_KEYS.datacrons, dcData, 86400);
      }
      return dcData;
    },
    getPlayerPortriats: async function()
    {
      let playerPortraits = metadataCache.get(metadataCache.CACHE_KEYS.playerPortrait);
      if (playerPortraits == undefined)
      {
        let version = await this.getLatestGamedataVersion();
        let response = await comlinkClient.getGameData(version, false, 0);

        playerPortraits = response.playerPortrait;

        metadataCache.set(metadataCache.CACHE_KEYS.playerPortrait, playerPortraits, 86400);
      }
      return playerPortraits;
    }
}

// error: code == 51