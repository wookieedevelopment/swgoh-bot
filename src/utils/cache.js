const database = require ("../database.js")
const constants = require("./constants")

module.exports = {
    cachePlayerDataInDb: async function(playerData, timestamp, cacheType)
    {
        let comlinkGuildId = playerData.guildId;
        let wookieeBotGuildId = await database.db.oneOrNone("SELECT guild_id FROM guild WHERE comlink_guild_id = $1", [comlinkGuildId]);
        if (wookieeBotGuildId == null) wookieeBotGuildId = constants.NULL_GUILD_ID;
        else wookieeBotGuildId = wookieeBotGuildId.guild_id;

        let allycode = playerData.allyCode;
        var dataText = JSON.stringify(playerData);

        // await database.db.none(`
        // WITH ins_cd as (
        //     INSERT INTO cache_data (data_text, timestamp)
        //     VALUES ($2, $3::timestamp)
        //     RETURNING cache_data_id
        // ) INSERT INTO player_cache (allycode, timestamp, guild_id, comlink_guild_id, type, cache_data_id)
        //     VALUES ($1, $3, $4, $5, $6, (SELECT cache_data_id FROM ins_cd))
        //     ON CONFLICT (allycode, type)
        //     DO UPDATE SET (timestamp, guild_id, comlink_guild_id, cache_data_id) = (EXCLUDED.timestamp, EXCLUDED.guild_id, EXCLUDED.comlink_guild_id, EXCLUDED.cache_data_id);`, 
        // [allycode, dataText, timestamp, wookieeBotGuildId, comlinkGuildId, cacheType]);

        await database.db.none(`
        WITH new_data (data_text, timestamp) AS (
            VALUES ($2, $3::timestamp)
        ), dup as (
            select player_cache_id, cache_data_id from player_cache pc1
            where allycode = $1 and type = $6 and NOT EXISTS (select player_cache_id from player_cache pc2 where pc2.cache_data_id = pc1.cache_data_id and pc2.player_cache_id <> pc1.player_cache_id)
        ), ins_cd as (
            INSERT INTO cache_data (data_text, timestamp)
            select * from new_data
            RETURNING cache_data_id
        ), ins_pc as (
            INSERT INTO player_cache (allycode, timestamp, guild_id, comlink_guild_id, type, cache_data_id)
            VALUES ($1, $3, $4, $5, $6, (SELECT cache_data_id FROM ins_cd))
            ON CONFLICT (allycode, type)
            DO UPDATE SET (timestamp, guild_id, comlink_guild_id, cache_data_id) = (EXCLUDED.timestamp, EXCLUDED.guild_id, EXCLUDED.comlink_guild_id, EXCLUDED.cache_data_id)
        )
        DELETE FROM cache_data where cache_data_id = (select cache_data_id from dup);`, 
        [allycode, dataText, timestamp, wookieeBotGuildId, comlinkGuildId, cacheType]);

        let playerRosterValues = playerData.rosterUnit.map(ru => 
            {
                return {
                    allycode: allycode,
                    unit_def_id: ru.defId,
                    relic_level: ru.relic?.currentTier ?? -1,
                    gear_level: ru.currentTier,
                    rarity: ru.currentRarity,
                    timestamp: timestamp
                };
            });

        let playerRosterScript = database.pgp.helpers.insert(playerRosterValues, database.columnSets.playerRosterCS) + 
        ' ON CONFLICT(allycode,unit_def_id) DO UPDATE SET ' +
        database.columnSets.playerRosterCS.assignColumns({from: 'EXCLUDED', skip: ['allycode', 'unit_def_id']});

        await database.db.none(playerRosterScript);
    },
    cacheTbAssignments: async function(locationCode, metadata, missing, operationAssignments, playerAssignments, comlinkGuildId)
    {
        let missingJson = JSON.stringify(missing);
        let operationAssignmentsJson = JSON.stringify(operationAssignments);
        let playerAssignmentsJson = JSON.stringify(playerAssignments);

        await database.db.none(`
        INSERT INTO tb_assignments_cache (location_code, missing_json, op_assignments_json, player_assignments_json, timestamp, comlink_guild_id, metadata)
        VALUES ($1, $2, $3, $4, $5, $6, $7)
        ON CONFLICT (comlink_guild_id, location_code, metadata)
        DO UPDATE SET missing_json = $2, op_assignments_json = $3, player_assignments_json = $4, timestamp = $5`,
        [locationCode, missingJson, operationAssignmentsJson, playerAssignmentsJson, new Date(), comlinkGuildId, metadata])
    },
    loadTbAssignmentsFromCache: async function(locationCode, comlinkGuildId, metadata)
    {
        let cache = await database.db.oneOrNone(`
        SELECT * FROM tb_assignments_cache
        WHERE comlink_guild_id = $1 AND location_code = $2 AND metadata = $3 AND timestamp > now() - INTERVAL '4 hours'`,
        [comlinkGuildId, locationCode, metadata]);
        
        if (!cache) return null;

        return { missing: cache.missing_json, operationAssignments: cache.op_assignments_json, playerAssignments: cache.player_assignments_json };
    }
}