const database = require ("../database.js")
const metadataCache = require("./metadataCache");
const swapiUtils = require("./swapi")
const fs = require("fs");

const STAT_MANIPULATIONS = 
{
    "0": (stat) => { return Math.round(stat); },
    "1": (stat) => { return Math.round(stat); },
    "2": (stat) => { return Math.round(stat); },
    "3": (stat) => { return Math.round(stat); },
    "4": (stat) => { return Math.round(stat); },
    "5": (stat) => { return Math.round(stat); },
    "6": (stat) => { return Math.round(stat); },
    "7": (stat) => { return Math.round(stat); },
    "8": (stat) => { return (+(Math.round((stat*100) + "e+1")  + "e-1")).toString() + "%"; },
    "9": (stat) => { return (+(Math.round((stat*100) + "e+1")  + "e-1")).toString() + "%"; },
    "10": (stat) => { return Math.round(stat); },
    "11": (stat) => { return Math.round(stat); },
    "12": (stat) => { return Math.round(stat); },
    "13": (stat) => { return Math.round(stat); },
    "14": (stat) => { return (+(Math.round((stat*100) + "e+2")  + "e-2")).toString() + "%"; },
    "15": (stat) => { return Math.round(stat); },
    "16": (stat) => { return (+(Math.round((stat*100) + "e+2")  + "e-2")).toString() + "%"; },
    "17": (stat) => { return (+(Math.round((stat*100) + "e+2")  + "e-2")).toString() + "%"; },
    "18": (stat) => { return (+(Math.round((stat*100) + "e+2")  + "e-2")).toString() + "%"; },
    "19": (stat) => { return Math.round(stat); },
    "20": (stat) => { return Math.round(stat); },
    "21": (stat) => { return Math.round(stat); },
    "22": (stat) => { return Math.round(stat); },
    "23": (stat) => { return Math.round(stat); },
    "24": (stat) => { return (+(Math.round((stat*100) + "e+2")  + "e-2")).toString() + "%"; },
    "25": (stat) => { return Math.round(stat); },
    "26": (stat) => { return Math.round(stat); },
    "27": (stat) => { return (+(Math.round((stat*100) + "e+2")  + "e-2")).toString() + "%"; },
    "28": (stat) => { return Math.round(stat); },
    "29": (stat) => { return Math.round(stat); },
    "30": (stat) => { return Math.round(stat); },
    "31": (stat) => { return Math.round(stat); },
    "32": (stat) => { return Math.round(stat); },
    "33": (stat) => { return (+(Math.round((stat*100) + "e+2")  + "e-2")).toString() + "%"; },
    "34": (stat) => { return Math.round(stat); },
    "35": (stat) => { return (+(Math.round((stat*100) + "e+2")  + "e-2")).toString() + "%"; },
    "36": (stat) => { return Math.round(stat); },
    "37": (stat) => { return (+(Math.round((stat*100) + "e+2")  + "e-2")).toString() + "%"; },
    "38": (stat) => { return Math.round(stat); },
    "39": (stat) => { return (+(Math.round((stat*100) + "e+2")  + "e-2")).toString() + "%"; },
    "40": (stat) => { return (+(Math.round((stat*100) + "e+2")  + "e-2")).toString() + "%"; },
    "41": (stat) => { return Math.round(stat); },
    "42": (stat) => { return Math.round(stat); },
    "43": (stat) => { return Math.round(stat); },
    "44": (stat) => { return Math.round(stat); },
    "45": (stat) => { return Math.round(stat); },
    "46": (stat) => { return Math.round(stat); },
    "47": (stat) => { return Math.round(stat); },
    "48": (stat) => { return Math.round(stat); },
    "49": (stat) => { return Math.round(stat); },
    "50": (stat) => { return Math.round(stat); },
    "51": (stat) => { return Math.round(stat); },
    "52": (stat) => { return (+(Math.round((stat*100) + "e+2")  + "e-2")).toString() + "%"; },
    "53": (stat) => { return Math.round(stat); },
    "54": (stat) => { return Math.round(stat); },
    "55": (stat) => { return Math.round(stat); },
    "56": (stat) => { return Math.round(stat); },
    "57": (stat) => { return Math.round(stat); },
    "58": (stat) => { return Math.round(stat); },
    "59": (stat) => { return Math.round(stat); },
    "61": (stat) => { return Math.round(stat); },

    // EXTRA
    "5000": (stat) => { return Math.round(stat); }
}

module.exports = {
    formatStat: (value, statType)=> STAT_MANIPULATIONS[statType](value),
    getUnitPortraits: async function()
    {
        let portraits = metadataCache.get(metadataCache.CACHE_KEYS.unitPortraits);
        if (portraits == undefined)
        {
            portraits = await database.db.any("SELECT unit_def_id,img FROM unit_portrait");
            
            metadataCache.set(metadataCache.CACHE_KEYS.unitPortraits, portraits, 8640000)
        }
        return portraits;
    },
    getAbilityIcons: async function()
    {
        let abilityIcons = metadataCache.get(metadataCache.CACHE_KEYS.abilityIcons);
        if (abilityIcons == undefined)
        {
            abilityIcons = await database.db.any("SELECT img,def_id FROM unit_ability_icon");
            
            metadataCache.set(metadataCache.CACHE_KEYS.abilityIcons, abilityIcons, 8640000)
        }
        return abilityIcons;
    },
    getAbilityIconForUnitByBaseId: async function(abilityBaseId)
    {
        const abilityIcons = await this.getAbilityIcons();
        return abilityIcons.find(a => a.def_id === abilityBaseId)?.img;
    },
    getPortraitForUnitByDefId: async function(unitDefId)
    {
        const portraits = await this.getUnitPortraits();
        return portraits.find(p => p.unit_def_id === unitDefId)?.img;
    },
    getPortraitForUnit: async function(unitAPIData)
    {
        const portraits = await this.getUnitPortraits();
        const unitDefId = swapiUtils.getUnitDefId(unitAPIData);
        return portraits.find(p => p.unit_def_id === unitDefId)?.img;
    },
    getUnitDefIdByNickname: function(name, exact)
    {
        if (!name || name.length === 0) return null;
        let fixedName = name.replace(/\W/g, "").toLowerCase();

        let unitNicknames = metadataCache.get(metadataCache.CACHE_KEYS.unitNicknames);

        if (exact) return unitNicknames.lowerUnitNicknames[fixedName];

        return [...new Set(unitNicknames.lowerUnitNicknamesArray.filter(n => n.nickname.indexOf(fixedName) != -1).map(n => n.defId))];
    },
    getUnitMatchesByNickname: async function(name)
    {
        if (!name || name.length === 0) return null;
        let fixedName = name.replace(/\W/g, "").toLowerCase();

        const unitNicknames = metadataCache.get(metadataCache.CACHE_KEYS.unitNicknames);

        let nicknameMatch = unitNicknames.lowerUnitNicknames[fixedName];
        let unitsList = await swapiUtils.getUnitsList();
        let unitMatches;

        if (nicknameMatch) {
            unitMatches = unitsList.filter((u) => swapiUtils.getUnitDefId(u) === nicknameMatch);
    
            if (unitMatches.length === 1) return unitMatches;
    
            // typo in the unit_nicknames.json file
            logger.error(`There may be a typo in the unit_nicknames.json. Match found for ${cutUnitName} but no unit ${nicknameMatch} exists in API.`);
        }

        unitMatches = [];
        let exactMatch = null;
        for (let u of unitsList)
        {
            let cutNameKey = u.name.replace(/\W/g, "").toLowerCase();
            
            if (cutNameKey === fixedName)
            {
                exactMatch = [u];
                break;
            }

            if (cutNameKey.indexOf(fixedName) >= 0) unitMatches.push(u);
        }

        return exactMatch || unitMatches;
    },
    getUnitNicknameByDefId: function(defId)
    {
        const unitNicknames = metadataCache.get(metadataCache.CACHE_KEYS.unitNicknames);
        return unitNicknames.reverseNicknames[defId];
    },
    parseUnitsStringAsDefIds: async function(units)
    {
        if (!units) return null;
        
        let strippedUnits = units.replace(/\W/g, "").toLowerCase();
    
        if (strippedUnits.length === 0) return null;
    
        let split = units.split(/[,;|:]+/g);
        let parsedUnits = [];
    
        for (let input of split)
        {
            let matches = await this.getUnitMatchesByNickname(input);
            if (matches?.length > 0) parsedUnits.push(swapiUtils.getUnitDefId(matches[0]));
        }
    
        return parsedUnits;
    },
    unitHasAllFactions: function(u, factions)
    {
        if (!factions || factions.length === 0) return true;

        // if (factions[0] === "Wookiee")
        // {
        //     // special handling, because this isn't a real faction
        //     return WOOKIEE_DEF_IDS.find(w => w === swapiUtils.getUnitDefId(u)) != null;
        // }

        // otherwise, loop through all faction requirements and make sure the unit matches all of them
        for (let fac of factions)
        {
            if (!u.categories.find(c => c === fac)) return false;
        }

        return true;
    },
    getUnitAutocompleteOptions: async function(str)
    {
        let unitsList = await swapiUtils.getUnitsList();
        if (str) unitsList = unitsList.filter(u => u.name.toLowerCase().indexOf(str.toLowerCase()) >= 0);
        let options = unitsList.map(u => { return { name: u.name, value: swapiUtils.getUnitDefId(u) } });
        options.sort((a, b) => a.name.localeCompare(b.name));
        return options.slice(0, 25);
    },
    getUnitsNames: async function(units, useNicknames, omiUnitDefIds)
    {
        let allUnits = await swapiUtils.getUnitsList();
        let unitNames = units.map(u => {
            let omiText = "";
            if (omiUnitDefIds?.includes(u)) omiText = " (omi)";

            if (useNicknames)
            {
                let nickname = this.getUnitNicknameByDefId(u);
                if (nickname) return `${nickname}${omiText}`;    
            }
    
            let apiUnit = allUnits.find(unit => swapiUtils.getUnitDefId(unit) === u);
            return `${apiUnit.name}${omiText}`;
        });
    
        return unitNames;
    },
    getUnitDefIdsByAbilities: async function(abilitiesList)
    {
        if ((abilitiesList?.length ?? 0) === 0) return null;
        let allAbilities = await swapiUtils.getAbilitiesList();
        return allAbilities.filter(a => abilitiesList.find(ab => ab === a.base_id)).map(a => a.character_base_id);
    }
}


cacheUnitNicknames();
fs.watchFile("./src/data/unit_nicknames.json",
  (curr, prev) => { cacheUnitNicknames(); });

function cacheUnitNicknames() {
    fs.readFile("./src/data/unit_nicknames.json", { encoding: "utf-8" }, (err, data) => {
        const nicknamesData = {
            nicknames: JSON.parse(data),
            lowerUnitNicknames: {},
            lowerUnitNicknamesArray: [],
            reverseNicknames: {}
        };
        
        for (const n in nicknamesData.nicknames)
        {
            nicknamesData.lowerUnitNicknamesArray.push({ nickname: n.toLowerCase(), defId: nicknamesData.nicknames[n] });
            nicknamesData.lowerUnitNicknames[n.toLowerCase()] = nicknamesData.nicknames[n];
        }
        
        for (const key of Object.keys(nicknamesData.nicknames))
        {
            if (nicknamesData.reverseNicknames[nicknamesData.nicknames[key]]) continue;
            nicknamesData.reverseNicknames[nicknamesData.nicknames[key]] = key;
        }

        metadataCache.set(metadataCache.CACHE_KEYS.unitNicknames, nicknamesData, 1000 * 60 * 60 * 24 * 30);
    });
}