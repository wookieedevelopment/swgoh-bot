const discordUtils = require('./discord');
const guildUtils = require("./guild")
const swapi = require('./swapi');
const database = require('../database'); //TODO: Should move this to utils?
const statCalculator = require('swgoh-stat-calc');
const statNamingOption = require("../data/stats.json");
const { logger, formatError } = require('./log');
const constants = require('./constants');
const cacheUtils = require("./cache");
const raidCommon = require('../commands/raid/raidCommon');
const comlink = require('./comlink');
const tools = require("./tools");
const metadataCache = require("./metadataCache");
const _ = require("lodash");
const entitlements = require("./entitlements");

const UserRoles = {
    User: 1<<0,         // everyone
    GuildAdmin: 1<<1,   // for management of game activities
    GuildBotAdmin: 1<<2 // for permissions specifically related to the bot's configuration, not about the game itself
}

module.exports = {
    UserRoles: UserRoles,
    getUser: async function(userId) {
        const cleanUserId = discordUtils.cleanDiscordId(userId);
        const [userRoles, userRegs] = await database.db.multi(`
            SELECT guild_id,guild_admin,guild_bot_admin,comlink_guild_id,guild.guild_name 
            FROM user_guild_role 
            LEFT JOIN guild USING (guild_id) 
            WHERE discord_id = $1; 
            
            SELECT allycode,utc_offset,guild_players.guild_id,player_name,guild.guild_name,guild.comlink_guild_id,alt
            FROM user_registration 
            LEFT JOIN guild_players USING (allycode) 
            LEFT JOIN guild USING (guild_id)
            WHERE discord_id = $1
            ORDER BY alt ASC`, [cleanUserId]);
        
        if (!userRoles && !userRegs) return null;

        const userObj = {
            userId: cleanUserId,
            isBotOwner: discordUtils.isBotOwner(cleanUserId),
            utcOffset: 0,
            alts: new Array(),
            allycodes: new Array(),
            guilds: new Array()
        }

        if (userRoles && userRoles.length > 0) {
            userObj.guilds = userRoles.map(ur => { return {
                guildId: ur.guild_id,
                guildAdmin: ur.guild_admin,
                guildBotAdmin: ur.guild_bot_admin,
                comlinkGuildId: ur.comlink_guild_id,
                guildName: ur.guild_name,
                allycodes: userRegs.filter(reg => reg.guild_id === ur.guild_id).map(r => r.allycode)
            }});
        }

        if (userRegs && userRegs.length > 0)
        {
            userObj.utcOffset = userRegs[0].utc_offset;
            userObj.allycodes = userRegs.map(r => r.allycode);
            userObj.alts = userRegs.map(r => 
                { return { 
                    allycode: r.allycode, 
                    guildId: r.guild_id, 
                    name: r.player_name, 
                    alt: r.alt, 
                    guildName: r.guild_name,
                    comlinkGuildId: r.comlink_guild_id
                 };
                })

            // there may be some alts where there's an allycode but no name, because the player was deleted from guild_players
            // retrieve the name from comlink and insert into guild_players with null guild info
            for (let a of userObj.alts)
            {
                if (a.name) continue;

                let player = await comlink.getPlayerArenaProfileByAllycode(a.allycode);
                
                let name = swapi.getPlayerName(player);

                await database.db.any(
                    `INSERT INTO guild_players (allycode, player_name, player_id) 
                    VALUES ($1, $2, $3) 
                    ON CONFLICT (allycode) DO UPDATE SET player_name = EXCLUDED.player_name, player_id = EXCLUDED.player_id`,
                    [a.allycode, name, player.id]);
            }
        }

        return userObj;
    },
    convertToUserTime(datetime, userObj)
    {
        if (!userObj.utcOffset) return datetime;
        let dtConv = new Date(datetime);
        dtConv.setUTCSeconds(dtConv.getUTCSeconds() + 3600*userObj.utcOffset.hours + 60*userObj.utcOffset.minutes + userObj.utcOffset.seconds)
        return dtConv;
    },
    isSubscriberInteraction: async function(interaction)
    {
        if (interaction.entitlements)
        {
            for (const entitlement of interaction.entitlements.values()) {
                if (entitlement.skuId === entitlements.BASIC_SUBSCRIPTION_SKU_ID) {
                    logger.info(`User ${interaction.user?.id} has a basic subscription.`)
                    return true;
                }
            }
        }

        return this.isSubscriber(interaction.user?.id);
    },
    isSubscriber: async function(userId)
    {
        if (!userId) return false;

        let subscribedGuilds = await database.db.any(`
            SELECT guild_id 
            FROM guild 
            WHERE priority > 0 AND 
                guild_id IN (
                    SELECT guild_id 
                    FROM guild_players 
                    WHERE allycode IN (
                        SELECT allycode 
                        FROM user_registration 
                        WHERE discord_id = $1
                    )
                )`, [userId]);

        if (subscribedGuilds.length > 0) return true;
        return false;
    },
    getUserRegistration: async function(userId, allycode) {
        const cleanUserId = discordUtils.cleanDiscordId(userId);
        return await database.db.oneOrNone('SELECT * FROM user_registration WHERE discord_id = $1 AND allycode = $2', [cleanUserId, allycode]);
    },
    calcRosterStats: async function(roster)
    {
        await swapi.setupStatCalc();
        let count = statCalculator.calcRosterStats(roster, { gameStyle: true, calcGP: true, language: statNamingOption });
        return count;
    },
    calculateEHP: function(health, protection, armor)
    {
        return Math.round((health + (protection ? protection : 0)) / (1 - ((armor < 1) ? armor : (armor/100.0))));
    },
    getPlayerRaidScoresByPlayerId: async function(comlinkPlayerId, { raidId = raidCommon.DEFAULT_RAID.raidId, limit = 30 })
    {
        return await database.db.any(`SELECT end_time_seconds, score FROM player_raid_score WHERE player_id = $1 AND raid_id = $2 ORDER BY end_time_seconds DESC LIMIT ${limit}`, [comlinkPlayerId, raidId]);
    },
    getAllPlayerDataByAllyCode: async function (allycode, limit = 30) {
        return await database.db.any(`SELECT * FROM player_data WHERE allycode = $1 ORDER BY timestamp DESC LIMIT ${limit}`, [allycode]);
    },
    cleanAndVerifyStringAsAllyCode: function (arg) {
        return tools.cleanAndVerifyStringAsAllyCode(arg);
    },
    isGuildAdmin: async function(userId, guildId)
    {
        var me = await this.getUser(userId);
        if (!me) return false;

        var guild = me.guilds.find(g => g.guildId == guildId)
        if (!guild || (!guild.guildAdmin && !guild.guildBotAdmin))
            return false;
        
        return true;
    },
    authorize: async function(input, roles)
    {
        let result = { botUser: null, guildId: null, error: null }
     
        if (input == null)
        {
            result.error = "This Discord request is malformed.";
            return result;
        }
           
        try {
            result.botUser = await this.getUser(input.author ? input.author.id : input.user.id);
        } catch (e)
        {
            result.error = e.message;
            return result;
        }

        if (result.botUser == null || result.botUser.allycodes.length === 0)
        {
            result.error = "You are not registered with WookieeBot. Register with `/register`";
            return result;
        }

        if (result.botUser.guilds.length === 0)
        {
            result.error = "You do not have any guilds assigned in WookieeBot.";
            return result;
        }

        try {
            let guildIdForChannel = await guildUtils.getGuildIdByInput(input);

            if (result.botUser.guilds.find(g => g.guildId === guildIdForChannel)) result.guildId = guildIdForChannel;
        } catch (e)
        {
            result.error = e.message;
            return result;
        }

        // if (result.guildId == null)
        // {
        //     result.error = "There is no guild registered to this channel or server. Have an administrator run guild.addchannel or guild.addserver."
        //     return result;
        // }

        if (roles)
        {
            if (result.guildId == null) 
            {
                result.error = "Your guild is not linked to this channel or server. A guild admin can link with `/guild link`";
                return result;
            }
            
            let guildRoles = result.botUser.guilds.find(g => g.guildId == result.guildId);

            if ((((roles & UserRoles.User) == UserRoles.User) && !guildRoles)
                ||
                (((roles & UserRoles.GuildAdmin) == UserRoles.GuildAdmin) && (!guildRoles || !guildRoles.guildAdmin))
                ||
                (((roles & UserRoles.GuildBotAdmin) == UserRoles.GuildBotAdmin) && (!guildRoles || !guildRoles.guildBotAdmin))
                )
            {
                result.error = "You do not have permission to do this.";
            }
        }

        return result;
    },
    cachePlayerDataInDb: async function(data, timestamp, cacheType)
    {
        try {
            cacheUtils.cachePlayerDataInDb(data, timestamp, cacheType);
        } catch (error) {
            logger.error(formatError(error))
            return "Could not cache player: " + swapi.getPlayerAllycode(data);
        }
        return null;
    },
    calculateModScore: function(playerAPIData, modScoreType)
    {
        let modCounts = swapi.getPlayerModSummary(playerAPIData);
        let gpSquad = parseInt(swapi.getGPSquad(playerAPIData));

        switch (modScoreType)
        {
            case constants.MOD_SCORE_TYPES.WOOKIEEBOT:
                return (modCounts.speed15 + modCounts.speed20 + modCounts.speed25 + 0.5*modCounts.sixDot) / (gpSquad / 100000.0);
            case constants.MOD_SCORE_TYPES.DSR:
                return modCounts.speed15 / (gpSquad / 100000.0);
            case constants.MOD_SCORE_TYPES.HOTBOT:
                return ((modCounts.speed15-modCounts.speed20) * 0.8 + (modCounts.speed20-modCounts.speed25) + modCounts.speed25*1.6) / (gpSquad / 100000.0);
        }

        throw new Error(`No such mod score type: ${modScoreType}`)
    },
    chooseBestAlt: function(botUser, guildId, altNumInput, guildRequired = false)
    {
        if (!botUser || !botUser.alts || botUser.alts.length == 0) return null;

        if (altNumInput != null) return botUser.alts.find(a => a.alt == altNumInput);

        if (guildId)
        {
            let match = botUser.alts.find(a => a.guildId === guildId);
            if (match) return match;
        }

        if (!guildRequired) return botUser.alts.at(0);
        return botUser.alts.find(a => a.guildId != null);
    },
    chooseBestAllycode: function(botUser, guildId, allycodeInput, altNumInput, guildRequiredIfAllycodeNotProvided = false)
    {
        if (allycodeInput != null) return allycodeInput;

        let altMatch = this.chooseBestAlt(botUser, guildId, altNumInput, guildRequiredIfAllycodeNotProvided);

        return altMatch?.allycode;
    },
    getGuildPlayersAutocompleteOptions: async function(str, discordId)
    {
        str = str?.trim() ?? "";
        const names = await database.db.any(
            `
            SELECT * FROM 
            (
                (SELECT player_name, allycode, 1 o
                FROM guild_players
                WHERE guild_id IN (SELECT guild_id FROM guild_players WHERE allycode IN (SELECT allycode FROM user_registration WHERE discord_id = $1))
                AND (player_name ILIKE $2 OR allycode ILIKE $2)
                ORDER BY player_name ASC)
                UNION
                (SELECT player_name, allycode, 2 o
                FROM guild_players
                WHERE guild_id IN (SELECT guild_id FROM guild_players WHERE allycode IN (SELECT allycode FROM user_registration WHERE discord_id = $1))
                AND (player_name ILIKE $3 OR allycode ILIKE $3))
            ) q
            ORDER BY o ASC, player_name ASC
            LIMIT 25
            `, [discordId, `${str}%`, `_%${str}%`]
        );

        if (names.length == 0)
        {
            choices = [{ name: "No matching players in guild.", value: "x" }]
        }
        else
        {
            choices = names.map(choice => ({ name: `${choice.player_name} (${discordUtils.addDashesToAllycode(choice.allycode)})`, value: choice.allycode }));
        }  

        return choices;
    },
    getAltAutocompleteOptions: async function(str, discordId)
    {
        let intVal = parseInt(str);
        if (isNaN(intVal)) intVal = -1;

        let names = await database.db.any(
            `SELECT u.allycode, u.alt, u.discord_id, g.guild_name, gp.player_name
            FROM user_registration u
            JOIN guild_players gp ON u.allycode = gp.allycode
            LEFT OUTER JOIN guild g ON g.guild_id = gp.guild_id
            WHERE u.discord_id = $1 AND (gp.player_name ILIKE $2 OR u.alt = $3)
            ORDER BY u.alt LIMIT 25`,
            [discordId, `%${str.trim()}%`, intVal]
          );

        if (names.length == 0)
        {
            return [{ name: "No alts.", value: -1 }]
        }

        return names.map(choice => ({ name: `${choice.alt}: ${choice.player_name} (${discordUtils.addDashesToAllycode(choice.allycode)}) [${choice.guild_name ?? "No Guild"}]`, value: choice.alt }));
    },
    getPlayerNameByAllycode: async function(allycode)
    {
        let ac = tools.cleanAndVerifyStringAsAllyCode(allycode);
        const map = await this.getAllycodeNameMap();
        return map[ac];
    },
    getAllycodeNameMap: async function()
    {
        var map = metadataCache.get(metadataCache.CACHE_KEYS.allycodeNameMap);
        if (map == undefined || map === null)
        {
            let data = await database.db.any("SELECT player_name, allycode FROM guild_players");
            map = {};

            for (let d of data)
            {
                map[d.allycode] = d.player_name;
            }
            
            metadataCache.set(metadataCache.CACHE_KEYS.allycodeNameMap, map, 43200); // 12 hour life
        }
        return map;
    },
    // getPlayerAlts: async function(discordId)
    // {
    //     return await database.db.any(
    //         `SELECT u.allycode, u.alt, g.player_name
    //         FROM user_registration u
    //         JOIN guild_players g ON u.allycode = g.allycode
    //         WHERE u.discord_id = $1`, [discordId]
    //     );
    // },
    // getPlayerAltById: async function (discordId, alt = 0) {
    //     return await database.db.one("SELECT allycode, alt, discord_id FROM user_registration WHERE discord_id = $1 AND alt = $2", [discordId, alt]);
    // },
    // getPlayerAltByAllycode: async function (allycode) {
    //     return await database.db.any(
    //         `SELECT u.alt, u.allycode, g.player_name
    //         FROM user_registration u
    //         JOIN guild_players g ON u.allycode = g.allycode
    //         WHERE u.allycode = $1`,
    //         [allycode]
    //     )
    // }
}