

module.exports = {

    cleanAndVerifyStringAsAllyCode: function (arg) {
        if (!arg) throw new Error("No ally code provided.");

        const ALLYCODE_REGEX = /.*\(([1-9]{3}-?[1-9]{3}-?[1-9]{3})\).*/g
        let match = ALLYCODE_REGEX.exec(arg);
        if (match?.length == 2)
        {
            arg = match[1];
        }

        const allycode = arg.replace(/[^1-9]/g, "").trim();

        if (allycode.length != 9 || isNaN(allycode))
            throw new Error("Invalid ally code (" + arg + ").  Needs to be 9 digits.");
        if (allycode.indexOf("0") >= 0)
            throw new Error("Invalid ally code (" + arg + ").  Must contain only the numbers 1-9.");
            
        return allycode;
    },
    removeAccents: function(s) {
        return s?.normalize("NFD").replace(/[^\u0000-\u007F]/g, "");
    },
    sleep: function (ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }
}