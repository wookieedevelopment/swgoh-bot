const { CLIENT } = require("./discordClient");
const tools = require('./tools');
const database = require("../database");
const _ = require("lodash");
const { logger } = require("./log");
const async = require("async");
const discordUtils = require("./discord")

const SKU_ID = '1327377200110112768';
const DELAY_BETWEEN_ENTITLEMENT_POLLING_MS = 30000; // 30 seconds

async function fetchEntitlements() {
    await CLIENT.application?.fetch();
    await CLIENT.application?.entitlements?.fetch({ excludeEnded: true });
    return CLIENT.application?.entitlements?.cache;
}

function getSubscriberIds(entitlements) {
    const subscriberIds = new Array();
    for (const entitlement of entitlements.values()) {
        if (entitlement.skuId === SKU_ID) {
            // logger.log(`Found entitlement for ${entitlement.skuId}`);
            subscriberIds.push(entitlement.userId);
        }
    }
    return subscriberIds;
}

async function getSubscriberAllycodes(subscriberIds) {
    if (subscriberIds.length === 0) return [];
    return await database.db.any(
        `SELECT allycode FROM user_registration WHERE discord_id IN ($1:csv)`,
        [subscriberIds]
    );
}

async function getSubscriberGuilds(subscriberAllycodes) {
    if (subscriberAllycodes.length === 0) return [];
    return await database.db.any(
        `SELECT comlink_guild_id FROM guild_players LEFT JOIN guild USING (guild_id) WHERE allycode IN ($1:csv) AND comlink_guild_id IS NOT NULL`,
        [subscriberAllycodes.map(a => a.allycode)]
    );
}

async function updateGuildPriorities(subscriberGuilds) {
    if (subscriberGuilds.length === 0) {
        // if no users have entitlements, set all entitlement guilds (priority 2) to priority 1 (so syncbot can handle)
        await database.db.any(`
            UPDATE guild
            SET priority = 1
            WHERE priority = 2;`);
        return;
    }

    // if a user's entitlement disappears, set their guild to priority 1 so that syncbot can check for 
    // subscriptions to WookieeTools. If SyncBot doesn't find a WT subscription, then it will set the priority to 0.
    await database.db.any(`
        UPDATE guild
        SET priority = 2
        WHERE comlink_guild_id IN ($1:csv) AND priority < 2;

        UPDATE guild
        SET priority = 1
        WHERE priority = 2 AND comlink_guild_id NOT IN ($1:csv);`,
        [_.uniq(subscriberGuilds.map(g => g.comlink_guild_id))]
    );
}

async function pollForEntitlements() {
    try {
        const entitlements = await fetchEntitlements();
        const subscriberIds = getSubscriberIds(entitlements);
        const subscriberAllycodes = await getSubscriberAllycodes(subscriberIds);
        const subscriberGuilds = await getSubscriberGuilds(subscriberAllycodes);
        await updateGuildPriorities(subscriberGuilds);    
    }
    catch (e) {
        logger.error(e);
    }
}

async function startPollingForEntitlements() {
    async.forever(
        function (next)
        {
            pollForEntitlements()
                .catch((err) => {
                    logger.error(err);
                })
                .finally(() => setTimeout(next, DELAY_BETWEEN_ENTITLEMENT_POLLING_MS));
        },
        function (err)
        {
            logger.error(err);
        }
    );
}

function generateNotSubscribedResponseObject() {
    return { 
        content: `This is only available for subscribers at this time. Learn more on our WookieeTools server: ${discordUtils.WOOKIEEBOT_DISCORD_LINK}`,
        components: [
            {
                type: 1,
                components: [
                    {
                        type: 2,
                        style: 6,
                        sku_id: '1327377200110112768'
                    }
                ]
            }
        ]
    };
}

module.exports = {
    BASIC_SUBSCRIPTION_SKU_ID: SKU_ID,
    fetchEntitlements,
    getSubscriberIds,
    getSubscriberAllycodes,
    getSubscriberGuilds,
    updateGuildPriorities,
    pollForEntitlements,
    generateNotSubscribedResponseObject,
    startPollingForEntitlements
}
