const { InteractionType } = require("discord.js");
const database = require("../database");
const { logger, formatError } = require("../utils/log");

module.exports = {
    startActivity: startActivity,
    finishActivity: finishActivity
}

const BOT_ACTIVITY_INSERT_QUERY = `
INSERT INTO bot_activity (discord_user_id, start_dt, interaction_string)
VALUES ($1, $2, $3);
`;

const BOT_ACTIVITY_DELETE_QUERY = `
DELETE FROM bot_activity 
WHERE discord_user_id = $1 and start_dt = $2 and interaction_string = $3;
`;

const BOT_USAGE_START_QUERY = `
INSERT INTO bot_usage (bot_usage_id, started_count, last_used_dt)
VALUES ($4, 1, $2)
ON CONFLICT (bot_usage_id) DO UPDATE SET started_count = bot_usage.started_count+1, last_used_dt = $2;
`;

const BOT_USAGE_FINISH_QUERY = `
UPDATE bot_usage
SET finished_count = finished_count+1
WHERE bot_usage_id = $4;
`;

function buildFullCommandName(interaction)
{
    if (!interaction.commandName) return null;
    let group, subcommand;
    try { group = interaction.options.getSubcommandGroup(); } catch {}
    try { subcommand = interaction.options.getSubcommand(); } catch {}

    let cmd = `${interaction.commandName}${group ? ` ${group}` : ""}${subcommand ? ` ${subcommand}` : ""}`;

    return cmd;
}

function startActivity(interaction)
{
    let now = new Date();
    let commandName = buildFullCommandName(interaction);
    let interactionStr = (interaction.type === InteractionType.ApplicationCommand) 
                        ? (interaction.toString() === "[object Object]" ? interaction.commandName : interaction.toString() )
                        : interaction.customId;

    if (!interactionStr) return;
    
    logger.info(`[${now.toUTCString()}] Starting ${interactionStr}`);

    let query = BOT_ACTIVITY_INSERT_QUERY;
    let params = [interaction.user?.id, now, interactionStr];
    if (commandName) 
    {
        query += BOT_USAGE_START_QUERY;
        params.push(commandName);
    }

    database.db.any(query, params).catch((reason) => { logger.error(reason) });

    return now;
}

function finishActivity(interaction, startDate)
{
    let endStamp = new Date();
    let commandName = buildFullCommandName(interaction);
    let interactionStr = (interaction.type === InteractionType.ApplicationCommand) 
                        ? (interaction.toString() === "[object Object]" ? interaction.commandName : interaction.toString() )
                        : interaction.customId;
                        
    if (!interactionStr) return;

    logger.info(`[${endStamp.toUTCString()}] Finished ${interactionStr}. Duration: ${(endStamp - startDate).toLocaleString()}ms`);

    let query = BOT_ACTIVITY_DELETE_QUERY;
    let params = [interaction.user?.id, startDate, interactionStr];

    if (commandName) 
    {
        query += BOT_USAGE_FINISH_QUERY;
        params.push(commandName);
    }

    database.db.any(query, params).catch((reason) => { logger.error(reason); });
}