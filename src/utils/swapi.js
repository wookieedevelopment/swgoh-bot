const got = require('got');
const { logger, formatError } = require('./log');
const statCalc = require("./stats/stats");
const comlink = require('./comlink');
const metadataCache = require("./metadataCache");
const statsStringData = require("../data/stats.json");
const database = require("../database")
const constants = require('./constants');
const discordUtils = require("./discord");
const cacheUtils = require("./cache");
const datacronUtils = require("./datacron");
const tools = require("./tools");
const NodeCache = require('node-cache');

const statEnum = {
    MAX_HEALTH: 1,
    STRENGTH: 2,
    AGILITY: 3,
    INTELLIGENCE: 4,
    SPEED: 5,
    PHYSICAL_DAMAGE: 6, // ATTACK_DAMAGE
    SPECIAL_DAMAGE: 7, // ABILITY_POWER
    ARMOR: 8,
    SUPPRESSION: 9,
    ARMOR_PENETRATION: 10,
    SUPPRESSION_PENETRATION: 11,
    DODGE_RATING: 12,
    DEFLECTION_RATING: 13,
    ATTACK_CRITICAL_RATING: 14,
    ABILITY_CRITICAL_RATING: 15,
    CRITICAL_DAMAGE: 16,
    POTENCY: 17,   // named ACCURACY in code
    TENACITY: 18, // named RESISTANCE in code
    DODGE_PERCENT_ADDITIVE: 19,
    DEFLECTION_PERCENT_ADDITIVE: 20,
    ATTACK_CRITICAL_PERCENT_ADDITIVE: 21,
    ABILITY_CRITICAL_PERCENT_ADDITIVE: 22,
    ARMOR_PERCENT_ADDITIVE: 23,
    SUPPRESSION_PERCENT_ADDITIVE: 24,
    ARMOR_PENETRATION_PERCENT_ADDITIVE: 25,
    SUPPRESSION_PENETRATION_PERCENT_ADDITIVE: 26,
    HEALTH_STEAL: 27,
    MAX_SHIELD: 28,
    SHIELD_PENETRATION: 29,
    HEALTH_REGEN: 30,
    ATTACK_DAMAGE_PERCENT_ADDITIVE: 31,
    ABILITY_POWER_PERCENT_ADDITIVE: 32,
    DODGE_NEGATE_PERCENT_ADDITIVE: 33,
    DEFLECTION_NEGATE_PERCENT_ADDITIVE: 34,
    ATTACK_CRITICAL_NEGATE_PERCENT_ADDITIVE: 35,
    ABILITY_CRITICAL_NEGATE_PERCENT_ADDITIVE: 36,
    DODGE_NEGATE_RATING: 37,
    // PHYSICAL_ACCURACY: 37, 
    SPECIAL_ACCURACY: 38,
    PHYSICAL_CRITICAL_AVOIDANCE: 39,
    // ATTACK_CRITICAL_NEGATE_RATING: 39,
    SPECIAL_CRITICAL_AVOIDANCE: 40,
    UNIT_STAT_OFFENSE: 41,
    DEFENSE: 42,
    DEFENSE_PENETRATION: 43,
    EVASION_RATING: 44,
    CRITICAL_RATING: 45,
    EVASION_NEGATE_RATING: 46,
    CRITICAL_NEGATE_RATING: 47,
    OFFENSE_PERCENT_ADDITIVE: 48,
    DEFENSE_PERCENT_ADDITIVE: 49,
    DEFENSE_PENETRATION_PERCENT_ADDITIVE: 50,
    EVASION_PERCENT_ADDITIVE: 51,
    PHYSICAL_ACCURACY: 52,
    CRITICAL_CHANCE_PERCENT_ADDITIVE: 53,
    CRITICAL_NEGATE_CHANCE_PERCENT_ADDITIVE: 54,
    MAX_HEALTH_PERCENT_ADDITIVE: 55,
    MAX_SHIELD_PERCENT_ADDITIVE: 56,
    SPEED_PERCENT_ADDITIVE: 57,
    COUNTER_ATTACK_RATING: 58,
    TAUNT: 59,
    MASTERY: 61,
    EXTRA: {
        EHP: 5000
    }
  };

const modPrimaryStats = {
    SPEED: statEnum.SPEED,
    OFFENSE_PERCENT: statEnum.OFFENSE_PERCENT_ADDITIVE,
    DEFENSE_PERCENT: statEnum.DEFENSE_PERCENT_ADDITIVE,
    MAX_HEALTH_PERCENT: statEnum.MAX_HEALTH_PERCENT_ADDITIVE,
    MAX_SHIELD_PERCENT_ADDITIVE: statEnum.MAX_SHIELD_PERCENT_ADDITIVE,
}

const omicronModeEnum = {
    // CG values
    ALL_OMICRON: 1,
    PVE_OMICRON: 2,
    PVP_OMICRON: 3,
    GUILD_RAID_OMICRON: 4,
    TERRITORY_STRIKE_OMICRON: 5,
    TERRITORY_COVERT_OMICRON: 6,
    TERRITORY_BATTLE_BOTH_OMICRON: 7, // all TB
    TERRITORY_WAR_OMICRON: 8, // TW
    TERRITORY_TOURNAMENT_OMICRON: 9, // GAC
    WAR_OMICRON: 10,
    CONQUEST_OMICRON: 11,
    GALACTIC_CHALLENGE_OMICRON: 12,
    PVE_EVENT_OMICRON: 13,
    TERRITORY_TOURNAMENT_3_OMICRON: 14,
    TERRITORY_TOURNAMENT_5_OMICRON: 15
}

const FIND_UNIT_ERROR_CODES = {
    CORRUPT_PLAYER_DATA: 0,
    NOT_UNLOCKED: 1,
    TOO_LOW_RARITY: 2,
    TOO_LOW_RELIC: 3
};

const PLAYER_CACHE = new NodeCache({useClones: false});
const MAX_GET_PLAYER_ATTEMPTS = 5;
const SCALING_FACTOR_MILLIS = 2000;

module.exports = {
    STATS: statEnum,
    OMICRON_MODE: omicronModeEnum,
    FIND_UNIT_ERROR_CODES: FIND_UNIT_ERROR_CODES,
    // omicronAbilities: new Array(),
    unitImageCache: {},
    getUnitImage: async function(baseId)
    {
        if (this.unitImageCache[baseId])
            return unitImageCache[baseId];
            
        var image = await got("https://swgoh.gg/game-asset/u/" + baseId + ".png").buffer();
        unitImageCache[baseId] = image;
        return image;
    },
    getAbilitiesList: async function()
    {
        var list = metadataCache.get(metadataCache.CACHE_KEYS.abilitiesList);
        if (list == undefined || list === null)
        {
            let data = await database.db.one("SELECT value FROM metadata_cache WHERE key = $1", [metadataCache.CACHE_KEYS.abilitiesList]);
            list = JSON.parse(data.value);
            
            metadataCache.set(metadataCache.CACHE_KEYS.abilitiesList, list, 86400);
        }
        return list;
    },
    getOmicronAbilitiesList: async function()
    {
        var list = metadataCache.get(metadataCache.CACHE_KEYS.omicronsList);
        if (list == undefined || list === null)
        {
            let abilitiesList = await this.getAbilitiesList();
            list = abilitiesList.filter(a => a.is_omicron);
            metadataCache.set(metadataCache.CACHE_KEYS.omicronsList, list, 86400);
        }
        return list;
    },
    getDatacronsList: async function()
    {
        return await comlink.getDatacronData();
    },
    getPlayerPortriatsList: async function()
    {
        var portraits = metadataCache.get(metadataCache.CACHE_KEYS.playerPortrait);
        if (portraits == undefined)
        {
            portraits = await database.db.any("SELECT player_portrait_id,img FROM player_portrait");
            
            metadataCache.set(metadataCache.CACHE_KEYS.playerPortrait, portraits, 8640000)
        }
        return portraits;
    },
    getActiveWookieeBotDatacronData: datacronUtils.getActiveWookieeBotDatacronData,
    getPlayers: async function(
        { allycodes = [], 
          useCache = false, 
          cacheTimeHours = 4, 
          cacheType = constants.CacheTypes.QUERY, 
          updateStatusCallback = null,
          cacheUpdatedPlayerData = true })
    {
        let players = new Array();
        let errors = new Array();
        let nextStatusUpdateIx = Math.ceil(Math.random()*5)+8;

        if (useCache)
        {
            const caches = await database.db.any(`
            SELECT * FROM (
                SELECT
                    ROW_NUMBER() OVER (
                        PARTITION BY allycode 
                        ORDER BY player_cache.timestamp desc
                    ) AS r,
                    allycode, 
                    data_text
                FROM player_cache
                LEFT JOIN cache_data USING (cache_data_id)
                WHERE allycode in ($1:csv) AND player_cache.timestamp >= NOW() - interval '${cacheTimeHours} hours'
            ) x
            WHERE x.r <= 1`,
            [allycodes]);

            for (let pc of caches)
            {
                let player = JSON.parse(pc.data_text ?? null);
                if (player) players.push(player);
            }
            

            if (updateStatusCallback && players.length >= nextStatusUpdateIx) {
                await updateStatusCallback(allycodes, players, errors);
                nextStatusUpdateIx = players.length + Math.floor(Math.random()*5+10);
            }
        }

        let missingAllycodes = allycodes.filter(a => !players.find(p => this.getPlayerAllycode(p) === a));

        for (let ac = 0; ac < missingAllycodes.length; ac++)
        {
            let player;
            
            try {
                player = await this.getPlayer(missingAllycodes[ac], cacheType, cacheUpdatedPlayerData);
            } catch (e)
            {
                logger.error(`Failed to get player from comlink: ${missingAllycodes[ac].toString()}. ${formatError(e)}`);
                errors.push({ allycode: missingAllycodes[ac], message: e.message });
            }

            if (player) players.push(player);

            if (updateStatusCallback && ac === nextStatusUpdateIx) {
                await updateStatusCallback(allycodes, players, errors);
                nextStatusUpdateIx += Math.floor(Math.random()*5+10);
            }
        }

        if (updateStatusCallback) await updateStatusCallback(allycodes, players, errors);

        return { players: players, errors: errors };
    },
    getPlayer: async function(allycodeOrPlayerId, cacheType = constants.CacheTypes.QUERY, cacheUpdatedPlayerData = true)
    {
        for (let attempt = 1; attempt <= MAX_GET_PLAYER_ATTEMPTS; attempt++)
        {
            try {
                return await this.getPlayerInternal(allycodeOrPlayerId, cacheType, cacheUpdatedPlayerData);
            }
            catch (e) {
                // invalid allycode, so don't retry
                if (e.message.startsWith("Failed to find")) throw e;
                if (e.message.indexOf("Error while validating") != -1) throw e;

                logger.error(`Failed to get player ${allycodeOrPlayerId} from comlink (Attempt ${attempt}): ${formatError(e)}`);
            }

            await tools.sleep(SCALING_FACTOR_MILLIS * (attempt ** 2));
        }
    },
    getPlayerInternal: async function(allycodeOrPlayerId, cacheType = constants.CacheTypes.QUERY, cacheUpdatedPlayerData = true)
    {
        if (!allycodeOrPlayerId) return null;

        // primary source is comlink
        let data;
        if (allycodeOrPlayerId.toString().length === 9) data = await comlink.getPlayer(allycodeOrPlayerId);
        else data = await comlink.getPlayerByPlayerId(allycodeOrPlayerId);

        logger.info(`Retrieved player ${this.getPlayerAllycode(data)} from comlink.`)
        
        if (data.rosterUnit.length > 0)
        {
            statCalc.calcRosterStats(data.rosterUnit, { gameStyle: true, calcGP: true });
        }

        let wookieeBotDcData = await datacronUtils.getActiveWookieeBotDatacronData();
        let unitsList = await this.getUnitsList();
        let abilitiesList = await this.getAbilitiesList();
        let modSetsData = await comlink.getModSetData();

        for (dcIx in data.datacron)
        {
            let dc = data.datacron[dcIx];
            dc.meta = {
                alignment: null,
                faction: null,
                character: null
            }

            let affixes = dc.affix.filter(a => a.abilityId != null && a.abilityId.length > 0);
            for (var aix in affixes)
            {
                let affix = affixes[aix];
                let wookieeDc = wookieeBotDcData.find(
                                        d => 
                                            d.set_id == dc.setId &&
                                            (d.ability_id == affix.abilityId || d.duplicate_ability_ids?.indexOf(affix.abilityId) >= 0) && 
                                            d.target_rule == affix.targetRule);
                
                if (!wookieeDc) continue;

                if (wookieeDc.alignment) dc.meta.alignment = wookieeDc.alignment;
                if (wookieeDc.faction) dc.meta.faction = wookieeDc.faction;
                if (wookieeDc.character) dc.meta.character = wookieeDc.character;

                affix.short_description = wookieeDc.short_description;
                affix.wookieeBotDatacronId = wookieeDc.datacron_id;
                affix.level = aix + 1;
            }
        }

        let playerOverview = {
            zetas: 0,
            omicrons: new Array(Object.keys(omicronModeEnum).length),
            mods: {
                sixDot: 0,
                speed25: 0,
                speed20: 0,
                speed15: 0,
                speed10: 0,
                offense6: 0
            },
            gears: new Array(15).fill(0),
            relics: new Array(15).fill(0),
            galacticLegends: new Array(),
            capitalShips: new Array()
        };

        let gls = unitsList.filter(u => u.is_galactic_legend).map(u => this.getUnitDefId(u));
        let capitalShips = unitsList.filter(u => u.is_capital_ship).map(u => this.getUnitDefId(u));

        // do some manipulation of comlink results to reduce future calculation times
        for (var ru in data.rosterUnit)
        {
            let u = data.rosterUnit[ru];
            
            if (this.getUnitCombatType(u) == 1) {
                let gearLevel = this.getUnitGearLevel(u);
                if (gearLevel) playerOverview.gears[gearLevel]++;
                let relicLevel = this.getUnitRelicLevel(u);
                if (relicLevel) playerOverview.relics[relicLevel]++;
            }

            if (capitalShips.indexOf(this.getUnitDefId(u)) >= 0)
            {
                playerOverview.capitalShips.push(this.getUnitDefId(u));
                u.is_capital_ship = true;
            }

            if (gls.indexOf(this.getUnitDefId(u)) >= 0)
            {
                playerOverview.galacticLegends.push(this.getUnitDefId(u));
                u.is_galactic_legend = true;
            }

            u.equippedStatMod?.forEach(mod => {
                // best way I could see of finding 6dot mods from comlink
                if (mod.xp >= 1105000) playerOverview.mods.sixDot++;
                
                mod.secondaryStat
                    .forEach(ss => {
                        if (ss.stat.unitStatId == this.STATS.SPEED)
                        {
                            let value = parseInt(ss.stat.statValueDecimal)/10000;
                            if (value >= 25) playerOverview.mods.speed25++;
                            if (value >= 20) playerOverview.mods.speed20++;
                            if (value >= 15) playerOverview.mods.speed15++;
                            if (value >= 10) playerOverview.mods.speed10++;    
                        }
                        else if (ss.stat.unitStatId == this.STATS.OFFENSE_PERCENT_ADDITIVE)
                        {
                            let value = parseInt(ss.stat.statValueDecimal)/100;
                            if (value >= 6) playerOverview.mods.offense6++;
                        }
                    });

                mod.setId = modSetsData.modDefinitionIdMap[mod.definitionId];
            });
            
            u.defId = this.getUnitDefId(u); 
            u.omicron_abilities = (await this.getUnitOmicronAbilities(u))?.map(a => a.base_id);
            u.zeta_abilities = (await this.getUnitZetaAbilities(u))?.map(a => a.base_id);

            u.omicron_abilities?.forEach(a => 
                {
                    let mode = abilitiesList.find(ability => ability.base_id === a)?.omicron_mode;
                    if (mode != omicronModeEnum.ALL_OMICRON) {
                        if (playerOverview.omicrons[mode] == null) playerOverview.omicrons[mode] = new Array();
                        playerOverview.omicrons[mode].push(a);
                    }
                });

            playerOverview.zetas += await this.getUnitZetaCount(u);

            // maybe it's possible this doesn't exist?
            let apiUnit = unitsList.find(unit => this.getUnitDefId(unit) === this.getUnitDefId(u));
            if (apiUnit) 
            {
                u.name = apiUnit.name;
                u.alignment = apiUnit.alignment;
                u.categories = apiUnit.categories;
                u.categoryId = apiUnit.categoryId;
            }
        }; 

        data.overview = playerOverview;
        
        if (cacheUpdatedPlayerData)
        {
            let mark = new Date();
            try {
                await cacheUtils.cachePlayerDataInDb(data, new Date(), cacheType);
            } catch (err)
            {
                logger.error(`Failed to cache data for player ${this.getPlayerAllycode(data)} in swapi.getPlayer. Not breaking process.\r\n${formatError(err)}`);
            }

            logger.info(`Cache took ${new Date().getTime() - mark.getTime()}ms`);
        }

        return data;
    },
    getUnitsList: async function () {
        // var list = metadataCache.get(metadataCache.CACHE_KEYS.unitsList);
        // if (list == undefined)
        // {
        //     let data = await got("http://api.swgoh.gg/units");
        //     ggUnits = JSON.parse(data.body).data;

        //     let list = await comlink.getUnitsList();
        //     let localizationData = await comlink.getLocalization();
        //     list.forEach(u => 
        //         {
        //             u.name = localizationData.find(l => l.key === u.nameKey)?.value;
        //             u.is_galactic_legend = u.categoryId.indexOf("galactic_legend") != -1;

        //             let ggEntry = ggUnits.find(ggu => this.getUnitDefId(ggu) === this.getUnitDefId(u));

        //             if (!ggEntry)
        //             {
        //                 logger.info(`No match in swgoh.gg unit data for unit ${this.getUnitDefId(u)} when populating categories.`);
        //             } else {
        //                 u.categories = ggEntry.categories;
        //             }
        //         })

        //     metadataCache.set(metadataCache.CACHE_KEYS.unitsList, list, 86400)
        // }
        // return list;

        var list = metadataCache.get(metadataCache.CACHE_KEYS.unitsList);
        if (list == undefined || list === null)
        {
            let data = await database.db.one("SELECT value FROM metadata_cache WHERE key = $1", [metadataCache.CACHE_KEYS.unitsList]);
            list = JSON.parse(data.value);

            metadataCache.set(metadataCache.CACHE_KEYS.unitsList, list, 86400)
        }
        return list;
    },
    setupStatCalc: async function () {
        let gameData = metadataCache.get(metadataCache.CACHE_KEYS.gameStatsData);
        if (gameData == undefined)
        {
            try {
                gameData = await got('https://swgoh.shittybots.me/api/data/crinolo_core.json', 
                    {
                        timeout: 10000, 
                        headers: { "shittybot": process.env.SHITTYBOT_TOKEN }
                    }
                ).json();
            } catch (err)
            {
                logger.error("Error retrieving crinolo_core json from shittybots: " + formatError(err))
                try {
                    gameData = await got('https://swgoh-stat-calc.glitch.me/gameData.json').json();
                } catch (err)
                {
                    logger.error("Error retrieving game data from swgoh-stat-calc glitch: " + formatError(err));
                    throw err;
                }
            }
            metadataCache.set(metadataCache.CACHE_KEYS.gameStatsData, gameData, 86400)
            statCalculator.setGameData( gameData );
        }
    },
    getUltDataForPlayer: function(player)
    {
        let ultUnits = this.getUnitsWithUltimate(player);
        return { allycode: this.getPlayerAllycode(player), ultUnits: ultUnits, error: null };
    },
    getPlayerGuildId: function(playerAPIData)
    {
        if (playerAPIData.guildId) return playerAPIData.guildId;
        if (playerAPIData.data) return playerAPIData.data.guild_id;
    },
    getPlayerGuildMemberLevel: function(playerAPIData)
    {
        return playerAPIData.memberLevel ?? playerAPIData.guildMemberLevel ?? playerAPIData.member_level;
    },
    getGP: function(playerAPIData)
    {
        if (playerAPIData.profileStat) return playerAPIData.profileStat.find(s => s.nameKey == comlink.profileStatsNames.GP).value;
        if (playerAPIData.data) return playerAPIData.data.galactic_power;
        if (playerAPIData.galacticPower) return playerAPIData.galacticPower;
        return playerAPIData.stats[0].value;
    },
    getGPSquad: function(playerAPIData)
    {
        if (playerAPIData.profileStat) return playerAPIData.profileStat.find(s => s.nameKey == comlink.profileStatsNames.GPCharacters).value;
        if (playerAPIData.data) return playerAPIData.data.character_galactic_power;
        return playerAPIData.stats[1].value;
    },
    getGPFleet: function(playerAPIData)
    {
        if (playerAPIData.profileStat) return playerAPIData.profileStat.find(s => s.nameKey == comlink.profileStatsNames.GPShips).value;
        if (playerAPIData.data) return playerAPIData.data.ship_galactic_power;
        return playerAPIData.stats[2].value;
    },
    getSquadArenaRank: function(playerAPIData)
    {
        if (playerAPIData.pvpProfile) return playerAPIData.pvpProfile[0].rank;
        if (playerAPIData.data) return playerAPIData.data.arena.rank;
        return playerAPIData.arena.char.rank;
    },
    getFleetArenaRank: function(playerAPIData)
    {
        if (playerAPIData.pvpProfile) return playerAPIData.pvpProfile[1]?.rank;
        if (playerAPIData.data) return playerAPIData.data.fleet_arena?.rank;
        return playerAPIData.arena.ship?.rank;
    },
    getPlayerName: function(playerAPIData)
    {
        if (playerAPIData.data) return playerAPIData.data.name;
        return playerAPIData.name; // both .help and comlink
    },
    getPlayerAllycode: function(playerAPIData)
    {
        if (playerAPIData.data) return playerAPIData.data.ally_code;
        return playerAPIData.allyCode; // both .help and comlink
    },
    getPlayerAllycodeWithDashes: function(playerAPIData)
    {
        let allycode = this.getPlayerAllycode(playerAPIData);
        return discordUtils.addDashesToAllycode(allycode);
    },
    getPlayerSkillRating: function(playerAPIData)
    {
        if (playerAPIData.playerRating) return playerAPIData.playerRating.playerSkillRating?.skillRating;
        if (playerAPIData.data) return playerAPIData.data.skill_rating;
        return null;
    },
    getPlayerRoster: function(playerAPIData)
    {
        if (playerAPIData.rosterUnit) return playerAPIData.rosterUnit;
        if (playerAPIData.rosterUnitList) return playerAPIData.rosterUnitList;
        return playerAPIData.units ?? playerAPIData.roster;
    },
    getPlayerPayoutUTCOffsetMinutes: function(playerAPIData)
    {
        if (playerAPIData.localTimeZoneOffsetMinutes) return playerAPIData.localTimeZoneOffsetMinutes;
        if (playerAPIData.poUTCOffsetMinutes) return playerAPIData.poUTCOffsetMinutes;
        return null;
    },
    getPlayerUnlockedPortraits: function(playerAPIData)
    {
        if (playerAPIData.unlockedPlayerPortrait) return playerAPIData.unlockedPlayerPortrait.map(p => p.id);
        if (playerAPIData.portraits) return playerAPIData.portraits.unlocked;
        return null;
    },
    getPlayerSelectedPortrait: function(playerAPIData)
    {
        if (playerAPIData.selectedPlayerPortrait) return playerAPIData.selectedPlayerPortrait['id'];
        if (playerAPIData.portraits) return playerAPIData.portraits.selected;
        return null;
    },
    getPlayerZetaCount: function(playerAPIData)
    {
        if (playerAPIData.overview) return playerAPIData.overview.zetas;
        return null;
    },
    getPlayerOmicrons: function(playerAPIData, omicronMode = null)
    {  
        if (playerAPIData.overview)
        {
            if (omicronMode) return playerAPIData.overview.omicrons[omicronMode];
            return playerAPIData.overview.omicrons;
        }

        return null;
    },
    getPlayerOmicronCount: function(playerAPIData, omicronMode = null)
    {
        if (playerAPIData.overview) {
            if (omicronMode) return playerAPIData.overview.omicrons[omicronMode]?.length ?? 0;
            return playerAPIData.overview.omicrons.reduce((p, c) => p + (c?.length ?? 0), 0);
        }
        return null;
    },
    getPlayerGacOmicronCount: function(playerAPIData)
    {
        if (!playerAPIData.overview) return null;

        return playerAPIData.overview.omicrons[omicronModeEnum.TERRITORY_TOURNAMENT_OMICRON]?.length ?? 0 +
                playerAPIData.overview.omicrons[omicronModeEnum.TERRITORY_TOURNAMENT_3_OMICRON]?.length ?? 0 +
                playerAPIData.overview.omicrons[omicronModeEnum.TERRITORY_TOURNAMENT_5_OMICRON]?.length ?? 0;
    },
    getPlayerModSummary: function(playerAPIData)
    {
        return playerAPIData.overview?.mods;
    },
    getPlayerGearSummary: function(playerAPIData)
    {
        return playerAPIData.overview?.gears;
    },
    getPlayerRelicSummary: function(playerAPIData)
    {
        return playerAPIData.overview?.relics;
    },
    getPlayerCapitalShips: function(playerAPIData)
    {
        return playerAPIData.overview?.capitalShips;
    },
    getPlayerGalacticLegends: function(playerAPIData)
    {
        return playerAPIData.overview?.galacticLegends;
    },
    findUnit: function(playerAPIData, unitDefId, { minRelicLevel, minRarity } = { minRelicLevel: null, minRarity: null })
    {
        let response = { errorCode: null, unit: null };
        let roster = this.getPlayerRoster(playerAPIData);
        if (!roster) { response.errorCode = FIND_UNIT_ERROR_CODES.CORRUPT_PLAYER_DATA; return response; }

        let ru = roster.find(u => this.getUnitDefId(u) === unitDefId);
        if (!ru) { response.errorCode = FIND_UNIT_ERROR_CODES.NOT_UNLOCKED; return response; }

        response.unit = ru;
        // character that's not minRelicLevel can't be used
        if (this.getUnitCombatType(ru) == constants.COMBAT_TYPES.SQUAD)
        {
            if (!isNaN(minRelicLevel) && (this.getUnitRelicLevel(ru) - 2 < minRelicLevel)) { response.errorCode = FIND_UNIT_ERROR_CODES.TOO_LOW_RELIC; return response; }
        }
        
        if (!isNaN(minRarity) && (this.getUnitRarity(ru) < minRarity)) { response.errorCode = FIND_UNIT_ERROR_CODES.TOO_LOW_RARITY; return response; };

        return response;
    },
    getUnitCombatType: function(unitAPIData)
    {
        if (!unitAPIData) return null;
        if (unitAPIData.data) return unitAPIData.data.combat_type;
        if (unitAPIData.combatType) return unitAPIData.combatType;
        if (unitAPIData.combat_type) return unitAPIData.combat_type;

        // comlink
        return unitAPIData.relic == null ? 2 : 1;
    },
    getUnitGearLevel: function(unitAPIData)
    {
        if (!unitAPIData) return null;
        if (unitAPIData.data) return unitAPIData.data.gear_level;
        if (unitAPIData.gear) return unitAPIData.gear;
        return unitAPIData.currentTier;
    },
    getUnitRelicLevel: function(unitAPIData)
    {
        if (!unitAPIData) return null;
        if (unitAPIData.data) return unitAPIData.data.relic_tier;
        return unitAPIData.relic?.currentTier; // .help and comlink
    },
    getUnitRarity: function(unitAPIData)
    {
        if (!unitAPIData) return null;
        if (unitAPIData.currentRarity) return unitAPIData.currentRarity;
        if (unitAPIData.data) return unitAPIData.data.rarity;
        if (unitAPIData.rarity) return unitAPIData.rarity;
    },
    getUnitGP: function(unitAPIData)
    {  
        if (!unitAPIData) return null;
        if (unitAPIData.data) return unitAPIData.data.power;
        return unitAPIData.gp; // .help and comlink via crinolo
    },
    getUnitDefId: function(unitAPIData)
    {
        if (!unitAPIData) return null;
        if (unitAPIData.data) return unitAPIData.data.base_id;
        if (unitAPIData.defId) return unitAPIData.defId;
        if (unitAPIData.baseId) return unitAPIData.baseId;
        if (unitAPIData.base_id) return unitAPIData.base_id;
        if (unitAPIData.definitionId)
        {
            return unitAPIData.definitionId.slice(0, unitAPIData.definitionId.indexOf(":"));
        }
    },
    getUnitAbilities: function(unitAPIData)
    {
        if (unitAPIData.data) return unitAPIData.data.ability_data;
        if (unitAPIData.skills) return unitAPIData.skills;
        return unitAPIData.skill; // comlink
    },
    getUnitZetaAbilities: async function(unitAPIData)
    {
        if (unitAPIData == null) return null;
        
        let allAbilities = await this.getAbilitiesList();

        // internal manipulation makes this
        if (unitAPIData.zeta_abilities)
        {
            return allAbilities.filter(a => unitAPIData.zeta_abilities.indexOf(a.base_id) >= 0);
        }
  
        // .help and comlink. Leverage swgoh.gg ability API for simplicity
        let tierAdd = (unitAPIData.equippedStatMod ? 2 : 0); // comlink shows abilities 2 tiers lower
        let unitZetas = allAbilities.filter(a => a.character_base_id === this.getUnitDefId(unitAPIData) && a.is_zeta == true)
        
        // it's either at max tier or, if it's an omi skill, at max tier - 1
        let zetas = unitZetas.filter(
                        a => this.getUnitAbilities(unitAPIData).find(
                                ua => ua.id === a.base_id &&
                                (ua.tier + tierAdd) >= (a.tier_max - (a.is_omicron ? 1 : 0))
                            )
                        );

        return zetas;
    },
    getUnitZetaCount: async function(unitAPIData)
    {
        if (unitAPIData == null) return 0;
        
        if (unitAPIData.data) return unitAPIData.data.zeta_abilities.length;
  
        // .help and comlink. Leverage swgoh.gg ability API for simplicity
        let tierAdd = (unitAPIData.equippedStatMod ? 2 : 0); // comlink shows abilities 2 tiers lower
        let allAbilities = await this.getAbilitiesList();
        let unitZetas = allAbilities.filter(a => a.character_base_id === this.getUnitDefId(unitAPIData) && a.is_zeta == true)

        let zetas = this.getUnitAbilities(unitAPIData).filter(s => 
            unitZetas.find(zs => zs.base_id === s.id && // it's in the list of skills with a zeta 
            ((s.tier + tierAdd) == zs.tier_max || (zs.is_omicron && (s.tier + tierAdd) >= zs.tier_max-1)))); // it's either at max tier or, if it's an omi skill, at max tier - 1

        return zetas.length;
    },
    getUnitOmicronAbilities: async function(unitAPIData)
    {
        if (unitAPIData == null) return null;

        let allAbilities = await this.getOmicronAbilitiesList();
        
        if (unitAPIData.data) {
            return allAbilities.filter(a => unitAPIData.data.omicron_abilities.indexOf(a.base_id) >= 0);
        }

        // internal manipulation makes this
        if (unitAPIData.omicron_abilities)
        {
            return allAbilities.filter(a => unitAPIData.omicron_abilities.indexOf(a.base_id) >= 0);
        }

        // .help and comlink. Leverage swgoh.gg ability API for simplicity
        let tierAdd = (unitAPIData.equippedStatMod ? 2 : 0); // comlink shows abilities 2 tiers lower
        let unitOmis = allAbilities.filter(a => a.character_base_id === this.getUnitDefId(unitAPIData));
        let omis = unitOmis.filter(a => this.getUnitAbilities(unitAPIData).find(ua => ua.id === a.base_id && (ua.tier + tierAdd) === a.tier_max));
        // let omis = this.getUnitAbilities(unitAPIData).filter(s => unitOmis.find(os => os.base_id === s.id && (s.tier + tierAdd) === os.tier_max));

        return omis;
    },
    getUnitOmicronCount: async function(unitAPIData)
    {
        if (unitAPIData == null) return 0;
        
        if (unitAPIData.data) return unitAPIData.data.omicron_abilities.length;
        if (unitAPIData.omicron_abilities) return unitAPIData.omicron_abilities.length;
  
        // .help and comlink. Leverage swgoh.gg ability API for simplicity
        let tierAdd = (unitAPIData.equippedStatMod ? 2 : 0); // comlink shows abilities 2 tiers lower
        let allAbilities = await this.getOmicronAbilitiesList();
        let unitOmis = allAbilities.filter(a => a.character_base_id === this.getUnitDefId(unitAPIData));
        let omis = unitOmis.filter(a => this.getUnitAbilities(unitAPIData).find(ua => ua.id === a.base_id && (ua.tier + tierAdd) === a.tier_max));

        return omis.length;
    },
    getUnitsWithUltimate: function(playerAPIData)
    {
        if (!playerAPIData) return null;
        if (playerAPIData.units)
            return this.getPlayerRoster(playerAPIData).filter(u => u.data.has_ultimate); // .gg
        else if (playerAPIData.rosterUnitList)
            return this.getPlayerRoster(playerAPIData).filter(u => u.purchasedAbilityId.length > 0); // shittybots
        else if (playerAPIData.rosterUnit)
            return this.getPlayerRoster(playerAPIData).filter(u => u.purchasedAbilityId.length > 0); // comlink
        return null;
    },
    getUnitStat: function(unitAPIData, stat)
    {
        if (!unitAPIData) return null;
        
        if (unitAPIData.data)
        {
            // swgoh.gg data
            return unitAPIData.data.stats[stat];
        }

        if (!unitAPIData.stats || !unitAPIData.stats.final) return null;

        return unitAPIData.stats.final[stat];
    },
    getManipulatedUnitStat: function(unitAPIData, stat)
    {
        let rawValue = this.getUnitStat(unitAPIData, stat);

        if (unitAPIData.data)
        {
            return swgohGGStatManipulations[stat](rawValue);
        }

        return swgohHelpStatManipulations[stat](rawValue);
    },
    getPlayerDatacronsByCharacter: async function(playerAPIData, character)
    {
        let dcData = await datacronUtils.getActiveWookieeBotDatacronData();

        let origDcs = dcData.filter(origDc => origDc.character === character);

        return playerAPIData.datacron.filter(dc => 
            origDcs.find(odc => odc.set_id == dc.setId && (odc.ability_id === dc.abilityId || odc.duplicate_ability_ids?.indexOf(dc.abilityId) >= 0))
            )
    },
    getPlayerDatacronsByFaction: async function(playerAPIData, faction)
    {
        let dcData = await datacronUtils.getActiveWookieeBotDatacronData();

        let origDcs = dcData.filter(origDc => origDc.faction === faction);

        return playerAPIData.datacron.filter(dc => 
            origDcs.find(odc => odc.set_id == dc.setId && (odc.ability_id === dc.abilityId || odc.duplicate_ability_ids?.indexOf(dc.abilityId) >= 0))
            )
    },
    getPlayerDatacronsByAlignment: async function(playerAPIData, alignment)
    {
        let dcData = await datacronUtils.getActiveWookieeBotDatacronData();

        let origDcs = dcData.filter(origDc => origDc.alignment === alignment);

        return playerAPIData.datacron.filter(dc => 
            origDcs.find(odc => odc.set_id == dc.setId && (odc.ability_id === dc.abilityId || odc.duplicate_ability_ids?.indexOf(dc.abilityId) >= 0))
            )
    },
    getPlayerDatacronByWookieeBotDatacronId: function(playerAPIData, datacronId)
    {
        return playerAPIData.datacron.filter(dc => dc.affix && dc.affix.find(af => af.wookieeBotDatacronId == datacronId));
    },
    getPlayerDatacronWithAllWookieeBotDatacronIds: function (playerAPIData, datacronIds)
    {
        // no datacrons specified, so match all
        if (!datacronIds || datacronIds.length === 0) return this.getPlayerDatacrons(playerAPIData);

        // find all datacrons that have *all* of the specified datacronIds in its affixes
        
        let specifiedDatacronIdsOnly = datacronIds.filter(r => r != constants.NO_DATACRON_SPECIFIED);

        return this.getPlayerDatacrons(playerAPIData).filter(dc => 
            {
                for (let datacronId of specifiedDatacronIdsOnly)
                {
                    if (dc.affix?.find(af => af.wookieeBotDatacronId === datacronId) == null) return false;
                }
                return true;
            });
    },
    getPlayerDatacrons: function(playerAPIData)
    {
        return playerAPIData.datacron;
    },
    getGuildRoster: function(guildAPIData)
    {
        return guildAPIData?.member ?? guildAPIData?.roster ?? guildAPIData?.data?.members;
    },
    getGuildName: function(guildAPIData)
    {
        return guildAPIData?.profile?.name ?? guildAPIData.name;
    },
    getGuildId: function(guildAPIData)
    {
        return guildAPIData?.profile?.id ?? guildAPIData?.id;
    },
    getGuildDescription: function(guildAPIData)
    {
        return (guildAPIData?.profile?.externalMessageKey ?? guildAPIData?.desc) ?? guildAPIData?.data.external_message
    }
}

const swgohGGStatManipulations = {
    "0": (stat) => { return stat; },
    "1": (stat) => { return stat; },
    "2": (stat) => { return stat; },
    "3": (stat) => { return stat; },
    "4": (stat) => { return stat; },
    "5": (stat) => { return stat; },
    "6": (stat) => { return stat; },
    "7": (stat) => { return stat; },
    "8": (stat) => { return stat; },
    "9": (stat) => { return stat; },
    "10": (stat) => { return stat; },
    "11": (stat) => { return stat; },
    "12": (stat) => { return stat; },
    "13": (stat) => { return stat; },
    "14": (stat) => { return stat; },
    "15": (stat) => { return stat; },
    "16": (stat) => { return stat*100; },
    "17": (stat) => { return stat*100; },
    "18": (stat) => { return stat*100; },
    "19": (stat) => { return stat; },
    "20": (stat) => { return stat; },
    "21": (stat) => { return stat; },
    "22": (stat) => { return stat; },
    "23": (stat) => { return stat; },
    "24": (stat) => { return stat; },
    "25": (stat) => { return stat; },
    "26": (stat) => { return stat; },
    "27": (stat) => { return stat*100; },
    "28": (stat) => { return stat; },
    "29": (stat) => { return stat; },
    "30": (stat) => { return stat; },
    "31": (stat) => { return stat; },
    "32": (stat) => { return stat; },
    "33": (stat) => { return stat; },
    "34": (stat) => { return stat; },
    "35": (stat) => { return stat; },
    "36": (stat) => { return stat; },
    "37": (stat) => { return stat; },
    "38": (stat) => { return stat; },
    "39": (stat) => { return stat; },
    "40": (stat) => { return stat; },
    "41": (stat) => { return stat; },
    "42": (stat) => { return stat; },
    "43": (stat) => { return stat; },
    "44": (stat) => { return stat; },
    "45": (stat) => { return stat; },
    "46": (stat) => { return stat; },
    "47": (stat) => { return stat; },
    "48": (stat) => { return stat; },
    "49": (stat) => { return stat; },
    "50": (stat) => { return stat; },
    "51": (stat) => { return stat; },
    "52": (stat) => { return stat; },
    "53": (stat) => { return stat; },
    "54": (stat) => { return stat; },
    "55": (stat) => { return stat; },
    "56": (stat) => { return stat; },
    "57": (stat) => { return stat; },
    "58": (stat) => { return stat; },
    "59": (stat) => { return stat; },
    "61": (stat) => { return stat; }
}

const swgohHelpStatManipulations = {
    "0": (stat) => { return stat; },
    "1": (stat) => { return stat; },
    "2": (stat) => { return stat; },
    "3": (stat) => { return stat; },
    "4": (stat) => { return stat; },
    "5": (stat) => { return stat; },
    "6": (stat) => { return stat; },
    "7": (stat) => { return stat; },
    "8": (stat) => { return stat*100; },
    "9": (stat) => { return stat*100; },
    "10": (stat) => { return stat; },
    "11": (stat) => { return stat; },
    "12": (stat) => { return stat; },
    "13": (stat) => { return stat; },
    "14": (stat) => { return stat*100; },
    "15": (stat) => { return stat; },
    "16": (stat) => { return stat*100; },
    "17": (stat) => { return stat*100; },
    "18": (stat) => { return stat*100; },
    "19": (stat) => { return stat; },
    "20": (stat) => { return stat; },
    "21": (stat) => { return stat; },
    "22": (stat) => { return stat; },
    "23": (stat) => { return stat; },
    "24": (stat) => { return stat*100; },
    "25": (stat) => { return stat; },
    "26": (stat) => { return stat; },
    "27": (stat) => { return stat*100; },
    "28": (stat) => { return stat; },
    "29": (stat) => { return stat; },
    "30": (stat) => { return stat; },
    "31": (stat) => { return stat; },
    "32": (stat) => { return stat; },
    "33": (stat) => { return stat*100; },
    "34": (stat) => { return stat; },
    "35": (stat) => { return stat*100; },
    "36": (stat) => { return stat; },
    "37": (stat) => { return stat*100; },
    "38": (stat) => { return stat; },
    "39": (stat) => { return stat; },
    "40": (stat) => { return stat; },
    "41": (stat) => { return stat; },
    "42": (stat) => { return stat; },
    "43": (stat) => { return stat; },
    "44": (stat) => { return stat; },
    "45": (stat) => { return stat; },
    "46": (stat) => { return stat; },
    "47": (stat) => { return stat; },
    "48": (stat) => { return stat; },
    "49": (stat) => { return stat; },
    "50": (stat) => { return stat; },
    "51": (stat) => { return stat; },
    "52": (stat) => { return stat; },
    "53": (stat) => { return stat; },
    "54": (stat) => { return stat; },
    "55": (stat) => { return stat; },
    "56": (stat) => { return stat; },
    "57": (stat) => { return stat; },
    "58": (stat) => { return stat; },
    "59": (stat) => { return stat; },
    "61": (stat) => { return stat; }
}