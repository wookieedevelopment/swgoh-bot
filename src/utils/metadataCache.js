
const NodeCache = require('node-cache');

const metadataCache = new NodeCache({useClones: false});

const metadataCacheKeys = {
    unitsList: "unitsList",
    gameStatsData: "gameStatsData",
    abilitiesList: "abilitiesList",
    gamedataVersion: "gamedataVersion",
    datacrons: "datacrons",
    comlinkUnitsList: "comlinkUnitsList",
    wookieeBotActiveDatacronData: "wookiee_active_datacrons",
    wookieeBotDatacronData: "wookiee_datacrons",
    gacRiskData: "gacRiskData",
    localization: "localization",
    categoryData: "categories",
    twScoutQueries: "twScoutQueries",
    operationRequirements: "operationRequirements",
    unitPortraits: "unitPortraits",
    playerPortrait: "playerPortrait",
    tbCombatTeamRecommendations: "tbCombatTeams",
    tbMissions: "tbMissions",
    raidTeams: "raidTeams",
    abilityIcons: "abilityIcons",
    omicronsList: "omicronsList",
    operationGroupingRules: "opGroupRules",
    gunganData: "gungan",
    modSets: "modSets",
    unitNicknames: "unitNicknames",
    allycodeNameMap: "allycodeNameMap",
    materialsData: "materialsData"
}

module.exports = {
    CACHE_KEYS: metadataCacheKeys,
    get: function(key)
    {
        return metadataCache.get(key);
    },
    set: function(key, value, ttl)
    {
        metadataCache.set(key, value, ttl)
    }
}