const swapiUtils = require("./swapi");
const constants = require("./constants");
const unitsUtils = require("./units");
const raidCommon = require("../commands/raid/raidCommon");
const { EmbedBuilder, AttachmentBuilder } = require("discord.js");
const { createCanvas, loadImage } = require("canvas");
const Gradient = require("javascript-color-gradient");
const { parseAsync } = require('json2csv');

const readinessGradient = new Gradient().setColorGradient("#330000", "#555500", "#003300").setMidpoint(20);

const UNIT_REQUIREMENT_TYPE = {
    REQUIRED: { value: 1, text: "Req", color: "#df2312" },
    OPTIONAL: { value: 2, text: "Opt", color: "#aaaa00" },
    NICE_TO_HAVE: { value: 3, text: "Nice", color: "#aa00aa" }
}

const WEIGHTS = {
    GEAR_AND_RARITY: {
        rarity: 50,
        gear: 50,
        relic: 0
    },
    GEAR_RARITY_AND_RELIC: {
        rarity: 10,
        gear: 10,
        relic: 80
    }
}

const TB_CHECK_FUNCTION_MAPPING = {
    Reva: { 
        value: "Reva", 
        desc: "Reva - (MX 3) Tatooine SM",
        reqs: {
            minRelicLevel: 7,
            units: [
                { defId: "GRANDINQUISITOR", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "FIFTHBROTHER", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "SEVENTHSISTER", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "NINTHSISTER", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "EIGHTHBROTHER", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "SECONDSISTER", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "THIRDSISTER", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "MARROK", req: UNIT_REQUIREMENT_TYPE.OPTIONAL }
            ],
            teamSize: 5
        }
    },
    LS2: {
        value: "LS2", 
        desc: "(LS 2) Bracca SM (Cere, Any Cal)",
        reqs: {
            minRelicLevel: 7,
            units: [
                { defId: "CEREJUNDA", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "JEDIKNIGHTCAL", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "CALKESTIS", req: UNIT_REQUIREMENT_TYPE.OPTIONAL }
            ],
            teamSize: 2
        }
    },
    LS3: {
        value: "LS3", 
        desc: "(LS 3) Kashyyyk SM (Saw, Rebel Fighters)",
        reqs: {
            minRelicLevel: 7,
            units: [
                { defId: "SAWGERRERA", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "CAPTAINDROGAN", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "LUTHENRAEL", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "CARADUNE", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "KYLEKATARN", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "CAPTAINREX", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "BAZEMALBUS", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "CASSIANANDOR", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "SCARIFREBEL", req: UNIT_REQUIREMENT_TYPE.OPTIONAL }
            ],
            teamSize: 5
        }
    },
    ZEFFO: {
        value: "ZEFFO", 
        desc: "(Zeffo) Zeffo SM (Clone Troopers)",
        reqs: {
            minRelicLevel: 7,
            units: [
                { defId: "CT7567", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "CT5555", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "ARCTROOPER501ST", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "CT210408", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "CAPTAINREX", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "BADBATCHECHO", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "BADBATCHWRECKER", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "BADBATCHHUNTER", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "BADBATCHTECH", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "BADBATCHOMEGA", req: UNIT_REQUIREMENT_TYPE.OPTIONAL, omi:true }
            ],
            teamSize: 5
        }
    },
    TATOOINEKRAYT: {
        value: "TATOOINEKRAYT", 
        desc: "(Tatooine Krayt) Unlock Mandalore SM (BKM, BAM)",
        reqs: {
            minRelicLevel: 7,
            units: [
                { defId: "MANDALORBOKATAN", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "THEMANDALORIANBESKARARMOR", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "IG12", req: UNIT_REQUIREMENT_TYPE.OPTIONAL }
            ],
            teamSize: 3
        }
    },
    MX1: { 
        value: "MX1", 
        desc: "(MX 1) Corellia SM (Qi'ra, Yolo)",
        reqs: {
            minRelicLevel: 5,
            units: [
                { defId: "QIRA", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "YOUNGHAN", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "GLREY", req: UNIT_REQUIREMENT_TYPE.NICE_TO_HAVE }
            ],
            teamSize: 5
        }
    },
    MX4: { 
        value: "MX4", 
        desc: "(MX 4) Kessel SM (Qi'ra, L3-37)",
        reqs: {
            minRelicLevel: 8,
            units: [
                { defId: "QIRA", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "L3_37", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "GLREY", req: UNIT_REQUIREMENT_TYPE.NICE_TO_HAVE }
            ],
            teamSize: 5
        }
    },
    MX5: { 
        value: "MX5", 
        desc: "(MX 5) Vandor SM (Yolo, Vandor)",
        reqs: {
            minRelicLevel: 9,
            units: [
                { defId: "YOUNGHAN", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "YOUNGCHEWBACCA", req: UNIT_REQUIREMENT_TYPE.REQUIRED }
            ],
            teamSize: 5
        }
    },
    DS3: { 
        value: "DS3", 
        desc: "(DS 3) Dathomir SM (Merrin, Nightsisters)",
        reqs: {
            minRelicLevel: 7,
            units: [
                { defId: "MERRIN", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "GREATMOTHERS", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "MORGANELSBETH", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "MOTHERTALZIN", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "DAKA", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "NIGHTSISTERZOMBIE", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "ASAJVENTRESS", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "NIGHTSISTERSPIRIT", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "NIGHTTROOPER", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "DEATHTROOPERPERIDEA", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
            ],
            teamSize: 5
        }
    },
    DS4: { 
        value: "DS4", 
        desc: "(DS 4) Haven SM (Reva, Inqs)",
        reqs: {
            minRelicLevel: 8,
            units: [
                { defId: "THIRDSISTER", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "GRANDINQUISITOR", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "FIFTHBROTHER", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "SEVENTHSISTER", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "NINTHSISTER", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "EIGHTHBROTHER", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "SECONDSISTER", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "MARROK", req: UNIT_REQUIREMENT_TYPE.OPTIONAL }
            ],
            teamSize: 5
        }
    },
    GI: { 
        value: "GI", 
        desc: "Grand Inquisitor Unlock",
        reqs: {
            minRelicLevel: 5,
            units: [
                { defId: "FIFTHBROTHER", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "SEVENTHSISTER", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "NINTHSISTER", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "EIGHTHBROTHER", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "SECONDSISTER", req: UNIT_REQUIREMENT_TYPE.REQUIRED }
            ],
            teamSize: 5
        }
    },
    KAM501: { 
        value: "KAM501", 
        desc: "(LS GTB) KAM - 501st",
        reqs: {
            minRelicLevel: 5,
            units: [
                { defId: "CT7567", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "CT5555", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "CT210408", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "CAPTAINREX", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "SHAAKTI", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "ARCTROOPER501ST", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
            ],
            teamSize: 5
        }
    },
    KAMBB: { 
        value: "KAMBB", 
        desc: "(LS GTB) KAM - Bad Batch",
        reqs: {
            minRelicLevel: 5,
            units: [
                { defId: "BADBATCHHUNTER", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "BADBATCHOMEGA", req: UNIT_REQUIREMENT_TYPE.REQUIRED, omi: true },
                { defId: "BADBATCHTECH", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "BADBATCHECHO", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "BADBATCHWRECKER", req: UNIT_REQUIREMENT_TYPE.REQUIRED }
            ],
            teamSize: 5
        }
    },
    WAT: { 
        value: "WAT", 
        desc: "(DS GTB) Wat - Geos",
        reqs: {
            minGearLevel: 12,
            units: [
                { defId: "GEONOSIANBROODALPHA", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "POGGLETHELESSER", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "GEONOSIANSPY", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "GEONOSIANSOLDIER", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "SUNFAC", req: UNIT_REQUIREMENT_TYPE.REQUIRED }
            ],
            teamSize: 5
        }
    },
};

const RAID_CHECK_FUNCTION_MAPPING = {
    ALLNABOO: {
        value: "ALLNABOO",
        desc: "(Naboo) All Units",
        raid: raidCommon.NABOO,
        reqs: {
            units: [
                // Gungans
                { defId: "BOSSNASS", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "CAPTAINTARPALS", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "BOOMADIER", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "GUNGANPHALANX", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "JARJARBINKS", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                // GR
                { defId: "QUEENAMIDALA", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "PADAWANOBIWAN", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "MASTERQUIGON", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "R2D2_LEGENDARY", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "MACEWINDU", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "JEDIKNIGHTCONSULAR", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "JEDIKNIGHTGUARDIAN", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "KELLERANBEQ", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "EETHKOTH", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "GRANDMASTERYODA", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "PLOKOON", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "KIADIMUNDI", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "AAYLASECURA", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "SHAAKTI", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "LUMINARAUNDULI", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "KITFISTO", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "QUIGONJINN", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                // seps
                { defId: "NUTEGUNRAY", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "B1BATTLEDROIDV2", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "STAP", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "DROIDEKA", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "B2SUPERBATTLEDROID", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "MAGNAGUARD", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                // sith
                { defId: "MAUL", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "DARTHSIDIOUS", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
            ],
            teamSize: 30
        }
    },
    NABOOGUNGANS: {
        value: "NABOOGUNGANS",
        desc: "(Naboo Team) Gungans",
        raid: raidCommon.NABOO,
        reqs: {
            units: [
                // Gungans
                { defId: "BOSSNASS", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "CAPTAINTARPALS", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "BOOMADIER", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "GUNGANPHALANX", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "JARJARBINKS", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
            ],
            teamSize: 5
        }
    },
    NABOOGR: {
        value: "NABOOGR",
        desc: "(Naboo Faction) All Galactic Republic",
        raid: raidCommon.NABOO,
        reqs: {
            units: [
                // GR
                { defId: "QUEENAMIDALA", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "PADAWANOBIWAN", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "MASTERQUIGON", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "R2D2_LEGENDARY", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "MACEWINDU", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "JEDIKNIGHTCONSULAR", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "JEDIKNIGHTGUARDIAN", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "KELLERANBEQ", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "EETHKOTH", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "GRANDMASTERYODA", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "PLOKOON", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "KIADIMUNDI", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "AAYLASECURA", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "SHAAKTI", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "LUMINARAUNDULI", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "KITFISTO", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "QUIGONJINN", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
            ],
            teamSize: 17
        }
    },
    NABOOQA: {
        value: "NABOOQA",
        desc: "(Naboo Team) Queen Amidala GR",
        raid: raidCommon.NABOO,
        reqs: {
            units: [
                // GR
                { defId: "QUEENAMIDALA", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "PADAWANOBIWAN", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "MASTERQUIGON", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "R2D2_LEGENDARY", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "MACEWINDU", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "JEDIKNIGHTCONSULAR", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "JEDIKNIGHTGUARDIAN", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "EETHKOTH", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "GRANDMASTERYODA", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "PLOKOON", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "KIADIMUNDI", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "AAYLASECURA", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "SHAAKTI", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "KITFISTO", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "QUIGONJINN", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
            ],
            teamSize: 3
        }
    },
    NABOOLU: {
        value: "NABOOLU",
        desc: "(Naboo Team) Luminara Jedi",
        raid: raidCommon.NABOO,
        reqs: {
            units: [
                // GR
                { defId: "LUMINARAUNDULI", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "KITFISTO", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "MACEWINDU", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "PLOKOON", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "QUIGONJINN", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "GRANDMASTERYODA", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "AAYLASECURA", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "SHAAKTI", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "KIADIMUNDI", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "EETHKOTH", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "JEDIKNIGHTCONSULAR", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "JEDIKNIGHTGUARDIAN", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
            ],
            teamSize: 5
        }
    },
    NABOOKB: {
        value: "NABOOKB",
        desc: "(Naboo Team) Kelleran Beq Jedi",
        raid: raidCommon.NABOO,
        reqs: {
            units: [
                // GR
                { defId: "KELLERANBEQ", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "KIADIMUNDI", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "SHAAKTI", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "MACEWINDU", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "AAYLASECURA", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "JEDIKNIGHTCONSULAR", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "JEDIKNIGHTGUARDIAN", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "EETHKOTH", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "GRANDMASTERYODA", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "PLOKOON", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "QUIGONJINN", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
            ],
            teamSize: 5
        }
    },
    NABOOMAUL: {
        value: "NABOOMAUL",
        desc: "(Naboo Team) Maul Lead",
        raid: raidCommon.NABOO,
        reqs: {
            units: [
                // sith
                { defId: "MAUL", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "DARTHSIDIOUS", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                // seps
                { defId: "NUTEGUNRAY", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "STAP", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
            ],
            teamSize: 3
        }
    },
    NABOOB2: {
        value: "NABOOB2",
        desc: "(Naboo Team) B2 Lead Sith",
        raid: raidCommon.NABOO,
        reqs: {
            units: [
                { defId: "B2SUPERBATTLEDROID", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "MAUL", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "DARTHSIDIOUS", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                // seps
                { defId: "NUTEGUNRAY", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "STAP", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "DROIDEKA", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "B1BATTLEDROIDV2", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "MAGNAGUARD", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
            ],
            teamSize: 5
        }
    },
    NABOOSEPSITH: {
        value: "NABOOSEPSITH",
        desc: "(Naboo Faction) All Separatists & Sith",
        raid: raidCommon.NABOO,
        reqs: {
            units: [
                // sith
                { defId: "MAUL", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "DARTHSIDIOUS", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                // seps
                { defId: "NUTEGUNRAY", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "B1BATTLEDROIDV2", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "STAP", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "DROIDEKA", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "B2SUPERBATTLEDROID", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "MAGNAGUARD", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
            ],
            teamSize: 8
        }
    },
    ALLENDOR: {
        value: "ALLENDOR",
        desc: "(Endor) All Units",
        raid: raidCommon.ENDOR,
        reqs: {
            units: [
                { defId: "ADMIRALACKBAR", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "C3POLEGENDARY", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "CHEWBACCALEGENDARY", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "HANSOLO", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "HERASYNDULLAS3", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "ADMINISTRATORLANDO", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "WEDGEANTILLES", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "JEDIKNIGHTLUKE", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "CHIEFCHIRPA", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "EWOKELDER", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "EWOKSCOUT", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "LOGRAY", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "PAPLOO", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "TEEBO", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "WICKET", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "PRINCESSKNEESAA", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "ADMIRALPIETT", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "STORMTROOPER", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "IDENVERSIOEMPIRE", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "MOFFGIDEONS1", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "RANGETROOPER", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "MAGMATROOPER", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "DEATHTROOPER", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "SHORETROOPER", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "VEERS", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "COLONELSTARCK", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "SCOUTTROOPER_V3", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "CAPTAINREX", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "CAPTAINDROGAN", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "GLLEIA", req: UNIT_REQUIREMENT_TYPE.REQUIRED }
            ],
            teamSize: 30
        }
    },
    ENDOR_REBELS: {
        value: "ENDOR_REBELS",
        desc: "(Endor) Rebels",
        raid: raidCommon.ENDOR,
        reqs: {
            units: [
                { defId: "ADMIRALACKBAR", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "C3POLEGENDARY", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "CAPTAINDROGAN", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "CAPTAINREX", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "CHEWBACCALEGENDARY", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "HANSOLO", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "HERASYNDULLAS3", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "JEDIKNIGHTLUKE", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "ADMINISTRATORLANDO", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "GLLEIA", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "WEDGEANTILLES", req: UNIT_REQUIREMENT_TYPE.REQUIRED }
            ],
            teamSize: 11
        }
    },
    ENDOR_IT: {
        value: "ENDOR_IT",
        desc: "(Endor) Imperial Troopers",
        raid: raidCommon.ENDOR,
        reqs: {
            units: [
                { defId: "ADMIRALPIETT", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "COLONELSTARCK", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "DEATHTROOPER", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "VEERS", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "IDENVERSIOEMPIRE", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "MAGMATROOPER", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "MOFFGIDEONS1", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "RANGETROOPER", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "SCOUTTROOPER_V3", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "SHORETROOPER", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "STORMTROOPER", req: UNIT_REQUIREMENT_TYPE.REQUIRED }
            ],
            teamSize: 11
        }
    },
    ENDOR_EWOKS: {
        value: "ENDOR_EWOKS",
        desc: "(Endor) Ewoks",
        raid: raidCommon.ENDOR,
        reqs: {
            units: [
                { defId: "CHIEFCHIRPA", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "EWOKELDER", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "EWOKSCOUT", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "LOGRAY", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "PAPLOO", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "PRINCESSKNEESAA", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "TEEBO", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "WICKET", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
            ],
            teamSize: 8
        }
    },
    HC: { 
        value: "HC", 
        desc: "(Krayt Team) Jabba Hutt Cartel",
        raid: raidCommon.KRAYT,
        reqs: {
            units: [
                { defId: "JABBATHEHUTT", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "BOUSHH", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "KRRSANTAN", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "UNDERCOVERLANDO", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "BOBAFETT", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "CADBANE", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "GREEDO", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "EMBO", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "GAMORREANGUARD", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "HUMANTHUG", req: UNIT_REQUIREMENT_TYPE.OPTIONAL }
            ],
            teamSize: 5
        }
    },
    Tuskens: {
        value: "Tuskens", 
        desc: "(Krayt Team) Tuskens",
        raid: raidCommon.KRAYT,
        reqs: {
            units: [
                { defId: "TUSKENCHIEFTAIN", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "TUSKENHUNTRESS", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "TUSKENRAIDER", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "URORRURRR", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "TUSKENSHAMAN", req: UNIT_REQUIREMENT_TYPE.REQUIRED }
            ],
            teamSize: 5
        }
    },
    Maul: {
        value: "Maul", 
        desc: "(Krayt Team) Maul Mandos",
        raid: raidCommon.KRAYT,
        reqs: {
            units: [
                { defId: "MAULS7", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "THEMANDALORIANBESKARARMOR", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "CANDEROUSORDO", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "BOKATAN", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "ARMORER", req: UNIT_REQUIREMENT_TYPE.REQUIRED }
            ],
            teamSize: 5
        }
    },
    Jawas: {
        value: "Jawas", 
        desc: "(Krayt Team) Jawas",
        raid: raidCommon.KRAYT,
        reqs: {
            units: [
                { defId: "CHIEFNEBIT", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "DATHCHA", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "JAWASCAVENGER", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "JAWAENGINEER", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "JAWA", req: UNIT_REQUIREMENT_TYPE.REQUIRED }
            ],
            teamSize: 5
        }
    },
    JKROR: {
        value: "JKROR",
        desc: "(Krayt Team) JKR Old Republic",
        raid: raidCommon.KRAYT,
        reqs: {
            units: [
                { defId: "JEDIKNIGHTREVAN", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "BASTILASHAN", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "JOLEEBINDO", req: UNIT_REQUIREMENT_TYPE.REQUIRED },
                { defId: "ZAALBAR", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "MISSIONVAO", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "JUHANI", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "50RT", req: UNIT_REQUIREMENT_TYPE.OPTIONAL }
            ],
            teamSize: 5
        }
    },
    Mandalorians: {
        value: "Mandalorians", 
        desc: "(Krayt Faction) Mandalorians",
        raid: raidCommon.KRAYT,
        reqs: {
            units: [
                { defId: "MAULS7", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "JANGOFETT", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "CANDEROUSORDO", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "THEMANDALORIANBESKARARMOR", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "ARMORER", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "BOKATAN", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "GARSAXON", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "IMPERIALSUPERCOMMANDO", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "THEMANDALORIAN", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "SABINEWRENS3", req: UNIT_REQUIREMENT_TYPE.OPTIONAL }
            ],
            teamSize: 5
        }
    },
    OR: {
        value: "OR", 
        desc: "(Krayt Faction) Old Republic",
        raid: raidCommon.KRAYT,
        reqs: {
            units: [
                { defId: "JEDIKNIGHTREVAN", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "BASTILASHAN", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "JOLEEBINDO", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "JUHANI", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "CARTHONASI", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "ZAALBAR", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "MISSIONVAO", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "CANDEROUSORDO", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "T3_M4", req: UNIT_REQUIREMENT_TYPE.OPTIONAL },
                { defId: "50RT", req: UNIT_REQUIREMENT_TYPE.OPTIONAL }
            ],
            teamSize: 5
        }
    },
};

const GL_CHECK_FUNCTION_MAPPING = {
    ALL: {
        value: "ALL",
        desc: "All Galactic Legends",
        reqs: {
            units: [
                { defId: "GLAHSOKATANO", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRarity: 7, minGearLevel: 1 },
                { defId: "GLLEIA", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRarity: 7, minGearLevel: 1 },
                { defId: "JABBATHEHUTT", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRarity: 7, minGearLevel: 1 },
                { defId: "LORDVADER", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRarity: 7, minGearLevel: 1 },
                { defId: "JEDIMASTERKENOBI", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRarity: 7, minGearLevel: 1 },
                { defId: "GRANDMASTERLUKE", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRarity: 7, minGearLevel: 1 },
                { defId: "SITHPALPATINE", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRarity: 7, minGearLevel: 1 },
                { defId: "GLREY", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRarity: 7, minGearLevel: 1 },
                { defId: "SUPREMELEADERKYLOREN", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRarity: 7, minGearLevel: 1 },
            ],
            teamSize: 9
        }
    },
    AHSOKA: {
        value: "AHSOKA",
        desc: "Galactic Legend Ahsoka Tano",
        defId: "GLAHSOKATANO",
        reqs: {
            units: [
                { defId: "AHSOKATANO", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 9 },
                { defId: "GENERALSKYWALKER", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 8 },
                { defId: "ASAJVENTRESS", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 5 },
                { defId: "NIGHTTROOPER", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 7 },
                { defId: "COMMANDERAHSOKA", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 9 },
                { defId: "CAPTAINENOCH", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 7 },
                { defId: "DEATHTROOPERPERIDEA", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 7 },
                { defId: "EZRABRIDGERS3", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 5 },
                { defId: "FULCRUMAHSOKA", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 9 },
                { defId: "CT7567", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 7 },
                { defId: "BARRISSOFFEE", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 5 },
                { defId: "PADAWANSABINE", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 7 },
                { defId: "GENERALSYNDULLA", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 5 },
                { defId: "PLOKOON", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 8 },
                { defId: "HUYANG", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 7 },
                { defId: "JEDISTARFIGHTERAHSOKATANO", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRarity: 7 }
            ],
            teamSize: 16
        }
    },
    LEIA: {
        value: "LEIA",
        desc: "Leia Organa",
        defId: "GLLEIA",
        reqs: {
            units: [
                { defId: "CAPTAINREX", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 7 },
                { defId: "PRINCESSKNEESAA", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 7 },
                { defId: "WICKET", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 3 },
                { defId: "ADMINISTRATORLANDO", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 5 },
                { defId: "ADMIRALACKBAR", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 3 },
                { defId: "SCOUTTROOPER_V3", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 7 },
                { defId: "R2D2_LEGENDARY", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 8 },
                { defId: "HOTHHAN", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 5 },
                { defId: "HOTHLEIA", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 5 },
                { defId: "CHIEFCHIRPA", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 3 },
                { defId: "CAPTAINDROGAN", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 7 },
                { defId: "COMMANDERLUKESKYWALKER", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 7 },
                { defId: "BOUSHH", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 5 },
                { defId: "C3POCHEWBACCA", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 7 },
                { defId: "LOBOT", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 3 },
            ],
            teamSize: 15
        }
    },
    JTH: { 
        value: "JTH", 
        desc: "Jabba the Hutt",
        defId: "JABBATHEHUTT",
        reqs: {
            units: [
                { defId: "KRRSANTAN", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 5 },
                { defId: "HANSOLO", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 8 },
                { defId: "GAMORREANGUARD", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 3 },
                { defId: "OUTRIDER", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRarity: 7 },
                { defId: "GREEDO", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 6 },
                { defId: "UNDERCOVERLANDO", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 5 },
                { defId: "JEDIKNIGHTLUKE", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 7 },
                { defId: "JAWA", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 3 },
                { defId: "URORRURRR", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 4 },
                { defId: "C3POLEGENDARY", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 7 },
                { defId: "BOUSHH", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 5 },
                { defId: "AURRA_SING", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 6 },
                { defId: "FENNECSHAND", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 7 },
                { defId: "BOBAFETT", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 7 },
                { defId: "HUMANTHUG", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 3 }
            ],
            teamSize: 15
        }
    },
    JMK: { 
        value: "JMK", 
        desc: "Jedi Master Kenobi",
        defId: "JEDIMASTERKENOBI",
        reqs: {
            units: [
                { defId: "GENERALKENOBI", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 8 },
                { defId: "CAPITALNEGOTIATOR", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRarity: 6 },
                { defId: "MACEWINDU", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 3 },
                { defId: "AAYLASECURA", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 3 },
                { defId: "BOKATAN", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 5 },
                { defId: "QUIGONJINN", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 3 },
                { defId: "MAGNAGUARD", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 5 },
                { defId: "CLONESERGEANTPHASEI", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 5 },
                { defId: "WATTAMBOR", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 7 },
                { defId: "GRIEVOUS", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 7 },
                { defId: "CADBANE", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 5 },
                { defId: "CC2224", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 5 },
                { defId: "JANGOFETT", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 7 },
                { defId: "SHAAKTI", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 7 },
                { defId: "GRANDMASTERYODA", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 8 }
            ],
            teamSize: 15
        }
    },
    JML: { 
        value: "JML", 
        desc: "Jedi Master Luke Skywalker",
        defId: "GRANDMASTERLUKE",
        reqs: {
            units: [
                { defId: "OLDBENKENOBI", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 5 },
                { defId: "REYJEDITRAINING", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 7 },
                { defId: "C3POLEGENDARY", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 5 },
                { defId: "MONMOTHMA", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 5 },
                { defId: "C3POCHEWBACCA", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 5 },
                { defId: "JEDIKNIGHTLUKE", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 7 },
                { defId: "R2D2_LEGENDARY", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 7 },
                { defId: "HANSOLO", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 6 },
                { defId: "CHEWBACCALEGENDARY", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 6 },
                { defId: "YWINGREBEL", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRarity: 6 },
                { defId: "PRINCESSLEIA", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 3 },
                { defId: "HERMITYODA", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 5 },
                { defId: "WEDGEANTILLES", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 3 },
                { defId: "BIGGSDARKLIGHTER", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 3 },
                { defId: "ADMINISTRATORLANDO", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 5 }
            ],
            teamSize: 15
        }
    },
    LV: { 
        value: "LV", 
        desc: "Lord Vader",
        defId: "LORDVADER",
        reqs: {
            units: [
                { defId: "BADBATCHHUNTER", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 5 },
                { defId: "BADBATCHTECH", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 5 },
                { defId: "BADBATCHWRECKER", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 5 },
                { defId: "TUSKENRAIDER", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 5 },
                { defId: "PADMEAMIDALA", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 8 },
                { defId: "EMBO", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 5 },
                { defId: "CT210408", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 7 },
                { defId: "BADBATCHECHO", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 5 },
                { defId: "COUNTDOOKU", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 8 },
                { defId: "ZAMWESELL", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 7 },
                { defId: "YWINGCLONEWARS", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRarity: 7 },
                { defId: "GRANDMOFFTARKIN", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 7 },
                { defId: "ARCTROOPER501ST", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 8 },
                { defId: "GENERALSKYWALKER", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 8 },
                { defId: "NUTEGUNRAY", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 7 }
            ],
            teamSize: 15
        }
    },
    Rey: { 
        value: "Rey", 
        desc: "Rey",
        defId: "GLREY",
        reqs: {
            units: [
                { defId: "REYJEDITRAINING", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 7 },
                { defId: "FINN", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 5 },
                { defId: "RESISTANCETROOPER", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 5 },
                { defId: "REY", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 7 },
                { defId: "RESISTANCEPILOT", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 3 },
                { defId: "POE", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 5 },
                { defId: "EPIXFINN", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 5 },
                { defId: "AMILYNHOLDO", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 5 },
                { defId: "ROSETICO", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 5 },
                { defId: "EPIXPOE", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 5 },
                { defId: "BB8", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 7 },
                { defId: "SMUGGLERCHEWBACCA", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 3 },
                { defId: "CAPITALRADDUS", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRarity: 5 }
            ],
            teamSize: 13
        }
    },
    SEE: { 
        value: "SEE", 
        desc: "Sith Eternal Emperor",
        defId: "SITHPALPATINE",
        reqs: {
            units: [
                { defId: "EMPERORPALPATINE", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 7 },
                { defId: "VADER", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 7 },
                { defId: "ROYALGUARD", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 3 },
                { defId: "ADMIRALPIETT", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 5 },
                { defId: "DIRECTORKRENNIC", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 4 },
                { defId: "DARTHSIDIOUS", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 7 },
                { defId: "MAUL", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 4 },
                { defId: "COUNTDOOKU", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 6 },
                { defId: "SITHMARAUDER", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 7 },
                { defId: "TIEBOMBERIMPERIAL", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRarity: 6 },
                { defId: "ANAKINKNIGHT", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 7 },
                { defId: "GRANDADMIRALTHRAWN", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 6 },
                { defId: "GRANDMOFFTARKIN", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 3 },
                { defId: "VEERS", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 3 },
                { defId: "COLONELSTARCK", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 3 }
            ],
            teamSize: 15
        }
    },
    SLK: { 
        value: "SLK", 
        desc: "Supreme Leader Kylo Ren",
        defId: "SUPREMELEADERKYLOREN",
        reqs: {
            units: [
                { defId: "KYLORENUNMASKED", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 7 },
                { defId: "FIRSTORDERTROOPER", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 5 },
                { defId: "FIRSTORDEROFFICERMALE", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 5 },
                { defId: "KYLOREN", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 7 },
                { defId: "PHASMA", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 5 },
                { defId: "FIRSTORDEREXECUTIONER", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 5 },
                { defId: "SMUGGLERHAN", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 3 },
                { defId: "FOSITHTROOPER", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 5 },
                { defId: "FIRSTORDERSPECIALFORCESPILOT", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 3 },
                { defId: "GENERALHUX", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 5 },
                { defId: "FIRSTORDERTIEPILOT", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 3 },
                { defId: "EMPERORPALPATINE", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 7 },
                { defId: "CAPITALFINALIZER", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRarity: 5 }
            ],
            teamSize: 13
        }
    }
};

const JOURNEY_CHECK_FUNCTION_MAPPING = {
    CONQUEST: {
        value: "CONQUEST",
        desc: "All Conquest Units",
        reqs: {
            units: [
                { defId: "DARKREY", req: UNIT_REQUIREMENT_TYPE.OPTIONAL, minRarity: 7 },
                { defId: "EZRAEXILE", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRarity: 7 },
                { defId: "LUTHENRAEL", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRarity: 7 },
                { defId: "QUEENAMIDALA", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRarity: 7 },
                { defId: "DARTHBANE", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRarity: 7 },
                { defId: "MOFFGIDEONS3", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRarity: 7 },
                { defId: "TARONMALICOS", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRarity: 7 },
                { defId: "FURYCLASSINTERCEPTOR", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRarity: 7 },
                { defId: "TRENCH", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRarity: 7 },
                { defId: "SCYTHE", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRarity: 7 },
                { defId: "BENSOLO", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRarity: 7 },
                { defId: "DARTHMALGUS", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRarity: 7 },
                { defId: "TIEINTERCEPTOR", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRarity: 7 },
                { defId: "BOBAFETTSCION", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRarity: 7 },
                { defId: "MAULS7", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRarity: 7 },
                { defId: "COMMANDERAHSOKA", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRarity: 7 },
                { defId: "RAZORCREST", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRarity: 7 },
            ],
            teamSize: 16
        }
    },
    GI: { 
        value: "GI", 
        desc: "Grand Inquisitor",
        defId: "GRANDINQUISITOR",
        reqs: {
            units: [
                { defId: "FIFTHBROTHER", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 5 },
                { defId: "SEVENTHSISTER", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 5 },
                { defId: "NINTHSISTER", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 5 },
                { defId: "EIGHTHBROTHER", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 5 },
                { defId: "SECONDSISTER", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 5 }
            ],
            teamSize: 5
        }
    },
    APHRA: {
        value: "APHRA",
        desc: "Doctor Aphra",
        defId: "DOCTORAPHRA",
        reqs: {
            units: [
                { defId: "HONDO", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 5 },
                { defId: "TRIPLEZERO", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 5 },
                { defId: "BT1", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 5 },
                { defId: "SANASTARROS", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 5 }
            ],
            teamSize: 4
        }
    },
    STARKILLER: {
        value: "STARKILLER",
        desc: "Starkiller",
        defId: "STARKILLER",
        reqs: {
            units: [
                { defId: "DASHRENDAR", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 5 },
                { defId: "KYLEKATARN", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 5 },
                { defId: "DARTHTALON", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 5 },
                { defId: "MARAJADE", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 5 }
            ],
            teamSize: 4
        }
    },
    JKCAL: {
        value: "JKCAL",
        desc: "Jedi Knight Cal Kestis",
        defId: "JEDIKNIGHTCAL",
        reqs: {
            units: [
                { defId: "CALKESTIS", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRarity: 7, minGearLevel: 12 },
                { defId: "CEREJUNDA", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRarity: 7, minGearLevel: 12 },
                { defId: "MERRIN", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRarity: 7, minGearLevel: 12 },
                { defId: "TARFFUL", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRarity: 7, minGearLevel: 12 },
                { defId: "SAWGERRERA", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRarity: 7, minGearLevel: 12 }
            ],
            teamSize: 5
        }
    },
    JKL: {
        value: "JKL",
        desc: "Jedi Knight Luke Skywalker",
        defId: "JEDIKNIGHTLUKE",
        reqs: {
            units: [
                { defId: "WAMPA", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 3 },
                { defId: "COMMANDERLUKESKYWALKER", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 3 },
                { defId: "C3POLEGENDARY", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 3 },
                { defId: "CHEWBACCALEGENDARY", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 3 },
                { defId: "HOTHLEIA", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 3 },
                { defId: "HOTHHAN", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 3 },
                { defId: "HERMITYODA", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 3 },
                { defId: "ADMINISTRATORLANDO", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 3 },
                { defId: "VADER", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 3 },
                { defId: "MILLENNIUMFALCON", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRarity: 7 },
                { defId: "XWINGRED2", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRarity: 7 }
            ],
            teamSize: 11
        }
    },
    EPICBK: {
        value: "EPICBK",
        desc: "Bo-Katan (Mand'alor)",
        defId: "MANDALORBOKATAN",
        reqs: {
            units: [
                { defId: "THEMANDALORIANBESKARARMOR", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 7 },
                { defId: "PAZVIZSLA", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 7 },
                { defId: "IG12", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 7 },
                { defId: "KELLERANBEQ", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 7 }
            ],
            teamSize: 4
        }
    },
    BAM: {
        value: "BAM",
        desc: "The Mandalorian (Beskar Armor)",
        defId: "THEMANDALORIANBESKARARMOR",
        reqs: {
            units: [
                { defId: "GREEFKARGA", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRarity: 7, minGearLevel: 10 },
                { defId: "THEMANDALORIAN", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRarity: 7, minGearLevel: 10 },
                { defId: "IG11", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRarity: 7, minGearLevel: 10 },
                { defId: "CARADUNE", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRarity: 7, minGearLevel: 10 },
                { defId: "KUIIL", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRarity: 7, minGearLevel: 10 }
            ],
            teamSize: 5
        }
    },
    JJB: {
        value: "JJB",
        desc: "Jar Jar Binks",
        defId: "JARJARBINKS",
        reqs: {
            units: [
                { defId: "BOSSNASS", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 5 },
                { defId: "CAPTAINTARPALS", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 5 },
                { defId: "BOOMADIER", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 5 },
                { defId: "GUNGANPHALANX", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 5 }
            ],
            teamSize: 4
        }
    },
    BAYLANSKOLL: {
        value: "BAYLANSKOLL",
        desc: "Baylan Skoll",
        defId: "BAYLANSKOLL",
        reqs: {
            units: [
                { defId: "GRANDADMIRALTHRAWN", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 7 },
                { defId: "MORGANELSBETH", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 7 },
                { defId: "GREATMOTHERS", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 7 },
                { defId: "SHINHATI", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 7 },
                { defId: "MARROK", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 7 }
            ],
            teamSize: 5
        }
    },
    LEV: {
        value: "LEV",
        desc: "Leviathan Capital Ship",
        defId: "CAPITALLEVIATHAN",
        reqs: {
            units: [
                { defId: "DARTHREVAN", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 9 },
                { defId: "DARTHMALAK", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 9 },
                { defId: "SITHTROOPER", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 7 },
                { defId: "FOSITHTROOPER", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 7 },
                { defId: "MAUL", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 7 },
                { defId: "HK47", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 7 },
                { defId: "BASTILASHAN", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 7 },
                { defId: "SITHASSASSIN", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 5 },
                { defId: "50RT", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 5 },
                { defId: "FURYCLASSINTERCEPTOR", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRarity: 7 },
                { defId: "SITHSUPREMACYCLASS", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRarity: 7 },
                { defId: "TIEDAGGER", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRarity: 7 },
                { defId: "SITHBOMBER", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRarity: 7 },
                { defId: "SITHINFILTRATOR", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRarity: 7 },
                { defId: "SITHFIGHTER", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRarity: 7 },
                { defId: "EBONHAWK", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRarity: 7 },
            ],
            teamSize: 16
        }
    },
    EXEC: {
        value: "EXEC",
        desc: "Executor Capital Ship",
        defId: "CAPITALEXECUTOR",
        reqs: {
            units: [
                { defId: "VADER", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 7 },
                { defId: "ADMIRALPIETT", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 8 },
                { defId: "BOBAFETT", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 8 },
                { defId: "DENGAR", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 5 },
                { defId: "IG88", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 5 },
                { defId: "BOSSK", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 5 },
                { defId: "TIEFIGHTERPILOT", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 5 },
                { defId: "TIEADVANCED", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRarity: 4 },
                { defId: "TIEBOMBERIMPERIAL", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRarity: 4 },
                { defId: "HOUNDSTOOTH", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRarity: 4 },
                { defId: "SLAVE1", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRarity: 4 },
                { defId: "IG2000", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 4 },
                { defId: "TIEFIGHTERIMPERIAL", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRarity: 4 },
                { defId: "RAZORCREST", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRarity: 5 }
            ],
            teamSize: 14
        }
    },
    PROF: {
        value: "PROF",
        desc: "Profundity Capital Ship",
        defId: "CAPITALPROFUNDITY",
        reqs: {
            units: [
                { defId: "ADMIRALRADDUS", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 9 },
                { defId: "CASSIANANDOR", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 8 },
                { defId: "DASHRENDAR", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 7 },
                { defId: "MONMOTHMA", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 5 },
                { defId: "BISTAN", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 5 },
                { defId: "JYNERSO", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 5 },
                { defId: "HERASYNDULLAS3", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 5 },
                { defId: "OUTRIDER", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRarity: 4 },
                { defId: "UWINGROGUEONE", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRarity: 4 },
                { defId: "UWINGSCARIF", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRarity: 4 },
                { defId: "XWINGRED2", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRarity: 4 },
                { defId: "XWINGRED3", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRelicLevel: 4 },
                { defId: "YWINGREBEL", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRarity: 4 },
                { defId: "GHOST", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRarity: 4 }
            ],
            teamSize: 14
        }
    },
    ALLCAPITALSHIPS: {
        value: "ALLCAPITALSHIPS",
        desc: "All Capital Ships (7*)",
        reqs: {
            units: [
                { defId: "CAPITALMONCALAMARICRUISER", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRarity: 7 },
                { defId: "CAPITALSTARDESTROYER", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRarity: 7 },
                { defId: "CAPITALJEDICRUISER", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRarity: 7 },
                { defId: "CAPITALCHIMAERA", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRarity: 7 },
                { defId: "CAPITALRADDUS", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRarity: 7 },
                { defId: "CAPITALFINALIZER", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRarity: 7 },
                { defId: "CAPITALMALEVOLENCE", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRarity: 7 },
                { defId: "CAPITALNEGOTIATOR", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRarity: 7 },
                { defId: "CAPITALEXECUTOR", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRarity: 7 },
                { defId: "CAPITALPROFUNDITY", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRarity: 7 },
                { defId: "CAPITALLEVIATHAN", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRarity: 7 }

            ],
            teamSize: 11
        }
        
    },
    METACAPITALSHIPS: {
        value: "METACAPITALSHIPS",
        desc: "Meta Capital Ships (7*)",
        reqs: {
            units: [
                { defId: "CAPITALEXECUTOR", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRarity: 7 },
                { defId: "CAPITALPROFUNDITY", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRarity: 7 },
                { defId: "CAPITALLEVIATHAN", req: UNIT_REQUIREMENT_TYPE.REQUIRED, minRarity: 7 }

            ],
            teamSize: 3
        }
        
    }
};

const CHECK_FUNCTION_MAPPING_DEF_ID_LOOKUP = {
    "JEDIMASTERKENOBI": GL_CHECK_FUNCTION_MAPPING.JMK,
    "GRANDMASTERLUKE": GL_CHECK_FUNCTION_MAPPING.JML,
    "JABBATHEHUTT": GL_CHECK_FUNCTION_MAPPING.JTH,
    "LORDVADER": GL_CHECK_FUNCTION_MAPPING.LV,
    "GLREY": GL_CHECK_FUNCTION_MAPPING.Rey,
    "SITHPALPATINE": GL_CHECK_FUNCTION_MAPPING.SEE,
    "SUPREMELEADERKYLOREN": GL_CHECK_FUNCTION_MAPPING.SLK,
    "GLLEIA": GL_CHECK_FUNCTION_MAPPING.LEIA,
    "DOCTORAPHRA": JOURNEY_CHECK_FUNCTION_MAPPING.APHRA,
    "CAPITALEXECUTOR": JOURNEY_CHECK_FUNCTION_MAPPING.EXEC,
    "GRANDINQUISITOR": JOURNEY_CHECK_FUNCTION_MAPPING.GI,
    "JARJARBINKS": JOURNEY_CHECK_FUNCTION_MAPPING.JJB,
    "JEDIKNIGHTCAL": JOURNEY_CHECK_FUNCTION_MAPPING.JKCAL,
    "JEDIKNIGHTLUKE": JOURNEY_CHECK_FUNCTION_MAPPING.JKL,
    "MANDALORBOKATAN": JOURNEY_CHECK_FUNCTION_MAPPING.EPICBK,
    "THEMANDALORIANBESKARARMOR": JOURNEY_CHECK_FUNCTION_MAPPING.BAM,
    "CAPITALLEVIATHAN": JOURNEY_CHECK_FUNCTION_MAPPING.LEV,
    "CAPITALPROFUNDITY": JOURNEY_CHECK_FUNCTION_MAPPING.PROF,
    "STARKILLER": JOURNEY_CHECK_FUNCTION_MAPPING.STARKILLER,
    "CONQUEST": JOURNEY_CHECK_FUNCTION_MAPPING.CONQUEST
};

const PORTRAIT_WIDTH = 50;
const PORTRAIT_HEIGHT = 50;
const readinessColWidth = 60;
const nameColWidth = 150;
const unitColWidth = 60;
const firstRowHeight = 100;
const playerRowHeight = 24;

module.exports = {
    UNIT_REQUIREMENT_TYPE: UNIT_REQUIREMENT_TYPE,
    JOURNEY_CHECK_FUNCTION_MAPPING: JOURNEY_CHECK_FUNCTION_MAPPING,
    GL_CHECK_FUNCTION_MAPPING: GL_CHECK_FUNCTION_MAPPING,
    RAID_CHECK_FUNCTION_MAPPING: RAID_CHECK_FUNCTION_MAPPING,
    TB_CHECK_FUNCTION_MAPPING: TB_CHECK_FUNCTION_MAPPING,
    CHECK_FUNCTION_MAPPING_DEF_ID_LOOKUP: CHECK_FUNCTION_MAPPING_DEF_ID_LOOKUP,
    getUnitDefinitionForRequirements: getUnitDefinitionForRequirements,
    getUnitReadiness: async function (player, unit, minimums, unitDefinition)
    {
        let rosterUnit = swapiUtils.getPlayerRoster(player).find(ru => swapiUtils.getUnitDefId(ru) === unit.defId);

        let unitReadinessObj = {
            unitDefId: unit.defId,
            unitName: unitDefinition?.name ?? unit.defId,
            combatType: swapiUtils.getUnitCombatType(unitDefinition),
            curRelicLevel: 0,
            curGearLevel: 0,
            curRarity: 0,
            curOmiCount: 0,
            percentReady: 0,
            requirementType: UNIT_REQUIREMENT_TYPE.NICE_TO_HAVE
        }
    
        unitReadinessObj.requirementType = unit.req;
    
        if (rosterUnit)
        {
            unitReadinessObj.curRelicLevel = swapiUtils.getUnitRelicLevel(rosterUnit);
            unitReadinessObj.curGearLevel = swapiUtils.getUnitGearLevel(rosterUnit);
            unitReadinessObj.curRarity = swapiUtils.getUnitRarity(rosterUnit);
            unitReadinessObj.curOmiCount = await swapiUtils.getUnitOmicronCount(rosterUnit);
        }

        let minRarity = unit.minRarity ?? minimums.minRarity ?? 0;
        let minGearLevel = unit.minGearLevel ?? minimums.minGearLevel ?? 0;
        let minRelicLevel = unit.minRelicLevel ?? minimums.minRelicLevel ?? -1;
    
        if (unitReadinessObj.combatType === constants.COMBAT_TYPES.FLEET)
        {
            // for ships, only rarity matters
            unitReadinessObj.percentReady = Math.min(1, unitReadinessObj.curRarity / minRarity)*100;
        } 
        else if (rosterUnit)
        {
            let weights = (minRelicLevel > 0) ? WEIGHTS.GEAR_RARITY_AND_RELIC : WEIGHTS.GEAR_AND_RARITY;
    
            unitReadinessObj.percentReady = Math.min(100, 
                Math.min(1, unitReadinessObj.curRarity / minRarity)*weights.rarity + 
                Math.min(1, unitReadinessObj.curGearLevel / minGearLevel)*weights.gear + 
                Math.max(((unitReadinessObj.curRelicLevel-2) / minRelicLevel)*weights.relic,0));
        
            // if they need omi but don't have, subtract 5% 
            if (unit.omi && unitReadinessObj.curOmiCount === 0)
            {
                unitReadinessObj.percentReady -= 5;
                unitReadinessObj.percentReady = Math.max(0, unitReadinessObj.percentReady); // make sure it's at least 0
            }   
        }
    
        return unitReadinessObj;
    },  
    getReadiness: async function(player, checkData, minimums, unitDefinitions)
    {
        let readinessObject = {
            ready: true,
            playerName: swapiUtils.getPlayerName(player),
            playerAllycode: swapiUtils.getPlayerAllycode(player),
            units: [],
            readyUnit: null,
            overallPercentReady: 0
        }
    
        let totalPercent = 0;
    
        let requiredUnitCount = checkData.reqs.units.filter(u => u.req === UNIT_REQUIREMENT_TYPE.REQUIRED).length;
        let optionalUnitCount = checkData.reqs.units.filter(u => u.req === UNIT_REQUIREMENT_TYPE.OPTIONAL).length;

        if (checkData.defId)
        {
            // get current status for the unit being checked, if there is one
            let rosterUnit = swapiUtils.getPlayerRoster(player).find(u => swapiUtils.getUnitDefId(u) === checkData.defId);
            let unitDefinition = unitDefinitions.find(d => swapiUtils.getUnitDefId(d) === checkData.defId)

            let unitReadinessObj = {
                unitDefId: checkData.defId,
                unitName: unitDefinition?.name ?? checkData.defId,
                combatType: swapiUtils.getUnitCombatType(unitDefinition),
                curRelicLevel: swapiUtils.getUnitRelicLevel(rosterUnit) ?? 0,
                curGearLevel: swapiUtils.getUnitGearLevel(rosterUnit) ?? 0,
                curRarity: swapiUtils.getUnitRarity(rosterUnit) ?? 0,
                isCheckedUnit: true
            }

            readinessObject.readyUnit = unitReadinessObj;
        }
        for (let unit of checkData.reqs.units)
        {
            let unitReadinessObj = await this.getUnitReadiness(
                player, 
                unit, 
                minimums,
                unitDefinitions.find(d => swapiUtils.getUnitDefId(d) === unit.defId));

            if (unitReadinessObj.curRarity === 0 && CHECK_FUNCTION_MAPPING_DEF_ID_LOOKUP[unit.defId]) {
                // show locked ready units as a % complete of the unlocking
                let readyUnitDefinitions = await getUnitDefinitionForRequirements(CHECK_FUNCTION_MAPPING_DEF_ID_LOOKUP[unit.defId]);
                let readyUnitReadinessObject = await this.getReadiness(player, CHECK_FUNCTION_MAPPING_DEF_ID_LOOKUP[unit.defId], {}, readyUnitDefinitions);
                unitReadinessObj.percentReady = readyUnitReadinessObject.overallPercentReady;

                // lose 1% readiness because they have all the requirements but haven't actually done the event to unlock
                // if (unitReadinessObj.percentReady === 100) unitReadinessObj.percentReady--;
            }

            // must have all unitsRequired
            if (unit.req === UNIT_REQUIREMENT_TYPE.REQUIRED)
            {
                if (unitReadinessObj.percentReady < 100) readinessObject.ready = false;
                if (unitReadinessObj.curRarity > 0) totalPercent += unitReadinessObj.percentReady; // only counts toward total % ready if unlocked
            }
    
            readinessObject.units.push(unitReadinessObj);
        }
    
        // If the required size of the team exceeds the number of required units, then find the closest-to-ready optional
        // units and include their readiness in the total. Otherwise, all required units are handled above.
        if ((checkData.reqs.teamSize ?? 5) > requiredUnitCount)
        {
            let percentsReady = readinessObject.units
                                .filter(u => u.requirementType === UNIT_REQUIREMENT_TYPE.OPTIONAL) // required units are handled above
                                .map(u => (u.curRarity > 0) ? u.percentReady : 0); // if a unit isn't unlocked, it's % ready is always zero

            // choose the top % ready among the optional to fill in the gaps
            percentsReady.sort((a, b) => b - a);
            percentsReady = percentsReady.slice(0, ((checkData.reqs.teamSize ?? 5) - requiredUnitCount));
        
            for (let p of percentsReady) totalPercent += p;
        }

        let divisor = Math.min(checkData.reqs.teamSize ?? 5, requiredUnitCount + optionalUnitCount);
    
        readinessObject.overallPercentReady = totalPercent / divisor;
    
        readinessObject.ready = readinessObject.overallPercentReady === 100;
    
        return readinessObject;
    },
    getReadinessResponse: async function(checkData, guildPlayerData, minimumsOverride)
    {
        let embed = new EmbedBuilder()
            .setTitle(`${checkData.desc} Readiness for ${guildPlayerData.guildName}`)
            .setColor(0xD2691E);
    
        let files = [];
        let gapData = [];
        let readyData = [];

        let globalMinimumsSpecified = (minimumsOverride != null) || (checkData.reqs.minRarity || checkData.reqs.minGearLevel || checkData.reqs.minRelicLevel);
        let minimums = {
            minRarity: (minimumsOverride?.minRarity) ?? checkData.reqs.minRarity ?? 7,
            minGearLevel: (minimumsOverride?.minGearLevel) ?? checkData.reqs.minGearLevel ?? 13,
            minRelicLevel: (minimumsOverride?.minRelicLevel) ?? checkData.reqs.minRelicLevel ?? -1
        }
    
        let unitDefinitions = await getUnitDefinitionForRequirements(checkData);

        for (let player of guildPlayerData.players)
        {
            let readiness = await this.getReadiness(player, checkData, minimums, unitDefinitions);
    
            if (readiness.ready) 
            {
                readyData.push(readiness);
            } else {
                gapData.push(readiness);
            }
        }
    
        gapData.sort((a, b) => {
            return (b.overallPercentReady - a.overallPercentReady) || (a.playerName.localeCompare(b.playerName))
        });
    
        readyData.sort((a, b) => {
            return (b.overallPercentReady - a.overallPercentReady) || ((b.readyUnit?.curRarity ?? 0) - (a.readyUnit?.curRarity ?? 0)) || (a.playerName.localeCompare(b.playerName))
        });
    
        let width = readinessColWidth + nameColWidth + (checkData.reqs.units.length + (checkData.defId ? 1 : 0))*unitColWidth;
        let height = firstRowHeight + playerRowHeight*(gapData.length + readyData.length)
    
        const canvas = createCanvas(width, height);
        const context = canvas.getContext('2d');
        context.fillStyle = "rgb(0, 0, 0)"
        context.fillRect(0, 0, width, height);
    
        for (let unitIx = 0; unitIx < checkData.reqs.units.length; unitIx++)
        {
            let unit = checkData.reqs.units[unitIx];
    
            context.font = `12pt "Noto Sans Extra Bold"`;
            context.textAlign = "center";
            context.textBaseline = "middle";
            context.fillStyle = unit.req.color;

            let reqDetails = "";
            if (unit.minRelicLevel || checkData.reqs.minRelicLevel) reqDetails = `R${unit.minRelicLevel ?? checkData.reqs.minRelicLevel}`;
            else  
            {
                let minRarityStr = (unit.minRarity || checkData.reqs.minRarity) ? `${unit.minRarity ?? checkData.reqs.minRarity}*` : "";
                let minGearStr = (unit.minGearLevel || checkData.reqs.minGearLevel) ? `G${unit.minGearLevel ?? checkData.reqs.minGearLevel}` : "";

                reqDetails = `${minRarityStr}${(minRarityStr.length > 0 && minGearStr.length > 0) ? " " : ""}${minGearStr}`;
            }

            context.fillText(unit.req.text,
                readinessColWidth + nameColWidth + (unitIx+0.5+(checkData.defId ? 1 : 0))*unitColWidth,
                13
                );
    
            context.font = `10pt "Noto Sans"`;
            context.fillStyle = "#aaaaaa";
            context.fillText(reqDetails,
                readinessColWidth + nameColWidth + (unitIx+0.5+(checkData.defId ? 1 : 0))*unitColWidth,
                30
                );
                
            let portrait = await unitsUtils.getPortraitForUnitByDefId(unit.defId);
            let img = portrait ?? './src/img/no-portrait.png';
            const unitIconImg = await loadImage(img);
            context.drawImage(unitIconImg, 
                readinessColWidth + nameColWidth + (unitIx+(checkData.defId ? 1 : 0))*unitColWidth + 5, 
                40, 
                PORTRAIT_WIDTH, PORTRAIT_HEIGHT);
    
            if (unit.omi)
            {
                // unit requires an omicron, so draw the omicron picture on
                const omicronImg = await loadImage(constants.OMICRON_IMG_URL);
                context.drawImage(omicronImg, 
                    readinessColWidth + nameColWidth + (unitIx+(checkData.defId ? 1 : 0))*unitColWidth + 25, 
                    70);
            }
        }
    
        for (let rIx = 0; rIx < readyData.length; rIx++)
        {
            drawPlayerRow(canvas, checkData, rIx, readyData[rIx])
        }
    
        for (let gIx = 0; gIx < gapData.length; gIx++)
        {
            drawPlayerRow(canvas, checkData, gIx + readyData.length, gapData[gIx]);
        }

    
        if (checkData.defId) 
        {
            let portrait = await unitsUtils.getPortraitForUnitByDefId(checkData.defId);
            let img = portrait ?? './src/img/no-portrait.png';
            const unitIconImg = await loadImage(img);
            context.drawImage(unitIconImg, 
                readinessColWidth + nameColWidth + 5, 
                40, 
                PORTRAIT_WIDTH, PORTRAIT_HEIGHT);

            context.fillStyle = "rgb(255, 255, 255)"
            context.fillRect(readinessColWidth + nameColWidth + PORTRAIT_WIDTH + 8, 40, 1, height);
            context.fillRect(readinessColWidth + nameColWidth + PORTRAIT_WIDTH + 11, 40, 1, height);
            
        }

        // used for both csv and image
        let fileName = `${checkData.value}-ready-${new Date().getTime()}`;

        // add csv export of data
        const csvParsingOptions = { defaultValue: 0 };

        let csvReadyData = readyData.concat(gapData).map(r => {
            let obj = {
                "Name": r.playerName,
                "Ally Code": r.playerAllycode,
                "Overall % Ready": r.overallPercentReady,
            };

            if (checkData.defId) obj[`${r.readyUnit.unitName}`] = getUnitDisplayText(r.readyUnit, "-");

            for (let u of r.units)
            {
                obj[`${u.unitName}`] = getUnitDisplayText(u, "-");
            }

            return obj;
        })

        let csvText = await parseAsync(csvReadyData, csvParsingOptions);
        let csvBuffer = Buffer.from(csvText);

        let csvAttachment = new AttachmentBuilder(csvBuffer, { name: `${fileName}.csv`});
        files.push(csvAttachment);

        // add image attachment
        let buffer = canvas.toBuffer("image/png");
            
        const file = new AttachmentBuilder(buffer, { name: fileName + ".png" });
        embed.setImage("attachment://" + fileName);
        files.push(file);
    
        let requirementsString = "";
        if (globalMinimumsSpecified)
        {
            requirementsString = `Requirements: ${minimums.minRarity}★, G${minimums.minGearLevel}${(minimums.minRelicLevel != -1) ? `, R${minimums.minRelicLevel}` : ""}\nTeam size: ${checkData.reqs.teamSize ?? "5"}`;
        }
        embed.setDescription(
`${requirementsString}
Total ready: ${readyData.length}
Not ready: ${gapData.length}`)
    
        return { embeds: [embed], files: files, content: null, components: null };
    
    }
}

async function getUnitDefinitionForRequirements(checkData)
{
    let allUnits = await swapiUtils.getUnitsList();

    let unitDefs = allUnits.filter(u => swapiUtils.getUnitDefId(u) === checkData.defId || checkData.reqs.units.find(reqUnit => reqUnit.defId === swapiUtils.getUnitDefId(u)));

    return unitDefs;
}

function drawPlayerRow(canvas, checkData, curPlayer, readinessData)
{
    const context = canvas.getContext('2d');

    let bgColor = readinessGradient.getColor(Math.max(1, Math.floor(readinessData.overallPercentReady / 5.0)));
    context.fillStyle = bgColor;
    context.fillRect(0, 
        firstRowHeight + (curPlayer)*playerRowHeight, 
        canvas.width, 
        playerRowHeight)

    context.font = `12pt "Noto Sans","Noto Sans SC","Noto Sans Armenian","Noto Sans Thai","Noto Sans Arabic","Noto Sans Korean"`
    context.textAlign = "left"
    context.textBaseline = "middle";
    context.fillStyle = "#fff"

    context.fillText(Math.floor(readinessData.overallPercentReady).toString() + "%",
        2,
        firstRowHeight + (curPlayer+0.5)*playerRowHeight,
        readinessColWidth);

    context.fillText(readinessData.playerName,
        readinessColWidth + 2,
        firstRowHeight + (curPlayer+0.5)*playerRowHeight,
        nameColWidth - 2
        )

    if (checkData.defId)
    {
        let checkedUnitText = getUnitDisplayText(readinessData.readyUnit);
    
        context.font = `12pt "Noto Sans"`
        context.textAlign = "center"
        context.textBaseline = "middle";
        context.fillStyle = "#fff"
        context.fillText(checkedUnitText,
            readinessColWidth + nameColWidth + (unitColWidth)*(0.5),
            firstRowHeight + (curPlayer+0.5)*playerRowHeight
            )
    }

    for (let uIx = 0; uIx < readinessData.units.length; uIx++)
    {
        let unit = readinessData.units[uIx];
        
        let text = getUnitDisplayText(unit);
    
        context.font = `12pt "Noto Sans"`
        context.textAlign = "center"
        context.textBaseline = "middle";
        context.fillStyle = "#fff"
        context.fillText(text,
            readinessColWidth + nameColWidth + (unitColWidth)*(uIx+0.5 + (checkData.defId ? 1 : 0)),
            firstRowHeight + (curPlayer+0.5)*playerRowHeight
            )
    }
}

function getUnitDisplayText(unit, lockedSymbolOverride)
{
    if (unit.curRarity === 0)
    {
        if (!unit.isCheckedUnit && CHECK_FUNCTION_MAPPING_DEF_ID_LOOKUP[unit.unitDefId])
        {
            return Math.floor(unit.percentReady).toString() + "%";
        } else {
            return lockedSymbolOverride ?? "—";
        }
    }

    if (unit.curRarity < 7 || unit.combatType === constants.COMBAT_TYPES.FLEET) 
    {
        return `${unit.curRarity}*`;
    } 
    else if (unit.curGearLevel < 13)
    {
        return `G${unit.curGearLevel}`;
    }
    else 
    {
        return `R${unit.curRelicLevel - 2}`;
    }
}




