const DataBuilder = require('./dataBuilder');
const statCalculator = require("./statCalculator");

const dataPath = process.env.DATA_PATH || 'statCalcData';

const dataBuilder = new DataBuilder({
  dataPath: dataPath,
  updateInterval: process.env.UPDATE_INTERVAL,
  url: process.env.COMLINK_URL,
  accessKey: process.env.ACCESS_KEY,
  secretKey: process.env.SECRET_KEY,
  zipGameData: process.env.ZIP_GAME_DATA,
  useSegments: process.env.USE_SEGMENTS,
  useUnzip: process.env.USE_UNZIP
});

const MAX_LEVEL = process.env.MAX_LEVEL || 85;
const MAX_GEAR_LEVEL = process.env.MAX_GEAR_LEVEL || 13;
const MAX_RARITY = process.env.MAX_RARITY || 7;
const MAX_RELIC = process.env.MAX_RELIC || 11;
const MAX_MOD_PIPS = process.env.MAX_MOD_PIPS || 6;
const MAX_MOD_LEVEL = process.env.MAX_MOD_LEVEL || 15;
const MAX_MOD_TIER = process.env.MAX_MOD_TIER || 5;

let gameData;
const MAX_VALUES = {
  char: {
    rarity: MAX_RARITY,
    level: MAX_LEVEL,
    gear: MAX_GEAR_LEVEL,
    equipped: "all",
    relic: MAX_RELIC,
    skills: "max",
    modRarity: MAX_MOD_PIPS,
    modLevel: MAX_MOD_LEVEL,
    modTier: MAX_MOD_TIER
  },
  ship: {
    rarity: MAX_RARITY,
    level: MAX_LEVEL,
    skills: "max"
  },
  crew: {
    rarity: MAX_RARITY,
    level: MAX_LEVEL,
    gear: MAX_GEAR_LEVEL,
    equipped: "all",
    skills: "max",
    modRarity: MAX_MOD_PIPS,
    modLevel: MAX_MOD_LEVEL,
    modTier: MAX_MOD_TIER,
    relic: MAX_RELIC
  }
};


module.exports.initCalc = async () => {
  try {
    // await dataBuilder.updateCheck();
    gameData = await dataBuilder.init();
    statCalculator.setGameData(gameData);
  } catch(error) {
    throw(error);
  }
};

module.exports.calcRosterStats = (roster, options) => {
  return statCalculator.calcRosterStats(roster, options);
}

module.exports.listenForUpdates = async () => {
  try {
    gameData = await dataBuilder.listenForUpdates((error, newGameData) => {
      if (error) {
        console.log(`Error updating game data: ${error.message}`);
      } else {
        gameData = newGameData;
        statCalculator.setGameData(newGameData);
      }
    });
    statCalculator.setGameData(gameData);
  } catch(error) {
    throw(error);
  }
};