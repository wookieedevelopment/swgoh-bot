const database = require ("../database.js");
const metadataCache = require("./metadataCache");


module.exports = {
    getDatacronSetList: async function(activeOnly = true)
    {
        let where = activeOnly ? "WHERE expiration_dt > now()" : "";
        let datacrons = await database.db.any(`SELECT DISTINCT set_id, color_text, color_code FROM datacron_template ${where} ORDER BY set_id DESC`);
        return datacrons;
    },
    getDatacronAbilities: async function(sets, level, activeOnly = true)
    {
        // TODO: Make sure this doesn't update the original metadata
        let dcs;
        if (activeOnly) dcs = await this.getActiveWookieeBotDatacronData();
        else dcs = await this.getWookieeBotDatacronData();

        if ((sets?.length ?? 0) > 0)
        {
            dcs = dcs.filter(d => sets.find(s => s === d.set_id));
        }

        if (level != null)
        {
            switch (level)
            {
                case 3: dcs = dcs.filter(d => d.alignment != null && d.faction == null); break;
                case 6: dcs = dcs.filter(d => d.faction != null && d.character == null); break;
                case 9: dcs = dcs.filter(d => d.character != null); break;
            }
        }
        
        return dcs;
    },
    getWookieeBotDatacronData: async function()
    {
        let datacronData = metadataCache.get(metadataCache.CACHE_KEYS.wookieeBotDatacronData);
    
        if (datacronData == undefined)
        {
            datacronData = await database.db.any(
            `select distinct datacron_id, affix_id, ability_id, set_id, short_description, mini_description, alignment, faction, character, target_rule, img, expiration_dt, duplicate_ability_ids from datacron
            left join datacron_template using (affix_id, set_id)
            left outer join asset_image ON icon_asset_name = asset_name
            `);
    
            metadataCache.set(metadataCache.CACHE_KEYS.wookieeBotDatacronData, datacronData, 3600);
        }
    
        return datacronData;
    },
    getActiveWookieeBotDatacronData: async function()
    {
        let datacronData = metadataCache.get(metadataCache.CACHE_KEYS.wookieeBotActiveDatacronData);
    
        if (datacronData == undefined)
        {
            datacronData = await database.db.any(
            `select distinct datacron_id, affix_id, ability_id, set_id, short_description, mini_description, alignment, faction, character, target_rule, img, expiration_dt, duplicate_ability_ids from datacron
            left join datacron_template using (affix_id, set_id)
            left outer join asset_image ON icon_asset_name = asset_name
            where expiration_dt > now()
            `);
    
            metadataCache.set(metadataCache.CACHE_KEYS.wookieeBotActiveDatacronData, datacronData, 3600);
        }
    
        return datacronData;
    },
    getMatchingDCsByWookieeBotDatacronId: async function(datacronId)
    {
        let datacronData = await this.getWookieeBotDatacronData();
        
        if (Array.isArray(datacronId))
        {
            let matches = datacronData.filter(r => datacronId.find(i => i === r.datacron_id) != null);
            return matches;            
        }
        else if (typeof datacronId === "number")
        {
            let match = datacronData.find(r => r.datacron_id === datacronId);
            return match;
        }

        return null;
    }
}