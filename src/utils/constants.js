
const NULL_GUILD_ID = -1;
const NO_DATACRON_SPECIFIED = -1;
const OMICRON_IMG_URL = './src/img/30px-Game-Icon-Ability_Material_Omicron.png';
const ZETA_IMG_URL = './src/img/30px-Game-Icon-Ability_Material_Zeta.png';


const DISCORD_LINK_TYPES = {
    server: "server",
    channel: "channel"
};

const CacheTypes = {
    MANUAL: "MANUAL",
    NIGHTLY: "NIGHTLY",
    TW: "TW",
    QUERY: "QUERY"
}

const CombatTypes = {
    SQUAD: 1,
    FLEET: 2
}

const MOD_SLOTS = {
    SQUARE: 2,
    ARROW: 3,
    DIAMOND: 4,
    TRIANGLE: 5,
    CIRCLE: 6,
    CROSS: 7
}

const UnitAlignments = {
    NEUTRAL: 1,
    LS: 2,
    DS: 3
}

const ModScoreTypes = {
    WOOKIEEBOT: "WookieeBot",
    DSR: "DSR",
    HOTBOT: "HotBot"
}

const MEMBER_TYPE_MAP = 
{
    2: "MEMBER",
    3: "OFFICER",
    4: "LEADER"
};

const GUILD_MEMBER_TYPES = {
    MEMBER: 2,
    OFFICER: 3,
    LEADER: 4
};

const TIME_PERIODS = {
    DAY_SECONDS: 24*60*60,
    WEEK_SECONDS: 7*24*60*60,
    MONTH_SECONDS: 30*24*60*60
};

module.exports = {
    NULL_GUILD_ID: NULL_GUILD_ID,
    CacheTypes: CacheTypes,
    NO_DATACRON_SPECIFIED: NO_DATACRON_SPECIFIED,
    COMBAT_TYPES: CombatTypes,
    OMICRON_IMG_URL: OMICRON_IMG_URL,
    ZETA_IMG_URL: ZETA_IMG_URL,
    MOD_SCORE_TYPES: ModScoreTypes,
    UNIT_ALIGNMENTS: UnitAlignments,
    MOD_SLOTS: MOD_SLOTS,
    MEMBER_TYPE_MAP: MEMBER_TYPE_MAP,
    GUILD_MEMBER_TYPES: GUILD_MEMBER_TYPES,
    DISCORD_LINK_TYPES: DISCORD_LINK_TYPES,
    TIME_PERIODS: TIME_PERIODS
}