const swapi = require('./swapi');
const database = require('../database');
const discordUtils = require ("./discord");
const { CLIENT } = require ("./discordClient");
const { ExceptionHandler } = require('winston');
const comlink = require('./comlink');
const { EmbedBuilder, ActionRowBuilder, ButtonBuilder, ButtonStyle } = require('discord.js');
const tools = require("./tools");
const constants = require("./constants");
const { logger, formatError } = require("../utils/log");

const GUILD_MEMBERS_SORT_OPTIONS = {
    Name: "N",
    Rank: "R",
    GP: "G",
    SkillRating: "S"
};

const RAID_NAME_MAPPING = {
    KRAYTDRAGON: "Krayt Dragon",
    RANCOR: "Rancor",
    AAT: "AAT",
    SITH_RAID: "Sith Triumvirate Raid",
    ROTJ: "Endor (Speeder Bike)",
    NABOO: "Battle for Naboo"
};

const MAX_GET_GUILD_ATTEMPTS = 5;
const SCALING_FACTOR_MILLIS = 20;

module.exports = {
    RAID_NAME_MAPPING: RAID_NAME_MAPPING,
    createGuildEntryAndStartTracking: async function(guildAPIData)
    {
        if (!guildAPIData) throw new Error("No guild data provided.");

        await database.db.any(
            `INSERT INTO guild(guild_name, comlink_guild_id, description, track_data)
            VALUES ($1, $2, $3, TRUE)
            ON CONFLICT (comlink_guild_id) DO UPDATE SET guild_name = $1, description = $3, track_data = TRUE;
            
            INSERT INTO tw_metadata (max_teams_per_territory, default_max_squads_per_player, default_max_fleets_per_player, guild_id) 
            VALUES ($4, $5, $6, (SELECT guild_id FROM guild WHERE comlink_guild_id = $2))
            ON CONFLICT (guild_id) DO NOTHING;`,
            [this.getGuildName(guildAPIData), swapi.getGuildId(guildAPIData), this.getGuildDescription(guildAPIData), 50, 15, 3]
        );
    },
    refreshGuildPlayers: async function (guildAPIData)
    {
        // no harm in running create repetitively
        await this.createGuildEntryAndStartTracking(guildAPIData);
        
        let guildId = await this.getWookieeBotGuildIdForAPIGuild(guildAPIData);
        
        if (!guildId)
        {
            throw new Error(`Something went wrong registering guild: ${swapi.getGuildId(guildAPIData)}`);
        }

        // records to be updated:
        let guildPlayersInsertData = getGuildPlayersInsertData(guildAPIData, guildId);

        const guildPlayersInsert = database.pgp.helpers.insert(guildPlayersInsertData, database.columnSets.guildPlayersCS);
        const guildPlayersConflict = " ON CONFLICT (allycode) DO UPDATE SET guild_id = EXCLUDED.guild_id, player_name = EXCLUDED.player_name, member_type = EXCLUDED.member_type, last_checked_dt = EXCLUDED.last_checked_dt, player_id = EXCLUDED.player_id, guild_join_time = EXCLUDED.guild_join_time"
        
        let guildAllycodes = guildPlayersInsertData.map(d => d.allycode);

        // 1. remove everyone from the guild that isn't in the guild anymore
        // 2. insert/update players
        // NOT NEEDED ANYMORE: 3. clean up data for players that moved from one guild to another, and the losing guild didn't get refreshed for whatever reason
        // 4. clean up the guild roles table so that people that aren't members anymore don't have roles
        // 5. add guild roles for players that have a discord ID but do not have a role in the guild
        // 6. set all registered players as admins who are officers or leaders
        await database.db.none(
            `
            UPDATE guild_players 
            SET guild_id = NULL, guild_join_time = NULL
            WHERE guild_id = $1 AND allycode NOT IN ($2:csv);

            ${guildPlayersInsert}${guildPlayersConflict};
            
            DELETE FROM user_guild_role 
                WHERE guild_id = $1
                AND discord_id not in (
                    select discord_id from user_registration where allycode in
                        (select allycode from guild_players where guild_id = $1)
                );
            INSERT INTO user_guild_role (discord_id, guild_id, guild_admin, guild_bot_admin)
                (SELECT discord_id, $1, false, false
                FROM user_registration
                WHERE allycode in (SELECT allycode FROM guild_players WHERE guild_id = $1))
                ON CONFLICT DO NOTHING;
                
            UPDATE user_guild_role
            SET guild_admin = true, guild_bot_admin = true
            WHERE (guild_id || ';' || discord_id) in 
                (
                    SELECT guild_id || ';' || discord_id 
                    FROM guild_players gp
                    INNER JOIN user_registration ur ON ur.allycode = gp.allycode
                    WHERE guild_id = $1 AND (member_type = 'OFFICER' OR member_type = 'LEADER')
                );`, 
                [guildId, guildAllycodes]
        );
    },
    updateGuildDetails: async function(guild)
    {
        let name = this.getGuildName(guild);
        let desc = this.getGuildDescription(guild);
        
        let comlinkGuildId = swapi.getGuildId(guild);
        await database.db.any("UPDATE guild SET guild_name = $1, description = $2 WHERE comlink_guild_id = $3", [name, desc, comlinkGuildId]);
    },
    getGuildDataFromAPI: async function(
        { allycode = null, comlinkGuildId = null, wookieeBotGuildId = null},
        { useCache = false, cacheTimeHours = 1, includeRecentGuildActivityInfo = true}) 
    {
        if (allycode === null && comlinkGuildId === null && wookieeBotGuildId === null)
        {
            throw new Error("Parameter missing for retrieving guild data.");
        }

        if (allycode != null) allycode = tools.cleanAndVerifyStringAsAllyCode(allycode);

        let guild;
        if (useCache)
        {
            let cache;
            if (comlinkGuildId != null)
            {
                cache = await database.db.oneOrNone(
                    `SELECT guild_info_json
                    FROM guild_info_cache
                    WHERE comlink_guild_id = $1 AND timestamp > NOW() - interval '${cacheTimeHours} hours'
                    ORDER BY timestamp DESC LIMIT 1`, 
                    [comlinkGuildId]);
            } 
            else if (wookieeBotGuildId != null)
            {
                cache = await database.db.oneOrNone(
                    `SELECT guild_info_json
                    FROM guild_info_cache
                    WHERE guild_id = $1 AND timestamp > NOW() - interval '${cacheTimeHours} hours'
                    ORDER BY timestamp DESC LIMIT 1`, 
                    [wookieeBotGuildId]);
            } 
            else if (allycode != null)
            {
                cache = await database.db.oneOrNone(
                    `SELECT guild_info_json
                    FROM guild_info_cache
                    WHERE $1 = ANY(allycodes) AND timestamp > NOW() - interval '${cacheTimeHours} hours'
                    ORDER BY timestamp DESC LIMIT 1`, 
                    [allycode.toString()]);
            }

            guild = cache?.guild_info_json;

            await parseMembersAndUpdateCacheIfNeeded(guild);

            if (this.getGuildRoster(guild)) return guild;
        }

        let guildId;

        if (comlinkGuildId != null) {  
            guildId = comlinkGuildId;
        } 
        else if (wookieeBotGuildId != null)
        {
            guildId = await database.db.one("SELECT comlink_guild_id FROM guild WHERE guild_id = $1", [wookieeBotGuildId], a => a.comlink_guild_id);
        } 
        else 
        {
            let player = await comlink.getPlayer(allycode);
            guildId = swapi.getPlayerGuildId(player);
            if (!guildId) throw new Error(`Player **${swapi.getPlayerName(player)} (${swapi.getPlayerAllycode(player)})** is not a member of a guild.`);
            
        }

        
        for (let attempt = 1; attempt <= MAX_GET_GUILD_ATTEMPTS; attempt++)
        {
            try {
                guild = (await comlink.getGuild(guildId, includeRecentGuildActivityInfo))?.guild;
            }
            catch (e) {
                if (e.message?.indexOf("must be 22") != -1) throw new Error(`${guildId} is not a valid Guild Identifier.`);
                logger.error(`Failed to get guild ${guildId} from comlink (Attempt ${attempt}): ${formatError(e)}`);
            }

            await tools.sleep(SCALING_FACTOR_MILLIS * (attempt ** 2));
        }

        if (!guild) throw new Error(`There was an error retrieving guild info for ${allycode ?? comlinkGuildId ?? wookieeBotGuildId}.`);

        // see if there are any guildmembers that weren't pulled correctly
        await resolveNonParsedGuildMembers(guild);

        await cacheGuildData(guild);

        if (includeRecentGuildActivityInfo) {
            await captureRaidPerformance(guild);
            await captureTWMatchups(guild);
        }

        return guild;
    },
    loadGuildPlayerData: async function ({ comlinkGuildId = null, allycode = null, includeRecentGuildActivityInfo = true, editMessageChannelId = null, editMessageId = null, cacheTimeHours = 24 }, interaction)
    {
        let guild = await this.getGuildDataFromAPI({ allycode: allycode, comlinkGuildId: comlinkGuildId }, { includeRecentGuildActivityInfo: includeRecentGuildActivityInfo, useCache: false });
    
        let guildAllycodes = this.getGuildAllycodesForAPIGuild(guild);
        let { players, errors } = await swapi.getPlayers({
            allycodes: guildAllycodes, 
            useCache: true, 
            cacheTimeHours: cacheTimeHours ?? 24, 
            updateStatusCallback: async (allycodes, players, errors) => { 
                let status = `Retrieved ${players.length}/${allycodes.length} rosters for **${this.getGuildName(guild)}**. Errors: ${errors.length}`;
                if (editMessageChannelId && editMessageId)
                {
                    const channel = CLIENT.channels.cache.get(editMessageChannelId);
                    (await channel.messages.fetch({ message: editMessageId })).edit(status);
                } 
                else if (interaction) 
                {
                    await interaction.editReply(status); 
                }
            }
        });
    
        let guildData = {
            comlinkGuildId: swapi.getGuildId(guild),
            guildName: swapi.getGuildName(guild),
            players: players,
            errors: errors,
            guildApiData: guild
        }
    
        return guildData;
    },
    getGuildLeaderAllycodeFromAPIData: function(guild)
    {
        // comlink
        if (guild.member)
        {
            return guild.member.find(m => m.memberLevel === 4).allyCode;
        }

        // .help
        if (guild.roster)
        {
            return guild.roster.find(m => m.guildMemberLevel === 4).allyCode;
        }
        
        // .gg
        if (guild.data)
        {
            return guild.data.members.find(m => m.member_level === 4).ally_code;
        }

        return null;
    },
    GUILD_MEMBERS_SORT_OPTIONS: GUILD_MEMBERS_SORT_OPTIONS,
    generateGuildMembersSortCustomId: function(prefix, guildId, by, asc)
    {
        return `${prefix}:${guildId}:${by}:${asc ? "A" : "D"}`;
    },
    parseGuildMembersSortCustomId: function(customId)
    {
        let regex = /(.+):(.+):(.):(.)/;
        let match = regex.exec(customId);
        
        let retObj = {
            prefix: match[1] ?? null,
            guildId: match[2] ?? null,
            by: match[3] ?? null,
            asc: (match[4] ?? null) === "A"
        };

        return retObj; 
    },
    createGuildMembersResponse: async function(guildId, customIdPrefix, by = GUILD_MEMBERS_SORT_OPTIONS.Rank, asc = true )
    {
        let guild = await this.getGuildDataFromAPI({ comlinkGuildId: guildId }, { useCache: true, cacheTimeHours: 24, includeRecentGuildActivityInfo: true });
    
        let embed = new EmbedBuilder()
            .setTitle(`Guild Membership for **${this.getGuildName(guild)}**`)
            .setColor("Blurple");
    
        let roster = this.getGuildRoster(guild);
        
        switch (by)
        {
            case GUILD_MEMBERS_SORT_OPTIONS.Name:
                roster.sort((a, b) => 
                {
                    if (asc) return swapi.getPlayerName(a).localeCompare(swapi.getPlayerName(b));
                    return swapi.getPlayerName(b).localeCompare(swapi.getPlayerName(a));
                });
                break;
            case GUILD_MEMBERS_SORT_OPTIONS.GP:
                roster.sort((a, b) => 
                {
                    if (asc) return swapi.getGP(a) - swapi.getGP(b);
                    return swapi.getGP(b) - swapi.getGP(a);
                });
                break;
            case GUILD_MEMBERS_SORT_OPTIONS.Rank:
                roster.sort((a, b) => 
                {
                    if (asc) return swapi.getPlayerGuildMemberLevel(a) - swapi.getPlayerGuildMemberLevel(b) ||
                        swapi.getPlayerName(a).localeCompare(swapi.getPlayerName(b));

                    return swapi.getPlayerGuildMemberLevel(b) - swapi.getPlayerGuildMemberLevel(a) ||
                        swapi.getPlayerName(a).localeCompare(swapi.getPlayerName(b));
                });
                break;
            case GUILD_MEMBERS_SORT_OPTIONS.SkillRating:
                roster.sort((a, b) => 
                {
                    if (asc) return (swapi.getPlayerSkillRating(a) ?? 0) - (swapi.getPlayerSkillRating(b) ?? 0);
                    return (swapi.getPlayerSkillRating(b) ?? 0) - (swapi.getPlayerSkillRating(a) ?? 0);
                });
                break;
        }

        let memberText = roster.map(m => {
            let name = swapi.getPlayerName(m);
            let ac = swapi.getPlayerAllycode(m);
            let gp = swapi.getGP(m);

            if (gp > 1000000) gp = +(Math.round(gp / 1000000 + "e+1") + "e-1").toLocaleString("en") + "M";
            else gp = +(Math.round(gp / 1000)).toLocaleString("en") + "K";

            let sr = swapi.getPlayerSkillRating(m) ?? " - ";
            let role;
    
            switch (swapi.getPlayerGuildMemberLevel(m))
            {
                case 4: role = "Lead"; break;
                case 3: role = "Off."; break;
                default: role = "Mem."; break;
            }
    
            return `${discordUtils.fit(name, 13)}${discordUtils.LTR} | ${ac} | ${role} | ${discordUtils.fit(gp, 5)} | ${sr}`;
        }).join("\n");
    
    
        let swgohggUrl = `https://swgoh.gg/g/${guildId}`;            
        let desc =
`\`\`\`
Name          | Ally Code | Role | GP    | SR
--------------|-----------|------|-------|-----
${memberText}
\`\`\`
${swgohggUrl}
Note: If the above link does not work, the guild has not been registered on swgoh.gg.
`;
    
        embed.setDescription(desc);
        embed.setURL(swgohggUrl);

        let sortButtonsRow = new ActionRowBuilder()
            .addComponents(
                new ButtonBuilder()
                    .setLabel("Sort by Name")
                    .setCustomId(`${customIdPrefix}:${guildId}:${GUILD_MEMBERS_SORT_OPTIONS.Name}:${((by === GUILD_MEMBERS_SORT_OPTIONS.Name ? !asc : true) ? "A" : "D")}`)
                    .setStyle(ButtonStyle.Primary),
                new ButtonBuilder()
                    .setLabel("Sort by Role")   
                    .setCustomId(`${customIdPrefix}:${guildId}:${GUILD_MEMBERS_SORT_OPTIONS.Rank}:${((by === GUILD_MEMBERS_SORT_OPTIONS.Rank ? !asc : false) ? "A" : "D")}`)
                    .setStyle(ButtonStyle.Primary),
                new ButtonBuilder()
                    .setLabel("Sort by GP")  
                    .setCustomId(`${customIdPrefix}:${guildId}:${GUILD_MEMBERS_SORT_OPTIONS.GP}:${((by === GUILD_MEMBERS_SORT_OPTIONS.GP ? !asc : false) ? "A" : "D")}`)
                    .setStyle(ButtonStyle.Primary),
                new ButtonBuilder()
                    .setLabel("Sort by Skill")  
                    .setCustomId(`${customIdPrefix}:${guildId}:${GUILD_MEMBERS_SORT_OPTIONS.SkillRating}:${((by === GUILD_MEMBERS_SORT_OPTIONS.SkillRating ? !asc : false) ? "A" : "D")}`)
                    .setStyle(ButtonStyle.Primary)
            );

        return { embeds: [embed], components: [sortButtonsRow] };
    },
    getGuildIdAndAllyCodesForGuildByGuildId: async function (guildId)
    {
        const guildPlayerData = await database.db.any(
            'SELECT allycode,player_name,guild_id FROM guild_players WHERE guild_id = $1'
            , [guildId]);

        return { 
            allycodes: guildPlayerData.map(d => d.allycode), 
            allycodesAndNames: guildPlayerData.map(d => { return { allycode: d.allycode, player_name: d.player_name } }), 
            guildId: guildPlayerData[0] ? guildPlayerData[0].guild_id : null 
        };
    },
    getGuildIdAndAllyCodesForGuildByAllycode: async function (allycode) {
        const guildPlayerData = await database.db.any(
            'SELECT allycode,player_name,guild_id FROM guild_players WHERE guild_id = (SELECT guild_id FROM guild_players WHERE allycode = $1)'
            , [allycode]);

        return { 
            allycodes: guildPlayerData.map(d => d.allycode), 
            allycodesAndNames: guildPlayerData.map(d => { return { allycode: d.allycode, player_name: d.player_name } }), 
            guildId: (guildPlayerData.length > 0 ? guildPlayerData[0].guild_id : null) 
        };
    },
    getGuildMetadataByAllyCode: async function(allycode)
    {
        const guildData = await database.db.any(
            `SELECT guild.guild_name,guild_id, comlink_guild_id, description
            FROM guild_players 
            LEFT JOIN guild USING (guild_id)
            WHERE allycode = $1`, [allycode]
        );

        if (guildData.length == 0)
            throw "Guild not registered for ally code " + allycode;
            
        return guildData[0];
    },
    getAPIGuildIdByAllyCode: async function (allycode)
    {
        const guildData = await database.db.any(
            `SELECT comlink_guild_id 
            FROM guild_players 
            LEFT JOIN guild USING (guild_id)
            WHERE allycode = $1`, [allycode]
        );

        if (guildData.length == 0)
            throw "Guild not registered for ally code " + allycode;
            
        return guildData[0].comlink_guild_id;
    },
    getGuildIdByAllyCode: async function (allycode)
    {
        const guildData = await database.db.any(
            'SELECT guild_id FROM guild_players WHERE allycode = $1', [allycode]
        );

        if (guildData.length == 0) return -1;
            
        return guildData[0].guild_id;
    },
    getGuildIdByInput: async function (input)
    {
        // order by link type (alphabetical) -- channel registration overrides server registration
        const guildId = await database.db.oneOrNone("SELECT guild_id FROM guild_discord_link WHERE discord_link = $1 OR discord_link = $2 ORDER BY link_type ASC LIMIT 1", [input.guildId ?? input.guild?.id, input.channelId ?? input.channel?.id], g => g ? g.guild_id : null);
        
        return guildId;
    },
    getGuildRaidScores: async function(guildApiId, { raidId = "speederbike", limit = 30 })
    {
        return await database.db.any(
            `SELECT 
                guild_raid_score.end_time_seconds,
                SUM(player_raid_score.score) as score
            FROM guild_raid_score
            JOIN player_raid_score USING (comlink_guild_id, end_time_seconds)
            WHERE guild_raid_score.comlink_guild_id = $1 AND guild_raid_score.raid_id = $2
            GROUP BY guild_raid_score.end_time_seconds
            ORDER BY guild_raid_score.end_time_seconds DESC
            LIMIT ${limit}`, [guildApiId, raidId]);
    },
    getGuildMembersCount: function(guild)
    {
        return swapi.getGuildRoster(guild)?.length ?? 0;
    },
    getWookieeBotGuildIdForAPIGuild: async function(guild)
    {
        return await database.db.oneOrNone("SELECT guild_id FROM guild WHERE comlink_guild_id = $1 LIMIT 1", [swapi.getGuildId(guild)], g => g ? g.guild_id : null);
    },
    getGuildAllycodesForAPIGuild: function(guild)
    {
        return getGuildAllycodesForAPIGuild(guild);
    },
    getGuildName: function(guild)
    {
        return swapi.getGuildName(guild);
    },
    getGuildDescription: function(guild)
    {
        return swapi.getGuildDescription(guild);
    },
    getGuildRoster: function(guild)
    {
        return getGuildRoster(guild);
    },
    getGuildDefaultRaidEffort: async function({ wookieeBotGuildId: wookieeBotGuildId, comlinkGuildId: comlinkGuildId, allycode: allycode })
    {
        let guildData;
        if (wookieeBotGuildId)
        {
            guildData = await database.db.oneOrNone("SELECT raid_effort_default FROM guild WHERE guild_id = $1", [wookieeBotGuildId]);
        }
        else if (comlinkGuildId)
        {
            guildData = await database.db.oneOrNone("SELECT raid_effort_default FROM guild WHERE comlink_guild_id = $1", [comlinkGuildId]);
        }
        else if (allycode)
        {
            guildData = await database.db.oneOrNone("SELECT raid_effort_default FROM guild WHERE guild_id = (SELECT guild_id FROM guild_players WHERE allycode = $1)", [allycode]);
        }

        if (!guildData) return null;
        return guildData.raid_effort_default;
    },
    isGuildSubscribed: async function({ wookieeBotGuildId, comlinkGuildId})
    {
        let guildInfo;
        if (wookieeBotGuildId)
            guildInfo = await database.db.any(`SELECT priority FROM guild WHERE guild_id = $1`, [wookieeBotGuildId]);
        else if (comlinkGuildId)
            guildInfo = await database.db.any(`SELECT priority FROM guild WHERE comlink_guild_id = $1`, [comlinkGuildId]);
        else
            return false;

        return (guildInfo.length > 0) && (guildInfo.priority > 0);
    },
    isInGuild: async function(allycode, wookieeBotGuildId)
    {
        let match = await database.db.oneOrNone("SELECT player_name FROM guild_players where guild_id = $1 and allycode = $2", [wookieeBotGuildId, allycode]);
        if (!match)
            return false;

        return match.player_name;
    }
}

async function captureTWMatchups(guild)
{
    if (!guild.recentTerritoryWarResult || guild.recentTerritoryWarResult.length === 0) return;

    let twInsertData = guild.recentTerritoryWarResult.map(wr => {
        return {
            comlink_guild_id: swapi.getGuildId(guild),
            guild_name: swapi.getGuildName(guild),
            end_time_seconds: parseInt(wr.endTimeSeconds),
            score: parseInt(wr.score),
            opponent_score: parseInt(wr.opponentScore),
            power: wr.power,
            territory_war_id: wr.territoryWarId
        }
    });

    const warResultInsert = database.pgp.helpers.insert(twInsertData, database.columnSets.warResultCS);
    
    const warResultOnConflict = ' ON CONFLICT(comlink_guild_id, end_time_seconds) DO NOTHING';

    await database.db.none(warResultInsert + warResultOnConflict);
}

async function captureRaidPerformance(guild)
{
    // make sure the guild has done a raid
    if (guild.recentRaidResult == null || guild.recentRaidResult.length === 0) return;

    let guildRaidScoreInsertData = guild.recentRaidResult.map(rrr => {
        return {
            comlink_guild_id: swapi.getGuildId(guild),
            raid_id: rrr.raidId,
            end_time_seconds: parseInt(rrr.endTime),
            outcome: rrr.outcome,
            progress: rrr.progress,
            campaign_mission_id: rrr.identifier.campaignMissionId,
            campaign_node_difficulty: rrr.identifier.campaignNodeDifficulty
        };
    });

    let lastRaid;
    for (let raid of guildRaidScoreInsertData)
    {
        if (raid.end_time_seconds > (lastRaid?.end_time_seconds ?? 0)) lastRaid = raid;
    }

    lastRaid = guild.recentRaidResult.find(r => r.raidId === lastRaid.raid_id);

    // lastRaid has the API data for the raid
    let playerRaidScoresInsertData = lastRaid.raidMember.map(rm => {
        return {
            comlink_guild_id: swapi.getGuildId(guild),
            raid_id: lastRaid.raidId,
            end_time_seconds: parseInt(lastRaid.endTime),
            player_id: rm.playerId,
            score: parseInt(rm.memberProgress)
        };
    });
    
    const guildRaidScoreInsert = database.pgp.helpers.insert(guildRaidScoreInsertData, database.columnSets.guildRaidScoreCS);
    
    const guildRaidScoreOnConflict = ' ON CONFLICT(comlink_guild_id, raid_id, end_time_seconds, campaign_mission_id) DO NOTHING';

    await database.db.none(guildRaidScoreInsert + guildRaidScoreOnConflict);
    
    if (playerRaidScoresInsertData.length > 0)
    {
        const playerRaidScoreInsert = database.pgp.helpers.insert(playerRaidScoresInsertData, database.columnSets.playerRaidScoreCS);
        
        const playerRaidScoreOnConflict = ' ON CONFLICT(player_id, comlink_guild_id, end_time_seconds, raid_id) DO NOTHING';

        await database.db.none(playerRaidScoreInsert + playerRaidScoreOnConflict);
    }
}

function getGuildAllycodesForAPIGuild(guild)
{
    if (!guild) return null;

    return swapi.getGuildRoster(guild)?.map(r => swapi.getPlayerAllycode(r));
}

async function cacheGuildData(guild)
{    
    await database.db.any(
        `INSERT INTO guild_info_cache (guild_info_json, comlink_guild_id, allycodes, timestamp) 
        VALUES ($1, $2, $3, $4)
        ON CONFLICT (comlink_guild_id)
        DO UPDATE SET guild_info_json = $1, allycodes = $3, timestamp = $4`,
        [JSON.stringify(guild), swapi.getGuildId(guild), getGuildAllycodesForAPIGuild(guild), new Date()])
}

async function parseMembersAndUpdateCacheIfNeeded(guild)
{
    // see if there are any guildmembers that weren't pulled correctly
    if (guild && await resolveNonParsedGuildMembers(guild))
    {
        await cacheGuildData(guild);
    }
}

async function resolveNonParsedGuildMembers(guild)
{
    if (!guild) return false;

    // see if there are any guildmembers that weren't pulled correctly
    let nonParsedGuildMembers = swapi.getGuildRoster(guild).filter(m => swapi.getPlayerAllycode(m) == null);
    let promises = new Array();
    for (var m of nonParsedGuildMembers)
    {
        promises.push(fetchAndUpdateGuildMember(m));
    }
    
    await Promise.all(promises);
    
    if (nonParsedGuildMembers.length > 0) return true;
    return false;
}

async function fetchAndUpdateGuildMember(member)
{
    let player = await comlink.getPlayerArenaProfileByPlayerId(member.playerId);
//    let player = await swapi.getPlayer(member.playerId)
    member.name = swapi.getPlayerName(player);
    member.allyCode = swapi.getPlayerAllycode(player);
    member.playerRating = { playerRankStatus: null, playerSkillRating: null };
    if (player.playerRating.playerRankStatus)
    {
        member.playerRating.playerRankStatus = {
                divisionId: player.playerRating.playerRankStatus.divisionId,
                leagueId: player.playerRating.playerRankStatus.leagueId
            };
        member.playerRating.playerSkillRating = 
            {
                skillRating: player.playerRating.playerSkillRating.skillRating
            };
    };
//   Node 18+: structuredClone(player.playerRating);
    // member.gp = swapi.getGP(player);
    // member.gpChar = swapi.getGPSquad(player);
    // member.gpFleet = swapi.getGPFleet(player); 
}

function getGuildRoster(guild)
{
    return swapi.getGuildRoster(guild);
}

function getGuildPlayersInsertData(guild, guildId)
{
    var guildPlayersInsertData = new Array();
    let now = new Date();

    let roster = swapi.getGuildRoster(guild);
    
    for (var m = 0; m < roster.length; m++)
    {
        let member = roster[m];
        guildPlayersInsertData.push({
            guild_id: guildId,
            player_name: member.name,
            allycode: member.allyCode,
            member_type: constants.MEMBER_TYPE_MAP[swapi.getPlayerGuildMemberLevel(member)] ?? "NONE",
            player_id: member.playerId,
            guild_join_time: parseInt(member.guildJoinTime),
            last_checked_dt: now
        });
    }

    return guildPlayersInsertData;

}