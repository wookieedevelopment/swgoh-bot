const { createCanvas, loadImage } = require('canvas');
const NodeCache = require('node-cache');
const database = require("../database");

const PORTRAIT_IMAGE_CACHE = new NodeCache({ useClones: false });

const IMAGES_DATA = new NodeCache({ useClones: false });

const IMAGES = {
    SMALL_CIRCLE: 1,
    CHECKBOX_EMPTY: 2,
    CHECKBOX_X: 3,
    CHECKBOX_CHECKED: 4,
    NOTEPAD: 5,
    NO_PORTRAIT: 6,
    BG_REC: 7,
    MOD_SPOTS: {
        ARROW: 8,
        CIRCLE: 9,
        CROSS: 10,
        DIAMOND: 11,
        SQUARE: 12,
        TRIANGLE: 13
    },
    MOD_SETS: {
        HEALTH: 14,
        POTENCY: 15,
        TENACITY: 16,
        SPEED: 17,
        DEFENSE: 18,
        CRITICAL_DAMAGE: 19,
        CRITICAL_CHANCE: 20,
        OFFENSE: 21
    },
    WAT: 22,
    KAM: 23,
    REVA: 24
};

module.exports = {
    IMAGES: IMAGES,
    setupImageCache: async function()
    {
        IMAGES_DATA.set(IMAGES.SMALL_CIRCLE, await loadImage('./src/img/dc/datacron-ring-thin.png'));
        IMAGES_DATA.set(IMAGES.CHECKBOX_EMPTY, await loadImage('./src/img/checkbox-empty.png'));
        IMAGES_DATA.set(IMAGES.CHECKBOX_CHECKED, await loadImage('./src/img/checkbox-checked.png'));
        IMAGES_DATA.set(IMAGES.CHECKBOX_X, await loadImage('./src/img/checkbox-x.png'));
        IMAGES_DATA.set(IMAGES.NOTEPAD, await loadImage('./src/img/notepad.png'));
        IMAGES_DATA.set(IMAGES.NO_PORTRAIT, await loadImage('./src/img/no-portrait.png'));
        IMAGES_DATA.set(IMAGES.BG_REC, await loadImage('./src/img/rec-bg.png'));
    
        IMAGES_DATA.set(IMAGES.MOD_SETS.CRITICAL_CHANCE, await loadImage('./src/img/mods/icon_buff_crit_chance.png'));
        IMAGES_DATA.set(IMAGES.MOD_SETS.CRITICAL_DAMAGE, await loadImage('./src/img/mods/icon_buff_critical_damage.png'));
        IMAGES_DATA.set(IMAGES.MOD_SETS.DEFENSE, await loadImage('./src/img/mods/icon_buff_defense.png'));
        IMAGES_DATA.set(IMAGES.MOD_SETS.HEALTH, await loadImage('./src/img/mods/icon_buff_health.png'));
        IMAGES_DATA.set(IMAGES.MOD_SETS.OFFENSE, await loadImage('./src/img/mods/icon_buff_offense.png'));
        IMAGES_DATA.set(IMAGES.MOD_SETS.POTENCY, await loadImage('./src/img/mods/icon_buff_potency.png'));
        IMAGES_DATA.set(IMAGES.MOD_SETS.SPEED, await loadImage('./src/img/mods/icon_buff_speed.png'));
        IMAGES_DATA.set(IMAGES.MOD_SETS.TENACITY, await loadImage('./src/img/mods/icon_buff_tenacity.png'));
        
        IMAGES_DATA.set(IMAGES.MOD_SPOTS.ARROW, await loadImage('./src/img/mods/tex.statmodslot_arrow.png'));
        IMAGES_DATA.set(IMAGES.MOD_SPOTS.CIRCLE, await loadImage('./src/img/mods/tex.statmodslot_circle.png'));
        IMAGES_DATA.set(IMAGES.MOD_SPOTS.CROSS, await loadImage('./src/img/mods/tex.statmodslot_cross.png'));
        IMAGES_DATA.set(IMAGES.MOD_SPOTS.DIAMOND, await loadImage('./src/img/mods/tex.statmodslot_diamond.png'));
        IMAGES_DATA.set(IMAGES.MOD_SPOTS.SQUARE, await loadImage('./src/img/mods/tex.statmodslot_square.png'));
        IMAGES_DATA.set(IMAGES.MOD_SPOTS.TRIANGLE, await loadImage('./src/img/mods/tex.statmodslot_triangle.png'));
        IMAGES_DATA.set(IMAGES.WAT, await loadImage('./src/img/Wat_Tambor.png'));
        IMAGES_DATA.set(IMAGES.KAM, await loadImage('./src/img/Ki-Adi-Mundi.png'));
        IMAGES_DATA.set(IMAGES.REVA, await loadImage('./src/img/Reva.png'));
        let portraits = await database.db.any("SELECT unit_def_id,img FROM unit_portrait");
        for (let p of portraits)
        {
            let portrait = await loadImage(p.img);
            PORTRAIT_IMAGE_CACHE.set(p.unit_def_id, portrait);
        }
    },
    getImage: function (imageKey)
    {
        return IMAGES_DATA.get(imageKey);
    },
    getLoadedPortraitImage: function(unitDefId)
    {
        return PORTRAIT_IMAGE_CACHE.get(unitDefId) ?? IMAGES_DATA.get(IMAGES.NO_PORTRAIT);
    },
    drawText: function(context, text, x, y, 
        { 
            maxWidth = undefined, 
            fontSize = "20pt",
            fontFace = `"Noto Sans","Noto Sans SC","Noto Sans Armenian","Noto Sans Thai","Noto Sans Arabic","Noto Sans Korean"`, 
            align = "left",
            baseline = "middle", 
            fillStyle = "#ffff"
        } = {})
    {
        context.font = `${fontSize} ${fontFace}`;
        context.textAlign = align;
        context.textBaseline = baseline;
        context.fillStyle = fillStyle;
    
        context.fillText(text, x, y, maxWidth);
    },
    loadAndDrawImage: async function(context, imageSource, x, y, width, height)
    {
        // Load the image
        const image = await loadImage(imageSource);
        // Draw the image on the canvas
        context.drawImage(image, x, y, width, height);
    },
    createBlankCanvas: function(width, height, bgColor)
    {
        const canvas = createCanvas(width, height);
        const context = canvas.getContext('2d');
        
        context.fillStyle = bgColor;
        context.fillRect(0, 0, width, height);
    
        return { canvas, context };
    }
}