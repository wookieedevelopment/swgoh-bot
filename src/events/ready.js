const { logger } = require("../utils/log");
const queueUtils = require ("../utils/queue");
const statCalc = require("../utils/stats/stats");
const drawingUtils = require("../utils/drawing");
const entitlements = require("../utils/entitlements");

module.exports = {
	name: 'ready',
	once: true,
	async execute(client) {
		await statCalc.initCalc();
		logger.info(`Shard ${client.shard.ids[0]}: Initialized Stat Calc`);

		try {
			await drawingUtils.setupImageCache();
			logger.info(`Preloaded images.`)
		} catch (e)
		{
			logger.error(`Error preloading images: ${e}`)
		}

		await queueUtils.start(client.shard)
		logger.info(`Shard ${client.shard.ids[0]}: Started queue.`);

		await entitlements.startPollingForEntitlements();
	},
};