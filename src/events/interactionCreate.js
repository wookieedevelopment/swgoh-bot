const twManagerInteract = require("../commands/tw/twManager/interact")
const showAssignmentDetails = require ("../commands/tw/showAssignmentDetails")
const twPlanReport = require("../commands/tw/planReport")
const gacCalc = require("../commands/gac/calc")
const twManager = require("../commands/twManager")
const tw = require("../commands/tw");
const scout = require("../commands/scout");
const tb = require("../commands/tb");
const krayt = require("../commands/krayt");
const counters = require("../commands/counters")
const { logger, formatError } = require("../utils/log");
const botUsage = require("../utils/botUsage");
const cheatingGuild = require("../commands/cheating/guild");
const cheatingSearch = require("../commands/cheating/search");
const { InteractionType, MessageFlags } = require("discord.js");
const raid = require("../commands/raid")
const guild = require("../commands/guild")
const ops = require("../commands/ops");


module.exports = {
	name: 'interactionCreate',
	async execute(interaction) {
        try {
            let startDate = botUsage.startActivity(interaction);
            
            if (interaction.isMessageContextMenuCommand())
            {
                await this.handleMessageContextMenu(interaction);
            }
            else if (interaction.type === InteractionType.ApplicationCommandAutocomplete)
            {
                await this.handleAutocomplete(interaction);
            }
            else if (interaction.type === InteractionType.ApplicationCommand)
            {
                await this.handleCommand(interaction);
            }
            else if (interaction.type === InteractionType.ModalSubmit)
            {
                await this.handleModal(interaction);
            }
            else if (interaction.isStringSelectMenu())
            {
                await this.handleSelectMenu(interaction);
            }
            else if (interaction.isButton())
            {
                await this.handleButton(interaction);
            }
            
            botUsage.finishActivity(interaction, startDate);                    
        } catch (error) {
            logger.error(formatError(error));
            try {
                await interaction.editReply(`${error.message ?? "Huac ooac. ('Uh oh.')."}${error.description ? `\r\n${error.description}` : ""}`);
            } catch { /* Nothing to do here */ }
        }
	},
    handleMessageContextMenu: async function(interaction)
    {
        await interaction.deferReply();
        let i = interaction.client.commands.get(interaction.commandName);
        if (i && i.interact) await i.interact(interaction);
    },
    async handleCommand(interaction)
    {
        let ephemeral = false;

        try {
            ephemeral = interaction.commandName == "help" || 
                        interaction.commandName == "invite" || 
                        interaction.commandName == "meme" ||
                        interaction.commandName == "register" ||
                        (interaction.commandName == "user" && interaction.options.getSubcommand() == "me") ||
                        (interaction.commandName == guild.data.name && guild.isEphemeral(interaction.options.getSubcommand())) ||
                        (interaction.commandName == tb.data.name && tb.isEphemeral(interaction)) ||
                        (interaction.commandName == counters.data.name && counters.isEphemeral(interaction.options.getSubcommand()));
            if (!ephemeral) ephemeral = interaction.options.getSubcommand() == "help";
        } catch {} // no subcommand

        let defer = true;
        let i = interaction.client.commands.get(interaction.commandName);
        if (i.shouldDefer)
        {
            defer = i.shouldDefer(interaction);
        }

        if (defer)
        {
            let reply = {};
            if (ephemeral) reply.flags = MessageFlags.Ephemeral;
            await interaction.deferReply(reply);
        }


        if (i && i.interact) await i.interact(interaction);
    },
    async handleButton(interaction)
    {

        if (interaction.customId.startsWith("tw_def"))
        {
            await tw.handleButton(interaction);
            return;
        }

        if (interaction.customId == "another_dad_joke")
        {
            await interaction.deferReply();
            let i = interaction.client.commands.get("dadjoke");
            await i.handleButton(interaction);
            return;
        }

        if (interaction.customId == "another_fact")
        {
            await interaction.deferReply();
            let i = interaction.client.commands.get("fact");
            await i.handleButton(interaction);
            return;
        }

        if (interaction.customId.startsWith("ops"))
        {
            await interaction.deferUpdate();
            await ops.handleButton(interaction);
            return;
        }

        if (interaction.customId.startsWith("tb"))
        {
            await interaction.deferReply();
            await tb.handleButton(interaction);
            return;
        }

        if (interaction.customId.startsWith("cheating.guild"))
        {
            await cheatingGuild.handleButton(interaction);
            return;
        }

        if (interaction.customId.startsWith("kr"))
        {
            await interaction.deferReply();
            await krayt.handleButton(interaction);
            return;
        }

        if (interaction.customId.startsWith("raid"))
        {
            await raid.handleButton(interaction);
            return;
        }

        if (interaction.customId.startsWith("guild"))
        {
            await guild.handleButton(interaction);
            return;
        }

        if (interaction.message && interaction.message.interaction && interaction.message.interaction.commandName)
        {
            let i = interaction.client.commands.get(interaction.message.interaction.commandName);
            if (i && i.handleButton) await i.handleButton(interaction);
            return;
        }
        
        await twManagerInteract.onButtonClick(interaction);
    },
    async handleSelectMenu(interaction)
    {
        if (interaction.customId.startsWith("tw"))
        {
            await tw.onSelect(interaction);
            return;
        }
        if (interaction.customId.startsWith("ops"))
        {
            await ops.onSelect(interaction);
            return;
        }
        if (interaction.customId.startsWith("gac-calc"))
        {
            await gacCalc.onSelect(interaction);
            return;
        } 
        
        if (interaction.customId.startsWith("cheating.guild"))
        {
            await cheatingGuild.onSelect(interaction);
            return;
        }

        if (interaction.customId.startsWith("cheating.search"))
        {
            await cheatingSearch.onSelect(interaction);
            return;
        }

        if (interaction.customId.startsWith("tb"))
        {
            await tb.onSelect(interaction);
            return;
        }

        if (interaction.customId.startsWith("kr"))
        {
            await krayt.onSelect(interaction);
            return;
        }

        if (interaction.customId.startsWith("raid"))
        {
            await raid.onSelect(interaction);
            return;
        }

        if (interaction.customId.startsWith("guild"))
        {
            await guild.onSelect(interaction);
            return;
        }

        
        if (interaction.customId.startsWith("scout"))
        {
            await scout.onSelect(interaction);
            return;
        }
    },
    async handleAutocomplete(interaction)
    {
        let i = interaction.client.commands.get(interaction.commandName);
        if (i && i.autocomplete) await i.autocomplete(interaction);
    },
    async handleModal(interaction)
    {
        if (interaction.customId.startsWith("twmanager"))
        {
            await twManager.handleModal(interaction);
        } 
        else if (interaction.customId.startsWith("counters"))
        {
            await counters.handleModal(interaction);
        }
        else if (interaction.customId.startsWith("tw"))
        {
            await tw.handleModal(interaction);
        }
    }
};