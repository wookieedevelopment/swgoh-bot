#!/bin/bash

# Stop and remove existing containers
docker stop swgoh-comlink
docker rm swgoh-comlink

docker stop swgoh-stats
docker rm swgoh-stats

# Pull Docker images
docker pull ghcr.io/swgoh-utils/swgoh-comlink:latest
docker pull ghcr.io/swgoh-utils/swgoh-stats:latest
docker pull ghcr.io/swgoh-utils/swgoh-ae2:latest
# Create network if it doesn't exist
docker network create --driver bridge --attachable swgoh-comlink 2>/dev/null || true

# Run swgoh-comlink container
docker run --name swgoh-comlink -d --restart always --network swgoh-comlink --env APP_NAME=wookieebot -p 3200:3000 ghcr.io/swgoh-utils/swgoh-comlink:latest

# Run swgoh-stats container
docker run --name swgoh-stats -d --restart always --network swgoh-comlink --env CLIENT_URL=http://swgoh-comlink:3000/ -p 3223:3223 -v "$(pwd)/statcalcdata:/app/statcalcdata" ghcr.io/swgoh-utils/swgoh-stats:latest


docker run --name=swgoh-ae2 -d --restart always -p 3000:80 ghcr.io/swgoh-utils/swgoh-ae2:latest
