# wookieeBot

:gear: [Requisites](#requisites)
:bear: [Run wookieeBot](#run-wookieebot)

# Requisites
||req||version||
|node|18.16.1| 
|npm|9.5.1|
|py|3.10.4|

# Run wookieeBot
`./scripts/wookieeBot.sh`
npm run wookieeBot

<!-- 
Short description or tagline for your project.

## Description

Provide a detailed description of your project. Explain its purpose, features, and any relevant information that users or contributors may need to know. Include information about the technologies used, dependencies, and any setup or installation instructions.

## Table of Contents

- [Installation](#installation)
- [Usage](#usage)
- [Contributing](#contributing)
- [License](#license)

## Installation

Provide step-by-step instructions on how to install and set up your project. Include any dependencies that need to be installed, configuration steps, and any other necessary instructions.

## Usage

Explain how to use your project. Provide examples, code snippets, or screenshots to demonstrate its functionality. Include any relevant details or instructions that users may need to interact with or customize your project.

## Contributing

Explain how others can contribute to your project. Provide guidelines for reporting issues, suggesting improvements, or submitting pull requests. Include any code formatting or style conventions you expect contributors to follow.

## License

Include information about the license under which your project is released. State the license type and provide a link to the license file if applicable.

## Contact

Provide contact information, such as your email or any other preferred method for users or contributors to reach out to you with questions, feedback, or collaboration opportunities.
 -->
