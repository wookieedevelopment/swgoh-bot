-- Column: public.guild.raid_effort_default

-- ALTER TABLE IF EXISTS public.guild DROP COLUMN IF EXISTS raid_effort_default;

ALTER TABLE IF EXISTS public.guild
    ADD COLUMN raid_effort_default smallint NOT NULL DEFAULT 2;