-- Column: public.player_data_static.gl_leia

-- ALTER TABLE IF EXISTS public.player_data_static DROP COLUMN IF EXISTS gl_leia;

ALTER TABLE IF EXISTS public.player_data_static
    ADD COLUMN gl_leia boolean NOT NULL DEFAULT false;