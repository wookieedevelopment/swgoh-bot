-- Column: public.tw_plan.max_defense_priority

-- ALTER TABLE IF EXISTS public.tw_plan DROP COLUMN IF EXISTS max_defense_priority;

ALTER TABLE IF EXISTS public.tw_plan
    ADD COLUMN max_defense_priority smallint;