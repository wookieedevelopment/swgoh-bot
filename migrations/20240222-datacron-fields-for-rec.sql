-- Column: public.datacron_template.color_text

-- ALTER TABLE IF EXISTS public.datacron_template DROP COLUMN IF EXISTS color_text;

ALTER TABLE IF EXISTS public.datacron_template
    ADD COLUMN color_text character varying COLLATE pg_catalog."default";

-- Column: public.datacron_template.color_code

-- ALTER TABLE IF EXISTS public.datacron_template DROP COLUMN IF EXISTS color_code;

ALTER TABLE IF EXISTS public.datacron_template
    ADD COLUMN color_code character varying COLLATE pg_catalog."default";

-- Column: public.datacron.mini_description

-- ALTER TABLE IF EXISTS public.datacron DROP COLUMN IF EXISTS mini_description;

ALTER TABLE IF EXISTS public.datacron
    ADD COLUMN mini_description character varying(20) COLLATE pg_catalog."default";

-- Column: public.guild_recommendation.data

-- ALTER TABLE IF EXISTS public.guild_recommendation DROP COLUMN IF EXISTS data;

ALTER TABLE IF EXISTS public.guild_recommendation
    ADD COLUMN data jsonb;