-- Column: public.cache_data.data_text

-- ALTER TABLE IF EXISTS public.cache_data DROP COLUMN IF EXISTS data_text;

ALTER TABLE IF EXISTS public.cache_data
    ADD COLUMN data_text text COLLATE pg_catalog."default";

ALTER TABLE public.cache_data DROP COLUMN data_json;