-- Index: ix_player_data_allycode_timestamp

-- DROP INDEX IF EXISTS public.ix_player_data_allycode_timestamp;

CREATE INDEX IF NOT EXISTS ix_player_data_allycode_timestamp
    ON public.player_data USING btree
    (allycode COLLATE pg_catalog."default" ASC NULLS LAST, "timestamp" DESC NULLS FIRST)
    TABLESPACE pg_default;

-- Index: ix_player_roster_allycode

-- DROP INDEX IF EXISTS public.ix_player_roster_allycode;

CREATE INDEX IF NOT EXISTS ix_player_roster_allycode
    ON public.player_roster USING btree
    (allycode COLLATE pg_catalog."default" ASC NULLS LAST)
    TABLESPACE pg_default;
