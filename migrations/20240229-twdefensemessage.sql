-- Column: public.tw_metadata.defense_message

-- ALTER TABLE IF EXISTS public.tw_metadata DROP COLUMN IF EXISTS defense_message;

ALTER TABLE IF EXISTS public.tw_metadata
    ADD COLUMN defense_message character varying COLLATE pg_catalog."default";