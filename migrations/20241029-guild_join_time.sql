-- Column: public.guild_players.guild_join_time

-- ALTER TABLE IF EXISTS public.guild_players DROP COLUMN IF EXISTS guild_join_time;

ALTER TABLE IF EXISTS public.guild_players
    ADD COLUMN guild_join_time integer;