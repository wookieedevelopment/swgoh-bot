-- Column: public.raid_team.zetas_required

-- ALTER TABLE IF EXISTS public.raid_team DROP COLUMN IF EXISTS zetas_required;

ALTER TABLE IF EXISTS public.raid_team
    ADD COLUMN zetas_required character varying[] COLLATE pg_catalog."default";