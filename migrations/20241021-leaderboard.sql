-- Table: public.leaderboard

-- DROP TABLE IF EXISTS public.leaderboard;

CREATE TABLE IF NOT EXISTS public.leaderboard
(
    leaderboard_id character varying COLLATE pg_catalog."default" NOT NULL,
    enabled boolean NOT NULL DEFAULT true,
    description character varying COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT leaderboard_pkey PRIMARY KEY (leaderboard_id)
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.leaderboard
    OWNER to wookiee500;

-- Table: public.leaderboard_data

-- DROP TABLE IF EXISTS public.leaderboard_data;

CREATE TABLE IF NOT EXISTS public.leaderboard_data
(
    leaderboard_data_id integer NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1 ),
    leaderboard_id character varying COLLATE pg_catalog."default" NOT NULL,
    key character varying COLLATE pg_catalog."default" NOT NULL,
    data jsonb,
    rank integer,
    CONSTRAINT leaderboard_data_pkey PRIMARY KEY (leaderboard_data_id)
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.leaderboard_data
    OWNER to wookiee500;
-- Index: ix_leaderboard_data_leaderboard_id

-- DROP INDEX IF EXISTS public.ix_leaderboard_data_leaderboard_id;

CREATE INDEX IF NOT EXISTS ix_leaderboard_data_leaderboard_id
    ON public.leaderboard_data USING btree
    (leaderboard_id COLLATE pg_catalog."default" ASC NULLS LAST)
    TABLESPACE pg_default;

-- Constraint: leaderboard_data_leaderboard_id_key_key

-- ALTER TABLE IF EXISTS public.leaderboard_data DROP CONSTRAINT IF EXISTS leaderboard_data_leaderboard_id_key_key;

ALTER TABLE IF EXISTS public.leaderboard_data
    ADD CONSTRAINT leaderboard_data_leaderboard_id_key_key UNIQUE (leaderboard_id, key);
