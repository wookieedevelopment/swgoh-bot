-- Column: public.guild_tb_config.post_assignments_on_send2

-- ALTER TABLE IF EXISTS public.guild_tb_config DROP COLUMN IF EXISTS post_assignments_on_send2;

ALTER TABLE IF EXISTS public.guild_tb_config
    ADD COLUMN post_assignments_on_send2 smallint NOT NULL DEFAULT 0;
	
UPDATE guild_tb_config
SET post_assignments_on_send2 = 3
WHERE post_assignments_on_send = TRUE;

alter table guild_tb_config
DROP column post_assignments_on_send;

ALTER TABLE guild_tb_config
RENAME COLUMN post_assignments_on_send2 TO post_assignments_on_send;