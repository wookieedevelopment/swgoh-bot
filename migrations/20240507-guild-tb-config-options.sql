-- Column: public.guild_tb_config.assignments_channel

-- ALTER TABLE IF EXISTS public.guild_tb_config DROP COLUMN IF EXISTS assignments_channel;

ALTER TABLE IF EXISTS public.guild_tb_config
    ADD COLUMN assignments_channel character varying COLLATE pg_catalog."default";

-- Column: public.guild_tb_config.post_assignments_on_send

-- ALTER TABLE IF EXISTS public.guild_tb_config DROP COLUMN IF EXISTS post_assignments_on_send;

ALTER TABLE IF EXISTS public.guild_tb_config
    ADD COLUMN post_assignments_on_send boolean NOT NULL DEFAULT false;