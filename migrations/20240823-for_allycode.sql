ALTER TABLE IF EXISTS public.tw_plan_saved
    ADD COLUMN for_allycode character varying COLLATE pg_catalog."default";

ALTER TABLE IF EXISTS public.tw_plan
    ADD COLUMN for_allycode character varying COLLATE pg_catalog."default";