-- Column: public.raid_team.tier_data

-- ALTER TABLE IF EXISTS public.raid_team DROP COLUMN IF EXISTS tier_data;

ALTER TABLE IF EXISTS public.raid_team
    ADD COLUMN tier_data jsonb[];

ALTER TABLE IF EXISTS public.raid_team
    ADD COLUMN data_points jsonb[];