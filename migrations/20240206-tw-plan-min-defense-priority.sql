-- Column: public.tw_plan.min_defense_priority

-- ALTER TABLE IF EXISTS public.tw_plan DROP COLUMN IF EXISTS min_defense_priority;

ALTER TABLE IF EXISTS public.tw_plan
    ADD COLUMN min_defense_priority smallint;

-- Column: public.tw_plan_saved.max_defense_priority

-- ALTER TABLE IF EXISTS public.tw_plan_saved DROP COLUMN IF EXISTS max_defense_priority;

ALTER TABLE IF EXISTS public.tw_plan_saved
    ADD COLUMN max_defense_priority smallint;

-- Column: public.tw_plan_saved.min_defense_priority

-- ALTER TABLE IF EXISTS public.tw_plan_saved DROP COLUMN IF EXISTS min_defense_priority;

ALTER TABLE IF EXISTS public.tw_plan_saved
    ADD COLUMN min_defense_priority smallint;