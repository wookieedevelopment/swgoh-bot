-- Column: public.tw_team_unit.logic

-- ALTER TABLE IF EXISTS public.tw_team_unit DROP COLUMN IF EXISTS logic;

ALTER TABLE IF EXISTS public.tw_team_unit
    ADD COLUMN logic character varying COLLATE pg_catalog."default";