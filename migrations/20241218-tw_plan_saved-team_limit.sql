-- Column: public.tw_plan.team_limit

-- ALTER TABLE IF EXISTS public.tw_plan DROP COLUMN IF EXISTS team_limit;

ALTER TABLE IF EXISTS public.tw_plan_saved
    ADD COLUMN team_limit integer;