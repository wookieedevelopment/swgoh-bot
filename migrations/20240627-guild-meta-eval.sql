-- Add the new column: public.guild.auto_eval_players if it does not exist
DO $$
BEGIN
    IF NOT EXISTS (
        SELECT 1
        FROM information_schema.columns
        WHERE table_name = 'guild'
        AND column_name = 'auto_eval_players'
    ) THEN
        ALTER TABLE public.guild
            ADD COLUMN auto_eval_players boolean DEFAULT false;
    ELSE
        ALTER TABLE public.guild
            ALTER COLUMN auto_eval_players SET DEFAULT false;
    END IF;
END $$;
