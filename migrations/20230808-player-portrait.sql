-- Table: public.player_portrait

-- DROP TABLE IF EXISTS public.player_portrait;

CREATE TABLE IF NOT EXISTS public.player_portrait
(
    player_portrait_id character varying COLLATE pg_catalog."default" NOT NULL,
    img bytea NOT NULL,
    CONSTRAINT player_portrait_pkey PRIMARY KEY (player_portrait_id)
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.player_portrait
    OWNER to wookiee500;