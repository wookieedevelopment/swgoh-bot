-- Table: public.guild_units_count_cache

-- DROP TABLE IF EXISTS public.guild_units_count_cache;

CREATE TABLE IF NOT EXISTS public.guild_units_count_cache
(
    guild_units_count_cache_id integer NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1 ),
    data jsonb NOT NULL,
    "timestamp" timestamp with time zone NOT NULL,
    comlink_guild_id character varying COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT guild_rares_cache_pkey PRIMARY KEY (guild_units_count_cache_id),
    CONSTRAINT guild_units_count_cache_comlink_guild_id_key UNIQUE (comlink_guild_id)
)

TABLESPACE pg_default;