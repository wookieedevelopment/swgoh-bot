-- Column: public.tw_team.portrait

-- ALTER TABLE IF EXISTS public.tw_team DROP COLUMN IF EXISTS portrait;

ALTER TABLE IF EXISTS public.tw_team
    ADD COLUMN portrait character varying COLLATE pg_catalog."default";