-- Column: public.player_data.tb_special_income

-- ALTER TABLE IF EXISTS public.player_data DROP COLUMN IF EXISTS tb_special_income;

ALTER TABLE IF EXISTS public.player_data
    ADD COLUMN tb_special_income jsonb;