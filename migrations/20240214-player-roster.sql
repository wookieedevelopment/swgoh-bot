-- Table: public.player_roster

-- DROP TABLE IF EXISTS public.player_roster;

CREATE TABLE IF NOT EXISTS public.player_roster
(
    player_roster_id integer NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1 ),
    allycode character varying COLLATE pg_catalog."default" NOT NULL,
    unit_def_id character varying COLLATE pg_catalog."default" NOT NULL,
    relic_level integer,
    gear_level integer,
    rarity integer,
    "timestamp" timestamp with time zone NOT NULL DEFAULT now(),
    CONSTRAINT player_roster_pkey PRIMARY KEY (player_roster_id),
    CONSTRAINT player_roster_allycode_unit_def_id_key UNIQUE (allycode, unit_def_id)
)

-- Index: ix_player_roster_allycode_unit_def_id

-- DROP INDEX IF EXISTS public.ix_player_roster_allycode_unit_def_id;

CREATE INDEX IF NOT EXISTS ix_player_roster_allycode_unit_def_id
    ON public.player_roster USING btree
    (allycode COLLATE pg_catalog."default" ASC NULLS LAST, unit_def_id COLLATE pg_catalog."default" ASC NULLS LAST)
    INCLUDE(allycode, unit_def_id)
    TABLESPACE pg_default;