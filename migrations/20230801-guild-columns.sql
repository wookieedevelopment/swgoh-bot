-- Column: public.guild.fixed_name

-- ALTER TABLE IF EXISTS public.guild DROP COLUMN IF EXISTS fixed_name;

ALTER TABLE IF EXISTS public.guild
    ADD COLUMN fixed_name character varying COLLATE pg_catalog."default";

-- Column: public.guild.logo

-- ALTER TABLE IF EXISTS public.guild DROP COLUMN IF EXISTS logo;

ALTER TABLE IF EXISTS public.guild
    ADD COLUMN logo bytea;