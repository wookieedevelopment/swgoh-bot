-- Table: public.tb_plan

-- DROP TABLE IF EXISTS public.tb_plan;

CREATE TABLE IF NOT EXISTS public.tb_plan
(
    tb_plan_id integer NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1 ),
    comlink_guild_id character varying COLLATE pg_catalog."default" NOT NULL,
    planets_by_phase jsonb,
    plan_name character varying COLLATE pg_catalog."default",
    CONSTRAINT tb_plan_pkey PRIMARY KEY (tb_plan_id),
    CONSTRAINT tb_plan_comlink_guild_id_unique UNIQUE (comlink_guild_id)
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.tb_plan
    OWNER to wookiee500;
-- Index: tb_plan_guild_id_ix

-- DROP INDEX IF EXISTS public.tb_plan_guild_id_ix;

CREATE INDEX IF NOT EXISTS tb_plan_guild_id_ix
    ON public.tb_plan USING btree
    (comlink_guild_id COLLATE pg_catalog."default" ASC NULLS LAST)
    TABLESPACE pg_default;


-- Table: public.guild_tb_config

-- DROP TABLE IF EXISTS public.guild_tb_config;

CREATE TABLE IF NOT EXISTS public.guild_tb_config
(
    guild_tb_config_id integer NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1 ),
    comlink_guild_id character varying COLLATE pg_catalog."default" NOT NULL,
    rare_threshold smallint NOT NULL DEFAULT 5,
    CONSTRAINT guild_tb_config_pkey PRIMARY KEY (guild_tb_config_id),
    CONSTRAINT guild_tb_config_guild_id_unique UNIQUE (comlink_guild_id)
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.guild_tb_config
    OWNER to wookiee500;

-- Table: public.tb_ops

-- DROP TABLE IF EXISTS public.tb_ops;

CREATE TABLE IF NOT EXISTS public.tb_ops
(
    tb_ops_id integer NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1 ),
    comlink_guild_id character varying COLLATE pg_catalog."default" NOT NULL,
    plan_name character varying COLLATE pg_catalog."default" NOT NULL,
    "timestamp" timestamp without time zone NOT NULL,
    phase integer,
    operation_req_id integer NOT NULL,
    preload boolean NOT NULL DEFAULT false,
    allycode character varying COLLATE pg_catalog."default",
    total_needed integer,
    total_available integer,
    CONSTRAINT tb_ops_pkey PRIMARY KEY (tb_ops_id),
    CONSTRAINT tb_ops_comlink_guild_id_phase_operation_req_id_key UNIQUE (comlink_guild_id, phase, operation_req_id)
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.tb_ops
    OWNER to wookiee500;