-- Column: public.guild_players.player_id

-- ALTER TABLE IF EXISTS public.guild_players DROP COLUMN IF EXISTS player_id;

ALTER TABLE IF EXISTS public.guild_players
    ADD COLUMN player_id character varying COLLATE pg_catalog."default";