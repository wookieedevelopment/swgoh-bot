-- Constraint: user_registration_allycode_unique

-- ALTER TABLE IF EXISTS public.user_registration DROP CONSTRAINT IF EXISTS user_registration_allycode_unique;

ALTER TABLE IF EXISTS public.user_registration
    ADD CONSTRAINT user_registration_allycode_unique UNIQUE (allycode);
