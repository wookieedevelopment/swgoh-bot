-- Column: public.guild_players.last_checked_dt

-- ALTER TABLE IF EXISTS public.guild_players DROP COLUMN IF EXISTS last_checked_dt;

ALTER TABLE IF EXISTS public.guild_players
    ADD COLUMN last_checked_dt timestamp with time zone;