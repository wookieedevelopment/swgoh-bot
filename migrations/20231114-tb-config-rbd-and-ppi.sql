-- Column: public.guild_tb_config.rare_by_default

-- ALTER TABLE IF EXISTS public.guild_tb_config DROP COLUMN IF EXISTS rare_by_default;

ALTER TABLE IF EXISTS public.guild_tb_config
    ADD COLUMN rare_by_default boolean NOT NULL DEFAULT false;
    
-- Column: public.guild_tb_config.players_per_image

-- ALTER TABLE IF EXISTS public.guild_tb_config DROP COLUMN IF EXISTS players_per_image;

ALTER TABLE IF EXISTS public.guild_tb_config
    ADD COLUMN players_per_image integer NOT NULL DEFAULT 10;