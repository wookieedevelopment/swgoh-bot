-- Column: public.guild_tb_config.tiebreaker

-- ALTER TABLE IF EXISTS public.guild_tb_config DROP COLUMN IF EXISTS tiebreaker;

ALTER TABLE IF EXISTS public.guild_tb_config
    ADD COLUMN tiebreaker character varying COLLATE pg_catalog."default" DEFAULT 'Random'::character varying;