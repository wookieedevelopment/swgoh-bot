-- Column: public.player_data.datacron_summary

-- ALTER TABLE IF EXISTS public.player_data DROP COLUMN IF EXISTS datacron_summary;

ALTER TABLE IF EXISTS public.player_data
    ADD COLUMN datacron_summary jsonb;