-- Table: public.operation_grouping_rule

-- DROP TABLE IF EXISTS public.operation_grouping_rule;

CREATE TABLE IF NOT EXISTS public.operation_grouping_rule
(
    operation_grouping_rule integer NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1 ),
    match_all character varying[] COLLATE pg_catalog."default",
    match_any character varying[] COLLATE pg_catalog."default",
    keep_all character varying[] COLLATE pg_catalog."default",
    give_all character varying[] COLLATE pg_catalog."default",
    planet character varying COLLATE pg_catalog."default",
    priority integer,
    has jsonb,
    CONSTRAINT operation_grouping_rule_pkey PRIMARY KEY (operation_grouping_rule)
)

TABLESPACE pg_default;