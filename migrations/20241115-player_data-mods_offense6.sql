-- Column: public.player_data.mods_offense6

-- ALTER TABLE IF EXISTS public.player_data DROP COLUMN IF EXISTS mods_offense6;

ALTER TABLE IF EXISTS public.player_data
    ADD COLUMN mods_offense6 integer;