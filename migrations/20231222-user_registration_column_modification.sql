ALTER TABLE IF EXISTS public.user_registration
    DROP COLUMN IF EXISTS player_name,
    ADD COLUMN alt integer ;

WITH numbered_users AS (
    SELECT
        allycode,
        discord_id,
        alt,
        ROW_NUMBER() OVER (PARTITION BY discord_id ORDER BY allycode) - 1 AS offset
    FROM
        public.user_registration
)
UPDATE
    public.user_registration AS ur
SET
    alt = nu.offset
FROM
    numbered_users AS nu
WHERE
    ur.allycode = nu.allycode
    AND ur.discord_id = nu.discord_id;
