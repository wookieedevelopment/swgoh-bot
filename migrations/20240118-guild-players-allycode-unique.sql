-- clear out any invalid entries in development database. There
-- are no duplicate allycodes in production.
delete from guild_players where allycode in (select allycode from (
	select allycode, count(*)
	from guild_players
	group by allycode) a
where count > 1);

-- guild_name is duplicated in guild, no reason for it to be in guild_players
alter table guild_players drop column guild_name;

-- allow guild fields to be nullable
alter table guild_players alter column member_type drop not null;

-- Constraint: guild_players_allycode_unique

-- ALTER TABLE IF EXISTS public.guild_players DROP CONSTRAINT IF EXISTS guild_players_allycode_unique;

ALTER TABLE IF EXISTS public.guild_players
    ADD CONSTRAINT guild_players_allycode_unique UNIQUE (allycode);
