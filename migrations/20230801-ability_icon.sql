-- Table: public.unit_ability_icon

-- DROP TABLE IF EXISTS public.unit_ability_icon;

CREATE TABLE IF NOT EXISTS public.unit_ability_icon
(
    img bytea NOT NULL,
    def_id character varying COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT unit_ability_icon_pkey PRIMARY KEY (def_id)
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.unit_ability_icon
    OWNER to wookiee500;
-- Index: unit_ability_icon_def_id_ix

-- DROP INDEX IF EXISTS public.unit_ability_icon_def_id_ix;

CREATE INDEX IF NOT EXISTS unit_ability_icon_def_id_ix
    ON public.unit_ability_icon USING btree
    (def_id COLLATE pg_catalog."default" ASC NULLS LAST)
    TABLESPACE pg_default;