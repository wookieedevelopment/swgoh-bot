-- Column: public.player_data.last_activity_time

-- ALTER TABLE IF EXISTS public.player_data DROP COLUMN IF EXISTS last_activity_time;

ALTER TABLE IF EXISTS public.player_data
    ADD COLUMN last_activity_time timestamp without time zone;