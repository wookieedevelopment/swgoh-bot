-- Column: public.raid_team.note

-- ALTER TABLE IF EXISTS public.raid_team DROP COLUMN IF EXISTS note;

ALTER TABLE IF EXISTS public.raid_team
    ADD COLUMN note character varying COLLATE pg_catalog."default";

-- Column: public.raid_team."auto-factor"

-- ALTER TABLE IF EXISTS public.raid_team DROP COLUMN IF EXISTS "auto-factor";

ALTER TABLE IF EXISTS public.raid_team
    ADD COLUMN "auto_factor" numeric;

-- Column: public.raid_team.ult_required

-- ALTER TABLE IF EXISTS public.raid_team DROP COLUMN IF EXISTS ult_required;

ALTER TABLE IF EXISTS public.raid_team
    ADD COLUMN ult_required boolean NOT NULL DEFAULT false;