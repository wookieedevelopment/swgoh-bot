-- Index: player_data_timestamp_index

-- DROP INDEX IF EXISTS public.player_data_timestamp_index;

CREATE INDEX IF NOT EXISTS player_data_timestamp_index
    ON public.player_data USING btree
    ("timestamp" ASC NULLS LAST)
    TABLESPACE pg_default;


-- Index: player_data_allycode_index

-- DROP INDEX IF EXISTS public.player_data_allycode_index;

CREATE INDEX IF NOT EXISTS player_data_allycode_index
    ON public.player_data USING btree
    (allycode COLLATE pg_catalog."default" ASC NULLS LAST)
    TABLESPACE pg_default;
