-- Column: public.guild_recommendation_template.essential

-- ALTER TABLE IF EXISTS public.guild_recommendation_template DROP COLUMN IF EXISTS essential;

ALTER TABLE IF EXISTS public.guild_recommendation_template
    ADD COLUMN essential boolean NOT NULL DEFAULT false;

-- Column: public.guild_recommendation.essential

-- ALTER TABLE IF EXISTS public.guild_recommendation DROP COLUMN IF EXISTS essential;

ALTER TABLE IF EXISTS public.guild_recommendation
    ADD COLUMN essential boolean NOT NULL DEFAULT false;