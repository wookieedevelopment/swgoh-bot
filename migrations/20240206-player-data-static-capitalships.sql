-- Column: public.player_data_static.capital_ships

-- ALTER TABLE IF EXISTS public.player_data_static DROP COLUMN IF EXISTS capital_ships;

ALTER TABLE IF EXISTS public.player_data_static
    ADD COLUMN capital_ships character varying[] COLLATE pg_catalog."default";