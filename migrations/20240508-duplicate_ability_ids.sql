-- Column: public.datacron.duplicate_ability_ids

-- ALTER TABLE IF EXISTS public.datacron DROP COLUMN IF EXISTS duplicate_ability_ids;

ALTER TABLE IF EXISTS public.datacron
    ADD COLUMN duplicate_ability_ids character varying[] COLLATE pg_catalog."default";