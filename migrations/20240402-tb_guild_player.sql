-- Table: public.tb_guild_player

-- DROP TABLE IF EXISTS public.tb_guild_player;

CREATE TABLE IF NOT EXISTS public.tb_guild_player
(
    tb_guild_player_id integer NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1 ),
    allycode character varying COLLATE pg_catalog."default" NOT NULL,
    comlink_guild_id character varying COLLATE pg_catalog."default" NOT NULL,
    phase smallint,
    available boolean NOT NULL DEFAULT true,
    CONSTRAINT tb_guild_player_pkey PRIMARY KEY (tb_guild_player_id),
    CONSTRAINT tb_guild_player_allycode_comlink_guild_id_phase_key UNIQUE (allycode, comlink_guild_id, phase)
)

TABLESPACE pg_default;