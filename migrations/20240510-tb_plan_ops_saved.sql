-- Table: public.tb_plan_ops_saved

-- DROP TABLE IF EXISTS public.tb_plan_ops_saved;

CREATE TABLE IF NOT EXISTS public.tb_plan_ops_saved
(
    plan_name character varying COLLATE pg_catalog."default" NOT NULL,
    comlink_guild_id character varying COLLATE pg_catalog."default" NOT NULL,
    tb_plan_ops_saved_id integer NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1 ),
    planets_by_phase jsonb NOT NULL,
    tb_ops_json jsonb NOT NULL,
    CONSTRAINT tb_plan_ops_saved_pkey PRIMARY KEY (tb_plan_ops_saved_id)
)

TABLESPACE pg_default;