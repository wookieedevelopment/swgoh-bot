-- Table: public.tw_player_priority

-- DROP TABLE IF EXISTS public.tw_player_priority;

CREATE TABLE IF NOT EXISTS public.tw_player_priority
(
    tw_player_priority_id integer NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1 ),
    guild_id integer NOT NULL,
    allycode character varying COLLATE pg_catalog."default" NOT NULL,
    type character(1) COLLATE pg_catalog."default" DEFAULT 'D'::bpchar,
    priority smallint DEFAULT 50,
    CONSTRAINT tw_player_priority_pkey PRIMARY KEY (tw_player_priority_id),
    CONSTRAINT tw_player_priority_guild_id_allycode_key UNIQUE (guild_id, allycode)
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.tw_player_priority
    OWNER to wookiee500;