-- Column: public.tw_player_priority."max-defense-assignments"

-- ALTER TABLE IF EXISTS public.tw_player_priority DROP COLUMN IF EXISTS "max-defense-assignments";

ALTER TABLE IF EXISTS public.tw_player_priority
    ADD COLUMN "max_defense_squads" integer;