-- Column: public.player_data_static.gl_ahsoka

-- ALTER TABLE IF EXISTS public.player_data_static DROP COLUMN IF EXISTS gl_ahsoka;

ALTER TABLE IF EXISTS public.player_data_static
    ADD COLUMN gl_ahsoka boolean;