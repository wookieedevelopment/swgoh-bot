-- Column: public.player_data.zetas

-- ALTER TABLE IF EXISTS public.player_data DROP COLUMN IF EXISTS zetas;

ALTER TABLE IF EXISTS public.player_data
    ADD COLUMN zetas smallint;

-- Column: public.player_data.omicrons

-- ALTER TABLE IF EXISTS public.player_data DROP COLUMN IF EXISTS omicrons;

ALTER TABLE IF EXISTS public.player_data
    ADD COLUMN omicrons smallint;