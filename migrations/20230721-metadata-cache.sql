-- Table: public.metadata_cache

DROP TABLE IF EXISTS public.metadata_cache;

CREATE TABLE IF NOT EXISTS public.metadata_cache
(
    metadata_cache_id integer NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1 ),
    key character varying COLLATE pg_catalog."default" NOT NULL,
    value character varying COLLATE pg_catalog."default",
    CONSTRAINT metadata_cache_key_key UNIQUE (key)
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.metadata_cache
    OWNER to wookiee500;