-- Table: public.bot_activity

-- DROP TABLE IF EXISTS public.bot_activity;

CREATE TABLE IF NOT EXISTS public.bot_activity
(
    bot_activity_id integer NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1 ),
    discord_user_id character varying COLLATE pg_catalog."default",
    start_dt timestamp without time zone NOT NULL,
    interaction_string character varying COLLATE pg_catalog."default",
    CONSTRAINT bot_activity_pkey PRIMARY KEY (bot_activity_id)
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.bot_activity
    OWNER to wookiee500;

-- Table: public.bot_usage

-- DROP TABLE IF EXISTS public.bot_usage;

CREATE TABLE IF NOT EXISTS public.bot_usage
(
    bot_usage_id character varying COLLATE pg_catalog."default" NOT NULL,
    started_count integer NOT NULL DEFAULT 0,
    finished_count integer NOT NULL DEFAULT 0,
    last_used_dt timestamp without time zone,
    CONSTRAINT bot_usage_pkey PRIMARY KEY (bot_usage_id)
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.bot_usage
    OWNER to wookiee500;