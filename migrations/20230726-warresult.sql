-- Table: public.war_result

-- DROP TABLE IF EXISTS public.war_result;

CREATE TABLE IF NOT EXISTS public.war_result
(
    war_result_id integer NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1 ),
    comlink_guild_id character varying COLLATE pg_catalog."default" NOT NULL,
    guild_name character varying COLLATE pg_catalog."default",
    end_time_seconds integer NOT NULL,
    score integer NOT NULL,
    opponent_score integer NOT NULL,
    territory_war_id character varying COLLATE pg_catalog."default",
    CONSTRAINT war_result_pkey PRIMARY KEY (war_result_id),
    CONSTRAINT war_result_comlink_guild_id_end_time_seconds_key UNIQUE (comlink_guild_id, end_time_seconds)
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.war_result
    OWNER to wookiee500;