-- Column: public.leaderboard.display_order

-- ALTER TABLE IF EXISTS public.leaderboard DROP COLUMN IF EXISTS display_order;

ALTER TABLE IF EXISTS public.leaderboard
    ADD COLUMN display_order smallint;