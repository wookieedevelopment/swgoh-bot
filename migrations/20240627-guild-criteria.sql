CREATE TABLE guild_criteria (
    comlink_guild_id VARCHAR PRIMARY KEY,
    gp_min INT DEFAULT 0,
    modq_min DECIMAL(4,2) DEFAULT 0,
    gl_count INT DEFAULT 0,
    missed_conquest_units_count INT DEFAULT 0,
    has_reva BOOLEAN DEFAULT true,
    has_wat BOOLEAN DEFAULT true,
    wat_ready BOOLEAN DEFAULT true,
    reva_ready BOOLEAN DEFAULT true,
    zeffo_ready BOOLEAN DEFAULT true,
    mandalore_ready BOOLEAN DEFAULT true,
    tw_omi_count INT DEFAULT 0,
    dc9_count INT DEFAULT 0
);
