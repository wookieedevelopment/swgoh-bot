-- Table: public.asset_image

-- DROP TABLE IF EXISTS public.asset_image;

CREATE TABLE IF NOT EXISTS public.asset_image
(
    asset_image_id integer NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1 ),
    version integer,
    asset_name character varying COLLATE pg_catalog."default" NOT NULL,
    img bytea NOT NULL,
    CONSTRAINT asset_image_asset_name_key UNIQUE (asset_name)
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.asset_image
    OWNER to wookiee500;
-- Index: ix_asset_name

-- DROP INDEX IF EXISTS public.ix_asset_name;

CREATE INDEX IF NOT EXISTS ix_asset_name
    ON public.asset_image USING btree
    (asset_name COLLATE pg_catalog."default" ASC NULLS LAST)
    TABLESPACE pg_default;

-- Column: public.datacron.icon_asset_name

-- ALTER TABLE IF EXISTS public.datacron DROP COLUMN IF EXISTS icon_asset_name;

ALTER TABLE IF EXISTS public.datacron
    ADD COLUMN icon_asset_name character varying COLLATE pg_catalog."default";