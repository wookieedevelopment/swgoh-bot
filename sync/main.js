require('dotenv').config()
const { Client, Events, GatewayIntentBits, Collection } = require('discord.js');
const database = require("../src/database");
const comlink = require("../src/utils/comlink");

const CLIENT = new Client(
    { 
        intents: [
            GatewayIntentBits.Guilds,
            GatewayIntentBits.GuildMembers
        ] 
    });

// invite link
// https://discord.com/api/oauth2/authorize?client_id=1210035338525933660&scope=bot%20applications.commands

const SUPPORTER_ROLE_ID = '1184183856421683370';
const WOOKIEETOOLS_SERVER_ID = '537868479471026176';

const DEV_IDS = ["276185061970149377", "628359474377129985"];


CLIENT.once('ready', () => {
    console.log(`Logged in as ${CLIENT.user.tag}. Time is: ${new Date().toLocaleDateString()}`);

    setInterval(pollForUpdates, 600000); // 10 minutes
});

async function pollForUpdates() {
    
    try {
        const server = CLIENT.guilds.cache.get(WOOKIEETOOLS_SERVER_ID);

        await server.members.fetch({ cache: false });

        const role = server.roles.cache.get(SUPPORTER_ROLE_ID);

        let subscriberIds = role.members.map(m => m.id);
        let namesAndIds = role.members.map(m => `${m.displayName} <@${m.id}>`);
        console.log(`${subscriberIds.length} subscribers:\n- ${namesAndIds.join('\n- ')}`);

        let subscriberAllycodes = await database.db.any(`SELECT allycode FROM user_registration WHERE discord_id IN ($1:csv)`, [subscriberIds]);
        let devAllycodes = await database.db.any(`SELECT allycode FROM user_registration WHERE discord_id IN ($1:csv)`, [DEV_IDS]);

        let subscriberGuilds = [];
        for (let a of subscriberAllycodes)
        {
            let player = await comlink.getPlayer(a.allycode);
            let guildId = player.guildId;
            if ((guildId?.length ?? 0) > 0) subscriberGuilds.push(guildId);
        }

        let devGuilds = [];
        for (let a of devAllycodes)
        {
            let player = await comlink.getPlayer(a.allycode);
            let guildId = player.guildId;
            if ((guildId?.length ?? 0) > 0) devGuilds.push(guildId);
        }


        await database.db.any(`
    UPDATE guild
    SET priority = 0
    WHERE comlink_guild_id NOT IN ($1:csv) AND priority = 1;

    UPDATE guild 
    SET priority = 1
    WHERE comlink_guild_id IN ($2:csv) AND priority = 0;

    UPDATE guild
    SET priority = 1000
    WHERE comlink_guild_id IN ($3:csv);`, [subscriberGuilds.concat(devGuilds), subscriberGuilds, devGuilds]);

        let priorityGuilds = await database.db.any(`select guild_name from guild where priority > 0`);

        let str = priorityGuilds.map(g => g.guild_name).join("\n");
        console.log(`${priorityGuilds.length} Priority Guilds:\n${str}`);
    } 
    catch (error)
    {
        console.error('Error syncing subscribers: ', error)
    }
}

CLIENT.login(process.env.SYNC_BOT_TOKEN);